<?php
/**
 * @package kraken\modules\categories
 */

if(!function_exists('module_categories')) {
	/**
	 * Returns array of categories within a module.
	 *
	 * @param string $module The module you want to get the categories of.
	 * @param string $type The type within the module you want to get the categories of. Default = NULL
	 * @param array $c An array of configuration values. Default = NULL
	 * @return array An array of the categories in the given module in a array(id => row) format.
	 */
	function module_categories($module,$type = NULL,$c = NULL) {
		// Module
		$m = module::load('categories');
		
		// Categories
		$rows = $m->rows(array('where' => "module_code = '".$module."'".($type ? " AND (type_code = '".a($type)."' OR type_code = '')" : ""),'db.visible' => 1));
		while($row = $m->db->f($rows)) {
			$array[$row[m('categories.db.id')]] = $row;
		}
		
		// Return
		return $array;
	}
}

if(!function_exists('categories_items')) {
	/**
	 * Gets an array of category items in the given category (includes category hiearchy).
	 *
	 * @param int $id The id of the category we want to get the items of.
	 * @param array $c An array of configuration values. Default = NULL
	 * @return array An array of the items in the category in array(id => row) format.
	 */
	function categories_items($id,$c = NULL) {
		// Config
		if(!$c['parent']) $c['parent'] = 0; // The id of the parent category item we want to get the children of. Used in our recursive loop.
		
		// Database
		$db = db::load();
		
		// Module
		$module = 'categories_items';
		
		// Code, get id
		if($id and !is_int_value($id)) $id = code_id('categories',$id);
		
		// Error
		if(!$id) return;
		
		// Query
		$query = "
		SELECT
			c1.*,
			c2.item_id child_id
		FROM
			categories_items c1
			LEFT JOIN categories_items c2
				ON c2.item_order_parent = c1.item_id
				AND c2.item_visible = 1
		WHERE
			c1.category_id = '".$id."'
			AND c1.item_order_parent = '".$c['parent']."'
			AND c1.item_visible = 1
		GROUP BY
			c1.item_id
		ORDER BY";
		if(m($module.'.db.order')) $query .= "
			".m($module.'.db.order')." ASC,";
		$query .= "
			".m($module.'.db.id')." ASC";
		
		// Items
		$rows = $db->q($query);
		while($row = $db->f($rows)) {
			$item_id = $row[m($module.'.db.id')];
			$array[$item_id] = $row;
			
			// Children
			if($row[child_id]) {
				$_c = $c;
				$_c['parent'] = $item_id;
				$children = categories_items($id,$_c);
				if($children) $array[$item_id][children] = $children;
			}
		}
		
		// Return
		return $array;
	}
}

if(!function_exists('categories_options')) {
	/**
	 * Gets a category's items and creates an 'options' array for use in the form class (includes category hiearchy).
	 *
	 * @param int $id The id of the category we want to get item options of.
	 * @param array $c An array of configuration values. Default = NULL
	 * @return array An array of options for selecting a category item within the category.
	 */
	function categories_options($id,$c = NULL) {
		// Config
		if(!$c['string']) $c['string'] = NULL; // String to append to begining of each value. Used for tweaking option value in recursive loops.
		
		// Items
		if(!$c[items]) $c[items] = categories_items($id);
		
		// Options
		$options = NULL;
		foreach($c[items] as $id => $row) {
			// Children
			$children = NULL;
			if($row[children]) {
				$_c = $c;
				$_c[items] = $row[children];
				//$_c['string'] .= $id."."; // Not using for now
				$children = categories_options($id,$_c);
			}
			// Option
			$options[$id] = array(
				'value' => $c['string'].$id,
				'label' => $row[m('categories_items.db.name')],
				'options' => $children
			);
		}
		
		// Return
		return $options;
	}
}
?>