<?php
if(!class_exists('form_input_file_swfupload_framework',false)) {
	/**
	 * An extension of the form_input_file_framework class with functionality specific to swfupload inputs.
	 *
	 * Note, this should probably be 2 classes, form_input_file_swfupload (which extends form_input_file)
	 * and form_input_file_swfupload_framework (which extends form_input_file_framework), but the difference
	 * in the two html_field() methods would only be the 'browse' bit, so skip it.
	 */
	class form_input_file_swfupload_framework extends form_input_file_framework {
		/**
		 * Loads class. Needed so we can use debug_backtrace() to get this class name in PHP < 5.3 in $form_input->load().
		 */
		static function load($label = NULL,$name = NULL,$value = NULL,$c = NULL) {
			return parent::load($label,$name,$value,$c);
		}
		
		/**
		 * Construct
		 */
		function __construct($label = NULL,$name = NULL,$value = NULL,$c = NULL) {
			$this->form_input_file_swfupload_framework($label,$name,$value,$c);
		}
		function form_input_file_swfupload_framework($label = NULL,$name = NULL,$value = NULL,$c = NULL) {
			// Construct
			parent::__construct($label,$name,$value,$c);
			
			// Attributes - class
			$this->attributes['class'] .= ($this->attributes['class'] ? " " : "")."swfupload-placeholder";
		}
		
		/**
		 * Renders the HTML for the input's 'field'.
		 *
		 * @param object $form The form object this input is a part of.
		 * @param array $c An array of config values. Default = NULL
		 * @return string The HTML of the input's field.
		 */
		function html_field($form,$c = NULL) {
			// Mobile
			if(browser_mobile()) return parent::html_field($form,$c);
			
			// Variables
			$r = random();
			
			// JS/CSS - Include both of these more than once (2nd param == 0) as input might get tested in $module->settings() method function without the HTML being displayed
			$html .= include_css('swfupload',0); 
			$html .= include_javascript('swfupload',0);
					
			// Prepend
			if($this->prepend) $html .= $this->prepend;
			
			// Button
			$settings = array(
				'upload_url' => D."?ajax_action=uploadIt&flash=1&name=".$this->name."&path=".$this->c[path],
				'file_post_name' => 'swfupload_'.$r,
				'limit' => 1,
				'instant_upload' => 0,
				'test' => 1,
			);
			if($this->c[overwrite] === "check") { // Not sure why we need ===, but we do (0 will match if only ==)
				$settings[on_queue] = "function(event,file,id) {file_overwrite_check({id:id,path:\"".$this->c[path]."\",file:file.name,swfupload:1,swfupload_file:file});}";
			}
			if($this->validate[accept]) $settings[file_types] = "*.".str_replace(',',";*.",str_replace('.','',$this->validate[accept]));
			$html .= "
<div class='swfupload ".json_encode_javascript($settings)."'>";
			
			// Append
			if($this->append) $html .= $this->append;
			
			// Browse - icon
			if($browse = $this->browse($form,$c)) $html .= $browse;
		
			// Help
			if($this->help) $html .= help($this->help);
				
			// Error
			if($this->error) $html .= "
				<label class='error form-error' for='".$this->name."' style='display:inline;'>".$this->error."</label>";
				
			// Input - will be hidden via javascript, but serves as the actual file input we use when swfupload not enabled
			$input = form_input_file::load($this->label,$this->name,$this->value,$this->c_duplicate());
			//$input->validate[required] = 0; // Don't require // Do require, SWFUpload can deal with it
			$html .= $input->html_element($form,$c);
			
			// Browse / preview
			if($this->value and $this->c[path] and $this->c[preview] == 1) {
				$html .= "
	<div class='clear' style='height:4px;'></div>
	<div class='swfupload-info'>";
				// Browse
				if($browse) $html .= "
		<div class='form-browse-preview'>";	
				// Preview
				$html .= "
			<div class='swfupload-previous'>
				".$this->preview($form,$c)."
			</div>";
			
				// End browse
				if($browse) $html .= "
		</div>";	
				$html .= "
	</div>";
			}
			else $html .= "
	<div class='form-browse-preview'></div>";
			
			$html .= "
</div>";
			
			// Return
			return $html;
		}
	}
}
?>