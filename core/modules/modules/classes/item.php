<?php
if(!class_exists('item_modules',false)) {
	/**
	 * An extension of the item class with functionality specific to the modules module.
	 *
	 * @package kraken\modules\modules
	 */
	class item_modules extends item {
		/**
		 * Constructs the class
		 *
		 * @param string $module The module this item is in.
		 * @param int $id The id of the item.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function __construct($module = NULL,$id = NULL,$c = NULL) {
			return $this->item_modules($module,$id,$c);
		}
		function item_modules($module = NULL,$id = NULL,$c = NULL) {
			parent::__construct($module,$id,$c);
		}
		
		/**
		 * Creates and returns the HTML of an icon for performing some action on this item.
		 *
		 * This method basically lets us check permission and data within this item before we send it to the $core->icon() method.
		 * See $core->icon() for more configuration values.
		 *
		 * @param string $action The action of the icon you're creating.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return string The HTML of the icon.
		 */
		function icon($action,$c = NULL) {
			// Error
			if(!$this->module or !$this->id) return;
		
			// Module
			$module = $this->db('code');
			$module_class = module::load($module);
			
			// Settings
			if($action == "settings") {
				if(!$c[icon]) $c[icon] = $action;
				if($module_class->permission($action)) {
					if($module_class->settings(1)) {
						// Area
						if(!$c[area]) {
							$page = g('page');
							if(in_array($page->area,array('admin','account'))) $c[area] = $page->area;
							else if(u('admin')) $c[area] = 'admin';
							else $c[area] = 'account';
						}
						// URL
						$c[url] = DOMAIN.$c[area]."/".$action."?module=".$module;
					}
				}
				if(!$c[url]) $c[url] = 0;
			}
			// Documentation
			if($action == "documentation") {
				if(!$c[icon]) $c[icon] = "help";
				if(!$c[tooltip_text]) $c[tooltip_text] = "Help";
				if($file = $module_class->documentation_file()) {
					$c[permission] = 0;
					$c[url] = D."?ajax_action=documentation&amp;ajax_module=".$module/*."&amp;file=".urlencode($file)*/; // The ajax documentation script will load the file and the class it uses within the file for $this
					$c[attributes]['class'] .= ($c[attributes]['class'] ? " " : "")."overlay {title:''}";
				}
				else $c[url] = 0; // Unnecessary as we can never have 'documentation' permission so no $c[url] will ever get created, but keep just for the hell of it
			}
			// Download
			if($action == "download") {
				if(!$c[icon]) $c[icon] = "arrow-down";
				if(u('type') == "supers" and !$module_class->v('parent')) {
					$c[permission] = 0;
					$c[url] = DOMAIN."?process_action=modules_download&amp;process_module=".$this->module."&amp;module=".$module;
				}
				else $c[url] = 0; // Unnecessary as we can never have 'download' permission so no $c[url] will ever get created, but keep just for the hell of it
			}
			// Download rows
			if($action == "download-rows") {
				if(!$c[icon]) $c[icon] = "module_rows";
				if(!$c[tooltip_text]) $c[tooltip_text] = "Download Rows";
				if(u('type') == "supers" and $module_class->v('db.table')) {
					$c[permission] = 0;
					$c[url] = DOMAIN."?process_action=modules_download&amp;process_module=".$this->module."&amp;module=".$module."&amp;rows=1";
				}
				else $c[url] = 0; // Unnecessary as we can never have 'download' permission so no $c[url] will ever get created, but keep just for the hell of it
			}
			// Delete
			if($action == "delete") {
				// Can't delete core modules
				if($module_class->v('core')) {
					$c[url] = 0;	
				}
			}
			
			// Parent
			return parent::icon($action,$c);
		}
	}
}
?>