<?php
// Heading
print $this->heading();
// Buttons
print $this->heading_buttons();
// Filters
//print $this->module_class->manage_filters();

// Variables
$module = NULL;
$type = NULL;
if($_GET['module']) {
	list($module,$type) = explode('.',urldecode($_GET['module']),2);
}
/*else if($this->page->parents) { // No longer use /{module}/settings, now we use /settings?module={module}
	$type = $this->page->parents[last][type];
	$module = $this->page->parents[last][module];
}*/
if(!$module) {
	$module = "settings";
	$type = NULL;
}

// Filters
if($module == "settings" or $_GET['module']) {
	if(!$_GET['module']) $_GET['module'] = $module;
	$form = form::load(array('method' => 'GET','inline' => 1,'process' => 0,'submit' => 0));
	$options = NULL;
	// Global settings
	if(m('settings.settings.inputs')) {
		$options[settings] = "Global Settings";
		$options[""] = "--------";
	}
	// Module settings
	foreach(m() as $k => $v) {
		$module_class = module::load($k);
		if($module_class->permission('settings') and !$options[$k]) {
			if($module_class->settings(1) and !$options[$k]) {
				$options[$k] = $v[name];
				if($v[types]) {
					foreach($v[types] as $type_k => $type_v) {
						$options[$k.".".$type_k] = "&nbsp;&nbsp;&nbsp;&nbsp;".$type_v[name];
					}
				}
			}
		}
	}
	$form->select(NULL,'module',$options,$_GET['module']);
	$form->submit('Go');
	print $form->render()."<br />";
}

// Settings
$module_class = module::load($module,array('type' => $type));
if($module_class->permission('settings')) {
	$settings = $module_class->settings(1);
	if($settings) {
		// Values
		foreach($settings as $k => $v) {
			if($v[name]) {
				// Value
				$name = array_string($v[name]);
				if($type) $settings[$k][value] = m($module.'.types.'.$type.'.settings.'.$name); // If type, only insert value if value is defined for this type
				else $settings[$k][value] = m($module.'.settings.'.$name);
					
				// Name
				$name = string_array(($type ? "type" : "module")."_settings.".$name);
				$settings[$k][name] = $name;
				
				// Type - if 'type' settings, add blank placeholder in case they want to use the module's setting
				if($type) {
					// Placeholder
					if($v[type] == "select" and !x($settings[$k][placeholder])) $settings[$k][placeholder] = ' '; // Add blank placeholder in case they want to use the module's setting
					// Remove required
					unset($settings[$k][validate][required],$settings[$k][required]);
				}
			}
		}
		
		// Form
		if($type) {
			$form = form::load("types",m($module.'.id',$type),array('redirect' => URL,'plugins' => 0));
			/*$form->text('Name','type_name',NULL,array('validate' => array('required' => 1)));
			$form->text('Singular','type_settings[single]',m($module.'.single',$type),array('validate' => array('required' => 1)));
			$form->text('Plural','type_settings[plural]',m($module.'.plural',$type),array('validate' => array('required' => 1)));*/
		}
		else {
			$form = form::load("modules",m($module.'.id'),array('redirect' => URL,'plugins' => 0));
			// Right now going with name (and international values) must be defined in config.php file. Could switch to doing name/single/plural in settings by uncommenting these fields and updating modules() array creation. Woulud also have to set up for types too though.
			/*$form->text('Name','module_name',NULL,array('validate' => array('required' => 1)));
			$form->text('Singular','module_settings[single]',m($module.'.single'),array('validate' => array('required' => 1)));
			$form->text('Plural','module_settings[plural]',m($module.'.plural'),array('validate' => array('required' => 1)));*/
		}
		$form->inputs($settings);
		print $form->render();
	}
	else print "
<div class='core-none'>No Settings Found</div>";
}
else print "
<div class='core-none'>You don't have permission to edit these settings.</div>";
?>