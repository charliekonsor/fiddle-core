<?php
/************************************************************************************/
/********************************* Local / Framework ********************************/
/************************************************************************************/
// Local
include_once SERVER."local/includes/ajax.php";

// Framework
if(FRAMEWORK) {
	// Core - framework - admin
	/*if(strstr($_SESSION['urls'][0],"admin"))*/ include_once SERVER."core/framework/admin/includes/ajax.php"; // Find a way to limit this
	// Core - framework
	include_once SERVER."core/framework/includes/ajax.php";
}

/************************************************************************************/
/************************************** Scripts *************************************/
/************************************************************************************/
/**
 * Checks to see if a username or e-mail address is already taken.
 *
 * Note, this is the 'core' version. We also have a framework version in core/modules/users/includes/ajax.php which overwrites this if framework is on.				    				
 */
if($_GET['ajax_action'] == "checkUsername" or $_GET['ajax_action'] == "checkEmail") {
	// ID
	$id = $_GET['id'];
	
	// Debug
	//$core->db->q("INSERT INTO test SET test = '".a("request = ".$_GET['ajax_action'])."'");
	$core->db->q("INSERT INTO test SET test = '".a("id = ".$id)."'");

	// Username
	if($_GET['ajax_action'] == "checkUsername") {
		$return = (user_name_exists($_GET['user_name'],$id) ? "false" : "true");
	}
	
	// E-mail
	if($_GET['ajax_action'] == "checkEmail") {
		if(g('config.register.email.unique')) {
			$return = (user_email_exists($_GET['user_email'],$id) ? "false" : "true");
		}
		else $return = "true";
	}
	
	// Debug
	$core->db->q("INSERT INTO test SET test = '".a("return = ".$return)."'");
	
	// Return
	print $return;
	
	// Exit
	exit;
}

/**
 * Processes files uploaded via AJAX.				
 */
if($_GET['ajax_action'] == "uploadIt") {
	if(!g('u.id') and $_GET['flash'] != 1) print "
<div class='core-red'>You don't have permission to do this.</div>";
	else {
		// Timeout settings (1 Hour)
		ini_set('max_execution_time',3600);
		set_time_limit(3600);
		// File size settings
		ini_set('upload_max_filesize','500M');
		ini_set('post_max_size','500M');
		
		// Variables
		$path = $_GET['path'];
		$name = $_GET['name'];
		$error = 0;
		
		if($path) {
			// Files
			$files = files_array($_FILES);
			foreach($files as $k => $v) {
				$file = NULL;
				$url = NULL;
				
				// Upload - advanced
				if(class_exists('file')) {
					// Overwrite?
					$overwrite = 0;
					if(x($_POST['overwrite'])) $overwrite = $_POST['overwrite'];
					
					// Upload
					$f = file::load($v);
					if($f_saved = $f->save($path,array('overwrite' => $overwrite,'debug' => 0))) {
						$file = $f_saved->name;
						$url = $f_saved->url();
					}
				}
				// Upload - simple
				else {
					move_uploaded_file($v[tmp_name],$path);
					$file = $v[name];
				}
				
				// Results
				if($file) {
					// Message/Input
					print "
<span class='core-green'>".string_limit($v[name],20)." uploaded!</span>
<input type='hidden' name='".$name."' value='".$file."' />";

					// Preview
					if($_GET['preview'] == 1 and $url) {
						// Photo
						/*if($type == "photo") print "
		<a href='".D."uploads/photos/o/".$file."' class='thickbox' title='".s($file,1)."'><img src='".D."uploads/photos/s/".$file."' alt='".s($file,1)."' class='box' /></a>";
						// Default
						else*/ print "
<a href='".$f->url()."' target='_blank'>".s($file)."</a><br />";
					}
				}
				// Error
				else $error = 1;
			}
		}
		else $error = 1;
		
		// Error
		if($error) print "
<span class='core-red'>There was an error uploading ".string_limit($v[name],20)."</span>";
	}
	
	// Exit
	exit;
}

/**
 * Processes a file uploaded via CKEditor.
 */
if($_GET['ajax_action'] == "file_upload_ckeditor") {
	// Variables
	$error = NULL;
	$overwrite = NULL;
	$file = g('unpure.files.upload');
	if(x($_GET['overwrite'])) {
		$overwrite = $_GET['overwrite'];
		$file = g('unpure.get.file');
	}
	print "
<html>
<body>";
	
	// Exists?
	if(file::exists($_GET['path'].$file[name]) and !x($overwrite)) {
		// Yes, show dialog for choosing what happens with it
		$_GET['file'] = $file;
		$url = DOMAIN."?".http_build_query($_GET);
		print "
<script type='text/javascript'>
window.onload = function() {
	window.parent.file_overwrite_check({path:'".$_GET['path']."',file:'".string_encode($file[name])."',ckeditor:'".$url."'});
}
</script>";
		/*print "
A file with the same name already exists.<br />
Do you want to overwrite the existing file or rename it?<br />

<a href='".$url."&overwrite=1' class='file-overwrite-button file-overwrite-button-overwrite'>Overwrite</a>
<a href='".$url."&overwrite=0' class='file-overwrite-button file-overwrite-button-rename'>Rename</a>";*/
	}
	// Upload
	else {
		$f = file::load($file);
		if($f_saved = $f->save($_GET['path'],array('overwrite' => $overwrite,'debug' => 0))) {
			$file = $f_saved->name;
			$url = $f_saved->url();
			$url = url_unescape($url);
			$url = str_replace("'","\\'",$url);
		}
		else {
			$error = "There was an error uploading this file.";	
		}
	
		// Results
		print "
<script type='text/javascript'>
window.parent.CKEDITOR.tools.callFunction(".$_GET['CKEditorFuncNum'].",'".$url."'".($error ? ",'".$error."'" : "").");
</script>";
	}
	
	print "
</body>
</html>";
	
	// Exit
	exit;
}

/**
 * Loads 'file browser' for selecting file from the given directory.				
 */
if($_GET['ajax_action'] == "file_browse") {
	// Input
	$input = cache_get("forms/inputs/".$_GET['input'],array('force' => 1));
	
	// Path
	$path = $input->c[browse][path];
	if(substr(str_replace(SERVER,'',$path),0,13) == "local/uploads") { // Simple security check so they can't just browse the entire server
		// Class
		require_once SERVER."core/core/classes/file_browse.php";
		$browse = file_browse::load($path);
		
		// HTML
		$c = array(
			'url_file' => D."?ajax_action=file_browse_file&input=".$_GET['input'],
			'row' => $input->c[browse][row],
			'type' => $input->c[browse][type],
			'preview_path' => $input->c[browse][preview_path],
		);
		print $browse->html($c);
	}
	
	// Exit
	exit;
}

/**
 * Loads 'file browser' for selecting file from the given directory within CKEditor.			
 */
if($_GET['ajax_action'] == "file_browse_ckeditor") {
	// Path
	$path = $_GET['path'];
	if(substr($path,0,13) == "local/uploads") { // Simple security check so they can't just browse the entire server
		// Header
		$page = page::load();
		$page->meta_title("File Browser");
		$html = $page->header();
		
		// Class
		require_once SERVER."core/core/classes/file_browse.php";
		$browse = file_browse::load($path);
		
		// HTML
		$html .= $browse->html();
		print $page->html_organize($html);
	}
	
	// Exit
	exit;
}

/**
 * Loads files from given directory in 'file browser'.				
 */
if($_GET['ajax_action'] == "file_browse_files") {
	require_once SERVER."core/core/classes/file_browse.php";
	$directory = url_unescape(g('unpure.get.directory'));
	$browse = file_browse::load($directory);
	print $browse->html_files();
	
	// Exit
	exit;
}

/**
 * Loads preview of selected file so we can add it to the form.		
 */
if($_GET['ajax_action'] == "file_browse_file") {
	// Form
	$form = form::load();
	// Input
	$input = cache_get("forms/inputs/".$_GET['input']);
	// File
	$file = url_unescape(g('unpure.get.file'));
	// Preview
	print $input->preview($form,array('browse' => $file));
	
	// Exit
	exit;
}

/**
 * Deletes the given file (from the file browser modal).
 */
if($_GET['ajax_action'] == "file_browse_delete") {
	print_array($_GET);
	if(u('admin')) {
		// File
		$file = url_unescape(g('unpure.get.file'));
		$f = file::load($file);
		print_array($f);
		// Delete
		$f->delete(array('debug' => 1));
	}
	
	// Exit
	exit;
}

/**
 * Saves what you want to do with a file when another file with the same names exists at the same location.
 */
if($_POST['ajax_action'] == "file_overwrite_save") {
	print_array($_POST);
	// Config
	$c = json_decode(g('unpure.post.c'),true);
	$c[file] = url_unescape($c[file]);
	print_array($c);
	
	// Save
	$_SESSION['__file_overwrite'][$c[id]][$c[file]] = $_POST['overwrite'];
	
	// Exit
	exit;	
}

?>