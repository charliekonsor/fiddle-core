<?php
/************************************************************************************/
/*************************************** Config *************************************/
/************************************************************************************/
// Speed
debug_speed('framework - dawn');
debug_speed('config','framework - dawn');

// Config
$config = NULL;
include_once SERVER."core/framework/config.php";
if($config) $__GLOBAL['config'] = array_merge_associative($__GLOBAL['config'],$config);

/************************************************************************************/
/************************************* Functions ************************************/
/************************************************************************************/
// Speed
debug_speed('functions','framework - dawn');

// Core
require_once SERVER."core/framework/functions/core.php";
// User
require_once SERVER."core/framework/functions/user.php";
// Crons
require_once SERVER."core/framework/functions/crons.php";

/************************************************************************************/
/************************************** Classes  ************************************/
/************************************************************************************/
// Speed
debug_speed('classes','framework - dawn');

/**
 * Autoloads non-essential classes.
 *
 * Note, we can use just 'include' because it will only ever be called once.
 *
 * @param string $class The name of the class we're trying to load.
 */
if(!function_exists('autoload')) {
	function autoload($class) {
		//print "autoload(".$class.")<br />";
		
		// Already attempted to autoload
		if(g('autoload.'.$class)) {
			//print "already tried to load ".$class."<br />";
			return;
		}
		// Save autoload attempt
		g('autoload.'.$class,1);
		
		// Hybrid Auth
		if($class == "Hybrid_Auth") {
			require_once SERVER."core/core/libraries/hybridauth/Hybrid/Auth.php";
			return;
		}
		
		// Framework
		$framework = 0;
		if(strstr($class,'_framework')) {
			$framework = 1;
			$class = str_replace('_framework','',$class);
		}
		$class = str_replace('_local','',$class);
		
		// Explode
		$ex = explode('_',$class);
		
		// Variables
		$folder = NULL;
		$module = NULL;
		$module_v = NULL;
		$type = NULL;
		$type_v = NULL;
		$plugin = NULL;	
		$plugin_v = NULL;
		
		// Folder
		if($ex[0] == "form") $folder = "form/";
			
		// Form input
		if($ex[0] == "form" and $ex[1] == "input") {
			$class = implode('_',$ex);
		}
		// Plugin
		else if($ex[0] == "plugin") {
			$class = $ex[0];
			
			$start = 1;
			if(in_array($ex[1],array('item','api','cart'))) {
				$class .= "_".$ex[1];
				$start++;
				if(in_array($ex[2],array('api','cart'))) {
					$class .= "_".$ex[2];
					$start++;
				}
			}
			
			for($x = ($start + 5);$x >= $start;$x--) {
				$string = NULL;
				for($y = $start;$y <= $x;$y++) {
					$string .= ($string ? "_" : "").$ex[$y];
				}
				if($plugin_v = plugin($string)) { // plugin_{plugin}
					$plugin = $string;
					$start = $x + 1;
					
					for($x = ($start + 5);$x >= $start;$x--) {
						$string = NULL;
						for($y = $start;$y <= $x;$y++) {
							$string .= ($string ? "_" : "").$ex[$y];
						}
						if($module_v = m($string)) { // plugin_{plugin}_{module}
							$module = $string;
							$start = $x + 1;
							
							for($x = ($start + 5);$x >= $start;$x--) {
								$string = NULL;
								for($y = $start;$y <= $x;$y++) {
									$string .= ($string ? "_" : "").$ex[$y];
								}
								if($type_v = $module_v[types][$string]) { // plugin_{plugin}_{module}_{type}
									$type = $string;
									break;
								}
							}
							break;
						}
					}
					break;
				}
			}
		}
		// Module
		else if(in_array($ex[0],array('form','module','item','cart'))) {
			$class = $ex[0];
			
			$start = 1;
			if(in_array($ex[1],array('api'))) {
				$class .= "_".$ex[1];
				$start++;
			}
			
			for($x = ($start + 5);$x >= $start;$x--) {
				$string = NULL;
				for($y = $start;$y <= $x;$y++) {
					$string .= ($string ? "_" : "").$ex[$y];
				}
				if($module_v = m($string)) { // item_{module}
					$module = $string;
					$start = $x + 1;
					
					for($x = ($start + 5);$x >= $start;$x--) {
						$string = NULL;
						for($y = $start;$y <= $x;$y++) {
							$string .= ($string ? "_" : "").$ex[$y];
						}
						if($type_v = $module_v[types][$string]) { // item_{module}_{type}
							$type = $string;
							break;
						}
					}
					break;
				}
			}
		}
		
		// Old method
		/*// Form
		if($ex[0] == "form") {
			$folder = "form/";
			$class = $ex[0];
			
			if($ex[1] == "input") { // form_input_..
				unset($ex[0]);
				$class .= "_".implode('_',$ex);
			}
			else if($ex[1] and $ex[2] and $ex[3] and $module_v = m($ex[1]."_".$ex[2]."_".$ex[3])) {
				$module = $ex[1]."_".$ex[2]."_".$ex[3];	
				if($ex[4] and $type_v = $module_v[types][$ex[4]]) {
					$type = $ex[4];	
				}
			}
			else if($ex[1] and $ex[2] and $module_v = m($ex[1]."_".$ex[2])) {
				$module = $ex[1]."_".$ex[2];	
				if($ex[3] and $type_v = $module_v[types][$ex[3]]) {
					$type = $ex[3];	
				}
			}
			else if($ex[1] and $module_v = m($ex[1])) { // form_framework_{module}
				$folder = NULL;
				$module = $ex[1];
				if($ex[2] and $type_v = $module_v[types][$ex[2]]) {
					$type = $ex[2];	
				}
			}
		}
			
		// Plugin
		else if($ex[0] == "plugin") {
			$class = $ex[0];
			
			if(in_array($ex[1],array('item','api','cart'))) {
				$class .= "_".$ex[1];
				if(in_array($ex[2],array('api','cart'))) { // plugin_item_api
					$class .= "_".$ex[2];
					if($ex[3] and $plugin_v = plugin($ex[3])) $plugin = $ex[3];
					if($ex[4] and $module_v = m($ex[4])) { // plugin_item_api_{plugin}_{module}
						$module = $ex[4];
						if($ex[5] and $type_v = $module_v[types][$ex[5]]) { // plugin_item_api_{plugin}_{module}_{type}
							$type = $ex[5];	
						}
					}
				}
				else { // plugin_item
					if($ex[2] and $plugin_v = plugin($ex[2])) $plugin = $ex[2]; // plugin_item_{plugin}
					if($ex[3] and $module_v = m($ex[3])) { // plugin_item_{plugin}_{module}
						$module = $ex[3];
						if($ex[4] and $type_v = $module_v[types][$ex[4]]) { // plugin_item_{plugin}_{module}_{type}
							$type = $ex[4];	
						}
					}
				}
			}
			else { // plugin
				$plugin_v = plugin($ex[1]);
				if($ex[1] and x($plugin_v)) $plugin = $ex[1]; // plugin_{plugin}
				if($ex[2] and $module_v = m($ex[2])) { // plugin_{plugin}_{module}
					$module = $ex[2];
					if($ex[3] and $type_v = $module_v[types][$ex[3]]) { // plugin_{plugin}_{module}_{type}
						$type = $ex[3];	
					}
				}
			}
		}
		// Module
		else if(in_array($ex[0],array('module','item'))) {
			$class = $ex[0];
			
			if(in_array($ex[1],array('api'))) {
				$class .= "_".$ex[1];
				if($ex[2] and $ex[3] and $module_v = m($ex[2]."_".$ex[3])) {
					$module = $ex[2]."_".$ex[3];
					if($ex[4] and $type_v = $module_v[types][$ex[4]]) {
						$type = $ex[4];	
					}
				}
				if($ex[2] and $module_v = m($ex[2])) {
					$module = $ex[2];
					if($ex[3] and $type_v = $module_v[types][$ex[3]]) {
						$type = $ex[3];	
					}
				}
			}
			else {
				if($ex[1] and $ex[2] and $ex[3] and $module_v = m($ex[1]."_".$ex[2]."_".$ex[3])) {
					$module = $ex[1]."_".$ex[2]."_".$ex[3];	
					if($ex[4] and $type_v = $module_v[types][$ex[4]]) {
						$type = $ex[4];	
					}
				}
				else if($ex[1] and $ex[2] and $module_v = m($ex[1]."_".$ex[2])) {
					$module = $ex[1]."_".$ex[2];	
					if($ex[3] and $type_v = $module_v[types][$ex[3]]) {
						$type = $ex[3];	
					}
				}
				else if($ex[1] and $module_v = m($ex[1])) {
					$module = $ex[1];	
					if($ex[2] and $type_v = $module_v[types][$ex[2]]) {
						$type = $ex[2];	
					}
				}
			}
		}*/
					
		//print "autoloading ".$class." (module: ".$module.", type: ".$type.", plugin: ".$plugin.", folder: ".$folder.", framework: ".$framework.")<br />";
			
		// Plugin / module / type
		if($plugin and $module and $type) {
			$files[] = SERVER.$type_v[path]."plugins/".$plugin."/classes/".$class.".php";
			if($type_v[core]) $files[] = SERVER.$type_v[path_local]."plugins/".$plugin."/classes/".$class.".php";
		}
		// Plugin / module
		else if($plugin and $module) {
			$files[] = SERVER.$module_v[path]."plugins/".$plugin."/classes/".$class.".php";
			if($module_v[core]) $files[] = SERVER.$module_v[path_local]."plugins/".$plugin."/classes/".$class.".php";
		}
		// Plugin
		else if($plugin) {
			$files[] = SERVER.$plugin_v[path]."classes/".$class.".php";
			if($plugin_v[core]) $files[] = SERVER.$plugin_v[path_local]."classes/".$class.".php";
		}
		// Modue / type
		else if($module and $type) {
			$files[] = SERVER.$type_v[path]."classes/".$class.".php";
			if($type_v[core]) $files[] = SERVER.$type_v[path_local]."classes/".$class.".php";
		}
		// Modue 
		else if($module) {
			$files[] = SERVER.$module_v[path]."classes/".$class.".php";
			if($module_v[core]) $files[] = SERVER.$module_v[path_local]."classes/".$class.".php";
		}
		// Core
		else { 
			if(!$framework) $files[] = SERVER."core/core/classes/".$folder.$class.".php";
			$files[] = SERVER."core/framework/classes/".$folder.$class.".php";
		}
		
		// Include
		if($files) {
			//print_array($files);
			foreach($files as $file) {
				if(file_exists($file)) {
					//print "including ".$file."<br />";
					include_once $file;	
				}
			}
		}
	}
}
spl_autoload_register('autoload');
if(function_exists('__autoload')) spl_autoload_register('__autoload');

// Classes - if something else is using __autoload() (ex: kcfinder)
/*else {
	// Loader
	require_once SERVER."core/framework/classes/loader.php";
	// View
	require_once SERVER."core/framework/classes/view.php";
	// Page
	require_once SERVER."core/framework/classes/page.php";
	// Module
	require_once SERVER."core/framework/classes/module.php";
	// Item
	require_once SERVER."core/framework/classes/item.php";
	//require_once SERVER."core/framework/classes/item_form.php";
	// Plugin
	require_once SERVER."core/framework/classes/plugin.php";
	// Plugin item
	require_once SERVER."core/framework/classes/plugin_item.php";
	// Form
	require_once SERVER."core/framework/classes/form/form.php";
	require_once SERVER."core/framework/classes/form/form_input.php";
	// Form - core - need to be included after /core/framework/form/form_input.php as all input classes extend the form_input_framework class
	$files = file::directory_files(SERVER."core/core/classes/form/");
	usort($files,'array_compare_length'); // Want shortest first so they 'extend' properly (meaning parent function is included before child)
	foreach($files as $file) {
		require_once SERVER."core/core/classes/form/".$file;
	}
	// Form - framework - all need to be included here (even though we'll include framework input classes in their core class) since we cache them
	$files = file::directory_files(SERVER."core/framework/classes/form/");
	usort($files,'array_compare_length'); // Want shortest first so they 'extend' properly (meaning parent function is included before child)
	foreach($files as $file) {
		require_once SERVER."core/framework/classes/form/".$file;
	}
	// Cart
	if(g('config.cart.active')) {
		require_once SERVER."core/framework/classes/cart.php";
		require_once SERVER."core/framework/classes/plugin_cart.php";
		require_once SERVER."core/framework/classes/plugin_item_cart.php";
	}
	// API
	if(g('config.api.active')) {
		require_once SERVER."core/framework/classes/api.php";
		require_once SERVER."core/framework/classes/module_api.php";
		require_once SERVER."core/framework/classes/item_api.php";
		require_once SERVER."core/framework/classes/plugin_api.php";
		require_once SERVER."core/framework/classes/plugin_item_api.php";
	}
}*/

/************************************************************************************/
/************************************* Variables ************************************/
/************************************************************************************/
// Speed
debug_speed('variables','framework - dawn');

// Database
$db = cache_get('framework/db');
if(!$db) {
	$db = array(
		'table' => array(
			'label' => 'Table'
		),
		'id' => array(
			'label' => 'Item ID',
			'search' => 1,
			'weight' => 8,
		),
		'parent_module' => array(
			'label' => 'Parent module',
		),
		'parent_id' => array(
			'label' => 'Parent Item ID',
		),
		'parent_plugin' => array(
			'label' => 'Parent Plugin',
		),
		'parent_plugin_id' => array(
			'label' => 'Parent Plugin Item ID',
		),
		'type' => array(
			'label' => 'Type'
		),
		'user' => array(
			'label' => 'User'
		),
		/*'code' => array( // Moved to 'code' plugin
			'label' => 'Code',
			'search' => 1,
			'weight' => 9,
		),*/
		'name' => array(
			'label' => 'Name',
			'search' => 1,
			'weight' => 10,
		),
		'description' => array(
			'label' => 'Description',
			'search' => 1,
			'weight' => 4,
		),
		'file' => array(
			'label' => 'File'
		),
		'image' => array(
			'label' => 'Image'
		),
		'video' => array(
			'label' => 'Video'
		),
		'audio' => array(
			'label' => 'Audio'
		),
		/*'link' => array( // Deprecated
			'label' => 'Link',
			'search' => 1,
			'weight' => 9,
		),*/
		/*'theme' => array( // Added via the 'theme' plugin now
			'label' => 'Theme'
		),
		'template' => array(
			'label' => 'Template'
		),
		/*'meta_title' => array( // Added via the 'meta' plugin now
			'label' => 'Meta Title',
			'search' => 1,
			'weight' => 7,
		),
		'meta_description' => array(
			'label' => 'Meta Description',
			'search' => 1,
			'weight' => 6,
		),
		'meta_keywords' => array(
			'label' => 'Meta Keywords',
			'search' => 1,
			'weight' => 5,
		),
		'meta_image' => array(
			'label' => 'Meta Image'
		),
		/*'permissions' => array( // Deprecated
			'label' => 'Permissions'
		),
		'published' => array( // Deprecated
			'label' => 'Published',
			'default' => 1
		),
		'published_start' => array( // Deprecated
			'label' => 'Start Publication',
		),
		'published_end' => array( // Deprecated
			'label' => 'End Publication',
		),*/
		'order' => array(
			'label' => 'Order'
		),
		'order_parent' => array(
			'label' => 'Order - Nested - Parent ID'
		),
		'order_level' => array(
			'label' => 'Order - Nested - Level'
		),
		/*'order_string' => array( // Deprecated, can't think of a need for this
			'label' => 'Order - Nested - String'
		),*/
		'active' => array(
			'label' => 'Active',
			'default' => 1
		),
		'visible' => array(
			'label' => 'Visible',
			'default' => 1
		),
		'created' => array(
			'label' => 'Created',
			'format' => 'unix'
		),
		'updated' => array(
			'label' => 'Updated',
			'format' => 'unix'
		),
		/* Cart specific definitions */
		'cart_name' => array(
			'label' => 'Shopping Cart - Name',
			'cart' => 1,
		),
		'cart_image' => array(
			'label' => 'Shopping Cart - Image',
			'cart' => 1,
		),
		'cart_price' => array(
			'label' => 'Shopping Cart - Price',
			'cart' => 1,
		),
		'cart_quantity_minimum' => array(
			'label' => 'Shopping Cart - Quantity - Minimum',
			'cart' => 1,
		),
		'cart_quantity_maximum' => array(
			'label' => 'Shopping Cart - Quantity - Maximum',
			'cart' => 1,
		),
		'cart_quantity_unit_single' => array(
			'label' => 'Shopping Cart - Quantity - Unit - Singular',
			'cart' => 1,
		),
		'cart_quantity_unit_plural' => array(
			'label' => 'Shopping Cart - Quantity - Unit - Plural',
			'cart' => 1,
		),
		'cart_shipping_weight_weight' => array(
			'label' => 'Shopping Cart - Shipping - Weight',
			'cart' => 1,
		),
		'cart_shipping_weight_unit' => array(
			'label' => 'Shopping Cart - Shipping - Weight Unit',
			'cart' => 1,
		),
		'cart_shipping_dimensions_width' => array(
			'label' => 'Shopping Cart - Shipping - Dimensions - Width',
			'cart' => 1,
		),
		'cart_shipping_dimensions_length' => array(
			'label' => 'Shopping Cart - Shipping - Dimensions - Length',
			'cart' => 1,
		),
		'cart_shipping_dimensions_height' => array(
			'label' => 'Shopping Cart - Shipping - Dimensions - Height',
			'cart' => 1,
		),
		'cart_shipping_dimensions_unit' => array(
			'label' => 'Shopping Cart - Shipping - Dimensions - Unit',
			'cart' => 1,
		),
		'cart_shipping_price' => array(
			'label' => 'Shopping Cart - Shipping - Cost',
			'cart' => 1,
		),
		'cart_shipping_charge' => array(
			'label' => 'Shopping Cart - Shipping - Application',
			'cart' => 1,
		),
		'cart_handling_price' => array(
			'label' => 'Shopping Cart - Handling - Cost',
			'cart' => 1,
		),
		'cart_handling_charge' => array(
			'label' => 'Shopping Cart - Handling - Application',
			'cart' => 1,
		),
	);
	
	$db_cache = 1;
}
$__GLOBAL['framework']['db'] = $db;

/************************************************************************************/
/************************************** Plugins *************************************/
/************************************************************************************/
// Speed
debug_speed('plugins','framework - dawn');

// Cached
$plugins = cache_get('framework/plugins');
// No, create
if(!$plugins) $plugins = plugins();
// Global
$__GLOBAL['plugins'] = $plugins;

// Database (save after 'plugins' created so they can dynamically add 'db' variables)
if($db_cache) {
	cache_save('framework/db',$__GLOBAL['framework']['db']);
}

/************************************************************************************/
/************************************** Modules *************************************/
/************************************************************************************/
// Speed
debug_speed('modules','framework - dawn');

// Cached
$modules = cache_get('framework/modules');
// No, create
if(!$modules) $modules = modules();
// Global
$__GLOBAL['modules'] = $modules;

/************************************************************************************/
/********************************** Functions / Dawn ********************************/
/************************************************************************************/
// Modules
foreach($modules as $module => $v) {
	// Functions
	if($v[core]) include_once SERVER.$v[path_local]."functions/core.php";
	include_once SERVER.$v[path]."functions/core.php";
	
	// Dawn
	if($v[core]) include_once SERVER.$v[path_local]."includes/dawn.php";
	include_once SERVER.$v[path]."includes/dawn.php";
}

// Plugins
foreach($plugins as $plugin => $v) {
	// Functions
	if($v[core]) include_once SERVER.$v[path_local]."functions/core.php";
	include_once SERVER.$v[path]."functions/core.php";
	
	// Dawn
	if($v[core]) include_once SERVER.$v[path_local]."includes/dawn.php";
	include_once SERVER.$v[path]."includes/dawn.php";
}

/************************************************************************************/
/*************************************** Theme **************************************/
/************************************************************************************/
// Speed
debug_speed('theme','framework - dawn');

// Theme
if(m('settings.settings.theme')) {
	$__GLOBAL['theme'] = theme(m('settings.settings.theme'));
}
?>