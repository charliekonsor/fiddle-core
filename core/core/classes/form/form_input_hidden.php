<?php
if(!class_exists('form_input_hidden',false)) {
	/**
	 * Creates a text input
	 */
	class form_input_hidden extends form_input_framework {
		/**
		 * Loads class. Needed so we can use debug_backtrace() to get this class name in PHP < 5.3 in $form_input->load().
		 *
		 * Note, only 3 params (no $label).
		 */
		static function load($name = NULL,$value = NULL,$c = NULL) {
			return parent::load(NULL,$name,$value,$c);
		}
		
		/**
		 * Construct
		 */
		function __construct($label = NULL,$name = NULL,$value = NULL,$c = NULL) {
			$this->form_input_hidden($label,$name,$value,$c);
		}
		function form_input_hidden($label = NULL,$name = NULL,$value = NULL,$c = NULL) {
			// Construct
			parent::__construct($label,$name,$value,$c);
		}
		
		/**
		 * Renders the HTML for the input's 'row'.
		 *
		 * @param object $form The form object this input is a part of.
		 * @param array $c An array of config values. Default = NULL
		 * @return string The HTML of the input's row.
		 */
		function html_row($form,$c = NULL) {
			// Return
			return $this->html_element($form,$c);
		}
	}
}
?>