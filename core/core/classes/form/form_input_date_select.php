<?php
if(!class_exists('form_input_date_select',false)) {
	/**
	 * Creates a date input, using dropdowns for the month / day / year.
	 */
	class form_input_date_select extends form_input_framework {
		/**
		 * Loads class. Needed so we can use debug_backtrace() to get this class name in PHP < 5.3 in $form_input->load().
		 */
		static function load($label = NULL,$name = NULL,$value = NULL,$c = NULL) {
			return parent::load($label,$name,$value,$c);
		}
		
		/**
		 * Construct
		 */
		function __construct($label = NULL,$name = NULL,$value = NULL,$c = NULL) {
			$this->form_input_date_select($label,$name,$value,$c);
		}
		function form_input_date_select($label = NULL,$name = NULL,$value = NULL,$c = NULL) {
			// Legacy - used to be $c[date]
			if($c[date]) {
				foreach($c[date] as $k => $v) $c[$k] = $v;
			}		
			
			// Construct
			parent::__construct($label,$name,$value,$c);
			
			// Config
			if(!$this->c[format]) $this->c[format] = "date";
			if(!$this->c[year_min]) $this->c[year_min] = 1900;
			if(!$this->c[year_max]) $this->c[year_max] = date('Y') + 100;
		}
		
		/**
		 * Creates the HTML for an input
		 *
		 * @param object $form The form object this input is a part of.
		 * @param array $c An array of config values. Default = NULL
		 * @return string The HTML element for the input.
		 */
		function html_element($form,$c = NULL) {
			// Name
			$name = str_replace(array('[',']'),'',$this->name);
			
			// Attributes
			#build this. need to strip out required or group required by all 3. id (if exists) must be appeneded with _month or _day or _year
			
			// Time
			$time = 0;
			if($this->value) $time = time_format($this->value,'unix');
			
			// Month
			$value = NULL;
			if($time) $value = adodb_date('n',$time);
			$html .= "
		<select name='".$name."_month'>
			<option value=''>Month</option>";
			for($x = 1;$x <= 12;$x++) $html .= "
			<option value='".$x."'".($time && $value == $x ? " selected='selected'" : '').">".date('F',mktime(0,0,0,$x,1,2000))."</option>";
			$html .= "
		</select>";
		
			// Day
			$value = NULL;
			if($time) $value = adodb_date('j',$time);
			$html .= "
		<select name='".$name."_day'>
			<option value=''>Day</option>";
			for($x = 1;$x <= 31;$x++) $html .= "
			<option value='".$x."'".($time && $value == $x ? " selected='selected'" : '').">".$x."</option>";
			$html .= "
		</select>";
		
			// Year
			$value = NULL;
			if($time) $value = adodb_date('Y',$time);
			$html .= "
		<select name='".$name."_year'".($this->validate[required] ? " class='required'" : "").">
			<option value=''>Year</option>";
			for($x = $this->c[year_min];$x <= $this->c[year_max];$x++) $html .= "
			<option value='".$x."'".($time && $value == $x ? " selected='selected'" : '').">".$x."</option>";
			$html .= "
		</select>";
		
			$html .= "
		<label class='error form-error' for='".$this->name."_year'".($this->c[errors_inline] && $this->name && $this->errors[inline][$this->name] ? " style='display:inline;'>".$this->errors[inline][$this->name] : ">Please select a date.")."</label>";
			
			// Return
			return $html;
		}
			
		/**
		 * Processes the value for this input.
		 *
		 * @param object $form The form object which contains the submitted values ($form->post and $form->files).
		 * @param array $c An array of configuration values. Default = NULL
		 * @return mixed The resulting value.
		 */
		function process($form,$c = NULL) {
			// Value
			$value = parent::process($form,$c);
			
			// Name (used for month, day, and year values)
			$name = str_replace(array('[',']'),'',$this->name);
			
			// Values
			$month = $form->post[$name.'_month'];
			$day = $form->post[$name.'_day'];
			$year = $form->post[$name.'_year'];
			
			// Value (unix)
			$value = adodb_mktime(0,0,0,$month,$day,$year);
			
			// Value (formatted)
			if($value) $value = time_format($value,$this->c[format]);
			else $value = NULL;
			
			// Return
			return $value;
		}
	}
}
?>