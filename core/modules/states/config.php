<?php
/************************************************************************************/
/************************************** Config **************************************/
/************************************************************************************/
$config = array(
	'name' => 'States',
	'single' => 'State',
	'plural' => 'States',
	'icon' => 'states',
	'db' => array(
		'table' => 'states',
		'id' => 'state_id',
		'code' => 'state_code',
		'name' => 'state_name',
	),
	'permissions' => array(),
	'admin' => array(
		'active' => 0,
	),
	'account' => array(
		'active' => 0,
	),
	'public' => array(
		'active' => 0,
	),
	'settings' => array(),
	'install' => array(
		'db' => array(
			'rows' => 1,
		),
	),
);
?>