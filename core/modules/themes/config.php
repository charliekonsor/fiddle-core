<?php
/************************************************************************************/
/************************************** Config **************************************/
/************************************************************************************/
$config = array(
	'name' => 'Themes',
	'single' => 'Theme',
	'plural' => 'Themes',
	'icon' => 'themes',
	'db' => array(
		'table' => 'themes',
		'id' => 'theme_id',
		'code' => 'theme_code',
		'name' => 'theme_name'
	),
	'permissions' => array(
		'change' => array(
			'label' => 'Change',
			'manage' => 1,
			'default' => array(
				'admins',
			)
		),
		'edit' => array(
			'label' => 'Settings',
			'manage' => 1,
			'default' => array(
				'admins',
			)
		),
		/*'settings' => array( // This is the module's settings, not a theme's settings (the 'edit' permission above).  Remove for now since no settings applied to it.
			'label' => 'Settings',
			'default' => array(
				'admins',
			)
		),*/
	),
	'admin' => array(
		'section' => 'system',
		'views' => array(
			'home' => 'Themes',
			'item' => 'Theme Settings'
		),
	),
	'public' => array(
		'active' => 0
	)
);
?>