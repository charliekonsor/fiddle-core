<?php
/************************************************************************************/
/*************************************** Dawn ***************************************/
/************************************************************************************/
/** Dawn - loads core functionality */
require_once "includes/dawn.php";

// Errors
$errors = array(
	400 => array(
		'title' => 'Bad Request',
		'text' => "I'm sorry, but your request could not be understood by the server due to malformed syntax.",
	),
	401 => array(
		'title' => 'Unauthorized',
		'text' => "I'm sorry, but you are not authorized to view this.",
	),
	402 => array(
		'title' => 'Payment Required',
		'text' => "Payment Required.",
	),
	403 => array(
		'title' => 'Forbidden',
		'text' => "I'm sorry, but the page you are trying to view is forbidden.",
	),
	404 => array(
		'title' => 'Not Found',
		'text' => "I'm sorry, the page you were looking for could not be found.",
	),
	500 => array(
		'title' => 'Internal Server Error',
		'text' => "An internal server error occured.  We apologize for this and will try to fix it as soon as we can."
	),
);

// Redirect to home page // Have a lot of trouble with for some reason, 404 gets called if missing image, etc.
/*if(in_array($_GET['error'],array(404))) {
	//q("INSERT INTO test SET test = '2 - missing page = ".URL.", sef 0 = ".$page->sef[0].", user = ".u('id').", url 1 = ".$_SESSION['urls'][1].", url 2 = ".$_SESSION['urls'][2].", url 3 = ".$_SESSION['urls'][3]."'");
	page_error($errors[$_GET['error']],1);
	redirect(DOMAIN);
}
// Default
else {*/
	$error = "
<div class='html-error html-error-".$_GET['error']."'>
	<h1 class='html-error-heading'>".$errors[$_GET['error']][title]."</h1>
	<div class='html-error-text'>
		".$errors[$_GET['error']][text]."
	</div>
</div>";
	print $page->html(array('error' => $error));
//}

/************************************************************************************/
/*************************************** Dusk ***************************************/
/************************************************************************************/
/** Dusk - unloads core functionality. */
require_once "includes/dusk.php";
?>