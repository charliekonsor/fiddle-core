<?php
if(!class_exists('form_input_file_framework',false)) {
	class form_input_file_framework extends form_input_file {
		/**
		 * Loads class. Needed so we can use debug_backtrace() to get this class name in PHP < 5.3 in $form_input->load().
		 */
		static function load($label = NULL,$name = NULL,$value = NULL,$c = NULL) {
			return parent::load($label,$name,$value,$c);
		}
		
		/**
		 * Construct
		 */
		function __construct($label = NULL,$name = NULL,$value = NULL,$c = NULL) {
			$this->form_input_file_framework($label,$name,$value,$c);
		}
		function form_input_file_framework($label = NULL,$name = NULL,$value = NULL,$c = NULL) {
			// Construct
			parent::__construct($label,$name,$value,$c);
			
			// Config
			if(!$this->c[type]) $this->c[type] = "file";
			
			// Validate
			if(!x($this->validate[accept])) $this->validate[accept] = implode(",",g('config.uploads.types.'.$this->c[type].'.accept'));
		}
		
		/**
		 * Renders the HTML for the input's 'row'.
		 *
		 * @param object $form The form object this input is a part of.
		 * @param array $c An array of config values. Default = NULL
		 * @return string The HTML of the input's row.
		 */
		function html_row($form,$c = NULL) {
			// Config - need to do here as we need to know $form variables ($form->module, $form->plugin, etc.)
			if($form->module) {
				if(!$this->c[path]) $this->c[path] = m($form->module.'.uploads.types.'.$this->c[type].'.path');
				if(!x($this->c[hash])) $this->c[hash] = m($form->module.'.uploads.types.'.$this->c[type].'.hash');
				if(!$this->c[storage]) $this->c[storage] = m($form->module.'.uploads.types.'.$this->c[type].'.storage');
				if(!x($this->c[thumbs])) $this->c[thumbs] = m($form->module.'.uploads.types.'.$this->c[type].'.thumbs');
				else if(is_array($this->c[thumbs])) $this->c[thumbs] = array_merge_associative(m($form->module.'.uploads.types.'.$this->c[type].'.thumbs'),$this->c[thumbs]);
				if(!x($this->c[extensions])) $this->c[extensions] = m($form->module.'.uploads.types.'.$this->c[type].'.extensions');
			}
			else if($form->plugin) {
				if(!$this->c[path]) $this->c[path] = plugin($form->plugin.'.uploads.types.'.$this->c[type].'.path');
				if(!x($this->c[hash])) $this->c[hash] = plugin($form->plugin.'.uploads.types.'.$this->c[type].'.hash');
				if(!$this->c[storage]) $this->c[storage] = plugin($form->plugin.'.uploads.types.'.$this->c[type].'.storage');
				if(!x($this->c[thumbs])) $this->c[thumbs] = plugin($form->plugin.'.uploads.types.'.$this->c[type].'.thumbs');
				else if(is_array($this->c[thumbs])) $this->c[thumbs] = array_merge_associative(plugin($form->plugin.'.uploads.types.'.$this->c[type].'.thumbs'),$this->c[thumbs]);
				if(!x($this->c[extensions])) $this->c[extensions] = plugin($form->plugin.'.uploads.types.'.$this->c[type].'.extensions');
			}
			else {
				if(!$this->c[path]) $this->c[path] = g('config.uploads.types.'.$this->c[type].'.path');
				if(!x($this->c[hash])) $this->c[hash] = g('config.uploads.types.'.$this->c[type].'.hash');
				if(!$this->c[storage]) $this->c[storage] = g('config.uploads.types.'.$this->c[type].'.storage');
				if(!x($this->c[thumbs])) $this->c[thumbs] = g('config.uploads.types.'.$this->c[type].'.thumbs');
				else if(is_array($this->c[thumbs])) $this->c[thumbs] = array_merge_associative(g('config.uploads.types.'.$this->c[type].'.thumbs'),$this->c[thumbs]);
				if(!x($this->c[extensions])) $this->c[extensions] = g('config.uploads.types.'.$this->c[type].'.extensions');
			}
			// Config - browse - depends upon $this->c[path] so wait until here to check fo rit
			if(!x($this->c[browse][active])) $this->c[browse][active] = (u('admin') ? 1 : 0);
			if(!class_exists('file',false)) $this->c[browse][active] = 0;
			if($this->c[storage] and $this->c[storage] != "local") $this->c[browse][active] = 0;
			if($this->c[browse][active] == 1) {
				if(!$this->c[browse][path]) $this->c[browse][path] = $this->c[path];
				if(!$this->c[browse][type]) $this->c[browse][type] = $this->c[type];
				if(!$this->row[attributes][id]) $this->row[attributes][id] = "file-row-".random();
				if(!$this->c[browse][row]) $this->c[browse][row] = "#".$this->row[attributes][id];
				if(!$this->c[browse][preview_path] and strstr($this->c[browse][path],'/o/') and $this->c[thumbs][s]) $this->c[browse][preview_path] = str_replace('/o/','/s/',$this->c[browse][path]); // Use 'thumb' instead of original when displaying 'preview' (for faster loading)
			}
			
			// Parent
			$html = parent::html_row($form,$c);
			
			// Return
			return $html;
		}
		
		/**
		 * Renders the HTML for the input's 'field'.
		 *
		 * @param object $form The form object this input is a part of.
		 * @param array $c An array of config values. Default = NULL
		 * @return string The HTML of the input's field.
		 */
		function html_field($form,$c = NULL) {
			// International
			$this->plugin_international($form);
			
			// Input
			$html .= "
				".$this->html_element($form,$c);
					
			// Prepend
			if($this->prepend) $html = $this->prepend.$html;
			
			// Append
			if($this->append) $html .= $this->append;
			
			// Browse - icon
			if($browse = $this->browse($form,$c)) $html .= $browse;
		
			// Help
			if($this->help) $html .= help($this->help);
				
			// Error
			if($this->error) $html .= "
				<label class='error form-error' for='".$this->name."' style='display:inline;'>".$this->error."</label>";
			
			// Browse - start container
			if($browse) $html .= "
				<div class='form-browse-preview'>";
					
			// Preview
			if($this->c[preview] and $this->value and $this->c[path]) {
				$html .= $this->preview($form,$c);
			}
				
			// Browse - end container
			if($browse) $html .= "</div>";
			
			// Return
			return $html;
		}
			
		/**
		 * Processes the value for this input.
		 *
		 * @param object $form The form object which contains the submitted values ($form->post and $form->files).
		 * @param array $c An array of configuration values. Default = NULL
		 * @return mixed The resulting value.
		 */
		function process($form,$c = NULL) {
			// Parent
			$value = parent::process($form,$c);
			
			if($this->process and $this->name and $this->c[path]) {
				// Have a file..
				if($value) {
					// Values - from form inputs (ex: file_thumb)
					$array = $form->post[__plugins][files][$this->name];
					
					// Values - new file
					if($form->values_files[$this->name][name]) {
						$array[file_type] = $form->values_files[$this->name][type];
						$array[file_path] = $form->values_files[$this->name][path];
						$array[file_extension] = $form->values_files[$this->name][extension];
						$array[file_width] = $form->values_files[$this->name][width];
						$array[file_height] = $form->values_files[$this->name][height];
						$array[file_length] = $form->values_files[$this->name][length];
						$array[file_size] = $form->values_files[$this->name][size];
						$array[file_extensions] = $form->values_files[$this->name][extensions];
						$array[file_thumbs] = $form->values_files[$this->name][thumbs];
						$array[file_storage] = $form->values_files[$this->name][storage];
					}
						
					// Values - thumb - if none selected, use first thumb
					if(!$array[file_thumb]) {
						if($thumbs = $form->values_files[$this->name][thumbs]) {
							foreach($thumbs as $k => $v) {
								$array[file_thumb] = array_first_key($v);
								break;
							}
						}
					}
					
					// Values - basic - only needed if we have some values to save, if not, we simply don't save it at all
					if(x($array)) {
						$array[file_column] = $this->name;
						$array[file_name] = $value;
					}
					// Nothing's changed, don't save
					else {
						unset($array);
					}
				}
				// No file, delete plugin row entry
				else $array = array();
				
				// Value
				if(substr($this->name,0,9) == "__plugins") {
					// Make sure file_column doesn't dontain __plugins, aka __plugins[menu][item_image] becomes item_image
					if($array[file_column]) {
						$array[file_column] = preg_replace('/__plugins\[(.*?)\]\[(.*?)\](\[?)/','$2$3',$array[file_column]);
					}
				
					// Want to change __plugins[files][__plugins[menu][item_image]] to __plugins[menu][__plugins][files][item_image] format // But only if we have a value to set. If we don't, the $form->value() calle will return the value (NULL) and all old values will be lost
					if(x($array)) {
						$name = preg_replace('/__plugins(.+?)\[(.*?)\]$/','__plugins$1[__plugins][files][$2]',$this->name);
						debug("file name string: ".$name,$c[debug]);
						$form->values = $form->value($form->values,$name,$array);
					}
				}
				else if(x($array)) $form->values[__plugins][files][$this->name] = $array;
			}
			
			// Return
			return $value;
		}
		
		/**
		 * Deletes previous file after new one is uploaded.
		 *
		 * @param object $form The form object this input is a part of.
		 * @param array $c An array of config values. Default = NULL
		 */
		function delete($form,$c = NULL) {
			// Delete by plugin
			if($form->module and $form->id > 0 and m($form->module.'.plugins.files') and $this->name) {
				// Plugin
				$plugin = plugin::load($form->module,$form->id,'files');
				// Plugin item row
				$plugin_item_row = $plugin->db->f($plugin->rows(array('where' => "file_column = '".$this->name."'")));
				// Plugin item
				$plugin_item = $plugin->plugin_item($plugin_item_row);
				if($plugin_item->id) {
					// Delete // This already happens when we save the item: $item->save() -> $plugin->item_delete() -> $plugin_item->delete()
					//$plugin_item->delete(array('debug' => $this->c[debug]));	
					debug("Not deleting ".$plugin_item->v('file_path').$plugin_item->v('file_name').", it'll be deleted when we save the results for this item",$this->c[debug]);
					
					// Return - deleting will happen when we save the item
					return;
				}
			}
			
			// Parent
			parent::delete($c);
		}
		
		/**
		 * Creates and returns the HTML for previewing a previously uploaded file.
		 *
		 * @param object $form The form object this input is a part of.
		 * @param array $c An array of config values. Default = NULL
		 * @return string The HTML of the file preview.
		 */
		function preview($form,$c = NULL) {
			// Value
			if($this->value and $this->c[path] or $c[browse]) {
				// Config
				if(!x($c[delete])) $c[delete] = 1; // Show delete icon for removing the file
				if(!x($c[crop])) $c[crop] = 1; // Show crop icon for cropping the file (images only)
				if(!x($c[rotate])) $c[rotate] = 1; // Show rotate icons for rotating the file left/right (images only)
				if(!x($c[browse])) $c[browse] = NULL; // Full path to file we selected via file browser
				
				// Random
				$r = "preview".random();
			
				// File - browse
				if($c[browse]) {
					$storage = "local";
					$file = $c[browse];
					$this->value = file::name($file);
				}
				// File - saved
				else {
					// File - files plugin
					if($form->item->id) {
						$f = $form->item->file($this->name);
						$file = $f->file['class']->file;
						$storage = $f->file['class']->storage;
					}
					// File - default
					if(!$file) {
						$storage = $this->c[storage];
						$file = $this->c[path].$this->value;
					}
				}
			
				// File class
				if(class_exists('file')) {
					$f = file::load($file,array('storage' => $storage));
					$type = $f->type;
				}
	
				// Container
				$text .= "
<div id='".$r."' class='form-preview".($type ? " form-preview-".$type : "")."'>";

				// Text
				$text .= "
	<div class='form-preview-keep'>Leave this field blank to keep the existing file.</div>
	<div class='clear'></div>";
	
				// Display
				$text .= "
	<div class='form-preview-media'>";
			
				// Display - advanced
				if($f) {
					// Image
					if($type == "image") {
						// Thumb
						$file_thumb = NULL;
						
						// Via file browser
						if($c[browse]) {
							if($this->c[thumbs][s][path]) {
								$file_thumb = file::load($this->c[thumbs][s][path].$this->value,array('storage' => $this->c[thumbs][s][storage]));
							}
						}
						// Saved file
						else if($this->c[thumbs]) {
							$file_thumb = $form->item->file($this->name,array('thumb' => 's'));
						}
						
						// Thumb
						if($file_thumb->exists) {
							$url = $f->url();
							$text .= "
		".$file_thumb->image_html(array('attributes' => " target='_blank'",'link' => $url,'link_attributes' => " class='overlay'"));
						}
						// No thumb, use original (shrunk)
						else {
							$text .= "
		".$f->image_html(array('max_width' => 100,'max_height' => 100,'attributes' => " target='_blank'",'link' => $f->url(),'link_attributes' => " class='overlay'"));
						}
					}
					// Video
					if($type == "video") {
						// Via file browser
						if($c[browse]) {
							// Config
							$video_c = array(
								'max_width' => 265,
								'auto' => 0,
							);
							// Extensions
							if($this->c[extensions]) {
								foreach($this->c[extensions] as $extension => $v) {
									$f_extension = file::load($v[path].str_replace(".".$f->extension(),".".$extension,$this->value));
									if($f_extension->exists) {
										// Same as original, don't want 2, don't add as fallback
										if($f_extension->extension == $f->extension) {
											$f = $f_extension; // Use converted video if one exists as it'll have all necessary tweaks/headers
										}
										// Different than original, add as fallback
										else {
											$video_c[fallback][] = $f_extension->file;	
										}
									}
								}
							}
							// Video
							if($f) $file_video_html = $f->video_html($video_c);
							if($file_video_html) {
								$text .= "
		".$file_video_html;
		
								// Thumbs
								$query = "SELECT * FROM items_files WHERE file_path = '".a($this->c[path])."' AND file_name = '".a($this->value)."' AND file_thumbs != '' LIMIT 1";
								$row = $form->db->f($query);
								$thumbs = data_unserialize($row[file_thumbs]);
								$thumbs = $thumbs[t];
								if($thumbs) {
									$thumbs_count = count($thumbs);
									$thumb_selected = $row[file_thumb];
									$text .= "
		<table class='form-preview-video-thumbs'>
			<tr>";
									foreach($thumbs as $k => $v) {
										$file_video_thumb = file::load($v[path].$v[name],array('storage' => $v[storage]));
										if($file_video_thumb->exists) $text .= "
				<td width='".round(100 / $thumbs_count)."' align='center' valign='top'>
					<img src='".$file_video_thumb->url()."' />
					<input type='radio' name='__plugins[files][".string_encode($this->name)."][file_thumb]' value='".$k."'".($thumb_selected == $k ? " checked='checked'" : "")." />
				</td>";
									}
									$text .= "
			</tr>
		</table>";
								}
							}
							else $type = "file";
						}
						// Saved file
						else {
							// Video
							$file_video = $form->item->file($this->name,array('extension' => array('mp4','flv')));
							if($file_video) $file_video_html = $file_video->video_html(array('max_width' => 265,'width' => $file_video->width(),'height' => $file_video->height(),'poster' => $file_video->thumb_url(),'auto' => 0));
							if($file_video_html) {
								$text .= "
		".$file_video_html;
		
								// Thumbs
								if($thumbs = $file_video->thumbs('t')) {
									$thumbs_count = count($thumbs);
									$thumb_selected = $file_video->thumb('t');
									$text .= "
		<table class='form-preview-video-thumbs'>
			<tr>";
									foreach($thumbs as $k => $v) {
										$file_video_thumb = file::load($v[path].$v[name],array('storage' => $v[storage]));
										if($file_video_thumb->exists) $text .= "
				<td width='".round(100 / $thumbs_count)."' align='center' valign='top'>
					<img src='".$file_video_thumb->url()."' />
					<input type='radio' name='__plugins[files][".string_encode($this->name)."][file_thumb]' value='".$k."'".($thumb_selected[key] == $k ? " checked='checked'" : "")." />
				</td>";
									}
									$text .= "
			</tr>
		</table>";
								}
							}
							else $type = "file";
						}
					}
					// Audio
					if($type == "audio") $text .= "
		".$f->audio_html();
					// File
					if($type == "file") $text .= "
		<a href='".$f->url()."' target='_blank'>".basename($file)."</a>";
				}
				// Display - simple
				else {
					// URL
					$url = str_replace(SERVER,DOMAIN,$file);
					// File
					if($url) $text .= "
		<a href='".$url."' target='_blank'>".basename($file)."</a>";
					else $text .= basename($file);
				}
				
				$text .= "
	</div>";
	
				// Icons
				$text .= "
	<div class='form-preview-icons'>";
	
				// Delete
				if($c[delete]) {
					// Deleted input name
					$name = $this->name;
					$name = preg_replace('/\[/','][',$name,1); // Add first closing bracket bracket. Example: __plugins[menu][item_image] becomes __plugins][menu][item_image]
					$name = "__file_deleted[".$name;
					if(substr($name,-1) != "]") $name .= "]";
					
					// Required element selector
					if($this->attributes[id])  $selector = "#".$this->attributes[id].".required-input";
					else $selector = ".required-input:input[name=".$this->name."]"; // !!! This needs to be updated to work with a multilevel input names
					
					$text .= "
		<script type='text/javascript'>
		function deleteFile".$r."() {
			if(confirm('Are you sure you want to delete this file?')) {
				$('#".$r."').html(\"<input type='hidden' name='".$name."' value='1' />\");
				$('".$selector."').addClass('required'); // Re-require
			}
		}
		</script>
		<a href='javascript:void(0);' onclick='deleteFile".$r."();' class='i i-clear i-inline tooltip core-hover' title='Delete'></a>";
				}
			
				/*if($type == "image") {
					// Image - Crop
					if($c[crop] == 1 and g('config.javascript.admin.jquery\.jcrop\.js')) $text .= "
		<a href='".D."?ajax_action=cropIt&amp;module=".$module."&amp;id=".$id."&amp;file=".urlencode($url)."&amp;div=".$r."' class='i i-crop i-inline tooltip overlay' title='Crop'></a>";
	
					// Image - Rotate
					if($c[rotate] == 1) {
					$oncomplete = "function(image){\$('#".$module."-".$id."-image-".$f."').closest('a').attr('href',str_replace('/".$c[size]."/','/".$c[size_large]."/',image));}";
					$text .= "
		<a href='javascript:void(0);' onclick=\"rotate('".$module."-".$id."-image-".$f."',270,{vars:'module=".$module."&id=".$id."&size=".$c[size]."','loading':1,oncomplete:".$oncomplete."});\" class='i i-rotate-left i-inline tip' title='Rotate Left'></a>
		<a href='javascript:void(0);' onclick=\"rotate('".$module."-".$id."-image-".$f."',90,{vars:'module=".$module."&id=".$id."&size=".$c[size]."','loading':1,oncomplete:".$oncomplete."});\" class='i i-rotate-right i-inline tip' title='Rotate Right'></a>";
					}
				}*/
			
				$text .= "
	</div>
	<div class='clear'></div>";

				// AJAX
				if($c[browse]) $text .= "
	<input type='hidden' name='".$this->name."' value='".string_encode($c[browse])."' />";
	
				$text .= "
</div>";
				
				// Return
				return $text;
			}
		}
		
		/**
		 * Returns HTML for displaying icon to browse previously uploaded files via the file browser.
		 *
		 * @param object $form The form object this input is a part of.
		 * @param array $c An array of config values. Default = NULL
		 * @return string The HTML for the browse icon.
		 */
		function browse($form,$c = NULL) {// Browse
			// Active
			if($this->c[browse][active] and $this->c[browse][path] and $this->c[browse][row]) {
				// Key
				$key = microtime_float()."-".random();
				
				// Store
				cache_save("forms/inputs/".$key,$this,array('force' => 1));
				
				// Icon
				$icon = "<a href='".D."?ajax_action=file_browse&amp;input=".$key."' class='overlay i i-search i-inline' title='File Browser'></a>";
			}
			
			// Return
			return $icon;	
		}
	}
}
?>