<?php
$server = str_replace('core/core/libraries/hybridauth/config.php','',realpath(__FILE__));
require_once $server."core/core/includes/dawn.php"; // Must be 'require_once'

global $__GLOBAL;
$__GLOBAL['hybridauth']['keys'] = array(
	'openid' => 'OpenID',
	'aol' => 'AOL',
	'yahoo' => 'Yahoo',
	'google' => 'Google',
	'facebook' => 'Facebook',
	'twitter' => 'Twitter',
	'live' => 'Live',
	'myspace' => 'MySpace',
	'linkedin' => 'LinkedIn',
	'foursquare' => 'Foursquare',
	'instagram' => 'Instagram',
);

if(!function_exists('hybridauth_provider')) {
	/**
	 * Returns the HybridAuth provider key given the key we use for the same provider.
	 * 
	 * @param string $provider The key of the provider as we define it in the code.
	 * @return string string The key used by HybridAuth for the same provider.
	 */
	function hybridauth_provider($provider) {
		$keys = g('hybridauth.keys');
		return $keys[$provider];
	}
}

if(!function_exists('hybridauth_provider_code')) {
	/**
	 * Returns the internal code for the given HybridAuth provider key.
	 * 
	 * @param string $provider The HybridAuth provider key you want to get the internal code of.
	 * @return string string The internal ocode that corresponds to the given HybridAuth provider key.
	 */
	function hybridauth_provider_code($provider) {
		$keys = g('hybridauth.keys');
		return array_search($provider,$keys);
	}
}

/*!
* HybridAuth
* http://hybridauth.sourceforge.net | http://github.com/hybridauth/hybridauth
* (c) 2009-2012, HybridAuth authors | http://hybridauth.sourceforge.net/licenses.html
*/

// ----------------------------------------------------------------------------------------
//	HybridAuth Config file: http://hybridauth.sourceforge.net/userguide/Configuration.html
// ----------------------------------------------------------------------------------------

// Config
$config = array(
	"base_url" => DOMAIN."core/core/libraries/hybridauth/", // MUST HAVE TRAILING SLASH, IF YOU DON'T, FACEBOOK'S CODE (base_facebook.php?) ADDS IT IN AND YOU GET A MISMATCH WITH URLS WHEN VERIFYING ACCESS CODE
	// if you want to enable logging, set 'debug_mode' to true  then provide a writable file by the web server on "debug_file"
	"debug_mode" => true,
	"debug_file" => SERVER."local/cache/hybridauth/debug.txt",
);
if(g('config.social')) {
	include_function('hybridauth');
	foreach(g('config.social') as $k => $v) {
		if(count(array_filter($v[app])) and $key = hybridauth_provider($k)) {
			/*$config[providers][$key] = array(
				"enabled" => ($v[active] ? true : false),
				"keys" => $v[app]
			);*/
			// This will handle other config options too (scope in Facebook for example)
			foreach($v as $_k => $_v) {
				if($_k == "active") {
					$_k= "enabled";
					$_v = ($_v ? true : false);	
				}
				if($_k == "app") $_k = "keys";
				$config[providers][$key][$_k] = $_v;
			}
		}
	}
}
if($config[debug_mode] == true and !file_exists($config[debug_file])) {
	cache_save(str_replace(array(SERVER."local/cache/",".txt"),"",$config[debug_file]),"CREATED");
}

// Return
return $config;