<?php
if(!class_exists('item_home',false)) {
	/**
	 * Extends the item class, providing scripts specific to the 'home' module.
	 *
	 * @package kraken\modules\home
	 */
	class item_home extends item {
		/**
		 * construct
		 */
		function __construct($module,$id = NULL,$c = NULL) {
			$this->item_home($module,$id,$c);
		}
		function item_home($module,$id = NULL,$c = NULL) {
			// ID
			$id = 1;
			
			// Parent
			parent::__construct($module,$id,$c);
			
			// Doesn't exist, add
			if(!$this->v(m($this->module.'.db.id'))) {
				$values = array(
					'db.id' => 1,
					'db.name' => 'Home'
				);	
				$this->save($values);
			}
		}
	}
}
?>