<?php
if(!class_exists('plugin_comments',false)) {
	/**
	 * An extenion of the plugin class which handles functionality specific to the comments plugin.
	 *
	 * @package kraken\plugins\comments
	 */
	class plugin_comments extends plugin {
		/**
		 * Constructs the class.
		 *
		 * @param string $module The module of the item the plugin is acting upon.
		 * @param int $id The id of the item the plugin is acting upon. Default = NULL
		 * @param string $plugin The plugin we're using.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function __construct($module,$id = NULL,$plugin,$c = NULL) {
			$this->plugin_comments($module,$id,$plugin,$c);
		}
		function plugin_comments($module,$id = NULL,$plugin,$c = NULL) {
			parent::__construct($module,$id,$plugin,$c);
		}
		
		/**
		 * Returns the form class object for adding an item in this plugin.
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 * @return object The form class object with fields pre-added in.
		 */
		function form($c = NULL) {
			// Error
			if(!$this->module or !$this->id or !$this->plugin) return;

			// Config
			if(!$c[container]) $c[container] = "#comments-container-".$this->module."-".$this->id."-".mt_rand(); // The jquery selector for the element that contains the comments (where we'll post the resulting comment after it's saved).
			if(!$c[direction]) $c[direction] = "d"; // Which direction do you want to load the comments: a (ascending, oldest first) or d (descending, newest first) [default]
			$c[direction] = strtolower(substr($c[direction],0,1));

			// Form - config
			$plugin_form = plugin($this->plugin.'.forms.add');
			$form_c = array_merge($plugin_form,$c);
			if(!$form_c[id]) $form_c[id] = "comments-form-".mt_rand();
			$form_c['class'] .= ($form_c['class'] ? " " : "")."comments-form";
			$form_c[submit] = 0;
			$form_c[redirect] = NULL;
			unset($form_c[inputs],$form_c[container]);
			
			// Form - config - action
			if(!$form_c[action]) {
				$on_success = "
function(result,form,c) {";
				if($c[direction] == "a") $on_success .= "
	$('".$c[container]."').append(result);
	$('.jquery-slideDown').removeClass('.jquery-slideDown').show();
	$('".$c[container]."').scrollTop($('".$c[container]."')[0].scrollHeight);";
				else $on_success .= "
	$('".$c[container]."').prepend(result);
	$('".$c[container]."').scrollTop(0);
	$('.jquery-slideDown').removeClass('.jquery-slideDown').slideDown();";
				$on_success .= "
	$('".$c[container]." .core-none').remove();";
				if($plugin_form[inputs]) {
					foreach($plugin_form[inputs] as $v) {
						if($v[name]) $on_success .= "
	$('#".$form_c[id]." :input[name=".$v[name]."]').val('');";
					}
				}
				$on_success .= "
}";
				$form_c[action] = "javascript:save('".$form_c[id]."',{on_success:".string_strip_breaks($on_success)."});";
			}
			
			// Form
			$form = form::load($form_c);
			
			// Inputs
			if($plugin_form[inputs]) {
				// Inputs - plugin
				$form->inputs($plugin_form[inputs]);
				
				// Inputs - default
				$form->submit('Post');
				$form->hidden('ajax_action','plugin_save',array('process' => 0));
				$form->hidden('ajax_plugin',$this->plugin,array('process' => 0));
				$form->hidden('ajax_module',$this->module,array('process' => 0));
				$form->hidden('ajax_id',$this->id,array('process' => 0));
				$form->hidden('plugin',$this->plugin,array('process' => 0));
				if($name = plugin($this->plugin.'.db.parent_module')) $form->hidden($name,$this->module);
				if($name = plugin($this->plugin.'.db.parent_id')) $form->hidden($name,$this->id);
				if($name = plugin($this->plugin.'.db.user')) $form->hidden($name,u('id'));
			}
			
			// Return
			return $form;
		}
		
		/**
		 * Returns HTML for displaying comments.
		 *
		 * !!! DEPRECATED - We use the 'items' view for this now !!!
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 * @return string The HTML for displaying the comments.
		 */
		/*function comments_html($c = NULL) {
			// Error
			if(!$this->module or !$this->id or !$this->plugin) return;
			
			// Config
			if(!x($c[form])) $c[form] = 1; // Include comment form
			if(!$c[page]) $c[page] = ($_GET['p'] ? $_GET['p'] : 1); // Page number
			if(!$c[perpage]) $c[perpage] = 10; // How many comments to show per page
			if(!x($c[query_c]['db.visible'])) $c[query_c]['db.visible'] = 1;
			if(!x($c[query_c][order])) $c[query_c][order] = plugin($this->plugin.'.db.created')." DESC";
			
			// Form
			if($c[form]) {
				$html .= "
<div class='comments-form-container'>";
				if($this->permission('add')) $html .= $this->form();
				else $html .= "
	<div class='core-none comments-form-login'>
		".(u('id') ? "You don't have permission to add ".strtolower(plugin($this->plugin.'.single'))."." : "You must be logged in to do this.")."
		".$this->login_form()."
	</div>";
			}
			
			// Pagination
			$rows_count = $this->rows_count($c[query_c]);
			$c[query_c][limit] = pagination_limit($rows_count,$c[perpage],$c);
	
			// Results
			$html .= "
<div  class='comments-container'>";
			$rows = $this->rows($c[query_c]);
			if($this->db->n($rows)) {
				while($row = $this->db->f($rows)) {
					$item = $this->plugin_item($row);
					$html .= "
	".$item->view('simple');
				}
			}
			else $html .= "
	<div class='core-none'>No ".plugin($this->plugin.'.single')." Yet</div>";
			$html .= "
</div>";
			
			// Pages
			if($rows_count > $c[perpage]) $html .= "
<div class='comments-pages'>
	".pagination_html($rows_count,$c[perpage])."
</div>";
			
			// Return
			return $html;
		}
		
		/**
		 * Returns HTML for displaying a single comment.
		 *
		 * !!! DEPRECATED - We use the 'item' view for this now !!!
		 *
		 * @param int $id The id of the comment you want to display.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return string The HTML for displaying the comments.
		 */
		/*function comments_item_html($id,$c = NULL) {
			// Error
			if(!$this->module or !$this->id or !$this->plugin or !$id) return;
			
			// Item
			$item = $this->plugin_item($id);
			// User
			$user = item::load('users',$item->db('user'));
			$user_url = $user->url();
			
			// HTML
			$html .= "
	<div class='comments-comment'>
		<div class='comments-comment-user'>".($user_url ? "<a href='".$user_url."'>" : "").$user->name.($user_url ? "</a>" : "")."</div>
		<div class='comments-comment-datetime'>
			<span class='comments-comment-time'>Posted at ".date('g:i A',$item->created)."</span>
			<span class='comments-comment-date'>on ".date('F j, Y',$item->created)."</span>
		</div>
		<div class='comments-comment-text'>
			".string_breaks($item->v('comment_text'))."
		</div>
	</div>";
			
			// Return
			return $html;	
		}*/
			
		/**
		 * Determines and saves the number of comments the parent item has.
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 * @return int The number of comments the parent item has.
		 */
		function count($c = NULL) {
			// Error
			if(!$this->module or !$this->id or !$this->plugin) return;
			
			// Default
			$count = 0;
			
			// Have comment count field
			if(m($this->module.'.db.comments_count') and $this->item) {
				// Count
				$count = $this->rows_count();
				
				// Save
				$this->item->save(array(m($this->module.'.db.comments_count') => $count));
			}
			
			// Return 
			return $count;
		}
	}
}
?>