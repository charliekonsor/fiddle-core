<?php
// Permission
if($this->permission('edit',1)) {
	// Heading
	print $this->heading();
	// Buttons
	print $this->heading_buttons();
	
	// Form
	$form = $this->item_class->form(array('type' => $this->type,'redirect' => urldecode($_GET['redirect']))); // Form
	print $form->render(); // Render
}
?>