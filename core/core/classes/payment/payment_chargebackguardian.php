<?php
if(!class_exists('payment_chargebackguardian',false)) {
	/**
	 * An 'extension' of the payment class used for processing payments through Chargeback Guardian.
	 *
	 * Notes:
	 * - When capturing an authorized transaction you MUST pass an amount (less than or equal to the quthorize amount).
	 *
	 * Custom variables: 0
	 *
	 * Test account (this gets auto-set in the class when $c[test] = 1):
	 * - username: demo
	 * - password: password
	 *
	 * To do
	 * - Support for $payment->custom array
	 * - Return 'custom' variables in returned results (if possible)
	 * - check for required fields before sending?
	 *
	 * Dependencies
	 * - Functions
	 *   - x() - __construct()
	 *
	 * @package kraken\payments
	 */
	class payment_chargebackguardian {
		/** Stores the transaction key for your Chargeback Guardian account. */
		public $username;
		/** Stores the password for your Chargeback Guardian account. */
		public $password;
		/** Stores an array of configuration values passed to the class. */
		public $c;
		
		/**
		 * Constructs the class.
		 *
		 * Configuration values (key, type, default - description):
		 * - url, string, https://secure.chargebackguardiangateway.com/api/transact.php - The URL we'll be sending transactions to. There are several sites that use the sae format, but different URLs.  Here's an incomplete list:
		 *   - Chargeback Guardian: https://secure.chargebackguardiangateway.com/api/transact.php
		 *   - Network Merchants: https://secure.networkmerchants.com/api/transact.php
		 * - test, boolean, 0 - Whether or not we want to process transactions via the testing 'sandbox'.
		 *
		 * @param string $username The username of your Chargeback Guardian account.
		 * @param string $password The password of your Chargeback Guardian account.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function __construct($username,$password,$c = NULL) {
			self::payment_chargebackguardian($username,$password,$c);
		}
		function payment_chargebackguardian($username,$password,$c = NULL) {
			// Username
			$this->username = $username;
			// Password
			$this->password = $password;
			
			// Config
			if(!$c[url]) $c[url] = "https://secure.chargebackguardiangateway.com/api/transact.php";
			if(!x($c[test])) $c[test] = 0;
			$this->c = $c;
		}
		
		/**
		 * Authorizes the customer's account for given amount.
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction including 'result' (boolean, 1 = succes, 0 = error) and 'message'.
		 */
		function authorize($payment,$c = NULL) {
			// Common errors
			if($error_common = $this->errors()) {
				$error = $error_common;
			}
			// No amount passed
			else if(!$payment->amount) {
				$error = "No authorization amount was passed.";
			}
			// No credit card
			else if(!$payment->card[number]) {
				$error = "No credit card was passed for us to authorize.";	
			}
			
			// Error
			if($error) {
				$results = array(
					'result' => 0,
					'message' => $error
				);	
			}
			// Transaction
			else {
				// Send
				$results = $this->send($payment,'authorize',$c);
			}
			
			// Return
			return $results;
		}
		
		/**
		 * Captures a transaction that was previously authorized via the authorize() method.
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction including 'result' (boolean, 1 = succes, 0 = error) and 'message'.
		 */
		function capture($payment,$c = NULL) {
			// Common errors
			if($error_common = $this->errors()) {
				$error = $error_common;
			}
			// No transaction ID
			else if(!$payment->transaction) {
				$error = "No transaction ID was passed that we could cancel.";
			}
			// No amount passed
			else if(!$payment->amount) {
				$error = "No capture amount was passed.";
			}
			
			// Error
			if($error) {
				$results = array(
					'result' => 0,
					'message' => $error
				);	
			}
			// Transaction
			else {
				// Send
				$results = $this->send($payment,'capture',$c);
			}
			
			// Return
			return $results;
		}
		
		/**
		 * Charges the customer a given amount.
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction including 'result' (boolean, 1 = succes, 0 = error) and 'message' as well as the transaction id ('transaction').
		 */
		function charge($payment,$c = NULL) {
			// Common errors
			if($error_common = $this->errors()) {
				$error = $error_common;
			}
			// No amount passed
			else if(!$payment->amount) {
				$error = "No charge amount was passed.";
			}
			// No credit card
			else if(!$payment->card[number]) {
				$error = "No credit card was passed for us to charge.";	
			}
			
			// Error
			if($error) {
				$results = array(
					'result' => 0,
					'message' => $error
				);
			}
			// Transaction
			else {
				// Send
				$results = $this->send($payment,'charge',$c);
			}
			
			// Return
			return $results;
		}
		
		/**
		 * Refunds a customer/account a defined amount on a previous transaction.
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction including 'result' (boolean, 1 = succes, 0 = error) and 'message'.
		 */
		function refund($payment,$c = NULL) {
			// Common errors
			if($error_common = $this->errors()) {
				$error = $error_common;
			}
			// No transaction ID
			else if(!$payment->transaction) {
				$error = "No transaction ID was passed for the transaction we want to refund.";
			}
			// No amount passed
			else if(!$payment->amount) {
				$error = "No refund amount was passed.";
			}
			
			// Error
			if($error) {
				$results = array(
					'result' => 0,
					'message' => $error
				);	
			}
			// Transaction
			else {
				// Send
				$results = $this->send($payment,'refund',$c);
			}
			
			// Return
			return $results;
		}
		
		/**
		 * Cancels/voids the given transaction.
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction including 'result' (boolean, 1 = succes, 0 = error) and 'message'.
		 */
		function cancel($payment,$c = NULL) {
			// Common errors
			if($error_common = $this->errors()) {
				$error = $error_common;
			}
			// No transaction ID
			else if(!$payment->transaction) {
				$error = "No transaction ID was passed that we could cancel.";
			}
			
			// Error
			if($error) {
				$results = array(
					'result' => 0,
					'message' => $error
				);	
			}
			// Transaction
			else {
				// Send
				$results = $this->send($payment,'cancel',$c);
			}
			
			// Return
			return $results;
		}
		
		/**
		 * Credits a customer's credit card the given amount.
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction including 'result' (boolean, 1 = succes, 0 = error) and 'message'.
		 */
		function credit($payment,$c = NULL) {
			// Common errors
			if($error_common = $this->errors()) {
				$error = $error_common;
			}
			// No amount passed
			else if(!$payment->amount) {
				$error = "No credit amount was passed.";
			}
			// No credit card
			else if(!$payment->card[number]) {
				$error = "No credit card was passed for us to credit.";	
			}
			
			// Error
			if($error) {
				$results = array(
					'result' => 0,
					'message' => $error
				);	
			}
			// Transaction
			else {
				// Send
				$results = $this->send($payment,'credit',$c);
			}
			
			// Return
			return $results;
		}
		
		/**
		 * Sends charge, authorize, and credit requests to the Chargeback Guardian API.
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param string $action The transaction action we're performing.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction including 'result' (boolean, 1 = succes, 0 = error) and 'message' as well as the transaction id ('transaction').
		 */
		function send($payment,$action,$c = NULL) {
			// Standardize
			$payment = $this->standardize($payment);
			
			// Class
			$this->gw = new gwapi($this->c[url]);
			// Login
			if($this->c[test]) $this->gw->setLogin("demo","password");
			else $this->gw->setLogin($this->username,$this->password);
			
			// Values
			if(in_array($action,array('charge','authorize','credit'))) {
				// Billing
				$this->gw->setBilling(
					$payment->address[first_name],
					$payment->address[last_name],
					$payment->address[company],
					$payment->address[address],
					$payment->address[address_2],
					$payment->address[city],
					$payment->address[state],
					$payment->address[zip],
					$payment->address[country],
					$payment->address[phone],
					$payment->address[fax],
					$payment->address[email],
					$payment->address[website]
				);
				// Shipping
				$this->gw->setShipping(
					$payment->shipping[first_name],
					$payment->shipping[last_name],
					$payment->shipping[company],
					$payment->shipping[address],
					$payment->shipping[address_2],
					$payment->shipping[city],
					$payment->shipping[state],
					$payment->shipping[zip],
					$payment->shipping[country],
					$payment->shipping[email]
				);
				
				// Order
				$this->gw->setOrder($payment->invoice,$payment->description,0,0,$payment->invoice,$payment->ip());
			}
			
			// Authorize
			if($action == "authorize") $r = $this->gw->doAuth($payment->amount,$payment->card[number],$payment->card[expiration],$payment->card[code]);
			// Capture
			if($action == "capture") $r = $this->gw->doCapture($payment->transaction,$payment->amount);
			// Charge
			if($action == "charge") $r = $this->gw->doSale($payment->amount,$payment->card[number],$payment->card[expiration],$payment->card[code]);
			// Refund
			if($action == "refund") $r = $this->gw->doRefund($payment->transaction,$payment->amount);
			// Cancel
			if($action == "cancel") $r = $this->gw->doVoid($payment->transaction);
			// Credit
			if($action == "credit") $r = $this->gw->doCredit($payment->amount,$payment->card[number],$payment->card[expiration]);
			
			// Results
			if($this->gw->responses) {
				$results[result] = ($this->gw->responses[response] == 1 ? 1 : 0);
				$results[message] = $this->gw->responses[responsetext];
				$results[transaction] = $this->gw->responses[transactionid];
				$results[action] = $action;
				$results[test] = $this->c[test];
				$results[source] = "Chargeback Guardian";
				$results[response] = $this->gw->responses;
			}
			
			// Return
			return $results;
		}
		
		/**
		 * Standardizes some of the values used by Chargeback Guardian.
		 *
		 * @param object $payment The payment object we want to standize the values of.
		 * @return object The standardized payment object.
		 */
		function standardize($payment) {
			// Error
			if(!$payment) return;
			
			// Test
			if($this->c[test]) {
				if($payment->card[expiration_month]) $payment->card[expiration_month] = 10;
				if($payment->card[expiration_year]) $payment->card[expiration_year] = 2010;
				if($payment->card[number]) $payment->card[number] = 4111111111111111;
				if($payment->amount < 1) $payment->amount = 1; // Must be at least $1.00
			}
			
			// Card expiration - MMYY
			if($payment->card[expiration_month] and $payment->card[expiration_year]) {
				$payment->card[expiration] = str_pad($payment->card[expiration_month],2,"0",STR_PAD_LEFT).substr($payment->card[expiration_year],2,2);
			}
			
			// Return
			return $payment;
		}
		
		/**
		 * Detects some common errors for this gateway and returns the error message.
		 *
		 * @return string The error message (if an error was detected).
		 */
		function errors() {
			// No login credentials
			if((!$this->username or !$this->password) and !$this->c[test]) {
				$error = "The login credentials for Chargeback Guardian are missing.";
			}
			
			// Return
			return $error;
		}
	}
}

if(!class_exists('gwapi',false)) {
	/**
	 * Class used to send orders, refunds, etc. to the Chargeback Guardian API.
	 *
	 * @package	kraken\payments	
	 */
	class gwapi {
		// The URL to post to. Custom.
		var $url = "https://secure.chargebackguardiangateway.com/api/transact.php";
		
		// Construct the class. You can (optionally) adjust the url we post to if it's one of the other implementations of the same API (NetworkMerchants.com, etc.). Custom.
		function __contruct($url = NULL) {
			return $this->gwapi($url);
		}
		function gwapi($url = NULL) {
			if($url) $this->url = $url;
		}
	
		// Initial Setting Functions
		function setLogin($username, $password) {/*{{{*/
			$this->login['username'] = $username;
			$this->login['password'] = $password;
		}/*}}}*/
	
		function setOrder($orderid,
				$orderdescription,
				$tax,
				$shipping,
				$ponumber,
				$ipaddress) {/*{{{*/
			$this->order['orderid']          = $orderid;
			$this->order['orderdescription'] = $orderdescription;
			$this->order['tax']              = $tax;
			$this->order['shipping']         = $shipping;
			$this->order['ponumber']         = $ponumber;
			$this->order['ipaddress']        = $ipaddress;
		}/*}}}*/
	
		function setBilling($firstname,
				$lastname,
				$company,
				$address1,
				$address2,
				$city,
				$state,
				$zip,
				$country,
				$phone,
				$fax,
				$email,
				$website) {/*{{{*/
			$this->address['firstname'] = $firstname;
			$this->address['lastname']  = $lastname;
			$this->address['company']   = $company;
			$this->address['address1']  = $address1;
			$this->address['address2']  = $address2;
			$this->address['city']      = $city;
			$this->address['state']     = $state;
			$this->address['zip']       = $zip;
			$this->address['country']   = $country;
			$this->address['phone']     = $phone;
			$this->address['fax']       = $fax;
			$this->address['email']     = $email;
			$this->address['website']   = $website;
		}/*}}}*/
	
		function setShipping($firstname,
				$lastname,
				$company,
				$address1,
				$address2,
				$city,
				$state,
				$zip,
				$country,
				$email) {/*{{{*/
			$this->shipping['firstname'] = $firstname;
			$this->shipping['lastname']  = $lastname;
			$this->shipping['company']   = $company;
			$this->shipping['address1']  = $address1;
			$this->shipping['address2']  = $address2;
			$this->shipping['city']      = $city;
			$this->shipping['state']     = $state;
			$this->shipping['zip']       = $zip;
			$this->shipping['country']   = $country;
			$this->shipping['email']     = $email;
		}/*}}}*/
	
		// Transaction Functions
	
		function doSale($amount, $ccnumber, $ccexp, $cvv="") {/*{{{*/
	
			$query  = "";
			// Login Information
			$query .= "username=" . urlencode($this->login['username']) . "&";
			$query .= "password=" . urlencode($this->login['password']) . "&";
			// Sales Information
			$query .= "ccnumber=" . urlencode($ccnumber) . "&";
			$query .= "ccexp=" . urlencode($ccexp) . "&";
			$query .= "amount=" . urlencode(number_format($amount,2,".","")) . "&";
			$query .= "cvv=" . urlencode($cvv) . "&";
			// Order Information
			$query .= "ipaddress=" . urlencode($this->order['ipaddress']) . "&";
			$query .= "orderid=" . urlencode($this->order['orderid']) . "&";
			$query .= "orderdescription=" . urlencode($this->order['orderdescription']) . "&";
			$query .= "tax=" . urlencode(number_format($this->order['tax'],2,".","")) . "&";
			$query .= "shipping=" . urlencode(number_format($this->order['shipping'],2,".","")) . "&";
			$query .= "ponumber=" . urlencode($this->order['ponumber']) . "&";
			// Billing Information
			$query .= "firstname=" . urlencode($this->address['firstname']) . "&";
			$query .= "lastname=" . urlencode($this->address['lastname']) . "&";
			$query .= "company=" . urlencode($this->address['company']) . "&";
			$query .= "address1=" . urlencode($this->address['address1']) . "&";
			$query .= "address2=" . urlencode($this->address['address2']) . "&";
			$query .= "city=" . urlencode($this->address['city']) . "&";
			$query .= "state=" . urlencode($this->address['state']) . "&";
			$query .= "zip=" . urlencode($this->address['zip']) . "&";
			$query .= "country=" . urlencode($this->address['country']) . "&";
			$query .= "phone=" . urlencode($this->address['phone']) . "&";
			$query .= "fax=" . urlencode($this->address['fax']) . "&";
			$query .= "email=" . urlencode($this->address['email']) . "&";
			$query .= "website=" . urlencode($this->address['website']) . "&";
			// Shipping Information
			$query .= "shipping_firstname=" . urlencode($this->shipping['firstname']) . "&";
			$query .= "shipping_lastname=" . urlencode($this->shipping['lastname']) . "&";
			$query .= "shipping_company=" . urlencode($this->shipping['company']) . "&";
			$query .= "shipping_address1=" . urlencode($this->shipping['address1']) . "&";
			$query .= "shipping_address2=" . urlencode($this->shipping['address2']) . "&";
			$query .= "shipping_city=" . urlencode($this->shipping['city']) . "&";
			$query .= "shipping_state=" . urlencode($this->shipping['state']) . "&";
			$query .= "shipping_zip=" . urlencode($this->shipping['zip']) . "&";
			$query .= "shipping_country=" . urlencode($this->shipping['country']) . "&";
			$query .= "shipping_email=" . urlencode($this->shipping['email']) . "&";
			$query .= "type=sale";
			return $this->_doPost($query);
	
		}/*}}}*/
	
		function doAuth($amount, $ccnumber, $ccexp, $cvv="") {/*{{{*/
	
			$query  = "";
			// Login Information
			$query .= "username=" . urlencode($this->login['username']) . "&";
			$query .= "password=" . urlencode($this->login['password']) . "&";
			// Sales Information
			$query .= "ccnumber=" . urlencode($ccnumber) . "&";
			$query .= "ccexp=" . urlencode($ccexp) . "&";
			$query .= "amount=" . urlencode(number_format($amount,2,".","")) . "&";
			$query .= "cvv=" . urlencode($cvv) . "&";
			// Order Information
			$query .= "ipaddress=" . urlencode($this->order['ipaddress']) . "&";
			$query .= "orderid=" . urlencode($this->order['orderid']) . "&";
			$query .= "orderdescription=" . urlencode($this->order['orderdescription']) . "&";
			$query .= "tax=" . urlencode(number_format($this->order['tax'],2,".","")) . "&";
			$query .= "shipping=" . urlencode(number_format($this->order['shipping'],2,".","")) . "&";
			$query .= "ponumber=" . urlencode($this->order['ponumber']) . "&";
			// Billing Information
			$query .= "firstname=" . urlencode($this->address['firstname']) . "&";
			$query .= "lastname=" . urlencode($this->address['lastname']) . "&";
			$query .= "company=" . urlencode($this->address['company']) . "&";
			$query .= "address1=" . urlencode($this->address['address1']) . "&";
			$query .= "address2=" . urlencode($this->address['address2']) . "&";
			$query .= "city=" . urlencode($this->address['city']) . "&";
			$query .= "state=" . urlencode($this->address['state']) . "&";
			$query .= "zip=" . urlencode($this->address['zip']) . "&";
			$query .= "country=" . urlencode($this->address['country']) . "&";
			$query .= "phone=" . urlencode($this->address['phone']) . "&";
			$query .= "fax=" . urlencode($this->address['fax']) . "&";
			$query .= "email=" . urlencode($this->address['email']) . "&";
			$query .= "website=" . urlencode($this->address['website']) . "&";
			// Shipping Information
			$query .= "shipping_firstname=" . urlencode($this->shipping['firstname']) . "&";
			$query .= "shipping_lastname=" . urlencode($this->shipping['lastname']) . "&";
			$query .= "shipping_company=" . urlencode($this->shipping['company']) . "&";
			$query .= "shipping_address1=" . urlencode($this->shipping['address1']) . "&";
			$query .= "shipping_address2=" . urlencode($this->shipping['address2']) . "&";
			$query .= "shipping_city=" . urlencode($this->shipping['city']) . "&";
			$query .= "shipping_state=" . urlencode($this->shipping['state']) . "&";
			$query .= "shipping_zip=" . urlencode($this->shipping['zip']) . "&";
			$query .= "shipping_country=" . urlencode($this->shipping['country']) . "&";
			$query .= "shipping_email=" . urlencode($this->shipping['email']) . "&";
			$query .= "type=auth";
			return $this->_doPost($query);
	
		}/*}}}*/
	
		function doCredit($amount, $ccnumber, $ccexp) {/*{{{*/
	
			$query  = "";
			// Login Information
			$query .= "username=" . urlencode($this->login['username']) . "&";
			$query .= "password=" . urlencode($this->login['password']) . "&";
			// Sales Information
			$query .= "ccnumber=" . urlencode($ccnumber) . "&";
			$query .= "ccexp=" . urlencode($ccexp) . "&";
			$query .= "amount=" . urlencode(number_format($amount,2,".","")) . "&";
			// Order Information
			$query .= "ipaddress=" . urlencode($this->order['ipaddress']) . "&";
			$query .= "orderid=" . urlencode($this->order['orderid']) . "&";
			$query .= "orderdescription=" . urlencode($this->order['orderdescription']) . "&";
			$query .= "tax=" . urlencode(number_format($this->order['tax'],2,".","")) . "&";
			$query .= "shipping=" . urlencode(number_format($this->order['shipping'],2,".","")) . "&";
			$query .= "ponumber=" . urlencode($this->order['ponumber']) . "&";
			// Billing Information
			$query .= "firstname=" . urlencode($this->address['firstname']) . "&";
			$query .= "lastname=" . urlencode($this->address['lastname']) . "&";
			$query .= "company=" . urlencode($this->address['company']) . "&";
			$query .= "address1=" . urlencode($this->address['address1']) . "&";
			$query .= "address2=" . urlencode($this->address['address2']) . "&";
			$query .= "city=" . urlencode($this->address['city']) . "&";
			$query .= "state=" . urlencode($this->address['state']) . "&";
			$query .= "zip=" . urlencode($this->address['zip']) . "&";
			$query .= "country=" . urlencode($this->address['country']) . "&";
			$query .= "phone=" . urlencode($this->address['phone']) . "&";
			$query .= "fax=" . urlencode($this->address['fax']) . "&";
			$query .= "email=" . urlencode($this->address['email']) . "&";
			$query .= "website=" . urlencode($this->address['website']) . "&";
			$query .= "type=credit";
			return $this->_doPost($query);
	
		}/*}}}*/
	
		function doOffline($authorizationcode, $amount, $ccnumber, $ccexp) {/*{{{*/
	
			$query  = "";
			// Login Information
			$query .= "username=" . urlencode($this->login['username']) . "&";
			$query .= "password=" . urlencode($this->login['password']) . "&";
			// Sales Information
			$query .= "ccnumber=" . urlencode($ccnumber) . "&";
			$query .= "ccexp=" . urlencode($ccexp) . "&";
			$query .= "amount=" . urlencode(number_format($amount,2,".","")) . "&";
			$query .= "authorizationcode=" . urlencode($authorizationcode) . "&";
			// Order Information
			$query .= "ipaddress=" . urlencode($this->order['ipaddress']) . "&";
			$query .= "orderid=" . urlencode($this->order['orderid']) . "&";
			$query .= "orderdescription=" . urlencode($this->order['orderdescription']) . "&";
			$query .= "tax=" . urlencode(number_format($this->order['tax'],2,".","")) . "&";
			$query .= "shipping=" . urlencode(number_format($this->order['shipping'],2,".","")) . "&";
			$query .= "ponumber=" . urlencode($this->order['ponumber']) . "&";
			// Billing Information
			$query .= "firstname=" . urlencode($this->address['firstname']) . "&";
			$query .= "lastname=" . urlencode($this->address['lastname']) . "&";
			$query .= "company=" . urlencode($this->address['company']) . "&";
			$query .= "address1=" . urlencode($this->address['address1']) . "&";
			$query .= "address2=" . urlencode($this->address['address2']) . "&";
			$query .= "city=" . urlencode($this->address['city']) . "&";
			$query .= "state=" . urlencode($this->address['state']) . "&";
			$query .= "zip=" . urlencode($this->address['zip']) . "&";
			$query .= "country=" . urlencode($this->address['country']) . "&";
			$query .= "phone=" . urlencode($this->address['phone']) . "&";
			$query .= "fax=" . urlencode($this->address['fax']) . "&";
			$query .= "email=" . urlencode($this->address['email']) . "&";
			$query .= "website=" . urlencode($this->address['website']) . "&";
			// Shipping Information
			$query .= "shipping_firstname=" . urlencode($this->shipping['firstname']) . "&";
			$query .= "shipping_lastname=" . urlencode($this->shipping['lastname']) . "&";
			$query .= "shipping_company=" . urlencode($this->shipping['company']) . "&";
			$query .= "shipping_address1=" . urlencode($this->shipping['address1']) . "&";
			$query .= "shipping_address2=" . urlencode($this->shipping['address2']) . "&";
			$query .= "shipping_city=" . urlencode($this->shipping['city']) . "&";
			$query .= "shipping_state=" . urlencode($this->shipping['state']) . "&";
			$query .= "shipping_zip=" . urlencode($this->shipping['zip']) . "&";
			$query .= "shipping_country=" . urlencode($this->shipping['country']) . "&";
			$query .= "shipping_email=" . urlencode($this->shipping['email']) . "&";
			$query .= "type=offline";
			return $this->_doPost($query);
	
		}/*}}}*/
	
		function doCapture($transactionid, $amount =0) {/*{{{*/
	
			$query  = "";
			// Login Information
			$query .= "username=" . urlencode($this->login['username']) . "&";
			$query .= "password=" . urlencode($this->login['password']) . "&";
			// Transaction Information
			$query .= "transactionid=" . urlencode($transactionid) . "&";
			if ($amount>0) {
				$query .= "amount=" . urlencode(number_format($amount,2,".","")) . "&";
			}
			$query .= "type=capture";
			return $this->_doPost($query);
	
		}/*}}}*/
	
		function doVoid($transactionid) {/*{{{*/
	
			$query  = "";
			// Login Information
			$query .= "username=" . urlencode($this->login['username']) . "&";
			$query .= "password=" . urlencode($this->login['password']) . "&";
			// Transaction Information
			$query .= "transactionid=" . urlencode($transactionid) . "&";
			$query .= "type=void";
			return $this->_doPost($query);
	
		}/*}}}*/
	
		function doRefund($transactionid, $amount = 0) {/*{{{*/
	
			$query  = "";
			// Login Information
			$query .= "username=" . urlencode($this->login['username']) . "&";
			$query .= "password=" . urlencode($this->login['password']) . "&";
			// Transaction Information
			$query .= "transactionid=" . urlencode($transactionid) . "&";
			if ($amount>0) {
				$query .= "amount=" . urlencode(number_format($amount,2,".","")) . "&";
			}
			$query .= "type=refund";
			return $this->_doPost($query);
	
		}/*}}}*/
	
		function _doPost($query) {/*{{{*/
			print "chargeback guardian query: ".$query."<br />";
			
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $this->url);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 15);
			curl_setopt($ch, CURLOPT_TIMEOUT, 15);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	
			curl_setopt($ch, CURLOPT_POSTFIELDS, $query);
			curl_setopt($ch, CURLOPT_POST, 1);
	
			if (!($data = curl_exec($ch))) {
				return 3; // Error
			}
			curl_close($ch);
			unset($ch);
			print "chargeback guardian response: ".$data."<br />";
			$data = explode("&",$data);
			for($i=0;$i<count($data);$i++) {
				$rdata = explode("=",$data[$i]);
				$this->responses[$rdata[0]] = $rdata[1];
			}
			//print_array($this->responses);
			return $this->responses['response'];
		}/*}}}*/	
	}
}
