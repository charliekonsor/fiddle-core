<?php
// Permission
if($this->permission('manage',1)) {
	// Heading
	print $this->heading();
	// Buttons
	print $this->heading_buttons();
	// Filters
	print $this->module_class->manage_filters();
	// Items
	print $this->module_class->manage_items();
}
?>