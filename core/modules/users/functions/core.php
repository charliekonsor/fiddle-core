<?php
/**
 * @package kraken\modules\users
 */
 
if(!function_exists('users_type_permissions')) {
	/**
	 * Returns array of permissions for the given user type.
	 *
	 * @param string $type The code of the user type you want to get the permissions of.
	 * @return array An array of the permissions for this user type.
	 */
	function users_type_permissions($type) {
		// Cached?
		$permissions = cache_get('permissions/'.$type);
		if(!x($permissions)) {
			// Get
			$m = module::load('types');
			$row = $m->row(array('db.code' => $type));
			
			// Unserialize
			$permissions = data_unserialize($row[type_permissions]);
			if(!$permissions) $permissions = 0; // So it'll be cached
			
			// Cache
			cache_save('permissions/'.$type,$permissions);
		}
		
		// Return
		return $permissions;
	}
}

if(!function_exists('users_type_default')) {
	/**
	 * Returns the default user type.
	 *
	 * Overwrite this in /local/functions/core.php if you want to define the default user type.
	 * 
	 * @return string The default user type code.
	 */
	function users_type_default() {
		// Type
		$type = NULL;
		foreach(m('users.types') as $k => $v) {
			if($v[settings][registered] and !$v[settings][admin]) {
				$type = $k;
				break;
			}
		}
		
		// Return
		return $type;
	}
}
?>