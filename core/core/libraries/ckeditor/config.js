/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Spell Check as You Type
	//config.scayt_autoStartup = 'true';
	
	// Paste as plain text - as an alternative, could use ev.editor.on('paste', function(evt) {} ); See bottom of this file for example
	//config.forcePasteAsPlainText = true;
	
	// CSS
	if(typeof CSS !== 'undefined') config.contentsCss = new Array(CSS);
	
	// Styles
	if(typeof CKEDITOR_STYLES === 'undefined') var CKEDITOR_STYLES_EXIST = false;
	else CKEDITOR_STYLES_EXIST = true;
	if(CKEDITOR_STYLES_EXIST) config.stylesSet = "ckeditor-styles:"+CKEDITOR_STYLES;
	
	// Templates
	if(typeof CKEDITOR_TEMPLATES === 'undefined') var CKEDITOR_TEMPLATES_EXIST = false;
	else CKEDITOR_TEMPLATES_EXIST = true;
	if(CKEDITOR_TEMPLATES_EXIST) config.templates_files = [ CKEDITOR_TEMPLATES ];
	
	// Height
	config.height = 280;
	
	// FontSizes
	config.fontSize_Sizes = '8/8px;9/9px;10/10px;11/11px;12/12px;14/14px;16/16px;18/18px;20/20px;22/22px;24/24px;28/28px;36/36px';
	
	// Custom
	config.filebrowserBrowseUrl = DOMAIN+'?ajax_action=file_browse_ckeditor&path=local/uploads/content/files/';
	config.filebrowserImageBrowseUrl = DOMAIN+'?ajax_action=file_browse_ckeditor&path=local/uploads/content/images/';
	config.filebrowserFlashBrowseUrl = DOMAIN+'?ajax_action=file_browse_ckeditor&path=local/uploads/content/flash/';
	config.filebrowserUploadUrl = DOMAIN+'?ajax_action=file_upload_ckeditor&path=local/uploads/content/files/';
	config.filebrowserImageUploadUrl = DOMAIN+'?ajax_action=file_upload_ckeditor&path=local/uploads/content/images/';
	config.filebrowserFlashUploadUrl = DOMAIN+'?ajax_action=file_upload_ckeditor&path=local/uploads/content/flash/';
	// KCFinder
	/*config.filebrowserBrowseUrl = DOMAIN+'core/core/libraries/ckeditor/kcfinder/browse.php?type=files';
	config.filebrowserImageBrowseUrl = DOMAIN+'core/core/libraries/ckeditor/kcfinder/browse.php?type=images';
	config.filebrowserFlashBrowseUrl = DOMAIN+'core/core/libraries/ckeditor/kcfinder/browse.php?type=flash';
	config.filebrowserUploadUrl = DOMAIN+'core/core/libraries/ckeditor/kcfinder/upload.php?type=files';
	config.filebrowserImageUploadUrl = DOMAIN+'core/core/libraries/ckeditor/kcfinder/upload.php?type=images';
	config.filebrowserFlashUploadUrl = DOMAIN+'core/core/libraries/ckeditor/kcfinder/upload.php?type=flash';*/
	// CKFinder
	/*config.filebrowserBrowseUrl = DOMAIN+'core/core/libraries/ckeditor/ckfinder/ckfinder.html';
    config.filebrowserImageBrowseUrl = DOMAIN+'core/core/libraries/ckeditor/ckfinder/ckfinder.html?Type=Images';
    config.filebrowserFlashBrowseUrl = DOMAIN+'core/core/libraries/ckeditor/ckfinder/ckfinder.html?Type=Flash';
    config.filebrowserUploadUrl = DOMAIN+'core/core/libraries/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files';
    config.filebrowserImageUploadUrl = DOMAIN+'core/core/libraries/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';
    config.filebrowserFlashUploadUrl = DOMAIN+'core/core/libraries/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash';*/
	
	// Skin
	//config.skin = 'gray'; // kamam, office2003, v2, gray (custom built)
	
	// Toolbars
	config.toolbar = 'Medium';
	config.toolbar_Full =
	[
		['Source','-','Save','NewPage','Preview','-','Templates'],
		['Cut','Copy','Paste','PasteText','PasteFromWord','-','Print', 'SpellChecker', 'Scayt'],
		['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
		['Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField'],
		'/',
		['Bold','Italic','Underline','Strike','-','Subscript','Superscript'],
		['NumberedList','BulletedList','-','Outdent','Indent','Blockquote'],
		['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
		['Link','Unlink','Anchor'],
		['Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak'],
		'/',
		['Styles','Format','Font','FontSize'],
		['TextColor','BGColor'],
		['Maximize', 'ShowBlocks','-','About']
	];
	/*config.toolbar_Medium =
	[
		['Source','-',(CKEDITOR_TEMPLATES_EXIST == 1 ? 'Templates' : ''),(CKEDITOR_TEMPLATES_EXIST == 1 ? '-' : ''),'Cut','Copy','Paste','-','Bold','Italic','Underline','Strike','-','Subscript','Superscript','NumberedList','BulletedList','HorizontalRule','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','Outdent','Indent','-','Table'],
		'/',
		['Image','Flash','SpecialChar','-','Link','Unlink','Anchor','-','TextColor','BGColor','-','Format','Font','FontSize',(CKEDITOR_STYLES_EXIST ? 'Styles' : '')]
	];*/
	config.toolbar_Medium =
	[
		['Source'],
		['Cut','Copy','Paste','RemoveFormat'],
		['Bold','Italic','Underline','Strike','-','Subscript','Superscript','-','NumberedList','BulletedList'],
		['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','Outdent','Indent'],
		'/',
		['Table','Image','Flash','MediaEmbed','SpecialChar'],
		['Link','Unlink','Anchor'],
		['TextColor','BGColor'],
		['Format','Font','FontSize'],
		['Scayt']
	];

	config.toolbar_Basic =
	[
		['Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink']
	];
	
	// Extra Allowed Content - use this to allow certain attributes/styles on elements not defined in the default allowedContent value.
	// http://docs.ckeditor.com/#!/guide/dev_allowed_content_rules
	config.extraAllowedContent = {
		'iframe blockquote cite script': { // Allow some of these things, even if they're missing from toolbar (aka, iframe icon, want that because MediaEmbed needs it...plugin is a bit behind the times)
			attributes: '*',
			classes: '*'
		},
		'table thead tbody tfoot tr th td': {
			attributes: 'width,height,id',
			classes: '*',
			styles: '*'
		},
		'h1 h2 h3 h4 h5 h6': {
			classes: '*'
		},
		'table': {
			attributes: 'bordercolor'
		},
		'div a p': {
			styles: '*',
            classes: '*',
			attributes: '*'
		}
	}
	
	// Plugins
	config.extraPlugins = 'mediaembed';
};

/**
 * Custom script for cleaning up pasted text.
 *
 * http://www.question2answer.org/qa/17781/tip-cleaning-pasted-html-text-with-ckeditor-using-regex
 * http://www.keyvan.net/2012/11/clean-up-html-on-paste-in-ckeditor/
 */
CKEDITOR.on('instanceReady', function(ev){
	ev.editor.on('paste', function(evt) {    
		if(typeof(evt.data['html']) !== 'undefined' && evt.data['html'] != null) {
			// Remove all html tags but whitelisted tags
			//evt.data['html'] = (evt.data['html']).replace(/<(?!\/?(p|br|ul|li|b|i|sub|sup|strike|em|strong|span|caption|table|tbody|tr|td)(>|\s))[^<]+?>/g,''); // Allow tables
			evt.data['html'] = (evt.data['html']).replace(/<(?!\/?(p|br|ul|li|b|i|sub|sup|strike|em|strong|span|caption)(>|\s))[^<]+?>/g,''); // Remove tables
			
			// Remove font-family, font-size, line-height, background from style (requires style to end with ;)
			evt.data['html'] = (evt.data['html']).replace(/font-family\:[^;]+;|font-size\:[^;]+;|line-height\:[^;]+;|background\-?(color|image|position|repeat|attachment)?\:[^;]+;/g, '');
			
			// Remove all styles from tags
			//evt.data['html'] = (evt.data['html']).replace(/(<[^>]+) style=".*?"/i, '');
			
			// Remove empty style, e.g. style="" || style="   "
			evt.data['html'] = (evt.data['html']).replace(/style\="\s*?"/, '');
			
			// Remove empty tags, e.g. <p></p>
			evt.data['html'] = (evt.data['html']).replace(/<[^\/>][^>]*><\/[^>]+>/, '');
		}
	}, null, null, 9);
});