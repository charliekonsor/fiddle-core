<?php
/************************************************************************************/
/************************************** Config **************************************/
/************************************************************************************/
$config = array(
	'name' => 'Comments',
	'single' => 'Comment',
	'plural' => 'Comments',
	'icon' => 'comments',
	'db_variables' => array(
		'comments_count' => array(
			'label' => 'Comment Count',
		),
	),
	'db' => array(
		'table' => 'items_comments',
		'id' => 'id',
		'parent_module' => 'module_code',
		'parent_id' => 'item_id',
		'user' => 'user_id',
		'visible' => 'comment_visible',
		'created' => 'comment_created',
		'updated' => 'comment_updated'
	),
	'permissions' => array(
		'add' => array(
			'label' => 'Add',
			'manage' => 1,
			'default' => array(
				'admins',
				'members',
			)
		),
		'view' => array(
			'label' => 'View',
			'default' => array(
				'admins',
				'members',
				'guests'
			)
		),
		'edit_own' => array(
			'label' => 'Edit Own',
			'manage' => 1,
			'default' => array(
				'admins',
				'members'
			)
		),
		'edit_all' => array(
			'label' => 'Edit All',
			'manage' => 1,
			'default' => array(
				'admins'
			)
		),
		'delete_own' => array(
			'label' => 'Delete Own',
			'manage' => 1,
			'default' => array(
				'admins',
				'members'
			)
		),
		'delete_all' => array(
			'label' => 'Delete All',
			'manage' => 1,
			'default' => array(
				'admins',
			)
		),
	),
	'forms' => array(
		'add' => array(
			'inputs' => array(
				array(
					'type' => 'textarea',
					'name' => 'comment_text',
					'validate' => array(
						'required' => 1,
					),
				)
			),
			'validate_javascript' => 0
		)
	)
);
?>