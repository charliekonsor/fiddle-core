<?php
if(!class_exists('plugin_changes',false)) {
	/**
	 * An extenion of the plugin class which handles functionality specific to the changes plugin.
	 *
	 * @package kraken\plugins\changes
	 */
	class plugin_changes extends plugin {
		/**
		 * Constructs the class.
		 *
		 * @param string $module The module of the item the plugin is acting upon.
		 * @param int $id The id of the item the plugin is acting upon. Default = NULL
		 * @param string $plugin The plugin we're using.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function __construct($module,$id = NULL,$plugin,$c = NULL) {
			$this->plugin_changes($module,$id,$plugin,$c);
		}
		function plugin_changes($module,$id = NULL,$plugin,$c = NULL) {
			parent::__construct($module,$id,$plugin,$c);
		}
		
		/**
		 * Returns array of inputs for use in the sidebar box for this plugin.
		 *
		 * @param object $form The form object we'll be adding these inputs to.
		 * @return array An array of inputs to add to the form.
		 */
		function sidebar($form) {
			if($this->id > 0) {
				$text = $this->changes();
				if($text) {
					$inputs = array(
						array(
							'type' => 'html',
							'value' => $text
						)
					);
				}
			}
			
			// Return
			if($inputs) {
				$return = array(
					'label' => plugin($this->plugin.'.name'),
					'inputs' => $inputs
				);
				return $return;
			}
		}
		
		/**
		 * Logs changes to a given item.
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function item_process($c = NULL) {
			// Error
			if(!$this->module or !$this->id or !$this->plugin or !$c[old]) return;
			
			// Config
			if(!x($c[user])) $c[user] = u('id'); // The user who did the changing.
			if(!x($c[debug])) $c[debug] = 0; // Debug
			
			// Debug
			debug("\$".__CLASS__."->".__FUNCTION__."()",$c[debug]);
			
			// Old
			$values_old = $c[old];
			
			// New
			$values_new = $this->item->row();
			
			// Changes
			$changes = NULL;
			foreach($values_new as $k => $v) {
				if(!in_array($k,array(m($this->module.'.db.updated'),m($this->module.'.db.created')))) {
					debug("k: ".$k.", old: ".$c[old][$k].", new: ".$v,$c[debug]);
					if($v != $values_old[$k]) $changes[] = $k;
				}
			}
			
			// Changes were made
			if($changes) {
				// Values
				$values = array(
					'user_id' => $c[user],
					'user_ip' => IP,
					'change' => data_serialize($changes),
					'change_values_old' => data_serialize($values_old),
					'change_values_new' => data_serialize($values_new),
					'change_url' => $_SESSION['urls'][0],
				);
				$values = $this->save_values($values,"INSERT");
				if($values) {
					// Debug
					debug("\$".__CLASS__."->".__FUNCTION__."() values: ".return_array($values),$c[debug]);
					
					// Values string
					$values_string = $this->db->values($values);
					
					// Build query
					$query = "INSERT INTO ".plugin($this->plugin.'.db.table')." ".$values_string;
					
					// Debug
					debug($query,$c[debug]);
					
					// Run query
					$result = $this->db->q($query);
				}
			}
		}
		
		/**
		 * Copy's the plugin row(s) for the item to the given new item.
		 *
		 * Does nothing, $item->save() will call $item->process() which will processes changes (which means do nothing as it's a new item).
		 *
		 * @param int $new_id The id of the new, copied item.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function item_copy($new_id,$c = NULL) {
			return;
		}
		
		/**
		 * Returns display of changes that have occured for the current item.
		 *
		 * @return string The HTML for displaying a list of changs that have occured to this item.
		 */
		function changes() {
			// Changes
			$rows = $this->db->q("SELECT * FROM items_changes WHERE module_code = '".$this->module."' AND item_id = '".$this->id."' ORDER BY change_created DESC");
			if($this->db->n($rows)) {
				$text .= "
<table class='core-table core-rows'>";
				while($row = $this->db->f($rows)) {
					$user = item::load('users',$row[user_id]);
					$changes = count(data_unserialize($row[change]));
					$text .= "
	<tr>
		<td width='75' align='center'>".date('m/d/Y',$row[change_created])."</td>
		<td>".($user->id ? "<a href='".$user->url($this->page->area)."'>".$user->name."</a>" : $row[user_ip])."</td>
		<td width='75' align='center'><a href='".D."?ajax_plugin=".$this->plugin."&ajax_action=change&amp;module=".$this->module."&amp;id=".$this->id."&amp;change=".$row[id]."&amp;area=".$this->page->area."' class='overlay' title='Changelog'>".$changes." ".string_pluralize("change",$changes)."</a></td>
	</tr>";
				}
				$text .= "
</table>";
			}
			else $text .= "
<div class='core-none'>No Changes Yet</div>";

			// Return
			return $text;
		}
	}
}
?>