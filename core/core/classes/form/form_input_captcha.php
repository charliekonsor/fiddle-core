<?php
if(!class_exists('form_input_captcha',false)) {
	/**
	 * An extension of the form_input_framework class with functionality specific to captcha inputs.
	 */
	class form_input_captcha extends form_input_framework {
		/**
		 * Loads class. Needed so we can use debug_backtrace() to get this class name in PHP < 5.3 in $form_input->load().
		 */
		static function load($label = NULL,$name = NULL,$value = NULL,$c = NULL) {
			return parent::load($label,$name,$value,$c);
		}
		
		/**
		 * Construct
		 */
		function __construct($label = NULL,$name = NULL,$value = NULL,$c = NULL) {
			$this->form_input_captcha($label,$name,$value,$c);
		}
		function form_input_captcha($label = NULL,$name = NULL,$value = NULL,$c = NULL) {
			// Construct
			parent::__construct($label,$name,$value,$c);
			
			// Config
			if(!x($this->c[refresh])) $this->c[refresh] = 1; // Include a 'refresh' link for changing captcha.
			
			// Name
			if(!x($this->name)) $this->name = "captcha";
			
			// Attributes - type - want to use the 'text' type on the input
			$this->attributes[type] = 'text';
			// Attributes - id - needed when we 'refresh' it
			if(!x($this->attributes[id])) $this->attributes[id] = $this->name; // ID of the input (needed when we 'refresh' it)
			
			// Validate - required
			if(!x($this->validate[required])) $this->validate[required] = 1;
			
		}
		
		/**
		 * Renders the HTML for the actual input.
		 *
		 * @param object $form The form object this input is a part of.
		 * @param array $c An array of config values. Default = NULL
		 * @return string The HTML of the input.
		 */
		function html_element($form,$c = NULL) {
			// No value
			$this->value = NULL;
			
			// Image
			$html = "
<div class='form-captcha-container'>
	<div class='form-captcha-image'>
		<img src='".D."core/core/libraries/captcha/captcha.php?session_var=".$this->name."&' id='".$this->attributes[id]."-captcha-image' />
	</div>
	<span class='form-captcha-help'>Please enter the text you see above.</span>";
	
			// Refresh
			if($this->c[refresh]) $html .= "
	<span class='form-captcha-refresh'><a href=\"javascript:document.getElementById('".$this->attributes[id]."-captcha-image').src='".D."core/core/libraries/captcha/captcha.php?session_var=".$this->name."&'+Math.random();document.getElementById('".$this->attributes[id]."').focus();\" style='text-decoration:underline;'>Change text</a></span>";
	
			// Element
			$html .= "
	<div class='form-captcha-input'>".parent::html_element($form,$c)."</div>
</div>";
			
			// Return
			return $html;
		}
			
		/**
		 * Processes the value for this input.
		 *
		 * Note, we do the validation in here (instead of $form_input->validate()) so that it'll be checked even if $this->c[validate_php] == 0.
		 *
		 * @param object $form The form object which contains the submitted values ($form->post and $form->files).
		 * @param array $c An array of configuration values. Default = NULL
		 * @return mixed The resulting value.
		 */
		function process($form,$c = NULL) {
			// Parent
			$value = parent::process($form,$c);
			
			// Valid
			if($this->name) {
				debug("input: ".$this->name.", value: ".$value.", session: ".$_SESSION[$this->name],$c[debug]);
				if(empty($_SESSION[$this->name]) or trim(strtolower($value)) != $_SESSION[$this->name]) {
					$form->errors[header][$this->name] = "The captcha you entered".($this->label[value] ? " for the ".$this->label[value] : "")." was incorrect.";
					$form->errors[inline][$this->name] = "The captcha you entered was incorrect.";
					$value = "NULL";
				}
			}
			
			// Return
			return $value;
		}
	}
}
?>