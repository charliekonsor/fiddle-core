<?php
/************************************************************************************/
/************************************** Config **************************************/
/************************************************************************************/
$config = array(
	'default' => 1,
	'required' => 1,
	'name' => 'Meta',
	'single' => 'Meta Data',
	'plural' => 'Meta Data',
	'icon' => 'meta',
	'db_variables' => array(
		'meta_title' => array(
			'label' => 'Meta Title',
			'search' => 1,
			'weight' => 7,
		),
		'meta_description' => array(
			'label' => 'Meta Description',
			'search' => 1,
			'weight' => 6,
		),
		'meta_keywords' => array(
			'label' => 'Meta Keywords',
			'search' => 1,
			'weight' => 5,
		),
		'meta_image' => array(
			'label' => 'Meta Image'
		),
	)
);
?>