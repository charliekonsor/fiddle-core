<?php
// Visible / permissions
if($this->visible(1) and $this->permission('view',1)) {
	print "
<div class='page page-module-".$this->module." page-view-".$this->view."'>
	".$this->item_class->v('page_text')."
</div>";
}