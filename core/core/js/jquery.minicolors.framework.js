/**
 * Applies colorpicker functionality to all inputs with class="colorpicker".
 *
 * http://www.abeautifulsite.net/blog/2011/02/jquery-minicolors-a-color-selector-for-input-controls/	
 */
function colorpicker() {
	// MiniColors
	if($().miniColors) {
		$('input.colorpicker:not(.colorpicker_added)').each(function() {
			// Default Settings
			var settings_default = {
				//readonly: true
			};
				
			// Settings
			var settings = $.extend(settings_default,$(this).metadata());
			
			// Apply
			$(this)
				.miniColors(settings)
				.addClass("colorpicker_added")
				.attr('autocomplete','off');
		});
	}
}