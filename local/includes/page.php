<?php
/**
 * This file gets called after all functions/classes have been loaded, the session started, any ajax/process scripts have been run, and the page we're on has been determined.
 *
 * It gets called just before the actual HTML content is determined and displayed.
 */
?>