<?php
if(!class_exists('plugin_search',false)) {
	/**
	 * An extenion of the plugin class which handles functionality specific to the search plugin.
	 *
	 * @package kraken\plugins\search
	 */
	class plugin_search extends plugin {
		/**
		 * Constructs the class.
		 *
		 * @param string $module The module of the item the plugin is acting upon.
		 * @param int $id The id of the item the plugin is acting upon. Default = NULL
		 * @param string $plugin The plugin we're using.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function __construct($module,$id = NULL,$plugin,$c = NULL) {
			$this->plugin_search($module,$id,$plugin,$c);
		}
		function plugin_search($module,$id = NULL,$plugin,$c = NULL) {
			parent::__construct($module,$id,$plugin,$c);
		}
		
		/**
		 * Returns array of inputs for use in the sidebar box for this plugin.
		 *
		 * @param object $form The form object we'll be adding these inputs to.
		 * @return array An array of inputs to add to the form.
		 */
		function sidebar($form) {
			#nothing
		}
		
		/**
		 * Indexes search terms for an item.
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function item_process($c = NULL) {
			// Error
			if(!$this->module or !$this->id or !$this->plugin) return;
			
			// Indexing turned on (or we want to search it publicly in which case we'll use the index)
			if(m($this->module.'.settings.search.index',$this->item->type) == 1 or m($this->module.'.settings.search.public',$this->item->type) == 1) {
				//$f_r = function_speed(__CLASS__.'->'.__FUNCTION__);
				
				// Config
				if(!$c[fields]) $c[fields] = NULL; // An array of fields to index in array(field => weight) format. Will default to module defined fields or global fields momentarily.
				if(!$c[stops]) { // Stop words
					if(!$c[stops] = cache_get('search/stops')) {
						$rows = $this->db->q("SELECT * FROM items_index_stops WHERE stop_visible = 1");
						while($row = $this->db->f($rows)) $c[stops][s($qry0[stop_word])] = $qry0[stop_score];
						cache_save('search/stops',$c[stops]);
					}
				}
				if(!x($c[debug])) $c[debug] = 0; // Debug
				
				/* To Do
				- After index, run script to weight words based upon frequency in site (one occurence = high score, 1000 occurences = low score)
				  - Maybe store in own index_words table and use 'weight' next time (or just update index scores on the fly)
				  - Only really benefits multiple words searches (ex: 'the inquisition', we'd know 'the' is less important and 'inquisition' is more important)
				- Pass on points if previous word was a 'common' word
				- Take length of word into account // Tried this, but think it won't really have any effect so commented out
				- Maybe give numbers a lower/higher score?
				- See if there is any good algorithim for weighting words out there (maybe the 'stem' class or 'quicksilver' js)
				*/
				
				// ID Columns (key => module) // These field store an id instead of text, have to get name (and code) of id's item and index that instead
				$id_columns = NULL;
				if($key = m($this->module.'.db.user')) $id_columns[$key] = "users";
				if($key = m($this->module.'.db.category')) $id_columns[$key] = "categories";
				
				// Fields - module defined
				if(!$c[fields]) $c[fields] = array_filter(m($this->module.'.settings.search.columns'));
				// Fields - global database veriables // Now pre-defined in $config_global when creating modules array
				/*if(!$c[fields]) {
					foreach(g('framework.db') as $k => $v) {
						if($v[search]) $fields_default["db.".$k] = $v[weight];
					}
				}*/
				// Fields - standardize
				$fields = search_fields_standardize($this->module,$c[fields]);
				if($c[debug]) debug("<br />\$".__CLASS__."->".__FUNCTION__."() - fields: ".return_array($fields),$c[debug]); // return_array() can consume a lot of memory, check debug first
				
				// Changed?
				if($c[debug]) debug("\$c[save][values]: ".return_array($c[save][values]),$c[debug]); // return_array() can consume a lot of memory, check debug first
				$changed = 0;
				if(!x($c[save][values])) $changed = 1; // Didn't pass 'save' values so we're not sure
				else if(array_intersect_key($c[save][values],$fields)) $changed = 1; // Changed at least one of the fields we're indexing
				if($changed) {
					// Delete old words
					$this->item_delete($c);
					
					// Loop through fields
					$index = NULL;
					foreach($fields as $field => $weight) {
						debug("\$".__CLASS__."->".__FUNCTION__."() - field: ".$field.", weight: ".$weight,$c[debug]); // return_array() can consume a lot of memory, check debug first
						if($weight) { 
							// This item has a value for this field
							$text = trim($this->item->v($field));
							if($text) {
								// ID Columns
								if($id_columns[$field]) {
									$id_item = item::load($id_columns[$field],$text);
									if($id_item) $text = $id_item->name.($id_item->code ? " ".$id_item->code : "");
								}
							
								// Prepare Text
								$words = search_index_words($text,array('return' => 'data'));
								debug("\$".__CLASS__."->".__FUNCTION__."() - string: ".$text,$c[debug]);
								if($c[debug]) debug("\$".__CLASS__."->".__FUNCTION__."() - array:".return_array($words),$c[debug]); // return_array() can consume a lot of memory, check debug first
								
								// Loop Through Words
								$x = 0;
								$top_score = 1;
								$count = count($words);
								$degradation = (.3 / $count); // Score each sucessive word loses (amount last word will be reduced to / number of words)
								foreach($words as $word => $v) {
									// Score
									$score = $top_score;
									// Score - order
									if($x > 0) {
										$score -= $degradation;
										$top_score = $score; // Update top score
									}
									// Score - length - give a bit extra weight for longer words // Not sure this would really add anything as searching for a word, all results would have the same extra weight (all same word, all same length)
									//$length = strlen($word);
									//if($length > 4) $score += ($length - 4) * .01;
									// Score - stop word
									if($c[stops][$word]) $score = $c[stops][$word];
									// Score - hashtag
									if($v[hashtag]) $score += 2;
									
									// Store
									if(!$index[$word]) {
										$v[score] = 0;
										$index[$word] = $v;
									}
									if(!$v[hashtag]) $index[$word][hashtag] = $v[hashtag];
									$index[$word][score] += ($score * $weight);
									
									$x++;
								}
							}
						}
					}
					
					// Insert
					if($index) {
						// Debug
						if($c[debug]) debug("\$".__CLASS__."->".__FUNCTION__."() - index:".return_array($index),$c[debug]); // return_array() can consume a lot of memory, check debug first
						
						// Query
						$query = NULL;
						foreach($index as $word) {
							if(!$query) $query = "INSERT INTO `items_index` (module_code,type_code,user_id,item_id,item_visible,index_word,index_score,index_hashtag) VALUES ";
							else $query .= ",";
							$query .= "('".$this->module."','".$this->item->type."','".$this->item->user."','".$this->id."','".$this->item->visible."','".a($word[word])."','".$word[score]."','".$word[hashtag]."')";
						}
						debug($query,$c[debug]);
						
						// Save
						$this->db->q($query);
					}
				}
				
				//function_speed(__CLASS__.'->'.__FUNCTION__,$f_r);
			}
		}
		
		/**
		 * Copy's the plugin row(s) for the item to the given new item.
		 *
		 * Does nothing as we already processed the item's search terms by calling $item->save(), then $item->process() which called $plugin_search->item_process().
		 *
		 * @param int $new_id The id of the new, copied item.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function item_copy($new_id,$c = NULL) {
			return;
		}
		
		/**
		 * Deletes plugin row(s) for the item.
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function item_delete($c = NULL) {
			// Error
			if(!$this->module or !$this->id or !$this->plugin) return;
			
			// Delete rows at once
			if(self::v('db.table') and self::v('db.parent_module') and self::v('db.parent_id')) {
				// Query
				$query = "DELETE FROM ".self::v('db.table')." WHERE ".self::v('db.parent_module')." = '".$this->module."' AND ".self::v('db.parent_id')." = '".$this->id."'";
				debug($query,$c[debug]);
				
				// Delete
				$this->db->q($query);
			}
		}
		
		/**
		 * Tweaks the $c configuration array before querying rows of a module.
		 *
		 * @param array $c The configuration array we want to tweak in this module. Defaut = NULL
		 * @return array The tweaked configuration array.
		 */
		function module_rows($c = NULL) {
			// Config
			if(!$c[search]) $c[search] = NULL; // A string to search for in query.
			if(!$c[search_level]) $c[search_level] = "advanced"; // Either 'advanced' (use index) or 'simple' (use LIKE)
			if(!x($c[search_all])) $c[search_all] = 1; // When searching do you want to match 'all' words. If == 0 it'll match one or more words in the string.
			if(!$c[search_match]) $c[search_match] = "partial"; // What wildcards to use in searches (partial = LIKE '%term%', start = LIKE 'term%', end = LIKE '%term', exact = LIKE 'term') (only applies to 'simple' search level)
			if(!x($c[search_split])) $c[search_split] = 1; // Do you want to split up the search string? 1 = Search all words in $c[search], 0 = search for entire phrase (only applies to 'simple' search level)
			if(!$c[search_fields]) $c[search_fields] = (m($this->module.'.settings.search.columns') ? array_keys(m($this->module.'.settings.search.columns')) : array('db.id','db.name','db.code','db.description')); // Array of fields to search
			
			// Have search term
			if($c[search]) {
				// Not indexed, must use 'simple' search
				if(!m($this->module.'.settings.search.index',$type) and !m($this->module.'.settings.search.public',$type)) $c[search_level] = "simple"; 
				
				// Search - advanced
				if($c[search_level] == "advanced") {
					// Words
					$words = search_index_words($c[search],array('return' => 'data'));
					if($words) {
						// Order - unless we're just getting count
						if(!strstr($c[select],"COUNT(*) c")) {
							// Make 'score' the first ORDER BY
							$c[order] = "score DESC".($c[order] ? ", ".$c[order] : "");
							// SELECT score (for ordering by)
							$c[select] .= ($c[select] ? ", " : "")."s.index_score score"; // Will only use SUM() if more than one word (we'll change it below if needs be)
						}
						
						// Must contain all words
						if($c[search_all] == 1 and count($words) > 1) {
							$c[select] = str_replace('s.index_score score','SUM(s.index_score) score',$c[select]);
							$c[group] .= ($c[group] ? ", " : "")./*"s.module_code, */"s.item_id"; // Don't need module_code because only getting from one module
							$c[having] .= ($c[having] ? " AND " : "")."COUNT(DISTINCT s.index_word) = ".count($words); 
						}
						
						// Hashtag? / simplified words (no data)
						$hashtag = 0;
						$hashtag_all = 1;
						$words_simple = NULL;
						foreach($words as $word) {
							if($word[hashtag]) $hashtag = 1;
							else $hashtag_all = 0;
							$words_simple[] = $word[word];
						}
						
						// Join
						$c[join] .= "
				JOIN
					`items_index` s
						ON i.".m($this->module.'.db.id')." = s.item_id";
							
						// Where
						$c[where] .= "
					".($c[where] ? "AND " : "")."s.module_code = '".$this->module."'";
					
						// Where - hashtag mix of hashtags and non-hashtags
						if($hashtag and !$hashtag_all) {
							$c[where] .= "
					AND (";
							$x = 0;				
							foreach($words as $word) {
								$c[where] .= "
						".($x > 0 ? " OR " : "");
								if($c[search_match] == "exact") $c[where] .= "s.index_word = '".a($word[word])."'";
								else if($c[search_match] == "start") $c[where] .= "s.index_word LIKE '".a($word[word])."%'";
								else if($c[search_match] == "end") $c[where] .= "s.index_word LIKE '%".a($word[word])."'";
								else $c[where] .= "s.index_word LIKE '%".a($word[word])."%'"; // Partial
								if($word[hashtag]) $c[where] .= " AND s.index_hashtag = 1";
								$x++;
							}
							$c[where] .= "
					)";
						}
						else {
							// Where - exact match
							if($c[search_match] == "exact") $c[where] .= "
						AND s.index_word ".(count($words) == 1 ? "= '".a($words_simple[0])."'" : "IN ('".implode("','",$words_simple)."')");
							// Where - match start of word
							else if($c[search_match] == "start") $c[where] .= "
						AND s.index_word RLIKE '^(".implode('|',$words_simple).")'";
							// Where - match end of word
							else if($c[search_match] == "end") $c[where] .= "
						AND s.index_word RLIKE '(".implode('|',$words_simple).")$'";
							// Where - partial match
							else $c[where] .= "
						AND s.index_word RLIKE '(".implode('|',$words_simple).")'";
							// Where - hashtag
							if($hashtag) $c[where] .= "
						AND s.index_hashtag = 1";
						}
						
						// Group
						$c[group] .= ($c[group] ? ", " : "")."
					s.item_id";
					}
				}
				// Search - simple
				else {
					// Fields
					foreach($c[search_fields] as $k => $field) {
						if(substr($field,0,3) == "db.") $field = m($this->module.'.'.$field);
						if($field) {
							if(strstr($field,' ')) {
								$ex = explode(' ',$_field);
								foreach($ex as $e) $c[search_fields][] = $e;
								unset($c[search_fields][$k]);
							}
							else $c[search_fields][$k] = $field;
						}
						else unset($c[search_fields][$k]);
					}
					if(!$c[search_fields]) $c[search] = NULL;
					
					// Where
					if($c[search]) {
						$c[where] .= ($c[where] ? " AND " : "")."(";
						if($c[search_split] == 1) $words = explode(' ',$c[search]); // Search all terms
						else $words = array($c[search]); // Search full term
						foreach($words as $x => $word) {
							// And/Or (match all words, or just one or more)
							$c[where] .= ($x > 0 ? ($c[search_all] == 1 ? " AND " : " OR ") : "")."(";
							foreach($c[search_fields] as $field) {
								if($field == m($this->module.'.db.id')) $c[where] .= " OR i.".$field." = '".$word."'";
								else {
									if($c[search_match] == "exact") $c[where] .= " OR i.".$field." LIKE '".$word."'";
									else if($c[search_match] == "start") $c[where] .= " OR i.".$field." LIKE '".$word."%'";
									else if($c[search_match] == "end") $c[where] .= " OR i.".$field." LIKE '%".$word."'";
									else $c[where] .= " OR i.".$field." LIKE '%".$word."%'";
								}
							}
							$c[where] .= ")";
						}
						$c[where] .= ")";
						$c[where] = str_replace('( OR','(',$c[where]);
					}
				}
			}
			
			// Return
			return $c;
		}
	}
}
?>