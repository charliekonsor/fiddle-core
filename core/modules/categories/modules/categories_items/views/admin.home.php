<?php
// Child module, but missing parent id, redirect to parent home page
if($this->module_class->v('parent') and !$this->page->parents[last][id]) {
	$parent_module = module::load($this->module_class->v('parent'));
	redirect($parent_module->url('home',$this->area));
}

// Permission
if($this->permission('manage',1)) {
	// Heading
	print $this->heading();
	// Buttons
	print $this->heading_buttons();
	// Filters
	print $this->module_class->manage_filters();
	// Items
	print $this->module_class->manage_items(array('query_c' => array('db.parent_id' => $this->page->parents[last][id])));
}
?>