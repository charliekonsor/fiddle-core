<?php
/**
 * Saves a submitted plugin form.
 */
if($_POST['ajax_action'] == "plugin_save") {
	if($_POST['ajax_plugin']) {
		// Plugin
		$plugin_item = plugin_item::load($_POST['ajax_module'],$_POST['ajax_id'],$_POST['ajax_plugin'],$_POST['id']);
		
		// Form
		$form = $plugin_item->form();
		
		// Process
		$results = $form->process(g('unpure.post'),g('unpure.files'),array('debug' => 0));
		
		// Errors
		if($results[errors]) {
			if($results[c][url] and (!$results[c][debug] or !debug_active())) redirect($results[c][url]);
			else if(debug_active()) print "
<div class='core-red'>
	<b>Errors:</b>
	".return_array($results[errors])."
</div>";
		}
		// No errors
		else {
			// Save
			$id = $plugin_item->save($results[values],array('debug' => $results[c][debug]));
			
			// Result
			if($id) {
				if(!$_POST['id']) print "
<div class='jquery-slideDown' style='display:none;'>";
				/*print "
	".$plugin_item->load->view(array('module' => $plugin_item->module,'id' => $plugin_item->id,'plugin' => $plugin_item->plugin,'plugin_id' => $plugin_item->plugin_id,'view' => 'comment'));*/
				print "
	".$plugin_item->view('item');
				if(!$_POST['id'])  print "
</div>";
			}
		}
	}
	
	// Exit
	exit;
}
?>