<?php
/************************************************************************************/
/************************************** Config **************************************/
/************************************************************************************/
$config = array(
	'name' => 'Categories',
	'single' => 'Category',
	'plural' => 'Categoriess',
	'icon' => 'category',
	'db' => array(
		'table' => 'categories',
		'id' => 'category_id',
		'parent_module' => 'module_code', // Not sure this is exactly proper as it's not really a 'child' module (not child to items anyway), but need this so urls can be created properly
		'code' => 'category_code',
		'name' => 'category_name',
		'visible' => 'category_visible',
		'created' => 'category_created',
		'updated' => 'category_updated',
	),
	'permissions' => array(
		'add' => array(
			'label' => 'Add',
			'manage' => 1,
			'default' => array(
				'admin'
			)
		),
		'edit' => array(
			'label' => 'Edit',
			'manage' => 1,
			'default' => array(
				'admin'
			)
		),
		'delete' => array(
			'label' => 'Delete',
			'manage' => 1,
			'default' => array(
				'admins',
			)
		),
		'copy' => array(
			'label' => 'Copy',
			'manage' => 1,
			'default' => array(
				'admin'
			)
		),
		'settings' => array(
			'label' => 'Settings',
			'default' => array(
				'admins',
			)
		),
	),
	'admin' => array(
		'section' => '',
		'views' => array(
			'home' => array(
				'label' => 'All Categories',
				'urls' => array(
					array(
						'format' => 'admin/{parent_module}/{module}',
					),
				),
			),
			'item' => array(
				'label' => 'Edit Category',
				'urls' => array(
					array(
						'format' => 'admin/{parent_module}/{module}/{id}',
					),
				),
			),
		),
	),
	'public' => array(
		// We have a custom url() method in the categories item class which will get parent module, then get the 'category' view from there. We'd only use this if we were linking to a category independent of it's parent module
		'active' => 0
	),
	'forms' => array(
		'add' => array(
			'inputs' => array(
				array(
					'label' => 'Name',
					'type' => 'text',
					'name' => 'category_name',
					'validate' => array(
						'required' => 1,
					),
				),
				array(
					'label' => 'Multiple',
					'type' => 'checkbox',
					'name' => 'category_multiple',
					'options' => array(
						1 => 'Yes'
					),
				),
				array(
					'label' => 'Required',
					'type' => 'checkbox',
					'name' => 'category_required',
					'options' => array(
						1 => 'Yes'
					),
				)
			)
		)
	)
);
?>