<?php
$module_class = module::load($this->module);
$single = strtolower($module_class->v('single'));

print "
The 'URL' section in the sidebar lets us define the URL for this ".$single.".<br /><br />

<em>URL sidebar</em><br />
<img src='".DOMAIN.$this->v('path')."documentation/images/sidebar.png' alt='' /><br /><br />

Now, exactly how a URL is formatted depends largely on the module. As you can see in the example above (in the 'Blogs' module') the URL is composed of 'blogs', a forward slash, the 'id' of the item, another forward slash, and finally a text field. This text field is what we can customize.<br /><br />

In this example (the Blogs module) we can't change the 'blogs/{id}/' part, but we can change the text that appears after that. In some other modules, the Pages module for example, the URL is composed entirely of this text field so we can define exactly what the URL will look like.<br /><br />

By default, this text field will be populated with a URL friendly version of this ".string_ownership($single)." 'Name'.  So, for exapmle, if I were to name my blog entry 'My First Blog Entry' and then opened up the URL sidebar section, I'd see this:<br /><br />

<em>Auto-popuplated URL</em><br />
<img src='".DOMAIN.$this->v('path')."documentation/images/sidebar-autopopulated.png' alt='' /><br /><br />

This means that you never <em>have to</em> define the URL for this ".$single.".  But if you <em>wanted to</em> all you'd have to do was type the custom URL into the text field.  For example:<br /><br />

<em>Custom URL</em><br />
<img src='".DOMAIN.$this->v('path')."documentation/images/sidebar-custom.png' alt='' /><br /><br />

Now, when the ".$single." is saved, it will be accessible via your custom URL.<br /><br />

Now, let's say you this ".$single." is somewhat old. People have already linked to it. You want to change the URL, but you don't want the old links to broken. Well, never you mind, my dear. All past URLs for this ".$single." will still point to this ".$single.", forever and always.";
?>