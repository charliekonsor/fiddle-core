<?php
if(!class_exists('core',false)) {
	/**
	 * Loads core classes and creates methods for accessing them.
	 *
	 * @package kraken
	 */
	class core {
		// Variables
		public $c = array(); // array
		public $db; // object
		
		/**
		 * Loads and returns an instance of either the core class or (if it exists) the framework core class.
		 *
		 * Example:
		 * - $page = core::load($c);
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 * @return object An instance of either the core class or (if it exists) the framework core class.
		 */
		static function load($c = NULL) {
			// Class name
			$class = __CLASS__;
			
			// Framework
			if(FRAMEWORK) {
				// Framework class
				$class_framework = $class."_framework";
				if(class_exists($class_framework)) return new $class_framework($c);
			}
			
			// Core class
			return new $class($c);
		}

		/**
		 * Constructs the class
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function __construct($c = NULL) {
			self::core($c);
		}
		function core($c = NULL) {
			// Config
			if($c and $this->c) $this->c = array_merge($this->c,$c);
			else if($c) $this->c = $c;
			
			// Database
			$this->db = new db();
		}

		/**
		 * Returns HTML content of given file as it would be displayed to the end user (meaning, PHP gets processed).													
		 * 
		 * @param string $file The file you want to get the displayed content of.
		 * @param array $variables An array of variables to pass when get the HTML of the view. Example: array('perpage' => 5); would be accessible via $perpage in the view file. Default = NULL
		 * @param string The HTML of the displayed file.
		 */
		function html_get($file,$variables = NULL) {
			// Start capture
			ob_start();
			
			// File contains query string, need to use file_get_contents()
			if(strstr($file,"?")) print file_get_contents(str_replace(SERVER,DOMAIN,$file));
			// Basic file
			else if(file_exists($file)) {
				// Variables
				if($variables) extract($variables);
				// File
				include $file;
			}
			
			// Get captured contents
			$html = ob_get_contents();
			// End capture
			ob_end_clean();
			
			// Remove space before DOCTYPE
			$html = ltrim($html);
			
			// Return
			return $html;
		}
		
		/**
		 * Method for eaily creating a form instance within the page class.
		 * 
		 * @param array $c An array of configuration values. Default = NULL
		 * @return object The instance of the form class.
		 */
		function form($c = NULL) {
			return form::load($c);	
		}
		
		/**
		 * Creates and returns the HTML of an icon for performing some action.
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 * @return string The HTML of the icon.
		 */
		function icon($c = NULL) {
			// Config
			if(!$c[url]) $c[url] = NULL; // You can pass a specific URL you want this icon to link to. Will create the URL by default momentarily.
			if(!$c[icon]) $c[icon] = NULL; // The CSS 'i-' class you want to apply to generate the icon. Example: 'add' would result in 'i-add'.
			if(!x($c[button])) $c[button] = 0; // Make this icon a 'button' icon with text
			if(!$c[text] and $c[button]) $c[text] = $c[tooltip_text]; // Text to display alongside the icon (required if a 'button')
			if(!x($c[inline])) $c[inline] = 1; // Is this an 'inline' icon?
			if(!x($c[tooltip])) $c[tooltip] = 1; // Show a 'tooltip' on hover?
			if(!$c[tooltip_text]) $c[tooltip_text] = $c[text]; // The tooltooltip text to show on hover (if $c[tooltip] == 1).
			if(!$c[attributes]) $c[attributes] = NULL; // An array of attributes to add to the link element. Example: array('class' => 'overlay','title' => 'My Title').
			if(!x($c[required])) $c[required] = 0; // Icon required. Show a faded, span icon if no $c[url].
		
			// Cached?
			#do we want this? so specific and frequently called that it's probably not worth it to cache it
			
			// Link class
			$link_class = NULL;
			if($c[inline]) $link_class .= ($link_class ? " " : "")."i-inline";
			if($c[tooltip] and $c[tooltip_text]) $link_class .= ($link_class ? " " : "")."tooltip";
			if($c[button]) $link_class .= ($link_class ? " " : "")."i-button";
			if(!$c[url] and $c[required]) $link_class .= ($link_class ? " " : "")."core-fade";
			if($c[attributes]['class']) {
				$link_class .= ($link_class ? " " : "").$c[attributes]['class'];
				unset($c[attributes]['class']);
			}
			$link_class_quotes = (strstr($link_class,"'") ? '"' : "'");
			// Icon class
			$icon_class = NULL;
			$icon_class .= ($icon_class ? " " : "")."i i-".$c[icon];
			if($c[text]) $icon_class .= ($icon_class ? " " : "")."i-text";
			$icon_class_quotes = (strstr($icon_class,"'") ? '"' : "'");
			
			// Link attributes
			$link_attributes = NULL;
			if(!$c[attributes][title] and $c[tooltip] and $c[tooltip_text]) $c[attributes][title] = string_encode($c[tooltip_text]);
			if($c[attributes]) {
				foreach($c[attributes] as $k => $v) {
					if(x($v)) {
						if($k == "title") $v = string_encode($v);
						$quote = (preg_match('/[^\\\]"/',$v) ? "'" : '"');
						$link_attributes .= " ".$k."=".$quote.$v.$quote;
					}
				}
			}
			
			// Button
			if($c[button]) {
				$span = "<span class=".$icon_class_quotes.$icon_class.$icon_class_quotes.">".$c[text]."</span>";
			}
			// Default
			else {
				$link_class .= ($link_class ? " " : "").$icon_class;
				$span = $c[text];
			}
			
			// HTML
			if($c[url] or $c[required]) {
				$html = "<".($c[url] ? "a href='".$c[url]."'" : "span")." class=".$link_class_quotes.$link_class.$link_class_quotes.$link_attributes.">".$span."</".($c[url] ? "a" : "span").">";
			}
			
			// Return
			return $html;
		}
		
		
		
		/**
		 * Sets session / cookie variables for given user and redirects (optional)			
		 * 
		 * @param int $id The id of the user you want to login as.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return boolean Whether or not we were able to log this user in.
		 */
		function login($id,$c = NULL) {
			// Config
			if(!x($c[source])) $c[source] = 'form'; // The source of the login: login, cookie, register, update
			if(!x($c[redirect])) $c[redirect] = NULL; // URL to redirect to after login
			if(!x($c[remember])) $c[remember] = 1; // 'Remember' user by saving a cookie.
			
			// Return - default value
			$return = false;
			
			// Get user
			$row = $this->db->f("SELECT * FROM users WHERE user_id = '".$id."'");
			if($row[user_id]) {
				// Logout
				if(u('id')) $this->logout();
					
				// Login
				$_SESSION['u'] = $row;
				$_SESSION['u']['id'] = $row[user_id];
				$_SESSION['u']['name'] = s($row[user_first_name]." ".$row[user_last_name]);
						
				// Cookie
				if($c[remember] == 1) {
					$this->cookie_save("u_id",$row[user_id],array('expires' => (time() + (86400 * 30))));
				}
				
				// Return value
				$return = true;
			}
				
			// Redirect
			if($c[redirect]) {
				$c[redirect] = str_replace('&amp;','&',$c[redirect]);
				redirect($c[redirect]);
			}
			
			// Return 
			return $return;
		}
		
		/**
		 * Logs out current user															
		 */
		function logout() {
			// Save session values that carry over
			$session = $_SESSION;
			unset($session[u]);
				
			// Delete cookie
			$this->cookie_delete("u_id");
			
			// Destroy session
			$this->session_destroy();
			
			// New session
			$this->session_start();
			
			// Restore session values that carry over
			if(!$session[session][id]) $session[session][id] = $_SESSION['session']['id']; // Use new session id (as we didn't have one previously)
			$session[domain] = $_SESSION['domain']; // Must use new 'domain' (defined in $this->session_start())
			foreach($session as $k => $v) $_SESSION[$k] = $v; // Apparently you're supposed to set session like this, not update the whole $_SESSION variable: http://stackoverflow.com/a/415034/502311
			//$_SESSION = $session;
		}
		
		/**
		 * Returns HTML of a basic login form.          
		 * 
		 * @param array $c An array of configuration values. Default = NULL
		 * @return string The HTML of the login form.
		 */
		function login_form($c = NULL) {
			// Config
			if(!x($c[https])) $c[https] = 0; // Use HTTPS
			if(!x($c[cookie])) $c[cookie] = 1; // Show checkbox for storing u.id cookie once the login
			if(!$c[button]) $c[button] = "Login"; // Login button text
			if(!$c[label_user]) $c[label_user] = "Username";
			if(!$c[label_password]) $c[label_password] = "Password";
			if(!x($c[redirect])) { // Redirect
				if($_GET['redirect']) $c[redirect] = htmlspecialchars(urldecode($_GET['redirect']));
				else {
					for($x = 0;$x <= 10;$x++) {
						if($_SESSION['urls'][$x] and !strstr($_SESSION['urls'][$x],'login')) {
							$c[redirect] = $_SESSION['urls'][$x];
							break;
						}
					}
				}
				if(!x($c[redirect])) $c[redirect] = DOMAIN;
			}
			// HTTPS
			if($c[https] == 1 and $_SERVER["HTTPS"] != "on") r("https".substr(URL,4));
			
			// Login inactve
			if(x(g('config.login.active')) and g('config.login.active') != 1) $text .= "
	<div class='red'>Login is currently disabled</div>";
			// Login active
			else {
				// Form
				$form_c = array(
					'name' => 'login',
					'class' => 'login-form form-table',
					'container' => array(
						'class' => 'login-form-container'
					),
					'row' => array(
						'class' => 'login-form-row'
					),
					'input' => array(
						'class' => 'login-form-input'
					),
					'label' => array(
						'class' => 'login-form-label'
					),
					'field' => array(
						'class' => 'login-form-field'
					),
					'redirect' => $c[redirect],
				);
				// User
				$form_inputs = array(
					array(
						'type' => 'text',
						'name' => 'user',
						'autocorrect' => 'off',
						'autocapitalize' => 'off',
						'class' => 'login-form-input-user',
						'validate' => array(
							'required' => 1,
						),
						'label' => array(
							'value' => $c[label_user],
							'class' => 'login-form-label-user'
						),
						'field' => array(
							'class' => 'login-form-field-user'
						)
					),
					array(
						'type' => 'password',
						'name' => 'password',
						'autocorrect' => 'off',
						'autocapitalize' => 'off',
						'class' => 'login-form-input-password',
						'validate' => array(
							'required' => 1,
						),
						'label' => array(
							'value' => $c[label_password],
							'class' => 'login-form-label-password'
						),
						'field' => array(
							'class' => 'login-form-field-password'
						)
					)
				);
				// Remember
				if($c[cookie] == 1) $form_inputs[] = array(
					'type' => 'checkbox',
					'name' => 'remember',
					'options' => array(
						1 => "<span class='core-tiny'>Remember me</span>"
					),
					'value' => 1,
					'class' => 'login-form-input-cookie',
					'label' => array(
						'class' => 'login-form-label-cookie'
					),
					'field' => array(
						'class' => 'login-form-field-cookie'
					)
				);
				// Submit
				$form_inputs[] = array(
					'type' => 'submit',
					'class' => 'login-form-input-submit',
					'label' => array(
						'class' => 'login-form-label-submit'
					),
					'field' => array(
						'class' => 'login-form-field-submit'
					),
					'value' => $c[button]
				);
				// Action
				$form_inputs[] = array(
					'type' => 'hidden',
					'name' => 'process_action',
					'value' => 'login'
				);
				
				// Display
				$form = form::load($form_c);
				$form->inputs($form_inputs);
				$text = $form->render();
			}
			
			// Return
			return $text;
		}
		
		/**
		 * Starts session.
		 */
		function session_start() {
			// Name - means we can share sessions across sub-domains: http://stackoverflow.com/a/1457582
			session_name(md5(DOMAIN));
			$cookie = str_replace(array('http://','https://','www.'),'',DOMAIN);
			list($cookie,$extra) = explode('/',$cookie);
			$cookie = ".".$cookie;
			session_set_cookie_params(0, '/', $cookie);
			
			// Start session
			session_start();

			// Session is from a different site
			if(
				// No domain defined, but session exists
				(
					count($_SESSION) > 0
					and !$_SESSION['domain']
				)
				// Domain defined, but wrong domain
				or (
					$_SESSION['domain']
					and $_SESSION['domain'] != DOMAIN
					and $_SESSION['domain'] != SDOMAIN
				)
			) {
				// Destory session
				$this->session_destroy();
				
				// Start session
				session_start();
			}
			
			// Session info
			if(!$_SESSION['session']['id']) $_SESSION['session']['id'] = session_id(); // An id to use across sessions. We'll restore this to the previous value in $this->logout() if one existed.
			
			// Domain
			$_SESSION['domain'] = DOMAIN;
		}
		
		/**
		 * Destroys session.
		 *
		 * http://php.net/manual/en/function.session-destroy.php
		 * http://www.php.net/manual/en/function.session-unset.php#107089
		 */
		function session_destroy() {
			// Must be started to be destroyed
			if(session_id() == '') $this->session_start();
			
			// Empty array
			$_SESSION = array();
			// Unset all values
			session_unset();
			// Delete session cookie (if exists)
			if(ini_get("session.use_cookies")) {
				$params = session_get_cookie_params();
				setcookie(session_name(),'',time() - 42000,$params["path"], $params["domain"],$params["secure"], $params["httponly"]);
			}
			// Destory session
			session_destroy();
		}
		
		/**
		 * Stores a cookie of the given $key with the given value.
		 * 
		 * @param string The key of the cookie you want to stare.
		 * @param string The value of the cookie you want to store.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function cookie_get($key,$c = NULL) {
			// Error
			if(!$key) return;
			
			// Domain key
			$key .= "_".$this->cookie_key();
			
			// Value
			$value = $_COOKIE[$key];
			
			// Return
			return $value;
		}
		
		/**
		 * Stores a cookie of the given $key with the given value.
		 * 
		 * @param string The key of the cookie you want to stare.
		 * @param string The value of the cookie you want to store.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function cookie_save($key,$value,$c = NULL) {
			// Error
			if(!$key) return;
			
			// Config
			if(!x($c[expires])) $c[expires] = 0;
			if(!x($c[path])) $c[path] = g('config.cookie.path');
			if(!x($c[domain])) {
				//$c[domain] = rtrim(str_replace(array('http://','https://','www'),'',url_domain(URL)),'/'); // Doesn't work for dev URL's which is the only reason we wanted to use this.
				$c[domain] = NULL;
			}
			if(!x($c[https])) $c[https] = 0;
			if(!x($c[httponly])) $c[httponly] = 1;
			
			// Domain key
			$key .= "_".$this->cookie_key();
			
			// Set
			setcookie($key,$value,$c[expires],$c[path],$c[domain],$c[https],$c[httponly]); // Note: nothing can be outputted/printed before this is called
		}
		
		/**
		 * Deletes cookie stored under the given $key.
		 * 
		 * @param string The key of the cookie you want to delete.
		 */
		function cookie_delete($key) {
			// Set to empty and expired (1 = unix timestamp in 1970)
			$this->cookie_save($key,"",array('expires' => 1));
		}
		
		/**
		 * Returns key unique to this domain for use with storing/retrieving domain specific cookies.
		 * 
		 * Couldn't get the setcookie() 'domain' value to work on dev sites. This is a workaround.
		 *
		 * @param string The cookie key unique to this domain.
		 */
		function cookie_key() {
			// Key
			$key = md5(SERVER);
			
			// Domain
			/*$domain = DOMAIN;
			
			// Make http (if https)
			if(substr($domain,0,8) == "https://") $domain = "http://".substr($domain,8);
			
			// Key
			$key = md5($domain);*/
			
			// Return
			return $key;
		}
	}
}
?>