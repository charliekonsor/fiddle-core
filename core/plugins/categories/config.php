<?php
/************************************************************************************/
/************************************** Config **************************************/
/************************************************************************************/
$config = array(
	'name' => 'Categories',
	'single' => 'Category',
	'plural' => 'Categories',
	'icon' => 'category',
	'db' => array(
		'table' => 'items_categories',
		'id' => 'id',
		'parent_module' => 'module_code',
		'parent_id' => 'item_id',
		'created' => 'item_created',
		'updated' => 'item_updated'
	)
);
?>