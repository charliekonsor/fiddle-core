<?php
if(!class_exists('form_input_collapsed_end_framework',false)) {
	/**
	 * Creates a collapsed_end input
	 */
	class form_input_collapsed_end_framework extends form_input_framework {
		/**
		 * Loads class. Needed so we can use debug_backtrace() to get this class name in PHP < 5.3 in $form_input->load().
		 */
		static function load($c = NULL) {
			return parent::load(NULL,NULL,NULL,$c);
		}
		
		/**
		 * Construct
		 */
		function __construct($label = NULL,$name = NULL,$value = NULL,$c = NULL) {
			$this->form_input_collapsed_end_framework($label,$name,$value,$c);
		}
		function form_input_collapsed_end_framework($label = NULL,$name = NULL,$value = NULL,$c = NULL) {
			// Construct
			parent::__construct($label,$name,$value,$c);
		}
		
		/**
		 * Renders the HTML for the input's 'label', including the container.
		 *
		 * @param object $form The form object this input is a part of.
		 * @param array $c An array of config values. Default = NULL
		 * @return string The HTML of the input's label, including the container.
		 */
		function html_row($form,$c = NULL) {
			// HTML (if collapsed section begun)
			if($form->collapsed) $html = "
	</div>
</div>";
			
			// Store that collapsed section has been cosed
			$form->collapsed = 0;
			
			// Return
			return $html;
		}
	}
}
?>