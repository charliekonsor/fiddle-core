<?php
if(!class_exists('geo',false)) {
	/**
	 * Handles geocoding and other location based information, using several geocoders.
	 *
	 * To Do
	 * - Make completely independent (don't use location zips table for zip lookup)
	 * - Different geocoder's have different Country Name values (USA vs United States)
	 * - Maybe use JSON instead of XML as it's quicker to deal with (can't simply have an array of $service_fields then unless all in array are on top level of json array)
	
	 * Old To Do
	 * - Google provides state code as state name
	 * - Geonames doesn't provide city or postal code
	 * - Yahoo missing county, only returns 'code' for state and country
	 * - Maybe pass timezones, elevation, continent, and other available info
	 *
	 * @package kraken\geo
	 */
	class geo {
		
		// Variables
		var $search,$search_method,$search_full;
		var $array = array();
		var $db = NULL;
		var $c = array(
			'latitude' => NULL, // If a latitude and longitude are passed and google is turned on we can use google places search
			'longitude' => NULL, // If a latitude and longitude are passed and google is turned on we can use google places search
			'radius' => 5, // Radius, in miles, to search for places (requires latitude/longitude to be passed) (google max: 31)
			'google' => 1, // Use the Google geocoder
			'google_version' => 3, // Which version of Google geocoder to use: 2, 3
			'yahoo' => 0, // Use the Yahoo geocoder // Has been deprecated in favor of a paid geocoder: http://developer.yahoo.com/boss/geo/
			'geonames' => 0, // Use the GeoNames geocoder // Not very good at addresses. Can do cities and some famous locations, but not a great geocoder.
			'all' => 0, // Return all results (not just the first) // Might want to move to search() function
			//'google_key' => 'ABQIAAAA3lLJgXkmhKRetLQKNT5VURRUmfUlQ7yf4yUdFHz0x3HjDQxGHhTywn6IHYcRaUMzkQOAf29FBYqalw', // Google API Key (google maps key)
			'google_key' => 'AIzaSyA9DdPVVzo8N0BdEVwOt9fR5M4YuKPgVmo', // Google API Key (general key)
			'yahoo_key' => 'YD-9G7bey8_JXxQP6rxl.fBFGgCdNjoDMACQA--', // Yahoo Maps App ID
			'geonames_key' => 1, // Don't need Geonames Key, but pass a value so it works in the $services loop
			'ipinfodb_key' => '0f2350da17a12c1af18cacc9d17d5d1185d8427ead477403a2916e58d7c1aff3', // Key to use when using IPInfoDB api
			'cache' => 1, // Cache
			'debug' => 0 // Debug
		);
		
		// Services
		var $services = array(
			'google',
			'yahoo',
			'geonames'
		);
		// Service Fields
		var $services_fields = array(
			'google_places' => array( // Places - http://code.google.com/apis/maps/documentation/places/
				'name' => 'name',
				'address_string' => 'vicinity',
				'latitude' => 'lat',
				'longitude' => 'lng',
			),
			'google_2' => array( // Google Maps V2 - http://code.google.com/apis/maps/documentation/geocoding/v2/
				'name' => 'address',
				'address' => 'address',
				'country' => 'CountryName',
				'country_code' => 'CountryNameCode',
				//'state' => '', // Only returns 'code'
				'state_code' => 'AdministrativeAreaName',
				'county' => 'SubAdministrativeAreaName',
				'city' => 'LocalityName',
				'zip' => 'PostalCodeNumber'
			),
			'google_3' => array( // Google Maps V3 - http://code.google.com/apis/maps/documentation/geocoding/ (most of the values pulled manually in parse_google())
				'name' => 'formatted_address'
			),
			'geonames' => array(
				'name' => 'name',
				'latitude' => 'latitude',
				'longitude' => 'longitude',
				'country' => 'countryName',
				'country_code' => 'countryCode',
				'state' => 'adminName1',
				'state_code' => 'adminCode1',
				'county' => 'adminName2',
				'accuracy' => 'score',
			),
			'yahoo' => array(
				'name' => 'name',
				'address' => 'line1',
				'latitude' => 'latitude',
				'longitude' => 'longitude',
				'country' => 'country',
				'country_code' => 'countrycode',
				'state' => 'state',
				'state_code' => 'statecode',
				'county' => 'county',
				'city' => 'city',
				'zip' => 'postal',
				'accuracy' => 'quality'
			)
		);
		
		// __construct()
		function __construct($search = NULL,$search_method = NULL,$c = NULL) {
			// Passed $c as 1st param: new geo($c);
			if(!$search_method and is_array($search)) {
				$c = $search;
				$search = NULL;
			}
			// Passed $c as 2nd param: new geo($search,$c);
			else if(!$c and is_array($search_method)) {
				$c = $search_method;
				$search_method = NULL;
			}
			// Version
			if(!$c[google_version] and $c[latitude] and $c[longitude]) $c[google_version] = "places";
			
			// Config
			if($c) $this->c = array_merge($this->c,$c);
			$this->c_string = cache_config($this->c);
			
			// Database
			$this->db = db::load();
			
			// Search new location
			if($search or $this->c[latitude] and $this->c[longitude]) $this->search($search,$search_method);
		}
		// __call()
		function __call($name,$arguments) {
			if($name == "search") $this->search_full = 1;
			else $this->search_full = 0;
		}
		
		// search() - tries to geocode given location
		function search($search = NULL,$search_method = NULL) {
			$f_r = function_speed("class:".__CLASS__."->".__FUNCTION__);
			
			// Latitude/Longitude - passed lat in 1st param, lng in 2nd
			if(is_numeric($search) and is_numeric($search_method)) {
				$search = $search.",".$search_method;
				$search_method = "reverse";
			}
			
			// Trim
			$this->search = trim($search);
			
			// Search method (must set to variable, even if NULL, in case we set it to something else before)
			$this->search_method = $search_method;
			
			// No location, use user's IP address
			if(!$this->search and !$this->c[latitude] and !$this->c[longitude]) $this->search = IP;
			
			// Search method
			if(!$this->search_method) {
				// IP Address (v4 of v6)
				if(preg_match('/^(?:(?>(?>([a-f0-9]{1,4})(?>:(?1)){7})|(?>(?!(?:.*[a-f0-9](?>:|$)){8,})((?1)(?>:(?1)){0,6})?::(?2)?))|(?>(?>(?>(?1)(?>:(?1)){5}:)|(?>(?!(?:.*[a-f0-9]:){6,})((?1)(?>:(?1)){0,4})?::(?>(?3):)?))?(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9]?[0-9])(?>\.(?4)){3}))$/iD',$this->search)) $this->search_method = "ip";
				// Zip (US only)
				else if(preg_match('/^[0-9]{5}$/',$this->search)) $this->search_method = "zip";
				// Latitude/Longitude
				else if(preg_match('/^-?[0-9]*\.?[0-9]*? ?, ?-?[0-9]*\.?[0-9]*?$/',$this->search)) $this->search_method = "reverse";
				// Default
				else $this->search_method = "text";
			}
			
			// Debug
			debug("Search term: ".$this->search,$this->c[debug]);
			debug("Search method: ".$this->search_method,$this->c[debug]);
			
			// Get cached location
			if($this->c[cache] == 1) {
				if($this->results = $this->cache_get()) {
					debug("Getting cached results",$this->c[debug]);
					//$_SESSION['geo'][$this->search] = $this->results; // Legacy
					
					// Return
					debug("Results:".return_array($this->results),$this->c[debug]);
					function_speed("class:".__CLASS__."->".__FUNCTION__,$f_r,1);
					return $this->results;
				}
			}
			
			// Search - IP
			if($this->search_method == "ip") $this->results = $this->geocode_ip($this->search);
			// Search - Zip
			if($this->search_method == "zip") {
				$this->results = $this->geocode_zip($this->search);
				if(!$this->results) $this->search_method = "text";
			}
			// Search - Latitude/Longitude
			if($this->search_method == "reverse") {
				list($latitude,$longitude) = explode(',',$this->search);
				$this->results = $this->geocode_reverse($latitude,$longitude);
			}
			// Search - Text
			if($this->search_method == "text") $this->results = $this->geocode_text($this->search);
			
			// Cache
			if($this->results) $this->cache_save();
			
			// Return
			debug("Results:".return_array($this->results),$this->c[debug]);
			function_speed("class:".__CLASS__."->".__FUNCTION__,$f_r);
			return $this->results;
		}
		
		// geocode_text() - takes text (address, place name, etc.), returns location information
		function geocode_text($q) {
			$f_r = function_speed("class:".__CLASS__."->".__FUNCTION__);
			$this->search = $q; // In case not called via search()
			$this->search_method = "text"; // In case not called via search()
			
			// Debug
			debug("<b>geocode_text($q);</b>",$this->c[debug]);
			debug("c:".return_array($this->c),$this->c[debug]);
		
			// Cached
			if($this->c[cache] == 1) {
				$array = $this->cache_get("text/".md5($q.$this->c_string));
				if($array[latitude] or $array[longitude]) {
					function_speed("class:".__CLASS__."->".__FUNCTION__,$f_r,1);
					return $array;
				}
			}
			
			// Services
			foreach($this->services as $service) {
				$url = NULL;
				if((!$this->results and !$return or $this->c[all] == 1) and $this->c[$service] == 1 and $this->c[$service.'_key']) {
					debug("service: ".$service,$this->c[debug]);
					
					// Google
					if($service == "google") {
						if($this->c[google_version] == "places") $url = "https://maps.googleapis.com/maps/api/place/search/xml?keyword=".urlencode($q)."&radius=".($this->c[radius] / 0.000621371192)."&location=".$this->c[latitude].",".$this->c[longitude]."&sensor=false&key=".$this->c[google_key]; // Google Places (radius in meters)
						else if($this->c[google_version] == 2 and $q) $url = "http://maps.google.com/maps/geo?q=".urlencode($q)."&output=xml&key=".$this->c[google_key]; // Google Maps V2
						else if($q) $url = "http://maps.googleapis.com/maps/api/geocode/xml?address=".urlencode($q)."&sensor=false"; // Google Maps V3
					}
					// Yahoo
					if($service == "yahoo" and $q) $url = "http://where.yahooapis.com/geocode?q=".urlencode($q)."&appid=".$this->c[yahoo_key];
					// Geonames
					if($service == "geonames" and $q) $url = "ws.geonames.org/search?q=".urlencode($q)."&maxRows=".($this->c[all] == 1 ? 20 : 1)."&style=FULL";
				
					if($url) {
						// Curl
						debug("url: ".$url,$this->c[debug]);
						$contents = $this->curl($url);
						if($contents) {
							debug("contents: <xmp>".$contents."</xmp>",$this->c[debug]);
						
							// Parse
							$parsed = $this->parse($contents,$service);
							if($parsed) {
								if($return) $return = array_merge($return,$parsed);
								else $return = $parsed;
							}
						}
					}
				}
			}
			
			// Cache
			$this->cache_save("text/".md5($q.$this->c_string),$return);
			
			// Return
			debug('results:'.return_array($return),$this->c[debug]);
			function_speed("class:".__CLASS__."->".__FUNCTION__,$f_r);
			return $return;
		}
		
		// geocode_reverse() - takes latitude and longitude, retuns location info
		function geocode_reverse($latitude,$longitude,$c = NULL) {
			$f_r = function_speed("class:".__CLASS__."->".__FUNCTION__);
			$this->search = $latitude.",".$longitude; // In case not called via search()
			$this->search_method = "reverse"; // In case not called via search()
			
			// Debug
			debug("<b>geocode_reverse($latitude,$longitude);</b>",$this->c[debug]);
			debug("c:".return_array($c),$this->c[debug]);
		
			// Cached
			if($this->c[cache] == 1) {
				$array = $this->cache_get("reverse/".md5($latitude.",".$longitude.$this->c_string));
				if($array[latitude] or $array[longitude]) {
					function_speed("class:".__CLASS__."->".__FUNCTION__,$f_r,1);
					return $array;
				}
			}
			
			foreach($this->services as $service) {
				if((!$this->results and !$return or $this->c[all] == 1) and $this->c[$service] == 1 and $this->c[$service.'_key']) {
					// Google
					if($service == "google") {
						if($this->c[google_version] == 2) $url = "http://maps.google.com/maps/geo?q=".$latitude.",".$longitude."&output=xml&sensor=false&key=".$this->c[google_key]; // Google Maps V2
						else $url = "http://maps.googleapis.com/maps/api/geocode/xml?address=".$latitude.",".$longitude."&sensor=false"; // Google Maps V3
					}
					// Yahoo
					if($service == "yahoo") $url = "http://where.yahooapis.com/geocode?q=".urlencode($q)."&appid=".$this->c[yahoo_key];
					// Geonames // Not yet supported
					//if($service == "geonames") $url = "ws.geonames.org/search?q=".urlencode($q)."&maxRows=".($this->c[all] == 1 ? 20 : 1)."&style=FULL";
				
					// Curl
					$contents = $this->curl($url);
					
					// Debug
					debug("url: ".$url,$this->c[debug]);
					debug("contents: <xmp>".$contents."</xmp>",$this->c[debug]);
					
					// Parse
					$parsed = $this->parse($contents,$service);
					if($return) $return = array_merge($return,$parsed);
					else $return = $parsed;
				}
			}
			
			// Cache
			$this->cache_save("reverse/".md5($latitude.",".$longitude.$this->c_string),$return);
			
			// Store
			if(!$this->search_full) $this->results = $return;
			
			// Return
			debug('results:'.return_array($return),$this->c[debug]);
			function_speed("class:".__CLASS__."->".__FUNCTION__,$f_r);
			return $return;
		}
		
		// geocode_zip() - takes in a zip code, returns location information
		function geocode_zip($zip) {
			$f_r = function_speed("class:".__CLASS__."->".__FUNCTION__);
			$this->search = $zip; // In case not called via search()
			$this->search_method = "zip"; // In case not called via search()
			
			// Debug
			debug("<b>geocode_zip($zip);</b>",$this->c[debug]);;
			
			// Zip
			debug("SELECT * FROM zips WHERE zip_name = '".a($zip)."'",$this->c[debug]);
			$row = $this->db->f("SELECT * FROM zips WHERE zip_name = '".a($zip)."'");
			if($row[zip_id]) {
				// Array
				$array = array(
					'latitude' => $row[zip_latitude],
					'longitude' => $row[zip_longitude],
					'zip' => $row[zip_name],
					'city' => $row[zip_city],
					'state_code' => $row[state_code],
					'country_code' => $row[country_code],
					'loaded' => 1
				);
			}
			
			// Standardize
			$array = $this->standardize($array);
			
			// Array (if returning 'all')
			if($this->c[all] == 1) $return[] = $array;
			else $return = $array;
			
			// Store
			if(!$this->search_full) $this->results = $return;
			
			// Return
			debug("geocode_zip() results:".return_array($return),$this->c[debug]);
			function_speed("class:".__CLASS__."->".__FUNCTION__,$f_r);
			return $return;
		}
		
		// geocode_ip() - takes ip address, returns array of location information
		function geocode_ip($ip = NULL,$c = NULL) {
			$f_r = function_speed("class:".__CLASS__."->".__FUNCTION__);
			$this->search = $ip; // In case not called via search()
			$this->search_method = "ip"; // In case not called via search()
			if(!$ip) $ip = IP; // Default to current user's IP address
		
			// Debug
			debug("<b>geocode_ip($ip);</b>",$this->c[debug]);;
		
			// Cached
			if($this->c[cache] == 1) {
				$array = $this->cache_get("ip/".md5($ip.$this->c_string));
				if($array[latitude] or $array[longitude]) {
					debug("Returning cached array:".return_array($array),$this->c[debug]);
					function_speed("class:".__CLASS__."->".__FUNCTION__,$f_r,1);
					return $array;
				}
			}
			
			// Get Info
			$contents = $this->curl("http://api.ipinfodb.com/v3/ip-city/?key=".$this->c[ipinfodb_key]."&ip=".$ip."&format=json");
			$results = json_decode($contents);
			debug("url: http://api.ipinfodb.com/v3/ip-city/?key=".$this->c[ipinfodb_key]."&ip=".$ip."&format=json",$this->c[debug]);
			debug("raw contents: ".$contents,$this->c[debug]);
			debug("json object:".return_array($results),$this->c[debug]);
			
			// Format Array
			$array = array(
				'ip' => $ip,
				'city' => ucwords(strtolower($results->cityName)),
				'state' => ucwords(strtolower($results->regionName)),
				'zip' => $results->zipCode,
				'country' => ucwords(strtolower($results->countryName)),
				'country_code' => $results->countryCode,
				'latitude' => $results->latitude,
				'longitude' => $results->longitude,
				'timezone' => $results->timeZone
			);
			foreach($array as $k => $v) {
				if($v == "-") unset($array[$k]);	
			}
			
			// Standardize
			$array = $this->standardize($array);
			
			// Array (if returning 'all')
			if($this->c[all] == 1) $return[] = $array;
			else $return = $array;
		
			// Cache
			$this->cache_save("ip/".md5($ip.$this->c_string),$return);
			
			// Return
			debug("geocode_ip() results:".return_array($return),$this->c[debug]);
			function_speed("class:".__CLASS__."->".__FUNCTION__,$f_r);
			return $return;
		}
		
		// curl() - curl url, return results
		function curl($url) {
			//$contents = curl($url); // This throws an 'Incorrect Key' error with google
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$contents = curl_exec ($ch);
			curl_close ($ch);
			
			return $contents;
		}
		
		// distance() - calculates distance between 2 given lat/lng pairs
		function distance($lat1,$lon1,$lat2,$lon2,$c = NULL) {
			if(!$c[unit]) $c[unit] = "m"; // Unit to return distance in: m [default] (miles), k (kilometers), n (nautical miles)
			$c[unit] = strtolower(substr($c[unit],0,1));
			
			$theta = $lon1 - $lon2; 
			$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta)); 
			$dist = acos($dist); 
			$dist = rad2deg($dist); 
			$miles = $dist * 60 * 1.1515;	
			
			// Convert
			$return = $miles;
			$c[unit] = substr(strtolower($c[unit]),0,1);
			if($c[unit] == "k") $return = ($return * 1.609344);
			if($c[unit] == "n") $return = ($return * 0.8684);
			
			return $return;
		}
		
		// parse() - parses results
		function parse($contents,$service) {
			// Fields
			$fields = $this->services_fields[$service.($this->c[$service.'_version'] ? "_".$this->c[$service.'_version'] : "")];
			// Results
			if($service == "google") {
				if($this->c[google_version] == "places") preg_match_all("/<result(.*?)<\/result>/is",$contents,$points); // Google Places
				else if($this->c[google_version] == 2) preg_match_all("/<Placemark(.*?)<\/Placemark>/is",$contents,$points); // Google Maps V2
				else preg_match_all("/<result(.*?)<\/result>/is",$contents,$points); // Google Maps V3
			}
			if($service == "yahoo") preg_match_all("/<Result(.*?)<\/Result>/is",$contents,$points);
			if($service == "geonames") preg_match_all("/<geoname>(.*?)<\/geoname>/is",$contents,$points);
					
			if(count($points[0]) > 0) {
				foreach($points[1] as $x => $point) {
					if(!$this->results and !$return or $this->c[all] == 1) {
						$array = NULL;
						$array[geocoder] = $service;
						$array[geocoder_version] = $this->c[$service.'_version'];
						
						// Basic
						foreach($fields as $k => $v) {
							preg_match('/<'.$v.'>(.*?)<\/'.$v.'>/s',$point,$temp[$k]);
							$array[$k] = $temp[$k][1];
						}
						// Custom
						if($service == "google") $array = $this->parse_google($point,$array);
						if($service == "yahoo") $array = $this->parse_yahoo($point,$array);
						if($service == "geonames") $array = $this->parse_geonames($point,$array);
	
						// Standardize
						$array = $this->standardize($array);
						
						// Return
						if($array and $this->c[all] == 1) $return[] = $array;
						else $return = $array;
					}
				}
			}
			else debug("no points found",$this->c[debug]);
			
			// Store
			if(!$this->search_full) $this->results = $return;
			
			// Return
			return $return;
		}
		
		// parse_google() - parses custom info from google result
		function parse_google($point,$array) {
			
			// Google Places
			if($this->c[google_version] == "places") {
				// Place
				preg_match('/<id>(.*?)<\/id>/s',$point,$id);
				$array[google_places_id] = $id[1];
				preg_match('/<reference>(.*?)<\/reference>/s',$point,$reference);
				$array[google_places_reference] = $reference[1];
				
				// Details // This means a lot of new requests, might want to skip
				$contents = $this->curl("https://maps.googleapis.com/maps/api/place/details/xml?reference=".$reference[1]."&sensor=false&key=".$this->c[google_key]);
				
				// Address string
				preg_match('/<formatted_address>(.*?)<\/formatted_address>/si',$contents,$address);
				if($address[1]) $array[address_string] = $address[1];
				
				// Address parts
				preg_match_all('/<address_component>(.*?)<\/address_component>/si',$contents,$parts);
				if(count($parts[0]) > 0) {
					foreach($parts[1] as $x => $part) {
						preg_match('/<long_name>(.*?)<\/long_name>/s',$part,$long);
						preg_match('/<short_name>(.*?)<\/short_name>/s',$part,$short);
						preg_match('/<type>(.*?)<\/type>/s',$part,$type);
						if($type[1] == "street_number") $array[address] .= ($array[address] ? " " : "").$long[1];
						if($type[1] == "route") $array[address] .= ($array[address] ? " " : "").$long[1];
						if($type[1] == "postal_code") $array[zip] = $long[1];
						if($type[1] == "locality") $array[city] = $long[1];
						if($type[1] == "administrative_area_level_2") $array[county] = $long[1];
						if($type[1] == "administrative_area_level_1") $array[state_code] = $long[1];
						if($type[1] == "country") $array[country_code] = $long[1];
					}
				}
				
				// URL
				preg_match('/<website>(.*?)<\/website>/si',$contents,$website);
				if($website[1]) $array[url] = $website[1];
				// Phone
				preg_match('/<formatted_phone_number>(.*?)<\/formatted_phone_number>/si',$contents,$phone);
				if($phone[1]) $array[phone] = $phone[1];
				// Google Places URL
				preg_match('/<url>(.*?)<\/url>/si',$contents,$url);
				if($url[1]) $array[google_places_url] = $url[1];
				
				// Accuracy
				$distance = $this->distance($array[latitude],$array[longitude],$this->c[latitude],$this->c[longitude]);
				if($distance < 1) $array[accuracy] = 10;
				else if($distance < 2) $array[accuracy] = 9;
				else if($distance < 5) $array[accuracy] = 8;
				else if($distance < 10) $array[accuracy] = 7;
				else if($distance < 20) $array[accuracy] = 6;
				else $array[accuracy] = 5;
			}
			// Google Maps V2 // http://code.google.com/apis/maps/documentation/geocoding/v2/
			else if($this->c[google_version] == 2) {
				// Latitude / Longitude
				preg_match('/<coordinates>(.*?)<\/coordinates>/s',$point,$coordinates);
				list($longitude,$latitude) = explode(',',$coordinates[1]);
				$array[latitude] = $latitude;
				$array[longitude] = $longitude;
				
				// Accuracy
				preg_match('/AddressDetails Accuracy="(.*?)"/',$point,$accuracy);
				$array[accuracy] = round($accuracy[1] * (10 / 9),2); // Out of 9, we want out of 10*/
			}
			
			// Google Maps V3 // http://code.google.com/apis/maps/documentation/geocoding/
			else {
				preg_match_all('/<address_component>(.*?)<\/address_component>/s',$point,$matches);
				if(count($matches[0]) > 0) {
					foreach($matches[0] as $x => $match) {
						preg_match('/<long_name>(.*?)<\/long_name>/',$match,$value);
						preg_match('/<short_name>(.*?)<\/short_name>/',$match,$code);
						preg_match_all('/<type>(.*?)<\/type>/',$match,$types);
						foreach($types[1] as $y => $type) {
							if(in_array($type,array("point_of_interest","establishment"))) $array[name] = $value[1];
							if($type == "street_number") $array[address] .= ($array[address] ? " " : "").$value[1];
							if($type == "route") $array[address] .= ($array[address] ? " " : "").$value[1];
							if($type == "postal_code") $array[zip] = $value[1];
							if($type == "locality") $array[city] = $value[1];
							if($type == "administrative_area_level_2") $array[county] = $value[1];
							if($type == "administrative_area_level_1") {
								$array[state] = $value[1];
								$array[state_code] = $code[1];
							}
							if($type == "country") {
								$array[country] = $value[1];
								$array[country_code] = $code[1];
							}
						}
					}
				}
						
				// Latituce / Longitude
				preg_match('/<location>(.*?)<\/location>/s',$point,$location);
				preg_match('/<lat>(.*?)<\/lat>/',$location[1],$latitude);
				preg_match('/<lng>(.*?)<\/lng>/',$location[1],$longitude);
				$array[latitude] = $latitude[1];
				$array[longitude] = $longitude[1];
				
				// Accuracy
				list($start,$end) = explode('<formatted_address>',$point);
				preg_match_all('/<type>(.*?)<\/type>(.*?)/',$start,$types);
				foreach($types[1] as $x => $type) {
					if(in_array($type,array('street_address','route','intersection','premise','subpremise','establishment'))) $array[accuracy] = 10;
					else if(in_array($type,array('point_of_interest','park','zoo'))) $array[accuracy] = 9;
					else if(in_array($type,array('sublocality','neighborhood','airport','natural_feature'))) $array[accuracy] = 8;
					else if(in_array($type,array('locality','postal_code'))) $array[accuracy] = 7;
					else if(in_array($type,array('administrative_area_level_3','colloquial_area'))) $array[accuracy] = 5;
					else if(in_array($type,array('administrative_area_level_2','political'))) $array[accuracy] = 4;
					else if(in_array($type,array('administrative_area_level_1'))) $array[accuracy] = 3;
					else if(in_array($type,array('country'))) $array[accuracy] = 2;
					else if(in_array($type,array('country'))) $array[accuracy] = 1;
					if($array[accuracy]) break;
				}
			}
			
			return $array;
		}
		
		// parse_geonames() - parses custom info from geonames result
		function parse_geonames($point,$array) {
			// Accuracy
			$array[accuracy] = round($array[accuracy] * 10,3); // Out of 1, we want out of 10
			
			return $array;
		}
		
		// parse_yahoo() - parses custom info from yahoo result
		function parse_yahoo($point,$array) {
			// Accuracy
			$array[accuracy] = $array[accuracy] / 10; // Out of 100, we want out of 10
			// Name
			if(!$array[name]) {
				if($array[address]) $array[name] .= ($array[name] ? ", " : "").$array[address];	
				if($array[city]) $array[name] .= ($array[name] ? ", " : "").$array[city];
				if($array[state]) $array[name] .= ($array[name] ? ", " : "").$array[state];
				if($array[country] and $array[country_code] != "US") $array[name] .= ($array[name] ? ", " : "").$array[country];
			}
			
			return $array;
		}
		
		// standardize() - standardize resuls
		function standardize($array) {	
			// Have results
			if($array[latitude] and $array[longitude]) {
				// Tweaks
				if($array[country_code] == "UK") $array[country_code] = "GB"; // UK (United Kingdom) should be GB (official ISO 3166 country code)
				
				// Address
				if(!$array[address_string]) {
					if($array[address]) $array[address_string] .= $array[address];
					if($array[city]) $array[address_string] .= ($array[address_string] ? ", " : "").$array[city];
					if($array[state]) $array[address_string] .= ($array[address_string] ? ", " : "").$array[state];
					if($array[country_code]) $array[address_string] .= ($array[address_string] ? ", " : "").$array[country_code];
				}
				
				// Legacy
				/*$array[lat] = $array[latitude];
				$array[lng] = $array[longitude];
				$array[region] = $array[state];
				$array[region_code] = $array[state_code];*/
		
				// Search Info
				$array[search] = $this->search;
				$array[search_method] = $this->search_method;
		
				// Return
				return $array;
			}
		}
		
		// v() - returns given key's value (if no key, returns entire array) // doen't really with with array of results, need to find way to do that, maybe can pass an array as first param, if no array passed in first param (it would be key instead) we use the $this->results first item
		function v($key = NULL) {
			// Key changes - fix commonly used incorrect key names
			if($key == "country_name") $key = "country";
			if($key == "state_name") $key = "state";
			if($key == "county_name") $key = "county";
			if($key == "city_name") $key = "city";
			if($key == "postal_code") $key = "zip";
			if($key == "lat") $key = "latitude";
			if($key == "lng") $key = "longitude";
			
			// Array
			if(!$key) return $this->a();
			// Return saved value
			else if($value = $this->results[$key]) return $value;
			// Get new value
			else {
				// Country
				if($key == "country") {
					if($country_code = $this->v('country_code')) $row = $this->db->f("SELECT country_name FROM countries WHERE country_code = '".$country_code."'");
					$value = s($row[country_name]);
				}
				// State
				else if($key == "state") {
					if($state_code = $this->v('state_code')) $row = $this->db->f("SELECT state_name FROM states WHERE state_code = '".$state_code."'".($this->v('country_code') ? " AND country_code = '".$this->v('country_code')."'" : ""));
					else if($zip = $this->v('zip') and preg_match('/^[0-9]{5}$/',$zip)) $row = $this->db->f("SELECT s.state_name FROM zips z JOIN states s ON z.state_code = s.state_code WHERE z.zip_name = '".a($zip)."'");
					$value = s($row[state_name]);
				}
				// Country code
				else if($key == "country_code") {
					if($country = $this->results[country]) $row = $this->db->f("SELECT country_code FROM countries WHERE country_name = '".a($country)."'");
					else if($zip = $this->v('zip') and preg_match('/^[0-9]{5}$/',$zip)) $row = $this->db->f("SELECT zip_country country_code FROM zips WHERE zip_name = '".a($zip)."'");
					$value = s($row[country_code]);
				}
				// State code
				else if($key == "state_code") {
					if($state = $this->results[state]) $row = $this->db->f("SELECT state_code FROM states WHERE state_name = '".a($state)."'");
					else if($zip = $this->v('zip')) $row = $this->db->f("SELECT state_code FROM zips WHERE zip_name = '".a($zip)."'");
					$value = s($row[state_code]);
				}
				// City
				else if($key == "city") {
					if($zip = $this->v('zip') and preg_match('/^[0-9]{5}$/',$zip)) $row = $this->db->f("SELECT zip_city FROM zips WHERE zip_name = '".a($zip)."'");
					$value = s($row[zip_city]);
				}
				// Currency
				else if($key == "currency") {
					if($country_code = $this->v('country_code')) $row = $this->db->f("SELECT currency_code FROM countries_currencies WHERE country_code = '".$country_code."' AND currency_default = 1");
					$value = s($row[currency_code]);
				}
				
				if($value) {
					// Save value
					$this->results[$key] = $value;
					
					// Cache
					$this->cache_save();
					
					// Return
					return $value;
				}
			}
		}
		
		// results() - returns results array
		function results() {
			return $this->results;
		}
		
		/**
		 * Caches data for future use.
		 *
		 * @param string $name The specific cache name you want to save the cache under. Defaults to the search / search  method we're using.
		 * @param array $data An array of data to cache.
		 */
		function cache_save($name = NULL,$data = NULL) {
			// Error
			if(!$this->c[cache]) return;
			
			// Default name
			if(!$name) {
				$name = md5($this->search.$this->c_string);
				$data = $this->results;
			}
			
			// Prefix
			$name = 'geocode/'.$name;
			
			// Cache
			cache_save($name,$data);
		}
		
		/**
		 * Retrieves cached values (if vailable).
		 *
		 * @param string $name The specific cache name you're looking for. Defaults to the search / search  method we're using.
		 * @return array An array of cached values.
		 */
		function cache_get($name = NULL) {
			// Error
			if(!$this->c[cache]) return;
			
			// Default name
			if(!$name) $name = 'geocode/'.$this->search_method.'/'.md5($this->search.$this->c_string);
			
			// Prefix
			$name = 'geocode/'.$name;
			
			// Get
			$data = cache_get($name);
			
			// Return
			return $data;
		}
	}
}
?>