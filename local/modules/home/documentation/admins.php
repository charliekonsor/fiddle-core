<?php
print "
<h1>".$this->v('name')."</h1>
The home page is the first page users will see when they visit your site. As such, what it shows is very important.  That's why the home page has it's own section in the admin and isn't included in the Pages module.  However, you'll use it almost the same as you would the pages module (<a href='".D."?ajax_module=pages&amp;ajax_action=documentation' class='overlay'>click here</a> for help with Pages).<br /><br />

<em>Home page form</em><br />
<img src='".DOMAIN.$this->v('path')."documentation/images/form.png' alt='' width='650' /><br /><br />

The major difference between a 'Page' and the 'Home' page is the home page can have more than one content area which you can edit using a WYSIWYG (see the <a href='".D."?ajax_module=pages&amp;ajax_action=documentation' class='overlay'>Pages Help</a> for more info on the WYSIWYG).  By default, it has just one editable: 'Text'. However, if the design your using has several, separate editable sections of content on the home page, you may see more than one WYSIWYG when editing the home page, each of which will correspond to a section of the home page.<br /><br />

It's also worth noting that there may be some content on the home page you can't edit here. For example, you may have a 'carousel' on the top of the home page which displays different 'slides' of content.  These special pieces of content are usually editable in another area of the admin (you edit the carousel in the 'Carousels' section for example).<br /><br />

".$this->documentation_plugins();

?>