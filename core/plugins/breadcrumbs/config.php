<?php
/************************************************************************************/
/************************************** Config **************************************/
/************************************************************************************/
$config = array(
	'default' => 1,
	'name' => 'Breadcrumbs',
	'single' => 'Breadcrumb',
	'plural' => 'Breadcrumbs',
	'icon' => 'breadcrumbs',
	'settings' => array(
		// Defaults
		'separator' => ' > ',
		'limit' => 0, // Characters to limit each level's string to (0 = don't limit)
	),
);
?>