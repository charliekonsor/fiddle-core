<?php
if(!class_exists('core_framework',false)) {
	/**
	 * Loads core classes and creates methods for accessing them.
	 *
	 * @package kraken
	 */
	class core_framework extends core {
		/**
		 * Constructs the class
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function __construct($c = NULL) {
			self::core_framework($c);
		}
		function core_framework($c = NULL) {
			// Parent
			parent::__construct($c);
		}
		
		/**
		 * Sets session / cookie variables for given user and redirects (optional)			
		 * 
		 * @param int $id The id of the user you want to login as.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return boolean Whether or not we were able to log this user in.
		 */
		function login($id,$c = NULL) {
			// Config
			if(!x($c[source])) $c[source] = 'login'; // The source of the login: login, cookie, register, update
			if(!x($c[redirect])) $c[redirect] = NULL; // URL to redirect to after login
			if(!x($c[remember])) $c[remember] = 1; // 'Remember' user by saving a cookie.
			
			// Return - default value
			$return = false;
			
			// Get user
			$module = 'users';
			$item = item::load('users',$id);
			if($item->id) {
				// Logout
				if(u('id')) $this->logout();
			
				// Registered Type
				if(m($module.'.types.'.$item->type.'.settings.registered') == 1) {
					// User info
					$_SESSION['u'] = $item->a();
					// Registered
					$_SESSION['u']['registered'] = 1;
					// Admin
					$_SESSION['u']['admin'] = m($module.'.types.'.$item->type.'.settings.admin');
					
					// External ID's
					/*$sql1 = q("SELECT * FROM users_external WHERE user_id = '".$item->id."'");
					while($qry1 = f($sql1)) {
						$_SESSION['u']['external'][$qry1[external_site]] = option($qry1[external_session]);
						$_SESSION['u']['external'][$qry1[external_site]]['id'] = $qry1[external_id];
					}*/
				}
			
				// User id cookie
				if($c[remember] == 1) {
					$this->cookie_save("u_id",$item->id,array('expires' => (time() + (86400 * 30))));
				}
				
				// IP
				if(m('users_ips') and $c[source]) {
					$ip_item = item::load('users_ips','new');
					$ip_values = array(
						'user_id' => $item->id,
						'ip' => IP,
						'ip_source' => $c[source],
					);
					$ip_id = $ip_item->save($ip_values);
					
					$_SESSION['u']['ip_id'] = $ip_id;
				}
				
				// Return value
				$return = true;
			}
			
			// Redirect
			if($c[redirect]) {
				$c[redirect] = str_replace('&amp;','&',$c[redirect]);
				redirect($c[redirect]);
			}
			
			// Return 
			return $return;
		}
		
		/**
		 * Logs out current user															
		 */
		function logout() {
			// Parent
			parent::logout();
			
			// External
			$_SESSION['HA::STORE'] = NULL;
		}
		
		/**
		 * Returns HTML of a basic login form.          
		 * 
		 * @param array $c An array of configuration values. Default = NULL
		 * @return string The HTML of the login form.
		 */
		function login_form($c = NULL) {
			// Users module
			$module_class = module::load('users');
			$field = $module_class->login_field();
			
			// Config
			if(!x($c[https])) $c[https] = 0; // Use HTTPS
			if(!x($c[lost])) $c[lost] = 1; // Show link for retrieving lost username/password
			if(!x($c[cookie])) $c[cookie] = 1; // Show checkbox for storing u.id cookie once the login
			if(!x($c[register])) $c[register] = 1; // Show button for registering
			if(!$c[button_login]) $c[button_login] = "Login"; // Login button text
			if(!$c[button_register]) $c[button_register] = "Sign Up!"; // Registration button text
			if(!$c[label_user]) $c[label_user] = $module_class->login_label();
			if(!$c[label_password]) $c[label_password] = "Password";
			if(!$c[external]) $c[external] = m('settings.login.external');
			if(!x($c[redirect])) { // Redirect
				if($_GET['redirect']) $c[redirect] = htmlspecialchars(urldecode($_GET['redirect']));
				else {
					for($x = 0;$x <= 10;$x++) {
						if($_SESSION['urls'][$x] and !strstr($_SESSION['urls'][$x],'login')) {
							$c[redirect] = $_SESSION['urls'][$x];
							break;
						}
					}
				}
				if(!x($c[redirect])) $c[redirect] = DOMAIN;
			}
			// HTTPS
			if($c[https] == 1 and $_SERVER["HTTPS"] != "on") r("https".substr($_SESSION['urls'][0],4));
			
			// Login inactve
			if(x(g('config.login.active')) and g('config.login.active') != 1) {
				$text .= "
	<div class='center red'>Login is currently disabled</div>";
			}
			// Login active (or admin)
			if(!x(g('config.login.active')) or g('config.login.active') == 1) {
				$text .= "
	<div class='login-container'>";
	
				// Form
				$form_c = array(
					'name' => 'login',
					'class' => 'login-form form-table',
					'container' => array(
						'class' => 'login-form-container'
					),
					'row' => array(
						'class' => 'login-form-row'
					),
					'input' => array(
						'class' => 'login-form-input'
					),
					'label' => array(
						'class' => 'login-form-label login-form-label-'.$field
					),
					'field' => array(
						'class' => 'login-form-field'
					),
					'submit' => 0,
					'redirect' => $c[redirect],
				);
				// User
				$form_inputs = array(
					array(
						'type' => 'text',
						'name' => 'user',
						'autocorrect' => 'off',
						'autocapitalize' => 'off',
						'class' => 'login-form-input-user',
						'validate' => array(
							'required' => 1,
						),
						'label' => array(
							'value' => $c[label_user],
							'class' => 'login-form-label-user'
						),
						'field' => array(
							'class' => 'login-form-field-user'
						)
					),
					array(
						'type' => 'password',
						'name' => 'password',
						'autocorrect' => 'off',
						'autocapitalize' => 'off',
						'class' => 'login-form-input-password',
						'validate' => array(
							'required' => 1,
						),
						'label' => array(
							'value' => $c[label_password],
							'class' => 'login-form-label-password'
						),
						'field' => array(
							'class' => 'login-form-field-password'
						)
					)
				);
				// Password
				if($c[cookie] == 1) $form_inputs[] = array(
					'type' => 'checkbox',
					'name' => 'remember',
					'options' => array(
						1 => "<span class='core-tiny'>Remember me</span>"
					),
					'value' => 1,
					'class' => 'login-form-input-cookie',
					'label' => array(
						'class' => 'login-form-label-cookie'
					),
					'field' => array(
						'class' => 'login-form-field-cookie'
					)
				);
				// Submit
				$form_inputs[] = array(
					'type' => 'submit',
					'class' => 'login-form-input-submit',
					'label' => array(
						'class' => 'login-form-label-submit'
					),
					'field' => array(
						'class' => 'login-form-field-submit'
					),
					'value' => $c[button_login]
				);
				// Register
				if($c[register] == 1 and permission('users','add') and m('users.public.views.register')) {
					$form_inputs[] = array(
						'type' => 'button',
						'class' => 'login-form-input-register',
						'inline' => 1,
						'label' => array(
							'class' => 'login-form-label-register'
						),
						'field' => array(
							'class' => 'login-form-field-register'
						),
						'value' => $c[button_register],
						'onclick' => "location.assign('".DOMAIN."register?redirect=".$c[redirect]."');"
					);
				}
				// Action
				$form_inputs[] = array(
					'type' => 'hidden',
					'name' => 'process_action',
					'value' => 'login'
				);
				// Module
				$form_inputs[] = array(
					'type' => 'hidden',
					'name' => 'process_module',
					'value' => 'users'
				);
				
				// Display
				$form = form::load($form_c);
				$form->inputs($form_inputs);
				$text .= "
		".$form->render();
				
				
				// Lost
				if($c[lost]) {
					$text .= "
		<div class='login-form-lost'>
			<span class='core-tiny'>
				<a href='".DOMAIN."login/lost_password' class='login-form-lost-password'>Forgot your ".strtolower($c[label_password])."?</a>";
			if($module_class->login_field() != "user_email") $text .= " |
				<a href='".DOMAIN."login/lost_username' class='login-form-lost-username'>Forgot your username?</a>";
			$text .= "
			</span>
		</div>";
				}
				
				// External
				/*if($c[external]) {
					$text .= "
		<div class='login-external-container'>";
					foreach($c[external] as $site) {
						// Facebook
						if($site == "facebook" and m('settings.share.facebook.key') and m('settings.share.facebook.secret')) {
							$text .= "
			<span class='login-external login-external-facebook'>
				<a href='".facebook_login(array('redirect' => $c[redirect]))."'></a>
			</span>";
						}
						// Twitter
						if($site == "twitter" and m('settings.share.twitter.key') and m('settings.share.twitter.secret')) {
							$text .= "
			<span class='login-external login-external-twitter'>
				<a href='".twitter_login(array('redirect' => $c[redirect]))."'></a>
			</span>";
						}
					}
					$text .= "
		</div>";
				}*/
				
				$text .= "
	</div>";
			}
			
			// Return
			return $text;
		}
	}
}
?>