<?php
/**
 * This gets called after functions have been loaded and the session started, but before ajax or process get called and before we determine the page we're on.
 *
 * If you need to effect every page after we've determined the page we're on (e.g. the $page object exists), use the /local/includes/page.php file.
 */
?>