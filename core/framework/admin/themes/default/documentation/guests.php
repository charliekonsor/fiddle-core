<?php
$module_class = module::load('users');
$label = $module_class->login_label();

print "
<h1>Help</h1>

<h2>Welcome</h2>

Welcome to your brand new <b>Content Management System</b> (CMS).<br /><br />

Using your CMS you can set up menus, add and update content, manage users, configure the website settings, and much, more.<br /><br />

To get started, though, we must first log in.<br /><br />

<h4>Login</h4>

The first step in setting up your site is to login.  You can do so using the form you see on this page.<br /><br />

We should have provided an admin ".strtolower($label)." and password you can enter here. If we didn't (or you lost it), please <a href='mailto:lindsayb@angelvisiontech.com'>contact us</a> and we'll help you out.<br /><br />

<h2>Further Help</h2>
Once you've logged in, you'll have access to manage all sorts of content. If, along the road, you need any help, just click on the 'Help' button(s) which will provide more information on whatever you're trying to do.<br /><br />

<h4>General Help</h4>
General help will appear as a 'Help' link in the very top right of the page (just like the one you just clicked on).<br /><br />

<h4>Module Help</h4>
A 'module' is simply a specific area within your CMS.  For example, 'Menus', 'Users', and 'Pages' are all modules.<br /><br />
 
Most modules will include a 'Help' button that appears to the right of a module's page heading.  For example, if you're in the Menus module, you should see a 'Menus' heading near the top of the page. To the right of that heading you will find the 'Help' button which will tell you much more about exactly how menus work.<br /><br />";
?>