<?php
// User
$user = item::load('users',$this->plugin_item_class->db('user'));
$user_url = $user->url();

// HTML
print "
<div class='comments-comment' id='".$this->plugin."-".$this->plugin_item_class->db('id')."'>
	<div class='comments-comment-icons'>";
if($this->permission('edit')) {
	print "
		<a href='javascript:void(0);' onclick=\"toggle('#comments-comment-".$this->plugin_id."-form');toggle('#comments-comment-".$this->plugin_id."-text');\" class='i i-edit i-inline'></a>";
}
print "
		".$this->plugin_item_class->icon('delete')."
	</div>
	<div class='comments-comment-user'>".($user_url ? "<a href='".$user_url."'>" : "").$user->name.($user_url ? "</a>" : "")."</div>
	<div class='comments-comment-datetime'>
		<span class='comments-comment-time'>Posted at ".date('g:i A',$this->plugin_item_class->db('created'))."</span>
		<span class='comments-comment-date'>on ".date('F j, Y',$this->plugin_item_class->db('created'))."</span>
	</div>
	<div class='comments-comment-text' id='comments-comment-".$this->plugin_id."-text'>
		".string_breaks($this->plugin_item_class->v('comment_text'))."
	</div>";
if($this->permission('edit')) {
	$form_class = $this->plugin_item_class->form(array('container' => "#".$this->plugin."-".$this->plugin_item_class->db('id')));
	print "
	<div class='comments-comment-form' id='comments-comment-".$this->plugin_id."-form' style='display:none;'>
		".$form_class->render()."
	</div>";
}
print "
</div>";