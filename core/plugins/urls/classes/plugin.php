<?php
if(!class_exists('plugin_urls',false)) {
	/**
	 * An extenion of the plugin class which handles functionality specific to the urls plugin.
	 *
	 * @package kraken\plugins\urls
	 */
	class plugin_urls extends plugin {
		/**
		 * Constructs the class.
		 *
		 * @param string $module The module of the item the plugin is acting upon.
		 * @param int $id The id of the item the plugin is acting upon. Default = NULL
		 * @param string $plugin The plugin we're using.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function __construct($module,$id = NULL,$plugin,$c = NULL) {
			$this->plugin_urls($module,$id,$plugin,$c);
		}
		function plugin_urls($module,$id = NULL,$plugin,$c = NULL) {
			parent::__construct($module,$id,$plugin,$c);
		}
		
		/**
		 * Returns array of inputs for use in the sidebar box for this plugin.
		 *
		 * @param object $form The form object we'll be adding these inputs to.
		 * @return array An array of inputs to add to the form.
		 */
		function sidebar($form) {
			// Saved
			if(plugin($this->plugin.'.db.table') and plugin($this->plugin.'.db.parent_module') and plugin($this->plugin.'.db.parent_id')) {
				$row = $this->db->f("SELECT * FROM ".plugin($this->plugin.'.db.table')." WHERE ".plugin($this->plugin.'.db.parent_module')." = '".$this->module."' AND ".plugin($this->plugin.'.db.parent_id')." = '".$this->id."' AND url_primary = 1"); 
			}
		
			// Format
			$format = m($this->module.'.public.views.item.urls.0.format',$this->type);
			// Format contains name?
			if(strstr($format,'{name}')) {
				// Random
				$r = random();
					
				// Input
				$input = "<input type='text' name='__plugins[".$this->plugin."][url_name]' value='".string_encode($row[url_name])."' style='width:110px;' placeholder='".string_url_encode($this->item->name)."' id='__plugins_url_".$r."' />";
				
				// Format
				$ex = explode('/',$format);
				foreach($ex as $e) {
					$e = trim($e);
					if(x($e)) {
						$value = NULL;
						if(substr($e,0,1) == "{" and substr($e,-1) == "}") {
							$e = str_replace(array('{','}'),'',$e);
							if($e == "home") $value = "";
							else if($e == "module") $value = $this->module;
							else if($e == "name") $value = $input;
							else if($e == "id") $value = ($this->id ? $this->id : "{id}");
							else if(m($this->module.'.db.'.$e,$this->type)) {
								$value = $this->item->db($e);
								if(!x($value)) $value = "{".$e."}";
							}
							else {
								$value = $this->item->v($e);
								if(!x($value)) $value = "{".$e."}";
							}
						}
						else $value = $e;
						$html .= "/".$value;
					}
				}
				$inputs[] = array(
					'type' => 'html',
					'name' => 'url_name',
					'value' => "<span class='core-tiny'>".$html."</span>"
				);
				
				// Javscript
				if(m($this->module.'.db.name',$this->type)) {
					$javascript .= "
<script type='text/javascript'>
$(document).ready(function() {
	pluginsUrlsPlaceholder".$r."();
	
	$(':input[name=".m($this->module.'.db.name',$this->type)."]').blur(function() {
		pluginsUrlsPlaceholder".$r."();
	});
});
function pluginsUrlsPlaceholder".$r."() {
	var \$target = $('#__plugins_url_".$r."');
	var value = \$target.val();
	var \$source = $(':input[name=".m($this->module.'.db.name',$this->type)."]');
	var source_value = encodeURIComponent(\$source.val());
	$.ajax({
		type: 'POST',
		url: DOMAIN,
		data: 'ajax_action=string_url_encode&ajax_module=".$this->module."&ajax_plugin=".$this->plugin."&string='+source_value,
		success: function(placeholder) {
			\$target.attr('placeholder',placeholder);
			placeholders();
		}
	});
}
</script>";
					$inputs[] = array(
						'type' => 'html',
						'value' => $javascript
					);
				}
			}
			
			// Return
			if($inputs) {
				$return = array(
					'label' => plugin($this->plugin.'.name'),
					'inputs' => $inputs
				);
				return $return;
			}
		}
		
		/**
		 * Saves an item's plugin values, either inserting it or (if row already exists) updating the existing row.
		 *
		 * @param array $values An array of values to save (column => value). Note you can pass a global databse definition as the column, example "db.name" will save it to the defined name column.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function item_save($values,$c = NULL) {
			// Error
			if(!$this->module or !$this->id or !$this->plugin or !$c['isset']) return;
			
			// Config
			if(!x($c[primary])) $c[primary] = 1; // This is the 'primary' id for the item.
			if(!x($c[debug])) $c[debug] = 0; // Debug
			
			// Primary
			$values[url_primary] = $c[primary];
			
			// Name
			$name = $values[url_name];
			if(!$name) {
				//$name = string_url_encode($this->item->name); // Not sure why I was using string_url_encode() here when $item->url() uses it already. At any rate, if removing this causes an issue, turn it back on here and in the $item->url() class.
				$name = $this->item->name;
			}
			
			// Modules
			foreach(g('modules') as $module => $module_v) {
				// Views
				if($views = $module_v['public'][views]) {
					foreach($views as $view => $v) {
						if(
							$v[item] // View is item specific
							and $v[item_module] == $this->module // Item is in this module
							and (!$this->item->v('module_code') or $this->item->v('module_code') == $module) // Hacky check to see if item relates to this module (for categories_items)
							and $format = $v[urls][0][format] // URL format is defined for this view
						) {
							// Format
							if(strstr($format,"/{name}") and !m($this->module.'.db.name') and !$name) { // Name in format, but no name defined (probably defaulted to {module}/{name}/{id}), remove name
								$_format = str_replace("/{name}","",$format);
								if($_format) $format = $_format;
							}
							debug("<br />\$".__CLASS__."->".__FUNCTION__."() - format: ".$format.", name: ".$name,$c[debug]);
							if($format) {
								// URL
								$values[url] = $this->item->url('public',array('saved' => 0,'name' => $name,'module' => $module,'view' => $view,'debug' => $c[debug]));
								debug("\$".__CLASS__."->".__FUNCTION__."() - url: ".$values[url],$c[debug]);
								if($values[url]) {
									// Remove domain
									$values[url] = str_replace(array(DOMAIN,SDOMAIN),'',$values[url]);
								
									// Statement (exists already?)
									$query = "SELECT * FROM items_urls WHERE module_area = 'public' AND module_code = '".$module."' AND module_view = '".$view."' AND item_module = '".$this->module."' AND item_id = '".$this->id."' AND LOWER(url) COLLATE utf8_bin = '".a(strtolower($values[url]))."'";
									debug($query,$c[debug]);
									$row = $this->db->f($query);
									if($row[id]) $statement = "UPDATE";
									else $statement = "INSERT";
									
									// Inserting, make sure the URL is unique (if it has a format that may be in colission with other URLs)
									if($statement == "INSERT" and strstr($format,'{name}') and !strstr($format,'{id}')) {
										for($x = 0;$x <= 1000;$x++) {
											// Existed, append with x
											if($x > 0) {
												// New URL
												$values[url] = $this->item->url('public',array('saved' => 0,'name' => $name.'-'.$x));
												// Remove domain
												$values[url] = str_replace(array(DOMAIN,SDOMAIN),'',$values[url]);
											}
											
											// URL exists?
											$query = "SELECT * FROM items_urls WHERE LOWER(url) COLLATE utf8_bin = '".a(strtolower($values[url]))."' AND (item_module != '".$this->module."' OR item_id != '".$this->id."')";
											debug($query,$c[debug]);
											$row0 = $this->db->f($query);
											// No
											if(!$row0[id]) {
												// Update value
												if($x > 0) $values[url_name] = $name.'-'.$x;
												// End loop
												$x = 10001;
											}
										}
									}
									
									// Values
									$values = $this->save_values($values,$statement);
									if($values) {
										// Debug
										debug("\$".__CLASS__."->".__FUNCTION__."() values: ".return_array($values),$c[debug]);
										
										// Automatic values
										$values[module_area] = "public";
										$values[module_code] = $module;
										$values[module_view] = $view;
										$values[item_module] = $this->module;
										$values[item_id] = $this->id;
										
									
										// Primary, make sure it's the only one // Do this before saving as that'll call $item->process() which will call $plugin_urls->item_process() which will reset the primary one to the previous one...unless we remove the primary value first
										if($c[primary] == 1) {
											// Make sure no other urls for this item are considred primary
											$query = "UPDATE items_urls SET url_primary = 0, url_updated = '".time()."' WHERE item_module = '".$this->module."' AND item_id = '".$this->id."'".($row[id] ? " AND id != '".$row[id]."'" : "");
											debug($query,$c[debug]);
											$this->db->q($query);
										}
									
										// Save
										$plugin_item = $this->plugin_item($row);
										$row[id] = $plugin_item->save($values,$c);
									}
								}
							}
						}
					}
				}
			}
		}
		
		/**
		 * Reprocesses an item's URL. Basically just have this so when we independently call $item->process() this will again be processed.
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function item_process($c = NULL) {
			// Error
			if(!$this->module or !$this->id or !$this->plugin) return;
			
			// Values
			$query = "SELECT * FROM items_urls WHERE module_area = 'public' AND item_module = '".$this->module."' AND item_id = '".$this->id."' AND url_primary = 1";
			$row = $this->db->f($query);
			$values = array(
				'url_name' => $row[url_name]
			);
			$c['isset'] = 1;
			
			// Save
			$this->item_save($values,$c);
		}
		
		/**
		 * Copy's the plugin row(s) for the item to the given new item.
		 *
		 * Need to make sure we 'reprocess' URL after copying over row incase original had a url_name value.
		 *
		 * @param int $new_id The id of the new, copied item.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function item_copy($new_id,$c = NULL) {
			// Parent
			parent::item_copy($new_id,$c);
			
			// Process
			$this->item_process($c);
		}
	}
}
?>