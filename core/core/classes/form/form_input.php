<?php
if(!class_exists('form_input',false)) {
	class form_input/* extends form*/ {
		/**
		 * Variables
		 */	
		public $type;
		public $row;
		public $label;
		public $field;
		public $name;
		public $value;
		public $value_default;
		public $value_temp;
		public $attributes;
		public $prepend;
		public $append;
		public $help;
		public $validate;
		public $error;
		public $process = 1; // Whether or not we want to process the input's value or not (meaning put it in the resulting $values array)
		public $purify = 1; // Whether or not we want to 'purify' the values submitted via this input when we're 'processing' it. Can also pass an array which is the $c array in the $form->value_purify() method
		public $c = array();
		
		/**
		 * Loads and returns an instance of the form_input class, using framework level classes if they exist.
		 *
		 * Example (for the form_input_text extension):
		 * - $form = form_input_text::load($label,$name,$value,$c);
		 *
		 * @param string $label The label of the input
		 * @param string $name The name of the input field (aka the key in the $_POST/$_GET/$_FILES results)
		 * @param string $value The current value of this input
		 * @param array $c An array of configuration values. Default = NULL
		 * @return object An instance of either the input class.
		 */
		static function load($label = NULL,$name = NULL,$value = NULL,$c = NULL) {
			// Class name
			if(function_exists('get_called_class')) $class = get_called_class(); // PHP >= 5.3
			if(!$class) {
				$array = debug_backtrace();
				foreach($array as $k => $v) {
					if($k > 0) {
						if(strstr($array[$k]['class'],'form_input_')) $class = $array[$k]['class'];
						else break;
					}
				}
			}
			if($class) {
				// Framework class
				if(FRAMEWORK) {
					$class_framework = $class."_framework";
					if(!class_exists($class_framework)) include_once SERVER."core/framework/classes/".$class.".php";
					if(class_exists($class_framework)) return new $class_framework($label,$name,$value,$c);
				}
				
				// Core class
				return new $class($label,$name,$value,$c);
			}
		}

		/**
		 * Construct
		 */
		function __construct($label = NULL,$name = NULL,$value = NULL,$c = NULL) {
			$this->form_input($label,$name,$value,$c);
		}
		function form_input($label = NULL,$name = NULL,$value = NULL,$c = NULL) {
			// Type (if not defined in child class)
			if(!$this->type) $this->type = str_replace(array('form_input_','_framework'),'',get_class($this));
			
			// Label
			$this->label = $label;
			// Name
			$this->name = $name;
			// Value
			$this->value = $value;
			// Label - do both before and after merging with config since we can pass a string, but want it to be an array
			$this->label();
			
			// Config - attributes // Legacy, used to pass in $c, not $c[attributes]
			$vars = array_keys(get_object_vars($this));
			$reserved = array(
				'type',
			);
			if(x($c[attributes])) { // Must do before we loop through as we don't want to overwrite the $this->attributes if we've already added some legacy ones (ex: $c[disabled] = 'disbaled' becomes $this->attributes[disabled] = 'disabled')
				$this->attributes = $c[attributes]; 
				unset($c[attributes]);
			}
			if($c) {
				$attributes_valid = $this->attributes_valid();
				foreach($c as $k => $v) {
					if(!in_array($k,$reserved)) {
						// Class property
						if(in_array($k,$vars)) {
							if(is_array($this->$k)) $this->$k = array_merge_associative($this->$k,$v);
							else $this->$k = $v;
							unset($c[$k]);	
						}
						// Attribute
						else if(in_array($k,$attributes_valid)) {
							$this->attributes[$k] = $v;
							unset($c[$k]);
						}
					}
				}
			}
			// Config - legacy
			if(!x($this->value_default) and x($c['default'])) $this->value_default = $c['default'];
			if(x($c[required]) and !x($this->validate[required])) $this->validate[required] = $c[required];
			if($c[accept] and !$this->validate[accept]) $this->validate[accept] = $c[accept];
			unset($c['default'],$c[required],$c[accept]);
						
			// Config
			if($c) $this->c = array_merge($this->c,$c);
			
			// Row
			$this->row();
			// Label
			$this->label();
			// Field
			$this->field();
			
			// Attributes - class 
			$class = "form-input form-input-".$this->type;
			if(!strstr($this->attributes['class'],$class)) $this->attributes['class'] .= ($this->attributes['class'] ? " " : "").$class;
			// Attributes - disabled - make sure it's set to 'disabled' (ex: $this->disabled = 1; becomes $this->disabled = 'disabled';
			if($this->attributes[disabled]) $this->attributes[disabled] = 'disabled'; 
			// Attributes - readonly - make sure it's set to 'readonly' (ex: $this->readonly = 1; becomes $this->readonly = 'readonly';
			if($this->attributes[readonly]) $this->attributes[readonly] = 'readonly';
			
			// Validate - automatic - these attributes automatically add validation
			if($this->attributes[min] and !$this->validate[min]) $this->validate[min] = $this->attributes[min];
			if($this->attributes[max] and !$this->validate[max]) $this->validate[max] = $this->attributes[max];
			if($this->attributes[minlength] and !$this->validate[minlength]) $this->validate[minlength] = $this->attributes[minlength];
			if($this->attributes[maxlength] and !$this->validate[maxlength]) $this->validate[maxlength] = $this->attributes[maxlength];
			if($this->attributes[minwords] and !$this->validate[minwords]) $this->validate[minwords] = $this->attributes[minwords];
			if($this->attributes[maxwords] and !$this->validate[maxwords]) $this->validate[maxwords] = $this->attributes[maxwords];
			// Validate - accept - make sure it's a string
			if($this->validate[accept]) {
				if(is_array($this->validate[accept])) $this->validate[accept] = implode(',',$this->validate[accept]);		
				$this->validate[accept] = str_replace(' ','',$this->validate[accept]); // Remove spaces
			}
		}
		
		/**
		 * Standardizes input's row array.
		 */
		function row() {
			// Upgrade
			$attributes_valid = $this->attributes_valid();
			foreach($this->row as $k => $v) {
				// Attribute
				if(in_array($k,$attributes_valid)) {
					$this->row[attributes][$k] = $v;
					unset($c[$k]);
				}
			}
			
			// Classes
			$class = "form-row form-row-".$this->type;
			if(!strstr($this->row[attributes]['class'],$class)) {
				$this->row[attributes]['class'] .= ($this->row[attributes]['class'] ? " " : "").$class;
			}
		}
		
		/**
		 * Standardizes input's label, usually just turning $this->label = 'abc' to $this->label = array('value' => 'abc').
		 */
		function label() {
			// Array
			if(!is_array($this->label)) {
				$this->label = array('value' => $this->label);
			}
			
			// Upgrade
			$attributes_valid = $this->attributes_valid();
			foreach($this->label as $k => $v) {
				// Attribute
				if(in_array($k,$attributes_valid)) {
					$this->label[attributes][$k] = $v;
					unset($c[$k]);
				}
			}
			
			// Classes
			$class = "form-label form-label-".$this->type;
			if(!strstr($this->label[attributes]['class'],$class)) {
				$this->label[attributes]['class'] .= ($this->label[attributes]['class'] ? " " : "").$class;
			}
			if(!x($this->label[value])) $this->label[attributes]['class'] .= ($this->label[attributes]['class'] ? " " : "")."form-label-blank";
		}
		
		/**
		 * Standardizes input's field array.
		 */
		function field() {
			// Upgrade
			$attributes_valid = $this->attributes_valid();
			foreach($this->field as $k => $v) {
				// Attribute
				if(in_array($k,$attributes_valid)) {
					$this->field[attributes][$k] = $v;
					unset($c[$k]);
				}
			}
			
			// Classes
			$class = "form-field form-field-".$this->type;
			if(!strstr($this->field[attributes]['class'],$class)) {
				$this->field[attributes]['class'] .= ($this->field[attributes]['class'] ? " " : "").$class;
			}
		}
		
		/**
		 * Returns a string of attributes for an element.
		 *
		 * This really just acts as an interpreter between the new method (inputs as objects) and the old method (input as arrays).
		 *
		 * @param object $form The form object this input is a part of.
		 * @return string A string of HTML attributes for the element.
		 */
		function attributes($form) {
			// Error
			if(!$form) return;
			
			// Attributes - defined
			$input = $this->attributes;
			// Attributes - variables (if not explicity defined already)
			if(!$input[type]) $input[type] = $this->type;
			if(!$input[name]) $input[name] = $this->name;
			if(!$input[value]) $input[value] = $this->value;
			// Attributes - validate (so it works with old method where input was an array)
			$input[validate] = $this->validate;
			
			// Parent
			$string = $form->attributes($input,$c);
			
			// Return
			return $string;
		}
		
		/**
		 * Returns an array of attribute keys that are available to be used on this input.
		 *
		 * We use this instead of a class variable to keep the size of the form class down
		 * which will keep cached file size down too.
		 *
		 * @return array An array of valid attribute keys.
		 */
		function attributes_valid() {
			// Array
			$array = array(
				// Global - http://www.w3schools.com/tags/ref_standardattributes.asp
				'class',
				'id',
				'style',
				'tabindex',
				'title',
				// Input
				'accept',
				'align',
				'alt',
				'autocomplete',
				'autofocus',
				'checked',
				'disabled',
				'form',
				'formaction',
				'formenctype',
				'formmethod',
				'formnovalidate',
				'formtarget',
				'height',
				'list',
				'max',
				'maxlength',
				'min',
				'multiple',
				'name',
				'pattern',
				'placeholder',
				'readonly',
				'required',
				'size',
				'src',
				'step',
				'type',
				'value',
				'width',
				// Form
				'accept-charset',
				'action',
				'off',
				'enctype',
				'method',
				'novalidate',
				'target',
				// Custom
				'minlength',
				'minwords',
				'maxwords',
				// Javascript events
				'onclick',
				'ondblclick',
				'onmousedown',
				'onmousemove',
				'onmouseover',
				'onmouseout',
				'onmouseup',
				'onkeydown',
				'onkeypress',
				'onkeyup',
				'onabort',
				'onerror',
				'onload',
				'onresize',
				'onscroll',
				'onunload',
				'onblur',
				'onchange',
				'onfocus',
				'onreset',
				'onselect',
				'onsubmit',
			);
			
			// Return
			return $array;
		}
		
		/**
		 * Renders the HTML for the input's 'row'.
		 *
		 * @param object $form The form object this input is a part of.
		 * @param array $c An array of config values. Default = NULL
		 * @return string The HTML of the input's row.
		 */
		function html_row($form,$c = NULL) {
			// Attributes
			$array = $this->row[attributes];
			$array[type] = 'row';
			$attributes = $form->attributes($array);
			
			// Value - default
			if(!x($this->value) and x($this->value_default)) $this->value = $this->value_default;
			
			// HTML
			$html = "
		<div".($attributes ? " ".$attributes : "").">
			".$this->html_label_container($form,$c)."
			".$this->html_field_container($form,$c)."
			<div class='form-clear form-row-clear'></div>
		</div>";
			
			// Return
			return $html;
		}
		
		/**
		 * Renders the HTML for the input's 'label', including the container.
		 *
		 * @param object $form The form object this input is a part of.
		 * @param array $c An array of config values. Default = NULL
		 * @return string The HTML of the input's label, including the container.
		 */
		function html_label_container($form,$c = NULL) {
			// Attributes
			$array = $this->label[attributes];
			$array[type] = 'label';
			$attributes = $form->attributes($array);
			
			// HTML
			$html = "
			<div".($attributes ? " ".$attributes : "").">".$this->html_label($form,$c)."</div>";
			
			// Return
			return $html;
		}
		
		/**
		 * Renders the HTML for the input's 'label'.
		 *
		 * @param object $form The form object this input is a part of.
		 * @param array $c An array of config values. Default = NULL
		 * @return string The HTML of the input's label.
		 */
		function html_label($form,$c = NULL) {
			// HTML
			$html = $this->label[value];
			
			// Return
			return $html;
		}
		
		/**
		 * Renders the HTML for the input's 'field', including the container.
		 *
		 * @param object $form The form object this input is a part of.
		 * @param array $c An array of config values. Default = NULL
		 * @return string The HTML of the input's field, including the container.
		 */
		function html_field_container($form,$c = NULL) {
			// Attributes
			$array = $this->field[attributes];
			$array[type] = 'field';
			$attributes = $form->attributes($array);
			
			// HTML
			$html = "
			<div".($attributes ? " ".$attributes : "").">
				".$this->html_field($form,$c)."
			</div>";
			
			// Return
			return $html;
		}
		
		/**
		 * Renders the HTML for the input's 'field'.
		 *
		 * @param object $form The form object this input is a part of.
		 * @param array $c An array of config values. Default = NULL
		 * @return string The HTML of the input's field.
		 */
		function html_field($form,$c = NULL) {
			// Input
			$html .= "
				".$this->html_element($form,$c);
					
			// Prepend
			if($this->prepend) $html = $this->prepend.$html;
			
			// Append
			if($this->append) $html .= $this->append;
		
			// Help
			if($this->help) $html .= help($this->help);
				
			// Error
			if($this->error) $html .= "
				<label class='error form-error' for='".$this->name."' style='display:inline;'>".$this->error."</label>";
			
			// Clear
			$html .= "
				<div class='form-clear form-field-clear'></div>";
			
			// Return
			return $html;
		}
		
		/**
		 * Renders the HTML for the actual input.
		 *
		 * @param object $form The form object this input is a part of.
		 * @param array $c An array of config values. Default = NULL
		 * @return string The HTML of the input.
		 */
		function html_element($form,$c = NULL) {
			// HTML
			$html = "<input ".$this->attributes($form)." />";
			
			// Return
			return $html;
		}
			
		/**
		 * Processes the value for this input.
		 *
		 * @param object $form The form object which contains the submitted values ($form->post and $form->files).
		 * @param array $c An array of configuration values. Default = NULL
		 * @return mixed The resulting value.
		 */
		function process($form,$c = NULL) {
			// Config
			if($c) $this->c = array_merge($this->c,$c);
			
			// Value
			$value = $this->value($form);
			
			// Return
			return $value;
		}

		/**
		 * Determines and returns value for this input, taking multi-level array keys into account.
		 * 
		 * @param object $form The form object which contains the submitted values ($form->post and $form->files).
		 * @return mixed The retrieved value.
		 */
		function value($form) {
			return form::value($form->post,$this->name);
		}
		
		/**
		 * Validates the input and the value submitted. Saves any validation error to $this->errors.
		 *
		 * To do
		 * - creditcard
		 *
		 * @param object $form The form object this input is a part of.
		 * @param mixed $value The value submitted.
		 * @return boolean Whether or not the input is valid.
		 */
		function validate($form,$value = NULL) {
			// Already an error
			if($form->errors[header][$this->name]) return;
			
			// Field name
			$field_name = ($this->label[value] ? " in the ".$this->label[value]." field" : "");
			
			// Error
			$error = NULL;
			
			// Required
			if($this->validate[required]) {
				if(!x($value)) {
					$error = "This field is required";
					if($field_name) $error_header = "The ".$this->label[value]." field is required.";
					else $error_header = "A required field is missing.";
				}
			}
			if(!$error and x($value)) {
				// Min
				if(!$error and $this->validate[min] and is_numeric($this->validate[min]) and is_numeric($value)) {
					if($value < $this->validate[min]) {
						$error = "Please enter a value greater than or equal to ".$this->validate[min];
					}
				}
				// Max
				if(!$error and $this->validate[max] and is_numeric($this->validate[max]) and is_numeric($value)) {
					if($value > $this->validate[max]) {
						$error = "Please enter a value less than or equal to ".$this->validate[max];
					}
				}
				// Minlength
				if(!$error and $this->validate[minlength] and is_numeric($this->validate[minlength])) {
					if(is_array($value)) $length = count($value);
					else $length = mb_strlen(trim(string_decode($value)),'UTF-8');
					if($length < $this->validate[minlength]) {
						if(is_array($value)) $error = "Please select at least ".$this->validate[minlength]." options";
						else $error = "Please enter at least ".$this->validate[minlength]." characters";
					}
				}
				// Maxlength
				if(!$error and $this->validate[maxlength] and is_numeric($this->validate[maxlength])) {
					if(is_array($value)) $length = count($value);
					else $length = mb_strlen(trim(string_decode($value)),'UTF-8');
					if($length > $this->validate[maxlength]) {
						if(is_array($value)) $error = "Please select no more than ".$this->validate[maxlength]." options";
						else $error = "Please enter no more than ".$this->validate[maxlength]." characters";
					}
				}
				// Minwords
				if(!$error and $this->validate[minwords] and is_numeric($this->validate[minwords])) {
					$value_text = preg_replace('/<.[^<>]*?>/s',' ',string_decode($value));
					$value_text = preg_replace('/&nbsp;|&#160;/si',' ',$value_text);
					$value_text = preg_replace('/[^\w\s]/','',$value_text); // jquery.validate uses /[.(),;:!?%#$'"_+=\/\-]*/g, but not sure how to escape and preg_quote() doesn't seem to do it properly
					
					$words = preg_match_all('/\b\w+\b/s',$value_text,$matches);
					debug('words: '.$words,$this->c[debug]);
					
					if($words < $this->validate[minwords]) {
						$error = "Please enter at least ".$this->validate[minwords]." words";
					}
				}
				// Maxwords
				if(!$error and $this->validate[maxwords] and is_numeric($this->validate[maxwords])) {
					$value_text = preg_replace('/<.[^<>]*?>/s',' ',string_decode($value));
					$value_text = preg_replace('/&nbsp;|&#160;/si',' ',$value_text);
					$value_text = preg_replace('/[^\w\s]/','',$value_text); // jquery.validate uses /[.(),;:!?%#$'"_+=\/\-]*/g, but not sure how to escape and preg_quote() doesn't seem to do it properly
					
					$words = preg_match_all('/\b\w+\b/s',$value_text,$matches);
					debug('words: '.$words,$this->c[debug]);
					
					if($words > $this->validate[maxwords]) {
						$error = "Please enter no more than ".$this->validate[maxwords]." words";
					}
				}
				// Accept
				if(!$error and $this->validate[accept]) {
					$extension = trim(substr(strrchr($value, '.'),1));
					if($extension or $this->c[storage] or !$this->c[storage] == "local") { // Can't test non-extension files (aka, $value is id of external storage)
						if(is_array($this->validate[accept])) $this->validate[accept] = implode(',',$this->validate[accept]);	
						$this->validate[accept] = str_replace(' ','',$this->validate[accept]); // Remove spaces
						$this->validate[accept] = str_replace(',','|',$this->validate[accept]);
						if(!preg_match('/.('.$this->validate[accept].')$/i',$value)) {
							$error = "Please enter a value with a valid extension";
						}
					}
				}
				// Digits - must do before 'number' (allows for negative numbers too)
				if(!$error and $this->validate[digits]) {
					if(!preg_match('/^-?\d+$/',$value)) {
						$error = "Please enter only digits";
					}
				}
				// Number
				if(!$error and $this->validate[number]) {
					if(!is_numeric($value)) {
						$error = "Please enter a number";
					}
				}
				// E-mail
				if(!$error and $this->validate[email]) {
					// jQuery Validate version
					//$regex = "/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$/i";
					
					// jQuery Validate PHP Plugin version
					$regex = '/^[a-z0-9!#$%&*+=?^_`{|}~-]+(\.[a-z0-9!#$%&*+-=?^_`{|}~]+)*@([-a-z0-9]+\.)+([a-z]{2,3}|info|arpa|aero|coop|name|museum)$/i';
					
					$valid = 1;
					// Multiple
					if($this->c[multiple]) {
						$emails = explode(',',$value);
						foreach($emails as $email) {
							$email = trim($email);
							if($email) {	
								if(!preg_match($regex,$email)) {
									$valid = 0;
									break;
								}
							}
						}
					}
					// Single
					else if(!preg_match($regex,$value)) $valid = 0;
					
					// Invalid
					if(!$valid) {
						$error = "Please enter a valid e-mail";
					}
				}
				// URL
				if(!$error and $this->validate[url]) {
					// jQuery Validate version
					//$regex = "/^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=\{\}]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=\{\}]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=\{\}]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=\{\}]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=\{\}]|:|@)|\/|\?)*)?$/i";
					// jQuery Validate PHP Plugin version
					$regex = '{
  \\b
  # Match the leading part (proto://hostname, or just hostname)
  (
    # http://, or https:// leading part
    (https?)://[-\\w]+(\\.\\w[-\\w]*)+
  |
    # or, try to find a hostname with more specific sub-expression
    (?i: [a-z0-9] (?:[-a-z0-9]*[a-z0-9])? \\. )+ # sub domains
    # Now ending .com, etc. For these, require lowercase
    (?-i: com\\b
        | edu\\b
        | biz\\b
        | gov\\b
        | in(?:t|fo)\\b # .int or .info
        | mil\\b
        | net\\b
        | org\\b
        | [a-z][a-z]\\.[a-z][a-z]\\b # two-letter country code
    )
  )
  # Allow an optional port number
  ( : \\d+ )?
  # The rest of the URL is optional, and begins with /
  (
    /
    # The rest are heuristics for what seems to work well
    [^.!,?;"\'<>()\[\]\{\}\s\x7F-\\xFF]*
    (
      [.!,?]+ [^.!,?;"\'<>()\\[\\]\{\\}\s\\x7F-\\xFF]+
    )*
  )?
}ix';
					if(!preg_match($regex,$value)) {
						$error = "Please enter a valid URL";
					}
				}
				// Credit Card
				if(!$error and $this->validate[creditcard]) {
					// Valid to start
					$valid = 1;
					
					// Accept only spaces, digits and dashes
					if(preg_match('/[^0-9 -]+/',$value)) $valid = 0;
					else {
						// Variables
						$nCheck = 0;
						$nDigit = 0;
						$bEven = false;
						
						// Remove spaces/dashes
						$value_simple = preg_replace('/\D/','',$value);
		
						// Loop through numbers (backwards)
						for($x = strlen($value_simple) - 1;$x >= 0;$x--) {
							$cDigit = $value_simple[$x];
							$nDigit = intval($cDigit,10);
							if($bEven) {
								if(($nDigit *= 2) > 9) $nDigit -= 9;
							}
							$nCheck += $nDigit;
							$bEven = !$bEven;
						}
		
						// Value of 10?
						if(($nCheck % 10) != 0) $valid = 0;
					}
					if(!$valid) {
						$error = "Please enter a valid credit card number";
					}
				}
				// Regex
				if(!$error and $this->validate[regex]) {
					$regex = $this->validate[regex];
					if(substr($regex,0,1) != "/") $regex = "/".$regex."/";
					if(!preg_match($regex,$value)) {
						$error = "This format is not valid";
						if($field_name) $error_header = "The ".$this->label[value]." format is not valid.";
						else $error_header = "A format is not valid.";	
					}
				}
				// Equal to
				if(!$error and $this->validate[equalto]) {
					$input2 = $form->input_exists($this->validate[equalto]);
					if($input2) {
						// Process
						$value2 = $form->input_process($input2);
						
						// Purify
						if($input2->purify) $value2 = $form->value_purify($value2,(is_array($input2->purify) ? $input2->purify : NULL));
						
						// Validate // Won't alter $value2
						/*if($this->c[validate_php]) {
							if($input2[validate][equalto] != $this->name) $this->input_validate($input2,$value2);
						}*/
						
						// Different?
						debug("equal to. value original: ".$value2.", value match: ".$value,$this->c[debug]);
						if($value !== $value2) {
							if($input2[label][value]) $error = "This does not match ".$input2[label][value];
							else $error = "These don't match.";
							if($input2[label][value] and $this->label[value]) $error_header = "The ".$input2[label][value]." and ".$this->label[value]." values don't match.";
							else $error_header = "Required values don't match.";	
						}
					}
				}
			}
			
			// Error
			if($error) {
				if(!$error_inline) $error_inline = $error.".";
				if(!$error_header) $error_header = $error.$field_name.".";
				$form->errors[header][$this->name] = $error_header;
				$form->errors[inline][$this->name] = $error_inline;
				return false;
			}
			// Success
			else return true;
		}
		
		/**
		 * Standardizes the given array of options.
		 *
		 * Want in the following format:
		 * array(
		 *		'value' => '123',
		 *		'label' => 'Item 123'
		 * ) 
		 * 
		 * Not:
		 * array(
		 *		'123' => 'Item 123'
		 * )
		 *
		 * Note, with child options it'll be:
		 * array(
		 *		'label' => 'Size',
		 *		'options' => array(
		 *			112 => 'Small',
		 *			113 => 'Medium'
		 *		)
		 * ) 
		 *
		 * @param array $options The array of options we want to standardize.
		 * @return array The standardized array of options.
		 */
		function options($options) {
			// Options
			$options_new = NULL;
			if(is_array($options)) {
				foreach($options as $k => $v) {
					// Re-format
					if(!is_array($v)) {
						$v = array(
							'value' => $k,
							'label' => $v
						);
					}
					// Children
					if($v[options]) {
						$v[options] = $this->options($v[options]);
					}
					// Store
					$options_new[] = $v;
				}
			}
			
			// Return
			return $options_new;
		}
		
		/**
		 * Creates and returns the $c array you can use to duplicate this input.
		 *
		 * May use if changing an input to another type too.
		 * Ex: form_input_hidden::load($this->name,$this->value,$this->c_duplicate());
		 *
		 * Note, it doesn't carry over 'label', 'name', or 'value' information as
		 * we assume you'll pass that when creating the new input.
		 *
		 * @return array The $c array you can use for duplicating this input.
		 */
		function c_duplicate() {
			// Array
			$array = $this->c;
			foreach(get_object_vars($this) as $k => $v) {
				if(!in_array($k,array('c','label','name','value','type'))) {
					$array[$k] = $v;
				}
			}	
			
			// Return
			return $array;
		}
	}
}
?>