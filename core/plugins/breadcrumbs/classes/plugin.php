<?php
if(!class_exists('plugin_breadcrumbs',false)) {
	/**
	 * An extenion of the plugin class which handles functionality specific to the breadcrumbs plugin.
	 *
	 * @package kraken\plugins\breadcrumbs
	 */
	class plugin_breadcrumbs extends plugin {
		/**
		 * Constructs the class.
		 *
		 * @param string $module The module of the item the plugin is acting upon.
		 * @param int $id The id of the item the plugin is acting upon. Default = NULL
		 * @param string $plugin The plugin we're using.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function __construct($module,$id = NULL,$plugin,$c = NULL) {
			$this->plugin_breadcrumbs($module,$id,$plugin,$c);
		}
		function plugin_breadcrumbs($module,$id = NULL,$plugin,$c = NULL) {
			parent::__construct($module,$id,$plugin,$c);
		}
		
		/**
		 * Returns of the breadcrumbs for the current page ($__GLOBAL['page']).
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 * @return string The HTML of the breadcrumbs for this page.
		 */
		function breadcrumbs($c = NULL) {
			// Array
			$breadcrumbs = $this->breadcrumbs_array($breadcrumbs,$c);
			
			// HTML
			$html = $this->breadcrumbs_html($breadcrumbs,$c);
			
			// Return
			return $html;
		}
		
		/**
		 * Creates and returns an array of breadcrumbs for the current page ($__GLOBAL['page']).
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of the current page's breacrumbs in array(array('url' => $url,'text' => $text),array(..)) format.
		 */
		function breadcrumbs_array($c = NULL) {
			// Page
			$page = g('page');
			
			// Class
			$class = module::load($page->module,array('type' => $page->type));
			
			// Breadcrumbs plugin active?
			if(!$class->v('plugins.breadcrumbs')) return;
			
			// Home
			$breadcrumbs = array(
				array(
					'url' => DOMAIN,
					'text' => 'Home'
				),
			);
			
			// Parents
			if($page->parents) {
				foreach($page->parents as $k => $v) {
					// Skip last (already included in last 'int' array entry)
					if($k == "last") continue;
					
					// Breadcrumb - home
					if($v[module]) {
						$plugin_class = plugin::load($v[module],NULL,$this->plugin);
						if($breadcrumb = $plugin_class->breadcrumb()) $breadcrumbs[] = $breadcrumb;
					}
					// Breadcrumb - type
					if($v[type]) {
						$plugin_class = plugin::load($v[module],NULL,$this->plugin,array('type' => $v[type]));
						if($breadcrumb = $plugin_class->breadcrumb()) $breadcrumbs[] = $breadcrumb;
					}
					// Breadcrumb - item
					if($v[view] == "item" and $v[id]) {
						$plugin_class = plugin::load($v[module],$v[id],$this->plugin);
						if($breadcrumb = $plugin_class->breadcrumb()) $breadcrumbs[] = $breadcrumb;
					}
				}
			}
			
			// Page
			if($page->module != "home") {
				// Breadcrumb - home
				if($page->module) {
					$plugin_class = plugin::load($page->module,NULL,$this->plugin);
					if($breadcrumb = $plugin_class->breadcrumb()) $breadcrumbs[] = $breadcrumb;
				}
				// Breadcrumb - type
				if($page->type) {
					$plugin_class = plugin::load($page->module,NULL,$this->plugin,array('type' => $page->type));
					if($breadcrumb = $plugin_class->breadcrumb()) $breadcrumbs[] = $breadcrumb;
				}
				// Breadcrumb - item
				if($page->view == "item" and $page->id) {
					$plugin_class = plugin::load($page->module,$page->id,$this->plugin);
					if($breadcrumb = $plugin_class->breadcrumb()) $breadcrumbs[] = $breadcrumb;
				}
			}
			
			// Variables
			# do we want to build this?
			
			// Return
			return $breadcrumbs;
		}
		
		/**
		 * Creates the HTML for the given array of breadcrumbs.
		 *
		 * @param array $breadcrumbs The array of breadcrumbs in array(array('url' => $url,'text' => $text),array(..)) format.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return string The HTML of the breadcrumbs
		 */
		function breadcrumbs_html($breadcrumbs,$c = NULL) {
			// Error
			if(!$breadcrumbs) return;
			
			// Page
			$page = g('page');
			
			// Class
			$class = module::load($page->module,array('type' => $page->type));
			
			// Config
			if(!x($c[separator])) {
				$c[separator] = $class->v('settings.plugins.breadcrumbs.separator');
				if(!x($c[separator])) $c[separator] = $this->v('settings.separator');
			}
			if(!x($c[limit])) { // Characters to limit the 'text' string to for each crumb.
				$c[limit] = $class->v('settings.plugins.breadcrumbs.limit');
				if(!x($c[limit])) $c[limit] = $this->v('settings.limit');
			}
			
			// HTML
			$html = NULL;
			if(count($breadcrumbs)) {
				foreach($breadcrumbs as $k => $v) {
					$html .= ($html ? "<span class='breadcrumb-separator'>".$c[separator]."</span>" : "")."<span class='breadcrumb'><a href='".$v[url]."'>".($c[limit] ? string_limit($v[text],$c[limit]) : $v[text])."</a></span>";
				}
			}
			if($html) $html = "<div class='breadcrumbs'>".$html."</div>";
			
			// Return
			return $html;
		}
		
		/**
		 * Returns single breadcrumb URL and text (in an array) for this item/type/module.
		 *
		 * Note, this gets called on a different object than the breadcrumbs() method (which doesn't get called on specific module/item).
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array containing this crumb's 'url' and 'text'.
		 */
		function breadcrumb($c = NULL) {
			// Error
			if(!$this->module) return;
			
			// Item
			if($this->id > 0) {
				if($url = $this->item->url()) {
					$array = array(
						'url' => $url,
						'text' => $this->item->db('name'),
					);	
				}
			}
			// Type
			else if($this->type) {
				$type_class = module::load($this->module,array('type' => $this->type));
				if($url = $type_class->url()) {
					$array = array(
						'url' => $url,
						'text' => $type_class->v('name'),
					);
				}
			}
			// Module
			else if($this->module) {
				$module_class = module::load($this->module);
				if($url = $module_class->url()) {
					$array = array(
						'url' => $url,
						'text' => $module_class->v('name'),
					);
				}
			}
			
			// Return
			return $array;
		}
	}
}
?>