<?php
/************************************************************************************/
/********************************* Modules / Plugins ********************************/
/************************************************************************************/
// Theme
$theme = m('settings.settings.theme');

// Local - module + type
if($_REQUEST['ajax_module'] and $_REQUEST['ajax_type']) {
	include_once SERVER."local/themes/".$theme."/modules/".m($_REQUEST['ajax_module'].'.path_themes',$_REQUEST['ajax_type'])."includes/ajax.php";
	include_once SERVER.m($_REQUEST['ajax_module'].'.path_local',$_REQUEST['ajax_type'])."includes/ajax.php";
}
// Local - module
if($_REQUEST['ajax_module']) {
	include_once SERVER."local/themes/".$theme."/modules/".m($_REQUEST['ajax_module'].'.path_themes')."includes/ajax.php";
	include_once SERVER.m($_REQUEST['ajax_module'].'.path_local')."includes/ajax.php";
}
// Local - plugin
if($_REQUEST['ajax_plugin']) {
	include_once SERVER."local/themes/".$theme."/plugins/".plugin($_REQUEST['ajax_plugin'].'.path_themes')."includes/ajax.php";
	include_once SERVER.plugin($_REQUEST['ajax_plugin'].'.path_local')."includes/ajax.php";
}

// Core - module + type
if($_REQUEST['ajax_module'] and $_REQUEST['ajax_type'] and m($_REQUEST['ajax_module'].'.core')) {
	include_once SERVER.m($_REQUEST['ajax_module'].'.path',$_REQUEST['ajax_type'])."includes/ajax.php";
}
// Core - module
if($_REQUEST['ajax_module'] and m($_REQUEST['ajax_module'].'.core')) {
	include_once SERVER.m($_REQUEST['ajax_module'].'.path')."includes/ajax.php";
}
// Core - plugin
if($_REQUEST['ajax_plugin'] and plugin($_REQUEST['ajax_plugin'].'.core')) {
	include_once SERVER.plugin($_REQUEST['ajax_plugin'].'.path')."includes/ajax.php";
}

// Local - theme
include_once SERVER."local/themes/".$theme."/includes/ajax.php";

/************************************************************************************/
/************************************** Scripts *************************************/
/************************************************************************************/
/**
 * Saves a submitted item form.
 */
if($_POST['ajax_action'] == "save") {
	$module = ($_POST['ajax_module'] ? $_POST['ajax_module'] : $_POST['process_module']);
	if($module) {
		// Form
		$m = module::load($_POST['process_module']);
		$form = $m->form();
		// Process
		$results = $form->process(g('unpure.post'),g('unpure.files'),array('debug' => 0));
		// Errors
		if($results[errors]) {
			if($results[c][url] and (!$results[c][debug] or !debug_active())) redirect($results[c][url]);
			else if(debug_active()) print "
<div class='core-red'>
	<b>Errors:</b>
	".return_array($results[errors])."
</div>";
		}
		// No errors
		else {
			// Item
			$item = $m->item($results[id]);
			
			// Save
			$id = $item->save($results[values],array('debug' => $results[c][debug]));
		
			// Result - form field // Moved into on_success function when save() called in 'form_add_option'
			/*if($_POST['result'] == "form_add_option_load") {
				$input = $_POST['input'];
				$input[value] = $id;
				print $form->input_html($input);
			}
			// Result - id (default)
			else*/ print $id;
		}
	}
	
	// Exit
	exit;
}

/**
 * Saves a submitted plugin form.
 */
if($_POST['ajax_action'] == "plugin_save") {
	if($_POST['ajax_plugin']) {
		// Plugin
		$p = plugin_item::load($_POST['ajax_module'],$_POST['ajax_id'],$_POST['ajax_plugin'],$_POST['id']);
		
		// Form
		$form = $p->form();
		
		// Process
		$results = $form->process(g('unpure.post'),g('unpure.files'),array('debug' => 0));
		
		// Errors
		if($results[errors]) {
			if($results[c][url] and (!$results[c][debug] or !debug_active())) redirect($results[c][url]);
			else if(debug_active()) print "
<div class='core-red'>
	<b>Errors:</b>
	".return_array($results[errors])."
</div>";
		}
		// No errors
		else {
			// Save
			$id = $p->save($results[values],array('debug' => $results[c][debug]));
			
			// Result
			print $id;
		}
	}
	
	// Exit
	exit;
}

/**
 * Adds the given $_POST['input'] input array to the given $_POST['form'].
 */
if($_GET['ajax_action'] == "form_add_input") {
	if($_POST['form'] and $_POST['input']) {
		// Input
		$input = $_POST['input'];
		$input[hash] = 0;
		
		// Add to form
		$form = new form_framework();
		$form->cache_restore($_POST['form']);
		$form->inputs(array($input));
		$form->cache_save();
		
		// Display
		print $form->input_render($input);
	}
	
	// Exit
	exit;
}

/**
 * Displays form for adding a new item which will then be the selected option in a form element.
 */
if($_GET['ajax_action'] == "form_add_option") {
	if($_GET['ajax_module']) {
		// Random
		$r = random();
		
		// Input
		$input = unserialize(urldecode(g('unpure.get.input')));
		
		// On Success
		$on_success = s($_GET['on_success']);
		if(!$on_success) {
			$name = str_replace(array('[',']'),'',$input->name);
			$on_success = "function(result,form,c) {form_add_success(result);}";
		}
		$on_success = string_strip_breaks($on_success);
		
		// Form
		$m = module::load($_GET['ajax_module']);
		$form = $m->form(array('type' => $_GET['type'],'id' => 'form_add_option_'.$r,'action' => "javascript:save('form_add_option_".$r."',{on_success:".$on_success.",on_success_overwrite:1});"));
		$form->hidden('ajax_action','save');
		$form->hidden('ajax_module',$_GET['ajax_module']);
		$form_html = $form->render();
		
		// Type
		if(strstr($form_html," name='type'") or strstr($form_html,' name="type"')) {
			unset($_GET['type']); // Don't want to include in ajax URL as we'll be adding it in via javascript
			$action = 'javascript:form_add_option_type();';
			$form_html = "
<script type='text/javascript'>
function form_add_option_type() {
	var type = $('#form_add_option_".$r." :input[name=type]').val();
	$.ajax({
		type: 'GET',
		url: DOMAIN+'?type='+type+'&".http_build_query(g('unpure.get'))."',
		success: function(html){
			$('#form_add_option').html(html);
		}
	});
}
</script>
".preg_replace("/action=(['|\"])(.*?)(['|\"]) /","action=$1".$action."$3 ",$form_html);
		}
		
		// Display
		print "
<div id='form_add_option'>
	<script type='text/javascript'>
	var form_add_value = '';
	$(document).ready(function() {
		form_add_value = $(\":input[name='".$input->name."']".(in_array($input->type,array('checkbox','radio')) ? ":checked" : "")."\").val();
	});
	function form_add_success(result) {
		$.ajax({
			type: 'POST',
			url: DOMAIN,
			data: 'ajax_action=form_add_option_load&ajax_module=".$_GET['ajax_module']."&form_module=".$_GET['form_module']."&".http_build_query(array('input' => serialize($input)))."&id='+result+'&value='+form_add_value+'&"./*http_build_query(array('parent' => $_GET['parent'])).*/"',
			success: function(html){
				debug('form_add_option_load result: <xmp>'+html+'</xmp>');";
				/*if($_GET['parent']['input']) print "
				$('#form_add_option').html(html);
				$('#form_add_option select').trigger('change');";
				else*/ print "
				$('#form-add-field-".$name."').html(html);
				$('#form-add-field-".$name." select').trigger('change');
				overlay_close();";
				print "
				js_refresh();
			}
		});
	}
	</script>
	".$form_html."
</div>";
	}
	
	// Exit
	exit;
}

/**
 * Reloads input after we added a new option to it.
 */
if($_POST['ajax_action'] == "form_add_option_load") {
	//print_array($_POST);
	// Form
	/*if($_POST['parent']['input']) {
		// Random
		$r = random();
		
		// On Success
		$on_success = s($_POST['parent']['on_success']);
		if(!$on_success) {
			$on_success = "
function(result,form,c) {
	$.ajax({
		type: 'POST',
		url: DOMAIN,
		data: 'ajax_action=form_add_option_load&ajax_module=".$_POST['parent']['ajax_module']."&".http_build_query(array('input' => $_POST['parent']['input']))."&id='+result+'&".http_build_query(array('parent' => $_POST['parent']['parent']))."',
		success: function(html){";
			if($_POST['parent']['parent']['input']) $on_success .= "
			$('#form_add_option').html(html);";
			else $on_success .= "
			$('#form-add-field-".str_replace(array('[',']'),'',$_POST['parent']['input']['name'])."').html(html);
			overlay_close();";
			$on_success .= "
		}
	});
}";
		}
		$on_success = string_strip_breaks($on_success);
		
		// Form
		$m = module::load($_POST['parent']['ajax_module']);
		$form = $m->form(array('type' => $_POST['parent']['type'],'id' => 'form_add_option_'.$r,'action' => "javascript:save('form_add_option_".$r."',{on_success:".$on_success.",on_success_overwrite:1});"));
		$form->hidden('ajax_action','save');
		$form->hidden('ajax_module',$_POST['parent']['ajax_module']);
		
		// Values
		$values = $_POST['parent']['values'];
		$values[$_POST['input']['name']] = $_POST['id'];
		foreach($form->inputs as $k => $v) {
			$form->inputs[$k][value] = $values[$v[name]];
		}
		
		// HTML
		print $form->render();
		
	}
	// Input
	else {*/
		// Form
		$form = new form_framework($_POST['form_module']);
		
		// Input object
		$input = unserialize(urldecode(g('unpure.post.input')));
		if(!x($input->options) or is_array($input->options)) {
			$item = item::load($input->c[options_module],$_POST['id']);
			$input->options[$_POST['id']] = $item->db('name');
		}
		$input->options_standardized = $input->options($input->options);
		
		// Input
		/*$input = NULL;
		// Input - get actual computed input from form
		foreach($form->inputs as $k => $i) {
			if($i[name] == $input_object->name) {
				$input = $i;
				break;
			}
		}
		// Input - use input we passed along
		if(!$input) $input = $input_object;
		// Input - option - in case we weren't able to add it for some reason
		if(is_array($input->options) and $input->c[options_module]) {
			$item = item::load($input->c[options_module],$_POST['id']);
			if($item->name) $input->options[$_POST['id']] = $item->name;
		}*/
		
		// Value
		$value = $_POST['value'];
		if(!is_array($value)) $value = array($value);
		$value[] = $_POST['id'];
		$input->value = $value;
		
		// HTML
		print $input->html_element($form);
	//}
	
	// Exit
	exit;
}

/**
 * Saves order as sorted via jquery													
 */
if($_POST['ajax_action'] == "sortIt") {
	if($_POST['module'] and m($_POST['module'].'.db.order')) {
		// Order
		$ex = explode('|',$_POST['order']);
		$x = ($_POST['start'] > 0 ? $_POST['start'] : 1);
		foreach($ex as $id) {
			if(x($id)) {
				// Module Item
				$item = item::load($_POST['module'],$id);
				$item->save(array('db.order' => $x),array('debug' => 1));
			}
			// Counter
			$x++;
		}
	}
	else if($_POST['plugin'] and plugin($_POST['plugin'].'.db.order')) {
		// Order
		$ex = explode('|',$_POST['order']);
		$x = ($_POST['start'] > 0 ? $_POST['start'] : 1);
		foreach($ex as $id) {
			if(x($id)) {
				// Module Item
				$plugin_item = plugin_item::load(NULL,NULL,$_POST['plugin'],$id);
				$plugin_item->save(array('db.order' => $x),array('debug' => 1));
			}
			// Counter
			$x++;
		}
	}
	else print "No module (".$_POST['module']."), no plugin (".$_POST['plugin'].") or db.order is undefined<br />";
	
	// Exit
	exit;
}

/**
 * Saves nested order as sorted via jquery											
 */
if($_POST['ajax_action'] == "nestIt") {
	// Timer
	$startTimer = microtime_float();
	debug("<b>nestIt</b>");
	
	// Order
	$order = g('unpure.post.order');
	$order = json_decode(s($order),1);
	debug("order: ".return_array($order));
	
	// Save Nested Items
	nest_save($_POST['module'],$order);
	
	// Debug
	$endTimer = microtime_float();
	debug("Took ".round($endTimer - $startTimer, 4)." seconds<br />---------");
	print debug_html();
	
	// Exit
	exit;
}

/**
 * Delete's an item and its related data.
 */
if($_POST['ajax_action'] == "delete") {
	if($_POST['module'] and $_POST['id'] > 0) {
		// Item
		$item = item::load($_POST['module'],$_POST['id']);
		// Permission
		if($item->permission('delete')) {
			// Delete
			$item->delete(array('debug' => 1));
		}
	}
	
	// Exit
	exit;
}

/**
 * Delete's a plugin item and it's related data.
 */
if($_POST['ajax_action'] == "plugin_delete") {
	if($_POST['module'] /*and $_POST['id'] > 0*/ and $_POST['plugin'] and $_POST['plugin_id'] > 0) {
		// Item
		$plugin_item = plugin_item::load($_POST['module'],$_POST['id'],$_POST['plugin'],$_POST['plugin_id']);
		// Permission
		if($plugin_item->permission('delete')) {
			// Delete
			$plugin_item->delete(array('debug' => 1));
		}
	}
	
	// Exit
	exit;
}

/**
 * Disables/enables an item.
 */
if($_POST['ajax_action'] == "disable") {
	if($_POST['module'] and $_POST['id'] > 0) {
		// Plugin item
		if($_POST['plugin']) {
			if($_POST['plugin_id'] > 0) {
				// Plugin item
				$plugin_item = plugin_item::load($_POST['module'],$_POST['id'],$_POST['plugin'],$_POST['plugin_id']);
				// Permission
				if($plugin_item->permission('disable')) {
					// Delete
					$plugin_item->disable(array('debug' => 1));
				}
			}
		}
		// Item
		else {
			// Item
			$item = item::load($_POST['module'],$_POST['id']);
			// Permission
			if($item->permission('disable')) {
				// Delete
				$item->disable(array('debug' => 1));
			}
		}
	}
	
	// Exit
	exit;
}

/**
 * Gets the price for the item we're viewing, taking into account options/quantity/etc.
 */
if($_POST['ajax_action'] == "cart_price") {
	// Config
	$c = $_POST;
	$c[debug] = 0;
	unset($c[module],$c[id],$c[quantity][count],$c[ajax_action],$c[ajax_module]);
	
	// Cart
	$cart = cart::load($_POST['module']);
	
	// Add
	$item = $cart->item_add($_POST['id'],$_POST['quantity']['count'],$c);
	
	// Price
	$price = $item[price];
	
	// Remove
	$item[quantity][count] -= $_POST['quantity']['count'];
	if(!$item[quantity][count]) $cart->item_delete($item[key]);
	else $cart->item_update($item[key],$item,array('debug' => $c[debug]));
	
	// Return
	print string_price($price);
	
	// Exit
	exit;
}

/**
 * Calculates shipping rates to given address.
 */
if($_POST['ajax_action'] == "cart_shipping") {
	// Debuggin start
	ob_start();
	print "post: ".return_array($_POST);
		
	// Address
	$address = $_POST['address'];
	
	// Cart
	$cart = cart::load($_POST['ajax_module']);
	
	// Saved
	if($address > 0) {
		$cart->shipping_address($address);
	}
	// New
	else if($address == "new" and $_POST['country'] and $_POST['state'] and $_POST['zip']) {
		$address_array = array(
			'country' => country_code($_POST['country']),
			'state' => state_code($_POST['state']),
			'zip' => $_POST['zip'],
		);
		$cart->shipping_address($address_array);
	}
	// Empty
	else {
		$cart->shipping[addresses] = NULL;
	}
	
	// Selected
	$selected = $_POST['selected'];
	if($selected == "undefined") $selected = NULL;
	
	// Shipping
	$shipping = $cart->form_checkout_cart_shipping(array('selected' => $selected,'debug' => 1));
	
	// Debugging end
	$debug = ob_get_contents();
	ob_end_clean();
		
	// Return
	$array = array(
		'tax' => $cart->totals[tax],
		'shipping' => $shipping,
		'subtotal' => ($cart->totals[total] - $cart->totals[shipping]),
		'debug' => $debug,
	);
	print json_encode($array);
	
	// Exit
	exit;
}

/**
 * Displays for adding a discount to our cart.
 */
if($_GET['ajax_action'] == "cart_discount_add") {
	// Page
	$page = page::load();
	
	// Header
	print $page->header();

	// Form
	$form_c = array(
		'action' => D, // Need an action otherwise it'll just revert to this ajax call (as it'll be posting to this url)
	);
	$form = form::load($form_c);
	$form->text('Promo Code','discount_code',NULL,array('help' => "Please enter the promo code for the discount you'd like to apply to your order.",'validate' => array('required' => 1)));
	$form->hidden('process_action','cart_discount_add');
	$form->hidden('process_module',$_GET['ajax_module']);
	print $form->render();
	
	// Exit
	exit;
}

/**
 * Removes a discount from the cart.
 */
if($_POST['ajax_action'] == "cart_discount_delete") {
	// Module
	$module = $_POST['ajax_module'];
	// Cart
	$cart = cart::load($module);
	
	// Remove
	$cart->discount_delete(urldecode($_POST['discount']));
	
	// Return
	$array = array(
		'tax' => $cart->totals[tax],
		'discounts' => $cart->totals[discounts],
		'subtotal' => ($cart->totals[total] - $cart->totals[shipping]),
	);
	print json_encode($array);
	
	// Exit
	exit;
}

/**
 * Documentation
 */
if($_GET['ajax_action'] == "documentation") {
	$documentation = NULL;
	$module = $_GET['ajax_module'];
	$id = $_GET['id'];
	$plugin = $_GET['ajax_plugin'];
	$file = urldecode($_GET['file']);
	
	// File
	if($file) {
		if(substr($file,0,strlen(SERVER)) == SERVER) {
			$documentation = $core->html_get($file);
		}
	}
	else {
		// Plugin
		if($plugin) $class = plugin::load($module,$id,$plugin);
		// Item // Skipping passing item for now as $this would then be the 'item' class and using $this->v() inside the documentation HTML would be completely different
		//else if($id > 0) $class = item::load($module,$id);
		// Module
		else if($module) $class = module::load($module);
		
		// Documentation
		if($class) $documentation = $class->documentation();
	}
	
	print "
<div class='documentation'>";
	
	// Display
	if($documentation) {
		print $documentation;	
	}
	else {
		print "
	<div class='core-none'>
		We couldn't find this documentation.
	</div>";	
	}
	
	print "
</div>";
	
	// Exit
	exit;
}
?>