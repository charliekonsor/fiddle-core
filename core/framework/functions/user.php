<?php
/**
 * @package kraken
 */

if(!function_exists('user_name_exists_framework')) {
	/**
	 * Checks whether the given username already belongs to a user in the database (besides the $user id).
	 * 
	 * @param string $name The username we want to check for.
	 * @param int $user The id of the user who should already have this username assigned to them. So, when checking if an existing user's username already exists, we'd want to return false if only they have that username.
	 * @return boolean Whether or not the username address already belongs to a user (other than the $user user).
	 */
	function user_name_exists_framework($name,$user = NULL) {
		$name = trim($name);
		if($name) {
			// Types
			$types = NULL;
			foreach(m('users.types') as $type => $v) {
				if($v[settings][registered]) {
					$types[] = $type;	
				}
			}
			
			// Query
			$m = module::load('users');
			$row = $m->row(array('where' => "user_name = '".a($name)."' AND user_id != '".$user."' AND type_code IN ('".implode("','",$types)."')"));
	
			// Exists
			if($row[user_id]) return true;
			// Doesn't exist
			else return false;
		}
		else return true;
	}
}

if(!function_exists('user_email_exists_framework')) {
	/**
	 * Checks whether the given e-mail already belongs to a user in the database (besides the $user id).
	 * 
	 * @param string $email The e-mail we want to check for.
	 * @param int $user The id of the user who should already have this e-mail assigned to them. So, when checking if an existing user's e-mail already exists, we'd want to return false if only they have that e-mail.
	 * @return boolean Whether or not the e-mail address already belongs to a user (other than the $user user).
	 */
	function user_email_exists_framework($email,$user = NULL) {
		$email = trim($email);
		if($email) {
			// Types
			$types = NULL;
			foreach(m('users.types') as $type => $v) {
				if($v[settings][registered]) {
					$types[] = $type;	
				}
			}
			
			// Query
			$m = module::load('users');
			$row = $m->row(array('where' => "user_email = '".a($email)."' AND user_id != '".$user."' AND type_code IN ('".implode("','",$types)."')"));
			
			// Exists
			if($row[user_id]) return true;
			// Doesn't exist
			else return false;
		}
		else return true;
	}
}
?>