<?php
/************************************************************************************/
/************************************** Config **************************************/
/************************************************************************************/
$config = array(
	'name' => 'Types',
	'single' => 'Type',
	'plural' => 'Types',
	'icon' => 'types',
	'db' => array(
		'table' => 'types',
		'id' => 'type_id',
		'code' => 'type_code',
		'name' => 'type_name',
		'order' => 'type_order',
		'visible' => 'type_visible'
	),
	'permissions' => array(
		/*'add' => array( // Net yet setup
			'label' => 'Install',
			'manage' => 1,
			'default' => array()
		),
		'copy' => array( // Not sure I want this
			'label' => 'Copy',
			'manage' => 1,
			'default' => array()
		),*/
		'permissions' => array(
			'label' => 'Permissions',
			'manage' => 1,
			'default' => array(
				'admins',
			)
		),
		'settings' => array(
			'label' => 'Settings',
			'default' => array(
				'admins',
			)
		),
	),
	'admin' => array(
		'section' => 'system',
		'views' => array(
			'home' => 'All Types',
			'item' => 'Edit Type'
		),
		/*'urls' => array(
			'item' => array(
				'format' => 'admin/{module}/{code}'
			)
		),*/
	),
	'public' => array(
		'views' => array(),
		'urls' => array(
			'item' => array(
				'format' => '{module_code}/{code}'
			)
		)
	),
	'plugins' => array(
		'international' => 1,
	),
);
?>