<?php
/**
 * @package kraken\curl
 */
 
if(!function_exists('curl')) {
	/**
	 * Curls a given url and returns the contents	
	 *									
	 * Notes
	 * - When sending POST to ASP.net site, you may have to http_build_query() and then urlencode the array as ASP has some security that doesn't like post arrays
	 *
	 * @param string $url The URL you want to curl.
	 * @param array $c An array of configuration values. Default = NULL
	 * @return string The content returned by the curled URL. 
	*/
	function curl($url,$c = NULL) {
		//$f_r = function_speed(__FUNCTION__);
		if(!$c[post]) $c[post] = NULL; // POST data to send
		if(!x($c[follow])) $c[follow] = 1; // Follow Location
		if(!x($c[header])) $c[header] = 0; // Include the header in results
		if(!$c[httpheader]) $c[httpheader] = NULL; // HTTP header to send
		if(!$c[referer]) $c[referer] = DOMAIN; // Referer Value (usually your domain)
		if(!$c[cookie]) $c[cookie] = NULL; // Path to a .txt file or a string containing cookie info
		if(!$c[ssl_cert]) $c[ssl_cert] = NULL; // Path to SSL certificate (usually a .pem file I believe)
		if(!$c[timeout]) $c[timeout] = 60; // How long (in seconds) before the request times out
		//ini_set('user_agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.9) Gecko/20071025 Firefox/2.0.0.9');
		
		if(function_exists('curl_init')) {
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, $c[follow]);
			if($c[cookie]) {
				// Cookie File
				if(strstr($c[cookie],'.txt')) {
					curl_setopt($ch,CURLOPT_COOKIEJAR, $c[cookie]);
					curl_setopt($ch,CURLOPT_COOKIEFILE, $c[cookie]);
				}
				// Cookie variable
				else curl_setopt($ch, CURLOPT_COOKIE, $c[cookie]);
			}
			curl_setopt($ch, CURLOPT_HEADER, $c[header]);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
			curl_setopt($ch, CURLOPT_TIMEOUT, $c[timeout]);
			if($c[httpheader]) curl_setopt($ch, CURLOPT_HTTPHEADER, (is_array($c[httpheader]) ? $c[httpheader] : Array($c[httpheader])));
			if($c[post]) {
				if(is_array($c[post])) $c[post] = http_build_query($c[post]); // Pass as a string
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $c[post]);
			}
			curl_setopt($ch, CURLOPT_REFERER, $c[referer]);
			if($c[ssl_cert]) curl_setopt($ch, CURLOPT_SSLCERT, $c[ssl_cert]);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
			if(!$contents = curl_exec($ch)) {
				debug("Curl error: ".curl_error($ch)." (".curl_errno($ch).")");
			}
			
			curl_close ($ch);
		}
		else if(function_exists('file_get_contents')) {
			$contents = file_get_contents($url);
		}
		
		//function_speed(__FUNCTION__,$f_r);
		return $contents;
		
		
		/* Cookie variable example
		// $url_login = url login form points to
		// $post = array of keys/values from login form
		// $url_account = url that requires login
		$cookie = curl_cookie($url_login,array('post' => $post));
		$result = curl($url_account,array('cookie' => $cookie));
		*/
		
		/* Cookie file example
		// $url_login = url login form points to
		// $post = array of keys/values from login form
		// $cookie = path to file where we'll store cookie info (will be created if doesn't exist)
		// $url_account = url that requires login
		$cookie = SERVER."uploads/cookie.txt";
		curl($url_login,array('post' => $post,'cookie' => $cookie));
		$result = curl($url_account,array('cookie' => $cookie));
		*/
	}
}
?>