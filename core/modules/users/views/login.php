<?php
// Already logged in
if(u('id')) {
	// Redirect URL
	if($_GET['redirect']) $redirect = urldecode($_GET['redirect']);
	else {
		for($x = 1;$x <= 10;$x++) {
			if($_SESSION['urls'][$x] and !strstr($redirect,'login') and !strstr($redirect,'register')) {
				$redirect = $_SESSION['urls'][$x];
				$x = 11;
			}
		}
	}
	if(!$redirect) $redirect = DOMAIN;
	
	// Redirect
	redirect($redirect);
}
// Login
else {
	// Field
	$field = $this->module_class->login_field();
	// Label
	$label = $this->module_class->login_label();
	
	print "
<div class='page page-module-".$this->module." page-view-".$this->view."'>";
	// Login
	if(!$this->page->variables) {
		print "
	<h1 class='page-heading'>Login</h1>
	".$this->login_form();
	}
	
	// Forgot Password
	if($this->page->variables[0] == "lost_password") {
		// Send
		if($_POST['user'] or $_POST['email']) {
			$query = "SELECT * FROM users WHERE (";
			if($field == "either") {
				if($_POST['user']) $query .= "user_name = '".a($_POST['user'])."'";
				if($_POST['user'] and $_POST['email']) $query .= " OR ";
				if($_POST['email']) $query .= "user_email = '".a($_POST['email'])."' OR user_name = '".a($_POST['email'])."'";
			}
			else $query .= $field." = '".a($_POST['user'])."'";
			$query .= ") AND user_visible = 1";
			$row = $this->db->f($query);
			if($row[user_password]) {
				// User
				$user = item::load('users',$row);
				
				// Hash
				for($x = 0;$x < 1;) {
					$hash = string_random();
					$row = $this->db->f("SELECT * FROM users_resets WHERE reset_hash = '".a($hash)."'");
					if(!$row[reset_id]) $x = 1;
				}
				$this->db->q("INSERT INTO users_resets SET user_id = '".$user->id."', reset_type = 'password', reset_hash = '".a($hash)."', reset_created = '".time()."'");
				$url = DOMAIN."login/reset_password?hash=".$hash;
				
				// E-mail
				$to = $user->v('user_email');
				if(!$to and strstr($user->v('user_name'),'@')) $to = $user->v('user_name');
				if(debug_active() and !strstr(DOMAIN,"daedalus")) $to = g('config.debug.email');
				$subject = "Reset Your ".m('settings.settings.site.name')." Password";
				$contents = "
Hello ".$user->name.",<br /><br />

Please follow the link below to reset your ".m('settings.settings.site.name')." password:<br /><br />

<a href='".$url."'>".$url."</a><br /><br />

-".m('settings.settings.site.name');
				include_function('email');
				email($to,$subject,$contents);
				
				list($username,$provider) = explode('@',s($user->v('user_email')));
				print "
	<h1>Reset Directions Sent</h1>
	<div class='login-lost'>
		Directions on how to reset your password have been sent to ".$to.".";/*<br /><br />
		<input type='button' value='Go to $provider' onclick=\"window.open('http://".$provider."');\" />*/
				print "
	</div>";
			}
			else {
				page_error("We have no record of this ".strtolower($label),1);
				redirect(D."login/lost_password".SITE_EXT);
			}
		}
		// Submit
		else {
			print "
	<h1>Forgot Password</h1>
	<div class='login-lost'>
		Please enter your ".str_replace('/',' or ',strtolower($label))." and we will send you directions on how you can reset your password.<br /><br />
		<form action='".D."login/lost_password' method='post' class='require'>";
			if($field == "either") {
				print "
			<table class='core-table'>
				<tr>
					<td width='80'>Username:</td>
					<td><input type='text' name='user' /></td>
				</tr>
				<tr>
					<td></td>
					<td>-or-</td.
				</tr>
				<tr>
					<td width='80'>E-mail:</td>
					<td><input type='text' name='email' /></td>
				</tr>
			</table>";
			}
			else print "
			".$label.": <input type='text' name='user' class='required' />";
			print "
			<input type='submit' value='Submit' style='margin-top:9px;' />
		</form>
	</div>";
		}
	}
	
	// Reset Password
	if($this->page->variables[0] == "reset_password") {
		// Save
		if($_POST['user_password'] and $_POST['hash']) {
			// Process
			$form = form::load();
			$results = $form->process(g('unpure.post'));
			
			// Error
			if($results[error]) $error = "There was an error resetting your password. Please correct the ".string_pluralize("error",count($results[errors][inline]))." shown below.";
			
			// Hash
			$row = $this->db->f("SELECT * FROM users_resets WHERE reset_hash = '".a($results[values]['hash'])."' AND reset_type = 'password' AND reset_created >= '".(time() - (86400 * 30))."'");
			if(!$row[user_id]) $error = "This link has expired.";
			
			// Error
			if($error) {
				page_error($error);
				redirect($results[url]);	
			}
			// Success
			else {
				// Save
				$item = item::load('users',$row[user_id]);
				$item->save(array('user_password' => $results[values][user_password]));	
				
				// Login
				$this->login($row[user_id],array('source' => "resetpassword"));
				
				// Message
				page_message("Your password has been successfully reset.");
				
				// Redirect
				redirect(DOMAIN);
			}
		}
		// Reset
		else {
			// Hash
			if($_GET['hash']) $row = $this->db->f("SELECT * FROM users_resets WHERE reset_hash = '".a($_GET['hash'])."' AND reset_type = 'password' AND reset_created >= '".(time() - (86400 * 30))."'");
			if(!$_GET['hash'] or !$row[reset_id]) {
				print "
		<h1>Link Expired</h1>
		
		This link has expired. <a href='".DOMAIN."login/lost_password'>Click here</a> to resend the directions to reset your password.";
			}
			// Form
			else {
				print "
		<h1>Reset Password</h1>
		
		Please enter a new password:";
				$form = form::load(array('submit' => array('value' => 'Reset Password')));
				$form->input(form_input_password::load(NULL,'user_password',NULL,array('validate' => array('required' => 1))));
				$form->input(form_input_hidden::load('hash',$_GET['hash']));
				print $form->render();
			}
		}
	}
	
	// Forgot Username
	if($this->page->variables[0] == "lost_username" and $field != "user_email") {
		// Label
		$label = "Username";
		// Send
		if($_POST['email']) {
			$row = $this->db->f("SELECT * FROM users WHERE user_email = '".a($_POST['email'])."' AND user_visible = 1");
			if($row[user_name]) {
				// User
				$user = item::load('users',$row);
				
				// Hash
				for($x = 0;$x < 1;) {
					$hash = string_random();
					$row = $this->db->f("SELECT * FROM users_resets WHERE reset_hash = '".a($hash)."'");
					if(!$row[reset_id]) $x = 1;
				}
				$this->db->q("INSERT INTO users_resets SET user_id = '".$user->id."', reset_type = 'username', reset_hash = '".a($hash)."', reset_created = '".time()."'");
				$url = DOMAIN."login/reset_username?hash=".$hash;
				
				// E-mail
				$to = $user->v('user_email');
				if(!$to and strstr($user->v('user_name'),'@')) $to = $user->v('user_name');
				if(debug_active() and !strstr(DOMAIN,"daedalus")) $to = g('config.debug.email');
				$subject = "Reset Your ".m('settings.settings.site.name')." ".$label;
				$contents = "
Hello ".$user->name.",<br /><br />
			
Please follow the link below to reset your ".m('settings.settings.site.name')." ".strtolower($label).":<br /><br />

<a href='".$url."'>".$url."</a><br /><br />

-".m('settings.settings.site.name');
				include_function('email');
				email($to,$subject,$contents);
				
				list($username,$provider) = explode('@',s($user->v('user_email')));
				print "
	<h1>Reset Directions Sent</h1>
	<div class='login-lost'>
		Directions on how to reset your username have been sent to ".$to.".";/*<br /><br />
		<input type='button' value='Go to $provider' onclick=\"window.open('http://".$provider."');\" />*/
				print "
	</div>";
			}
			else {
				page_error("We have no record of this e-mail address",1);
				redirect(D."login/lost_username".SITE_EXT);
			}
		}
		// Submit
		else {
			print "
	<h1>Forgot Username</h1>
	<div class='login-lost'>
		Please enter the e-mail address you used when registering and we will send you directions on how you can reset your ".strtolower($label).".<br /><br />
		<form action='".D."login/lost_username' method='post' class='require'>
			E-mail: <input type='text' name='email' class='required' />
			<input type='submit' value='Submit' style='margin-top:9px;' />
		</form>
	</div>";
		}
	}
	
	// Reset Username
	if($this->page->variables[0] == "reset_username") {
		// Save
		if($_POST['user_name'] and $_POST['hash']) {
			// Process
			$form = form::load();
			$results = $form->process(g('unpure.post'));
			
			// Error
			if($results[error]) $error = "There was an error resetting your username. Please correct the ".string_pluralize("error",count($results[errors][inline]))." shown below.";
			
			// Hash
			$row = $this->db->f("SELECT * FROM users_resets WHERE reset_hash = '".a($results[values]['hash'])."' AND reset_type = 'username' AND reset_created >= '".(time() - (86400 * 30))."'");
			if(!$row[user_id]) $error = "This link has expired.";
			
			// Double check username
			if(user_name_exists_framework($results[values][user_name],$row[user_id])) {
				$error = "This username is already taken.";
			}
			
			// Error
			if($error) {
				page_error($error);
				redirect($results[url]);	
			}
			// Success
			else {
				// Save
				$item = item::load('users',$row[user_id]);
				$item->save(array('user_name' => $results[values][user_name]));	
				
				// Login
				$this->login($row[user_id],array('source' => "resetusername"));
				
				// Message
				page_message("Your username has been successfully reset.");
				
				// Redirect
				redirect(DOMAIN);
			}
		}
		// Reset
		else {
			// Hash
			if($_GET['hash']) $row = $this->db->f("SELECT * FROM users_resets WHERE reset_hash = '".a($_GET['hash'])."' AND reset_type = 'username' AND reset_created >= '".(time() - (86400 * 30))."'");
			if(!$_GET['hash'] or !$row[reset_id]) {
				print "
		<h1>Link Expired</h1>
		
		This link has expired. <a href='".DOMAIN."login/lost_username'>Click here</a> to resend the directions to reset your username.";
			}
			// Form
			else {
				print "
		<h1>Reset Username</h1>
		
		Please enter a new username:";
				
				// Get javascript validation from actual form...might want to get user_name input from there too // Seems to cause more issues than it fixes (checkUsername ajax doesn't work because no user id on form class
				/*$item = item::load('users',$row[user_id]);
				$item_form = $item->form();
				foreach($item_form->inputs as $input) {
					//print_array($input);
					if($input->type == "html" and strstr($input->value,'.validate(')) {
						$validation_input = $input;
						break;
					}
				}*/
				
				$form_c = array(
					'submit' => array(
						'value' => 'Reset Username'
					)
				);
				if($validation_input) {
					$form_c[attributes]['class'] = "register";
					$form_c[validate] = 0;
				}
				$form = form::load($form_c);
				$form->input(form_input_text::load(NULL,'user_name',NULL,array('validate' => array('required' => 1))));
				$form->input(form_input_hidden::load('hash',$_GET['hash']));
				if($validation_input) $form->input($validation_input);
				print $form->render();
			}
		}
	}
	
	print "
</div>";
}
?>