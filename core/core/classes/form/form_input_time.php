<?php
if(!class_exists('form_input_time',false)) {
	/**
	 * Creates a time input.
	 */
	class form_input_time extends form_input_framework {
		/**
		 * Loads class. Needed so we can use debug_backtrace() to get this class name in PHP < 5.3 in $form_input->load().
		 */
		static function load($label = NULL,$name = NULL,$value = NULL,$c = NULL) {
			return parent::load($label,$name,$value,$c);
		}
		
		/**
		 * Construct
		 */
		function __construct($label = NULL,$name = NULL,$value = NULL,$c = NULL) {
			$this->form_input_time($label,$name,$value,$c);
		}
		function form_input_time($label = NULL,$name = NULL,$value = NULL,$c = NULL) {
			// Construct
			parent::__construct($label,$name,$value,$c);
			
			// Config
			if(!$this->c[hours]) $this->c[hours] = 12; // What hour format do you want to use: 12 [default, includes AM/PM], 24
			if(!$this->c[minutes]) $this->c[minutes] = 1; // Increment between minutes (ex: $this->c[minutes] = 15; options = 00, 15, 30, 45)
		}
		
		/**
		 * Creates the HTML for an input
		 *
		 * @param object $form The form object this input is a part of.
		 * @param array $c An array of config values. Default = NULL
		 * @return string The HTML element for the input.
		 */
		function html_element($form,$c = NULL) {
			// Name
			$name = str_replace(array('[',']'),'',$this->name);
			
			// Parse
			if($this->value) list($hour,$minute,$second) = explode(':',$this->value);
			$meridiem = "am";
			
			// Hour
			if($this->c[hours] == 24) {
				$start = 0;
				$end = 23;
			}
			else {
				if($hour >= 12) {
					$meridiem = "pm";	
					if($hour > 12) $hour -= 12;
				}
				$start = 1;
				$end = 12;
			}
			$html .= "
		<select name='".$name."_hour'>
			<option value=''>Hr</option>";
			for($x = $start;$x <= $end;$x++) $html .= "
			<option value='".$x."'".($this->value && $hour == $x ? " selected='selected'" : '').">".str_pad($x,2,0,STR_PAD_LEFT)."</option>";
			$html .= "
		</select> :";
		
			// Minute
			$value = $minute;
			$html .= "
		<select name='".$name."_minute'>
			<option value=''>Min</option>";
			for($x = 0;$x < 60;$x += $this->c[minutes]) $html .= "
			<option value='".$x."'".($this->value && $minute == str_pad($x,2,0,STR_PAD_LEFT) ? " selected='selected'" : '').">".str_pad($x,2,0,STR_PAD_LEFT)."</option>";
			$html .= "
		</select>";
		
			// Meridiem
			if($this->c[hours] != 24) {
				$html .= "
		<select name='".$name."_meridiem'>
			<option value='am'".($meridiem == "am" ? " selected='selected'" : '').">AM</option>
			<option value='pm'".($meridiem == "pm" ? " selected='selected'" : '').">PM</option>
		</select>";
			}
		
			$html .= "
		<label class='error form-error' for='".$this->name."'".($this->c[errors_inline] && $this->name && $this->errors[inline][$this->name] ? " style='display:inline;'>".$this->errors[inline][$this->name] : ">Please select a time.")."</label>";
			
			// Return
			return $html;
		}
			
		/**
		 * Processes the value for this input.
		 *
		 * @param object $form The form object which contains the submitted values ($form->post and $form->files).
		 * @param array $c An array of configuration values. Default = NULL
		 * @return mixed The resulting value.
		 */
		function process($form,$c = NULL) {
			// Value
			$value = parent::process($form,$c);
			
			// Name (used for hour, minute ,and meridiem values)
			$name = str_replace(array('[',']'),'',$this->name);
			
			// Values
			$value = NULL;
			$hour = $form->post[$name.'_hour'];
			$minute = $form->post[$name.'_minute'];
			$meridiem = $form->post[$name.'_meridiem'];
			
			// Debug
			debug("hour: ".$hour.", minute: ".$minute.", meridiem: ".$meridiem,$this->c[debug]);
			
			// Value
			if(x($hour) and x($minute)) {
				if($this->c[hours] == 12) {
					if($hour == "12" and $meridiem == "am") $hour = 0;
					if($meridiem == "pm" and $hour < 12) $hour += 12;
				}
				$value = str_pad($hour,2,0,STR_PAD_LEFT).":".str_pad($minute,2,0,STR_PAD_LEFT);
			}
			
			// Return
			return $value;
		}
	}
}
?>