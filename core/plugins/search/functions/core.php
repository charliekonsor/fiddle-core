<?php
/**
 * @package kraken\plugins\search
 */
 
if(!function_exists('search_index')) {
	/**
	 * Recreates search index for all 'searchable' items or for all searchabled items in a specific module (if given).
	 * 
	 * @param string $module The module you want to re-index. Default = NULL
	 * @param array $c An array of configuration values. Default = NULL
	 */
	function search_index($module = NULL,$c = NULL) {
		$f_r = function_speed(__FUNCTION__);
		// Database
		$db = new db();
		
		// Config
		if(!$c[fields]) $c[fields] = NULL; // An array of fields to index in array(field => weight) format. Will default to module defined fields or global fields momentarily.
		if(!$c[stops]) { // Stop words
			if(!$c[stops] = cache_get('search/stops')) {
				$rows = $db->q("SELECT * FROM items_index_stops WHERE stop_visible = 1");
				while($row = $db->f($rows)) $c[stops][s($row[stop_word])] = $row[stop_score];
				cache_save('search/stops',$c[stops]);
			}
		}
		if(!x($c[debug])) $c[debug] = 0; // Debug
		
		// Default fields // Now pre-defined in $config_global when creating modules array
		/*$fields_default = NULL;
		if(!$c[fields]) {
			foreach(g('framework.db') as $k => $v) {
				if($v[search]) $fields_default["db.".$k] = $v[weight];
			}
		}*/
		
		// Empty table // Do inside $item->process_index()
		//if(!$module) $db->q("TRUNCATE TABLE `items_index`");
		
		// Modules
		foreach(m() as $code => $v) {
			if(!$module or $module == $code) {
				
				// Delete module's old indexed values // Do inside $item->process_index()
				//if($module) $db->q("DELETE FROM `items_index` WHERE module_code = '".$module."'");
				
				// Types
				if(!$v[types]) $v[types][0] = $v; // No Types, make one which has same variables as the module
				foreach($v[types] as $type => $type_v) {
					// Searchable?
					if(m($code.'.settings.search.public',$type) == 1) {
						// Fields
						$fields = NULL;
						if($c[fields]) $fields = $c[fields];
						if(!$fields) $fields = m($code.'.settings.search.columns',$type);
						//if(!$fields) $fields = $fields_default; // Now pre-defined in $config_global when creating modules array
						
						// Fields - standardize
						$fields = search_fields_standardize($code,$fields);
						
						// Debug
						debug("fields:".return_array($fields),$c[debug]);
		
						// Config
						$_c = array(
							'fields' => $fields,
							'stops' => $c[stops],
							'debug' => $c[debug]
						);
						
						// Query
						$query = "SELECT * FROM ".m($code.'.db.table',$type)." WHERE".(m($code.'.db.extra',$type) ? " AND ".m($code.'.db.extra',$type) : "").($type > 0 && m($code.'.db.type',$type) ? " AND ".m($code.'.db.type',$type)." = '".$type."'" : "");
						$query = trim(str_replace("WHERE AND","WHERE",$query));
						if(substr($query,-5) == "WHERE") $query = substr($query,0,strlen($query) - 6);
						
						// Index items
						debug($query,$c[debug]);
						$rows = $db->q($query);
						while($row = $db->f($rows)) {
							$item = item::load($code,$row);
							$item->process_index($_c);
						}
					}
				}
			}
		}
		function_speed(__FUNCTION__,$f_r);
	}
}

if(!function_exists('search_fields_standardize')) {
	/**
	 * Standardizes search fields, replacing global db variables with actual column and splitting up any 2 column global db variables.
	 * 
	 * @param string $module The module these fields are in.
	 * @param array $fields The array of fields you want to standardize.
	 * @return array The array of fields, now standardized.
	 */
	function search_fields_standardize($module,$array) {
		if($module and $array) {
			foreach($array as $field => $weight) {
				// Global database variable
				if(substr($field,0,3) == "db.") {
					$field = m($module.'.'.$field);
					// More than 1 column
					if(strstr($field,' ')) {
						$ex = explode(' ',$field);
						foreach($ex as $e) $array_new[$e] = $weight;
						$field = NULL;
					}
				}
				// Save
				if($field and $weight) $array_new[$field] = $weight;
			}
		}
		
		return $array_new;
	}
}

if(!function_exists('search_index_words')) {
	/**
	 * Prepares given text for indexing by removing punctuation, breaks, etc. then returns an array of words.
	 *
	 * If $c['return'] == "data" the array we return will be associative (so we can return whether it was a 'hashtag' word or not). Example:
	 * $array = array(
	 *		'hello' => array(
	 *			'word' => 'hello',
	 *			'hashtag' => 0,
	 *			'order' => 0
	 *		),
	 *		'world' => array(
	 *			'word' => 'world',
	 *			'hashtag' => 1,
	 *			'order' => 1
	 *		)
	 * );
	 *			
	 * 
	 * @param string $text The text you want to prepare for indexing. Default = NULL
	 * @param array $c An array of configuration values. Default = NULL
	 * @return array An array of words to be indexed.
	 */
	function search_index_words($text = NULL,$c = NULL) {
		//$f_r = function_speed(__FUNCTION__);
		if(!$c['return']) $c['return'] = "array"; // What to return: array [default], data (including if it's a hashtag, the order, etc.), string
		
		// Decode UTF-8
		//$text = utf8_decode($text);
		//debug("text (0.1): ".$text);
		
		//$text = html_entity_decode($text);
		//debug("text (0.5): ".$text);
		
		// Strip Tags
		$text = str_replace("&nbsp;", " ", strip_tags($text));

		// Remove alphanumeric characters
		$text = preg_replace('/&(.){2,4};/im','',$text);
		
		// Convert international characters to local characters
		$text = string_convert_accents($text);

		// Remove Breaks
		$text = str_replace("\t","",$text);
		while(substr_count($text, "\n")) $text = str_replace("\n"," ",$text);
		while(substr_count($text, "\r")) $text = str_replace("\r"," ",$text);
		
		// Add spaces around punctuation (and remove that punctuation)
		$text = str_replace(array(',','. ',';','/')," ",$text);
			
		// Remove non-alpha-numeric characters
		$text = preg_replace('/[^\w \-'.($c['return'] == "data" ? '\#' : '').']+/u','',$text);
		
		// Remove dashes (which aren't part of a word)
		$text = str_replace(" - "," ",$text);
		
		// Remove double spaces
		while(substr_count($text, "  ")) $text = str_replace("  "," ",$text);

		// All lowercase
		$text = strtolower($text);
		
		// Array
		if($c['return'] != "string") {
			// Explode Words
			$array = explode(' ',$text);
			// Remove emptys
			$array = array_filter($array);
			
			// Data
			if($c['return'] == "data") {
				// Loop through words
				$x = 0;
				$words = NULL;
				foreach($array as $word) {
					$hashtag = 0;
					// Hash tag?
					if(substr($word,0,1) == "#") $hashtag = 1;
					// Strip hashes
					$word = str_replace('#','',$word);
					// Store
					if(!$words[$word]) {
						$words[$word] = array(
							'word' => $word,
							'hashtag' => $hashtag,
							'order' => $x,
						);
						$x++;
					}
					else if($hashtag) $words[$word][hashtag] = $hashtag;
				}
				$array = $words;
			}
			// Array
			else {
				// Remove duplicates
				$array = array_unique($array);
			}
			
			// Return
			$return = $array;
		}
		// String
		else {
			$return = $text;
		}
		
		// Return
		//function_speed(__FUNCTION__,$f_r);
		return $return;
	}
}
?>