<?php
/************************************************************************************/
/************************************** Config **************************************/
/************************************************************************************/
$config = array(
	'name' => 'Trash',
	'single' => 'Trash',
	'plural' => 'Trash',
	'icon' => 'trash',
	'db' => array(
		'table' => 'items_trash',
		'id' => 'id',
		'parent_module' => 'module_code',
		'parent_id' => 'item_id',
		'created' => 'trash_created',
		'updated' => 'trash_updated'
	),
	// Yes, trashed plugin items can be trashed (and are by default)
	'plugins' => array(
		'trash' => 1,
	),
	'settings' => array(
		'trash' => array(
			'auto' => 1,
		),
	),
	// Settings you can set in module/plugin that enables this plugin
	/*'settings' => array(
		'trash' => array(
			'auto' => 1 // Do you want to automtically add items to the trash when deleted? If not, you'll have to pass 'delete' => 1 in the config array (1st param) when calling $item->delete(). Default = 1
		)
	),*/
);
?>