<?php
if(!class_exists('payment_googlecheckout',false)) {
	/**
	 * An 'extension' of the payment class used for processing payments through Google Checkout.
	 *
	 * Custom variables: infinite
	 *
	 * Test account:
	 * - merchant id: 764280823798613
	 * - merchant key: eaPScFRFnSYL7TizC6ok5Q
	 * - Other:
	 *   - E-mail: charlie@dwstudios.net
	 *   - Password: dwstudios
	 *   - SSN: 111111111
	 *   - Customer:
	 *    - E-mail: rich@dwstudios.net
	 *    - Password: richmanchan
	 *    - Card: 4111111111111111
	 *    - Expiration: 2/2018
	 *    - Code: 123
	 *
	 * References
	 * - https://developers.google.com/checkout/developer/Google_Checkout_XML_API_Processing#order_processing_api
	 * - http://code.google.com/p/google-checkout-php-sample-code/wiki/Documentation
	 *
	 * To do
	 * - reponse()
	 *   - get error message
	 *   - get currency from XML, pass in $currency variable to new GoogleRequest()
	 *   - Try to do the same form 'test' (what server we use), meaning get that from the XML sent to us
	 * - Support for $payment->custom array
	 * - Return 'custom' variables in returned results (if possible)
	 * - check for required fields before sending?
	 *
	 * Dependencies
	 * - Functions
	 *   - x() - __construct()
	 *
	 * @package kraken\payments
	 */
	class payment_googlecheckout {
		/** Stores the merchant id for your Google Checkout account. */
		public $merchant_id;
		/** Stores the merchant key for your Google Checkout account. */
		public $merchant_key;
		/** Stores an array of configuration values passed to the class. */
		public $c;
		
		/**
		 * Constructs the class.
		 *
		 * Configuration values (key, type, default - description):
		 * - test, boolean, 0 - Whether or not we want to process transactions via the testing 'demo' version.
		 *
		 * @param string $merchant_id The merchant id of your Google Checkout account.
		 * @param string $merchant_key The merchant key of your Google Checkout account.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function __construct($merchant_id,$merchant_key,$c = NULL) {
			self::payment_googlecheckout($merchant_id,$merchant_key,$c);
		}
		function payment_googlecheckout($merchant_id,$merchant_key,$c = NULL) {
			// Merchant ID
			$this->merchant_id = $merchant_id;
			// Merchant Key
			$this->merchant_key = $merchant_key;
			
			// Config
			if(!x($c[test])) $c[test] = 0;
			$this->c = $c;
		}
		
		/**
		 * Authorizes the customer's account for given amount.
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction including 'result' (boolean, 1 = succes, 0 = error) and 'message'.
		 */
		/*function authorize($payment,$c = NULL) {
			// Common errors
			if($error_common = $this->errors()) {
				$error = $error_common;
			}
			// No amount passed
			else if(!$payment->amount) {
				$error = "No authorization amount was passed.";
			}
			
			// Error
			if($error) {
				$results = array(
					'result' => 0,
					'message' => $error
				);	
			}
			// Transaction
			else {
				// Send
				$results = $this->send($payment,'authorize',$c);
			}
			
			// Return
			return $results;
		}
		
		/**
		 * Captures a transaction that was previously authorized via the authorize() method.
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction including 'result' (boolean, 1 = succes, 0 = error) and 'message'.
		 */
		/*function capture($payment,$c = NULL) {
			// Common errors
			if($error_common = $this->errors()) {
				$error = $error_common;
			}
			// No transaction ID
			else if(!$payment->transaction) {
				$error = "No transaction ID was passed that we could cancel.";
			}
			// No amount passed
			else if(!$payment->amount) {
				$error = "No capture amount was passed.";
			}
			
			// Error
			if($error) {
				$results = array(
					'result' => 0,
					'message' => $error
				);	
			}
			// Transaction
			else {
				// Send
				$results = $this->send($payment,'capture',$c);
			}
			
			// Return
			return $results;
		}
		
		/**
		 * Charges the customer a given amount.
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction including 'result' (boolean, 1 = succes, 0 = error) and 'message' as well as the transaction id ('transaction').
		 */
		function charge($payment,$c = NULL) {
			// Common errors
			if($error_common = $this->errors()) {
				$error = $error_common;
			}
			// No amount passed
			else if(!$payment->amount) {
				$error = "No charge amount was passed.";
			}
			
			// Error
			if($error) {
				$results = array(
					'result' => 0,
					'message' => $error
				);
			}
			// Transaction
			else {
				// Send
				$results = $this->send($payment,'charge',$c);
			}
			
			// Return
			return $results;
		}
		
		/**
		 * Refunds a customer/account a defined amount on a previous transaction.
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction including 'result' (boolean, 1 = succes, 0 = error) and 'message'.
		 */
		function refund($payment,$c = NULL) {
			// Common errors
			if($error_common = $this->errors()) {
				$error = $error_common;
			}
			// No transaction ID
			else if(!$payment->transaction) {
				$error = "No transaction ID was passed for the transaction we want to refund.";
			}
			// No amount passed
			else if(!$payment->amount) {
				$error = "No refund amount was passed.";
			}
			
			// Error
			if($error) {
				$results = array(
					'result' => 0,
					'message' => $error
				);	
			}
			// Transaction
			else {
				// Send
				$results = $this->send($payment,'refund',$c);
			}
			
			// Return
			return $results;
		}
		
		/**
		 * Cancels/voids the given transaction.
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction including 'result' (boolean, 1 = succes, 0 = error) and 'message'.
		 */
		function cancel($payment,$c = NULL) {
			// Common errors
			if($error_common = $this->errors()) {
				$error = $error_common;
			}
			// No transaction ID
			else if(!$payment->transaction) {
				$error = "No transaction ID was passed that we could cancel.";
			}
			
			// Error
			if($error) {
				$results = array(
					'result' => 0,
					'message' => $error
				);	
			}
			// Transaction
			else {
				// Send
				$results = $this->send($payment,'cancel',$c);
			}
			
			// Return
			return $results;
		}
		
		/**
		 * Credits a customer's credit card the given amount.
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction including 'result' (boolean, 1 = succes, 0 = error) and 'message'.
		 */
		/*function credit($payment,$c = NULL) {
			// Common errors
			if($error_common = $this->errors()) {
				$error = $error_common;
			}
			// No amount passed
			else if(!$payment->amount) {
				$error = "No credit amount was passed.";
			}
			
			// Error
			if($error) {
				$results = array(
					'result' => 0,
					'message' => $error
				);	
			}
			// Transaction
			else {
				// Send
				$results = $this->send($payment,'credit',$c);
			}
			
			// Return
			return $results;
		}
		
		/**
		 * Sends charge, authorize, and credit requests to the Google Checkout API.
		 *
		 * Configuration values (key, type, default - description):
		 * - url_cancel, string, NULL - The URL to send the customer to if they cancel the payment process or want to edit their cart.
		 * - url_return, string, NULL - The URL to send the customer to after they've completed payment.
		 * - reason, string, Cancelled (for 'cancel' transaactions) - The reason you are cancelling/refunding a previous transaction (only applies to 'cancel' and 'refund' actions, required for 'cancel' which is why we default it to 'Cancelled' in that instance).
		 * - comment, string, NULL - A comment on why you're cancelling a previous transaction (only applies to 'cancel' action).
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param string $action The transaction action we're performing.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction.  Since Google Checkout is an external payment service (we have to leave your site to process payments) this simply returns array('url' => http://www.thegooglecheckouturl.com/');
		 */
		function send($payment,$action,$c = NULL) {
			// Standardize
			$payment = $this->standardize($payment);
				
			// Server
			if($this->c[test]) $server = "sandbox";
			else $server = "production";
		
			// Charge
			if($action == "charge") {
				// Files
				require_once dirname(__FILE__)."/googlecheckout/googlecart.php";
				require_once dirname(__FILE__)."/googlecheckout/googleitem.php";
				require_once dirname(__FILE__)."/googlecheckout/googleshipping.php";
				require_once dirname(__FILE__)."/googlecheckout/googletax.php";
				
				// Class
				$cart = new GoogleCart($this->merchant_id,$this->merchant_key,$server,$payment->currency);
				
				// Items - eventually we'll check for the future $payment->items and loop through those if they exist, but for now (and in future if no $payment->items) we'll just add a single 'Payment' item.
				if($payment->items) {
					foreach($payment->items as $k => $v) {
						// Item
						$item = new GoogleItem($v[name],$v[description],$v[quantity],$v[price]); // Name, description, quantity, price
						if($v[id]) $item->SetMerchantItemId($v[id]);
						$cart->AddItem($item);
						
						// Subscription
						// Note! To keep the original payment and the subscription separate, we add the item (above) without subscription, then add the subscription starting one recurrence from now.
						if($v[subscription]) {
							// Class
							require_once dirname(__FILE__)."/googlecheckout/googlesubscription.php";
							
							// Period
							$period = NULL; // DAILY, WEEKLY, SEMI_MONTHLY, MONTHLY, EVERY_TWO_MONTHS, QUARTERLY, YEARLY
							if($v[subscription][length_unit] == "year" and $v[subscription][length] == 1 or $v[subscription][length_unit] == "month" and $v[subscription][length] == 12) $period = "YEARLY";
							if($v[subscription][length_unit] == "month" and $v[subscription][length] == 3) $period = "QUARTERLY";
							if($v[subscription][length_unit] == "month" and $v[subscription][length] == 2) $period = "EVERY_TWO_MONTHS";
							if($v[subscription][length_unit] == "month" and $v[subscription][length] == 1) $period = "MONTHLY";
							if($v[subscription][length_unit] == "week" and $v[subscription][length] == 1 or $v[subscription][length_unit] == "day" and $v[subscription][length] == 7) $period = "WEEKLY";
							if($v[subscription][length_unit] == "day" and $v[subscription][length] == 1) $period = "DAILY";
							
							// Item - which will hold the subscription but have no cost
							$item = new GoogleItem($v[name],$v[description],$v[quantity],0); // Name, description, quantity, price (0 as our price comes from the 'subscription' item (below))
							if($v[id]) $item->SetMerchantItemId($v[id]);
							
							// Subscription
							$start = strtotime("+".$v[subscription][length]." ".$v[subscription][length_unit]."s");
							$subscription_item = new GoogleSubscription("google", $period, $v[price], $v[subscription][times]); // type (who handles rebilling? google or merchant), period, maximum amount per transaction, times, item
							$recurrent_item = new GoogleItem($v[name],$v[description],$v[quantity],$v[price]); // Name, description, quantity, price
							if($v[subscription][id]) $recurrent_item->SetMerchantItemId($v[subscription][id]);
							$subscription_item->SetStartDate(date('c',$start)); // Want to start 1 'recurrence' after now as we're keeping first charge and subscription separate (so we can cancel subscription)
							$subscription_item->SetItem($recurrent_item);
							$item->SetSubscription($subscription_item);
							
							// Add to cart
							$cart->AddItem($item);
						}
					}
				}
				else {
					$item = new GoogleItem("Payment","Payment",1,($payment->total[subtotal] ? $payment->total[subtotal] : $payment->amount)); // Name, description, quantity, price
					$cart->AddItem($item);
				}
				
				// Discounts
				if($payment->total[discounts] > 0) {
					$name = "Discount";
					$item = new GoogleItem($name,$name,1,$payment->total[discounts]); // Name, description, quantity, price
					$cart->AddItem($item);
				}
				// Tax
				if($payment->total[tax] > 0) {
					$name = "Tax";
					$item = new GoogleItem($name,$name,1,$payment->total[tax]); // Name, description, quantity, price
					$cart->AddItem($item);
				}
				// Shipping
				if($payment->total[shipping] > 0) {
					$name = "Shipping";
					$item = new GoogleItem($name,$name,1,$payment->total[shipping]); // Name, description, quantity, price
					$cart->AddItem($item);
				}
				// Handling
				if($payment->total[handling] > 0) {
					$name = "Handling";
					$item = new GoogleItem($name,$name,1,$payment->total[handling]); // Name, description, quantity, price
					$cart->AddItem($item);
				}
				// Insurance
				if($payment->total[insurance] > 0) {
					$name = "Insurance";
					$item = new GoogleItem($name,$name,1,$payment->total[insurance]); // Name, description, quantity, price
					$cart->AddItem($item);
				}
				
				// Cancel / edit cart URL
				if($c[url_cancel]) $cart->SetEditCartUrl($c[url_cancel]);
				// Return URL
				if($c[url_return]) $cart->SetContinueShoppingUrl($c[url_return]);
				
				// Custom variables
				$custom = $payment->custom;
				// Custom variables - internal - these let us know what we were working with when we process the notification results
				$custom[__payment_action] = $action;
				$custom[__payment_test] = $this->c[test];
				// Custom variables - set
				$cart->SetMerchantPrivateData(new MerchantPrivateData($custom));
				
				// Debug
				if($this->c[debug]) {
					$xml = $cart->GetXML();
					print "xml: <xmp>".$xml."</xmp>";
				}
				
				// URL
				list($status, $result) = $cart->CheckoutServer2Server(NULL,NULL,false);
				if($status == 200) {
					$url = $result;
					$payment->debug("redirect url: ".$url,$c[debug]);
				}
				else {
					$payment->debug("status: ".$status.", error: ".$result,$c[debug]);
				}
				
				// Results
				$results = array(
					'url' => $url,
					'test' => $this->c[test],
					'source' => 'Google Checkout',
				);
			}
			
			if(in_array($action,array('cancel','refund'))) {
				// File
				require dirname(__FILE__)."/googlecheckout/googlerequest.php";
			
				// Request class
				$Grequest = new GoogleRequest($this->merchant_id,$this->merchant_key,$server,$payment->currency);
					
				// Cancel
				if($action == "cancel") {
					// Config
					if(!$c[reason]) $c[reason] = "Cancelled";
					if(!$c[comment]) $c[comment] = NULL;
					
					// Cancel
					$response = $Grequest->SendCancelOrder($payment->transaction,$c[reason],$c[comment]);
					$payment->debug("SendCancelOrder() response:".return_array($response),$c[debug]);
					if($response[0] == 200) {
						$results = array(
							'result' => 1,
							'message' => '',
							'transaction' => $response_parsed['request-received']['order-state-change-notification']['google-order-number'],
							'action' => $action,
							'test' => $this->c[test],
							'custom' => $payment->custom,
							'source' => 'Google Checkout',
							'response' => $response,
						);
					}
					// Couldn't cancel, try refunding (if payment has cleared, must refund first, then cancel)
					else if($payment->amount > 0) {
						// Refund
						$response_refund = $Grequest->SendRefundOrder($payment->transaction,$payment->amount,$c[reason],$c[comment]);
						$response_refund_parsed = $payment->xml_array($response_refund[1]);
						$payment->debug("SendRefundOrder() response:".return_array($response_refund),$c[debug]);
						if($response_refund[0] == 200) {
							$results = array(
								'result' => 1,
								'message' => '',
								'transaction' => $response_refund_parsed['request-received']['refund-amount-notification']['google-order-number'],
								'action' => 'refund',
								'test' => $this->c[test],
								'custom' => $payment->custom,
								'source' => 'Google Checkout',
								'response' => $response_refund,
							);
							
							// Cancel - refunded, now try to cancel (if we still haven't reunfed it all, it won't let us)
							$response = $Grequest->SendCancelOrder($payment->transaction,$c[reason],$c[comment]);
							$payment->debug("SendCancelOrder() response:".return_array($response),$c[debug]);
							if($response[0] == 200) {
								$results[action] = $action;
								$results[response] = $response;
							}
						}
					}
					
					// Couldn't cancel or refund, return failed cancel
					if(!$results) {
						$results = array(
							'result' => 0,
							'message' => $response[1],
							'action' => $action,
							'test' => $this->c[test],
							'custom' => $payment->custom,
							'source' => 'Google Checkout',
							'response' => $response,
						);	
					}
				}
				
				// Refund
				if($action == "refund") {
					// Config
					if(!$c[reason]) $c[reason] = NULL;
					if(!$c[comment]) $c[comment] = NULL;
					
					$response = $Grequest->SendRefundOrder($payment->transaction,$payment->amount,$c[reason],$c[comment]);
					$response_parsed = $payment->xml_array($response[1]);
					$payment->debug("SendRefundOrder() response:".return_array($response),$c[debug]);
					if($response[0] == 200) {
						$results = array(
							'result' => 1,
							'message' => ($response[0] != 200 ? $response[1] : ""),
							'transaction' => $response_parsed['request-received']['refund-amount-notification']['google-order-number'],
							'action' => $action,
							'test' => $this->c[test],
							'custom' => $payment->custom,
							'source' => 'Google Checkout',
							'response' => $response,
						);
					}
					else {
						$results = array(
							'result' => 0,
							'message' => $response[1],
							'action' => $action,
							'test' => $this->c[test],
							'custom' => $payment->custom,
							'source' => 'Google Checkout',
							'response' => $response,
						);
					}
				}
			}
			
			// Remove internal custom variables
			unset($results[custom][__payment_action],$results[custom][__payment_test]);
			
			// Return
			return $results;
		}
		
		/**
		 * Process an array of data returned by Google Checkout after a transaction attempt and returns an array of results.
		 *
		 * You should call this up on the URL you defined when you setup your 'Notify URL' in your Google Checkout account.  Example:
		 *
		 * $payment = new payment();
		 * $payment->gateway(new payment_googlecheckout($merchant_id,$merchant_key));
		 * $results = $payment->results($payment,$_POST);
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param array $array The array of information sent back by Google Checkout. Usually this is sent via $_POST.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction including 'result' (boolean, 1 = succes, 0 = error) and 'message' as well as the transaction id ('transaction').
		 */
		function results($payment,$response,$c = NULL) {
			// Reply to Google that we got this
			print '_type=notification-acknowledgment&serial-number='.$response['serial-number']; // Name/value pairs reponse
			
			// Custom
			$custom = NULL;
			if($response['order-summary_shopping-cart_merchant-private-data']) {
				$custom = $payment->xml_array(html_entity_decode(html_entity_decode($response['order-summary_shopping-cart_merchant-private-data'])),array('debug' => $c[debug]));
			}
			
			// Action
			$action = $custom[__payment_action];
			
			// Test
			$test = $custom[__payment_test];
			
			// Remove internal custom variables
			unset($custom[__payment_action],$custom[__payment_test]);
			
			// Currency
			$currency = $response['order-summary_total-charge-amount_currency'];
			
			// Server
			if($test) $server = "sandbox";
			else $server = "production";
			
			// Files
			require_once dirname(__FILE__)."/googlecheckout/googleresponse.php";
			require_once dirname(__FILE__)."/googlecheckout/googlemerchantcalculations.php";
			require_once dirname(__FILE__)."/googlecheckout/googleresult.php";
			require_once dirname(__FILE__)."/googlecheckout/googlerequest.php";
			
			// Response class
			/*$Gresponse = new GoogleResponse($this->merchant_id,$this->merchant_key);
			// Reply to google that we got data
			$Gresponse->SendAck($data['serial-number'],false); // XML response (make sure we don't print any other text/data otherwise XML will be invalid). // We're using name/value pairs response (The print '_type=notification-acknowledgment... at top of method)*/
			
			// Request class
			$Grequest = new GoogleRequest($this->merchant_id,$this->merchant_key,$server,$currency);
				
			// Charge
			if($action == "charge") {
				// Process order
				$payment->debug("SendProcessOrder(".$response['google-order-number'].")",$c[debug]);
				$response_process = $Grequest->SendProcessOrder($response['google-order-number']);
				$payment->debug("SendProcessOrder() response:".return_array($response_process),$c[debug]);
				
				// Charge order
				if($response['order-summary_financial-order-state'] != "CHARGED" and in_array($response['_type'],array('authorization-amount-notification','new-order-notification'))) {
					$payment->debug("SendChargeOrder(".$response['google-order-number'].",".$response['order-summary_order-total'].")",$c[debug]);
					$response_charge = $Grequest->SendChargeOrder($response['google-order-number'],$response['order-summary_order-total']);
					$payment->debug("SendChargeOrder() response: ".return_array($response_charge),$c[debug]);
				}
			}
			
			// Results
			$results = array(
				'result' => ($response['fulfillment-order-state'] == "NEW" ? 1 : 0),
				'message' => '', // Not sure what holds this
				'transaction' => $response['google-order-number'],
				'action' => $action,
				'test' => $test,
				'custom' => $custom,
				'source' => "Google Checkout",
				'response' => $response,
			);
			
			// Return		
			return $results;
		}
		
		/**
		 * Standardizes some of the values used by Google Checkout.
		 *
		 * @param object $payment The payment object we want to standize the values of.
		 * @return object The standardized payment object.
		 */
		function standardize($payment) {
			// Error
			if(!$payment) return;
			
			// Currency - required
			if(!$payment->currency) {
				$payment->currency = "USD";
			}
			
			// Return
			return $payment;
		}
		
		/**
		 * Detects some common errors for this gateway and returns the error message.
		 *
		 * @return string The error message (if an error was detected).
		 */
		function errors() {
			// No login credentials
			if(!$this->merchant_id or !$this->merchant_key) {
				$error = "The login credentials for Google Checkout are missing.";
			}
			
			// Return
			return $error;
		}
	}
}