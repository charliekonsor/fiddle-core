<?php
// Themes
$themes = themes();

// Permission
if($this->permission('manage',1)) {
	// Heading
	print $this->heading();
	// Buttons
	print $this->heading_buttons();
	// Filters
	//print $this->module_class->manage_filters();
	
	if($themes) {
		print "
<table class='core-table core-rows'>";
		foreach($themes as $theme => $v) {
			// Item
			$item = $this->module_class->item($v[id]);
			
			// Icons
			$icons = NULL;
			if($v[settings][inputs]) $icons[settings] = $item->icon('edit',array('icon' => 'settings','tip_text' => 'Settings'));
			else $icons[settings] = "<span class='i i-empty i-inline'></span>";
			if($this->permission('change')) $icons[change] = "<a href='".url_noquery()."?process_action=themes_change&amp;process_module=".$this->module."&amp;theme=".urlencode($theme)."' class='i i-enabled i-inline tooltip".(m('settings.settings.theme') == $theme ? "" : " core-hover")."' title='Use this theme'></a>";
			
			// Row
			print "
	<tr>
		<td width='".(count($icons) * 18)."'>
			".implode($icons)."
		</td>
		<td>".$theme."</td>
	</tr>";
		}
		print "
</table>";
	}
	else print "
<div class='core-none'>No ".$this->module_class->v('plural')." Found</div>";
}

?>