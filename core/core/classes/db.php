<?php
if(!class_exists('db',false)) {
	/**
	 * A class that handles connection to and querying of the website's database.
	 *
	 * Dependencies
	 * - functions
	 *   - x
	 *   - cache_delete_framework
	 *   - g
	 * - variables
	 *   - $__GLOBAL
	 * - constants
	 *   - FRAMEWORK
	 *
	 * @package kraken
	 */
	class db {
		/** An array of configuration options */
		public $c;
		
		/**
		 * Loads and returns an instance of the db class.
		 *
		 * Example:
		 * - $db = db::load($c);
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 * @return object An instance of the class.
		 */
		static function load($c = NULL) {
			// Class name
			$class = __CLASS__;
			
			// Class
			return new $class($c);
		}
		
		/**
		 * Constructs the class
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function __construct($c = NULL) { // PHP 5+
			$this->db($c);
		}
		function db($c = NULL) {
			// Config
			$this->c = $c;
			
			// Defaults // We'll use the defined constants for these if not defined so we're not adding the same variables to every db class as it's used within lots of other classes and this adds 4 lines we don't need.
			//if(!$this->c[username]) $this->c[username] = g('config.db.mysql.username'); // Username to access the database
			//if(!$this->c[password]) $this->c[password] = g('config.db.mysql.password'); // Password to access the database
			//if(!$this->c[database]) $this->c[database] = g('config.db.mysql.database'); // Name of the database we are accessing
			//if(!$this->c[server]) $this->c[server] = g('config.db.mysql.server'); // Server location of our database (usually localhost)
		}
		
		/**
		 * Creates a connection to the database and stores it in the global variables.
		 */
		function connect() {
			// Global
			global $_DB_CONNECTION,$_DB_CONNECTION_SERVER,$_DB_CONNECTION_USERNAME,$_DB_CONNECTION_DATABASE;
			
			// Variables
			$username = ($this->c[username] ? $this->c[username] : DB_MYSQL_USERNAME);
			$password = ($this->c[password] ? $this->c[password] : DB_MYSQL_PASSWORD);
			$server = ($this->c[server] ? $this->c[server] : DB_MYSQL_SERVER);
			$database = ($this->c[database] ? $this->c[database] : DB_MYSQL_DATABASE);
			
			// Existing connection
			if($_DB_CONNECTION and $_DB_CONNECTION_SERVER == $server and $_DB_CONNECTION_USERNAME == $username and $_DB_CONNECTION_DATABASE == $database) {
				return $_DB_CONNECTION;
			}
			// New connection
			else {
				$_DB_CONNECTION = new mysqli($server,$username,$password,$database);
				if($_DB_CONNECTION->connect_errno) {
					print "Unable to connect to database. Error: ".$_DB_CONNECTION->connect_errno;
					exit;
				}
				else {
					$_DB_CONNECTION_SERVER = $server;
					$_DB_CONNECTION_USERNAME = $username;
					$_DB_CONNECTION_DATABASE = $database;
					return $_DB_CONNECTION;
				}
			}
			
			/*// Global
			global $_DB_CONNECTION,$_DB_CONNECTION_SERVER,$_DB_CONNECTION_USERNAME;
			
			// Variables
			$username = ($this->c[username] ? $this->c[username] : DB_MYSQL_USERNAME);
			$password = ($this->c[password] ? $this->c[password] : DB_MYSQL_PASSWORD);
			$server = ($this->c[server] ? $this->c[server] : DB_MYSQL_SERVER);
			
			// Existing connection
			if($_DB_CONNECTION and $_DB_CONNECTION_SERVER == $server and $_DB_CONNECTION_USERNAME == $username) {
				return $_DB_CONNECTION;
			}
			// New connection
			else {
				$_DB_CONNECTION = mysql_connect($server,$username,$password) or die('Unable to connect to the database. Error: '.mysql_error());
				return $_DB_CONNECTION;
			}*/
		}
		
		/**
		 * Runs a query against the database
		 * 
		 * @param string $query The query we want to run
		 * @param array $c An array of configuration options
		 * @return resource|int Retuns either the row's primary id (if INSERT call) or the database resource (if any other call)
		 */
		function q($query,$c = NULL) {
			global $__GLOBAL;
			// Config
			if(!x($c[cached])) $c[cached] = $__GLOBAL[config][db][cached]; // Return cached results (only applies to SELECT and SHOW statements)
			if(!x($c[cache])) $c[cache] = $__GLOBAL[config][db][cache]; // Cache results (only applies to SELECT and SHOW statements)
			if(!x($c[debug_count])) $c[debug_count] = $__GLOBAL[config][db][debug][count]; // Debug the query counts (including cached and slow counts, and overall time)
			if(!x($c[debug_queries])) $c[debug_queries] = $__GLOBAL[config][db][debug][queries]; // Debug queries 
			if(!$c[error]) $c[error] = "debug"; // What to do with errors: print, debug, log
			
			// Statement
			$q = $this->clean($query);
			$ex = explode(" ",$q);
			$statement = strtoupper($ex[0]);
			
			// Table
			if(($statement == "SELECT" or $statement == "SHOW") and $c[cache] == 1) { // Only need the table for these statements if caching
				preg_match('/FROM(.*?)([a-zA-Z0-9_-]{1,50}) /',$q." ",$t);
				$table = $t[2];
			}
			else if($statement == "INSERT" or $statement == "DELETE" or $statement == "ALTER") $table = $ex[2];
			else if($statement == "UPDATE") $table = $ex[1];
			
			// Database
			$database = ($this->c[database] ? $this->c[database] : DB_MYSQL_DATABASE);
			
			// Start timer
			$timer_start = microtime_float();
			
			// Cached
			$cached = 0;
			if(in_array($statement,array("SELECT","SHOW")) and $c[cached] == 1) {
				// Cache string - note, using the 'cleaned' query so it can catch all equal queries, even if they're in slightly different formats
				$q_cache = md5($q);
				
				// Local Cache
				$resource = $__GLOBAL[cache][db][mysql][$database][$table][$q_cache];
				
				// Global Cache
				if(!$resource) {
					# build this after we decide on using cache class or function
					# Might want to just do our 'caching' externally so as to keep the function clean (would either have to use external functions or add our own in...thoubh we could maybe just use if(class_exists('log')))
				}
				
				// Found Cached Query
				if($resource) {
					// Reset to begnning
					if($resource->num_rows) {
						$resource->data_seek(0);
					}
					/*if(mysql_num_rows($resource)) {
						mysql_data_seek($resource, 0);
					}*/
					
					// Timer
					$timer_total = microtime_float() - $timer_start;
					
					// Cached
					$cached = 1;
				}
			}
			
			// Clear cached values - do before atually changing so cache_delete_framework() (which references the actual row when deleting a plugin item) will work correctly
			if($statement == "UPDATE" or $statement == "INSERT" or $statement == "DELETE" or $statement == "ALTER") {
				// DB Changed? // Can't check because we're doing it before running the query now
				//if($connection->affected_rows > 0) {
				//if(mysql_affected_rows($connection) > 0) {
					// Clear local cache
					unset($__GLOBAL[cache][db][mysql][$database][$table]);
					if($statement == "ALTER") {
						unset($__GLOBAL[cache][db][mysql][primary][$table],$__GLOBAL[cache][db][mysql][columns][$table]);
					}
					
					// Clear global cache
					if($table) {
						# build this if we do 'Global cache' above.
					}
					
					// Framework
					if(FRAMEWORK) cache_delete_framework($statement,$table,$query);	
				//}
			}
			
			// Query
			if(!$cached) {
				// Connect
				$connection = $this->connect();
				if(!$connection) return false;
				
				// SQL Mode (don't want new MySQL 5 modes)
				$connection->query("SET sql_mode = ''");
				//mysql_query("SET sql_mode = ''");
				
				// Character Encoding
				$connection->set_charset("utf8");
				//mysql_query("SET CHARACTER SET utf8");
	   			//mysql_query("SET NAMES utf8");
				
				// Database
				/*global $_DB_CONNECTION_DATABASE;
				if(!$_DB_CONNECTION_DATABASE or $database != $_DB_CONNECTION_DATABASE) {
					mysql_query("USE `".$database."`", $connection) or $this->error("USE `".$database."`");
					$_DB_CONNECTION_DATABASE = $database;
				}*/
					
				// Query
				$resource = $connection->query($query);
				if($connection->errno) $this->error($query,$c);
				//$resource = mysql_query($query, $connection) or $this->error($query,$c);
			
				// INSERT - Return ID (do before any of the below stuff can perform another query)
				if($statement == "INSERT") {
					$id = $connection->insert_id;
					//$id = mysql_insert_id();
				}
				
				// Timer
				$timer_total = microtime_float() - $timer_start;
				
				// Cache
				if(in_array($statement,array("SELECT","SHOW")) and $c[cache] == 1) {
					// Local cache
					$__GLOBAL[cache][db][mysql][$database][$table][$q_cache] = $resource;
					
					// Global cache
					if($c[cache]) {
						# build this, have to loop through rows and create an array first
						# Might want to just do our 'caching' externally so as to keep the function clean (would either have to use external functions or add our own in...thoubh we could maybe just use if(class_exists('cache_save')))
					}
				}
			}
				
			// Debug
			if(debug_active()) {
				// Count
				if($c[debug_count]) {
					// Count
					$__GLOBAL[debug][db][mysql][count] += 1;
					// Cached query
					if($cached) $__GLOBAL[debug][db][mysql][cached] += 1;
					// Slow query
					if($timer_total > .05) $__GLOBAL[debug][db][mysql][slow] += 1;
					// Time
					$__GLOBAL[debug][db][mysql][time] += $timer_total; 
				}
				// Queries
				if($c[debug_queries]) {
					$__GLOBAL[debug][db][mysql][queries][] = "<span class='debug-query".($cached ? " debug-query-cached" : "").($timer_total > .05 ? " debug-query-slow" : "")."'>".$query." (".round($timer_total,4)." seconds)</span>"; 
				}
			}
			
			// Return
			if($statement == "INSERT") return $id;
			else return $resource;
		}
		/**
		 * A more user friendly alias for the q() function.
		 * 
		 * @param string $query The query we want to run
		 * @param array $c An array of configuration options
		 * @return resource|int|null Retuns either the database resource (if SELECT, SHOW TABLES, etc. call), the row's primary id (if INSERT or UPDATE call), or null (if DELETE, etc. call)
		 */
		function query($query,$c = NULL) {
			return $this->q($query,$c);
		}
		
		/**
		 * Fetches the array of keys/values for row in given query result.
		 * 
		 * @param resource|string $result The query result or a fresh query we want to run and get the first row of.
		 * @param array $c An array of configuration options (these get passed along to q() if we pass a query in the $result parameter)
		 * @return array The array of key/value pairs for row in given query result
		 */
		function f($result,$c = NULL) {
			if(!$c[association]) $c[association] = MYSQL_ASSOC; // What type of array to return: MYSQL_ASSOC, MYSQL_BOTH, MYSQL_NUM
			if($result) {
				// Query?
				if(!is_object($result)) {
				//if(!is_resource($result)) {
					// Yes, run it // Want to allow SHOW, etc. so just allow all.
					//if(strstr(strtoupper($result),'SELECT')) {
						$result = $this->q($result,$c);
					//}
				}

				// Fetch
				if(is_object($result)) {
					if($c[association] == MYSQL_NUM) return $result->fetch_row();
					else if($c[association] == MYSQL_BOTH) return $result->fetch_array();
					else return $result->fetch_assoc();
					//return mysql_fetch_array($result,$c[association]);
				}
			}
		}
		
		/**
		 * Returns the number of rows in the given query result.
		 * 
		 * @param resource|string $result The query result or a fresh query we want to run and get the number of rows of.
		 * @param array $c An array of configuration options (these get passed along to q() if we pass a query in the $result parameter)
		 * @return int The number of rows in the given query result
		 */
		function n($result,$c = NULL) {
			if($result) {
				// Query?
				if(!is_object($result)) {
				//if(!is_resource($result)) {
					// Yes, run it // Want to allow SHOW, etc. so just allow all.
					//if(strstr(strtoupper($result),'SELECT')) {
						$result = $this->q($result,$c);
					//}
				}
				
				// Rows   
				return $result->num_rows;
				//return mysql_num_rows($result);
			}
		}
		
		/**
		 * Builds a query with the given parameters
		 *
		 * !!! Not sure I like dynamically getting the primary key. Using a module specific simiar function in db class. !!!
		 * 
		 * @param string $statement The statement you want to use (case insensetive): SELECT, INSERT, UPDATE, DELETE
		 * @param string $table The table this query will be querying.
		 * @param int|array $id Either the id we want to SELECT/UPDATE/DELETE or, if INSERT, an array of values to save.
		 * @param array $values Either the values we want to UPDATE or (if SELECT/INSERT/DELETE) an array of congiguration values. Default = NULL
		 * @param array $c An array of congiguration values. Default = NULL
		 * @return string The built query string.
		 */
		/*function build($statement,$table,$id = NULL,$values,$c = NULL) {
			$statement = strtoupper($statement);
			if($statement == "INSERT") {
				$c = $values;
				$values = $id;
				$id = NULL;
			}
			if($statement == "SELECT" or $statement == "DELETE") {
				$c = $values;
				$values = NULL;	
			}
			
			// Statement
			$query = $statement;
			if($statement == "INSERT") $query .= " INTO";
			// Table
			$query .= "`".$table."`";
			// Values
			if($values) $query .= $this->values($values);
			// ID
			if($id > 0) {
				$primary = $this->primary($table);
				if($primary) $query .= " WHERE ".$primary." = '".$id."'";
				else return false;
			}
		}
		
		/**
		 * Creates the 'values' portion of the query (including SET).
		 * 
		 * @param array $values The key/value pairs you're going to INSERT or UPDATE.
		 * @return string The 'values' string (including SET). Example: SET id = '123', name = 'Test Item'
		 */
		function values($values) {
			$string = NULL;
			foreach($values as $k => $v) {
				$string .= (!$string ? " SET " : ", ")."`".$k."` = ";
				if(substr($v,0,4) == "AES_") $string .= $v;
				else $string .= "'".a($v)."'";
			}
			return $string;
		}
		
		/**
		 * Cleans up the given query string.
		 * 
		 * Strips out all unnecessary spaces, line beaks, and ` from query.  Used when determining the query's statement and table where that stripped data isn't needed.
		 * 
		 * @param string $query The query we want to clean up.
		 * @return string The cleaned up query
		 */
		function clean($query) {
			// Strip line breaks, extra spaces
			$query = trim(preg_replace('/\s+/',' ',$query));
			// Remove smart quotes
			$query = str_replace('`','',$query);
			
			// Return
			return $query;
		}
		
		/**
		 * Returns the primary key for the given table (if one exists).
		 * 
		 * @param string $table The table we want to get the primary key of.
		 * @return string The table's primary key.
		 */
		function primary($table) {
			// Cached
			global $__GLOBAL;
			$primary = $__GLOBAL[cache][db][mysql][primary][$table];
			// Not yet cached, get
			if(!$primary) {
				$row = $this->f("SHOW KEYS FROM `".$table."` WHERE Key_name = 'PRIMARY'");
				$primary = $row[Column_name];
				$__GLOBAL[cache][db][mysql][primary][$table] = $primary;
			}
			// Return
			return $primary;
		}
		
		/**
		 * Handles query errors.
		 * 
		 * Adds the error to the global $__GLOBAL[debug] array.
		 * 
		 * @param string $query The query that caused the error.
		 * @param array $c An array of configuration options. See db->q() function for $c values/descriptions. Default = NULL
		 */
		function error($query,$c = NULL) {
			// Error
			global $_DB_CONNECTION;
			$error = "Database Error ".$_DB_CONNECTION->errorno.": ".$_DB_CONNECTION->error;
			//$error = "Database Error ".mysql_errno().": ".mysql_error();
			$query = "Query: ".$query;
			//$script = $_SERVER['QUERY_STRING'] ? substr(DOMAIN, 0, strlen(DOMAIN) - 1) . $_SERVER['PHP_SELF'] . "?" . $_SERVER['QUERY_STRING'] : substr(DOMAIN, 0, strlen(DOMAIN) - 1) . $_SERVER['PHP_SELF'];
			
			// Print
			if($c[error] == "print") {
				print "
".$error."<br />
".$query."<br />";
			}
			// Log
			else if($c[error] == "log") {
				log_save('db-errors',$error."\r\n".$query."\r\n\r\n");
			}
			// Debug
			else {
				global $__GLOBAL;
				$__GLOBAL[debug][db][mysql]['errors'][] = $error."<br />".$query;
			}
		}
		
		/**
		 * Gets data on all the columns in the given table (if it exists).
		 * 
		 * @param string $table The table you want to check in.
		 * @return array An array of columns and information about each column.
		 */
		function table_columns($table) {
			if($table) {
				global $__GLOBAL;
				
				// Saved columns
				if($__GLOBAL[cache][db][mysql][columns][$table]) {
					$columns = $__GLOBAL[cache][db][mysql][columns][$table];
				}
				// Get columns
				else {
					$rows = $this->q("SHOW COLUMNS FROM `".$table."`");
					while($row = $this->f($rows)) {
						$columns[$row[Field]] = $row;
					}
					
					// Save
					$__GLOBAL[cache][db][mysql][columns][$table] = $columns;
				}
				
				// Return
				return $columns;
			}
		}
		
		/**
		 * Gets data on the given column in the given table (if it exists).
		 * 
		 * @param string $table The table you want to check in.
		 * @param string $column The column you're checking for.
		 * @return array An array of information about the column. If it doesn't exist, nothing in returned.
		 */
		function table_column($table,$column) {
			if($table and $column) {
				// Columns
				$columns = $this->table_columns($table);
				// Column
				return $columns[$column];
			}
		}
		
		/**
		 * Returns the CREATE TABLE query for a given table.
		 *
		 * Uses the SHOW CREATE TABLE call.
		 *
		 * Note, this also set ths AUTO_INCREMENT to 0 and adds IF NOT EXISTS to the CREATE TABLE call.
		 * 
		 * @param string $table The table you want to get the CREATE TABLE query for.
		 * @return string The query you can use to create this table.
		 */
		function table_create($table) {
			// Error
			if(!$table) return;
			
			// Create table query
			$row = $this->f("SHOW CREATE TABLE `".$table."`",array('association' => MYSQL_BOTH));
			$query = $row[1];
			
			// Add IF NOT EXISTS
			$query = preg_replace('/CREATE TABLE/','CREATE TABLE IF NOT EXISTS',$query);
			
			// Reset auto incremnt
			$query = preg_replace('/AUTO_INCREMENT ?= ?([0-9]+) ?/','AUTO_INCREMENT=0 ',$query);
			
			// Return
			return $query;
		}
	}
}
?>