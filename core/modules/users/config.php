<?php
/************************************************************************************/
/************************************** Config **************************************/
/************************************************************************************/
$config = array(
	'name' => 'Users',
	'single' => 'User',
	'plural' => 'Users',
	'icon' => 'user',
	'db' => array(
		'table' => 'users',
		'id' => 'user_id',
		'type' => 'type_code',
		'user' => 'user_id',
		'name' => 'user_first_name user_last_name',
		'theme' => 'user_theme',
		'template' => 'user_template',
		'visible' => 'user_visible',
		'created' => 'user_created',
		'updated' => 'user_updated'
	),
	'permissions' => array(
		'add' => array(
			'label' => 'Add/Register',
			'manage' => 1,
			'default' => array(
				'admins',
			)
		),
		/*'view' => array( // Don't exist
			'label' => 'View Profiles',
			'default' => array(
				'admins',
				'members',
			)
		),*/ 
		'edit_own' => array(
			'label' => 'Edit Own Profile',
			'manage' => 1,
			'default' => array(
				'admins',
				'members',
			)
		),
		'edit_all' => array(
			'label' => 'Edit Other Profiles',
			'manage' => 1,
			'default' => array(
				'admins',
			)
		),
		'delete_own' => array(
			'label' => 'Delete Own Profile',
			'manage' => 1,
			'default' => array(
				'admins',
			)
		),
		'delete_all' => array(
			'label' => 'Delete Other Profiles',
			'manage' => 1,
			'default' => array(
				'admins',
			)
		),
		/*'copy' => array( // Not sure I want this
			'label' => 'Copy',
			'manage' => 1,
			'default' => array(
				'admins',
			)
		),*/
		'settings' => array(
			'label' => 'Settings',
			'default' => array(
				'admins',
			)
		),
	),
	'admin' => array(
		'sidebar' => 0,
		'section' => 'users',
		'views' => array(
			'home' => 'All Users',
			'type' => 'User Type',
			'item' => 'Edit User'
		)
	),
	'account' => array(
		'active' => 1,
		'views' => array(
			'profile' => 'Edit Profile'
		),
		'urls' => array(
			'profile' => array(
				'format' => 'account/profile',
			)
		),
	),
	'public' => array(
		'views' => array(
			//'home' => 'Users Home Page',
			//'item' => 'User Profile',
			'login' => 'Login',
			'logout' => 'Logout', // Not really a 'view', but needed so we can create links to it in $module->url() (checks if a view exists)
			//'register' => 'Register',
		),
		'urls' => array(
			'login' => array(
				'format' => 'login'
			),
			'logout' => array(
				'format' => 'logout'
			),
			/*'register' => array(
				'format' => 'register'
			)*/
		),
	),
	'settings' => array(
		// Inputs
		'inputs' => array(
			array(
				'label' => '<b>Login</b>',
				'type' => 'html',
			),
			array(
				'label' => 'Field',
				'type' => 'select',
				'name' => 'login[field]',
				'options' => array(
					'user_name' => 'Username',
					'user_email' => 'E-mail',
					'either' => 'Either Username or E-mail'
				),
				'help' => "What do you want a user to have to enter when they're loggin in?  There username, there e-mail address, or either?",
				'validate' => array(
					'required' => 1,
				),
			),
		),
		// Module - hidden settings and default settings
		'login' => array(
			'field' => 'username'
		),
		// Plugins - hidden settings and default settings (only if they overwrite the plugin's default settings)
		'search' => array(
			'index' => 1
		),
		'trash' => array(
			'cascade' => 0, // The 'trash' plugin is disabled by default, but this is a special setting we created ust for the users module which lets us 'cascade' the trashing so, if set to 1, all items belong to this user (and their associated plugins) will also be 'trashed'
		),
	),
	'forms' => array(
		'add' => array(
			'inputs' => array(
				array( // Note: if different types have different forms, we would have to comment out this field. Just have it  here for now so we can change a user's type after they're first created. If we want that ability with different forms must tweak $form_framework->render() so that it adds type field even if there's already a type (just for the users module) and doesn't NULLify $this->inputs (again, just for the user's module).
					'label' => 'Type',
					'type' => 'select',
					'name' => 'type_code',
					'options' => 'types',
					'options_module' => 'users',
					'validate' => array(
						'required' => 1,
					),
					'types' => array(
						'supers',
						'admins',
					)
				),
				array(
					'label' => 'First Name',
					'type' => 'text',
					'name' => 'user_first_name',
					'validate' => array(
						'required' => 1,
					),
				),
				array(
					'label' => 'Last Name',
					'type' => 'text',
					'name' => 'user_last_name',
					'validate' => array(
						'required' => 1,
					),
				),
				array(
					'label' => 'E-mail',
					'type' => 'email',
					'name' => 'user_email',
					'validate' => array(
						'required' => 1,
						'email' => 1,
					),
				),
				array(
					'label' => 'Username',
					'type' => 'text',
					'name' => 'user_name',
					'validate' => array(
						'required' => 1,
					),
				),
				array(
					'label' => 'Password',
					'type' => 'password',
					'name' => 'user_password',
					'validate' => array(
						'required' => 1,
						'minlength' => 3,
						'maxlength' => 50,
					),
				),
				array(
					'label' => 'Confirm Password',
					'type' => 'password',
					'name' => 'user_password_confirm',
					'value' => '{input.user_password}',
					'validate' => array(
						'required' => 1,
						'minlength' => 3,
						'maxlength' => 50,
					),
				),
				array(
					'type' => 'html',
					'value' => "
<script type='text/javascript'>
$(document).ready(function() {
	// Messages
	var register_regex_rule = '".g('config.register.username.regex')."';
	var register_regex_message = 'Your username may only contain letters and numbers (no spaces)';
	var register_username_unique_message = 'This username is already taken';
	var register_email_unique_message = 'This e-mail address is already in use';
	
	$('form.register').each(function() { 
		$(this)
			.submit(function(event) {
				// CKEditor (Make sure content is intextarea)
				if(CKEDITOR) {
					for(instance in CKEDITOR.instances) CKEDITOR.instances[instance].updateElement();
				}
			})
			.validate({
				rules: {
					user_name: { remote: DOMAIN+\"?ajax_module=users&ajax_action=checkUsername&form=\"+$('input[name=__form]',this).val(), regex: register_regex_rule },
					user_email: { remote: DOMAIN+\"?ajax_module=users&ajax_action=checkEmail&form=\"+$('input[name=__form]',this).val() }, 
					user_password_confirm: { equalTo: \"input[name=user_password]\"},
					user_email_confirm: { equalTo: \"input[name=user_email\"},
				},
				messages: {
					// If user_name validation isn't working, there's probably an error in the AJAX file
					user_name: { remote: register_username_unique_message, regex: register_regex_message },
					user_email: { remote: register_email_unique_message },
					user_password_confirm: { equalTo: \"Your passwords don't match\" },
					user_email_confirm: { equalTo: \"Your e-mail addresses don't match\" }
				},
				submitHandler: function(form) { form_submit(form); }
			});
	});
});
</script>"
				)
			),
			'validate' => 0,
			'class' => 'register'
		)
	)
);
?>