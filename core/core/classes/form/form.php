<?php
if(!class_exists('form',false)) {
	/**
	 * Class for creating a form and adding inputs to it.
	 *
	 * Example:
	 *	$form = new form(); // Create an instance of the form.
	 *	$form->text('Name','name'); // Adding an input via a helper method: $form->text(), $form->textarea(), $form->select(), etc.
	 * 	$form->input('Phone','text','phone'); // Adding an input via the input() method: $form->input($label,$type,$name,$value,$c);
	 *	$form->inputs(array(array('label' => 'E-mail','type' => 'email','name' => 'email','validate' => array('email' => 1))); // Adding an array of inputs
	 *	$form->submit('Submit'); // Adding a submit button via the 'submit()' helper method.
	 *	print $form->render(); // Render the form HTML
	 *
	 * Update
	 * - Most input_...() methods can be removed
	 *   - Must pull out all type specific stuff in them though
	 * - Handle 'errors'.
	 *   - Maybe attach to each input, then gather them all in the form class at some point, must probably clear them out before processing.
	 *   - Right now it's expecting $this->error in $form_input->render(), but not sure if that's the best way for when the form gets cached and reloaded.
	 * - form_input_file - have the beginnings of it, but not complete in the least
	 *   - not sure how we want to handle processing file like stuff in similar types (swfupload, plupload, etc.)
	 *   - probably just keep the value_file() and input_process_file() methods in the core form class.
	 * - framework specific stuff
	 *   - right now inputs extend core form class
	 *   - since we're not using any variables from the 'form' class we're extending, this is fine. we just have to make sure input specific methods (now in the form_input class) can somehow use their framework specific things
	 *     - maybe do in form_framework->render() before calling up $input->render()
	 * - Remove
	 *   - value_file()
	 * 	 - input_process_file()
	 *   - file_delete()
	 *   - Most every input_...() method
	 *
	 * To do
	 * - validate
	 *   - required by group (date_select, datetime)
	 *
	 * Dependencies:
	 * - functions
	 *   - x
	 *   - data_serialize
	 *   - data_unserialize
	 *   - time_format
	 *   - string_encode
	 *   - help
	 *   - cache_save
	 *   - cache_get
	 *   - load_class_module
	 *   - random
	 *   - purify - add purification script to the value_purify function if we make this class standalone
	 *   - attributes_string - can remove function and uncomment code below it
	 * - classes
	 *   - file - falls back to basic file functionality otherwise
	 * - contstants
	 *   - FRAMEWORK
	 *   - URL
	 *   - SERVER - in preview() if file class not present
	 *   - DOMAIN - in preview() if file class not present
	 * - variables
	 *   - $_SESSION['__form']
	 *   - $_SESSION['forms'] // Deprecated, using cache instead
	 * - css
	 *   - .i, .i-inline, .i-help, .i-delete
	 * - js
	 *   - tooltips() - Degrades gracefully
	 *   - placeholders() - Degrades gracefully
	 *   - ckeditor - Degrades gracefully
	 *
	 * @package kraken\forms
	 */
	class form {
		/** An array that stores the inputs that are added to this form. */
		public $inputs = array();
		/** An array that stores the submitted values which we'll process after the form is submitted (if $this->c[process] == 1). */
		public $post = array();
		/** An array that stores the submitted files we'll process after the form is submitted (if $this->c[process] == 1). */
		public $files = array();
		/** An array that stores any validatio errors that are discovered wile processing (if $this->c[process] == 1). */
		public $errors = array();
		/** An array of submitted values. Used either after the form is submitted or after it failed validation and we returned to the form, in which case we restore the previously submitted values using the $_GET['__form'] value which points us to the saved form instance. */
		public $values;
		/** An array of data related to uploaded files that were processed by the class.  Includes thumb paths/sizes/etc., extension paths, etc. */
		public $values_files;
		/** The unique identifier of this form. Used when processing it after submissions, redisplaying old values if showing the form after invalid submission, etc. */
		public $hash;
		/** An array of configuration values */
		public $c = array(
			'action' => NULL, // The action of the form, meaning the URL it's sent to after it's submitted. Could be javascript too, ex: 'javascript:submitForm();'. Default = NULL (current page)
			'method' => 'POST', // The method for submitting the form: POST or GET. Default = POST
			'name' => 'form', // The 'name' attribute of the form. Default = form
			'values' => array(), // Can pass an array of values (as opposed to passing a value for individual inputs) in an array(name => value) format.
			'defaults' => array(), // Can pass an array of default values (as opposed to passing a default value for individual inputs) in an array(name => value) format.
			'validate' => 1, // Whether or not you want us to validate the form (both through javascript before submitted and PHP after submitted). Default = 1
			'errors_inline' => 1, // Whether or not you want to show validation errors inline (as opposed or in addition to header errors). Default = 1
			'errors_header' => 0, // Whether or not you want to show validation errors in the header of the form (as opposed or in addition to inline errors). Default = 0
			'process' => 1, // Whether or not you're going to process the form. If $c[process] == 0, no __form_counter will be added to form. Default = 1
			'saved' => 1, // Get values/errors from form saved in $_SESSION['__form'] if it exists.
			'serialize' => 1, // Do you want the class to automatically serialize any array values that are processed? Default = 1
			'inline' => 0, // Whether or not you want all the inputs to be displayed 'inline' as opposed to one on top of another. Default = 0
			'debug' => 0 // Debug. Default = 0
		);
		
		/**
		 * Loads and returns an instance of the form class, using module or framework level classes if they exist.
		 *
		 * Example:
		 * - $form = form::load($module,$id,$c);
		 *
		 * @param string $module The module this item is in. Only applies to framework and module level classes.
		 * @param int The id of the item. Only applies to framework and module level classes.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return object An instance of either the core framework form class or (if it exists) the module specific framework form class.
		 */
		static function load($module = NULL,$id = NULL,$c = NULL) {
			// Class name
			$class = __CLASS__;
			
			// Params
			if(!$module or is_array($module)) { // form::load($c)
				$c = $module;
				$module = NULL;
				$id = NULL;	
			}
			
			// Framework
			if(FRAMEWORK) {
				$class_framework = $class."_framework";
				$params = func_get_args(); // Must be outside function call
				$class_load = load_class_module($class_framework,$params,$module,$c[type]); // No way to instanitiate class with array of params so we'll just return the class name and...
				if($class_load) return new $class_load($module,$id,$c); // ...manually pass the params here
			}
			
			// Core class
			return new $class($c);
		}
		
		/**
		 * Creates an instance of the class and stores configuration options
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function __construct($c = NULL) {
			self::form($c);
		}
		function form($c = NULL) {
			// Config
			if($c) $this->c = array_merge($this->c,$c);
			
			// Validate defaults - depends on the $c[validate] value
			if(!x($this->c[validate_php])) $this->c[validate_php] = $this->c[validate]; // Whether or not you want us to validate the form via PHP after submitted. Default = 1
			if(!x($this->c[validate_javascript])) $this->c[validate_javascript] = $this->c[validate]; // Whether or not you want us to validate the form via javascript before submitted. Default = 1
			
			// Action
			//if(!$this->c[action]) $this->c[action] = URL; // Don't think I need this
			
			// URL
			if(!$this->c[url]) $this->c[url] = URL; // Used when redirecting after error
			
			// Method - make sure it's all uppercase
			$this->c[method] = strtoupper($this->c[method]);
		}
		
		/** 
		 * Extends the class internally by calling up an input type specific class's method. For example, if input type = ckeditor, it'll call form_ckeditor and use that classes method if it exists.
		 *
		 * @param string $type The input 'type'. The class that extends this would be name form_$type.
		 * @param string $method The method we want to call up within that extending class.
		 * @param array $params An array of parameters to pass to the method.
		 * @return mixed Whatever the method in the extending class returned or, if nothing returned, true.  It won't return anything (false) if the class or method didn't exist.
		 */
		/*function extend($type,$method,$params) {
			$class = 'form_'.$type;
			if(class_exists($class)) {
				$instance = new $class($this->c);
				//if(method_exists($instance,$method)) { // Will always exist because we're extending the 'form' class so we'll always be returning 'true'...not sure what to do.
					// Call method
					//print "calling ".$method." on class ".$class."<br />";
					$return = call_user_func_array(array($instance,$method),$params);
					
					// Return
					//if(!x($return)) $return = true;
					return $return;
				//}
			}
		}
		
		/**
		 * Catches legacy inputs and determines what new method class they should use (if one exists).
		 *
		 * @param string $label The label of the input
		 * @param string $type The input type (ex: text, textarea, hidden, etc.)
		 * @param string $name The name of the input field (the key in the $_POST/$_GET results)
		 * @param string $value The current value of this input
		 * @param array $c An array of config values. Default = NULL
		 * @return object The object for this input (if the new method input class exists).
		 */
		function input_upgrade($label,$type,$name = NULL,$value = NULL,$c = NULL) {
			// Already an object (aka, already using new method)
			if(is_object($label)) {
				return $label;
			}
			
			// Class
			$class = 'form_input_'.$type;
			// Class - name changed
			if($type == "ckeditor") $class = 'form_input_textarea_'.$type;
			if($type == "swfupload") $class = 'form_input_file_'.$type;
			
			// Exists?
			if(!class_exists($class)) {
				// No, try framework class
				$class .= "_framework";
				// That doesn't exist either, we don't have a class to handle this input type yet
				if(!class_exists($class)) $class = NULL;
			}
			
			// Input object
			if($class) {
				// Hidden - just $name and $value
				if($type == "hidden") {
					$c[label] = $label;
					//$input = $class::load($name,$value,$c);
					$input = call_user_func(array($class,'load'),$name,$value,$c);
				}
				// Select/checkbox/radio - inclue $options array
				else if($c[options]) {
					//$input = $class::load($label,$name,$c[options],$value,$c);
					$input = call_user_func(array($class,'load'),$label,$name,$c[options],$value,$c);
				}
				// HTML / submit / button - just $value
				else if(in_array($type,array('html','submit','button'))) {
					//$input = $class::load($value,$c);
					$input = call_user_func(array($class,'load'),$value,$c);
				}
				// Collapsed - just $label
				else if($type == "collapsed") {
					//$input = $class::load($label,$c);
					$input = call_user_func(array($class,'load'),$label,$c);
				}
				// Collapsed End - just $c
				else if($type == "collapsed_end") {
					//$input = $class::load($c);
					$input = call_user_func(array($class,'load'),$c);
				}
				// Default
				else {
					//$input = $class::load($label,$name,$value,$c);
					$input = call_user_func(array($class,'load'),$label,$name,$value,$c);
				}
			}
			
			// Return
			return $input;
		}
		
		/**
		 * Adds an input to the form
		 *
		 * @param string $label The label of the input
		 * @param string $type The input type (ex: text, textarea, hidden, etc.)
		 * @param string $name The name of the input field (the key in the $_POST/$_GET results)
		 * @param string $value The current value of this input
		 * @param array $c An array of config values. Default = NULL
		 */
		function input($label,$type,$name = NULL,$value = NULL,$c = NULL) {
			// New method
			if($input = $this->input_upgrade($label,$type,$name,$value,$c)) {
				$this->inputs[] = $input;
			}
			// Old method
			else {
				// Config // Moved to input_standardize()
				/*if(!x($c[process]) and $name) $c[process] = 1; // Whether or not we want to process the input's value or not (meaning put it in the resulting $values array).
				if(!x($c[purify])) $c[purify] = 1; // Whether or not we want to 'purify' the values submitted via this input when we're 'processing' it. Can also pass an array which is the $c array in the $form->value_purify() method.
				if(!x($c[inline])) $c[inline] = $this->c[inline]; // Whether we want the input to appear inline with the previous input or below it.*/
				
				// Variables
				$type = trim(strtolower($type)); // Type - make sure it's all lowercase, do in input_standardize() too, but need since we may call up class form_$type
				$external = 0;
				
				// Extended input
				/*if(!$c[form]) { // Make sure we're not calling this from 
					$params = func_get_args();
					$params[1] = $type; // Make sur ewe're using the lowercase type
					$params[4][form] = $this; // Need to pass form so we can effect it the new instance of the extending class
					$array = $this->extend($type,__FUNCTION__,$params);
				}
				
				// Local input
				if(!$array) {*/
					// Label
					$label = $this->input_label($label);
					if(x($c[label])) {
						$c[label] = $this->input_label($c[label]);
						if(!x($label)) $label = array();
						$label = array_merge($c[label],$label);
					}
					
					// Remove form (in case we processed it through an extenral class where we passed $c[form])
					if($c[form]) unset($c[form]);
					
					// Array
					$array = $c;
					$array[label] = $label;
					$array[type] = $type;
					$array[name] = $name;
					$array[value] = $value;
				//}
				
				if($array) {
					// Standardize
					$this->input_standardize($array);
					
					// Save
					$this->inputs[] = $array;
				}
			}
		}
		
		/**
		 * Updates the given input with the given array of information, merging the new info with the old info.
		 *
		 * If you want to completely replace the input's info, use the replace() method.
		 *
		 * Note, this will be deprecated when we make the move to the 'New method' as we don't want to go about
		 * merging two objects.  You must simply 'replace' the object.
		 *
		 * @param string $name The name of the input field you want to update.
		 * @param object $input The new object for this input.
		 */
		function input_update($name,$input) {
			// Error
			if(!$name or !$array) return;
			
			// Inputs
			if($this->inputs) {
				foreach($this->inputs as $k => $v) {
					// New method
					if(is_object($v)) {
						if($v->name == $name) {
							$this->inputs[$k] = $input;
						}
					}
					// Old method - used to 'merge' the 2 arrays, not overwrite it like the new method does
					else {
						if($v[name] == $name) {
							// Merge
							$this->inputs[$k] = array_merge($this->inputs[$k],$input);
						}
					}
				}
			}
		}
		
		/**
		 * Replaces the given input with the given array of information, merging the new info with the old info.
		 *
		 * If you want to simply update (merge) the input's info, use the update() method.
		 *
		 * @param string $name The name of the input field you want to replace.
		 * @param object $input The new input object you want to replace the existing one with.
		 */
		function input_replace($name,$input) {
			// Error
			if(!$name or !$array) return;
			
			// Inputs
			if($this->inputs) {
				foreach($this->inputs as $k => $v) {
					$match = 0;
					// New method
					if(is_object($v)) {
						if($v->name == $name) $match = 1;
					}
					// Old method
					else {
						if($v[name] == $name) $match = 1;
					}
					if($match) {
						// Replace
						$this->inputs[$k] = $input;
					}
				}
			}
		}
		
		/**
		 * Removes an input from the form.
		 *
		 * @param string $name The name of the input field you want to remove.
		 * @return boolean Whether or not we were able to locate and remove that input.
		 */
		function input_remove($name) {
			// Error
			if(!$name) return false;
			
			// Inputs
			if($this->inputs) {
				foreach($this->inputs as $k => $v) {
					$match = 0;
					// New method
					if(is_object($v)) {
						if($v->name == $name) $match = 1;
					}
					// Old method
					else {
						if($v[name] == $name) $match = 1;
					}
					if($match) {
						// Remove
						unset($this->inputs[$k]);
						
						// Return true
						return true;	
					}
				}
			}
			
			// Return false
			return false;
		}
			
		/**
		 * Returns the HTML element used for the given input type.
		 *
		 * @param string|array $type The input type or an input's full array (which includes the 'type' value).
		 * @return string The HTML element used for the given input type.
		 */
		function input_element($type) {
			// Type
			if(is_array($type)) $type = $type[type];
			
			// Element
			if(in_array($type,array("textarea","textarea_ckeditor"))) $element = "textarea";
			else if(in_array($type,array("select"))) $element = "select";
			else if(in_array($type,array("container","row","field","html"))) $element = "div";
			else if($type == "label") $element = "label";
			else if($type == "form") $element = "form";
			else if($type) $element = "input";
			
			return $element;
		}
		
		/**
		 * Standardizes the given array of options.
		 *
		 * Want in array('value' => '123','label' => 'Item 123') format instead of array('123' => 'Item 123') format.
		 *
		 * @param array $input The input in which you want to standardize the options.
		 * @return array The standardized array of options.
		 */
		/*function input_options($input) {
			// Options
			$options = NULL;
			if($input[options]) {
				foreach($input[options] as $k => $v) {
					// Re-format
					if(!is_array($v)) {
						$v = array(
							'value' => $k,
							'label' => $v
						);
					}
					// Children
					if($v[options]) {
						$v[options] = $this->input_options(array('options' => $v[options]));
					}
					// Store
					$options[] = $v;
				}
			}
			
			// Return
			return $options;
		}
		
		/**
		 * Returns a string of a option elements for a 'select' input.
		 *
		 * @param array $options The array of options to create.
		 * @param string|array $value The current selected value. Default = NULL
		 * @param array $c An array of config values. Default = NULL
		 * @return string A string of HTML <option> elements.
		 */
		/*function input_select_options($options,$value = NULL,$c = NULL) {
			// Config
			if(!$c[indent]) $c[indent] = 0; // Number of levels to indent this option.
			
			// Indent
			for($x = 0;$x < $c[indent];$x++) $indent .= "&nbsp;&nbsp;&nbsp;";
			
			// Child config
			$_c = $c;
			$_c[indent] += 1;
			
			// Value
			if(!is_array($value)) $value = (string) $value;
			
			// Options
			foreach($options as $option) {
				// Optgroup
				if($option[options] and !x($option[value])) {
					// String + children
					$options_string .= "<optgroup label='".string_encode($indent.$option[label],1)."'>".$this->input_select_options($option[options],$value,$_c)."</optgroup>";
				}
				// Option
				else {
					// Value
					$option[value] = (string) $option[value];
					
					// Selected?
					if(x($value) and ($value === $option[value] or is_array($value) and in_array($option[value],$value))) {
						$option[selected] = 'selected';
					}
					// String
					$attributes = $this->attributes($option);
					$options_string .= "<option value='".string_encode($option[value])."'".($attributes ? " ".$attributes : "").">".$indent.$option[label]."</option>";
					
					// Children
					if($option[options]) {
						$options_string .= $this->input_select_options($option[options],$value,$_c);
					}
				}
			}
			
			// Return
			return $options_string;
		}
		
		/**
		 * Returns a string of checkbox/radio buttons for the given option(s).
		 *
		 * @param array $options The array of options to create inputs out of.
		 * @param string|array $value The current selected value. Default = NULL
		 * @param array $c An array of config values. Default = NULL
		 * @return string A string of HTML <input> elements.
		 */
		/*function input_check_options($options,$value = NULL,$c = NULL) {
			// Config
			if(!$c[separator]) $c[separator] = "<br />"; // What string do you want to use when separting the checkboxes/radio buttons
			if(!$c[indent]) $c[indent] = 0; // Number of levels to indent this option.
			if(!$c[id]) $c[id] = "form-checkbox-".mt_rand(); // ID to use for option (if no input id is defined and no option specific id is defined)
			
			// Indent
			for($x = 0;$x < $c[indent];$x++) $indent .= "&nbsp;&nbsp;&nbsp;&nbsp;";
			
			// Value
			if(!is_array($value)) $value = (string) $value;
			
			// Options
			$x = 0;
			foreach($options as $option) {
				// Value
				$option[value] = (string) $option[value];
				// Checked
				$checked = (x($value) && ($value === $option[value] || is_array($value) && in_array($option[value],$value)) ? 1 : 0);
				// Attributes
				$option_attributes = $c[attributes];
				// ID
				$option_id = NULL;
				if($option[id]) $option_id = $option[id];
				else if($c[count] == 1) $option_id = $c[id];
				if(!$option_id) $option_id = $c[id].$c[indent]."-".$x; // More than one option or no $input[id] was present, create on own
				$option_attributes .= ($option_attributes ? " " : "")."id='".$option_id."'";
				
				// Input
				$options_string .= $indent."<input value='".$option[value]."'".($checked ? " checked='checked'" : "").($option_attributes ? " ".$option_attributes : "")." /> <label for='".$option_id."'>".$option[label].$c[separator]."</label>";
				
				// Children
				if($option[options]) {
					$_c = $c;
					$_c[indent] += 1;
					$_c[id] = $option_id;
					$options_string .= $this->input_check_options($option[options],$value,$_c);	
				}
				
				// Counter
				$x++;
			}
			
			// Return
			return $options_string;
		}
		
		/**
		 * Standardizes an input and it's configuration values.
		 *
		 * @param array $input The input array we want to standardize.
		 * @param array $c An array of configuration values. Only really presents so we can pass $c[form]. Default = NULL
		 * @return array The standardized input array.
		 */
		function input_standardize($input,$c = NULL) {
			// New method
			if(is_object($input)) {
				# Nothing, no longer use standardization except when we create the input
			}
			// Old method
			else {
				// Type
				$input[type] = trim(strtolower($input[type]));
				// Label
				$input[label] = $this->input_label($input[label]);
				
				// Extended input
				/*if(!$c[form]) {
					$params = func_get_args();
					$params[0] = $input; // Make sure we're using the most recent $input
					$params[1][form] = $this;
					$_input = $this->extend($input[type],__FUNCTION__,$params);
					if(x($_input)) $input = $_input;
				}*/
				
				// Value - must do before 'file' standardization as it uses $input[value] for $input[file][old] and 'required' classes
				if(!x($input[value])) {
					// Value - global - passed array('values' => array('field_name' => 'field value here')) when creating form instance
					if($input[name] and x($this->c[values][$input[name]])) $input[value] = $this->c[values][$input[name]];
					// Default
					else if(x($input['default'])) $input[value] = $input['default'];
					// Default - global - passed array('defaults' => array('field_name' => 'field value here')) when creating form instance
					else if($input[name] and x($this->c[defaults][$input[name]])) $input[value] = $this->c[defaults][$input[name]];
				}
				
				// File
				/*if(in_array($input[type],array('file'))) {
					// Config
					if(!$input[file][old]) $input[file][old] = $input[value];
					if(!$input[size]) $input[size] = 20; // Size attributes of file input
					if(!x($input[file][preview])) $input[file][preview] = 1; // Whether or not we want to show preview of existing file on file inputs.
					if(!x($input[file][overwrite])) $input[file][overwrite] = 0; // Whether or not to overwrite existing files if the uploaded one has the same name.
					if(!strstr($input[onchange],'file_clear_button')) $input[onchange] .= "file_clear_button(this);"; // Add's functionality for clearning file after selecting one.
					
					// Remove required (but add classes indicating it should be required)
					if($input[value] and $input[validate][required]) {
						$input[validate][required] = 0;
						$input['class'] .= ($input['class'] ? " " : "")."required-input required-icon";
					}
				}*/
				// Date
				/*if(in_array($input[type],array("date","date_select","datetime")) and !$input[date]) {
					$input[date] = array(
						'format' => ($input[format] ? $input[format] : ($input[type] == "datetime" ? "datetime" : "date")), // Legacy, $input[date][format] used to be $input[format]
						'year_min' => 1900,
						'year_max' => 2100,
					);
				}*/
				// Select
				/*if($input[type] == "select") {
					if($input[multiple]) {
						$input[multiple] = "multiple";
						if($input[name] and substr($input[name],-2) == "[]") $input[name] = substr($input[name],0,(strlen($input[name]) - 2)); // Need so we get proper 'value' before creating HTML where we'll add it back?
						if(!strstr($input['class'],'form-input-select-multiple')) {
							$input['class'] .= ($input['class'] ? " " : "")."form-input-select-multiple";
						}
					}
				}
				// Password
				/*if($input[type] == "password") {
					if(!$input[autocomplete]) $input[autocomplete] = "off"; // Prevent Firefox from 'autocompleting' password
				}*/
				// Submit
				/*if($input[type] == "submit") {
					if(!$input[autocomplete]) $input[autocomplete] = "off"; // Prevent Firefox from 'autocompleting' button state to disabled when we press 'Back' in browser
				}*/
				
				// Options
				if($input[options]) {
					$input[options] = $this->input_options($input);
					// No options, remove 'required'
					if(!$input[options]) $input[required] = 0;
				}
				
				// Process - whether or not we want to process the input's value or not (meaning put it in the resulting $values array)
				if(!x($input[process]) and $input[name]) $input[process] = 1; 
				// Purify - whether or not we want to 'purify' the values submitted via this input when we're 'processing' it. Can also pass an array which is the $c array in the $form->value_purify() method
				if(!x($input[purify])) $input[purify] = 1;
				// Inline - whether we want the input to appear inline with the previous input or below it
				if(!x($input[inline])) $input[inline] = $this->c[inline];
				// Disabled - make sure it's set to 'disabled' (ex: $input[disabled] = 1; becomes $input[disabled] = 'disabled';
				if($input[disabled]) $input[disabled] = 'disabled';
				// Readonly - make sure it's set to 'readonly' (ex: $input[readonly] = 1; becomes $input[readonly] = 'readonly';
				if($input[readonly]) $input[readonly] = 'readonly';
				
				// Validate - legacy - move legacy variables to 'validate' array
				if(x($input[required]) and !x($input[validate][required])) $input[validate][required] = $input[required];
				if($input[min] and !$input[validate][min]) $input[validate][min] = $input[min];
				if(x($input[max]) and !$input[validate][max]) $input[validate][max] = $input[max];
				if($input[minlength] and !$input[validate][minlength]) $input[validate][minlength] = $input[minlength];
				if($input[maxlength] and !$input[validate][maxlength]) $input[validate][maxlength] = $input[maxlength];
				if($input[accept] and !$input[validate][accept]) $input[validate][accept] = $input[accept];
				
				// Validate - accept - make sure it's a string
				if($input[validate][accept]) {
					if(is_array($input[validate][accept])) $input[validate][accept] = implode(',',$input[validate][accept]);		
					$input[validate][accept] = str_replace(' ','',$input[validate][accept]); // Remove spaces
				}
				// Don't want to guess that they wanted to validate as such
				// Validate - number
				//if($input[type] == "number" and !x($input[validate][number])) $input[validate][number] = 1;
				// Validate - email
				//if($input[type] == "email" and !x($input[validate][email])) $input[validate][email] = 1;
				// Validate - url
				//if($input[type] == "url" and !x($input[validate][url])) $input[validate][url] = 1;
			}
			
			// Return
			return $input;
		}
		
		/**
		 * Standardizes input's label, usually just turning 'label' => $value to 'label' => array('value' => $value).
		 *
		 * @param string|array $label The label you want to standardize.
		 * @return array The standardizes label array.
		 */
		function input_label($label) {
			if(x($label) and !is_array($label)) {
				$label = array('value' => $label);
			}
			return $label;
		}
		
		/**
		 * Creates the HTML for an input
		 *
		 * @param array $input The array of data for the input
		 * @param array $c An array of config values. Default = NULL
		 * @return string The HTML element for the input.
		 */
		function input_html($input,$c = NULL) {
			// New method
			if(is_object($input)) {
				// Value - saved
				if($this->values and $input->name and $input->process) {
					$input->value = form::value($this->values,$input->name);
					$input->value_temp = $input->value; // Used to use 'input_update() to do this, not sure if this will update it in $this->inputs or not
				}
				// HTML
				$html = $input->html_field($this,$c);
			}
			// Old method
			else {
				// Standardize - did in render(), but just in case we call this method externally
				$input = $this->input_standardize($input);
				
				// Element
				$element = $this->input_element($input[type]);
				
				// Class
				$input['class'] .= ($input['class'] ? " " : "")."form-input form-input-".$input[type];
				
				//print_array($input);
				
				// Extended input
				/*if(!$c[form]) {
					$params = func_get_args();
					$params[0] = $input; // Make sure we're using the most recent $input
					$params[1][form] = $this;
					$html = $this->extend($input[type],__FUNCTION__,$params);
				}
				
				// Local input
				if(!x($html)) {*/
					// Textarea
					/*if($element == "textarea") {
						$html = "<textarea ".$this->attributes($input).">".$input[value]."</textarea>";
					}
					// Select
					else if($element == "select") {
						// Multiple
						if($input[multiple]) {
							if($input[name] and substr($input[name],-2) != "[]") $input[name] .= "[]";
						}
						// Value
						if($input[value] and $input[multiple]) {
							$value = data_unserialize($input[value]);
							if(is_array($value)) $input[value] = $value;
						}
						
						// Options
						$options = $input[options];
						
						// Placeholder
						if($input[placeholder] and $input[options]) {
							$options_placeholder = array(array('label' => $input[placeholder]));
							if($options) $options = array_merge($options_placeholder,$options);
							else $options = $options_placeholder;
							unset($input[placeholder]);
						}
						
						// Options HTML
						$options_string = $this->input_select_options($options,$input[value]);
						if($options_string) {
							// Blank - auto // Not doing automatically, don't want to assume, must manually add in blank option via array('value' => '','label' => '') or new $input[options_blank] = 1 if we want a blank option at top of dropdown
							/*if(!strstr($options_string,"<option>") and !strstr($options_string,"value=''")) {
								$options_string = "<option value=''></option>".$options_string;
							}*/
							// Blank - manual
							/*if($input[options_blank]) {
								$options_string = "<option value=''></option>".$options_string;
							}
							// Select
							$html = "<select ".$this->attributes($input).">".$options_string."</select>";
						}
						// No options, remove 'required'
						else if($input[name] and $input[validate][required]) {
							$this->input_update($input[name],array('validate' => array('required' => 0)));
						}
					}*/
					// Input - checkbox/radio
					/*else if($element == "input" and in_array($input[type],array('checkbox','radio'))) {
						// Separator
						if(!$c[separator]) $c[separator] = $input[separator];
						
						// Options
						if($input[options]) {
							// Options count
							$c[count] = count($input[options]);
							
							// Multiple
							if($input[type] == "checkbox" and $c[count] > 1) {
								// Name
								if(substr($input[name],-2) != "[]") $input[name] .= "[]";	
							}
							
							// Value - should mabye be in the 'Multiple' if statement above, but sometimes we serialize with just 1 option
							if($input[value]) {
								$value = data_unserialize($input[value]);
								if(is_array($value)) $input[value] = $value;
							}
							
							// ID
							if($input[id]) $c[id] = $input[id];
							else $c[id] = "form-checkbox-".str_replace(array('[',']'),array('-',''),$input[name])."-";
							unset($input[id]);
							
							// Attributes
							$c[attributes] = $this->attributes($input);
						
							// HTML
							$html = $this->input_check_options($input[options],$input[value],$c);
							// Required
							if($html) {
								if($input[validate][required]) $html .= "<label class='error form-error' for='".$input[name]."'".($this->c[errors_inline] && $input[name] && $this->errors[inline][$input[name]] ? " style='display:inline;'>".$this->errors[inline][$input[name]] : ">This field is required.")."</label>";	
							}
						}
						// No options, remove 'required'
						else if($input[name] and $input[validate][required]) {
							$this->input_update($input[name],array('validate' => array('required' => 0)));
						}
					}
					// Input - date
					/*else if($element == "input" and $input[type] == "date") {
						// Time
						$time = 0;
						if($input[value]) $time = time_format($input[value],'unix');
						// Value (in mm/dd/yyyy format)
						if($time) $input[value] = adodb_date('m/d/Y',$time);
						else $input[value] = NULL;
						// Type - maybe in future we can use type=date, but not well enough supported yet (only Opera 9.5+ and Chrome 20+). We'll use class='date' to apply datepicker for now.
						$input[type] = "text";
						
						// Class
						$input['class'] .= ($input['class'] ? " " : "")."date";
						// Years
						$years = "yearRange:[".($input[date][year_min] - date('Y')).",".($input[date][year_max] - date('Y') + 1)."]";
						if(strstr($input['class'],' {')) $input['class'] = str_replace(' {',' {'.$years.',',$input['class']);
						else $input['class'] .= " {".$years."}";
						
						// Input
						$html = "
					".include_css('jquery.tools.dateinput',0)."
					".include_javascript('jquery.tools.dateinput',0)."
					<input ".$this->attributes($input)." />";	
					}
					// Input - date_select
					else if($element == "input" and $input[type] == "date_select") {
						// Name
						$name = str_replace(array('[',']'),'',$input[name]);
						
						// Attributes
						#build this. need to strip out required or group required by all 3. id (if exists) must be appeneded with _month or _day or _year
						
						// Time
						$time = 0;
						if($input[value]) $time = time_format($input[value],'unix');
						
						// Month
						$value = NULL;
						if($time) $value = adodb_date('n',$time);
						$html .= "
					<select name='".$name."_month'>
						<option value=''>Month</option>";
						for($x = 1;$x <= 12;$x++) $html .= "
						<option value='".$x."'".($time && $value == $x ? " selected='selected'" : '').">".date('F',mktime(0,0,0,$x,1,2000))."</option>";
						$html .= "
					</select>";
					
						// Day
						$value = NULL;
						if($time) $value = adodb_date('j',$time);
						$html .= "
					<select name='".$name."_day'>
						<option value=''>Day</option>";
						for($x = 1;$x <= 31;$x++) $html .= "
						<option value='".$x."'".($time && $value == $x ? " selected='selected'" : '').">".$x."</option>";
						$html .= "
					</select>";
					
						// Year
						$value = NULL;
						if($time) $value = adodb_date('Y',$time);
						$html .= "
					<select name='".$name."_year'".($input[validate][required] ? " class='required'" : "").">
						<option value=''>Year</option>";
						for($x = $input[date][year_min];$x <= $input[date][year_max];$x++) $html .= "
						<option value='".$x."'".($time && $value == $x ? " selected='selected'" : '').">".$x."</option>";
						$html .= "
					</select>";
					
						$html .= "
					<label class='error form-error' for='".$input[name]."_year'".($this->c[errors_inline] && $input[name] && $this->errors[inline][$input[name]] ? " style='display:inline;'>".$this->errors[inline][$input[name]] : ">Please select a date.")."</label>";
					}
					// Input - datetime
					else if($element == "input" and $input[type] == "datetime") {
						// Config
						if(!$input[hours]) $input[hours] = 12; // What hour format do you want to use: 12 [default, includes AM/PM], 24
						if(!$input[minutes]) $input[minutes] = 1; // Increment between minutes (ex: $input[minutes] = 15; options = 00, 15, 30, 45)
						
						// Name
						$name = str_replace(array('[',']'),'',$input[name]);
						
						// Time
						$time = 0;
						if($input[value]) $time = time_format($input[value],'unix');
						
						// Date
						$_input = $input;
						$_input[type] = "text";
						// Date - class
						$_input['class'] .= ($_input['class'] ? " " : "")."date";
						// Date - years
						$years = "yearRange:[".($_input[date][year_min] - date('Y')).",".($_input[date][year_max] - date('Y') + 1)."]";
						if(strstr($_input['class'],' {')) $_input['class'] = str_replace(' {',' {'.$years.',',$_input['class']);
						else $_input['class'] .= " {".$years."}";
						// Date - value
						if($time) $_input[value] = adodb_date('m/d/Y',$time);
						else $_input[value] = NULL;
						// Date - input
						$html = "
					".include_css('jquery.tools.dateinput',0)."
					".include_javascript('jquery.tools.dateinput',0)."
					<input ".$this->attributes($_input)." />";
						
						// Hour
						$_input = $input;
						$_input[name] = $name."_hour";
						unset($_input[id]);
						$value = NULL;
						if($input[hours] == 24) {
							$start = 0;
							$end = 23;
							if($time) $value = adodb_date('h',$time);
						}
						else {
							if($hour >= 12) {
								$meridiem = "pm";	
								if($hour > 12) $hour -= 12;
							}
							$start = 1;
							$end = 12;
							if($time) $value = adodb_date('g',$time);
						}
						// Hour
						$html .= "
					<select ".$this->attributes($_input).">
						<option value=''>Hr</option>";
						for($x = $start;$x <= $end;$x++) $html .= "
						<option value='".$x."'".($time && $value == $x ? " selected='selected'" : '').">".str_pad($x,2,0,STR_PAD_LEFT)."</option>";
						$html .= "
					</select> :";
					
						// Minute
						$_input = $input;
						$_input[name] = $name."_minute";
						unset($_input[id]);
						$value = NULL;
						if($time) $value = adodb_date('i',$time);
						$html .= "
					<select ".$this->attributes($_input).">
						<option value=''>Min</option>";
						for($x = 0;$x < 60;$x += $input[minutes]) $html .= "
						<option value='".$x."'".($time && $value == str_pad($x,2,0,STR_PAD_LEFT) ? " selected='selected'" : '').">".str_pad($x,2,0,STR_PAD_LEFT)."</option>";
						$html .= "
					</select>";
					
						// Meridiem
						if($input[hours] != 24) {
							$_input = $input;
							$_input[name] = $name."_meridiem";
							$value = NULL;
							if($time) $value = adodb_date('a',$time);
							$html .= "
					<select ".$this->attributes($_input).">
						<option value='am'".($value == "am" ? " selected='selected'" : '').">AM</option>
						<option value='pm'".($value == "pm" ? " selected='selected'" : '').">PM</option>
					</select>";
						}
					
						$html .= "
					<label class='error form-error' for='".$input[name]."'".($this->c[errors_inline] && $input[name] && $this->errors[inline][$input[name]] ? " style='display:inline;'>".$this->errors[inline][$input[name]] : ">Please select a date and time.")."</label>";
					}
					// Input - time
					else if($element == "input" and $input[type] == "time") {
						// Config
						if(!$input[hours]) $input[hours] = 12; // What hour format do you want to use: 12 [default, includes AM/PM], 24
						if(!$input[minutes]) $input[minutes] = 1; // Increment between minutes (ex: $input[minutes] = 15; options = 00, 15, 30, 45)
						
						// Name
						$name = str_replace(array('[',']'),'',$input[name]);
						
						// Parse
						if($input[value]) list($hour,$minute,$second) = explode(':',$input[value]);
						$meridiem = "am";
						
						// Hour
						if($input[hours] == 24) {
							$start = 0;
							$end = 23;
						}
						else {
							if($hour >= 12) {
								$meridiem = "pm";	
								if($hour > 12) $hour -= 12;
							}
							$start = 1;
							$end = 12;
						}
						$html .= "
					<select name='".$name."_hour'>
						<option value=''>Hr</option>";
						for($x = $start;$x <= $end;$x++) $html .= "
						<option value='".$x."'".($input[value] && $hour == $x ? " selected='selected'" : '').">".str_pad($x,2,0,STR_PAD_LEFT)."</option>";
						$html .= "
					</select> :";
					
						// Minute
						$value = $minute;
						$html .= "
					<select name='".$name."_minute'>
						<option value=''>Min</option>";
						for($x = 0;$x < 60;$x += $input[minutes]) $html .= "
						<option value='".$x."'".($input[value] && $minute == str_pad($x,2,0,STR_PAD_LEFT) ? " selected='selected'" : '').">".str_pad($x,2,0,STR_PAD_LEFT)."</option>";
						$html .= "
					</select>";
					
						// Meridiem
						if($input[hours] != 24) {
							$html .= "
					<select name='".$name."_meridiem'>
						<option value='am'".($meridiem == "am" ? " selected='selected'" : '').">AM</option>
						<option value='pm'".($meridiem == "pm" ? " selected='selected'" : '').">PM</option>
					</select>";
						}
					
						$html .= "
					<label class='error form-error' for='".$input[name]."'".($this->c[errors_inline] && $input[name] && $this->errors[inline][$input[name]] ? " style='display:inline;'>".$this->errors[inline][$input[name]] : ">Please select a time.")."</label>";
					}
					// HTML
					else if($input[type] == "html") {
						//$html = "<div ".$this->attributes($input).">".$input[value]."</div>";	 // Can't use this as we may be calling </td><td> or something and can't encapsulate that in divs. Just add what we need to the actual HTML.
						$html = $input[value];	
					}
					// Price
					/*else if($input[type] == "price") {
						$input[type] = "number";
						if(x($input[value])) $input[value] = number_format($input[value],2);
						$html = "$<input ".$this->attributes($input)." />";
					}
					// Percent
					else if($input[type] == "percent") {
						$input[type] = "number";
						$html = "<input ".$this->attributes($input)." />%";
					}
					// Input
					else {*/
						$html = "<input ".$this->attributes($input)." />";
					//}
					
					// Prepend
					if($input[prepend]) $html = $input[prepend].$html;
					
					// Append
					if($input[append]) $html .= $input[append];
				
					// Help
					if($input[help]) $html .= help($input[help]);
					
					// File
					/*if($input[type] == "file" and $input[file][preview] and $input[value] and $input[file][path]) {
						$html .= $this->file_preview($input);
					}*/
				}
			//}
				
			//print "<xmp>".$html."</xmp>";
			
			// Return
			return $html;
		}
		
		/**
		 * Renders the HTML for an input and its label.
		 *
		 * @param array $input The array of data for the input
		 * @param array $c An array of config values. Default = NULL
		 * @return string The HTML element for the input and label.
		 */
		function input_render($input,$c = NULL) {
			// New method
			if(is_object($input)) {
				// Value - saved
				if($this->values and $input->name and $input->process) {
					$input->value = form::value($this->values,$input->name);
					$input->value_temp = $input->value; // Used to use 'input_update() to do this, not sure if this will update it in $this->inputs or not
				}
				// Error
				if($this->c[errors_inline] and $input->name and $error = $this->errors[inline][$input->name]) {
					$input->error = $error;
				}
				// HTML
				$html = $input->html_row($this,$c);
			}
			// Old method
			else {
				// Wrap
				$wrap = 1;
				if($input[type] == "hidden") $wrap = 0; // Leaving until we get all inputs to form_input as we will use hidden input if it doesn't match framework $input[types] filter
				//if($input[type] == "html" and !$input[label][value] and !$input[row] and !$input[field]) $wrap = 0;
				
				// Row
				$input[row][type] = 'row';
				$input[row]['class'] .= ($input[row]['class'] ? " " : "")."form-row form-row-".$input[type];
				$row_attributes = $this->attributes($input[row]);
				
				// Label
				$input[label] = $this->input_label($input[label]);
				$input[label][type] = 'label';
				$input[label]['class'] .= ($input[label]['class'] ? " " : "")."form-label form-label-".$input[type];
				if(!x($input[label][value])) $input[label]['class'] .= ($input[label]['class'] ? " " : "")."form-label-blank";
				$label_attributes = $this->attributes($input[label]);
				
				// Field
				$input[field][type] = 'field';
				$input[field]['class'] .= ($input[field]['class'] ? " " : "")."form-field form-field-".$input[type];
				$field_attributes = $this->attributes($input[field]);
				
				// Value - saved
				if($this->values and $input[name] and $input[process]) {
					$input[value] = form::value($this->values,$input[name]);
					$this->input_update($input[name],array('temp' => $input[value]));
				}
				// Value - default
				else if(!x($input[value]) and x($input['default'])) $input[value] = $input['default'];
				
				// Help - radio/checkbox - if more than one option (or is an 'option class') we need to add after label, not the input HTML
				/*if($input[help] and in_array($input[type],array('radio','checkbox')) /*and (count($input[options]) > 1 or !is_array($input[options]))*//*) {
					$input[label][value] .= help($input[help]);
					$input[help] = NULL;
				}*/
				
				// HTML
				$input_html = $this->input_html($input);
				if($input_html/* or $input[type] == "html" and $input[label][value]*/) {
					// Hidden / HTML (no wrapping divs)
					if(!$wrap) $html = "
		".$input_html;
					// Default
					else {
						$html = "
		<div".($row_attributes ? " ".$row_attributes : "").">
			<div".($label_attributes ? " ".$label_attributes : "").">".$input[label][value]."</div>
			<div".($field_attributes ? " ".$field_attributes : "").">
				".$input_html;
						if($this->c[errors_inline] && $input[name] and $error = $this->errors[inline][$input[name]] and !strstr($input_html,'form-error')) $html .= "
				<label class='error form-error' for='".$input[name]."' style='display:inline;'>".$error."</label>";
						$html .= "
			</div>
			<div class='form-clear form-row-clear'></div>
		</div>";
					}
				}
			}
			
			// Return
			return $html;
		}
		
		/**
		 * Checks whether or not the given input 'name' already exists in the stored inputs.
		 *
		 * @param string $name The 'name' value you want to search for in existing inputs.
		 * @return object The input object, if it exists. If it doesn't, nothing is returned.
		 */
		function input_exists($name) {
			$exists = NULL;
			foreach($this->inputs as $k => $v) {
				// New method
				if(is_object($v)) {
					if($v->name == $name) {
						$exists = $v;
						break;
					}
				}
				// Old method
				else {
					if($v[name] == $name) {
						$exists = $v;
						break;
					}
				}
			}
			return $exists;
		}
			
		/**
		 * Processes the value for the given input.
		 *
		 * @param array $input The array of the input you want to process.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return mixed The resulting value.
		 */
		function input_process($input,$c = NULL) {
			// New method
			if(is_object($input)) {
				$value = $input->process($this,$c);
			}		
			// Old method
			else {
				$value = form::value($this->post,$input[name]);
			}
			
			// Return
			return $value;
		}

		/**
		 * Processes a file input in the submitted form.
		 *
		 * @param array $input The inputs array of data.
		 * @param string|array $value The value submitted. Either a file name or uploaded file array. Default = NULL
		 * @param array $c An array of configuration values. Default = NULL
		 * @return string The resulting value (file name).
		 */
		/*function input_process_file($input,$value = NULL,$c = NULL) {
			// Delete
			$delete = 0;
			
			// Original value
			$value_original = $value;
			
			// Upload (or already uploaded and have resulting value)
			if($input[file][path] and ($value[name] or !is_array($value) and $value)) {
				// Advanced
				if(class_exists('file')) {
					// Process / upload
					$f = new file($value,array('overwrite' => $input[file][overwrite],'debug' => $this->c[debug]));
					
					// Error uploading
					if(!$f->exists) {
						$this->errors[header][$input[name]] = "There was an error uploading ".($input[label][value] ? "the ".$input[label][value] : "a")." file.";
						$this->errors[inline][$input[name]] = "There was an error uploading this file.";
						return;
					}
					
					// Resize
					if($input[file][width] and $input[file][height] and $f->type == "image") {
						$f->image_thumb($input[file][width],$input[file][height],array('crop' => $input[file][crop],'enlarge' => $input[file][enlarge]));
					}
					
					// Save
					if($f_saved = $f->save($input[file][path],array('smush' => $input[file][smush]))) {
						$f = $f_saved;
						// Value
						$value = $f->name;
						
						// Store
						$this->values_files[$input[name]]['new'] = 1;
						$this->values_files[$input[name]][name] = $f->name;
						$this->values_files[$input[name]][type] = $f->type;
						$this->values_files[$input[name]][extension] = $f->extension;
						$this->values_files[$input[name]][path] = $input[file][path]; // Should maybe use the actual path (see below) in case we ever get a different file via the file browser) but old files only used the relative path (not full server path). Also, would have to update if we change servers and is just more to store.
						//$this->values_files[$input[name]][path] = $f->path;
						$this->values_files[$input[name]][width] = $f->width();
						$this->values_files[$input[name]][height] = $f->height();
						$this->values_files[$input[name]][length] = $f->length();
						$this->values_files[$input[name]][size] = $f->size();
						
						// Quickstart
						if($f->type == "video" and $f->extension_standardized == "mp4") {
							$f->video_faststart(array('debug' => $this->c[debug]));
						}
						
						// Extensions
						if($input[file][extensions]) {
							// Unstore, we'll recreate in after conversion
							$this->values_files[$input[name]][extensions] = NULL;
							
							// Debug
							debug("Converting file to other extensions:".return_array($input[file][extensions]),$this->c[debug]);
							
							// File
							$f = new file($input[file][path].$value,array('debug' => $this->c[debug]));
							
							// Extensions - video
							if($f->type == "video") {
								// Extensions
								foreach($input[file][extensions] as $extension => $v) {
									$converted = 0;
									
									// Already converted - might have selected a previously uploaded file via the file browser
									$name = str_replace(".".$f->extension,".".$extension,$f->name);
									$f_convert = new file($v[path].$name);
									if($f_convert->exists and !is_array($value_original)) {
										debug("already convereted, not re-converting",$this->c[debug]);
										$converted = 1;
									}
									// Convert
									else {
										$f_convert = $f;
										$f_convert->video_convert($extension,array('path' => $v[path],'debug' => $this->c[debug]));
										if($f_convert->exists) {
											$converted = 1;
										}
									}
									
									// Store
									if($converted) {
										$v[name] = $f_convert->name;
										$v[extension] = $f_convert->extension;
										$v[width] = $f_convert->width();
										$v[height] = $f_convert->height();
										$v[length] = $f_convert->length();
										$v[size] = $f_convert->size();
										$this->values_files[$input[name]][extensions][$extension] = $v;
									}
								}
							}
						}
						
						// Thumbs
						if($input[file][thumbs]) {
							// Defaults
							foreach($input[file][thumbs] as $thumb => $v) {
								if(!$v[count]) $input[file][thumbs][$thumb][count] = 1;
								if(!$v[extension])  $input[file][thumbs][$thumb][extension] = ($f->type == "video" ? "jpg" : $f->extension);	
							}
							
							// Unstore, we'll recreate in after thumbing
							$this->values_files[$input[name]][thumbs] = NULL;
							
							// Debug
							debug("Creating thumbnails from file:".return_array($input[file][thumbs]),$this->c[debug]);
							
							// File
							$f = new file($input[file][path].$value);
							// Thumbs - image
							if($f->type == "image") {
								foreach($input[file][thumbs] as $thumb => $v) {
									$thumbed = 0;
									
									// Already thumbed - might have selected a previously uploaded file via the file browser
									$f_thumb = new file($v[path].$f->name);
									if($f_thumb->exists and !is_array($value_original)) {
										debug("already thumbed, not re-thumbing",$this->c[debug]);
										$thumbed = 1;
									}
									// Thumb
									else {
										//$v[debug] = $this->c[debug];
										$f_thumb = new file($input[file][path].$value);
										$f_thumb->image_thumb($v[width],$v[height],$v);
										if($f_thumb_saved = $f_thumb->save($v[path])) {
											$f_thumb = $f_thumb_saved;
											if($f_thumb->exists) {
												$thumbed = 1;
											}
										}
									}
									
									// Store
									if($thumbed) {
										$v[name] = $f_thumb->name;
										$v[extension] = $f_thumb->extension;
										$v[size] = $f_thumb->size();
										//$v[width] = $f_thumb->width(); // Already set
										//$v[height] = $f_thumb->height(); // Already set
										$this->values_files[$input[name]][thumbs][$thumb] = $v;
									}
								}
							}
							// Thumbs - video
							if($f->type == "video") {
								// Count/extension of screenshots (these should be the same across all thumb sizes, but check just to be sure)
								$count = 4;
								$extension = "jpg";
								foreach($input[file][thumbs] as $thumb => $v) {
									if($v[count] and $v[count] > $count) $count = $v[count];
									if($v[extension]) $extension = $v[extension]; // All using one for now
								}
								
								// Get video screenshot(s) (save to temp folder for now)
								$temp = $f->temp();
								$thumbs = $f->video_thumb($temp,array('number' => $count,'extension' => $extension,'debug' => $this->c[debug]));
								debug("video thumbs: ".return_array($thumbs),$this->c[debug]);
								
								// Resize screenshots
								if($thumbs) {
									foreach($input[file][thumbs] as $thumb => $v) {
										foreach($thumbs as $x => $thumb_name) {
											if($x <= $v[count]) {
												$thumbed = 0;
									
												// Already thumbed - might have selected a previously uploaded file via the file browser
												$f_thumb = new file($v[path].$thumb_name);
												if($f_thumb->exists and !is_array($value_original)) {
													debug("already thumbed, not re-thumbing",$this->c[debug]);
													$thumbed = 1;
												}
												// Thumb
												else {
													//$v[debug] = $this->c[debug];
													$f_thumb = new file($temp.$thumb_name);
													$f_thumb->image_thumb($v[width],$v[height],$v); // If no width/height it won't thumb and save will effectively 'copy' the original thumb.
													if($f_thumb_saved = $f_thumb->save($v[path])) {
														$f_thumb = $f_thumb_saved;
														if($f_thumb->exists) {
															$thumbed = 1;	
														}
													}
												}
												
												// Store
												if($thumbed) {
													$v[name] = $f_thumb->name;
													$v[extension] = $f_thumb->extension;
													$v[size] = $f_thumb->size();
													//$v[width] = $f_thumb->width(); // Already set
													//$v[height] = $f_thumb->height(); // Already set
													$this->values_files[$input[name]][thumbs][$thumb][$x] = $v;
												}
											}
										}
									}
									// Delete temp video screenshot(s)
									foreach($thumbs as $x => $thumb_name) {
										debug("Deleting temp thumb: ".$temp.$thumb_name,$this->c[debug]);
										$f_thumb = new file($temp.$thumb_name);
										$f_thumb->delete();
									}
								}
							}
						}
					}
							
					// Values (framework only) - yes, this should be in the form_framework class, but that's a lot of duplicate code for a little change
					$values = NULL;
					if($this->module) {
						foreach($this->values as $k => $v) {
							if($key = array_search($k,m($this->module.'.db'))) {
								$values[$key] = $v;
							}
						}
					}
				
					// Storage - file
					if($this->values_files[$input[name]]) {
						// Storage
						$storage = $input[file][storage];
						debug("file, storage: ".$storage,$this->c[debug]);
						if($storage and $storage != "local") {
							$v = $this->values_files[$input[name]];
						
							// Push
							debug("Pushing ".$v[path].$v[name]." (local) to ".$v[path]." (".$storage.")",$this->c[debug]);
							$file = file::load(NULL,array('storage' => $storage,'debug' => $this->c[debug]));
							if($file_pushed = $file->push($v[path].$v[name],$v[path],array('values' => $values))) {
								// Update values
								$this->values_files[$input[name]][name] = $value = $file_pushed->name;
								$this->values_files[$input[name]][path] = $file_pushed->path;
								$this->values_files[$input[name]][extension] = $file_pushed->extension;
								$this->values_files[$input[name]][storage] = $file_pushed->storage;
							
								// Delete original
								debug("deleting original file: ".$v[path].$v[name],$this->c[debug]);
								$file = file::load($v[path].$v[name]);
								$file->delete(array('debug' => $this->c[debug]));
							}
							else {
								$this->values_files[$input[name]][storage] = "local";
							}
						}
					}
					
					// Storage - extensions
					if($this->values_files[$input[name]][extensions]) {
						foreach($this->values_files[$input[name]][extensions] as $k => $v) {
							// Storage
							$storage = $input[file][extensions][$k][storage];
							if(!$storage) $storage = $input[file][storage];
							debug("extension: ".$k.", storage: ".$storage,$this->c[debug]);
							if($storage and $storage != "local") {
								// Push
								debug("Pushing ".$v[path].$v[name]." (local) to ".$v[path]." (".$storage.")",$this->c[debug]);
								$file = file::load(NULL,array('storage' => $storage,'debug' => $this->c[debug]));
								if($file_pushed = $file->push($v[path].$v[name],$v[path],array('values' => $values))) {
									// Update values
									$this->values_files[$input[name]][extensions][$k][name] = $file_pushed->name;
									$this->values_files[$input[name]][extensions][$k][path] = $file_pushed->path;
									$this->values_files[$input[name]][extensions][$k][storage] = $file_pushed->storage;
								
									// Delete original
									debug("Deleting original file: ".$v[path].$v[name],$this->c[debug]);
									$file = file::load($v[path].$v[name]);
									$file->delete(array('debug' => $this->c[debug]));
								}
								else {
									$this->values_files[$input[name]][extensions][$k][storage] = "local";
								}
							}
						}
					}
					
					// Storage - thumbs
					if($this->values_files[$input[name]][thumbs]) {
						foreach($this->values_files[$input[name]][thumbs] as $k => $v) {
							// Storage
							$storage = $input[file][thumbs][$k][storage];
							if(!$storage) $storage = $input[file][storage];
							debug("thumb: ".$k.", storage: ".$storage,$this->c[debug]);
							if($storage and $storage != "local") {
								// Video - mulitple thumbs
								if($f->type == "video") {
									foreach($v as $k0 => $v0) {
										// Push
										debug("Pushing ".$v0[path].$v0[name]." (local) to ".$v[path]." (".$storage.")",$this->c[debug]);
										$file = file::load(NULL,array('storage' => $storage,'debug' => $this->c[debug]));
										if($file_pushed = $file->push($v0[path].$v0[name],$v0[path],array('values' => $values))) {
											// Update values
											$this->values_files[$input[name]][thumbs][$k][$k0][name] = $file_pushed->name;
											$this->values_files[$input[name]][thumbs][$k][$k0][path] = $file_pushed->path;
											$this->values_files[$input[name]][thumbs][$k][$k0][storage] = $file_pushed->storage;
										
											// Delete original
											debug("deleting original file: ".$v0[path].$v0[name],$this->c[debug]);
											$file = file::load($v0[path].$v0[name]);
											$file->delete(array('debug' => $this->c[debug]));
										}
										else {
											$this->values_files[$input[name]][thumbs][$k][$k0][storage] = "local";
										}
									}
								}
								// Default - single thumb
								else {
									// Push
									debug("Pushing ".$v[path].$v[name]." (local) to ".$v[path]." (".$storage.")",$this->c[debug]);
									$file = file::load(NULL,array('storage' => $storage,'debug' => $this->c[debug]));
									if($file_pushed = $file->push($v[path].$v[name],$v[path],array('values' => $values))) {
										// Update values
										$this->values_files[$input[name]][thumbs][$k][name] = $file_pushed->name;
										$this->values_files[$input[name]][thumbs][$k][path] = $file_pushed->path;
										$this->values_files[$input[name]][thumbs][$k][storage] = $file_pushed->storage;
										
										// Delete original
										debug("deleting original file: ".$v[path].$v[name],$this->c[debug]);
										$file = file::load($v[path].$v[name]);
										$file->delete(array('debug' => $this->c[debug]));
									}
									else {
										$this->values_files[$input[name]][thumbs][$k][storage] = "local";
									}
								}
							}
						}
					}
				}
				// Basic
				else {
					// Save
					$temp_file = (is_array($value) ? $value[tmp_name] : $value);
					move_uploaded_file($temp_file,$input[file][path]);
					$value = $value[name];
					debug("moved uploaded file from ".$value[tmp_name]." to ".$input[file][path],$this->c[debug]);
					
					// Store
					$this->values_files[$input[name]]['new'] = 1;
					$this->values_files[$input[name]][name] = $value;
					$this->values_files[$input[name]][path] = $input[file][path];
					$this->values_files[$input[name]][type] = ($input[file][type] ? $input[file][type] : "file");
				}
			
				// Delete
				if($input[file][old] and $input[file][old] != $value) $delete = 1;
			}
			// No file uploaded, did we delete the old one?
			else if($input[file][old]) {
				// Deleted?
				$deleted = form::value($this->post,'__file_deleted['.$input[name].']');
				// Yes
				if($deleted) {
					// Empty value
					$value = NULL;
					// Delete
					$delete = 1;
				}
				// No, use old value
				else $value = $input[file][old];	
			}
			// Nothing uploaded, no old value, no value
			else $value = NULL;
			
			// Delete old file / thumbs
			if($delete) {
				$this->file_delete($input,$c);
			}
			
			// Return
			return $value;
		}
		
		/**
		 * Validates the given input and the value submitted. Saves any validation error to $this->errors.
		 *
		 * To do
		 * - creditcard
		 *
		 * @param array $input The array of the input you want to validate.
		 * @param mixed $value The value submitted.
		 * @return boolean Whether or not the input is valid.
		 */
		function input_validate($input,$value = NULL) {
			// New method
			if(is_object($input)) {
				$input->validate($value);
			}
			// Old method
			else {
				// Already an error
				if($this->errors[header][$input[name]]) return;
				
				// Field name
				$field_name = ($input[label][value] ? " in the ".$input[label][value]." field" : "");
				
				// Error
				$error = NULL;
				
				// Required
				if($input[validate][required]) {
					if(!x($value)) {
						$error = "This field is required";
						if($field_name) $error_header = "The ".$input[label][value]." field is required.";
						else $error_header = "A required field is missing.";
					}
				}
				if(!$error and x($value)) {
					// Min
					if(!$error and $input[validate][min] and is_numeric($input[validate][min]) and is_numeric($value)) {
						if($value < $input[validate][min]) {
							$error = "Please enter a value greater than or equal to ".$input[validate][min];
						}
					}
					// Max
					if(!$error and $input[validate][max] and is_numeric($input[validate][max]) and is_numeric($value)) {
						if($value > $input[validate][max]) {
							$error = "Please enter a value less than or equal to ".$input[validate][max];
						}
					}
					// Minlength
					if(!$error and $input[validate][minlength] and is_numeric($input[validate][minlength])) {
						if(is_array($value)) $length = count($value);
						else $length = strlen($value);
						if($length < $input[validate][minlength]) {
							if(is_array($value)) $error = "Please select at least ".$input[validate][minlength]." options";
							else $error = "Please enter at least ".$input[validate][minlength]." characters";
						}
					}
					// Maxlength
					if(!$error and $input[validate][maxlength] and is_numeric($input[validate][maxlength])) {
						if(is_array($value)) $length = count($value);
						else $length = strlen($value);
						if($length > $input[validate][maxlength]) {
							if(is_array($value)) $error = "Please select no more than ".$input[validate][maxlength]." options";
							else $error = "Please enter no more than ".$input[validate][maxlength]." characters";
						}
					}
					// Minwords
					if(!$error and $input[validate][minwords] and is_numeric($input[validate][minwords])) {
						$value_text = preg_replace('/<.[^<>]*?>/s',' ',$value);
						$value_text = preg_replace('/&nbsp;|&#160;/si',' ',$value_text);
						$value_text = preg_replace('/[.(),;:!?%#$\'"_+=\/\-]*/s','',$value_text);
						
						$words = preg_match_all('/\b\w+\b/s',$value_text,$matches);
						
						if($words < $input[validate][minwords]) {
							$error = "Please enter at least ".$input[validate][minwords]." words";
						}
					}
					// Maxwords
					if(!$error and $input[validate][maxwords] and is_numeric($input[validate][maxwords])) {
						$value_text = preg_replace('/<.[^<>]*?>/s',' ',$value);
						$value_text = preg_replace('/&nbsp;|&#160;/si',' ',$value_text);
						$value_text = preg_replace('/[.(),;:!?%#$\'"_+=\/\-]*/s','',$value_text);
						
						$words = preg_match_all('/\b\w+\b/s',$value_text,$matches);
						
						if($words > $input[validate][maxwords]) {
							$error = "Please enter no more than ".$input[validate][maxwords]." words";
						}
					}
					// Accept
					if(!$error and $input[validate][accept]) {
						if(is_array($input[validate][accept])) $input[validate][accept] = implode(',',$input[validate][accept]);	
						$input[validate][accept] = str_replace(' ','',$input[validate][accept]); // Remove spaces
						$input[validate][accept] = str_replace(',','|',$input[validate][accept]);
						if(!preg_match('/.('.$input[validate][accept].')$/i',$value)) {
							$error = "Please enter a value with a valid extension";
						}
					}
					// Digits - must do before 'number'
					if(!$error and $input[validate][digits]) {
						if(!preg_match('/^\d+$/',$value)) {
							$error = "Please enter only digits";
						}
					}
					// Number
					if(!$error and $input[validate][number]) {
						if(!is_numeric($value)) {
							$error = "Please enter a number";
						}
					}
					// E-mail
					if(!$error and $input[validate][email]) {
						// jQuery Validate version
						//$regex = "/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$/i";
						
						// jQuery Validate PHP Plugin version
						$regex = '/^[a-z0-9!#$%&*+=?^_`{|}~-]+(\.[a-z0-9!#$%&*+-=?^_`{|}~]+)*@([-a-z0-9]+\.)+([a-z]{2,3}|info|arpa|aero|coop|name|museum)$/i';
						
						$valid = 1;
						// Multiple
						if($input[multiple]) {
							$emails = explode(',',$value);
							foreach($emails as $email) {
								$email = trim($email);
								if($email) {	
									if(!preg_match($regex,$email)) {
										$valid = 0;
										break;
									}
								}
							}
						}
						// Single
						else if(!preg_match($regex,$value)) $valid = 0;
						
						// Invalid
						if(!$valid) {
							$error = "Please enter a valid e-mail";
						}
					}
					// URL
					if(!$error and $input[validate][url]) {
						// jQuery Validate version
						//$regex = "/^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=\{\}]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=\{\}]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=\{\}]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=\{\}]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=\{\}]|:|@)|\/|\?)*)?$/i";
						// jQuery Validate PHP Plugin version
						$regex = '{
	  \\b
	  # Match the leading part (proto://hostname, or just hostname)
	  (
		# http://, or https:// leading part
		(https?)://[-\\w]+(\\.\\w[-\\w]*)+
	  |
		# or, try to find a hostname with more specific sub-expression
		(?i: [a-z0-9] (?:[-a-z0-9]*[a-z0-9])? \\. )+ # sub domains
		# Now ending .com, etc. For these, require lowercase
		(?-i: com\\b
			| edu\\b
			| biz\\b
			| gov\\b
			| in(?:t|fo)\\b # .int or .info
			| mil\\b
			| net\\b
			| org\\b
			| [a-z][a-z]\\.[a-z][a-z]\\b # two-letter country code
		)
	  )
	  # Allow an optional port number
	  ( : \\d+ )?
	  # The rest of the URL is optional, and begins with /
	  (
		/
		# The rest are heuristics for what seems to work well
		[^.!,?;"\'<>()\[\]\{\}\s\x7F-\\xFF]*
		(
		  [.!,?]+ [^.!,?;"\'<>()\\[\\]\{\\}\s\\x7F-\\xFF]+
		)*
	  )?
	}ix';
						if(!preg_match($regex,$value)) {
							$error = "Please enter a valid URL";
						}
					}
					// Credit Card
					if(!$error and $input[validate][creditcard]) {
						// Valid to start
						$valid = 1;
						
						// Accept only spaces, digits and dashes
						if(preg_match('/[^0-9 -]+/',$value)) $valid = 0;
						else {
							// Variables
							$nCheck = 0;
							$nDigit = 0;
							$bEven = false;
							
							// Remove spaces/dashes
							$value_simple = preg_replace('/\D/','',$value);
			
							// Loop through numbers (backwards)
							for($x = strlen($value_simple) - 1;$x >= 0;$x--) {
								$cDigit = $value_simple[$x];
								$nDigit = intval($cDigit,10);
								if($bEven) {
									if(($nDigit *= 2) > 9) $nDigit -= 9;
								}
								$nCheck += $nDigit;
								$bEven = !$bEven;
							}
			
							// Value of 10?
							if(($nCheck % 10) != 0) $valid = 0;
						}
						if(!$valid) {
							$error = "Please enter a valid credit card number";
						}
					}
					// Regex
					if(!$error and $input[validate][regex]) {
						$regex = $input[validate][regex];
						if(substr($regex,0,1) != "/") $regex = "/".$regex."/";
						if(!preg_match($regex,$value)) {
							$error = "This format is not valid";
							if($field_name) $error_header = "The ".$input[label][value]." format is not valid.";
							else $error_header = "A format is not valid.";	
						}
					}
					// Equal to
					if(!$error and $input[validate][equalto]) {
						$input2 = $this->input_exists($input[validate][equalto]);
						if($input2) {
							// Process
							$value2 = $this->input_process($input2);
							
							// Purify
							if($input2[purify]) $value2 = $this->value_purify($value2,(is_array($input2[purify]) ? $input2[purify] : NULL));
							
							// Validate // Won't alter $value2
							/*if($this->c[validate_php]) {
								if($input2[validate][equalto] != $input[name]) $this->input_validate($input2,$value2);
							}*/
							
							// Different?
							debug("equal to. value original: ".$value2.", value match: ".$value,$this->c[debug]);
							if($value !== $value2) {
								if($input2[label][value]) $error = "This does not match ".$input2[label][value];
								else $error = "These don't match.";
								if($input2[label][value] and $input[label][value]) $error_header = "The ".$input2[label][value]." and ".$input[label][value]." values don't match.";
								else $error_header = "Required values don't match.";	
							}
						}
					}
				}
				
				// Error
				if($error) {
					if(!$error_inline) $error_inline = $error.".";
					if(!$error_header) $error_header = $error.$field_name.".";
					$this->errors[header][$input[name]] = $error_header;
					$this->errors[inline][$input[name]] = $error_inline;
					return false;
				}
				// Success
				else return true;
			}
		}
		
		/**
		 * Returns an array of the form's inputs or (if 1st $inputs param passed) adds the given input arrays to the form's inputs.
		 *
		 * @param array $inputs An array of inputs to add to the form. Example: array('type' => 'text','name' => 'First Name','value' => 'John Doe'). Default = NULL
		 * @return array An array of inputs which have been added to the form so far.
		 */
		function inputs($inputs = NULL) {
			if($inputs and is_array($inputs)) {
				foreach($inputs as $input) {
					// New method
					if(is_object($input)) {
						$this->input($input);
					}
					// Old method
					else {
						$_input = $input;
						unset($_input[label],$_input[type],$_input[name],$_input[value]);
						$this->input($input[label],$input[type],$input[name],$input[value],$_input);
					}
				}
			}
			
			return $this->inputs;
		}
		
		/**
		 * Renders the HTML of the form's inputs and returns the HTML.
		 *
		 * @return array The HTML of the rendered inputs.
		 */
		function inputs_render() {
			foreach($this->inputs as $x => $input) {
				// Next - inline?
				if($input->type != "html") {
					$next = $this->inputs[($x + 1)];
					$next_inline = $this->c[inline];
					if(is_object($next)) {
						if(x($next->c[inline])) $next_inline = $next->c[inline];
					}
					else {
						if(x($next[inline])) $next_inline = $next[inline];
					}
				}
				
				// New method
				if(is_object($input)) {
					// Inline
					if($input->c[inline] or $next_inline) $input->row[attributes]['class'] .= ($input->row[attributes]['class'] ? " " : "")."form-row-inline";
					
					// Render
					$html .= $this->input_render($input);
			
					if(!$next_inline and $input->c[inline] and $html) $html .= "
		<div class='clear'></div>";
				}		
				// Old method
				else {
					// Inline
					if($input[inline] or $next_inline) $input[row]['class'] .= ($input[row]['class'] ? " " : "")."form-row-inline";
					
					// Render
					$html .= $this->input_render($input);
			
					if(!$next_inline and $input[inline] and $html) $html .= "
		<div class='clear'></div>";
				}
			}
			
			return $html;
		}
		
		/**
		 * Returns a string of attributes for an element.
		 *
		 * @param array $input The array of data for the element (input, row, table, etc.).
		 * @param array $c An array of config values. Default = NULL
		 * @return string A string of HTML attributes for the element.
		 */
		function attributes($input,$c = NULL) {
			//debug("input: ".return_array($input));
			// Preserve - these attributes can only have one value so don't append with global
			$preserve = array('type','name','value','id','default','automcomplete','placeholder'); // List is probably much larger, but start with this for now
			
			// Input\
			$attributes = $this->attributes_array($input);
		
			// Global
			//debug("type: ".$input[type]);
			if($input[type] and is_array($this->c[$input[type]])) $attributes_global = $this->c[$input[type]];
			//debug("c: ".return_array($this->c));
			//debug("global: ".return_array($attributes_global));
			
			// Merge
			if($attributes) {
				foreach($attributes as $k => $v) {
					if($attributes_global[$k]) {
						if(!in_array($k,$preserve)) $attributes[$k] .= " ".$attributes_global[$k];
						unset($attributes_global[$k]);
					}
				}
			}
			if($attributes and $attributes_global) $attributes = array_merge($attributes,$attributes_global);
			else if($attributes_global) $attributes = $attributes_global;
			
			// String
			$string = attributes_string($attributes);
			/*$string = NULL;
			foreach($attributes as $k => $v) {
				if(x($v)) {
					$quote = (preg_match('/[^\\\]"/',$v) ? "'" : '"');
					$string .= ($string ? " " : "").$k."=".$quote.$v.$quote;
				}
			}*/
			
			// Return
			return $string;
		}
		
		/**
		 * Returns an array of attributes for given element array.
		 *
		 * @param array $input The array of data for the element (input, row, table, etc.).
		 * @return array An array of the key/value pairs of the element's attributes.
		 */
		function attributes_array($input) {
			// Attirbutes - just in case we still have them in the 'attributes' key of the input array, not within the input array itself
			$attributes = $input[attributes];
			
			// Element
			$element = $this->input_element($input[type]);
			
			// Name
			if($input[name]) $attributes[name] = $input[name];
			// Input specific attributes
			if($element == "input") {
				if($input[type]) $attributes[type] = $input[type];
				if(/*!in_array($input[type],array('checkbox','radio')) and */x($input[value])) $attributes[value] = $input[value];
			}
			
			// Other attributes
			$nonattributes = $this->nonattributes();
			foreach($input as $k  => $v) {
				if(!is_array($v) and !in_array($k,$nonattributes)) {
					$attributes[$k] = $v;
				}
			}
			
			// Validate
			$class_quote = (preg_match('/[^\\\]"/',$attributes['class']) ? "'" : '"');
			if($input[validate][required] and !strstr($attributes['class'],'required')) $attributes['class'] .= ($attributes['class'] ? " " : "")."required required-input required-icon";
			if(x($input[validate][min]) and !x($attributes[min])) $attributes[min] = $input[validate][min];
			if(x($input[validate][max]) and !x($attributes[max])) $attributes[max] = $input[validate][max];
			if(x($input[validate][minlength]) and !x($attributes[minlength])) $attributes[minlength] = $input[validate][minlength];
			if(x($input[validate][maxlength]) and !x($attributes[maxlength])) $attributes[maxlength] = $input[validate][maxlength];
			if(x($input[validate][minwords]) and !x($attributes[minwords])) $attributes[minwords] = $input[validate][minwords];
			if(x($input[validate][maxwords]) and !x($attributes[maxwords])) $attributes[maxwords] = $input[validate][maxwords];
			if($input[validate][accept] and !$attributes[extension]) $attributes[extension] = $input[validate][accept]; // Used to be 'accept' attribute, now 'extension'
			if($input[validate][digits] and !strstr($attributes['class'],'digits')) $attributes['class'] .= ($attributes['class'] ? " " : "")."digits";
			if($input[validate][number] and !strstr($attributes['class'],'number')) $attributes['class'] .= ($attributes['class'] ? " " : "")."number";
			if($input[validate][email] and !strstr($attributes['class'],'email')) $attributes['class'] .= ($attributes['class'] ? " " : "")."email";
			if($input[validate][url] and !strstr($attributes['class'],'url')) $attributes['class'] .= ($attributes['class'] ? " " : "")."url";
			if($input[validate][creditcard] and !strstr($attributes['class'],'creditcard')) $attributes['class'] .= ($attributes['class'] ? " " : "")."creditcard";
			if($input[validate][regex] and !$attributes[regex]) $attributes[regex] = $input[validate][regex];
			if($input[validate][equalto] and !strstr($attributes['class'],'equalto')) {
				$attributes['class'] .= ($attributes['class'] ? " " : "")."{equalTo:".$class_quote.":input[name=".$input[validate][equalto]."]".$class_quote."}";
				$attributes[equalTo] = "input[name=".$input[validate][equalto]."]";
			}
			
			// Classes
			if($input[disabled]) $attributes['class'] .= ($attributes['class'] ? " " : "")."form-input-disabled";
			if($input[readonly]) $attributes['class'] .= ($attributes['class'] ? " " : "")."form-input-readonly";
			
			// Return
			return $attributes;
		}
		
		/**
		 * Returns an array of non-attributes.
		 *
		 * Non-attriubutes are usually configuration vales (for an input, field, row, or entire form) which aren't key/value pair attributes (or one's we'll manually set).
		 *
		 * @return array An array of non-attributes.
		 */
		function nonattributes() {
			// Array
			$array = array(
				'validate',
				'validate_php',
				'validate_javascript',
				'saved',
				'errors_inline',
				'errors_header',
				'process',
				'type_process',
				'purify',
				'serialize',
				'url',
				'debug',
				'container',
				'field',
				'form',
				'row',
				'inputs',
				'inline',
				'type',
				'name',
				'value',
				'label',
				'default',
				'options',
				'options_blank',
				'separator',
				'temp',
				'required',
				'prepend',
				'append',
				'help',
				//'format', // Now stored in the 'date' array
				'minutes',
				'file',
				'date',
				'toolbar'
			);
			
			// Return
			return $array;
		}
		
		/**
		 * Returns the HTML of the form
		 */
		function render() {
			// Speed
			$f_r = function_speed(__CLASS__."->".__FUNCTION__);
			
			// Inputs
			if($this->inputs) {
				// Standardize
				foreach($this->inputs as $k => $input) {
					$this->inputs[$k] = $this->input_standardize($input);
				}
				
				// Form attributes
				$form = $this->c;
				if($attributes = $form[attributes]) { // Really it should be passed this way, but legacy passed within $c, not $c[attributes]
					unset($form[attributes]);
					$form = array_merge($form,$attributes);
				}
				$form[enctype] = "multipart/form-data";
				$form['class'] .= ($form['class'] ? " " : "")."form";
				if($this->c[validate_javascript]) {
					$form['class'] .= ($form['class'] ? " " : "")."require";
				}
				// Always include so if we attach validation to form on our own , it's still here for use
				$html .= "
".include_css('jquery.validation',0)."
".include_javascript('jquery.validation',0);
				$form[novalidate] = "novalidate";
				$form_attributes = $this->attributes($form);
				$this->c[container]['class'] .= ($this->c[container]['class'] ? " " : "")."form-container";
				$container_attributes = $this->attributes($this->c[container]);
				
				// Form hash
				$this->hash = $this->hash();
				
				// Saved form
				if($this->c[saved]) {
					if($_SESSION['__form'] == $this->hash) {
						$saved = $this->cache_get($_SESSION['__form']); // Cache, passed via SESSION
						$_SESSION['__form'] = NULL; // Remove from session
					}
					if($saved) {
						// Values
						$this->values = $saved->values;
						foreach($this->values as $k => $v) {
							$v_unserialized = data_unserialize($v);
							if(is_array($v_unserialized)) $this->values[$k] = $v_unserialized;
						}
						// Errors
						$this->errors = $saved->errors;
						// Redirect
						$this->c[redirect] = $saved->c[redirect];
					}
				}
			
				// Debug
				//print_array($this->inputs);
				
				// Container
				$html .= "
<div".($container_attributes ? " ".$container_attributes : "").">";

				// Errors
				if($this->errors[header] and $this->c[errors_header]) {
					$html .= "
	<div class='form-errors'>
		".implode('<br />',$this->errors[header])."
	</div>";
				}
				
				// Form
				$html .= "
	<form ".$form_attributes.">
		".$this->inputs_render();
			
				// Cache
				if($this->c[process]) {
					$this->cache_save(); 
					$html .= $this->input_html(array('type' => 'hidden','name' => '__form','value' => $this->hash));
				}
				
				// End form
				$html .= "
	</form>";
				
				// End container
				$html .= "
	<div class='form-clear form-container-clear'></div>
</div>";
			}
			
			// Speed
			function_speed(__CLASS__."->".__FUNCTION__,$f_r);
			
			// Return
			return $html;
		}
		
		/**
		 * Processes the submitted form.
		 *
		 * @param array $post The array of submitted form data.
		 * @param array $files The array of submitted file data. Default = NULL
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of the resulting values.
		 */
		function process($post,$files = NULL,$c = NULL) {
			// Speed
			$f_r = function_speed(__CLASS__."->".__FUNCTION__);
			
			// Config
			if(!x($c[delete])) $c[delete] = 1; // Delete form cache (including submitted values) after processing is complete (unless an error occured).
			
			// Form
			if(!$this->hash and $post[__form]) $this->cache_restore($post[__form]);
			unset($post[__form]);
			if($this->hash) {
				//debug("got saved form. ".return_array($this),$c[debug]);
				
				// Config
				if($c) $this->c = array_merge($this->c,$c);
				
				// Post
				$this->post = $post;
				// Files
				$this->files = $files;
				
				// Reset errors
				$this->errors = NULL;
				
				// Debug
				debug("\$".__CLASS__."->".__FUNCTION__."() form id: ".$this->hash,$this->c[debug]);
				debug("\$".__CLASS__."->".__FUNCTION__."() post: ".return_array($this->post),$this->c[debug]);
				debug("\$".__CLASS__."->".__FUNCTION__."() files: ".return_array($this->files),$this->c[debug]);
				//debug("\$".__CLASS__."->".__FUNCTION__."() form: ".return_array($this),$this->c[debug]);
				
				// Values
				$this->values = array();
				foreach($this->inputs as $x => $input) {
					// New method
					if(is_object($input)) {
						// Process?
						if($input->name and $input->process/* and !$input->attributes[disabled]*/) {
							// Process
							$value = $input->process($this,array('debug' => $this->c[debug]));
					
							// Debug
							debug("\$".__CLASS__."->".__FUNCTION__."() value (before). name: ".$input->name.", value: ".$value,$this->c[debug]);
							
							// Purify
							if($input->purify) $value = $this->value_purify($value,(is_array($input->purify) ? $input->purify : NULL));
							
							// Validate
							if($this->c[validate_php]) $input->validate($this,$value);
							
							// NULL
							if(!x($value)) $value = "NULL";
							
							// Save
							$this->values = form::value($this->values,$input->name,$value);
							
							// Debug
							debug("\$".__CLASS__."->".__FUNCTION__."() values (after). name: ".$input->name.", value: ".$value,$this->c[debug]);
						}
					}
					// Old method
					else {
						// Standardize
						$input = $this->inputs[$x] = $this->input_standardize($input);
						
						// Process?
						if($input[name] and $input[process]/* and !$input[disabled]*/) {
							// Process
							$value = $this->input_process($input);
					
							// Debug
							debug("\$".__CLASS__."->".__FUNCTION__."() value (before). name: ".$input[name].", value: ".$value,$this->c[debug]);
							
							// Purify
							if($input[purify]) $value = $this->value_purify($value,(is_array($input[purify]) ? $input[purify] : NULL));
							
							// Validate
							if($this->c[validate_php]) $this->input_validate($input,$value);
							
							// NULL
							if(!x($value)) $value = "NULL";
							
							// Save
							$this->values = form::value($this->values,$input[name],$value);
							
							// Debug
							debug("\$".__CLASS__."->".__FUNCTION__."() values (after). name: ".$input[name].", value: ".$value,$this->c[debug]);
						}
					}
				}
				
				// Values - arrays
				if($this->c['serialize'] and $this->values) {
					//debug("\$".__CLASS__."->".__FUNCTION__."() values (before serialization): ".return_array($this->values),$this->c[debug]);
					foreach($this->values as $k => $v) {
						if($v and is_array($v)) $this->values[$k] = data_serialize($v);	
					}
					//debug("\$".__CLASS__."->".__FUNCTION__."() values (after serialization): ".return_array($this->values),$this->c[debug]);
				}
				
				// Results
				$results = array(
					'form' => $this->hash,
					'url' => $this->c[url],
					'redirect' => $this->c[redirect],
					'values' => $this->values,
					'values_files' => $this->values_files,
					'post' => $this->post,
					'files' => $this->files,
					'errors' => $this->errors,
					'c' => $this->c
				);
				
				// Debug
				debug("\$".__CLASS__."->".__FUNCTION__."() results: ".return_array($results),$this->c[debug]);
				
				// Cache
				if($results[errors]) $this->cache_save(1);
				else if($c[delete]) $this->cache_delete();
			}
			
			// Speed
			function_speed(__CLASS__."->".__FUNCTION__,$f_r);
			
			// Return
			return $results;
		}

		/**
		 * Returns value for given key while taking multi-level array keys into account.
		 * 
		 * @param array $array The array you want to get the value from.
		 * @param string $name The name of the key we want to get (or set) the value of. Example: "first_name" or "site[meta][title]". Default = NULL
		 * @param mixed $value If we pass a $value, we'll update the value of the given array's $name key and return it rather than returning the got value. Default = NULL
		 * @return mixed The value retrieved from the array or (if $value is passed) the updated array.
		 */
		function value($array,$name = NULL,$value = NULL) {
			// Variable
			$variable = '$array';
			
			// Keys
			if(substr($name,-2) == "[]") $name = substr($name,0,strlen($name) - 2);
			$keys = explode('[',str_replace(']','',$name));
			$count = count($keys);
			
			// Set Value
			if(x($value)) {
				$value_passed = 1;
				if($value == "NULL") $value = NULL;
				foreach($keys as $var) $string .= "['".$var."']";
				$eval = '$array'.$string.' = $value;';
			}
			
			// Get Value
			else {
				// Build string (with if/else so we don't get a fatal error)
				if($keys) {
					foreach($keys as $x => $var) {
						$string .= "['".$var."']";
						if($x < ($count - 1)) {
							// Value
							$if .= 'if(is_array('.$variable.$string.')) {';
							$endif .= "}";
						}
					}
				}
				
				// Build eval
				$eval = $if.'$value = $array'.$string.';'.$endif;
			}
			
			// Debug
			//debug($eval);
			
			// Evaluate
			eval($eval);
			
			// Return
			if($value_passed) return $array;
			else return $value;
		}
		
		/**
		 * Purifies the submitted value of an input.
		 *
		 * @param mixed $value The value you want to purify.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return mixed The purified value.
		 */
		function value_purify($value,$c = NULL) {
			// Debug
			debug("Value before purification: ".(is_array($value) ? return_array($value) : "<xmp style='display:inline;'>".$value."</xmp>"),$this->c[debug]);
			
			// Purify
			$value = purify($value,$c);
			
			// Debug
			debug("Value after purification: ".(is_array($value) ? return_array($value) : "<xmp style='display:inline;'>".$value."</xmp>"),$this->c[debug]);
			
			// Return
			return $value;
		}

		/**
		 * Returns file input value for given key while tacking multi-level array keys into account.
		 * 
		 * @param array $array The array you want to get the value from.
		 * @param string $name The name of the key we want to get (or set) the value of. Example: "first_name" or "site[meta][title]". Default = NULL
		 * @return mixed The value retrieved from the array or (if $value is passed) the updated array.
		 */
		function value_file($array,$name = NULL) {
			// Keys
			$keys = explode('[',str_replace(']','',$name));
			$count = count($keys);
			
			// Not an array, can use form::value();
			if($count <= 1) return form::value($array,$name);
			// Is an array, have to add in file parts (name, tmp_name, etc.)
			else {
				// First key
				$first = $keys[0];
				unset($keys[0]);
				
				// Last key(s)
				$last = implode('[',$keys);
				
				// Parts
				$parts = array('name','type','tmp_name','error','size');
				
				// File
				$value = NULL;
				foreach($parts as $part) {
					$value[$part] = form::value($array,$first.'['.$part.'['.$last);
				}
				
				// Return
				return $value;
			}
		}
		
		/**
		 * Catches non-existant methods and assumes they're input types. Loads an input of that type into the form.
		 *
		 * Example:
		 * $form->text('Label','name','saved value',$c);
		 * ...becomes...
		 * $form->input(form_input_text::load('Label','name','saved value',$c));
		 *
		 * Note, you should really be using the new format instead of the old.
		 *
		 * @param string $method The method that was called.
		 * @param array $parameters An array of parameters passed to the method.
		 * @return mixed The value returned by the called $method.
		 */
		function __call($method,$parameters) {
			// Class
			$class = 'form_input_'.$method;
			// Class - name changed
			if($method == "ckeditor") $class = 'form_input_textarea_'.$method;
			if($method == "swfupload") $class = 'form_input_file_'.$method;
			
			// Exists?
			if(!class_exists($class)) {
				// No, try framework class
				$class .= "_framework";
				// That doesn't exist either, we don't have a class to handle this input type yet
				if(!class_exists($class)) $class = NULL;
			}
			// Yes, call
			if($class) {
				$input = call_user_func(array($class,'load'),$parameters[0],$parameters[1],$parameters[2],$parameters[3],$parameters[4]);
				return $this->input($input);
			}
		}
		
		/**
		 * Creates and returns the HTML for previewing a previously uploaded file.
		 *
		 * @param array $input The input's array of information including file path and value.
		 * @param array $c An array of config values. Default = NULL
		 * @return string The HTML of the file preview.
		 */
		/*function file_preview($input,$c = NULL) {
			// Value
			if($input[value] and $input[file][path]) {
				// Speed
				$f_r = function_speed(__CLASS__."->".__FUNCTION__);
				
				// Config
				if(!x($c[delete])) $c[delete] = 1; // Show delete icon for removing the file
				
				// Random
				$r = "preview".random();
			
				// File
				$file = $input[file][path].$input[value];
			
				// File class
				if(class_exists('file')) {
					$f = new file($file);
					$type = $f->type;
				}
	
				// Container
				$text .= "
<div id='".$r."' class='form-preview".($type ? " form-preview-".$type : "")."'>";

				// Text
				$text .= "
	<div class='form-preview-keep'>Leave this field blank to keep the existing file.</div>
	<div class='clear'></div>";
	
				// Display
				$text .= "
	<div class='form-preview-media'>";
			
				// Display - advanced
				if($f) {
					// Image
					if($type == "image") $text .= "
		".$f->image_html(array('max_width' => 100,'max_height' => 100,'attributes' => " target='_blank'",'link' => $f->url(),'link_attributes' => " class='overlay'"));
					// Video
					else if($type == "video") {
						$video_c = array(
							'max_width' => 265,
							'auto' => 0,
						);
						// Extensions
						if($input[file][extensions]) {
							foreach($input[file][extensions] as $extension => $v) {
								$f_extension = file::load($v[path].str_replace(".".$f->extension(),".".$extension,$input[value]));
								if($f_extension->exists) {
									// Same as original, don't want 2, don't add as fallback
									if($f_extension->extension == $f->extension) {
										$f = $f_extension; // Use converted video if one exists as it'll have all necessary tweaks/headers
									}
									// Different than original, add as fallback
									else {
										$video_c[fallback][] = $f_extension->file;	
									}
								}
							}
						}
						// HTML
						$text .= "
		".$f->video_html($video_c);
					}
					// Audio
					else if($type == "audio") $text .= "
		".$f->audio_html();
					// File
					else $text .= "
		<a href='".$f->url()."' target='_blank'>".basename($file)."</a>";
				}
				// Display - simple
				else {
					// URL
					$url = str_replace(SERVER,DOMAIN,$file);
					// File
					if($url) $text .= "
		<a href='".$url."' target='_blank'>".basename($file)."</a>";
					else $text .= basename($file);
				}
				
				$text .= "
	</div>";
	
				// Icons
				$text .= "
	<div class='form-preview-icons'>";
	
				// Delete
				if($c[delete]) {
					// Deleted input name
					$name = $input[name];
					$name = preg_replace('/\[/','][',$name,1); // Add first closing bracket bracket. Example: plugins[menu][item_image] becomes plugins][menu][item_image]
					$name = "__file_deleted[".$name;
					if(substr($name,-1) != "]") $name .= "]";
					
					// Required element selector
					if($input[id])  $selector = "#".$input[id].".required-input";
					else $selector = ".required-input:input[name=".$input[name]."]"; // !!! This needs to be updated to work with a multilevel input names
					
					$text .= "
		<script type='text/javascript'>
		function deleteFile".$r."() {
			if(confirm('Are you sure you want to delete this file?')) {
				$('#".$r."').html(\"<input type='hidden' name='".$name."' value='1' />\");
				$('".$selector."').addClass('required'); // Re-require
			}
		}
		</script>
		<a href='javascript:void(0);' onclick='deleteFile".$r."();' class='i i-clear i-inline tooltip core-hover' title='Delete'></a>";
				}
				
				$text .= "
	</div>
	<div class='clear'></div>
</div>";

				// Speed
				function_speed(__CLASS__."->".__FUNCTION__,$f_r);
				
				// Return
				return $text;
			}
		}
		
		/**
		 * Deletes previous file after new one is uploaded.
		 *
		 * @param array $input The input's array of information including file path and old value.
		 * @param array $c An array of config values. Default = NULL
		 */
		/*function file_delete($input,$c = NULL) {
			// Config
			if(!x($c[debug])) $c[debug] = 0; // Debug
			
			// Advanced
			if(class_exists('file')) {
				// File
				$f = file::load($input[file][path].$input[file][old],array('storage' => $input[file][storage]));
				if($f->file) {
					$f_name = $f->name(0);
					
					// Delete - extensions
					if($input[file][extensions]) {
						foreach($input[file][extensions] as $extension => $v) {
							$extension_file = $v[path].$f_name.".".$extension;
							$file = file::load($extension_file,array('storage' => $input[file][storage]));
							$file->delete();
							debug("deleted old extension file (".$extension."): ".$extension_file,$this->c[debug]);
						}
					}
					
					// Delete - thumbs
					if($input[file][thumbs]) {
						foreach($input[file][thumbs] as $thumb => $v) {
							for($x = 1;$x <= $v[count];$x++) {
								$thumb_file = $v[path].$f_name.($v[count] > 1 ? "_".$x : "").".".$v[extension];
								$file = file::load($thumb_file,array('storage' => $input[file][storage]));
								$file->delete();
								debug("deleted old thumb file (".$thumb.($v[count] > 1 ? "-".$x : "")."): ".$thumb_file,$this->c[debug]);
							}
						}
					}
					
					// Delete - file
					debug("deleting old file: ".$f->file,$this->c[debug]);
					$f->delete();
				}
			}
			// Basic
			else {
				if(is_file($input[file][path].$input[file][old])) {
					unlink($input[file][path].$input[file][old]);
				}
			}
		}
		
		/**
		 * Returns a hash string unique to this form and user, but equal to other instances of this same form (for use when restoring saved values/errors).
		 *
		 * @return string The unique hash string for this form.
		 */
		function hash() {
			// User IP and Browser
			$string = $_SERVER['REMOTE_ADDR'].$_SERVER['HTTP_USER_AGENT'];
			// Config
			foreach($this->c as $k => $v) {
				if(!in_array($k,array('id','inputs','debug','validate','validate_javascript','validate_php'))) {
					if(is_array($v)) serialize($v);
					else $string .= $v;
				}
			}
			// Inputs
			if($this->inputs) {
				foreach($this->inputs as $input) {
					foreach($input as $k => $v) {
						if(in_array($k,array('label','name'))) {
							if(is_array($v)) serialize($v);
							else $string .= $v;
						}
					}
				}
			}
			
			// MD5
			$hash = md5($string);
			
			// Return
			return $hash;
		}
		
		/**
		 * Caches an instance of this form.
		 *
		 * @param boolean $session Do you want to store this form in the $_SESSION['__form'] variable which is used when restoring submitted values after invalid redirect back to form. You should pass 1 if you're caching after processing the values.
		 * @return string The id of the cache.
		 */
		function cache_save($session = 0) {
			// Hash
			$this->hash();
			
			// Save
			//$_SESSION['forms'][$id] = serialize($this); // Session
			cache_save("forms/".$this->hash,$this,array('force' => 1)); // Cache
			
			// Session
			if($session) $_SESSION['__form'] = $this->hash;
		}
		
		/**
		 * Returns a cached instance of the given form id.
		 *
		 * @param string $id The id of the cached form. Defaults to current instance's form id.
		 * @return object The cached instance of the form.
		 */
		function cache_get($id = NULL) {
			// ID
			if(!$id) $id = $this->hash();
			if($id) {
				// Get
				//$form = unserialize($_SESSION['forms'][$id]); // Session
				$form = cache_get("forms/".$id,array('life' => 0,'force' => 1)); // Cache
			}
			// Return
			return $form;
		}
		
		/**
		 * Restores the given cache, meaning it merges the cached object instance ($cache) with the current one.
		 *
		 * @param string|object $form The name of the cached form object or the cached object that you want to merge with the current one.
		 */
		function cache_restore($form) {
			// Error
			if(!$form) return;
			
			// Passed string (cache name), get form object
			if(!is_object($form)) {
				$form = $this->cache_get($form);
			}
			
			// Have object
			if(is_object($form)) {
				// Restore
				foreach($form as $k => $v) {
					if($k == "c") $v = array_merge($this->$k,$v);
					$this->$k = $v;
				}
			}
		}
		
		/**
		 * Deletes cached form (and removes the $_SESSION variable).
		 */
		function cache_delete($id = NULL) {
			if(!$id) $id = $this->hash();
			if($id) cache_delete("forms/".$id);
			$_SESSION['__form'] = NULL;
		}
	}
}

/*if(!class_exists('form_ckeditor',false)) {
	/**
	 * Creates input for editing HTML via CKEditor
	 *
	 * @package kraken\forms
	 */
	/*class form_ckeditor extends form {
		/**
		 * Adds all necessary information for a CKEditor= input to the form instance.
		 *
		 * @param string $label The label of the input. Default = NULL
		 * @param string $name The name of the input field (the key in the $_POST/$_GET results). Default = NULL
		 * @param string $value The current value of this input. Default = NULL
		 * @param array $c An array of config values. Default = NULL
		 */
		/*function input($label = NULL,$type = 'ckeditor',$name = NULL,$value = NULL,$c = NULL) {
			// Config
			if(!x($c[purify])) $c[purify] = 0;
			
			// Array
			$array = $c;
			$array[label] = $label;
			$array[type] = $type;
			$array[name] = $name;
			$array[value] = $value;
			
			// Return
			return $array;
			/*if(!$c[toolbar]) $c[toolbar] = "Medium"; // What predefined toolbar do you want to use: Full, Medium [default], Basic
			if(!$c[height]) $c[height] = 250; // Height (in pixels) of texteditor.
			if(!$c[id]) $c[id] = "ckeditor_".random(); // ID of the textarea
			
			// CKEditor javascript
			$javascript = "
<script type='text/javascript'>
$(document).ready(function() {
	if(CKEDITOR) {
		CKEDITOR.replace(
			'".$c[id]."',
			{
				toolbar : '".$c[toolbar]."',
				height : '".$c[height]."'
			}
		);
	}
});
</script>";
			$c[form]->html($javascript);
			
			// Textarea
			$c[form]->textarea($label,$name,$value,$c);*/
		//}
		
		/**
		 * Standardizes an input and it's configuration values.
		 *
		 * @param array $input The input array we want to standardize.
		 * @param array $c An array of configuration values. Only really presents so we can pass $c[form]. Default = NULL
		 * @return array The standardized input array.
		 */
		/*function input_standardize($input,$c = NULL) {
			// Config
			if(!x($input[purify])) $input[purify] = 0; // Want purification off by default.
			if(!$input[toolbar]) $input[toolbar] = "Medium"; // What predefined toolbar do you want to use: Full, Medium [default], Basic
			if(!$input[height]) $input[height] = 250; // Height (in pixels) of texteditor.
			
			// Return
			return $input;
		}
		
		/**
		 * Creates the HTML for an input
		 *
		 * @param array $input The array of data for the input
		 * @param array $c An array of config values. Default = NULL
		 * @return string The HTML element for the input.
		 */
		/*function input_html($input,$c = NULL) {
			// Random
			$r = random();
			
			// Config
			if(!$input[id]) $input[id] = "ckeditor_".$r; // ID of the textarea
			
			// Javascript
			/*$html = "
<script type='text/javascript' language='javascript' src='".D."core/core/libraries/ckeditor/ckeditor.js'></script>";*/
			/*$html = "
".include_javascript('ckeditor',0); // Use this method instead (of above) so we can avoid including it more than once  // Scratch that, Include more than once (2nd param == 0) as input might get tested in $module->settings() method without the HTML being displayed
			$html .= "
<script type='text/javascript'>
$(document).ready(function() {
	var settings = {
		toolbar : '".$input[toolbar]."',
		height : '".$input[height]."'
	};
	framework_ckeditor_load_".$r."('".$input[id]."',settings);
});
function framework_ckeditor_load_".$r."(id,settings,tries) {
	if(CKEDITOR) {
		// Base
		CKEDITOR.basePath = '".D."core/core/libraries/ckeditor/';
		
		// Visible
		var visible = $('#'+id).is(':visible');
		var fancybox_visible = 0;
		var fancybox = $('#'+id).parents('.fancybox-wrap');
		if(!fancybox.length) fancybox_visible = 1;
		else if($(fancybox).hasClass('fancybox-opened')) fancybox_visible = 1;
		
		debug('trying. id: '+id+', visible: '+visible+', fancybox visible: '+fancybox_visible+', tries: '+tries);
		if(visible && fancybox_visible) {
			CKEDITOR.replace(id,settings);
		}
		else {
			if(tries) tries += 1;
			else tries = 1;
			if(tries < 20) {
				debug('setting up timeout. tries: '+tries);
				
				setTimeout(function() {
					debug('trying again');
					framework_ckeditor_load_".$r."(id,settings,tries);
				},250);
			}
		}
	}
}
</script>";
			// Textarea
			$input[type] = "textarea";
			$html .= "
".$c[form]->input_html($input);

			// Return
			return $html;
		}
		
		/**
		 * Helper for adding a CKEitor input to the form.
		 *
		 * Same as $form->input($label,'ckeditor',$name,$value,$c);
		 *
		 * !!! Useless as this isn't a part of the original form instance we would be calling the method on.
		 *
		 * @param string $label The label of the input
		 * @param string $name The name of the input field (the key in the $_POST/$_GET results)
		 * @param string $value The current value of this input
		 * @param array $c An array of config values. Default = NULL
		 */
		/*function ckeditor($label,$name,$value,$c = NULL) {
			$this->input($label,'ckeditor',$name,$value,$c);
		}*/
	/*}
}*/

/*if(!class_exists('form_captcha',false)) {
	/**
	 * Creates input for displaying a catpcha image for human verification.
	 *
	 * @package kraken\forms
	 */
	/*class form_captcha extends form {
		/**
		 * Standardizes an input and it's configuration values.
		 *
		 * @param array $input The input array we want to standardize.
		 * @param array $c An array of configuration values. Only really presents so we can pass $c[form]. Default = NULL
		 * @return array The standardized input array.
		 */
		/*function input_standardize($input,$c = NULL) {
			// Config
			if(!x($input[refresh])) $input[refresh] = 1; // Include a 'refresh' link for changing captcha.
			
			// Return
			return $input;
		}
		
		/**
		 * Creates the HTML for an input
		 *
		 * @param array $input The array of data for the input
		 * @param array $c An array of config values. Default = NULL
		 * @return string The HTML element for the input.
		 */
		/*function input_html($input,$c = NULL) {
			// Config
			if(!$input[id]) $input[id] = "captcha"; // ID of the input (needed when we process it too)
			
			// No value
			$input[value] = NULL;
			
			// Image
			$html = "
<div class='form-captcha-container'>
	<div class='form-captcha-image'>
		<img src='".D."core/core/libraries/captcha/captcha.php' id='".$input[id]."-captcha' />
	</div>
	<span class='form-captcha-help'>Please enter the text you see above.</span>";
	
			// Refresh
			if($input[refresh]) $html .= "
	<span class='form-captcha-refresh'><a href=\"javascript:document.getElementById('".$input[id]."-captcha').src='".D."core/core/libraries/captcha/captcha.php?'+Math.random(); document.getElementById('".$input[id]."').focus();\" style='text-decoration:underline;'>Change text</a></span>";
	
			// Text
			$input[type] = "text";
			$html .= "
	<div class='form-captcha-input'>".$c[form]->input_html($input)."</div>
</div>";

			// Return
			return $html;
		}
		
		/**
		 * Processes the value for the given input.
		 *
		 * @param array $input The array of the input you want to process.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return mixed The resulting value.
		 */
		/*function input_process($input,$c = NULL) {
			// Config
			if(!$input[id]) $input[id] = "captcha"; // ID of the input (needed when we process it too)
			
			// Value
			$value = form::value($c[form]->post,$input[name]);
			
			// Valid
			debug("input: ".$input[id].", value: ".$value.", session: ".$_SESSION[$input[id]],$c[debug]);
			if(empty($_SESSION[$input[id]]) or trim(strtolower($value)) != $_SESSION[$input[id]]) {
				$c[form]->errors[header][$input[name]] = "The captcha you entered".($input[label][value] ? " for the ".$input[label][value] : "")." was incorrect.";
				$c[form]->errors[inline][$input[name]] = "The captcha you entered was incorrect.";
				$value = "NULL";
			}
			
			// Return
			return $value;
		}	
		
		/**
		 * Helper for adding a captcha input to the form.
		 *
		 * Same as $form->input($label,'captcha',$name,$value,$c);
		 *
		 * !!! Useless as this isn't a part of the original form instance we would be calling the method on.
		 *
		 * @param string $label The label of the input
		 * @param string $name The name of the input field (the key in the $_POST/$_GET results)
		 * @param string $value The current value of this input
		 * @param array $c An array of config values. Default = NULL
		 */
		/*function captcha($label,$name,$value,$c = NULL) {
			$this->input($label,'captcha',$name,$value,$c);
		}*/
	/*}
}

/*if(!class_exists('form_swfupload',false)) {
	/**
	 * Creates input for uplading files via swfupload.
	 *
	 * @package kraken\forms
	 */
	/*class form_swfupload extends form {
		/**
		 * Adds all necessary information for adding a SWFUpload file input to the form instance.
		 *
		 * @param string $label The label of the input. Default = NULL
		 * @param string $name The name of the input field (the key in the $_POST/$_GET results). Default = NULL
		 * @param string $value The current value of this input. Default = NULL
		 * @param array $c An array of config values. Default = NULL
		 */
		/*function input($label = NULL,$type = 'swfupload',$name = NULL,$value = NULL,$c = NULL) {
			// Config
			if(!$c[size]) $c[size] = 23; // Size attribute of file input
			if(!x($c[file][preview])) $c[file][preview] = 1; // Whether or not we want to show preview of existing file on file inputs.
			
			// Variables
			$c[label] = $label;
			$c[name] = $name;
			$c[value] = $value;
			$c['class'] .= ($c['class'] ? " " : "")."swfupload-placeholder";
			$c[file][old] = $value;
			$c[type_process] = "file";
			$r = random();
			$html = NULL;
			
			// Framework variables (bit messy to include them here, but no real way to get them in $form_framework->input() without defining 'swfupload' as a 'file' input that needs these defauts.
			if($c[form]->module) {
				// File Type
				if(!$c[file][type]) $c[file][type] = "file";
				// Path
				if(!$c[file][path]) $c[file][path] = m($c[form]->module.'.uploads.types.'.$c[file][type].'.path');
				// Thumbs
				if(!$c[file][thumbs]) $c[file][thumbs] = m($c[form]->module.'.uploads.types.'.$c[file][type].'.thumbs');
				// Extensions
				if(!$c[file][extensions]) $c[file][extensions] = m($c[form]->module.'.uploads.types.'.$c[file][type].'.extensions');
				// Accept
				if(!$c[accept]) $c[accept] = implode(",",g('config.uploads.types.'.$c[file][type].'.accept'));
			}
				
			// JS/CSS - Include both of these more than once (2nd param == 0) as input might get tested in $module->settings() method without the HTML being displayed
			$html .= include_css('swfupload',0); 
			$html .= include_javascript('swfupload',0);
			
			// Button
			/*$html .= "
		<div class=\"swfupload {'upload_url':'".D."?ajax_action=uploadIt&flash=1&module=".$module."&id=".$id."&type=".$type."&field_id=".$c[field_id]."&field=".$f."&preview=".$c[file_ajax]."','file_post_name':'".$name."','limit':'1','instant_upload':'".$c[file_ajax]."'".($extensions ? ",'file_types':'*.".str_replace(',',";*.",str_replace('.','',m('settings','uploads.types.'.$c[input].'.accept')))."','error':'You may only upload files in ".$extensions." format'" : "")."}\">";*/
			/*$html .= "
<div class=\"swfupload {upload_url:'".D."?ajax_action=uploadIt&flash=1&name=".$name."&path=".$c[file][path]."',file_post_name:'swfupload_".$r."',limit:1,instant_upload:0}\">";
				
			// Input - will be hidden via javascript, but serves as the actual file input we use when swfupload not enabled
			$_c = $c;
			$_c[type] = "file";
			$_c[required] = 0; // Don't require
			$_c[help] = NULL; // No 'help'
			$_c[file][preview] = 0;
			$html .= $c[form]->input_html($_c);
			
			// Preview
			if($value and $c[file][path]) {
				$html .= "
	<div class='clear' style='height:4px;'></div>
	<div class='swfupload-info'>";
				if($c[file][preview] == 1) $html .= "
		<div class='swfupload-previous'>
			".$c[form]->file_preview($c)."
		</div>";
				$html .= "
	</div>";
			}
			
			$html .= "
</div>";

			// SWFUpload button
			if(!$label) $label = "&nbsp;"; // Since we're adding HTML we need a label otherwise it won't put it in a proper row.
			$c[label] = $label;
			$c[form]->html($html,$c);
		}
		
		/**
		 * Standardizes an input and it's configuration values.
		 *
		 * @param array $input The input array we want to standardize.
		 * @param array $c An array of configuration values. Only really presents so we can pass $c[form]. Default = NULL
		 * @return array The standardized input array.
		 */
		/*function input_standardize($input,$c = NULL) {
			// Config
			if(!$input[size]) $input[size] = 23; // Size attribute of file input
			if(!x($input[file][preview])) $input[file][preview] = 1; // Whether or not we want to show preview of existing file on file inputs.
			
			// Variables
			$input['class'] .= ($input['class'] ? " " : "")."swfupload-placeholder";
			$input[file][old] = $input[value];
			$input[type_process] = "file";
			
			// Framework variables (bit messy to include them here, but no real way to get them in $form_framework->input() without defining 'swfupload' as a 'file' input that needs these defauts.
			if($c[form]->module) {
				// File Type
				if(!$input[file][type]) $input[file][type] = "file";
				// Path
				if(!$input[file][path]) $input[file][path] = m($c[form]->module.'.uploads.types.'.$input[file][type].'.path');
				// Thumbs
				if(!x($input[file][thumbs])) $input[file][thumbs] = m($c[form]->module.'.uploads.types.'.$input[file][type].'.thumbs');
				else if(is_array($input[file][thumbs])) $input[file][thumbs] = array_merge_associative(m($c[form]->module.'.uploads.types.'.$input[file][type].'.thumbs'),$input[file][thumbs]);
				// Extensions
				if(!x($input[file][extensions])) $input[file][extensions] = m($c[form]->module.'.uploads.types.'.$input[file][type].'.extensions');
				// Accept
				if(!x($input[validate][accept])) $input[validate][accept] = implode(",",g('config.uploads.types.'.$input[file][type].'.accept'));
				// Browse
				if(!x($input[file][browse][active])) $input[file][browse][active] = (u('admin') ? 1 : 0);
				if(!class_exists('file')) $input[file][browse][active] = 0;
				if($input[file][storage] and $input[file][storage] != "local") $input[file][browse][active] = 0;
				if($input[file][browse][active] == 1) {
					if(!$input[file][browse][path]) $input[file][browse][path] = $input[file][path];
					if(!$input[file][browse][type]) $input[file][browse][type] = $input[file][type];
					if(!$input[row][id]) $input[row][id] = "file-row-".random();
					if(!$input[file][browse][row]) $input[file][browse][row] = "#".$input[row][id];
				}
			}
			
			// Return
			return $input;
		}
		
		/**
		 * Creates the HTML for an input
		 *
		 * @param array $input The array of data for the input
		 * @param array $c An array of config values. Default = NULL
		 * @return string The HTML element for the input.
		 */
		/*function input_html($input,$c = NULL) {
			// Variables
			$r = random();
			
			// JS/CSS - Include both of these more than once (2nd param == 0) as input might get tested in $module->settings() method function without the HTML being displayed
			$html .= include_css('swfupload',0); 
			$html .= include_javascript('swfupload',0);
			
			// Prepend
			if($input[prepend]) $html .= $input[prepend];
			
			// Button
			/*$html .= "
		<div class=\"swfupload {'upload_url':'".D."?ajax_action=uploadIt&flash=1&module=".$module."&id=".$id."&type=".$type."&field_id=".$input[field_id]."&field=".$f."&preview=".$input[file_ajax]."','file_post_name':'".$input[name]."','limit':'1','instant_upload':'".$input[file_ajax]."'".($extensions ? ",'file_types':'*.".str_replace(',',";*.",str_replace('.','',m('settings','uploads.types.'.$input[input].'.accept')))."','error':'You may only upload files in ".$extensions." format'" : "")."}\">";*/
			/*$settings = array(
				'upload_url' => D."?ajax_action=uploadIt&flash=1&name=".$input[name]."&path=".$input[file][path],
				'file_post_name' => 'swfupload_'.$r,
				'limit' => 1,
				'instant_upload' => 0,
				'test' => 1,
			);
			if($input[validate][accept]) $settings[file_types] = "*.".str_replace(',',";*.",str_replace('.','',$input[validate][accept]));
			$html .= "
<div class='swfupload ".json_encode($settings)."'>";

				
			// Browse
			$browse = $c[form]->file_browse($input);
			if($browse) $html .= $browse;
				
			// Help
			if($input[help]) $html .= help($input[help]);
				
			// Append
			if($input[append]) $html .= $input[append];
				
			// Input - will be hidden via javascript, but serves as the actual file input we use when swfupload not enabled
			$_input = $input;
			$_input[type] = "file";
			$_input[required] = 0; // Don't require
			$_input[prepend] = NULL; // No 'prepend', already included
			$_input[help] = NULL; // No 'help', already included
			$_input[append] = NULL; // No 'append', already included
			$_input[file][preview] = 0; // We'll handle preview here
			$_input[file][browse][active] = 0; // We'll handle 'browse' here
			$html .= $c[form]->input_html($_input);
			
			
			// Browse / preview
			if($input[value] and $input[file][path] and $input[file][preview] == 1) {
				$html .= "
	<div class='clear' style='height:4px;'></div>
	<div class='swfupload-info'>";
				// Browse
				if($browse) $html .= "
		<div class='form-browse-preview'>";	
				// Preview
				$html .= "
			<div class='swfupload-previous'>
				".$c[form]->file_preview($input)."
			</div>";
			
				// End browse
				if($browse) $html .= "
		</div>";	
				$html .= "
	</div>";
			}
			else $html .= "
	<div class='form-browse-preview'></div>";
			
			$html .= "
</div>";
			
			// Return
			return $html;
		}
		
		/**
		 * Helper for adding a SWFUpload file input to the form.
		 *
		 * Same as $form->input($label,'swfupload',$name,$value,$c);
		 *
		 * !!! Useless as this isn't a part of the original form instance we would be calling the method on.
		 *
		 * @param string $label The label of the input
		 * @param string $name The name of the input field (the key in the $_POST/$_GET results)
		 * @param string $value The current value of this input, meaning the name of the file previously uploaded.
		 * @param array $c An array of config values. Default = NULL
		 */
		/*function swfupload($label,$name,$value,$c = NULL) {
			$this->input($label,'swfupload',$name,$value,$c);
		}*/
	/*}
}*/

/*if(!class_exists('form_color',false)) {
	/**
	 * Creates input for adding a color picker input.
	 *
	 * @package kraken\forms
	 */
	/*class form_color extends form {
		/**
		 * Creates the HTML for an input
		 *
		 * @param array $input The array of data for the input
		 * @param array $c An array of config values. Default = NULL
		 * @return string The HTML element for the input.
		 */
		/*function input_html($input,$c = NULL) {
			// Class
			$input['class'] .= ($input['class'] ? " " : "")."colorpicker";
			
			// Javascript / CSS - Include more than once (2nd param = 0) in case input type got tested in $module->settings() but not included in HTML
			$html .= "
".include_javascript('colorpicker',0)."
".include_css('colorpicker',0);

			// Input
			$input[type] = "text";
			if($input[value]) $input[value] = "#".$input[value];
			$html .= "
".$c[form]->input_html($input);

			// Return
			return $html;
		}
			
		/**
		 * Processes the value for the given input.
		 *
		 * @param array $input The array of the input you want to process.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return mixed The resulting value.
		 */
		/*function input_process($input,$c = NULL) {	
			// Value
			$value = form::value($c[form]->post,$input[name]);
			
			// Remove #
			$value = str_replace('#','',$value);
			
			// Return
			return $value;
		}
		
		/**
		 * Helper for adding a color picker input to the form.
		 *
		 * Same as $form->input($label,'color',$name,$value,$c);
		 *
		 * !!! Useless as this isn't a part of the original form instance we would be calling the method on.
		 *
		 * @param string $label The label of the input
		 * @param string $name The name of the input field (the key in the $_POST/$_GET results)
		 * @param string $value The current value of this input
		 * @param array $c An array of config values. Default = NULL
		 */
		/*function color($label,$name,$value,$c = NULL) {
			$this->input($label,'color',$name,$value,$c);
		}*/
	/*}
}*/

?>