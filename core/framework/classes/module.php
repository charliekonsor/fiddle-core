<?php
if(!class_exists('module',false)) {
	/**
	 * A class for easily accessing and manipulating a module.
	 * 
	 * Note, it just calls the item class without an $id.
	 *
	 * Dependencies
	 * - functions
	 *   - m
	 * - constants
	 *   - SERVER
	 *
	 * @package kraken
	 */
	class module extends core_framework {
		// Variables
		public $module; // string
		//public $type; // string // Can't define this yet otherwise calling $this->type, before we got row, will return blank
		
		/**
		 * Loads and returns an instance of either the core module class or (if it exists) the module specific module class.
		 *
		 * Example:
		 * - $module = module::load($module,$c);
		 *
		 * @param string $module The module we want to access.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return object An instance of either the core module class or (if it exists) the module specific module class.
		 */
		static function load($module = NULL,$c = NULL) {
			$params = func_get_args(); // Must be outside function call
			$class_load = load_class_module(__CLASS__,$params,$module,$c[type]); // No way to instanitiate class with array of params so we'll just return the class name and...
			return new $class_load($module,$c); // ...manually pass the params here
		}
		
		/**
		 * Constructs the class
		 *
		 * @param string $module The module this item is in.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function __construct($module = NULL,$c = NULL) {
			self::module($module,$c);
		}
		function module($module = NULL,$c = NULL) {
			// Config
			if(!x($c[cached])) $c[cached] = 1; // Get cached data (if available)
			if(!x($c[cache])) $c[cache] = 1; // Cache data for future use
			
			// Parent
			parent::__construct($c);
			
			// Module
			$this->module = $module;
			
			// Type
			if($c[type]) $this->type = $c[type];
			
			// Config
			//if($c) $this->c = array_merge($this->c,$c); // Don't need now that we use a 'core' class
			
			// Database
			//$this->db = new db(); // Don't need now that we use a 'core' class
		}
		
		/**
		 * Helper function so you can easily get a module's config/setting value. Example: $this->plural will call $this->v('plural').
		 *
		 * Deprecated, must use $module->v() from now on.
		 *
		 * @param string $key The key of the module value you want to get.
		 * @return mixed The key's corresponding value within this module.
		 */
		/*function __get($key) {
			// Error
			if(!$key) return;
			
			// Get value
			$value = $this->v($key);
			
			// Set globally (3x quicker 2nd time around)
			$this->$key = $value;
				
			// Return value
			return $value;
		}
		
		/**
		 * Returns a module's config/setting value. Example: $this->v('plural') will return $this->v('plural').
		 *
		 * @param string $key The key of the module value you want to get.
		 * @param string $type The type you want to get the value of. Defaul = $this->type
		 * @return mixed The key's corresponding value within this module.
		 */
		function v($key,$type = NULL) {
			// Error
			if(!$key) return;
			
			// Type
			if(!$type) $type = $this->type;
			
			// Value
			$value = m($this->module.'.'.$key,$type);
				
			// Return value
			return $value;
		}
		
		/**
		 * Determines the URL for viewing the module in the given area.
		 *
		 * @param string $view The view within the module you want to get the URL of. Default = home
		 * @param string $area The area of the site where this URL will link to: public, admin, account. Default = public
		 * @param array $c An array of configuration values. Default = NULL
		 * @return string The URL of the module's home page in the given area (if it exists).
		 */
		function url($view = NULL,$area = NULL,$c = NULL) {
			// Config
			if(!$c[parent_module]) $c[parent_module] = m($this->module.'.parent'); // Parent module
			if(!$c[parent_id]) $c[parent_id] = NULL; // Parent id
			if(!x($c[https])) $c[https] = 0; // Make domain HTTPS
			if(!x($c[debug])) $c[debug] = 0; // Debug
			
			// Variables
			$url = NULL;
			if(!$area) $area = "public";
			if(!$view) {
				// Type
				if($this->type and $views = $this->v($area.'.views')) {
					foreach($views as $k => $v) {
						if($v[item] and $v[item_module] == "types") {
							$view = $k;
							break;
						}
					}
				}
				// Default
				if(!$view) $view = "home";
			}
			debug("module: ".$this->module.", type: ".$this->type.", area: ".$area.", view: ".$view,$c[debug]);
			
			// Get saved value
			if($value = $this->cached[urls][$area.".".$view]){
				$url = $value;	
			}
			// Get new value
			else if($view_array = $this->v($area.'.views.'.$view)) {
				// View uses a URL?
				if($format = $view_array[urls][0][format]) {
					// Debug
					debug("format: ".$format,$c[debug]);
					
					// Build
					$ex = explode('/',$format);
					foreach($ex as $e) {
						$e = trim($e);
						if(x($e)) {
							$value = NULL;
							if(substr($e,0,1) == "{" and substr($e,-1) == "}") {
								$e = str_replace(array('{','}'),'',$e);
								if($e == "home") $value = "";
								else if($e == "module") $value = $this->module;
								else if($e == "code" and $this->type) $value = $this->type;
								else if($e == "parent_module") $value = $c[parent_module];
								else if($e == "parent_id") $value = $c[parent_id];
								else if($e == "parent_parent_module" and $c[parent_module]) {
									if($parent = m($c[parent_module].'.parent')) $value = $parent;
									else if($c[parent_id] > 0) {
										$parent_item = item::load($c[parent_module],$c[parent_id]);
										$value = $parent_item->db('parent_module');
									}
								}
							}
							else $value = $e;
							if(!x($value)) {
								$url = 0;
								break;	
							}
							$url .= (!$url ? "" : "/").$value;
						}
					}
			
					if($url) {
						// Domain
						$url = domain_page().$url;
				
						// Save
						$this->cached[urls][$area.".".$view] = $url;
					}
				}
				else {
					debug("No url defined for `".$view."` view in the `".$area."` area for this ".($this->type ? "type" : "module"),$c[debug]);
				}
			}
			else {
				debug("No `".$view."` view defined in the `".$area."` area for this ".($this->type ? "type" : "module"),$c[debug]);
			}
						
			// HTTPS
			if($c[https] == 1) $url = str_replace(DOMAIN,SDOMAIN,$url);
			
			// Debug
			debug("URL: ".$url,$c[debug]);
			
			// Return
			return $url;
		}
		
		/**
		 * Queries rows within this module using the given configuration values.
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 * @return resource The database query resource.
		 */
		function rows($c = NULL) {
			//print "rows() called, module = ".$this->module.", c: ".return_array($c)."<br />";
			// Error
			if(!$this->module) return;
			
			if($this->v('db.table')) {
				// Config
				if(!$c[select]) $c[select] = "i.*"; // What to 'SELECT'. Default = i.* (all in module's table)
				if(!$c[where]) $c[where] = NULL; // The 'WHERE' clause of the query.
				if(!$c[order]) $c[order] = NULL; // The 'ORDER BY' clause of the query.
				if(!$c[limit]) $c[limit] = NULL; // The 'LIMIT' clause of the query (note, you could add it to $c[order] too if you wanted).
				if(!$c[db]) $c[db] = array(); // An array of 'global databse variables' we want to match.
				if(!$c['db.code']) $c['db.code'] = NULL; // An example of passing a global database variable we want to match.
				if(!$c['db.type']) $c['db.type'] = $this->type; // Another example of passing a global database variable we wantt o match. This one defaults to the class variable $this->type.
				if(!$c['return']) $c['return'] = "resource"; // What to return: resource [default], query
				if(!x($c[debug])) $c[debug] = 0; // Debug
				
				// Type
				$type = $c['db.type'];
				if(!$type) $type = $c[db][type];
				
				// Global db variables
				foreach($c as $k => $v) {
					// Array of global database variables. Example: $c[db] = array('code' => 'test','order_parent' => 123);
					if($k == "db" and is_array($v)) {
						foreach($v as $k0 => $v0) {
							if($key = $this->v('db.'.$k0) and x($v0)) $c[where] .= "
					".($c[where] ? "AND " : "")."i.".$key." = '".a($v0)."'";	
						}
					}
					// Single global database variable. Example: $c['db.order_parent'] = 123
					else if(strstr($k,'db.')) {
						if($key = $this->v($k) and x($v)) $c[where] .= "
					".($c[where] ? "AND " : "")."i.".$key." = '".a($v)."'";	
					}
				}
				
				// Plugins
				$method = __CLASS__."_".__FUNCTION__;
				if($plugins = $this->v('plugins')) {
					foreach($plugins as $plugin => $plugin_active) {
						if($plugin_active) {
							$plugin_instance = $this->plugin($plugin,$c);
							if(method_exists($plugin_instance,$method)) {
								debug("Rows query in plugin ".$plugin,$c[debug]);
								$c = $plugin_instance->$method($c);
							}
						}
					}
				}
				
				// Don't mix COUNT(*) and GROUP BY 
				if($c[group] and strstr($c[select],"COUNT(*) c")) {
					$c[select] = str_replace("COUNT(*) c","i.".m($this->module.'.db.id')." c_rows",$c[select]);
				}
				
				// Select
				$query = "
				SELECT
					".$c[select];
					
				// Tables
				$query .= "
				FROM
					`".$this->v('db.table')."` i";
				// Tables - join - note: must pass full statement, including the 'JOIN' or 'LEFT JOIN' or whatever
				if($c[join]) $query .= "
					".$c[join];
				
				// Where
				if($c[where]) $query .= "
				WHERE
					".$c[where];
				
				// Group
				if($c[group]) $query .= "
				GROUP BY
					".$c[group];
				
				// Having
				if($c[having]) $query .= "
				HAVING
					".$c[having];
				
				// Order
				if($c[order]) $query .= "
				ORDER BY
					".$c[order];
					
				// Limit
				if($c[limit]) $query .= "
				LIMIT
					".$c[limit];
					
				// Clean up query
				$query = str_replace(array("\r","\n","\r\n","\t",'  ','WHERE ORDER','WHERE AND'),array(' ',' ',' ','',' ','ORDER','WHERE'),$query);
				if(substr($query,-5) == "WHERE") $query = substr($query,0,strlen($query) - 6);
				
				// Debug
				debug($query,$c[debug]);
				
				// Return
				if($c['return'] == "query") return $query;
				else return $this->db->q($query);
			}
		}
		
		/**
		 * Similar to the rows() function, but only returns the number of rows from the module that match the given parameters.
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 * @return int The number of rows in the database that match the given parameters.
		 */
		function rows_count($c = NULL) {
			// Error
			if(!$this->module) return;
			
			// Don't want these
			unset($c[order],$c[limit]);
			
			// Select
			$c[select] = "COUNT(*) c";
			
			// Query
			$rows = $this->rows($c);
			$row = $this->db->f($rows);
			
			// Rows
			if($row[c_rows]) $rows = $this->db->n($rows); // Had to count items individually because we were using a GROUP BY
			else $rows = $row[c];
			if(!$rows) $rows = 0;
			
			// Return
			return $rows;
		}
		
		/**
		 * Similar to the rows() function, but only returns the first result and returns array instead of a resource.
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array The array of data for the first result returned in the query.
		 */
		function row($c = NULL) {
			// Config
			if(!$c[limit]) $c[limit] = 1;
			// Query
			$rows = $this->rows($c);
			// Return
			return $this->db->f($rows);
		}

		/**
		 * Returns information about the form field associated with a column in this module.
		 *
		 * @param string $column The column we're trying to match to a form field.
		 * @return array An array of information about the matched form field.
		 */
		function column_form($column) {	
			// Error
			if(!$this->module) return;
		
			// Not found yet
			$found = 0;
			
			// Loop through forms
			foreach($this->v('forms',$this->type) as $form_k => $form_v) {
				// Loop through form inputs
				foreach($form_v[inputs] as $k => $v) {
					// Input matches
					if($v[name] == $column) {
						$field = $v;
						$found = 1;
						break;
					}
				}
				if($found) break;
			}
			
			return $field;
		}
	
		/**
		 * Method for eaily creating an item instance within the module class. Note, preloads $this->module so you only have to pass the item $id.
		 * 
		 * @param int|array $id Either the id or the database array of the item you want to create an item instance of.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return object The item class instance for the given item.
		 */
		function item($id,$c = NULL) {
			// Error
			if(!$this->module) return;
			
			// Config
			if(!x($c[cache])) $c[cache] = $this->c[cache];
			if(!x($c[cached])) $c[cached] = $this->c[cached];
			if(!$c[type]) $c[type] = $this->type;
			
			// Form
			return item::load($this->module,$id,$c);
		}
		
		/**
		 * Loads an instance of the given plugin for the module.
		 * 
		 * @param string $plugin The plugin you want to load.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return object The instance of the plugin class.
		 */
		function plugin($plugin,$c = NULL) {
			// Error
			if(!$plugin or !$this->module) return;
			
			// Saved? // Won't we want unique instances always, especially with the file plugin? // Reset the $this->file in $plugin_files->file() so may be able to turne caching back on.
			if($instance = $this->cache[plugins][$plugin]) return $instance;
			
			// Active
			if($this->v('plugins.'.$plugin)) {
				// Get
				$plugin_instance = plugin::load($this->module,NULL,$plugin,$c);
				
				// Save // Won't we want unique instances always, especially with the file plugin? // Reset the $this->file in $plugin_files->file() so may be able to turne caching back on.
				$this->cache[plugins][$plugin] = $plugin_instance;
				
				// Return
				return $plugin_instance;
			}
		}
	
		/**
		 * Method for eaily creating a form instance within the module class. Note, preloads $this->module.
		 * 
		 * @param array $c An array of configuration values. Default = NULL
		 * @return object The instance of the form class.
		 */
		function form($c = NULL) {
			// Error
			if(!$this->module) return;
			
			// Config
			if(!x($c[type])) $c[type] = $this->type;
			
			// Form
			return form_framework::load($this->module,NULL,$c); // form_framework extends form
			//return item_form::load($this->module,NULL,$c); // item_form extends item
		}
		
		/**
		 * Creates and returns the HTML of an icon for performing some action on this module.
		 *
		 * This method basically lets us check permission and data within this module before we send it to the $core->icon() method.
		 * See $core->icon() for more configuration values.
		 *
		 * @param string $action The action of the icon you're creating.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return string The HTML of the icon.
		 */
		function icon($action,$c = NULL) {
			// Error
			if(!$this->module) return;
			
			// Config
			if(!$c[icon]) $c[icon] = $action; // The CSS 'i-' class you want to apply to generate the icon. Example: 'add' would result in 'i-add'.
			if(!$c[text] and $c[button]) $c[text] = ($c[tooltip_text] ? $c[tooltip_text] : ucwords($action)); // Text to display alongside the icon (required if a 'button')
			if(!$c[tooltip_text]) $c[tooltip_text] = ($c[text] ? $c[text] : ucwords($action)); // The tooltooltip text to show on hover (if $c[tooltip] == 1).
			if(!x($c[permission])) $c[permission] = 1; // Do you want to check whether or not the user has permission to perform this action on this item?
			if(!x($c[permission_action])) $c[permission_action] = $action; // What permission action are we checking against?
			if(!x($c[area])) $c[area] = NULL; // What 'area' are we linking icons to (ex: edit). Defaults to current area (if admin/account), else admin (if user is admin), else account.
			
			// Permission?
			$permission = 0;
			if(!$c[permission]) $permission = 1;
			else if($this->permission($c[permission_action])) $permission = 1;
			if($permission) {
				// URL
				if(!x($c[url])) {
					// Area
					if(!$c[area]) {
						$page = g('page');
						if(in_array($page->area,array('admin','account'))) $c[area] = $page->area;
						else if(u('admin')) $c[area] = 'admin';
						else $c[area] = 'account';
					}
				
					// Add
					if($action == "add") $c[url] = domain_page().$c[area]."/".$this->module."/new".($this->type ? "?type=".$this->type : "");
					// Default
					else $c[url] = $this->url($c[area])."/".$action;
				}
			}
			else $c[url] = 0;
			
			// Return
			if($c[url] or $c[required]) {
				return parent::icon($c);
			}
		}
	
		/**
		 * Returns the documentation HTML for this module.
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 * @return string The documentation HTML.
		 */
		function documentation($c = NULL) {
			// File
			$file = $this->documentation_file($c);
			if($file) {
				// HTML
				$html = $this->html_get($file,array('module' => $this->module,'id' => $this->id));
				
				// Table of contents
				$html = $this->documentation_tableofcontents($html);
				
				// Return
				return $html;
			}
		}
	
		/**
		 * Returns the path to the documentation file for this module (if one exists).
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 * @return string The path to the documentaiton file.
		 */
		function documentation_file($c = NULL) {
			// File
			$file = NULL;
			if(!$file) {
				$file = SERVER.$this->v('path_local')."documentation/".u('type').".php";
				if(!file_exists($file)) $file = NULL;
			}
			if(!$file) {
				$file = SERVER.$this->v('path')."documentation/".u('type').".php";
				if(!file_exists($file)) $file = NULL;
			}
			
			// HTML - check to make sure it's not empty
			if($file) {
				$html = $this->html_get($file,array('module' => $this->module,'id' => $this->id));
				if(!$html) $file = NULL;
			}
			
			// Return
			return $file;
		}
	
		/**
		 * Determines contents of given documentation and returns that documentation HTML prepended with a table of contents.
		 *
		 * @param string $html The HTML of the documentation (from which we'll determine the table of contents).
		 * @param array $c An array of configuration values. Default = NULL
		 * @return string The HTML of the documentation with a table of contents at the top.
		 */
		function documentation_tableofcontents($html,$c = NULL) {
			// Error
			if(!$html) return;
			
			// Heading 1
			list($html_h1,$html_content) = explode('</h1>',$html,2);
			if($html_content) $html_h1 .= "</h1>";
			else {
				$html_content = $content;
				$content = NULL;
			}
			
			// Array
			$array = $this->documentation_tableofcontents_array($html_content,$c);
			
			// HTML - heading 1
			$html_full .= $html_h1;
			// HTML - table of contents
			$html_tableofcontents = $this->documentation_tableofcontents_html($array['headings'],$c);
			if($html_tableofcontents) {
				$html_full .= "
<div class='documentation-toc'>
	<h2 class='documentation-toc-header'>Table of Contents</h2>
	".$html_tableofcontents."
</div>";
			}
			// HTML - content
			//$html_full .= $this->documentation_tableofcontents_anchors($array[html],$array['headings'],$c);
			$html_full .= $array[html];
			
			// Return
			return $html_full;
		}
	
		/**
		 * Determines contents of documentation and returns array of headings as well as the HTML with anchors added for use in creating a table of contents
		 *
		 * @param string $html The HTML of the documentation (from which we'll determine the table of contents).
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array containing an array of headings as found in the $html and the HTML for this section with the anchors added.
		 */
		function documentation_tableofcontents_array($html,$c = NULL) {
			// Error
			if(!$html) return;
			
			// Config
			if(!$c[heading]) $c[heading] = 2;
			if(!$c[anchor]) $c[anchor] = "documentation-".$c[heading];
			
			// Match
			$regex = '/<h'.$c[heading].'(.*?)>(.*?)<\/h'.$c[heading].'>(.*?)(?=(<h'.$c[heading].'|$))/si';
			preg_match_all($regex,$html,$matches);
			//if($c[heading] == 3) print_array($matches);
			if(count($matches[3]) > 0) {
				foreach($matches[3] as $k => $v) {
					// Children
					$children = $this->documentation_tableofcontents_array($v,array('heading' => ($c[heading] + 1),'anchor' => $c[anchor]."-".$k));
					// Label
					$label = $matches[2][$k];
					$label_escaped = preg_quote($label,'/');
					
					// Array
					$headings[] = array(
						'label' => $label,
						'anchor' => $c[anchor]."-".$k,
						'heading' => $c[heading],
						'children' => $children['headings'],
					);
					
					// HTML
					$html = preg_replace('/<h'.$c[heading].'([^>]*?)>'.$label_escaped.'<\/h'.$c[heading].'>(.*?)(?=(<h'.$c[heading].'|$))/si','<a name="'.$c[anchor].'-'.$k.'"></a><h'.$c[heading].'$1>'.$matches[2][$k].'</h'.$c[heading].'>'.($children[html] ? $children[html] : $v),$html);
				}
			}
			// No heading 1, try heading 2 (this is the only level we'll check at, if no h2, we don't check h3) // Starting at h2
			/*else if($c[heading] == 1) {
				$headings = $this->documentation_tableofcontents($html,array('heading' => ($c[heading] + 1)));
			}*/
			
			// Return
			return array(
				'headings' => $headings,
				'html' => $html,
			);
		}
	
		/**
		 * Builds HTML for the table of contents based upon the given array of headings.
		 *
		 * @param array $array An array of headings determined to be in the content via documentation_tableofcontents_array(),
		 * @param array $c An array of configuration values. Default = NULL
		 * @return string The HTML of the documentation table of contents.
		 */
		function documentation_tableofcontents_html($array,$c = NULL) {
			// Error
			if(!$array) return;
			
			// Headings
			foreach($array as $k => $v) {
				// Heading
				$tableofcontents .= "
	<h".$v[heading]." class='documentation-toc-heading documentation-toc-heading-".$v[heading]."'><a href='#".$v[anchor]."'>".$v[label]."</a></h".$v[heading].">";
				
				// Children
				if($v[children]) $tableofcontents .= $this->documentation_tableofcontents_html($v[children],$c);
			}
			
			// Return
			return $tableofcontents;
		}
	
		/**
		 * Returns the documentation HTML for the plugins (or Sidebar) section of this module's documentation.
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 * @return string The plugins documentation HTML.
		 */
		function documentation_plugins($c = NULL) {
			$form = $this->form();
			$plugins = $form->plugins();
			if($plugins) {
				foreach($plugins as $plugin => $inputs) {
					// Get documentation
					if(!$inputs[documentation]) {
						$plugin_class = $this->plugin($plugin);
						$documentation = $plugin_class->documentation();
						if($documentation) $plugins[$plugin][documentation] = $documentation;
					}
					// No documentation, don't show
					if(!$plugins[$plugin][documentation]) {
						unset($plugins[$plugin]);
					}
				}
			}
			
			if($plugins) {
				// String
				$x = 0;
				$string = NULL;
				foreach($plugins as $plugin => $input) {
					$string .= ($string ? ", " : "").$input[label];
					if($x == 2) {
						$string .= ", etc.";
						break;
					}
					$x++;
				}
				
				// Introduction
				$html .= "
<h3>Sidebar</h3>
In the sidebar of the new ".strtolower($this->v('single'))." form, you'll should also see several collapsed sections (".$string.".). These are we we call 'Plugins'.  A plugin simply provides a way to further customize a ".strtolower($this->v('single')).". So, let's take a look at what each of these plugins do.<br /><br />";
				
				// Plugins
				foreach($plugins as $plugin => $input) {
					$html .= "
<h4>".$input[label]."</h4>
".$input[documentation]."<br /><br />";
				}
				
				// Return
				return $html;
			}
		}
	
		/**
		 * Returns a documentation HTML for a basic explanation of how to manage this module's content. 
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 * @return string The documentation HTML for managing this module.
		 */
		function documentation_manage($c = NULL) {
			// Introduction
			$html .= "
<h2>Managing ".$this->v('plural')."</h2>
After you've added some ".strtolower($this->v('plural')).", you're going to want to be able manage them.  You can do this via the ".strtolower($this->v('name'))." home page which you've probably already seen. But let's take a moment to look a little deeper and see exactly what everything on this page does.<br /><br />";
			
			// Screenshot
			if(file_exists(SERVER.$this->v('path')."documentation/images/home.png")) $html .= "
<em>".$this->v('name')." module home page</em><br />
<img src='".DOMAIN.$this->v('path')."documentation/images/home.png' alt='' width='650' /><br /><br />";
			
			// Filters - search
			if($this->v('admin.filters.search')) $html .= "
<b>Search..</b><br />
The 'Search..' filter at the top of the page, you may have guessed, simply lets you search for a specific ".strtolower($this->v('single')).". Just type the name of the ".strtolower($this->v('single')).", click 'Go', and it'll show you the ".strtolower($this->v('plural'))." that match your search.<br /><br />";
			
			// Icons - edit
			if($this->permission('edit')) $html .= "
<b>Edit</b><br />
When you click on the edit icon <span class='i i-edit i-inline-block'></span> you're able to edit the ".strtolower($this->v('single')).".  The form for this is exactly the same as the 'Add New ".$this->v('single')."' form, except that it already has the saved content in it.<br /><br />";
			
			// Icons - delete
			if($this->permission('delete')) $html .= "
<b>Delete</b><br />
The delete icon <span class='i i-delete i-inline-block'></span>, simply put, lets you delete a ".strtolower($this->v('single')).". Don't worry about accidentally clicking on it either, it'll double check you actually want to delete the ".strtolower($this->v('single'))." before it's deleted.<br /><br />";
			
			// Name
			$html .= "
<b>".$this->v('single')." Name</b><br />
You can also click on the name of the ".strtolower($this->v('single')).". This works exactly the same as the 'Edit' icon and will take you to a form where you can edit the ".strtolower($this->v('single')).".<br /><br />";
			
			// Pagination
			$html .= "
<b>Page Numbers</b><br />
If you have a bunch of ".strtolower($this->v('plural')).", they will probably be divided into several pages. To navigate the pages of ".strtolower($this->v('plural')).", simply click a page number, the prev / next link, or the first / last link.<br /><br />

If you don't see the page numbers at the bottom of the page, that means all your ".strtolower($this->v('plural'))." fit on one page.<br /><br />";

			// Return
			return $html;
		}
		
		/**
		 * Checks whether current user has permission to perform given action in this module and (if present) item.
		 *
		 * @param string $action The action we want to check the permission of.
		 * @param boolean|string $message Whether or not you want to display an error message if they don't have permission. 0 = no, 1 = yes, use default message, string = yes, use the $message string.
		 * @return boolean Whether or not the current user has the permission to perform this action on this module/module item.
		 */
		function permission($action,$message = 0) {
			// Page - need for area
			$page = g('page');
			
			// Message - build here so we can overwrite entire thing in plugin check
			if(!x($message) or $message === 1) $message = "I'm sorry, but you don't have permission to ".($page->area == "public" ? "view" : "do")." this.";
			if($message) {
				$message = "<div class='core-red core-none'>".$message."</div>";	
				if(!g('u.id')) $message .= "{login_form}";
			}
			
			// Permission - global
			if($action) $permission = permission($this->module,$action,NULL,array('type' => $this->type));
			
			// Message
			if(!$permission and $message) {	
				// Login form
				if(strstr($message,"{login_form}")) {
					$login_form = $this->login_form();	
					$message = str_replace("{login_form}",$login_form,$message);
				}		
				
				// Message
				print "
<div class='permission-error permission-error-module'>
	".$message."
</div>";
			}
			
			// Return
			return $permission;
		}
		
		/**
		 * Determines whether or not the current module is 'visible' to the current user.
		 *
		 * Currently no way to make one 'invisible', but may need it in the future.
		 * 
		 * @param boolean|string $message Whether or not you want to display an error message if it's not visible. 0 = no, 1 = yes, use default message, string = yes, use the $message string.
		 * @return boolean Whether or not the module is visible.
		 */
		function visible($message = 0) {
			// Visible
			$visible = 1;
			
			// Page - need for area
			$page = g('page');
			
			// Message
			if(!$visible and $message) {
				// Container
				print "<div class='core-red core-none'>";
				
				// Default
				if($message === 1) print "I'm sorry, but you don't have permission to ".($page->area == "public" ? "view" : "do")." this.";
				// Custom
				else print $message;
				
				// End container
				print "</div>";
			}
			
			// Return
			return $visible;
		}
		
		/**
		 * Returns HTML of this given view for this module.
		 *
		 * @param string $view The 'view' you want to load.
		 * @param array $variables An array of variables to pass when get the HTML of the view. Example: array('perpage' => 5); would be accessible via $perpage in the view file. Default = NULL
		 * @return string The HTML of the view.
		 */
		function view($view,$variables = NULL) {
			// Error
			if(!$this->module or !$view) return;
			
			// Array
			$array = array(
				'module' => $this->module,
				'view' => $view
			);
			
			// View
			$loader = loader::load();
			return $loader->view($array,$variables);
		}
		
		/**
		 * Returns the HTML for displaying filters on the current page.
		 * 
		 * @param array $c An array of configuration values. Default = NULL
		 * @return string|object The HTML of the filters or (if $c['return'] == "object") the form class object of the filters.
		 */
		function manage_filters($c = NULL) {
			// Config
			$page = g('page');
			if(!$c[area]) $c[area] = $page->area;
			if(!x($c[inputs])) $c[inputs] = 1; // Automatically add filter inputs to the form. Only really works if you're returning an 'object'
			if(!x($c[type])) $c[type] = 1; // Include type filter (if the module has types).
			if(!x($c[category])) $c[category] = 1; // Include category filter (if it's turned on in the module).
			if(!x($c[search])) $c[search] = 1; // Include search filter (if it's turned on in the module).
			if(!x($c[query_skip])) $c[query_skip] = array(); // An array of values in the current URL's query string that you don't want to include when the filter is applied.
			if(!$c['return']) $c['return'] = "html"; // What to return: html [default], object (form class object).
			
			// Form
			$form = form::load(array('action' => url_noquery(),'method' => 'GET','process' => 0,'inline' => 1,'submit' => 0));
			
			// Inputs
			if($c[inputs]) {
				// Type
				if($this->v($c[area].'.filters.types') and $types = $this->v('types') and $c[type]) {
					$options = NULL;
					foreach($types as $type => $v) {
						$options[$type] = $v[name];
					}
					if($options) $form->select(NULL,'type',$options,$page->type,array('placeholder' => '- Type - '));
				}
				
				// Category
				if($this->v($c[area].'.filters.categories') and $this->v('plugins.categories') and $c[category]) {
					$categories = module_categories($this->module,$this->type);
					foreach($categories as $id => $row) {
						$options = categories_options($id);
						if($options) {
							$options = array_merge(array(array('label' => '- '.$row[m('categories.db.name')].' -')),$options);
							$form->select(NULL,'categories['.$id.']',$options,$_GET['categories'][$id]);
						}
					}
				}
				
				// Search
				if($this->v($c[area].'.filters.search') and $c[search]) {
					$form->text(NULL,'q',urldecode($_GET['q']),array('placeholder' => 'Search..'));
				}
			}
			
			if($form->inputs() or $c['return'] == "object") {
				// Hidden
				foreach($_GET as $k => $v) {
					if(!in_array($k,array('p','sef','categories')) and !in_array($k,$c[query_skip])) {
						if(is_array($v)) {
							foreach($v as $k1 => $v1) {
								if(!$form->input_exists($k."[".$k1."]")) {
									$form->hidden($k."[".$k1."]",$v1);
								}	
							}
						}
						else {
							if(!$form->input_exists($k)) {
								$form->hidden($k,$v);
							}
						}
					}
				}
			
				// Return
				if($c['return'] == "object") return $form;
				else {
					// Submit
					$form->submit('Go');
					// HTML
					return "<div class='page-filters'>".$form->render()."</div>";
				}
			}
		}
		
		/**
		 * Queries a page worth of managable items and returns either the HTML display for those items or an array of information about the items (depending on the $c['return'] value).
		 *
		 * Array contains:
		 * - rows_count: The total number of managable items
		 * - page: The current page we're on ($c[page]). Defaults to $_GET['p'] || 1 unless a $c[page] value was passed.
		 * - perpage: The number of items we're displaying per page ($c[perpage]). Deafults to 20 unless a $c[perpage] value was passed.
		 * - items: An array of item objects for $c[perpage] items on the $c[page] page.
		 *
		 * Note:  $c gets passed to pagination_html() so the configuration options in that function are also available to you.
		 * 
		 * @param array $c An array of congiguration values. Default = NULL
		 * @return string|array The HTML of the items or an array of information about the items.
		 */
		function manage_items($c = NULL) {
			// Sortable?
			$sort = $this->manage_items_sort($c);
			if($sort and $this->v('db.order_parent')) $nest = 1;
			
			// Config
			if(!$c[page]) $c[page] = ($_GET['p'] ? $_GET['p'] : 1); // The current page number we're on.
			if(!$c[perpage]) $c[perpage] = ($c[pp] ? $c[pp] : ($sort ? 500 : 20)); // The number of items to display per page. Legacy: $c[pp].
			if(!x($c[pagination])) $c[pagination] = 1; // Include pagination below list of items (if more than one page worth)
			if(!$c[query_c]) $c[query_c] = array(); // An array of configuration values to be passed on to the $db->rows() function.
			if(!$c[none]) $c[none] = "<div class='core-none'>No ".$this->v('plural')." Found</div>"; // Message to display if no items found
			if(!$c['return']) $c['return'] = "html"; // What to return: html [default], array
			if(!$c[area]) {
				$page = g('page');
				$c[area] = $page->area;
			}
			if(!x($c[debug])) $c[debug] = 0; // Debug
			
			// Query config
			if(!$c[query_c]['db.type']) $c[query_c]['db.type'] = $this->type;
			//if(!$c[query_c]['db.parent_id']) $c[query_c]['db.parent_id'] = $this->parents[last][id]; // $this->parents not availabile in module class, passing this when we call manage_items() and there is a parent
			if(!$c[query_c]['db.order_parent'] and $nest) $c[query_c]['db.order_parent'] = 0;
			if(!$c[query_c]['categories']) $c[query_c]['categories'] = $_GET['categories'];
			if(!$c[query_c]['search']) $c[query_c]['search'] = urldecode($_GET['q']);
			// Query config - account
			if($c[area] == "account") {
				if(!$c[query_c]['db.user']) $c[query_c]['db.user'] = u('id');	
			}
			
			// Query config - types
			if(!$c[query_c]['db.type']) {
				if($this->v('types')) {
					$types_show = NULL;
					$types_hide = NULL;
					foreach($this->v('types') as $type => $v) {
						if($this->v($c[area].'.active',$type)) $types_show[] = $type;
						else $types_hide[] = $type;
					}
					if($types_hide) {
						$c[query_c]['where'] .= ($c[query_c]['where'] ? " AND " : "")."i.".$this->v('db.type')." IN ('".implode("','",$types_show)."')";
					}
				}
			}
			// Query config - debug
			$c[query_c][debug] = $c[debug];
			
			// Rows
			$rows_count = $this->rows_count($c[query_c]);
			
			// Query config - order
			if(!$c[query_c][order]) {
				if($sort) $c[query_c][order] = $this->v('db.order')." ASC, ".$this->v('db.id')." ASC";
				else if($this->v('db.name')) {
					if(strstr($this->v('db.name'),' ')) {
						$ex = explode(' ',$this->v('db.name'));
						foreach($ex as $v) $c[query_c][order] .= ($c[query_c][order] ? ", " : "").$v." ASC";
					}
					else $c[query_c][order] = $this->v('db.name')." ASC";
				}
				else $c[query_c][order] = $this->v('db.id')." DESC";
			}
			// Query config - limit
			$c[query_c][limit] = pagination_limit($rows_count,$c[perpage],$c);
	
			// Items
			$items = NULL;
			$rows = $this->rows($c[query_c]);
			if($this->db->n($rows)) {
				if($c['return'] == "html") {
					// HTML - start - nest
					if($nest) {
						if(!$c[query_c]['db.order_parent']) $text .= "
".include_javascript('jquery.ui.nested')."
".include_css('jquery.ui.nested');
						$text .= "
<ul class='nest-ul".(!$c[query_c]['db.order_parent'] ? " nest {module:\"".$this->module."\"}" : "")."'>";
					}
					
					// HTML - start - table
					else {
						if($sort) $text .= "
".include_javascript('jquery.ui.sortable')."
".include_css('jquery.ui');
						$text .= "
<table class='core-table core-rows".($sort ? " sort {vars:\"module=".$this->module."\"}" : "")."'>";
					}
				}
				
				while($row = $this->db->f($rows)) {
					// Item
					$item = $this->item($row);
					
					// Array - item
					if($c['return'] == "array") {
						// Array - item - nest
						if($nest) {
							$_c = $c;
							$_c[query_c]['db.order_parent'] = $item->id;
							$children = $this->manage_items($_c);
							if($children) $item[children] = $children;
						}
						$items[] = $item;
					}
					// HTML - item
					else $text .= $this->manage_items_row($item,array('sort' => $sort,'nest' => $nest,'area' => $c[area]));
				}
				if($c['return'] == "html") {
					// HTML - end - nest
					if($nest) $text .= "
</ul>";
					// HTML - end - table
					else $text .= "
</table>";
				}
			}
			// HTML - none
			else if($c['return'] == "html") {
				if(!$nest or !$c[query_c]['db.order_parent']) $text .= $c[none];
			}
	
			// HTML - pages
			if($c['return'] == "html" and $c[pagination]) {
				if($rows_count > $c[perpage]) $text .= "<br />
".pagination_html($rows_count,$c[perpage],$c);
			}
			
			// Return
			if($c['return'] == "array") {
				$return = array(
					'rows_count' => $rows_count,
					'page' => $c[page],
					'perpage' => $c[perpage],
					'items' => $items,
					'c' => $c
				);
			}
			else $return = $text;
			return $return;
			
			/*// Simple version - for use in a module class method (NOTE: doesn't call $this->manage_items_row(), handles rows within this method. If you want separate, can get rid of $c[area] stuff at top and everything after $item.)
			// Config
			if(!$c[area]) {
				$page = g('page');
				$c[area] = $page->area;
			}
			
			// Pagination
			$query_c = array();
			$rows_count = $this->rows_count($query_c);
			$perpage = 20;
			$query_c[order] = $this->v('db.created')." DESC";
			$query_c[limit] = pagination_limit($rows_count,$perpage);
			
			// Items
			$rows = $this->rows($query_c);
			if($this->db->n($rows)) {
				$text .= "
<table class='core-table core-rows'>";
				while($row = $this->db->f($rows)) {
					// Item
					$item = $this->item($row);
					
					// Image
					if($this->v('db.image')) {
						$file = $item->file('db.image',array('thumb' => 't'));
						$file_url = $file->url();
					}
					
					// Icons
					$icons = NULL;
					$icons[edit] = $item->icon('edit');
					$icons[delete] = $item->icon('delete');
					$icons = array_filter($icons);
					$icons_width = (count($icons) * 18);
					
					$text .= "
	<tr id='".$this->module."-".$item->id."'>";
					if($icons) $text .= "
		<td width='".$icons_width."'>".implode($icons)."</td>";
					if($this->v('db.image')) $text .= "
		<td width='".$this->v('uploads.types.image.thumbs.t.width')."'><a href='".$item->url($c[area])."'><img src='".$file_url."' alt='".string_encode($item->name)."' /></a></td>";
					$text .= "
		<td><a href='".$item->url($c[area])."?redirect=".urlencode(URL)."'>".$item->name."</a></td>
	</tr>";	
				}
				$text .= "
</table>";
			}
			else {
				$text .= "
<div class='core-none'>
	No ".$this->v('plural')." Found
</div>";	
			}
			
			// Pages
			if($rows_count > $perpage) $text .= "<br />
".pagination_html($rows_count,$perpage);

			// Return
			return $text;*/
			
			
			
			/*// Simple version - for use in a 'view' (NOTE: doesn't call $this->manage_items_row(), handles rows within this method)
			// Pagination
			$query_c = array();
			$rows_count = $this->module_class->rows_count($query_c);
			$perpage = 20;
			$query_c[order] = $this->module_class->v('db.created')." DESC";
			$query_c[limit] = pagination_limit($rows_count,$perpage);
			
			// Items
			$rows = $this->module_class->rows($query_c);
			if($this->db->n($rows)) {
				print "
<table class='core-table core-rows'>";
				while($row = $this->db->f($rows)) {
					// Item
					$item = item::load($this->module,$row);
					
					// Image
					if($this->module_class->v('db.image')) {
						$file = $item->file('db.image',array('thumb' => 't'));
						$file_url = $file->url();
					}
					
					// Icons
					$icons = NULL;
					$icons[edit] = $item->icon('edit');
					$icons[delete] = $item->icon('delete');
					$icons = array_filter($icons);
					$icons_width = (count($icons) * 18);
					
					// Row
					print "
	<tr id='".$this->module."-".$item->id."'>";
					if($icons) $text .= "
		<td width='".$icons_width."'>".implode($icons)."</td>";
					if($this->module_class->v('db.image')) print "
		<td width='".$this->module_class->v('uploads.types.image.thumbs.t.width')."'><a href='".$item->url($this->area)."'><img src='".$file_url."' alt='".string_encode($item->name)."' /></a></td>";
					print "
		<td><a href='".$item->url($c[area])."?redirect=".urlencode(URL)."'>".$item->name."</a></td>
	</tr>";	
				}
				print "
</table>";
			}
			else {
				print "
<div class='core-none'>
	No ".$this->module_class->v('plural')." Found
</div>";	
			}
			
			// Pages
			if($rows_count > $perpage) print "<br />
".pagination_html($rows_count,$perpage);*/
		}
		
		/**
		 * Returns HTML of an item row to be displayed when managing items in the admin/account.
		 * 
		 * @param object $item The item() object of the item we want to display.
		 * @param array $c An array of congiguration values. Default = NULL
		 * @return string The HTML of the item row.
		 */
		function manage_items_row($item,$c = NULL) {
			// Config
			if(!x($c[area])) { // What 'area' are we linking icons to (ex: edit). 
				$page = g('page');
				$c[area] = $page->area;
			}
			
			// URL - do here so we can effect in child module icons
			$url = $item->url($c[area])."?redirect=".urlencode(URL);
			
			// Icons
			$icons = NULL;
			if($c[sort] and !$c[nest]) $icons[sort] = "<span class='i i-sort i-inline sort-handle tooltip' title='Sort'><input type='hidden' class='sort-sorted' value='".$item->id."' /></span>";
			$icons[edit] = $item->icon('edit',$c);
			$icons[copy] = $item->icon('copy',$c);
			$icons[disable] = $item->icon('disable',$c);
			$icons[delete] = $item->icon('delete',$c);
			if($this->v('children')) {
				$icons_children = NULL;
				foreach($this->v('children') as $module) {
					if(permission($module,'manage') and m($module.'.'.$c[area].'.views.home')) {
						$url = $item->url(($c[area] ? $c[area] : $c[area]))."/".$module;
						$icons_children['child_'.$module] = "<a href='".$url."' class='i i-".m($module.'.icon')." i-inline tooltip' title='".string_encode(m($module.'.name'))."'></a>";
					}
				}
				if($icons_children) {
					$icons[] = "<span class='i i-inline i-separator'></span>";
					$icons = array_merge($icons,$icons_children);
				}
			}
			$icons = array_filter($icons);
			$icons_width = (count($icons) * 18);
			// Image
			if($this->v('db.image')) {
				$file = $item->file('db.image',array('thumb' => 't','default' => 1));
				$file_url = $file->url();
			}
			
			// Row - nest
			if($c[nest]) {
				$_c = $c;
				$_c[query_c]['db.order_parent'] = $item->id;
				$text .= "
		<li class='clear nest-item".($item->visible ? "" : " core-fade")."' id='".$this->module."-".$item->id."'>
			<div class='nest-handle core-row'>";
				if($icons) $text .= "
				<div class='nest-icons' style='width:".$icons_width."px;'>
					".implode($icons)."
				</div>";
				$text .= "
				<a href='".$url."'>".$item->name."</a>
			</div>
			".$this->manage_items($_c)."
		</li>";
			}
			// Row - table
			else {
				$class = NULL;
				if(!$item->visible) $class .= ($class ? " " : "")."core-fade";
				if($c[sort]) $class .= ($class ? " " : "")."sort-sortable";
				
				$text .= "
	<tr id='".$this->module."-".$item->id."'".($class ? " class='".$class."'" : "").">";
				if($icons) $text .= "
		<td width='".$icons_width."'>".implode($icons)."</td>";
				if($this->v('db.image')) $text .= "
		<td width='".$this->v('uploads.types.image.thumbs.t.width')."'><a href='".$url."'><img src='".$file_url."' alt='".string_encode($item->name)."' /></a></td>";
				$text .= "
		<td><a href='".$url."'>".$item->name."</a></td>
	</tr>";	
			}
		
			// Return
			return $text;
			
			/*// Simple version - for use in module classes
			// Config
			if(!x($c[area])) { // What 'area' are we linking icons to (ex: edit). 
				$page = g('page');
				$c[area] = $page->area;
			}
			
			// Icons
			$icons = NULL;
			$icons[edit] = $item->icon('edit',$c);
			$icons[delete] = $item->icon('delete',$c);
			$icons = array_filter($icons);
			$icons_width = (count($icons) * 18);
			
			// Image
			if($this->v('db.image')) {
				$file = $item->file('db.image',array('thumb' => 't','default' => 1));
				$file_url = $file->url();
			}
			
			// Row
			$text .= "
	<tr id='".$this->module."-".$item->id."'>";
			if($icons) $text .= "
		<td width='".$icons_width."'>".implode($icons)."</td>";
			if($this->v('db.image')) $text .= "
		<td width='".$this->v('uploads.types.image.thumbs.t.width')."'><a href='".$url."'><img src='".$file_url."' alt='".string_encode($item->name)."' /></a></td>";
			$text .= "
		<td><a href='".$item->url($c[area])."?redirect=".urlencode(URL)."'>".$item->name."</a></td>
	</tr>";	
		
			// Return
			return $text;*/
		}
		
		/**
		 * Determines if the current user can sort the items they're managing on this page.
		 * 
		 * @param array $c An array of congiguration values. Default = NULL
		 * @return boolean Whether of not the items on the current page can be sorted by the current user.
		 */
		function manage_items_sort($c = NULL) {
			// Sortable?
			if($this->v('db.order') and (!$c[query_c][order] or substr($c[query_c][order],0,strlen($this->v('db.order'))) == $this->v('db.order'))) $sort = 1;
			else $sort = 0;
			
			// Return
			return $sort;
		}
		
		/**
		 * Returns array of settings for this module (and type if set), including plugin settings.
		 *
		 * @param boolean $inputs Do you want to include just the inputs for the settings form (0) or just the setting values? Default = 0 (just the values)
		 * @return array An array of setting inputs.
		 */
		function settings($inputs = 0) {
			// Values
			if(!$inputs) {
				$settings = $this->v('settings');
				unset(
					$settings[inputs],
					$settings[form],
					$settings[container],
					$settings[row],
					$settings[label],
					$settings[field],
					$settings[input]
				);
			}
			// Inputs
			else {
				// Inputs - module
				$settings = $this->v('settings.inputs',$type);
				if(!$settings) $settings = array();
				
				// Inputs - plugins
				if($plugins = $this->v('plugins')) {
					foreach($plugins as $plugin => $active) {
						if($active) {
							$plugin_class = plugin::load($this->module,NULL,$plugin);
							$plugin_settings = $plugin_class->module_settings();
							if($plugin_settings) {
								foreach($plugin_settings as $k => $v) {
									if($v[name] and substr($v[name],0,(strlen($plugin) + 1)) != $plugin."[") {
										$name = array_string($v[name]);
										$name = string_array($plugin.".".$name);
										$plugin_settings[$k][name] = $name;
									}
								}
								$settings = array_merge($settings,$plugin_settings);
							}
						}
					}
				}
			}
			
			// Return
			if(count($settings) == 0) return false;
			else return $settings;
		}
	
		/**
		 * Returns the desired setting in this module or, if $value is passed, it updates the setting.
		 *
		 * @param string $key The key of the setting you want to retrieve.
		 * @param mixed $value A value you want to save for the given setting. Default = NULL
		 * @return mixed The specific setting's value.
		 */
		function setting($key,$value = NULL) {
			// Error
			if(!$key) return;
			
			// Get
			if(!x($value)) {
				return $this->v('settings.'.$key);
			}
			// Set
			else {
				// Settings
				$settings = $this->settings();
				
				// Update/add new setting
				$settings = array_eval($settings,$key,$value);
				
				// Item
				$item = item::load('modules',$this->v('id'));
				// Save
				$item->save(array('module_settings' => data_serialize($settings)));
				
				// Return
				return $value;
			}
		}
	}
}
?>