<?php
if(!class_exists('plugin',false)) {
	/**
	 * Class for managing plugin values for an item.
	 *
	 * @package kraken
	 */
	class plugin extends core_framework {
		// Variables
		var $plugin, $module, $type, $id, $view; // string|int
		var $cache = array(); // array
		var $item; // object
		
		/**
		 * Loads and returns an instance of either the core plugin class or (if it exists) the plugin specific plugin class.
		 *
		 * Example:
		 * - $plugin = plugin::load($module,$id,$plugin,$c);
		 *
		 * @param string $module The module of the item the plugin is acting upon.
		 * @param int $id The id of the item the plugin is acting upon.
		 * @param string $plugin The plugin we're using.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return object An instance of either the core plugin class or (if it exists) the plugin specific plugin class.
		 */
		static function load($module,$id,$plugin,$c = NULL) {
			$params = func_get_args(); // Must be outside function call
			$class_load = load_class_plugin(__CLASS__,$params,$module,$id,$plugin,$c); // No way to instanitiate class with array of params so we'll just return the class name and...
			return new $class_load($module,$id,$plugin,$c); // ...manually pass the params here
		}
		
		/**
		 * Constructs the class.
		 *
		 * @param string $module The module of the item the plugin is acting upon.
		 * @param int $id The id of the item the plugin is acting upon.
		 * @param string $plugin The plugin we're using.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function __construct($module,$id,$plugin,$c = NULL) {
			self::plugin($module,$id,$plugin,$c);
		}
		function plugin($module,$id,$plugin,$c = NULL) {
			// Config
			if(!x($c[cached])) $c[cached] = 1; // Get cached data (if available)
			if(!x($c[cache])) $c[cache] = 1; // Cache data for future use
			
			// Parent
			parent::__construct($c);
			
			// Item
			if($c[item]) $this->item = $c[item];
			else $this->item = item::load($module,$id,$this->c);
			
			// Variables
			$this->module = $module;
			$this->id = $this->item->id; // Use $this->item->id instead of $id in case we passed item row array as $id
			$this->plugin = $plugin;
			
			// Type
			if($this->item->type) $this->type = $this->item->type;
			else if($this->c[type]) $this->type = $this->c[type];
			
			// Parent(s)
			if($this->c[parent_plugin] and $this->c[parent_plugin_id]) {
				$parents = array(
					array(
						'plugin' => $this->c[parent_plugin],
						'plugin_id' => $this->c[parent_plugin_id],
					)
				);
				if($this->c[parents]) {
					foreach($this->c[parents] as $k => $v) {
						$parents[] = $v;	
					}
				}
				$this->c[parents] = $parents;
			}
			
			// Config
			//if($c) $this->c = array_merge($this->c,$c); // Don't need now that we use a 'core' class
			
			// Database
			//$this->db = new db(); // Don't need now that we use a 'core' class
		}
		
		/**
		 * Helper function so you can easily get a plugin's config/setting value. Example: $this->plural will call $this->v('plural').
		 *
		 * Deprecated, must use $plugin->v() from now on.
		 *
		 * @param string $key The key of the plugin value you want to get.
		 * @return mixed The key's corresponding value within this plugin.
		 */
		/*function __get($key) {
			// Error
			if(!$key) return;
			
			// Get value
			$value = self::v($key);
			
			// Set globally (3x quicker 2nd time around)
			$this->$key = $value;
				
			// Return value
			return $value;
		}
		
		/**
		 * Returns a plugin's config/setting value. Example: $this->v('plural') will return m($this->plugin.'.plural').
		 *
		 * @param string $key The key of the plugin value you want to get.
		 * @return mixed The key's corresponding value within this plugin.
		 */
		function v($key) {
			// Error
			if(!$key) return;
			
			// Value
			$value = plugin($this->plugin.'.'.$key);
				
			// Return value
			return $value;
		}
		
		/**
		 * Gets array of plugin rows for this item.
		 *
		 * Works fine if plugin only inserts one row. If not, you'll have to use rows.
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of rows for this item in this plugin.
		 */
		function rows($c = NULL) {
			// Data not stored locally
			if(self::v('db.table')) {
				// Config
				if(!$c[select]) $c[select] = "i.*"; // What to 'SELECT'. Default = * (all)
				if(!$c[where]) $c[where] = NULL; // The 'WHERE' clause of the query.
				if(!$c[order]) $c[order] = NULL; // The 'ORDER BY' clause of the query.
				if(!$c[limit]) $c[limit] = NULL; // The 'LIMIT' clause of the query (note, you could add it to $c[order] too if you wanted).
				if(!$c[db]) $c[db] = array(); // An array of 'global databse variables' we want to match.
				if(!$c['db.code']) $c['db.code'] = NULL; // An example of passing a global database variable we want to match.
				$c_string = cache_config($c);
				
				// Cached?
				if($rows = $this->cache[rows][$c_string]) return $rows;
				
				// Global db variables
				foreach($c as $k => $v) {
					// Array of global database variables. Example: $c[db] = array('code' => 'test','order_parent' => 123);
					if($k == "db" and is_array($v)) {
						foreach($v as $k0 => $v0) {
							if($key = self::v('db.'.$k0) and x($v0)) $c[where] .= "
					".($c[where] ? "AND " : "")."i.".$key." = '".a($v0)."'";	
						}
					}
					// Single global database variable. Example: $c['db.order_parent'] = 123
					else if(strstr($k,'db.')) {
						if($key = self::v($k) and x($v)) $c[where] .= "
					".($c[where] ? "AND " : "")."i.".$key." = '".a($v)."'";	
					}
				}
				
				// Don't mix COUNT(*) and GROUP BY 
				if($c[group] and strstr($c[select],"COUNT(*) c")) {
					$c[select] = str_replace("COUNT(*) c","i.".m($this->module.'.db.id')." c_rows",$c[select]);
				}
			
				// Select
				$query = "
				SELECT
					".$c[select];
				
				// Table
				$query .= "
				FROM
					`".self::v('db.table')."` i";
				// Tables - join - note: must pass full statement, including the 'JOIN' or 'LEFT JOIN' or whatever
				if($c[join]) $query .= "
					".$c[join];
				
				// Where
				$where = NULL;
				if($this->module) $where .= "
					".($where ? "AND " : "")."i.`".self::v('db.parent_module')."` = '".$this->module."'";
				if($this->id > 0) $where .= "
					".($where ? "AND " : "")."i.`".self::v('db.parent_id')."` = '".$this->id."'";
				$parent_plugin = self::v('db.parent_plugin');
				if($parent_plugin and !strstr($c[where],$parent_plugin." = ") and $this->c[parents][0][plugin]) $where .= "
					".($where ? "AND " : "")."i.`".$parent_plugin."` = '".$this->c[parents][0][plugin]."'";
				$parent_plugin_id = self::v('db.parent_plugin_id');
				if($parent_plugin_id and !strstr($c[where],$parent_plugin_id." = ") and $this->c[parents][0][plugin_id]) $where .= "
					".($where ? "AND " : "")."i.`".$parent_plugin_id."` = '".$this->c[parents][0][plugin_id]."'";
				if($c[where]) $where .= "
					".($where ? "AND " : "").$c[where];
				if($where) $query .= "
				WHERE".$where;
				
				// Group
				if($c[group]) $query .= "
				GROUP BY
					".$c[group];
				
				// Having
				if($c[having]) $query .= "
				HAVING
					".$c[having];
				
				// Order
				if($c[order]) $query .= "
				ORDER BY
					".$c[order];
					
				// Limit
				if($c[limit]) $query .= "
				LIMIT
					".$c[limit];
					
				// Debug
				debug($query,$c[debug]);
			
				// Get
				$rows = $this->db->q($query); 
				
				// Cache
				$this->cache[rows][$c_string] = $rows;
				$this->cache_save();
				
				// Return
				return $rows;
			}
		}
		
		/**
		 * Gets number of plugin rows for this item.
		 *
		 * @param array $c An array of configuration values. Note, same configuration values as $plugin->rows(). Default = NULL
		 * @return array An array of rows for this item in this plugin.
		 */
		function rows_count($c = NULL) {
			// Data not stored locally
			if(self::v('db.table')) {
				// Don't want these
				unset($c[order],$c[limit]);
				
				// Select
				$c[select] = "COUNT(*) c";
				
				// Query
				$rows = $this->rows($c);
				$row = $this->db->f($rows);
				
				// Rows
				if($row[c_rows]) $rows = $this->db->n($rows);
				else $rows = $row[c];
				if(!$rows) $rows = 0;
				
				// Return
				return $rows;
			}
		}
		
		/**
		 * Returns the form class object for adding an item in this plugin.
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 * @return object The form class object with fields pre-added in.
		 */
		function form($c = NULL) {
			// Error
			if(!$this->module or /*!$this->id or */!$this->plugin) return;
			
			// Form - config
			if(!$c) $c = array();
			$plugin_form = self::v('forms.add');
			if($plugin_form) $form_c = array_merge($plugin_form,$c);
			else $form_c = $c;
			unset($form_c[inputs]);
			
			// Form - config - redirect
			if(!$form_c[redirect]) $form_c[redirect] = $_SESSION['urls'][0];
			
			// Form
			$form = form::load($form_c);
			
			// Inputs - plugin
			if($plugin_form[inputs]) {
				$form->inputs($plugin_form[inputs]);
			}
				
			// Inputs - plugins (that are a part of this plugin...plugins within a plugin, but sidebar must be stored 'locally' as there's no way to store a parent plugin in a plugin table) // Too much can go wrong here, just have to manually check/add to forms
			/*if($plugins = self::v('plugins')) {
				foreach($plugins as $plugin => $active) {
					if($active == 1) {
						$inputs = plugin($plugin.'.forms.sidebar.inputs');
						foreach($inputs as $k => $v) {
							if($v[name]) {
								// Database variable name
								if(substr($v[name],0,3) == "db.") $inputs[$k][name] = self::v($v[name]);
								
								// Value
								$inputs[$k][value] = $this->item->v($inputs[$k][name]);
							}
						}
						if($inputs) {
							$form->collapsed(plugin($plugin.'.name'));
							$form->inputs($inputs);
							$form->collapsed_end();
						}
					}
				}
			}*/
			
			// Inputs - default
			//$form->submit('Save');
			$form->hidden('process_action','plugin_save',array('process' => 0));
			$form->hidden('process_plugin',$this->plugin,array('process' => 0));
			$form->hidden('process_module',$this->module,array('process' => 0));
			$form->hidden('process_id',$this->id,array('process' => 0));
			$form->hidden('plugin',$this->plugin,array('process' => 0));
			if($name = self::v('db.parent_module')) $form->hidden($name,$this->module);
			if($name = self::v('db.parent_id')) $form->hidden($name,$this->id);
			if($name = self::v('db.parent_plugin')) $form->hidden($name,$this->c[parents][0][plugin]);
			if($name = self::v('db.parent_plugin_id')) $form->hidden($name,$this->c[parents][0][plugin_id]);
			if($name = self::v('db.user')) $form->hidden($name,u('id'));
			
			// Return
			return $form;
		}
		
		/**
		 * Returns array of inputs for the plugin's sidebar in a framework form.
		 *
		 * @return array An array of inputs.
		 */
		function sidebar() {
			// Data stored locally
			if(!self::v('db.table')) {
				// Inputs
				$inputs = self::v('forms.sidebar.inputs');
				foreach($inputs as $k => $v) {
					if($v[name]) {
						// Database variable name
						if(substr($v[name],0,3) == "db.") $inputs[$k][name] = m($this->module.'.'.$v[name]);
						
						// Value
						$inputs[$k][value] = $this->item->v($inputs[$k][name]);
					}
				}
			}
			// Data stored globally
			else {
				// Inputs
				$inputs = self::v('forms.sidebar.inputs');
				if($inputs) {
					// Saved values
					if(self::v('db.table') and self::v('db.parent_module') and self::v('db.parent_id')) {
						foreach($inputs as $k => $v) {
							if($v[name]) {
								// Row
								if(!$row) $row = $this->db->f($this->db->rows()); 
								// Value
								$inputs[$k][value] = $row[$v[name]];
							}
						}
					}
				}
			}
			
			// Return
			if($inputs) {
				$return = array(
					'label' => self::v('name'),
					'inputs' => $inputs
				);
				return $return;
			}
		}
		
		/**
		 * Processes an array of values we're about to save.
		 * 
		 * Converts all keys in the values array that are prefixed with 'db.' to the corresponding global database key. Example: db.name becomes photo_name.
		 * Also checks to make sure each column/key actually exists in the table.
		 *
		 * @param array $values The array of values.
		 * @param string $statement The statement we'll use to save the values: INSERT or UPDATE. Used for determining automatic values. Default = NULL
		 * @return array The array of values with the 'db' keys replaced with their actual keys.
		 */
		function save_values($values,$statement = NULL) {
			// Error
			if(!$this->plugin or !is_array($values)) return;
			
			foreach($values as $k => $v) {
				// Global database variable
				if(substr($k,0,3) == "db.") {
					$key = self::v($k);
					if($key) $values[$key] = $v;
					unset($values[$k]);
					$k = $key;
				}
				
				// Column exists?
				if(!$this->db->table_column(self::v('db.table'),$k)) {
					unset($values[$k]);	
				}
			}
				
			// Automatic values
			if($values) {
				// Automatic values
				if($key = self::v('db.parent_module')) { // Parent module
					if(!x($values[$key])) $values[$key] = $this->module;
				}
				if($key = self::v('db.parent_id')) { // Parent id
					if(!x($values[$key])) $values[$key] = $this->id;
				}
				if($key = self::v('db.parent_plugin') and $this->c[parents][0][plugin]) { // Parent plugin
					if(!x($values[$key])) $values[$key] = $this->c[parents][0][plugin];
				}
				if($key = self::v('db.parent_plugin_id') and $this->c[parents][0][plugin_id]) { // Parent plugin id
					if(!x($values[$key])) $values[$key] = $this->c[parents][0][plugin_id];
				}
				if($key = self::v('db.updated')) { // Updated
					if(!x($values[$key])) $values[$key] = time();
				}
				// Automatic values - insert
				if($statement == "INSERT") {
					if($key = self::v('db.created')) { // Created
						if(!x($values[$key])) $values[$key] = time();
					}
				}
				// Automatic values - update
				if($statement == "UPDATE") {
					# none yet
				}
			}
			
			// Return
			return $values;
		}
		
		/**
		 * Called whenever an item is saved.
		 *
		 * If there are plugin values in the form, they'll be passed in $values.
		 * If there aren't, but the option was present in the form, $c['isset'] will be set to 1.
		 *
		 * @param array $values An array of values to save (column => value). Note you can pass a global databse definition as the column, example "db.name" will save it to the defined name column.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		/*function item_save($values,$c = NULL) {
			// Error
			if(!$this->module or /*!$this->id or *//*!$this->plugin) return;
			// Data stored locally
			if(!self::v('db.table')) return;
			
			// Config
			if(!x($c['isset'])) $c['isset'] = 0; // This plugin was set in the values array, even if empty. Meaning the option to add values was present and, if none are present, we may want to delete existing ones.
			if(!x($c[debug])) $c[debug] = 1; // Debug
			
			// Values?
			$values = $this->save_values($values);
			if($values) {
				// Exists?
				if(self::v('db.table') and self::v('db.parent_module') and self::v('db.parent_id')) {
					$row = $this->db->f("SELECT * FROM ".self::v('db.table')." WHERE ".self::v('db.parent_module')." = '".$this->module."' AND ".self::v('db.parent_id')." = '".$this->id."'"); 
				}
				
				// Save
				$plugin_item = $this->plugin_item($row);
				$id = $plugin_item->save($values,$c);
			}
			// No values, delete
			else if($c['isset']) {
				$query = "DELETE FROM ".self::v('db.table')." WHERE ".self::v('db.parent_module')." = '".$this->module."' AND ".self::v('db.parent_id')." = '".$this->id."'";
				debug($query,$c[debug]);
				$this->db->q($query); 
			}
		}
			
		/**
		 * Gets called after a parent item's process() method has been called.
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function item_process($c = NULL) {
			// Error
			if(!$this->module or !$this->id or !$this->plugin) return;
			
			
		}
		
		/**
		 * Copies the plugin row(s) for the item to the given new item.
		 *
		 * @param int $new_id The id of the new, copied item.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function item_copy($new_id,$c = NULL) {
			// Error
			if(!$this->module or /*!$this->id or */!$this->plugin) return;
			
			// Skip
			$skip = array(
				self::v('db.id'),
				self::v('db.created'),
				self::v('db.updated'),
			);
			// Changes
			$changes = array(
				self::v('db.parent_id') => $new_id
			);
			
			// Rows
			$rows = $this->rows();
			while($row = $this->db->f($rows)) {
				// Values
				$values = NULL;
				foreach($row as $k => $v) {
					// Not skipped
					if(!in_array($k,$skip)) {
						// Change
						if($changes[$k]) $v = $changes[$k];
						// Value
						$values[$k] = $v;
					}
				}
				
				// Save
				$plugin_item = $this->plugin_item();
				$plugin_item->save($values,$c);
			}
		}
		
		/**
		 * Deletes plugin row(s) for the item.
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function item_delete($c = NULL) {
			// Error
			if(!$this->module or !$this->id or !$this->plugin) return;
			
			// Delete rows individually - slower, but gives us the ability to perform actions on a per plugin item basis
			$rows = $this->rows();
			while($row = $this->db->f($rows)) {
				$plugin_item = $this->plugin_item($row);
				$plugin_item->delete($c);
			}
			
			// Delete rows at once
			/*if(self::v('db.table') and self::v('db.parent_module') and self::v('db.parent_id')) {
				// Query
				$query = "DELETE FROM ".self::v('db.table')." WHERE ".self::v('db.parent_module')." = '".$this->module."' AND ".self::v('db.parent_id')." = '".$this->id."'";
				debug($query,$c[debug]);
				
				// Delete
				$this->db->q($query);
			}*/
		}
		
		/**
		 * Deletes plugin row(s) attached to the plugin item we're about to delete.
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function plugin_item_delete($c = NULL) {
			// Error
			if(!$this->module or !$this->id or !$this->plugin) return;
			
			// Delete rows individually - slower, but gives us the ability to perform actions on a per plugin item basis
			$rows = $this->rows();
			while($row = $this->db->f($rows)) {
				$plugin_item = $this->plugin_item($row);
				$plugin_item->delete($c);
			}
			
			// Delete rows at once
			/*if(self::v('db.table') and self::v('db.parent_module') and self::v('db.parent_id')) {
				// Query
				$query = "DELETE FROM ".self::v('db.table')." WHERE ".self::v('db.parent_module')." = '".$this->module."' AND ".self::v('db.parent_id')." = '".$this->id."'";
				$parent_plugin = self::v('db.parent_plugin');
				if($parent_plugin) $query .= " AND ".$parent_plugin." = '".$this->c[parents][0][plugin]."'";
				$parent_plugin_id = self::v('db.parent_plugin_id');
				if($parent_plugin) $query .= " AND ".$parent_plugin_id." = '".$this->c[parents][0][plugin_id]."'";
				
				// Delete
				$this->db->q($query);
			}*/
		}
		
		/**
		 * Returns array of settings for this plugin which can be set on a per module basis.
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of inputs that constitute the plugin settings that can be set on a per module basis.
		 */
		function module_settings($c = NULL) {
			// Label
			$label = array(
				array(
					'label' => self::v('name'),
					'type' => 'collapsed'
				)
			);
			
			// Settings
			$settings = self::v('settings.inputs');
			if($settings) {
				// Test inputs for this module
				foreach($settings as $k => $v) {
					$settings[$k][options_module] = $this->module;	
					$settings[$k][options_type] = $this->type;
				}
				
				$module_class = module::load($this->module);
				$form = $module_class->form(array('saved' => 0));
				$form->inputs($settings);
				if($form->inputs_render()) {
					// Add label
					$settings = array_merge($label,$settings);
					$settings[] = array('type' => 'collapsed_end');
					
					// Return
					return $settings;
				}
			}
		}
		
		/**
		 * Loads an instance of a plugin item for the current plugin.
		 *
		 * @param int|array $plugin_id The id of the plugin item you want to create the item instance of. You can also pass the item's full db row array.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return object An instance of the plugin_item class for the given plugin item.
		 */
		function plugin_item($plugin_id = NULL,$c = NULL) {
			// Error
			if(/*!$this->module or !$this->id or */!$this->plugin) return;
			
			// Config
			if(!x($c[parents])) $c[parents] = $this->c[parents];
			if(!x($c[cache])) $c[cache] = $this->c[cache];
			if(!x($c[cached])) $c[cached] = $this->c[cached];
			
			// Item
			$plugin_item = plugin_item::load($this->module,$this->id,$this->plugin,$plugin_id,$c);
			
			// Return
			return $plugin_item;
		}
		
		/**
		 * Returns HTML of this given view for this plugin.
		 *
		 * @param string $view The 'view' you want to load.
		 * @param array $variables An array of variables to pass when get the HTML of the view. Example: array('perpage' => 5); would be accessible via $perpage in the view file. Default = NULL
		 * @return string The HTML of the view.
		 */
		function view($view,$variables = NULL) {
			// Error
			if(!$this->module or /*!$this->id or */!$this->plugin or !$view) return;
			
			// Array
			$array = array(
				'module' => $this->module,
				'id' => $this->id,
				'plugin' => $this->plugin,
				'view' => $view
			);
			
			// View
			$loader = loader::load();
			return $loader->view($array,$variables);
		}
		
		/**
		 * Checks whether current user has permission to perform given plugin action in this module and (if present) item.
		 *
		 * @param string $action The action we want to check the permission of.
		 * @return boolean Whether or not the current user has the permission to perform this plugin action on this module/module item.
		 */
		function permission($action) {
			// Permission
			$permission = permission($this->module,$this->plugin.'.'.$action,$this->id,array('type' => $this->item->type));
			// Return
			return $permission;
		}
		
		/**
		 * Creates and returns the HTML of an icon for performing some action on this plugin item.
		 *
		 * This method basically lets us check permission and data within this plugin before we send it to the $core->icon() method.
		 * See $core->icon() for more configuration values.
		 *
		 * @param string $action The action of the icon you're creating.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return string The HTML of the icon.
		 */
		function icon($action,$c = NULL) {
			// Error
			if(/*!$this->module or !$this->id or */!$this->plugin) return;
			
			// Documentation
			if($action == "documentation") {
				if(!$c[icon]) $c[icon] = "help";
				if(!$c[tooltip_text]) $c[tooltip_text] = "Help";
				if($file = $this->documentation_file()) {
					$c[permission] = 0;
					$c[url] = D."?ajax_action=documentation&amp;ajax_plugin=".$this->plugin."&amp;file=".urlencode($file);
					$c[attributes]['class'] .= ($c[attributes]['class'] ? " " : "")."overlay {title:''}";
				}
				else $c[url] = 0; // Unnecessary as we can never have 'documentation' permission so no $c[url] will ever get created, but keep just for the hell of it
			}
			// Download
			if($action == "download") {
				if(!$c[icon]) $c[icon] = "arrow-down";
				if(u('type') == "supers" and !$this->v('parent')) {
					$c[permission] = 0;
					if(!x($c[url])) $c[url] = DOMAIN."?process_action=modules_download&amp;process_module=modules&amp;plugin=".$this->plugin;	
				}
				else $c[url] = 0; // Unnecessary as we can never have 'download' permission so no $c[url] will ever get created, but keep just for the hell of it
			}
			
			// Config
			if(!$c[icon]) $c[icon] = $action; // The CSS 'i-' class you want to apply to generate the icon. Example: 'add' would result in 'i-add'.
			if(!$c[text] and $c[button]) $c[text] = ($c[tooltip_text] ? $c[tooltip_text] : ucwords($action)); // Text to display alongside the icon (required if a 'button')
			if(!$c[tooltip_text]) $c[tooltip_text] = ($c[text] ? $c[text] : ucwords($action)); // The tooltooltip text to show on hover (if $c[tooltip] == 1).
			if(!x($c[permission])) $c[permission] = 1; // Do you want to check whether or not the user has permission to perform this action on this item?
			if(!x($c[permission_action])) $c[permission_action] = $action; // What permission action are we checking against?
			if(!x($c[area])) $c[area] = NULL; // What 'area' are we linking icons to (ex: edit). Defaults to current area (if admin/account), else admin (if user is admin), else account.
			
			// Permission?
			$permission = 0;
			if(!$c[permission]) $permission = 1;
			else if($this->permission($c[permission_action])) $permission = 1;
			if($permission) {
				// URL
				if(!x($c[url])) {
					// Area
					if(!$c[area]) {
						$page = g('page');
						if(in_array($page->area,array('admin','account'))) $c[area] = $page->area;
						else if(u('admin')) $c[area] = 'admin';
						else $c[area] = 'account';
					}
					
					// Default
					# nothing here
				}
			}
			else $c[url] = 0;
				
			// Return
			if($c[url] or $c[required]) {
				return parent::icon($c);
			}
		}
	
		/**
		 * Returns the documentation HTML for this module.
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 * @return string The documentation HTML.
		 */
		function documentation($c = NULL) {
			// File
			$file = $this->documentation_file($c);
			if($file) {
				// HTML
				$html = $this->html_get($file,array('module' => $this->module,'id' => $this->id,'plugin' => $this->plugin));
				
				// Return
				return $html;
			}
		}
	
		/**
		 * Returns the path to the documentation file for this module (if one exists).
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 * @return string The path to the documentaiton file.
		 */
		function documentation_file($c = NULL) {
			// File
			$file = NULL;
			if(!$file) {
				$file = SERVER.$this->v('path_local')."documentation/".u('type').".php";
				if(!file_exists($file)) $file = NULL;
			}
			if(!$file) {
				$file = SERVER.$this->v('path')."documentation/".u('type').".php";
				if(!file_exists($file)) $file = NULL;
			}
			
			// HTML - check to make sure it's not empty
			if($file) {
				$html = $this->html_get($file,array('module' => $this->module,'id' => $this->id,'plugin' => $this->plugin));
				if(!$html) $file = NULL;
			}
			
			// Return
			return $file;
		}
		
		/**
		 * Saves plugin instance or specific data (if 1st and 2nd param) to global cache.
		 *
		 * If you don't pass a name and $data we'll simply save the entire instance to the cache.
		 *
		 * @param string $name The name we want to use when storing the cached data. We'll use this when retrieving the cached data later. 
		 * @param string|array $data The data (string or array) we want to cache
		 */
		function cache_save($name = NULL,$data = NULL) {
			// Error
			if(!$this->module or !$this->id or !$this->plugin or !$this->c[cache]) return;
			
			// Name
			$string = 'item/'.$this->module.'/'.$this->id.'/plugin/'.$this->plugin;
			
			// Specific data
			if($name) $string .= '/'.$name;
			// Class instance
			else $data = $this;
			
			// Cache
			cache_save($string,$data);
		}
		
		/**
		 * Returns saved cache (if it exists).
		 *
		 * @param string $name The name of the cached data.  Default = NULL (which will return a cache of the entire plugin instance if it exists).
		 * @return mixed The cached data.
		 */
		function cache_get($name = NULL,$data = NULL) {
			// Error
			if(!$this->module or !$this->id or !$this->plugin or !$this->c[cached]) return;
			
			// Name
			$string = 'item/'.$this->module.'/'.$this->id.'/plugin/'.$this->plugin;
			
			// Specific data
			if($name) $string .= '/'.$name;
			
			// Get cached data
			return cache_get($string);
		}
	}
}
?>