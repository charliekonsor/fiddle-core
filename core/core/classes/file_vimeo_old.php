<?php
// Class
require_once SERVER."core/core/libraries/vimeo/vimeo.php";

if(!class_exists('file_vimeo')) {
	/**
	 * An extension of the file class with functionality specific to storing files via Vimeo.
	 */
	class file_vimeo extends file {
		/** Your Vimeo Consumer Key. */
		public $vimeo_consumer_key = NULL;
		/** Your Vimeo Consumer Secret. */
		public $vimeo_consumer_secret = NULL;
		/** Your Vimeo's account OAuth token. */
		public $vimeo_oauth_token = NULL;
		/** Your Vimeo's account OAuth token secret. */
		public $vimeo_oauth_token_secret = NULL;
		/** Holds the instance of the Vimeo class we're using currently. */
		public $vimeo_client = NULL;
		
		/**
		 * Constructs the class.
		 *
		 * @param string|array $file A file (either the path to it or the $_FILES array of it) you will be interacting with with this instance of this class.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function __construct($file = NULL,$c = NULL) {
			self::file_vimeo($file,$c);
		}
		function file_vimeo($file = NULL,$c = NULL) {
			// Credentials
			if($c[consumer_key]) file::value('vimeo_consumer_key',$c[consumer_key]);
			if(!file::value('vimeo_consumer_key')) file::value('vimeo_consumer_key',g('config.uploads.storage.vimeo.login.consumer_key'));
			if($c[consumer_secret]) file::value('vimeo_consumer_secret',$c[consumer_secret]);
			if(!file::value('vimeo_consumer_secret')) file::value('vimeo_consumer_secret',g('config.uploads.storage.vimeo.login.consumer_secret'));
			if($c[oauth_token]) file::value('vimeo_oauth_token',$c[oauth_token]);
			if(!file::value('vimeo_oauth_token')) file::value('vimeo_oauth_token',g('config.uploads.storage.vimeo.login.oauth_token'));
			if($c[oauth_token_secret]) file::value('vimeo_oauth_token_secret',$c[oauth_token_secret]);
			if(!file::value('vimeo_oauth_token_secret')) file::value('vimeo_oauth_token_secret',g('config.uploads.storage.vimeo.login.oauth_token_secret'));
			
			// Client
			if(file::value('vimeo_consumer_key') and file::value('vimeo_consumer_secret')) {
				// Client
				$this->vimeo_client = new phpVimeo(file::value('vimeo_consumer_key'),file::value('vimeo_consumer_secret'));
				// Cache
				$this->vimeo_client->enableCache(phpVimeo::CACHE_FILE,SERVER."local/cache/vimeo/",300);
				// OAuth
				if(file::value('vimeo_oauth_token') and file::value('vimeo_oauth_token_secret')) {
					$this->vimeo_client->setToken(file::value('vimeo_oauth_token'),file::value('vimeo_oauth_token_secret'));
				}
			}
			
			// Parent
			parent::__construct($file,$c);
		}
		
		/**
		 * Anaylzes current file for basic information.
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function analyze($c = NULL) {
			// Error
			if(!file::value('file')) return;
			
			// Config
			if(!x($c[defaults])) $c[defaults] = 1; // If doesn't exist, use 'defaults'. Used after uploading since files isn't immediately available via API.
			
			// Exists
			if(file::call('exists')) {
				file::value('name',file::value('file'));
				file::value('path','');
				file::value('extension',file::call('extension'));
				file::value('extension_standardized',file::call('extension_standardized'));
				file::value('type','video');
				file::value('file_changed',0);
				file::value('exists',1);
			}
			// Defaults - file isn't available via API yet, assume defauult values (flv extension, etc.)
			else if($c[defaults] and is_int_value(file::value('file'))) {
				file::value('name',file::value('file'));
				file::value('path','');
				file::value('extension','flv');
				file::value('extension_standardized','flv');
				file::value('type','video');
				file::value('file_changed',0);
				file::value('exists',1);
			}
			// Doesn't exist
			else {
				file::value('exists',0);	
			}
			
			// Debug
			debug("name: ".file::value('name'),file::value('c.debug'));
			debug("extension: ".file::value('extension'),file::value('c.debug'));
			debug("extension_standardized: ".file::value('extension_standardized'),file::value('c.debug'));
			debug("type: ".file::value('type'),file::value('c.debug'));
			debug("exists: ".file::value('exists'),file::value('c.debug'));
		}
		
		/**
		 * Localizes a file, making sure it has a full path and doesn't contain the domain.
		 *
		 * @param string $file The path to the file you want to localize. Defaults to the global $this->file.
		 * @return string The localized file path.
		 */
		/*static*/ function localize($file = NULL) {
			// Default
			if(!$file) $file = file::value('file');
			// Error
			if(!file::value('vimeo_client')) return;
			
			// Remove path - don't use directories on vimeo
			$file = basename($file);
			
			// Return
			return $file;
		}
		
		/**
		 * Gets the full URL of a file (opposite of 'localize').
		 *
		 * @param string $file The path to the file you want to get the URL of. Defaults to the global $this->file.
		 * @return string The URL of the file.
		 */
		/*static*/ function url($file = NULL) {
			// Default
			if(!$file) $file = file::value('file');
			// Error
			if(!$file) return;
			
			// URL
			$url = "http://player.vimeo.com/video/".$file;
			
			// Object
			//$object = file::call('vimeo_file',$file);
			// URL // This is the video 'page' URL, not the URL of the actual video file
			//$url = $object->urls->url[0]->_content;
			
			// Return
			return $url;
		}
		
		/**
		 * Checks if a file exists.		
		 * 
		 * @param string $file The file you want to check exists. Defaults to the global $this->file.
		 * @param boolean $external Not used in this child class
		 * @return boolean Whether or not the file exists.
		 */
		/*static*/ function exists($file = NULL,$external = 0) {
			// Default
			if(!$file) $file = file::value('file');
			// Errorfunct
			if(!$file) return;
			
			// Object
			$object = file::call('vimeo_file',$file);
			
			// Exists
			$exists = ($object->id ? true : false);
			
			// Return
			return $exists;
		}
		
		/**
		 * Returns the extension of the file. Example: "/path/to/file.jpg" would return "jpg".
		 *
		 * @param string $file The path to the file you want to get the extensions of. Defaults to the global $this->file.
		 * @return string The extension of the file.
		 */
		/*static*/ function extension($file = NULL) {
			// Default
			if(!$file) $file = file::value('file');
			// Error
			if(!$file) return;
			
			// Vimeo ID
			if(is_int_value($file)) {
				return "mp4"; // Just returning this for now. Should probably return nothing.
			}
			// Regular file
			else {
				$extension = parent::extension($file);	
			}
			
			// Return
			return $extension;
		}

		/**
		 * Returns width/height of given file												
		 * 
		 * @param string $file The file you want to get the dimensions of. Defaults to the global $this->file.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of the width and height of the given file.
		 */
		/*static*/ function dimensions($file = NULL,$c = NULL) {
			// Default
			if(!$file) $file = file::value('file');
			// Error
			if(!$file) return;
			
			// Cached?
			if($array = file::value('cache.dimensions')) {
				// Return
				return $array;	
			}
			
			// Object
			$object = file::call('vimeo_file',$file);
			
			// Dimensions
			$array = array(
				'width' => $object->width,
				'height' => $object->height,
			);
			
			// Cache
			file::value('cache.dimensions',$array);
			
			// Return
			return $array;
		}
		
		/**
		 * Deletes a file.
		 *
		 * @param string $file The path to the file you want to delete. Defaults to the global $this->file.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		/*static*/ function delete($file = NULL,$c = NULL) {
			// Params
			if(is_array($file)) { // $file->delete($c);
				$c = $file;
				$file = NULL;
			}
			// Default
			if(!$file) $file = file::value('file');
			// Error
			if(!$file or !file::value('vimeo_client')) return;
			
			// Config
			if($c) file::call('c',$c);
			
			// Delete
			try {
				$result = $this->vimeo_client->call('vimeo.videos.delete',array('video_id' => $file));
				debug("Delete result: ".return_array($result),file::value('c.debug'));
			}
			catch (VimeoAPIException $e) {
				debug("Error occured while deleting video: ".$e->getMessage()." (".$e->getCode().")",file::value('c.debug'));
			}
			
			// Cache
			cache_delete('vimeo/files/'.$file);
			
			// Return
			return true;
		}

		/**
		 * Pushes the given $source to the given $destination on the external storage service.	
		 *
		 * Notes:
		 * - Also accepts 'name' and 'description' params passed in $c.	
		 * - If push is called via $form->process(), it'll pass an array of $c[values] which contain
		 *	 db defined values such as 'name', 'description', etc.			
		 * 
		 * @param string $source The full path to the local source file.
		 * @param string $destination The path on the external storage where you want to push this file. Default = NULL
		 * @param array $c An array of configuration values. Default = NULL
		 * @return object The object for the external file if we successfully pushed (nothing is returned if there was an error).
		 */
		function push($source,$destination = NULL,$c = NULL) {
			// Error
			if(!$this->vimeo_client) return;
			
			// Return
			$return = 0;
			
			// Source
			$array[source] = file::load($source);
			// Destination
			$array[destination] = file::call('prepare_destination',$source,$destination,$c);
			
			// Have what we need?
			if($array[source]->exists and $array[source]->type == "video") {
				// Space?
				$space = 0;
				try {
					$quota = $this->vimeo_client->call('vimeo.videos.upload.getQuota');
					if($quota->err->code) {
						debug("Error occured while checking quota (1): ".$quota->err->msg." (".$quota->err->code."): ".$quota->err->expl,file::value('c.debug'));
					}
					else {
						//print_array($quota);
						$space = 1;
					}
				}
				catch (VimeoAPIException $e) {
					debug("Error occured while checking qoota (0): ".$e->getMessage()." (".$e->getCode().")",file::value('c.debug'));
				}
				if($space) {
					// Debug
					debug("Pushing ".$array[source]->file." (local) to ".$array[destination]->storage,file::value('c.debug'));
					
					// Timeout
					ini_set('max_execution_time',1500); // 30 Minutes
					if(ini_get('safe_mode') == 0) set_time_limit(1500);
					ini_set('memory_limit','512M'); // 512 MB
					debug("time limit: ".ini_get('max_execution_time').", memory limit: ".ini_get('memory_limit'),file::value('c.debug'));
					$start = microtime_float();
				        
					// Save
					mkdir(SERVER."local/cache/vimeo/chunks/");
					try {
						$array[destination]->file = $this->vimeo_client->upload($array[source]->file,1,SERVER."local/cache/vimeo/chunks/");
						if($array[destination]->file) {
							// Name
							if(!x($c[name])) $c[name] = $c[values][name]; // Passed via form->push()
							if($c[name]) {
								debug("Setting name to ".$c[name],file::value('c.debug'));
								$this->vimeo_client->call('vimeo.videos.setTitle', array('title' => $c[name],'video_id' => $array[destination]->file));
							}
							// Description
							if(!x($c[description])) $c[description] = $c[values][description]; // Passed via form->push()
							if($c[description]) {
								debug("Setting name to ".$c[name],file::value('c.debug'));
								$this->vimeo_client->call('vimeo.videos.setDescription', array('description' => $c[description],'video_id' => $array[destination]->file));
							}
						}
					}
					catch (VimeoAPIException $e) {
						debug("Error occured while uploading: ".$e->getMessage()." (".$e->getCode().")",file::value('c.debug'));
					}

					// Debug
					debug("File ".$array[source]->file." was uploaded to ".$result,file::value('c.debug'));
					debug("Took ".round(microtime_float() - $start,3)." seconds",file::value('c.debug'));
						
					// Exists?
					$array[destination]->analyze();
					if($array[destination]->exists) {
						// Return
						$return = 1;
						
						// Debug
						debug("Pushed file and it does exist: ".return_array($array[destination]),file::value('c.debug'));
					}
				}
			}
			
			// Return
			if($return) return $array[destination];
		}

		/**
		 * Pulls the current file from external storage servicc to the local $destination.				
		 * 
		 * @param string $destination The local path where you want to save the pulled file.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return object The object for the local file if we successfully pulled it (nothing is returned if there was an error).
		 */
		function pull($destination,$c = NULL) {
			// Error
			if(!$this->vimeo_client or !$destination) return;
			
			#build this
		}
		
		/**
		 * Returns an array of items (files and directories) within the given directory.
		 *
		 * @param string $directory The directory you want to look for items in.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of files in the directory.
		 */
		/*static*/ function directory_items($directory,$c = NULL) {	
			// Error
			if(!file::value('vimeo_client')) return;
		
			// Config
			if(!x($c[files])) $c[files] = 1; // Get files in the root directory.
			if(!x($c[directories])) $c[directories] = 1; // Get directories in the root directory.
			if(!x($c[recursive])) $c[recursive] = 0; // Get items from sub-directories as well as the items in the root directory.
			if(!x($c[multilevel])) $c[multilevel] = 1; // Return a 'multi-level' array when we're searching recursively. If true, child folders will be the key and their contents the value, ex: array('folder' => array('child' => array('file.txt'))). If no, it'll be a single level array such as array('folder','folder/child','folder/child/file.txt').
			if(!x($c[prefix])) $c[prefix] = NULL; // A prefix to append to each item path (used when we're looking through sub-directories in 'recursive' mode when multilevel is turned off).
			if(!$c[paths]) $c[paths] = 'relative'; // Do you want the file names returned to be 'relative' to the directory or 'full' (meaning, include the parent directory's full path)?
				
			// Variables
			$array = array();
			if($c[paths] == "full") $c[prefix] = $directory.$c[prefix];
			
			// Object
			$items = $this->vimeo_client->call('vimeo.videos.getUploaded');
			foreach($items->videos->video as $item) {
				$array[] = $item->id;	
			}
			
			// Return
			return $array;
		}
		
		/**
		 * Determines if the given directory exists.
		 *
		 * Vimeo doesn't use directories, neither will we, must exist.
		 *
		 * @param string $directory The path to the directory you want to check for.
		 * @return boolean Whether or not the directory exists.
		 */
		/*static*/ function directory_exists($directory) {
			// Return
			return true;
		}
	
		/**
		 * Returns HTML for displaying a video.
		 * 
		 * @param array $c An array of configuration values. Default = NULL
		 * @return string The HTML foor displaying the video.
		 */
		function video_html($c = NULL) {
			// Error
			if(!file::value('file')) return;
			
			// Config
			if(!x($c[auto])) $c[auto] = 1; // Autoplay the video
			if(!$c[width]) $c[width] = file::call('width'); // Width of video. Defaults to actual video width or 265 if we can't get video width.
			if(!$c[height]) $c[height] = file::call('height'); // Height of video. Defaults to actual video width or 200 if we can't get video width.
			//if(!$c[min_width]) $c[min_width] = 265; // Minimum width to display video
			//if(!$c[max_width]) $c[max_width] = NULL; // Maximum width to display video
			//if(!$c[poster]) $c[poster] = NULL; // URL of the image to use as a 'poster' preview image
			if(!$c[id]) $c[id] = "player_".random(); // ID of player element
			if(!x($c[debug])) $c[debug] = 0; // Debug
			// Config - Vimeo specific - https://developer.vimeo.com/player/embedding
			if(!x($c[title])) $c[title] = 0; // Show video 'title' over video
			if(!x($c[badge])) $c[badge] = 0; // Show video 'badge' over video             
			if(!x($c[byline])) $c[byline] = 0; // Show uploader 'byline' over video
			if(!x($c[portrait])) $c[portrait] = 0; // Show uploader 'portrait' over video
			
			// Config options we handle, all other c values will be added to the video. This way you can pass config stuff specific to the player you're using.
			$c_skip  = array(
				'auto',
				'preload',
				'width',
				'height',
				'min_width',
				'max_width',
				'poster',
				'downloade',
				'fallback',
				'mobile_redirect',
				'events',
				'id',
				'player',
				'debug'
			);
			
			// Debug
			debug("<b>\$file->video_html();</b>",$c[debug]);
			debug("c:".return_array($c),$c[debug]);
			
			// Object
			$object = file::call('vimeo_file');
			
			// HTML
			if($object->id) {
				// URL
				$url = "http://player.vimeo.com/video/".$object->id;
				$query = NULL;
				if($c[auto]) $query .= ($query ? "&" : "")."autoplay=1";    
				if($c[id]) $query .= ($query ? "&" : "")."player_id=".$c[id];
				if($c[events]) $c[api] = 1;
				foreach($c as $k => $v) {
					if(!in_array($k,$c_skip)) $query .= ($query ? "&" : "").$k."=".$v;
				}
				
				// HTML
				$html .= '
<iframe src="'.$url.($query ? '?'.$query : '').'" width="'.$c[width].'" height="'.$c[height].'" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen'.($c[id] ? ' id="'.$c[id].'"' : '').'></iframe>';

				// Javascript
				if($c[events]) {
					$events_conversion = array(
						'ready' => 'ready',
						'ended' => 'finish',
						# Need to finish this list (my code => their code)
					);
					$html .= "
<script type='text/javascript' src='".DOMAIN."core/core/libraries/vimeo/froogaloop2.min.js'></script>
<script type='text/javascript'>
$(document).ready(function() {
	var player = \$f($('#".$c[id]."')[0]);
	player.addEvent('ready', function() {";
					foreach($c[events] as $k => $v) {
						if($event_converted = $events_conversion[$k]) $html .= "
		player.addEvent('".$event_converted."',".$v.");";
					}
					$html .= "
	});
});
</script>";
				}
			}
			
			// Debug
			debug('video() result: <xmp>'.$html.'</xmp>',$c[debug]);
			
			// Return
			return $html;
		 }
		
		/**
		 * Returns file object as retrieved by $this->vimeo_client->find('find_video_by_id', 123456789);
		 *
		 * @param string $file The Vimeo id of the media you want to get. Defaults to the global $this->file.
		 * @return object The Vimeo file object.
		 */
		/*static*/ function vimeo_file($file = NULL) {
			// Default
			if(!$file) $file = file::value('file');
			// Error
			if(!$file or !file::value('vimeo_client')) return;
			
			// Vimeo ID
			if(is_int_value($file)) {
				// Cached?
				$object = cache_get('vimeo/files/'.$file);
				// No
				if(!$object) {
					// Get
					$object = $this->vimeo_client->call('vimeo.videos.getInfo',array('video_id' => $file));
					$object = $object->video[0];
					// Cache
					cache_save('vimeo/files/'.$file,$object);
				}
			}
			else {
				$object = (object) array();	
			}
			
			// Return
			return $object;
		}

		/**
		 * These functions aren't doable with remotely hosted files.											
		 */
		/*statice*/ function permission($file,$permission = NULL) {
			return;
		}
		/*static*/ function orientation($file = NULL,$c = NULL) {
			return;
		}
		/*static*/ function length($file = NULL,$c = NULL) {
			return;
		}
		/*static*/ function getid3($file = NULL) {
			return;
		}
		function save($destination = NULL,$c = NULL) {
			return;
		}
		function copy($destination = NULL,$c = NULL) {
			return;
		}
		/*static*/ function directory_create($directory,$recursive = 1) {	
			return;
		}
	}
}
?>