<?php
/**
 * Checks to see if a username or e-mail address is already taken					    				
 */
if($_GET['ajax_action'] == "checkUsername" or $_GET['ajax_action'] == "checkEmail") {
	// Form
	$form = form::load('users',NULL);
	$results = $form->process(array('__form' => $_GET['form']),NULL,array('delete' => 0));
	$id = $results[id];
	
	// Debug
	//$core->db->q("INSERT INTO test SET test = '".a("request = ".$_GET['ajax_action'])."'");
	$core->db->q("INSERT INTO test SET test = '".a("results = ".return_array($results),1)."'");
	$core->db->q("INSERT INTO test SET test = '".a("id = ".$id)."'");

	// Username
	if($_GET['ajax_action'] == "checkUsername") {
		$return = (user_name_exists_framework($_GET['user_name'],$id) ? "false" : "true");
	}
	
	// E-mail
	if($_GET['ajax_action'] == "checkEmail") {
		if(g('config.register.email.unique')) {
			$return = (user_email_exists_framework($_GET['user_email'],$id) ? "false" : "true");
		}
		else $return = "true";
	}
	
	// Debug
	$core->db->q("INSERT INTO test SET test = '".a("return = ".$return)."'");
	
	// Return
	print $return;
	
	// Exit
	exit;
}
?>