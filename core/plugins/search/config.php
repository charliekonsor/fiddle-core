<?php
/************************************************************************************/
/************************************** Config **************************************/
/************************************************************************************/
$config = array(
	'default' => 1,
	'required' => 1,
	'name' => 'Search',
	'single' => 'Search Term',
	'plural' => 'Search Terms',
	'icon' => 'search',
	'db' => array(
		'table' => 'items_index',
		'id' => 'id',
		'parent_module' => 'module_code',
		'parent_id' => 'item_id',
		'name' => 'index_word'
	),
	'settings' => array(
		// Defaults
		'public' => 0,
		'index' => 0,
		'columns' => array(
			'db.name' => 10,
			'db.code' => 9,
			//'db.id' => 8,
			'db.description' => 7,
			'db.meta_title' => 8,
			'db.meta_description' => 7,
			'db.meta_keywords' => 6
		)
	)
);
?>