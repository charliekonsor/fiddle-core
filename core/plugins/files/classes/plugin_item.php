<?php
if(!class_exists('plugin_item_files',false)) {
	/**
	 * An extenion of the plugin_item class which handles functionality specific to the files plugin.
	 *
	 * @package kraken\plugins\files
	 */
	class plugin_item_files extends plugin_item {
		// Variables
		var $file;
		
		/**
		 * Constructs the class.
		 *
		 * @param string $plugin The plugin we're using.
		 * @param int $plugin_id The id of the item in the plugin we're acting upon.
		 * @param string $module The module of the item the plugin is acting upon.
		 * @param int $id The id of the item the plugin is acting upon. Default = NULL
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function __construct($module,$id,$plugin,$plugin_id,$c = NULL) {
			$this->plugin_item_files($module,$id,$plugin,$plugin_id,$c);
		}
		function plugin_item_files($module,$id,$plugin,$plugin_id,$c = NULL) {
			// Config
			if(!$c[type]) $c[type] = NULL; // If you want, you can define the 'type' of file this is (if not, we'll guess): file, image, video, audio
			if(!$c[extension]) $c[extension] = NULL; // The extension of the original file's converted extension that you want to get the URL of (only applies to videos). You can also pass an array here which will check to see if each extension exists and, if it does, use that one. Available forrmats are usually flv or mp4.
			if(!$c[thumb]) $c[thumb] = NULL; // The 'key' of the original file's thumb that you want to get the URL of (only applies to images). Available 'keys' are usually t, s, m, or l
			if(!x($c['default'])) $c['default'] = 0; // Return a 'default' file if we can't find one (only used for images right now)
			//if(!x($c[cached])) $c[cached] = 1; // Use 'cached' data (if available). // Defaults in plugin_item class (parent)
			//if(!x($c[cache])) $c[cache] = 1; // Cache data for future use. // Defaults in plugin_item class (parent)
			if(!x($c[debug])) $c[debug] = 0; // Debug
			
			// Parent
			parent::__construct($module,$id,$plugin,$plugin_id,$c);
			
			// Name
			$this->file[name] = $this->v('file_name');
			
			// Extensions
			if($this->v('file_extensions')) $this->file[extensions] = data_unserialize($this->v('file_extensions'));
			// Thumbs
			if($this->v('file_thumbs')) $this->file[thumbs] = data_unserialize($this->v('file_thumbs'));
				
			// Extension
			if(is_array($c[extension]) and $this->file[extensions]) {
				foreach($c[extension] as $extension) {
					$file = $this->file[extensions][$extension][path].$this->file[extensions][$extension][name];
					if(file::exists($file)) {
						if(!$this->file[extension]) $this->file[extension] = $extension;
						else {
							$this->file[fallback][] = $file;
						}
					}
				}
				$c[extension] = $this->file[extension];
			}
			else $this->file[extension] = $c[extension];
			
			// Thumb
			$this->file[thumb] = $c[thumb];
			
			// Type
			if($this->v('file_type')) $c[type] = $this->v('file_type'); // Overwrites the passed 'type'. Do this so if we get type from db variable (column = db.image, type = image) we will still use the actual type.
			if(!$c[type]) $c[type] = file::type($this->file[name]);
			$this->file[type] = $c[type];
			
			// Path / storage
			if($c[extension] and $this->file[extensions]) {
				$this->file[storage] = $this->file[extensions][$c[extension]][storage];
				$this->file[path] = $this->file[extensions][$c[extension]][path];
				if($this->file[extensions][$c[extension]][name]) $this->file[name] = $this->file[extensions][$c[extension]][name];
				if(!$this->file[path] and $this->file[storage] == "local") $this->file[path] = g('config.uploads.types.'.$this->file[type].'.extensions.'.$c[extension].'.path');
			}
			else if($c[thumb] and $this->file[thumbs]) {
				$this->file[storage] = $this->file[thumbs][$c[thumb]][storage];
				$this->file[path] = $this->file[thumbs][$c[thumb]][path];
				if($this->file[thumbs][$c[thumb]][name]) $this->file[name] = $this->file[thumbs][$c[thumb]][name];
				if(!$this->file[path] and $this->file[storage] == "local") $this->file[path] = g('config.uploads.types.'.$this->file[type].'.thumbs.'.$c[thumb].'.path');
				$this->file[type] = 'image';
			}
			else {
				$this->file[storage] = $this->v('file_storage');
				$this->file[path] = $this->v('file_path');
				if(!$this->file[path] and $this->file[storage] == "local") $this->file[path] = g('config.uploads.types.'.$this->file[type].'.path');
			}
			if($this->file[path] and substr($this->file[path],-1) != "/") $this->file[path] .= "/";
			
			// Class
			$_c = $c;
			$_c[storage] = $this->file[storage];
			$this->file['class'] = file::load($this->file[path].$this->file[name],$_c);
			
			// Default
			if(!$this->file['class']->exists and $c['default']) {
				// Type
				if(!$c[type]) $c[type] = file::type($this->file[name]);
				if($c[type] == "image") {
					// Dimensions
					if($c[thumb]) {
						$image_c[width] = m($this->module.'.uploads.types.'.$c[type].'.thumbs.'.$c[thumb].'.width');
						$image_c[height] = m($this->module.'.uploads.types.'.$c[type].'.thumbs.'.$c[thumb].'.height');
					}
					if(!$image_c[width]) $image_c[width] = 100;
					if(!$image_c[height]) $image_c[height] = 75;
					
					// No Image
					$none = str_replace(DOMAIN,SERVER,file::image_none($image_c));
					
					// Class
					$this->file['class'] = file::load($none);
					// Name
					$this->file[name] = $this->file['class']->name;
					// Path
					$this->file[path] = str_replace($this->file[name],'',$none);
				}
			}
		}
		
		/**
		 * Deletes plugin row for the current plugin item.
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function delete($c = NULL) {
			if(m($this->module.'.settings.plugins.files.keep')) { // Bit of a hacky solution for being able to keep files on server (in case cache issues are arriving or something)
				debug("this module wants us to keep files on the server, even if the item's deleted",$c[debug]);
			}
			else {
				// Used by another item? Might be the case if we have 'browse' setup for a file input, meaning we can select previously uploaded files
				$query = "
				SELECT
					*
				FROM
					".plugin($this->plugin.'.db.table')."
				WHERE
					file_path = '".a($this->v('file_path'))."'
					AND file_name = '".a($this->v('file_name'))."'
					AND file_storage = '".a($this->v('file_storage'))."'
					AND (
						".plugin($this->plugin.'.db.parent_module')." != '".$this->db('parent_module')."'
						OR ".plugin($this->plugin.'.db.parent_id')." != '".$this->db('parent_id')."'
					)";
				$row0 = $this->db->f($query);
				debug("checking if file is used by another item: ".$query,$c[debug]);
				
				// Nope, let's delete the file
				if(!$row0[id]) {
					debug("It isn't, we can delete the files",$c[debug]);
					
					// Delete - file
					$file = file::load($this->v('file_path').$this->v('file_name'),array('storage' => $this->v('file_storage'),'debug' => $c[debug]));
					$file->delete();
					
					// Delete - extensions
					if($this->v('file_extensions')) {
						$extensions = data_unserialize($this->v('file_extensions'));
						foreach($extensions as $k => $v) {
							$file = file::load($v[path].$v[name],array('storage' => $v[storage],'debug' => $c[debug]));
							$file->delete();
						}
					}
					// Delete - thumbs
					if($this->v('file_thumbs')) {
						$thumbs = data_unserialize($this->v('file_thumbs'));
						foreach($thumbs as $k => $v) {
							// Delete - thumbs - video
							if(!$v[name] and is_array($v)) {
								foreach($v as $k0 => $v0) {
									$file = file::load($v0[path].$v0[name],array('storage' => $v0[storage],'debug' => $c[debug]));
									$file->delete();
								}
							}
							// Delete - thumbs - image
							else {
								$file = file::load($v[path].$v[name],array('storage' => $v[storage],'debug' => $c[debug]));
								$file->delete();
							}
						}
					}
				}
				// Yes, exists in another item, don't delee
				else {
					debug("This file is used by another item, don't delete",$c[debug]);
				}
			}
			
			// Parent
			parent::delete($c);
		}
		
		/**
		 * Gets the dimensions of the file.
		 *
		 * @return int The file's width.
		 */
		function dimensions() {
			// Error
			if(!$this->file['class']->exists) return;
			
			// Saved - extension
			if($this->file[extension] and $this->file[extensions][$this->file[extension]][width] and $this->file[extensions][$this->file[extension]][height]) {
				$dimensions = array(
					'width' => $this->file[extensions][$this->file[extension]][width],
					'height' => $this->file[extensions][$this->file[extension]][height]
				);
			}
			// Saved - thumb
			else if(
				$this->file[thumb]
				and is_array($this->file[thumbs])
				and $this->file[thumbs][$this->file[thumb]][width]
				and $this->file[thumbs][$this->file[thumb]][height]
			) {
				$dimensions = array(
					'width' => $this->file[thumbs][$this->file[thumb]][width],
					'height' => $this->file[thumbs][$this->file[thumb]][height]
				);
			}
			// Saved - original
			else if($this->v('file_width') and $this->v('file_height')) {
				$dimensions = array(
					'width' => $this->v('file_width'),
					'height' => $this->v('file_height')
				);
			}
			// Get
			else {
				$dimensions = $this->file['class']->dimensions();
			}
			
			// Return
			return $dimensions; 	
		}
		
		/**
		 * Gets the width of the file.
		 *
		 * @return int The file's width.
		 */
		function width() {
			$dimensions = $this->dimensions();
			return $dimensions[width]; 	
		}
		
		/**
		 * Gets the width of the file.
		 *
		 * @return int The file's width.
		 */
		function height() {
			$dimensions = $this->dimensions();
			return $dimensions[height]; 	
		}
		
		/**
		 * Gets the length of the file.
		 *
		 * @return int The file's length.
		 */
		function length() {
			// Error
			if(!$this->file['class']->exists) return;
			
			// Saved - extension
			if($this->file[extension] and $value = $this->file[extensions][$this->file[extension]][length]) {
				$length = $value;
			}
			// Saved - thumb
			else if($this->file[thumb] and $value = $this->file[thumbs][$this->file[thumb]][length]) {
				$length = $value;
			}
			// Saved - original
			else if($value = $this->v('file_length')) {
				$length = $value;
			}
			// Get
			else {
				$length = $this->file['class']->length();
			}
			
			// Return
			return $length; 	
		}
		
		/**
		 * Gets the size of the file.
		 *
		 * @return int The file's size.
		 */
		function size() {
			// Error
			if(!$this->file['class']->exists) return;
			
			// Saved - extension
			if($this->file[extension] and $value = $this->file[extensions][$this->file[extension]][size]) {
				$size = $value;
			}
			// Saved - thumb
			else if($this->file[thumb] and $value = $this->file[thumbs][$this->file[thumb]][size]) {
				$size = $value;
			}
			// Saved - original
			else if($value = $this->v('file_size')) {
				$size = $value;
			}
			// Get
			else {
				$size = $this->file['class']->size();
			}
			
			// Return
			return $size; 	
		}
		
		/**
		 * Returns HTML for displaying video file.
		 *
		 * Basically just a way for us to autofill some configuration values before calling up the $file->video_html() method.
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 * @return string The HTML for displaying the video file.
		 */
		function video_html($c = NULL) {
			// Error
			if(!$this->file['class']->exists) return;
			
			// Config
			if(!$c[width]) $c[width] = $this->width();
			if(!$c[height]) $c[height] = $this->height();
			if(!$c[fallback]) $c[fallback] = $this->file[fallback];
			
			// HTML
			$html = $this->file['class']->video_html($c);
			
			// Return
			return $html; 	
		}
		
		
		/**
		 * Returns an array of thumb data for (optionally) the given thumb key (size).
		 *
		 * @param string $thumb The key of the thumbnail(s) you want to get information on. Usually t, s, m, l, or o. Deafult = NULL
		 * @return array An array of information about the thumbnail(s).
		 */
		function thumbs($thumb = NULL) {
			// Error
			if(!$this->file['class']->exists) return;
			
			// Thumbs
			$thumbs = $this->file[thumbs];
			if($thumb) $thumbs = $thumbs[$thumb];
			
			// Return
			return $thumbs; 	
		}
		
		/**
		 * Returns an array of thumb data for the file's primary thumb.
		 *
		 * @param string $thumb The key of the thumbnail size you want to get information on. Usually t, s, m, l, or o. Deafult = NULL
		 * @return array An array of information about the thumbnail(s).
		 */
		function thumb($thumb = "o") {
			// Error
			if(!$this->file['class']->exists) return;
			
			// Thumbs
			$thumbs = $this->file[thumbs][$thumb];
			if($thumbs) {
				foreach($thumbs as $k => $v) {
					if($k == $this->v('file_thumb')) {
						$array = $v;
						$array[key] = $k;
						break;	
					}
				}
			}
			
			// Return
			return $array; 	
		}
		
		/**
		 * Returns URL of the file's primary thumb in the given $thumb size.
		 *
		 * @param string $thumb The key of the thumbnail size you want to get the file URL of. Usually t, s, m, l, or o. Deafult = NULL
		 * @return string The URL of the file's thumbnail in the given $thumb size.
		 */
		function thumb_url($thumb = "o") {
			// Thumb
			if($thumb = $this->thumb($thumb)) {
				// File
				$file = file::load($thumb[path].$thumb[name],array('storage' => $thumb[storage]));
				// URL
				$url = $file->url();
			}
			// Return
			return $url; 	
		}
		
		/**
		 * Analyzes this file and returns array of data about it which can be saved in the plugin's database.
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of data about this file.
		 */
		function values($c = NULL) {
			$name = $this->v('file_name');
			$type = $this->v('file_type');
			return parent::values($name,$type,$c);
		}
		
		/**
		 * Catches all calls to instance that don't have a method and redirects them to the file class method (if it exists).
		 *
		 * @param string $method The method that was called.
		 * @param array $params An array of params called.
		 * @return mixed The method's return value.
		 */
		function __call($method,$params) {
			// Error
			if(!$this->file['class']->exists) return;
			
			// Method
			if(method_exists($this->file['class'],$method)) {
				// Call
				return call_user_func_array(array($this->file['class'],$method),$params);
			}
		}
		
		/**
		 * Catches all get calls to instance that don't have a variable and redirects them to the file class variable (if it exists).
		 *
		 * @param string $key The variable key that we're trying to 'get'.
		 * @return mixed The key's value in the file class.
		 */
		function __get($key) {
			// Error
			if(!$this->file['class']->exists) return;
			
			// Value
			return $this->file['class']->$key;
		}
	}
}
?>