<?php
if(!class_exists('payment_paypal_pro',false)) {
	/**
	 * An 'extension' of the payment class used for processing payments through PayPal Payments Pro.
	 *
	 * For basic PayPal payments, use the payment_paypal class.
	 *
	 * Notes: 
	 * - When testing, you can not use the usual 4111111111111111 credit card number. Try using your test credit card number (see below).
	 * 
	 * Custom variables: 1
	 *
	 * Test account (http://developer.paypal.com/):
	 * - user: charli_1351098011_biz_api1.dwstudios.net
	 * - password: 1351098057
	 * - signature: Athpch1HapgNdauj1tgkujVJDc29AUMkGfCjbuMTk0KrgmvsBWsqmESz 
	 * - Other:
	 *   - E-mail: charli_1351098011_biz@dwstudios.net
	 *   - Password: 178y1291
	 *   - Credit Card Number: 4264681480834282 (you should use this credit card number when processing test transactions)
	 *   - Credit Card Expiration: 10/2017
	 *
	 * Reference:
	 * - https://cms.paypal.com/us/cgi-bin/?cmd=_render-content&content_ID=developer/e_howto_api_WPWebsitePaymentsPro
	 * - https://cms.paypal.com/us/cgi-bin/?cmd=_render-content&content_ID=developer/howto_api_reference
	 *
	 * To do
	 * - Return 'custom' variables in returned results (if possible)
	 * - check for required fields before sending?
	 * 
	 * Dependencies
	 * - Functions
	 *   - x() - __construct()
	 *
	 * @package kraken\payments
	 */
	class payment_paypal_pro {
		/** Stores the user name for your PayPal Payments Pro account. */
		public $user;
		/** Stores the password for your PayPal Payments Pro account. */
		public $password;
		/** Stores the signature for your PayPal Payments Pro account. */
		public $signature;
		/** Stores an array of configuration values passed to the class. */
		public $c;
		
		/**
		 * Constructs the class.
		 *
		 * Configuration values (key, type, default - description):
		 * - test, boolean, 0 - Whether or not we want to process transactions via PayPal testing 'sandbox'.
		 *
		 * @param string $user The user name of your PayPal Payments Pro account.
		 * @param string $password The password of your PayPal Payments Pro account.
		 * @param string $signature The signature of your PayPal Payments Pro account.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function __construct($user,$password,$signature,$c = NULL) {
			self::payment_paypal_pro($user,$password,$signature,$c);
		}
		function payment_paypal_pro($user,$password,$signature,$c = NULL) {
			// User
			$this->user = $user;
			// Password
			$this->password = $password;
			// Signature
			$this->signature = $signature;
			
			// Config
			if(!x($c[test])) $c[test] = 0;
			$this->c = $c;
		}
		
		/**
		 * Authorizes the customer's account for given amount.
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction including 'result' (boolean, 1 = succes, 0 = error) and 'message'.
		 */
		function authorize($payment,$c = NULL) {
			// Common errors
			if($error_common = $this->errors(__FUNCTION__)) {
				$error = $error_common;
			}
			// No amount passed
			else if(!$payment->amount) {
				$error = "No authorization amount was passed.";
			}
			// No credit card
			else if(!$payment->card[number]) {
				$error = "No credit card was passed for us to authorize.";	
			}
			
			// Error
			if($error) {
				$results = array(
					'result' => 0,
					'message' => $error
				);	
			}
			// Transaction
			else {
				// Send
				$results = $this->send($payment,"authorize",$c);
			}
			
			// Return
			return $results;
		}
		
		/**
		 * Captures a transaction that was previously authorized via the authorize() method.
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction including 'result' (boolean, 1 = succes, 0 = error) and 'message'.
		 */
		function capture($payment,$c = NULL) {
			// Common errors
			if($error_common = $this->errors(__FUNCTION__)) {
				$error = $error_common;
			}
			// No transaction ID
			else if(!$payment->transaction) {
				$error = "No transaction ID was passed that we could cancel.";
			}
			// No amount passed
			else if(!$payment->amount) {
				$error = "No capture amount was passed.";
			}
			
			// Error
			if($error) {
				$results = array(
					'result' => 0,
					'message' => $error
				);	
			}
			// Transaction
			else {
				// Send
				$results = $this->send($payment,"capture",$c);
			}
			
			// Return
			return $results;
		}
		
		/**
		 * Charges the customer a given amount.
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction including 'result' (boolean, 1 = succes, 0 = error) and 'message' as well as the transaction id ('transaction').
		 */
		function charge($payment,$c = NULL) {
			// Common errors
			if($error_common = $this->errors(__FUNCTION__)) {
				$error = $error_common;
			}
			// No amount passed
			else if(!$payment->amount) {
				$error = "No charge amount was passed.";
			}
			// No credit card
			else if(!$payment->card[number]) {
				$error = "No credit card was passed for us to charge.";	
			}
			
			// Error
			if($error) {
				$results = array(
					'result' => 0,
					'message' => $error
				);
			}
			// Transaction
			else {
				// Send
				$results = $this->send($payment,"charge",$c);
			}
			
			// Return
			return $results;
		}
		
		/**
		 * Refunds a customer/account a defined amount on a previous transaction.
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction including 'result' (boolean, 1 = succes, 0 = error) and 'message'.
		 */
		function refund($payment,$c = NULL) {
			// Common errors
			if($error_common = $this->errors(__FUNCTION__)) {
				$error = $error_common;
			}
			// No transaction ID
			else if(!$payment->transaction) {
				$error = "No transaction ID was passed for the transaction we want to refund.";
			}
			// No amount passed
			else if(!$payment->amount) {
				$error = "No refund amount was passed.";
			}
			
			// Error
			if($error) {
				$results = array(
					'result' => 0,
					'message' => $error
				);	
			}
			// Transaction
			else {
				// Send
				$results = $this->send($payment,"refund",$c);
			}
			
			// Return
			return $results;
		}
		
		/**
		 * Cancels/voids the given transaction.
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction including 'result' (boolean, 1 = succes, 0 = error) and 'message'.
		 */
		function cancel($payment,$c = NULL) {
			// Common errors
			if($error_common = $this->errors(__FUNCTION__)) {
				$error = $error_common;
			}
			// No transaction ID
			else if(!$payment->transaction) {
				$error = "No transaction ID was passed that we could cancel.";
			}
			
			// Error
			if($error) {
				$results = array(
					'result' => 0,
					'message' => $error
				);	
			}
			// Transaction
			else {
				// Send
				$results = $this->send($payment,"cancel",$c);
			}
			
			// Return
			return $results;
		}
		
		/**
		 * Credits a customer the given amount.
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction including 'result' (boolean, 1 = succes, 0 = error) and 'message'.
		 */
		function credit($payment,$c = NULL) {
			// Common errors
			if($error_common = $this->errors(__FUNCTION__)) {
				$error = $error_common;
			}
			// No amount passed
			else if(!$payment->amount) {
				$error = "No credit amount was passed.";
			}
			// No credit card
			else if(!$payment->card[number]) {
				$error = "No credit card was passed for us to credit.";	
			}
			
			// Error
			if($error) {
				$results = array(
					'result' => 0,
					'message' => $error
				);	
			}
			// Transaction
			else {
				// Remove transaction - must NOT be submitted for this to work (if we want to refund on a specific transaction we use the refund() method).
				$payment->transaction = NULL;
				// Send
				$results = $this->send($payment,"credit",$c);
			}
			
			// Return
			return $results;
		}
		
		/**
		 * Sends various 'transactions' to PayPal API.
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param string $action The transaction action we're performing.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction including 'result' (boolean, 1 = succes, 0 = error) and 'message' as well as the transaction id ('transaction').
		 */
		function send($payment,$action,$c = NULL) {
			// Standardize
			$payment = $this->standardize($payment);
			
			// URL
			if($this->c[test]) $url = "https://api-3t.sandbox.paypal.com/nvp";
			else $url = "https://api-3t.paypal.com/nvp";
			
			// Method
			$method = NULL;
			if($action == "authorize") $method = "DoDirectPayment";
			if($action == "capture") $method = "DoCapture";
			if($action == "charge") $method = "DoDirectPayment";
			if($action == "refund") $method = "RefundTransaction";
			if($action == "cancel") $method = "DoVoid";
			if($action == "credit") $method = "DoNonReferencedCredit";
			
			// Valuues - common
			$values_common = array(
				'USER' => $this->user,
				'PWD' => $this->password,
				'SIGNATURE' => $this->signature,
				'VERSION' => '95.0',
				'METHOD' => $method,
				'IPADDRESS' => $payment->ip(),
				'DESCRIPTION' => $payment->description,
				'INVNUM' => $payment->invoice,
				'CUSTOM' => $payment->custom,
			);
			
			// Values - total
			$values_total = array(
			   'AMT' => $payment->amount,
			   'ITEMAMT' => $payment->total[subtotal],
			   'SHIPPINGAMT' => $payment->total[shipping],
			   'INSURANCEAMT' => $payment->total[insurance],
			   'HANDLINGAMT' => $payment->total[handling],
			   'TAXAMT' => $payment->total[tax],
			   'CURRENCYCODE' => $payment->currency,
			);
			
			// Values - transaction
			$values_transaction = array();
			if($payment->transaction) {
				$values_transaction = array(
					'AUTHORIZATIONID' => $payment->transaction
				);
			}
				
			// Values - address
			$values_address = array();
			if($payment->address) {
				$values_address = array(
				   'FIRSTNAME' => $payment->address[first_name],
				   'LASTNAME' => $payment->address[last_name],
				   'EMAIL' => $payment->address[email],
				   'COUNTRYCODE' => $payment->address[country],
				   'STATE' => $payment->address[state],
				   'CITY' => $payment->address[city],
				   'STREET' => $payment->address[address],
				   'STREET2' => $payment->address[address_2],
				   'ZIP' => $payment->address[zip],
				);
			}
			
			// Values - card
			$values_card = array();
			if($payment->card) {
				$values_card = array(
				   'CREDITCARDTYPE' => $payment->card[type],
				   'ACCT' => $payment->card[number],
				   'EXPDATE' => $payment->card[expiration],
				   'CVV2' => $payment->card[code],
				);
			}
			
			// Values - shipping
			$values_shipping = array();
			if($payment->shipping) {
				$values_shipping = array(
				   'SHIPTONAME' => $payment->shipping[first_name]." ".$payment_shipping[last_name],
				   'SHIPTOSTREET' => $payment->shipping[address],
				   'SHIPTOSTREET2' => $payment->shipping[address_2],
				   'SHIPTOCITY' => $payment->shipping[city],
				   'SHIPTOSTATE' => $payment->shipping[state],
				   'SHIPTOZIP' => $payment->shipping[zip],
				   'SHIPTOCOUNTRYCODE' => $payment->shipping[country],
				   'SHIPTOPHONENUM' => $payment->shipping[phone],
				);
			}
			
			// Values - phone - only a 'SHIPTOPHONENUM', but use address phone number if no shipping phone number
			if(!$values_shipping[SHIPTOPHONENUM]) $values_shipping[SHIPTOPHONENUM] = $payment->address[phone];
			
			// Values
			$values = array_merge($values_common,$values_total,$values_transaction,$values_address,$values_card,$values_shipping);
			
			// Values - authorize
			if($action == "authorize") {
				$values[PAYMENTACTION] = "Authorization";
			}
			// Values - capture
			if($action == "capture") {
				$values[COMPLETETYPE] = "NotComplete"; // Could be 'Complete' but we don't know original amount so we don't know if if this is full capture, so we'll just say 'NotComplete' for now.
			}
			// Values - charge
			if($action == "charge") {
				$values[PAYMENTACTION] = "Sale";
			}
			// Values - refund
			if($action == "refund") {
				$values[REFUNDTYPE] = "Partial"; // Could be 'Full' but we don't know original amount so we'll just say it's Partial	and pass our 'AMT'.
				$values[TRANSACTIONID] = $payment->transaction; // Refund takes the transaction id in 'TRANSACTIONID', not 'AUTHORIZATIONID' (like refund & cancel use).
			}
	
			// Data
			$data = http_build_query($values);
		
			// Send
			$response = $payment->curl($url,$data);
	
			// Debug
			$payment->debug("values: ".return_array($values),$c[debug]);
			$payment->debug("data: ".$url."?".$data,$c[debug]);
			$payment->debug("response: ".$response,$c[debug]);
			
			// Parse
			$response_parsed = array();
			parse_str($response,$response_parsed);
			
			// Result
			$results = NULL;
			$results[result] = (in_array($response_parsed[ACK],array('Success'/*,'SuccessWithWarning'*/)) ? 1 : 0); // Not going to include SuccessWithWarning for now
			$results[message] = $response_parsed[L_LONGMESSAGE0];
			$results[transaction] = $response_parsed[TRANSACTIONID];
			$results[test] = $this->c[test];
			$results[source] = "PayPal";
			$results[response] = $response;
			
			// Return
			return $results;
		}
		
		/**
		 * Parses the response sent by PayPal after a 'basic' payment.
		 *
		 * You should call this up on your 'url_notify' (which you will have set in the $c array when calling $this->url().  Example:
		 *
		 * $payment = new payment();
		 * $payment->gateway(new payment_paypal_pro($user));
		 * $results = $payment->response($_POST);
		 *
		 * @param array $array The array of information sent back by PayPal. Usually this is sent via $_POST.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction including 'result' (boolean, 1 = succes, 0 = error) and 'message' as well as the transaction id ('transaction').
		 */
		function response($array,$c = NULL) {
			// Results
			$results[result] = ($array[payment_status] == "Completed" ? 1 : 0);
			$results[message] = $array[payment_status]; // Would like something better, but don't know what.
			$results[transaction] = $array[txn_id];
			$results[source] = "PayPal";
			$results[response] = $array;
			
			// Return		
			return $results;
		}
		
		/**
		 * Standardizes some of the values used in PayPal.
		 *
		 * @param object $payment The payment object we want to standize the values of.
		 * @return object The standardized payment object.
		 */
		function standardize($payment) {
			// Error
			if(!$payment) return;
			
			// Amount - must include decimals, can optionally include commas in thousands, but I'm leaving it off for now
			if($payment->amount) {
				$payment->amount = number_format($payment->amount,2,'.','');
			}
			
			// Shipping - set to address if nothing set
			$skip = NULL;
			if($payment->shipping[first_name]) $skip[] = "last_name";
			if($payment->shipping[address]) {
				$skip[] = "address_2";
				$skip[] = "company";
				$skip[] = "city";
				$skip[] = "state";
				$skip[] = "zip";
				$skip[] = "country";
			}
			if($payment->address) {
				foreach($payment->address as $k => $v) {
					if(!$payment->x($payment->shipping[$k]) and !in_array($k,$skip)) $payment->shipping[$k] = $v;
				}
			}
			
			// Card expiration - MMYYYY
			if($payment->card[expiration_month] and $payment->card[expiration_year]) {
				$payment->card[expiration] = str_pad($payment->card[expiration_month],2,"0",STR_PAD_LEFT).$payment->card[expiration_year];
			}
			
			// Custom - 256 max, alpha-numeric
			if($payment->custom[0]) {
				$payment->custom[0] = preg_replace("/[^a-zA-Z0-9\s]/","",$payment->custom[0]);
				$payment->custom[0] = substr($payment->custom[0],0,256);
			}
			
			// Return
			return $payment;
		}
		
		/**
		 * Detects some common errors for this gateway and returns the error message.
		 *
		 * @param string $function The function from which we called this method.
		 * @return string The error message (if an error was detected).
		 */
		function errors($function) {
			// No login credentials
			if(!$this->user or !$this->password or !$this->signature) {
				$error = "The login credentials for PayPal Payments Pro are missing.";
			}
			
			// Return
			return $error;
		}
	}
}
?>