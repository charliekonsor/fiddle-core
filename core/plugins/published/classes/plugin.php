<?php
if(!class_exists('plugin_published',false)) {
	/**
	 * An extenion of the plugin class which handles functionality specific to the published plugin.
	 *
	 * @package kraken\plugins\published
	 */
	class plugin_published extends plugin {
		/**
		 * Constructs the class.
		 *
		 * @param string $module The module of the item the plugin is acting upon.
		 * @param int $id The id of the item the plugin is acting upon. Default = NULL
		 * @param string $plugin The plugin we're using.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function __construct($module,$id = NULL,$plugin,$c = NULL) {
			$this->plugin_published($module,$id,$plugin,$c);
		}
		function plugin_published($module,$id = NULL,$plugin,$c = NULL) {
			parent::__construct($module,$id,$plugin,$c);
		}
		
		/**
		 * Returns array of inputs for use in the sidebar box for this plugin.
		 *
		 * @param object $form The form object we'll be adding these inputs to.
		 * @return array An array of inputs to add to the form.
		 */
		function sidebar($form) {
			// Random
			$r = random();
			
			// New method - store in module's table
			// Published
			if($name = m($this->module.'.db.published')) {
				$inputs[] = array(
					'label' => 'Published',
					'type' => 'checkbox',
					'name' => $name,
					'options' => array(
						1 => 'Yes',
					),
					'default' => 1,
					'value' => (x($this->item->v($name)) ? $this->item->v($name) : 1), // Default (above) should work most of the time, but I think it won't use the default if the item (menus_item item_id) already exists. So, just to be sure.
					'id' => 'published-'.$r
				);
			}
			// Start
			if($name = m($this->module.'.db.published_start')) {
				$inputs[] = array(
					'label' => 'Start Publication',
					'type' => 'datetime',
					'name' => $name,
					'value' => $this->item->v($name),
					'row' => array(
						'class' => 'published_row',
						'style' => (m($this->module.'.db.published') ? "display:none;" : ""),
					)
				);
			}
			// End
			if($name = m($this->module.'.db.published_end')) {
				$inputs[] = array(
					'label' => 'End Publication',
					'type' => 'datetime',
					'name' => $name,
					'value' => $this->item->v($name),
					'row' => array(
						'class' => 'published_row',
						'style' => (m($this->module.'.db.published') ? "display:none;" : ""),
					)
				);
			}
			
			// Javascript
			if(m($this->module.'.db.published')) {
				$javascript = "
	<script type='text/javascript'>
	$(document).ready(function() {
		toggleSibling('#published-".$r."',1,'.published_row',1);
		
		$('#published-".$r."').click(function() {
			toggleSibling('#published-".$r."','1','.published_row',1);
		});
	});
	</script>";
				$inputs[] = array(
					'type' => 'html',
					'value' => $javascript
				);
			}
			
			// Old method - store in separate table
			/*// Saved
			if(plugin($this->plugin.'.db.table') and plugin($this->plugin.'.db.parent_module') and plugin($this->plugin.'.db.parent_id')) {
				$row = $this->db->f("SELECT * FROM ".plugin($this->plugin.'.db.table')." WHERE ".plugin($this->plugin.'.db.parent_module')." = '".$this->module."' AND ".plugin($this->plugin.'.db.parent_id')." = '".$this->id."'"); 
			}
			
			// Published
			$inputs[] = array(
				'label' => 'Published',
				'type' => 'checkbox',
				'name' => 'published',
				'options' => array(
					1 => 'Yes',
				),
				'default' => 1,
				'value' => (x($row[published]) ? $row[published] : 1), // Default should work most of the time, but I think it won't use the default if the item (menus_item item_id) already exists. So, just to be sure.
				'id' => 'published-'.$r
			);
			// Start
			$inputs[] = array(
				'label' => 'Start Publication',
				'type' => 'datetime',
				'name' => 'published_start',
				'value' => $row[published_start],
				'row' => array(
					'class' => 'published_row',
					'style' => 'display:none;'
				)
			);
			// End
			$inputs[] = array(
				'label' => 'End Publication',
				'type' => 'datetime',
				'name' => 'published_end',
				'value' => $row[published_end],
				'row' => array(
					'class' => 'published_row',
					'style' => 'display:none;'
				)
			);
			// Javascript
			$javascript = "
<script type='text/javascript'>
$(document).ready(function() {
	toggleSibling('#published-".$r."',1,'.published_row',1);
	
	$('#published-".$r."').click(function() {
		toggleSibling('#published-".$r."','1','.published_row',1);
	});
});
</script>";
			$inputs[] = array(
				'type' => 'html',
				'value' => $javascript
			);*/
			
			// Return
			if($inputs) {
				$return = array(
					'label' => plugin($this->plugin.'.name'),
					'inputs' => $inputs
				);
				return $return;
			}
		}
			
		/** 
		 * Determines if an item is visible to the public or not.
		 *
		 * @param array $c An array of configuration values. Deafult = NULL
		 * @return boolean Whether or not the item is visible to the public.
		 */
		function item_visible_check($c) {
			// Visible by default
			$visible = 1;
			
			// New method - store in module's table
			// Publication
			if($visible and m($this->module.'.db.published') and x($this->item->db('published')) and !$this->item->db('published')) {
				$visible = 0;
			}
			// Publication - Start
			if($visible and m($this->module.'.db.published_start') and $this->item->db('published_start') > 0) {	
				if($this->item->db('published_start') > time_format(time())) {
					$visible = 0;	
				}
			}
			// Publication - End
			if($visible and m($this->module.'.db.published_end') and $this->item->db('published_end') > 0) {	
				if($this->item->db('published_end') < time_format(time())) {
					$visible = 0;	
				}
			}
			
			// Old method - store in separate table
			/*// Row
			$row = $this->db->f($this->rows());
			if($row) {
				// Item
				$plugin_item = $this->plugin_item($row);
			
				// Publication
				if($visible and !$plugin_item->v('published')) {
					$visible = 0;
				}
				// Publication - Start
				if($visible and $plugin_item->v('published_start') > 0) {	
					if($plugin_item->v('published_start') > time_format(time())) {
						$visible = 0;	
					}
				}
				// Publication - End
				if($visible and $plugin_item->v('published_end') > 0) {	
					if($plugin_item->v('published_end') < time_format(time())) {
						$visible = 0;	
					}
				}
				
				// Save
				$query = "UPDATE ".plugin($this->plugin.'.db.table')." SET published_value = '".$visible."' WHERE ".plugin($this->plugin.'.db.parent_module')." = '".$this->module."' AND ".plugin($this->plugin.'.db.parent_id')." = '".$this->id."'";
				$this->db->q($query); 
			}*/
			
			// Return
			return $visible;
		}
	}
}
?>