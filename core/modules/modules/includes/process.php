<?php
if(!function_exists('package_install')) {
	/**
	 * Installs a package.
	 *
	 * @param string $package The 'code' of the package you want to install.
	 * @param string $path The path to the source folder for the package.
	 * @param string $path_save The path where you want to save the new package.
	 * @param array $array An array of configuration values. Default = NULL
	 */
	function package_install($package,$path,$path_save,$c = NULL) {
		// Error
		if(!$package or !$path or !$path_save) return;
		
		// Global
		global $install;
		
		// Config
		if(!x($c['parent'])) $c['parent'] = NULL; // Parent package's code
		if(!x($c[debug])) $c[debug] = 0; // Debug
		
		// Count
		$install[packages][count] += 1;
		
		// Already installed
		if(package($package)) $install[packages][previous] += 1;
		// Parse
		else {
			// Plugin path
			$path_save .= $package."/";
			// Move from temp
			if($path != $path_save) rename($path,$path_save);
			
			// Debug
			debug("package: ".$package,$c[debug]);
			debug("package path: ".$path_save,$c[debug]);
			debug("package items:".return_array(file::directory_items($path_save,array('recursive' => 1))),$c[debug]);
			
			// Install
			if(file_exists($path_save."config.php")) {
				// Success
				$install[packages][success] += 1;
			}
			// Plugin incomplete
			else {
				$install[packages][error] += 1;
			}
		}
	}
}

if(!function_exists('module_install')) {
	/**
	 * Installs a module.
	 *
	 * @param string $module The 'code' of the module you want to install.
	 * @param string $path The path to the source folder for the module.
	 * @param string $path_save The path where you want to save the new module.
	 * @param array $array An array of configuration values. Default = NULL
	 */
	function module_install($module,$path,$path_save,$c = NULL) {
		// Error
		if(!$module or !$path or !$path_save) return;
		
		// Global
		global $install;
		
		// Config
		if(!x($c['parent'])) $c['parent'] = NULL; // Parent module's code
		if(!x($c[rows])) $c[rows] = 0; // We're only importing the plugin's 'rows', not installing it fully
		if(!x($c[debug])) $c[debug] = 0; // Debug
		
		// Count
		$install[modules][count] += 1;
		
		// Database
		$db = db::load();
		
		// Already installed
		if(m($module) and !$c[rows]) $install[modules][previous] += 1;
		// Parse
		else {
			// Items
			$items = file::directory_items($path,array('recursive' => 1));
			
			// Module path
			$path_save .= $module."/";
			// Move from temp
			if($path != $path_save) {
				if(file_exists($path_save)) {
					foreach($items as $k => $v) {
						rename($path."/".$v,$path_save."/".$v);
					}
				}
				else rename($path,$path_save);
			}
			
			// Debug
			debug("module: ".$module,$c[debug]);
			debug("module path: ".$path_save,$c[debug]);
			debug("module items:".return_array($items),$c[debug]);
			debug("rows: ".$c[rows],$c[debug]);
			
			// Install
			if(file_exists($path_save."config.php") or $c[rows]) {
				if(!$c[rows]) {
					// Config
					include $path_save."config.php";
					$name = ($config[name] ? $config[name] : ucwords($module));
					debug("config:".return_array($config),$c[debug]);
					
					// Module row
					$values = array(
						'module_name' => $name,
						'module_code' => $module,
						'parent_module_code' => $c['parent'],
						'module_visible' => 1
					);
					$install[queries][] = "INSERT INTO modules ".$db->values($values);
				}
				
				// Queries
				debug($path_save."db.sql",$c[debug]);
				if(file_exists($path_save."db.sql")) {
					debug("it exists",$c[debug]);
					$queries = unserialize(file_get_contents($path_save."db.sql"));
					debug("queries: ".return_array($queries),$c[debug]);
					if($queries) {
						foreach($queries as $query) {
							$install[queries][] = $query;	
						}
					}
					unlink($path_save."db.sql");
				}
				
				if(!$c[rows]) {
					// Permissions
					$install[modules][permissions][$module] = $config[permissions];
					if(!$install[modules][permissions][$module]) $install[modules][permissions][$module] = $config_global[permissions];
					
					// Child modules
					if($children = file::directory_folders($path_save."modules/")) {
						foreach($children as $child) {
							module_install($child,$path_save."modules/".$child."/",$path_save."modules/",array('parent' => $module));
						}
					}
					
					// Types
					if($types = file::directory_folders($path_save."/types/")) {
						foreach($types as $type) {
							if(file_exists($path_save."/types/".$type."/config.php")) {
								// Config
								include $path_save."/types/".$type."/config.php";
								$type_name = ($config[name] ? $config[name] : ucwords($type));
								// Type row
								$values = array(
									'module_code' => $module,
									'type_name' => $type_name,
									'type_code' => $type,
									'type_visible' => 1
								);
								$install[queries][] = "INSERT INTO types ".$db->values($values);
							}
						}
					}
				}
						
				// Success
				$install[modules][success] += 1;
			}
			// Module incomplete
			else {
				$install[modules][error] += 1;
			}
		}
	}
}

if(!function_exists('plugin_install')) {
	/**
	 * Installs a plugin.
	 *
	 * @param string $plugin The 'code' of the plugin you want to install.
	 * @param string $path The path to the source folder for the plugin.
	 * @param string $path_save The path where you want to save the new plugin.
	 * @param array $array An array of configuration values. Default = NULL
	 */
	function plugin_install($plugin,$path,$path_save,$c = NULL) {
		// Error
		if(!$plugin or !$path or !$path_save) return;
		
		// Global
		global $install;
		
		// Config
		if(!x($c[rows])) $c[rows] = 0; // We're only importing the plugin's 'rows', not installing it fully
		if(!x($c[debug])) $c[debug] = 0; // Debug
		
		// Count
		$install[plugins][count] += 1;
		
		// Already installed
		if(plugin($plugin) and !$c[rows]) $install[plugins][previous] += 1;
		// Parse
		else {
			// Items
			$items = file::directory_items($path,array('recursive' => 1));
			
			// Plugin path
			$path_save .= $plugin."/";
			// Move from temp
			if($path != $path_save) {
				if(file_exists($path_save)) {
					foreach($items as $k => $v) {
						rename($path."/".$v,$path_save."/".$v);
					}
				}
				else rename($path,$path_save);
			}
			
			// Debug
			debug("plugin: ".$plugin,$c[debug]);
			debug("plugin path: ".$path_save,$c[debug]);
			debug("plugin items:".return_array($items),$c[debug]);
			debug("rows: ".$c[rows],$c[debug]);
			
			// Install
			if(file_exists($path_save."config.php") or $c[rows]) {
				if(!$c[rows]) {
					// Config
					include $path_save."config.php";
					$name = ($config[name] ? $config[name] : ucwords($plugin));
					debug("config:".return_array($config),$c[debug]);
				}
				
				// Queries
				if(file_exists($path_save."db.sql")) {
					$queries = unserialize(file_get_contents($path_save."db.sql"));
					if($queries) {
						foreach($queries as $query) {
							$install[queries][] = $query;	
						}
					}
					unlink($path_save."db.sql");
				}
				
				if(!$c[rows]) {
					// Permissions
					$install[plugins][permissions][$plugin] = $config[permissions];
					
					// Child plugins
					if($children = file::directory_folders($path_save."plugins/")) {
						foreach($children as $child) {
							plugin_install($child,$path_save."plugins/".$child."/",$path_save."plugins/");
						}
					}
				}
		
				// Success
				$install[plugins][success] += 1;
			}
			// Plugin incomplete
			else {
				$install[plugins][error] += 1;
			}
		}
	}
}

/**
 * Installs a module using the submitted module zip file.
 */
if($_POST['process_action'] == "modules_install") {
	// Form
	$form = new form();
	$results = $form->process(g('unpure.post'),g('unpure.files'));
	if($results[errors]) {
		if($results[c][url] and (!$results[c][debug] or !debug_active())) redirect($results[c][url]);
		else if(debug_active()) print "
<div class='core-red'>
	<b>Errors:</b>
	".return_array($results[errors])."
</div>";
	}
	else {
		$input = $form->input_exists('file');
		
		// Variables
		$path = $input->c[path];
		$path_packages = SERVER."local/packages/";
		$path_modules = SERVER."local/modules/";
		$path_plugins = SERVER."local/plugins/";
		$path_temp = $path."__temp/";
		$install = NULL;
		$debug = 0;
		
		// Debug
		debug("file: ".$results[values][file],$debug);
		debug("path: ".$path,$debug);
		
		// No Permission
		if(!permission('modules','add')) page_error("I'm sorry, but you don't have permission to do this.");
		// Upload Error
		else if(!$results[values][file]) page_error("There was an error uplading this file.");
		// Not writeable
		else if(!file::directory_writeable($path)) page_error("The /".str_replace(SERVER,"",$path)." folder is not writeable. Please make sure it has 777 permissions.",1);
		// Extract
		else {
			// Extract (to temp folder)
			include_function('zip');
			zip_extract($path.$results[values][file],$path_temp);
			$folders = file::directory_folders($path_temp,array('recursive' => 1));
			debug("contents: ".return_array(file::directory_items($path_temp,array('recursive' => 1))),$debug);
		
			// Directories exist and are writeable?
			$folder_error = NULL;
			if($folders[packages]) {
				if(!is_dir($path_packages)) mkdir($path_packages);
				if(!file::directory_writeable($path_packages)) $folder_error = $path_packages;
			}
			if($folders[modules]) {
				if(!is_dir($path_modules)) mkdir($path_modules);
				if(!file::directory_writeable($path_modules)) $folder_error = $path_modules;
			}
			if($folders[plugins]) {
				if(!is_dir($path_plugins)) mkdir($path_plugins);
				if(!file::directory_writeable($path_plugins)) $folder_error = $path_plugins;
			}
			
			// No folders
			if(!$folders) page_error("There was an error extracting this file.",1);
			// Not writeable
			else if($folder_error) page_error("The /".str_replace(SERVER,"",$folder_error)." folder is not writeable. Please make sure it has 777 permissions.",1);
			// Install
			else {
				// Rows only?
				$rows_only = 0;
				if(!$rows_only and $folders[modules]) {
					foreach($folders[modules] as $module => $files) {
						if(file_exists($path_temp."modules/".$module."/db.sql") and !file_exists($path_temp."modules/".$module."/config.php")) {
							$rows_only = 1;
						}
					}
				}
				if(!$rows_only and $folders[plugins]) {
					foreach($folders[plugins] as $plugin => $files) {
						if(file_exists($path_temp."plugins/".$plugin."/db.sql") and !file_exists($path_temp."plugins/".$plugin."/config.php")) {
							$rows_only = 1;
						}
					}
				}
				debug("rows only: ".$rows_only,$debug);
			
				// Packages
				if($folders[packages]) {
					foreach($folders[packages] as $package => $files) {
						package_install($package,$path_temp."packages/".$package."/",$path_packages,array('debug' => $debug));
					}
				}
				
				// Modules
				if($folders[modules]) {
					foreach($folders[modules] as $module => $files) {
						module_install($module,$path_temp."modules/".$module."/",$path_modules,array('debug' => $debug,'rows' => $rows_only));
					}
				}
				
				// Plugins
				if($folders[plugins]) {
					foreach($folders[plugins] as $plugin => $files) {
						plugin_install($plugin,$path_temp."plugins/".$plugin."/",$path_plugins,array('debug' => $debug,'rows' => $rows_only));
					}
				}
				
				// Debug
				debug("install:".return_array($install),$debug);
							
				// Queries
				if($install[queries]) {
					foreach($install[queries] as $query) {
						$core->db->q($query);
					}
				}
				
				if(!$rows_only) {
					// Refresh modules
					modules();
					
					// Permissions
					if($install[modules][permissions]) {
						// User type permissions
						$permissions_types = NULL;
						foreach(m('users.types') as $type => $v) {
							if($type != "supers") $permissions_types[$type] = users_type_permissions($type);
						}
						debug("permissions before:".return_array($permissions_types),$debug);
						
						// Modules
						if($install[modules][permissions]) {
							debug("permissions modules:".return_array($install[modules][permissions]),$debug);
							foreach($install[modules][permissions] as $module => $permissions) {
								foreach($permissions as $permission => $v) {
									if($v['default']) {
										debug($module." => ".$permission,$debug);
										foreach($permissions_types as $type => $type_permissions) {
											if(in_array($type,$v['default'])) {
												debug("giving permission to ".$type." to `".$permission."` in the `".$module."` module",$debug);
												$permissions_types[$type][$module][$permission] = 1;
											}
											else {
												$permissions_types[$type][$module][$permission] = NULL;
											}
										}
									}
								}
							}
						}
						
						// Plugins
						if($install[plugins][permissions]) {
							debug("permissions plugins:".return_array($install[plugins][permissions]),$debug);
							foreach($install[plugins][permissions] as $plugin => $permissions) {
								foreach($permissions as $permission => $v) {
									if($v['default']) {
										$permission = $plugin.".".$permission;
										debug($plugin." => ".$permission,$debug);
										foreach(m() as $module => $module_v) {
											if($module_v[plugins][$plugin]) {
												foreach($permissions_types as $type => $type_permissions) {
													if(in_array($type,$v['default'])) {
														debug("giving permission to ".$type." to `".$permission."` in the `".$plugin."` plugin of the `".$module."` module",$debug);
														$permissions_types[$type][$module][$permission] = 1;
													}
													else {
														$permissions_types[$type][$module][$permission] = NULL;
													}
												}
											}
										}
									}
								}
							}
						}
						
						// Debug
						debug("permissions after:".return_array($permissions_types),$debug);
						
						// Save
						foreach($permissions_types as $type => $permissions) {
							$item = item::load('types',m('users.types.'.$type.'.id'));
							$item->save(array('type_permissions' => data_serialize($permissions)),array('debug' => $debug));
						}
					}
				}
					
				// Message
				$message = NULL;
				$error = 0;
				// Message - package
				if($install[packages][count]) {
					$message = "This package has been successfully installed.";
					if($install[modules][error] or $install[plugins][error]) {
						$message .= " However, ";
						if($install[modules][error]) $message .= $install[modules][error]." ".string_pluralize("module",$install[modules][error]);
						if($install[plugins][error]) $message .= ($install[modules][error] ? " and " : "").$install[plugins][error]." ".string_pluralize("plugin",$install[plugins][error]);
						$message .= " couldn't be installed because they were incomplete.";
						$error = 1;
					}
					else if($install[modules][previous] or $install[plugins][previous]) {
						$message .= " Note: ";
						if($install[modules][previous]) $message .= $install[modules][previous]." ".string_pluralize("module",$install[modules][previous]);
						if($install[plugins][previous]) $message .= ($install[modules][previous] ? " and " : "").$install[plugins][previous]." ".string_pluralize("plugin",$install[plugins][previous]);
						$message .= " had been installed previously.";
					}
				}
				// Message - module
				else if($install[modules][count]) {
					if($install[modules][success]) {
						if($rows_only) $message = "These rows have been successfully imported.";
						else $message = "This module has been successfully installed.";
					}
					else if($install[modules][previous]) $message = "This module was already installed.";
					else if($install[modules][error]) {
						if($rows_only) $message = "There was an error importing these rows.";
						else $message = "This module couldn't be installed because it's incomplete.";
						$error = 1;
					}
				}
				// Message - plugin
				else if($install[plugins][count]) {
					if($install[plugins][success]) {
						if($rows_only) $message = "These rows have been successfully imported.";
						else $message = "This plugin has been successfully installed.";
					}
					else if($install[plugins][previous]) $message = "This plugin was already installed.";
					else if($install[plugins][error]) {
						if($rows_only) $message = "There was an error importing these rows.";
						else $message = "This plugin couldn't be installed because it's incomplete.";
						$error = 1;
					}
				}
				
				// Message - result
				if($error) {
					page_error($message,1);
					debug("error: ".$message,$debug);
				}
				else {
					page_message($message,1);
					debug("message: ".$message,$debug);
				}
			}
					
			// Delete temp
			file::directory_delete($path_temp);
		}
		
		// Redirect
		if(!debug_active() or !$debug) redirect(DOMAIN."admin/modules");
	}
}

if(!function_exists('module_download_database')) {
	/**
	 * Creates and saves the database file(s) when downloading a module.
	 *
	 * @param string $module The module you want to get the database structure of.
	 * @param string $path The path where we're saving the database structure file.
	 * @param array $array An array of configuration values. Default = NULL
	 */
	function module_download_database($module,$path,$c = NULL) {
		// Error
		if(!$module or !$path) return;
		
		// Config
		if(!x($c[table])) $c[table] = m($module.'.install.db.table'); // Do you want to include the table creation query in the download?
		if(!x($c[rows])) $c[rows] = m($module.'.install.db.rows'); // Do you want to include the rows in the download?
		if(!x($c[plugins])) $c[plugins] = 0; // Do you want to include related plugin data in the download?
		if(!x($c[children])) $c[children] = 1; // Do you want to include child modules in the download?
		
		// Path
		$path .= $module."/";
		
		// Database
		if($table = m($module.'.db.table')) {
			// Db
			$db = db::load();
			
			// Queries
			$queries = NULL;
			
			// Table
			if($c[table]) {
				$query_table = $db->table_create($table);
				$query_table = str_replace('DEFAULT CHARSET=latin1','DEFAULT CHARSET=utf8',$query_table); // Make sure it's UTF-8
				$queries[] = $query_table;
			}
			
			// Rows
			if($c[rows]) {
				$rows = $db->q("SELECT * FROM `".$table."`");
				while($row = $db->f($rows)) {
					$queries[] = "INSERT INTO `".$table."`".$db->values($row).";";	
				}
			}
			
			// Plugins
			if($c[plugins]) {
				foreach(m($module.'.plugins') as $plugin => $active) {
					if($active and $plugin_table = plugin($plugin.'.db.table')) {
						// Table
						$query_table = $db->table_create($plugin_table);
						$query_table = str_replace('DEFAULT CHARSET=latin1','DEFAULT CHARSET=utf8',$query_table); // Make sure it's UTF-8
						$queries[] = $query_table;
						
						// Rows
						$rows = $db->q("SELECT * FROM ".$plugin_table." WHERE ".plugin($plugin.'.db.parent_module')." = '".$module."'");
						while($row = $db->f($rows)) {
							unset($row[plugin($plugin.'.db.id')]);
							$queries[] = "INSERT INTO `".$plugin_table."`".$db->values($row).";";
						}
					}
				}
			}
			
			// Save
			if($queries) file_put_contents($path."db.sql",serialize($queries));
		}
		
		// Children
		if($children = m($module.'.children') and $c[children]) {
			$path .= "modules/";
			foreach($children as $child) {
				module_download_database($child,$path);
			}
		}
	}
}

if(!function_exists('plugin_download_database')) {
	/**
	 * Creates and saves the database file(s) when downloading a plugin.
	 *
	 * @param string $plugin The plugin you want to get the database structure of.
	 * @param string $path The path where we're saving the database structure file.
	 * @param array $array An array of configuration values. Default = NULL
	 */
	function plugin_download_database($plugin,$path,$c = NULL) {
		// Error
		if(!$plugin or !$path) return;
		
		// Config
		if(!x($c[table])) $c[table] = plugin($plugin.'.install.db.table'); // Do you want to include the table creation query in the download?
		if(!x($c[rows])) $c[rows] = plugin($plugin.'.install.db.rows'); // Do you want to include the rows in the download?
		if(!x($c[children])) $c[children] = 1; // Do you want to include child plugins in the download?
		
		// Path
		$path .= $plugin."/";
		
		// Database
		if($table = plugin($plugin.'.db.table')) {
			// Db
			$db = db::load();
			
			// Queries
			$queries = NULL;
			
			// Table
			if($c[table]) {
				$query_table = $db->table_create($table);
				$query_table = str_replace('DEFAULT CHARSET=latin1','DEFAULT CHARSET=utf8',$query_table); // Make sure it's UTF-8
				$queries[] = $query_table;
			}
			
			// Rows
			if($c[rows]) {
				$rows = $db->q("SELECT * FROM `".$table."`");
				while($row = $db->f($rows)) {
					$queries[] = "INSERT INTO `".$table."`".$db->values($row).";";	
				}
			}
			
			// Save
			if($queries) file_put_contents($path."db.sql",serialize($queries));
		}
		
		// Children
		if($children = plugin($plugin.'.children') and $c[children]) {
			$path .= "plugins/";
			foreach($children as $child) {
				plugin_download_database($child,$path);
			}
		}
	}
}

/**
 * Creates a zip file continaing all the data needed to install the given module or package on another site.
 */
if($_GET['process_action'] == "modules_download") {
	// Variables
	$package = NULL;
	$plugin = NULL;
	$module = NULL;
	if($_GET['package']) {
		$package = $_GET['package'];
		$name = "package-".$package;
	}
	else if($_GET['plugin']) {
		$plugin = $_GET['plugin'];
		$rows = $_GET['rows'];
		$name = "plugin-".$plugin.($rows ? "-rows" : "");
	}
	else {
		$module = $_GET['module'];
		$rows = $_GET['rows'];
		$name = $module.($rows ? "-rows" : "");
	}
	$path = SERVER."local/uploads/files/".$name."/";
	$file = SERVER."local/uploads/files/".$name.".zip";
	mkdir($path);
		
	// Not super admin
	if(u('type') != "supers") page_error("You don't have permission to download this.");	
	// No package/module
	else if(!$name) page_error("Please choose what you'd like to download.");	
	// Not wrteable
	else if(!file::directory_writeable($path)) page_error("The /".str_replace(SERVER,'',$path)." folder is not writeable. Please make sure it has 777 permissions.",1);
	// Download
	else {
		$modules = NULL;
		$plugins = NULL;
		if($package) {
			$package_array = package($package);
			$modules = $package_array[modules];
			$plugins = $package_array[plugins];
			mkdir($path."packages/");
			file::directory_copy(SERVER.$package_array[path],$path."packages/".$package."/");
		}
		else if($plugin) {
			$plugins = array($plugin);
		}
		else {
			$modules = array($module);
		}
		
		// Config
		$c = NULL;
		if($rows) {
			$c = array(
				'table' => 1,
				'rows' => 1,
				'children' => 0,
				'plugins' => 1,
			);
		}
		
		// Modules
		if($modules) {
			mkdir($path."modules/");
			foreach($modules as $module) {
				if(!m($module.'.parent')) {
					// Files
					if(!$rows) file::directory_copy(SERVER.m($module.'.path'),$path."modules/".$module."/");
					else mkdir($path."modules/".$module."/");
							
					// Database
					module_download_database($module,$path."modules/",$c);
				}
			}
		}
		
		// Plugins
		if($plugins) {
			mkdir($path."plugins/");
			foreach($plugins as $plugin) {
				if(!plugin($plugin.'.parent')) {
					// Files
					if(!$rows) file::directory_copy(SERVER.plugin($plugin.'.path'),$path."plugins/".$plugin."/");
							
					// Database
					plugin_download_database($plugin,$path."plugins/",$c);
				}
			}
		}
		
		// Zip
		include_function('zip');
		zip_directory($path,$file);
		
		// Delete
		file::directory_delete($path);
			
		// Download
		file::download($file);
	}
	
	// Redirect
	redirect(DOMAIN."admin/modules");
	exit;
}