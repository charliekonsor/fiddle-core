<?php
/**
 * @package kraken\modules\types
 */
 
if(!function_exists('type_module')) {
	/**
	 * Returns the module code the given type code is a part of.
	 *
	 * @param string $type The type code you want to get the module of.
	 * @return string The module code the given type is a part of.
	 */
	function type_module($type) {
		if($type) {
			foreach(m() as $module => $v) {
				if($v[types][$type]) return $module;	
			}
		}
	}
}
?>