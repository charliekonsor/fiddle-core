<?php
if(!class_exists('form_framework_users',false)) {
	/**
	 * Extends the form_framework class, providing scripts specific to the 'users' module.
	 *
	 * @package kraken\modules\users
	 */
	class form_framework_users extends form_framework {
		/**
		 * construct
		 */
		function __construct($module,$id = NULL,$c = NULL) {
			$this->form_framework_users($module,$id,$c);
		}
		function form_framework_users($module,$id = NULL,$c = NULL) {
			// Parent
			parent::__construct($module,$id,$c);
			
			// Type
			if($_GET['type']) {
				$module_class = module::load($this->module);
				if($module_class->permission('edit_all')) {
					if($this->type != $_GET['type']) {
						// Type
						$this->type = $_GET['type'];
						
						// Inputs
						if($this->module and $this->c[inputs]) {
							// Clear old inputs / plugins
							$this->inputs = NULL;
							$this->plugins = NULL;
							
							// Inputs
							$inputs = m($this->module.'.forms.add.inputs',$this->type);
							if($inputs) $this->inputs($inputs);
							// Plugins
							$this->plugins = m($this->module.'.forms.add.plugins',$this->type);
						}
					}
				}
			}
		}
		
		/**
		 * Renders a framework form, automatically setting missing values and adding in the submit button and hidden fields.
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 * @return string The HTML of the rendered form.
		 */
		function render($c = NULL) {
			// Config
			if($c) $this->c = array_merge($this->c,$c);
				
			// Type
			$type_key = m($this->module.'.db.type',$this->type);
			$types = m($this->module.'.types');
			foreach($types as $k => $v) { // Remove 'supers' as an option
				if(!$v[settings][registered] or $k == "supers") unset($types[$k]);
			}
			if($type_key and $types) {
				if(!$this->input_exists($type_key)) {
					// Only one type
					if(count($types) == 1) $this->type = array_first_key($types);
					// Defined type (and no permission to change type)
					if($this->type and ($types[$this->type] and !$this->item->permission('edit_all') or $this->type == "supers")) $this->hidden($type_key,$this->type);
					// Must choose type
					else {
						// Clear old inputs (temporarily)
						$inputs = $this->inputs;
						$this->inputs = NULL;
						
						// Add 'type' input
						$options = array('' => '- Select a Type -');
						foreach($types as $k => $v) $options[$k] = $v[single];
						$url = url_query_remove(URL,'type');
						$this->select('Type',NULL,$options,$this->type,array('onchange' => "location.assign('".$url.(strstr($url,'?') ? "&" : "?")."type='+this.options[this.selectedIndex].value)"));
						
						// Restore old inputs
						$this->inputs($inputs);
					}
				}
				else if($this->item->type == "supers") {
					$this->input_remove($type_key);
					$this->hidden($type_key,$this->type);
				}
			}
			
			// Return
			return parent::render($c);
		}
	}
}
?>