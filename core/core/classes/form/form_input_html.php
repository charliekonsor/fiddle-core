<?php
if(!class_exists('form_input_html',false)) {
	/**
	 * Creates a html input
	 */
	class form_input_html extends form_input_framework {
		/**
		 * Loads class. Needed so we can use debug_backtrace() to get this class name in PHP < 5.3 in $form_input->load().
		 */
		static function load($value = NULL,$c = NULL) {
			return parent::load($c[label],$c[name],$value,$c);
		}
		
		/**
		 * Construct
		 */
		function __construct($label = NULL,$name = NULL,$value = NULL,$c = NULL) {
			$this->form_input_html($label,$name,$value,$c);
		}
		function form_input_html($label = NULL,$name = NULL,$value = NULL,$c = NULL) {
			// Construct
			parent::__construct($label,$name,$value,$c);
		}
		
		/**
		 * Renders the HTML for the input's 'label', including the container.
		 *
		 * @param object $form The form object this input is a part of.
		 * @param array $c An array of config values. Default = NULL
		 * @return string The HTML of the input's label, including the container.
		 */
		function html_row($form,$c = NULL) {
			// Row
			$row = $this->row;
			$row[attributes]['class'] = str_replace('form-row form-row-html','',$row[attributes]['class']);
			$row[attributes] = array_filter($row[attributes]);
			$row = array_filter($row);
			
			// Label / row exists, must put in row container
			if(x($this->label[value]) or count($row)) {
				return parent::html_row($form,$c);
			}
			// No Label / row, can just display
			else {
				return $this->value;
			}
		}
		
		/**
		 * Renders the HTML for the actual input.
		 *
		 * @param object $form The form object this input is a part of.
		 * @param array $c An array of config values. Default = NULL
		 * @return string The HTML of the input.
		 */
		function html_element($form,$c = NULL) {
			return $this->value;
		}
	}
}
?>