/**
 * Uploads file using swfupload flash file uploader.
 *
 * Dependencies
 * - functions
 *   - x
 *   - element_id
 *   - form_submit_complete
 *   - form_submit				
 */
function swfupload() {
	if($().swfupload) {
		$('div.swfupload:not(.swfupload-created)').each(function() {
			// ID
			var id = element_id($(this),'swfupload-');
			
			// Simple layout
			$(this).prepend("<div class='swfupload-file swfupload-file-simple' id='swfupload-file-simple-"+id+"'></div>");
			
			// Button
			$(this).prepend("<div class='swfupload-button-container'><div class='swfupload-button' id='swfupload-button-"+id+"'></div></div>");
			
			// Clear
			$(this).append("<div class='clear'></div>");
			
			// Info Div (may already exist)
			var info = $(".swfupload-info",this);
			if(info.length > 0) info.attr("id","swfupload-info-"+id);
			else $(this).append("<div class='swfupload-info' id='swfupload-info-"+id+"'></div>");
			
			// Counter
			$(this).prepend("<input type='hidden' class='swfupload-counter' id='swfupload-counter-"+id+"' value='0' />");
			
			// Defaults
			var d = {
				upload_url : DOMAIN+"?ajax_action=uploadIt&flash=1&flash_id="+id,
				pending_layout : 'simple', // What layout ot show for pending: full (filename, close button, pending message, empty progress bar), simple (name next to swfupload field, only if limit == 1 and instand_upload isn't on) 
				file_post_name : 'file',
				//file_size_limit : '100 MB',
				file_types : "*", // "*" = all, images example: "*.jpg;*.jpeg;*.png;*.gif;*.bmp"
				file_types_description : "Files",
				//file_upload_limit : 1, // Use my own custom 'limit' variable for this instead (if exists, replace)
				limit : 1, // Custom file count limiter
				instant_upload : 1, // Custom setting which determines if we upload on file selection (1) or on form submission (0)
				flash_url : DOMAIN+"core/core/libraries/flash/swfupload/swfupload.swf",
				button_image_url : DOMAIN+"core/core/images/swfupload/button-browse.png",
				button_width : 73,
				button_height : 23,
				button_placeholder : $("#swfupload-button-"+id)[0],
				form_submit_error : 0, // Custom setting which determiens if we submit form, even if there was an error with upload
				on_queue: function(event,file,id) {},
				debug: false
			};
			
			// Config
			var c = $.extend(d,$(this).metadata());
			
			// Hide placeholder input
			//$(".swfupload-placeholder",this).hide(); // Turns off validation
			$(".swfupload-placeholder",this).css({width:'1px',height:'1px;',position:'absolute',left:'-9999px'});
			
			// SWFUpload
			$(this)
				.addClass("swfupload-created")
				.swfupload(c)
				.bind('fileQueued', function(event, file){
					debug('queing, file_id: '+file.id);
					
					// Info
					var layout_full='<div class="swfupload-file swfupload-file-full {\'file_id\':\''+file.id+'\'}" id="swfupload-file-'+id+'-'+file.id+'"'+(c.pending_layout == "simple" && c.limit == 1 && !c.instant_upload ? ' style="display:none;"' : '')+'>'+
						//'File: '+
						'<span class="swfupload-file-name">'+(file.name.length > 28 ? file.name.substr(0,28)+'..' : file.name)+'</span> '+
						//'<span class="swfupload-file-size">('+Math.round(file.size/1024)+' KB)</span> '+
						'<span class="swfupload-file-progressvalue" ></span>'+
						'<div class="swfupload-file-progressbar" ><div class="swfupload-file-progress" ></div></div>'+
						'<span class="swfupload-file-status" ></span>'+
						'<span class="swfupload-file-cancel swfupload-button swfupload-button-cancel swfupload-button-cancel-'+id+'-'+file.id+'"></span>'+
						'</div>';
					// Single Upload
					if(c.limit == 1) {
						// Cancel Previous
						var previous = $('.swfupload-file-full:first',this);
						if(previous.length > 0) {
							debug('previous: '+previous.metadata().file_id);
							swfuploadCancel(id,previous.metadata().file_id,1);
						}
						// Add New
						$("#swfupload-info-"+id).html(layout_full);
						if(c.pending_layout == "simple" && !c.instant_upload) {
							var layout_simple = '<span class="swfupload-file-name">'+(file.name.length > 60 ? file.name.substr(0,60)+'..' : file.name)+'</span>'+
							'<span class="swfupload-file-cancel swfupload-button swfupload-button-cancel swfupload-button-cancel-'+id+'-'+file.id+'"></span>';
							
							$("#swfupload-file-simple-"+id)
								.html(layout_simple)
								.removeClass('swfupload-file-simple-uploaded');
						}
					}
					// Multiple Upload
					else $("#swfupload-info-"+id).append(layout_full);
					
					// Adjust Counter
					swfuploadCount(id,c,1,'added to list');
					
					// Cancel Button
					$('span.swfupload-button-cancel-'+id+'-'+file.id).bind('click', function(){
						swfuploadCancel(id,file.id,1);
					});
					
					// On queue
					c.on_queue(event,file,id);
					
					// Get Form
					var form = $('#'+id).parents('form:first');
					
					// Instant Upload
					if(c.instant_upload == 1) {
						debug('instant upload, uploading');
						
						// Make sure visible
						$('#swfupload-file-'+id+'-'+file.id).show();
						$("#swfupload-file-simple-"+id).html("");
						
						// Disable Submit
						//form_submit(form,{submit_form:0});
						
						// Start the upload since it's queued
						$(this).swfupload('startUpload');
					}
					// Upload on Form Submit
					else {
						debug('non-instant upload, binding submit click');
						
						// Stop submission
						form_submit_complete(form);
						
						// Count/Validation
						var count = $("#swfupload-counter-"+id).val();
						debug('count: '+count);
						if(count > 0) {
							debug('selected a file, removing required');
							$("#"+id+" .swfupload-placeholder").removeClass('required');
							$("#"+id+" label.error").hide();
						}
						else {
							debug("no count, re-adding required to #"+id+" .swfupload-placeholder.required-input");
							$("#"+id+" .swfupload-placeholder.required-input").addClass('required');
						}
						
						// Setup submit click
						$("input[type='submit']",form).unbind('click').click(function() {
							debug('submit clicked');
							// Validate
							var valid = form.valid();
							debug('valid: '+valid);
							if($('label.error',form).is(':visible')) {
								debug('invalid form');	
							} // Invalid
							else {
								debug('starting upload (id: '+id+', file.id: '+file.id+')');
								// Make sure visible
								$(".swfupload-file-full").show();
								$(".swfupload-file-simple").not('.swfupload-file-simple-uploaded').html("");
								
								// Disable submit
								form_submit(form,{submit_form:0}); // Prefer this, but dosn't submit via form.submit() for some reason then
								
								// Start upload
								swfuploadStart(id,c);
							}
							return false;
						});
					}
				})
				.bind('fileQueueError', function(event, file, errorCode, message){
					debug("event = "+event+", file = "+file+", error code = "+errorCode+", message = "+message+"<br />");
					alert((c['error'] ? c['error'] : "You are not allowed to upload this type of file here."));
					//alert('Size of the file '+file.name+' is greater than limit');
				})
				.bind('fileDialogComplete', function(event, numFilesSelected, numFilesQueued){
					var queue = $("#swfupload-queue-"+id);
					if(queue.length == 0) {
						$("swfupload-info-"+id).prepend("<div class='swfupload-queue' id='swfupload-queue-"+id+"'></div>");
						var queue = $("#swfupload-queue-"+id);
					}
					queue.text('Files Selected: '+numFilesSelected+' / Queued Files: '+numFilesQueued);
				})
				.bind('uploadStart', function(event, file){
					$("#swfupload-file-"+id+"-"+file.id+" span.swfupload-file-status").text('Uploading...');
					$("#swfupload-file-"+id+"-"+file.id+" span.swfupload-file-progressvalue").text('0%');
					//$("#swfupload-file-"+id+"-"+file.id+" span.swfupload-file-cancel").hide();
				})
				.bind('uploadProgress', function(event, file, bytesLoaded){
					// Show Progress
					var percentage = Math.round((bytesLoaded / file.size) * 100);
					$("#swfupload-file-"+id+"-"+file.id+" div.swfupload-file-progress").css('width', percentage+'%');
					$("#swfupload-file-"+id+"-"+file.id+" span.swfupload-file-progressvalue").text(percentage+'%');
					if(percentage >= 100) {
						$("#swfupload-file-"+id+"-"+file.id+" span.swfupload-file-status").text('Processing...');
						$("#swfupload-file-"+id+"-"+file.id).addClass('swfupload-file-processing');
					}
				})
				.bind('uploadSuccess', function(event, file, serverData){			
					debug('data id: '+serverData);
					// Data (flash player has bug sometimes where doesn't pass more than 1000 characters, returning data_id, get data myself)
					/*$.ajax({
						type: 'POST',
						url: DOMAIN,
						data: 'ajax_action=loadIt&type=data&id='+serverData,
						success: function(serverData){
							debug('saved data: <xmp>'+serverData+'</xmp>');*/
							
							// Error
							var error = 0;
							if(serverData.indexOf(" class='core-red'>") > -1) error = 1;
							
							var item = $("#swfupload-file-"+id+"-"+file.id);
							debug('value: '+item.val()+', html: <xmp>'+item.html()+'</xmp>');
							item.find('div.swfupload-file-progress').css('width','100%');
							item.find('span.swfupload-file-progressvalue').text('100%');
							debug('adding success, about to fade');
							item.addClass('swfupload-file-success').fadeOut(300,function() {
								debug('item hidden');
								// Remove processing stuff
								$(this)
									.removeClass('swfupload-file')
									.removeClass('swfupload-file-full')
									.removeClass('swfupload-file-processing')
									.removeClass('swfupload-file-success');
								
								// Simple layout
								if(c.pending_layout == "simple" && !c.instant_upload) {
									$("#swfupload-file-simple-"+id)
										.html(serverData)
										.addClass('swfupload-file-simple-uploaded')
										.fadeIn(300);
								}
								// Full layout
								else {
									$(this)
										.html(serverData)
										.fadeIn(300);
								}
								
								// Form
								debug('getting form');
								var form = $('#'+id).parents('form:first');
								
								// Adjust Counter			
								debug('getting count');	
								var count = swfuploadCount(id,c,-1,'compelted upload');
								debug('getting total count');
								var count_total = swfuploadCountTotal(id,c);
								
								debug('upload complete (id = '+id+'), count = '+count+', count_total = '+count_total);
								
								// Upload Next
								if(count_total > 0) swfuploadStart(id,c);
								// Finished Uploading
								else {
									debug('finished uploading all');
									// Enable Submit
									if(c.instant_upload == 1) {
										debug('submitting form (instant upload turned on)');
										form_submit_complete(form);
									}
									// Submit Form
									if(c.instant_upload == 0) {
										debug("no instant upload, removing loader (don't want 2)<br />");
										$("input[type='submit']",form).next("img.loader").remove(); // Remove loader so we don't have 2
										// Error, don't submit form
										if(error && c.form_submit_error != 1) {
											debug('error, not submitting');
											swfuploadCancel(id,file.id,0);
											form_submit_complete(form);
										}
										// Success, submit form
										else {
											debug('submitting form');
											
											// New method
											form_submit_complete(form);
											form_submit(form);
											
											/// Old method
											/*form_submit_complete(form);
											form.submit();
											$('input[type=submit]',form).attr('disabled','disabled');*/
										}
									}
								}
								
								// Refresh jQuery
								debug('about to refresh jquery');
								js_refresh();
								
							});
							// Remove required from placeholder
							if(!error) {
								$("#"+id+" .swfupload-placeholder").removeClass('required');
								$("#"+id+" label.error").hide();
							}
							// Hide errors
							else {
								$('.core-red',item).animate({opacity: 1.0}, 5000).slideUp(650,function() { $(this).remove(); });
							}
						/*}
					});*/
				})
				.bind('uploadError', function(event, file, errorCode, message){	
					// Adjust Counter
					debug('upload error. id: '+file.id+', name: '+file.name+', errorCode: '+errorCode+', message: '+message);
					swfuploadCount(id,c,-1,'upload error');
					
					var form = $('#'+id).parents('form:first');
					form_submit_complete(form);
				})
				.bind('uploadComplete', function(event, file){
					debug('upload complete');
					// Upload has completed, try the next one in the queue
					//swfuploadStart(id,c);
				});
		});
	}
}
function swfuploadStart(id,c) {
	debug('starting upload for input '+id);
	var form = $('#'+id).parents('form:first');
	// Instant uploading, try next in this input's queue
	debug('input '+id+'\'s insant_upload value: '+c.instant_upload);
	if(c.instant_upload == 1) {
		// Items to upload?
		var count = swfuploadCount(id,c);
	debug('input '+id+'\'s count: '+count);
		// Yes, start upload
		if(count > 0) $(this).swfupload('startUpload');
		// No
		else {
			debug('checking to see if all instant swfupload input\'s are complete');
			// Loop through other swfupload inputs
			var show = true;
			$('.swfupload-created',form).each(function() {
				// Items left to upload?
				var _c = $(this).metadata();
				var _id = $(this).attr('id');
				debug('other swfupload: id = '+_id+', instant_upload = '+_c.instant_upload);
				if(_c.instant_upload == 1) {
					// Yes, don't enable submit yet
					var _count = swfuploadCount(_id,_c);
					debug('other swfupload: id = '+_id+', count = '+_count);
					if(_count > 0) {
						debug('other swfupload '+_id+' has items left to upload and is "instant_upload", do NOT enable submit');
						show = false;
					}
				}
			});
			// No items left to upload, enable submit
			if(show == true) {
				form_submit_complete(form);
			}
		}
	}
	// Submit uploading, try all input in form
	else {
		$('.swfupload-created',form).each(function() {
			debug('starting upload for id = '+$(this).attr('id'));
			$(this).swfupload('startUpload');						  
		});
	}
}
function swfuploadCancel(id,file,remove) {
	if(!x(remove)) remove = 1;
	
	// Cancel
	var swfu = $.swfupload.getInstance('#'+id);
	swfu.cancelUpload(file);
	debug('cancelled file '+file);
	
	// Remove
	if(remove == 1) {
		$("#swfupload-file-"+id+"-"+file).slideUp('fast');
		$("#swfupload-file-simple-"+id).html("");
	}
	
	// Form
	var form = $('#'+id).parents('form:first');
	form_submit_complete(form);
}
function swfuploadCount(id,c,change,reason) {
	// Get Count
	var counter = $("#swfupload-counter-"+id);
	// Update Count
	count = (counter.val() * 1);
	if(change) count += change;
	// Over Limit
	//if(count > c.limit && c.limit) count = c.limit;
	// Update Counter
	counter.val(count);
	debug('count ('+id+'): '+count+' ['+reason+']');
	
	// Total Counter
	swfuploadCountTotal(id,c);
	
	return count;
}
function swfuploadCountTotal(id,c) {
	// Form
	var form = $('#'+id).parents('form:first');
	
	// Count
	var count = 0;
	$('input.swfupload-counter',form).each(function() {
		count += ($(this).val() * 1);												
	});
	debug('total count: '+count);
	
	// Enable Submit
	if(count <= 0) {
		$("input[type='submit']",form).unbind('click').click(function() {
			form.submit();
		});
	}
	
	return count;
}
