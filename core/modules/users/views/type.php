<?php
// Visible / permission
if($this->visible(1) and $this->permission('view',1)) {
	print "
<div class='page page-module-".$this->module." page-view-".$this->view."'>";
	// Heading
	//print $this->heading();
	print "
<h1 class='page-heading'>
	<a href='".$this->module_class->url()."'>".$this->module_class->v('name')."</a>
</h1>";
	// Buttons
	print $this->heading_buttons();
		
	// All
	if(!$this->page->variables[0]) {
		// Filters
		print $this->module_class->manage_filters();
		// Items
		print $this->module_class->manage_items(array('area' => 'public'));
	}
	
	// New
	if($this->page->variables[0] == "new") {
		// Form
		$form = $this->module_class->form(array('type' => $this->type,'redirect' => $this->module_class->url()));
		print $form->render();
	}

	print "
</div>";
}
?>