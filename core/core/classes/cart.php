<?php
if(!class_exists('cart',false)) {
	/**
	 * A class for adding items, discounts, etc. to a cart and checking out.
	 *
	 * Pyment processing will be handled via the payment class.
	 * This class, on the other hand, is basicaly used to determine what we'll be charging in the payment class as well as to keep track of what is being purchased.
	 *
	 * Example:
	 *	$cart = cart::load();
	 *	$item = array( // See item_add() for more examples
	 * 		'key' => 123, // A unique key that we'll use when we call the update() and remove() methods.
	 *		'name' => 'Hamlet', // The name of the item
	 *		'price' => 15.45, // The price for 1 of the item
	 *	);
	 * 	$cart->item_add($item);
	 *	print $card->render();
	 *
	 * To do
	 * - integrate $this->currency
	 * - remove anything in curl() $c array we don't use (header?, follow?)
	 * - maybe ability to define shipping -> boxes on a per-item basis. Use those boxes when calculating what will fit in what.
	 * 
	 * Dependencies
	 * - None
	 *
	 * @package kraken\cart
	 */
	class cart extends core_framework {
		/** The key we use when caching the cart. */
		public $cache_key = "cart";
		/** Life (in seconds) that the cart cache is valid for.  Each access of the cart resets the counter. Since the cache name uses the session_id, we can set it for a very long time and it'll still expire if the session expires. */
		public $cache_life = 86400; // 1 day
		/** An array of items in this cart */
		public $items;
		/** The overall total of everything in the cart (including discounts, tax, shipping, handling, and insurance) */
		public $total;
		/** An array of information on the total amounts in the cart (subtotal, discounts, tax, shipping, handling, insurance, total) */
		public $totals;
		/** An array of tax states and their tax rate. Example: array('WA' => .075,'TX' => .0543) */
		public $tax;
		/** An array of shipping information including address(es), available methods and the chosen method */
		public $shipping = array(
			'from' => array(
				'zip' => 97005,
				'state' => 'OR',
				'country' => 'US'
			),
			'default' => array(
				'weight' => array(
					'weight' => '',
					'unit' => 'oz',
				),
				'dimensions' => array(
					'width' => '',
					'length' => '',
					'height' => '',
					'unit' => 'in',
				),
			),
			'boxes' => array(
				// Example of defining available box sizes to 'pack' the items into when calculating shipping
				/*array(
					'dimensions' => array( // Unit matches the standardized unit the cart class uses.  In this case, inches.
						100,
						100,
						100,
					),
					'weight' => 10, // Unit matches the standardized unit the cart class uses. In this case, ounces.
					'weight_max' => 1120, // 70lbs. Unit matches the standardized unit the cart class uses. In this case, ounces.
				),*/
			),
			// Note, all the 'companies' will default to the g('config.cart.shipping.companies') config values, but you can overwrite them within the class using these variables
			'companies' => array(
				'usps' => array(
					'username' => '',
				),
				'ups' => array(
					'access' => '',
					'username' => '',
					'password' => '',
					'account' => '',
				),
				'fedex' => array(
					'key' => '',
					'password' => '',
					'account' => '',
					'meter' => '',
					'wsdl' => '',
				),
			),
			'methods' => array(
				// USPS - domestic
				array(
					'company' => 'USPS',
					'code' => 'First-Class Mail Large Envelope',
					'name' => 'First-Class Mail Large Envelope',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'First-Class Mail Letter',
					'name' => 'First-Class Mail Letter',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'First-Class Mail Parcel',
					'name' => 'First-Class Mail Parcel',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'First-Class Mail Postcards',
					'name' => 'First-Class Mail Postcards',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'Priority Mail',
					'name' => 'Priority Mail',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'Priority Mail Express Hold For Pickup',
					'name' => 'Priority Mail Express Hold For Pickup',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'Priority Mail Express',
					'name' => 'Priority Mail Express',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'Standard Post',
					'name' => 'Standard Post',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'Media Mail',
					'name' => 'Media Mail',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'Library Mail',
					'name' => 'Library Mail',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'Priority Mail Express Flat Rate Envelope',
					'name' => 'Priority Mail Express Flat Rate Envelope',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'First-Class Mail Large Postcards',
					'name' => 'First-Class Mail Large Postcards',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'Priority Mail Flat Rate Envelope',
					'name' => 'Priority Mail Flat Rate Envelope',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'Priority Mail Medium Flat Rate Box',
					'name' => 'Priority Mail Medium Flat Rate Box',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'Priority Mail Large Flat Rate Box',
					'name' => 'Priority Mail Large Flat Rate Box',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'Priority Mail Express Sunday/Holiday Delivery',
					'name' => 'Priority Mail Express Sunday/Holiday Delivery',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'Priority Mail Express Sunday/Holiday Delivery Flat Rate Envelope',
					'name' => 'Priority Mail Express Sunday/Holiday Delivery Flat Rate Envelope',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'Priority Mail Express Flat Rate Envelope Hold For Pickup',
					'name' => 'Priority Mail Express Flat Rate Envelope Hold For Pickup',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'Priority Mail Small Flat Rate Box',
					'name' => 'Priority Mail Small Flat Rate Box',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'Priority Mail Padded Flat Rate Envelope',
					'name' => 'Priority Mail Padded Flat Rate Envelope',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'Priority Mail Express Legal Flat Rate Envelope',
					'name' => 'Priority Mail Express Legal Flat Rate Envelope',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'Priority Mail Express Legal Flat Rate Envelope Hold For Pickup',
					'name' => 'Priority Mail Express Legal Flat Rate Envelope Hold For Pickup',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'Priority Mail Express Sunday/Holiday Delivery Legal Flat Rate Envelope',
					'name' => 'Priority Mail Express Sunday/Holiday Delivery Legal Flat Rate Envelope',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'Priority Mail Hold For Pickup',
					'name' => 'Priority Mail Hold For Pickup',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'Priority Mail Large Flat Rate Box Hold For Pickup',
					'name' => 'Priority Mail Large Flat Rate Box Hold For Pickup',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'Priority Mail Medium Flat Rate Box Hold For Pickup',
					'name' => 'Priority Mail Medium Flat Rate Box Hold For Pickup',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'Priority Mail Small Flat Rate Box Hold For Pickup',
					'name' => 'Priority Mail Small Flat Rate Box Hold For Pickup',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'Priority Mail Flat Rate Envelope Hold For Pickup',
					'name' => 'Priority Mail Flat Rate Envelope Hold For Pickup',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'Priority Mail Gift Card Flat Rate Envelope',
					'name' => 'Priority Mail Gift Card Flat Rate Envelope',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'Priority Mail Gift Card Flat Rate Envelope Hold For Pickup',
					'name' => 'Priority Mail Gift Card Flat Rate Envelope Hold For Pickup',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'Priority Mail Window Flat Rate Envelope',
					'name' => 'Priority Mail Window Flat Rate Envelope',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'Priority Mail Window Flat Rate Envelope Hold For Pickup',
					'name' => 'Priority Mail Window Flat Rate Envelope Hold For Pickup',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'Priority Mail Small Flat Rate Envelope',
					'name' => 'Priority Mail Small Flat Rate Envelope',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'Priority Mail Small Flat Rate Envelope Hold For Pickup',
					'name' => 'Priority Mail Small Flat Rate Envelope Hold For Pickup',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'Priority Mail Legal Flat Rate Envelope',
					'name' => 'Priority Mail Legal Flat Rate Envelope',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'Priority Mail Legal Flat Rate Envelope Hold For Pickup',
					'name' => 'Priority Mail Legal Flat Rate Envelope Hold For Pickup',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'Priority Mail Padded Flat Rate Envelope Hold For Pickup',
					'name' => 'Priority Mail Padded Flat Rate Envelope Hold For Pickup',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'Priority Mail Regional Rate Box A',
					'name' => 'Priority Mail Regional Rate Box A',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'Priority Mail Regional Rate Box A Hold For Pickup',
					'name' => 'Priority Mail Regional Rate Box A Hold For Pickup',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'Priority Mail Regional Rate Box B',
					'name' => 'Priority Mail Regional Rate Box B',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'Priority Mail Regional Rate Box B Hold For Pickup',
					'name' => 'Priority Mail Regional Rate Box B Hold For Pickup',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'First-Class Package Service Hold For Pickup',
					'name' => 'First-Class Package Service Hold For Pickup',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'Priority Mail Express Flat Rate Boxes',
					'name' => 'Priority Mail Express Flat Rate Boxes',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'Priority Mail Express Flat Rate Boxes Hold For Pickup',
					'name' => 'Priority Mail Express Flat Rate Boxes Hold For Pickup',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'Priority Mail Express Sunday/Holiday Delivery Flat Rate Boxes',
					'name' => 'Priority Mail Express Sunday/Holiday Delivery Flat Rate Boxes',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'Priority Mail Regional Rate Box C',
					'name' => 'Priority Mail Regional Rate Box C',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'Priority Mail Regional Rate Box C Hold For Pickup',
					'name' => 'Priority Mail Regional Rate Box C Hold For Pickup',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'First-Class Package Service',
					'name' => 'First-Class Package Service',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'Priority Mail Express Padded Flat Rate Envelope',
					'name' => 'Priority Mail Express Padded Flat Rate Envelope',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'Priority Mail Express Padded Flat Rate Envelope Hold For Pickup',
					'name' => 'Priority Mail Express Padded Flat Rate Envelope Hold For Pickup',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'Priority Mail Express Sunday/Holiday Delivery Padded Flat Rate Envelope',
					'name' => 'Priority Mail Express Sunday/Holiday Delivery Padded Flat Rate Envelope',
					'active' => 1,
				),
				// USPS - international
				array(
					'company' => 'USPS',
					'code' => 'Priority Mail Express International',
					'name' => 'Priority Mail Express International',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'Priority Mail International',
					'name' => 'Priority Mail International',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'Global Express Guaranteed (GXG)',
					'name' => 'Global Express Guaranteed (GXG)',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'Global Express Guaranteed Document',
					'name' => 'Global Express Guaranteed Document',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'Global Express Guaranteed Non-Document Rectangular',
					'name' => 'Global Express Guaranteed Non-Document Rectangular',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'Global Express Guaranteed Non-Document Non-Rectangular',
					'name' => 'Global Express Guaranteed Non-Document Non-Rectangular',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'Priority Mail International Flat Rate Envelope',
					'name' => 'Priority Mail International Flat Rate Envelope',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'Priority Mail International Medium Flat Rate Box',
					'name' => 'Priority Mail International Medium Flat Rate Box',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'Priority Mail Express International Flat Rate Envelope',
					'name' => 'Priority Mail Express International Flat Rate Envelope',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'Priority Mail International Large Flat Rate Box',
					'name' => 'Priority Mail International Large Flat Rate Box',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'USPS GXG Envelopes',
					'name' => 'USPS GXG Envelopes',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'First-Class Mail International Letter',
					'name' => 'First-Class Mail International Letter',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'First-Class Mail International Large Envelope',
					'name' => 'First-Class Mail International Large Envelope',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'First-Class Package International Service',
					'name' => 'First-Class Package International Service',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'Priority Mail International Small Flat Rate Box',
					'name' => 'Priority Mail International Small Flat Rate Box',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'Priority Mail Express International Legal Flat Rate Envelope',
					'name' => 'Priority Mail Express International Legal Flat Rate Envelope',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'Priority Mail International Gift Card Flat Rate Envelope',
					'name' => 'Priority Mail International Gift Card Flat Rate Envelope',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'Priority Mail International Window Flat Rate Envelope',
					'name' => 'Priority Mail International Window Flat Rate Envelope',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'Priority Mail International Small Flat Rate Envelope',
					'name' => 'Priority Mail International Small Flat Rate Envelope',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'First-Class Mail International Postcard',
					'name' => 'First-Class Mail International Postcard',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'Priority Mail International Legal Flat Rate Envelope',
					'name' => 'Priority Mail International Legal Flat Rate Envelope',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'Priority Mail International Padded Flat Rate Envelope',
					'name' => 'Priority Mail International Padded Flat Rate Envelope',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'Priority Mail International DVD Flat Rate priced box',
					'name' => 'Priority Mail International DVD Flat Rate priced box',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'Priority Mail International Large Video Flat Rate priced box',
					'name' => 'Priority Mail International Large Video Flat Rate priced box',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'Priority Mail Express International Flat Rate Boxes',
					'name' => 'Priority Mail Express International Flat Rate Boxes',
					'active' => 1,
				),
				array(
					'company' => 'USPS',
					'code' => 'Priority Mail Express International Padded Flat Rate Envelope',
					'name' => 'Priority Mail Express International Padded Flat Rate Envelope',
					'active' => 1,
				),
				// UPS
				array(
					'company' => 'UPS',
					'code' => '01',
					'name' => 'Next Day Air',
					'active' => 1,
				),
				array(
					'company' => 'UPS',
					'code' => '02',
					'name' => 'Second Day Air',
					'active' => 1,
				),
				array(
					'company' => 'UPS',
					'code' => '03',
					'name' => 'Ground',
					'active' => 1,
				),
				array(
					'company' => 'UPS',
					'code' => '07',
					'name' => 'Worldwide Express',
					'active' => 1,
				),
				array(
					'company' => 'UPS',
					'code' => '08',
					'name' => 'Worldwide Expedited',
					'active' => 1,
				),
				array(
					'company' => 'UPS',
					'code' => '11',
					'name' => 'Standard',
					'active' => 1,
				),
				array(
					'company' => 'UPS',
					'code' => '12',
					'name' => 'Three-Day Select',
					'active' => 1,
				),
				array(
					'company' => 'UPS',
					'code' => '13',
					'name' => 'Next Day Air Saver',
					'active' => 1,
				),
				array(
					'company' => 'UPS',
					'code' => '14',
					'name' => 'Next Day Air Early A.M.',
					'active' => 1,
				),
				array(
					'company' => 'UPS',
					'code' => '54',
					'name' => 'Worldwide Express Plus',
					'active' => 1,
				),
				array(
					'company' => 'UPS',
					'code' => '59',
					'name' => 'Second Day Air A.M.',
					'active' => 1,
				),
				array(
					'company' => 'UPS',
					'code' => '65',
					'name' => 'Saver',
					'active' => 1,
				),
				// FedEx
				array(
					'company' => 'FedEx',
					'code' => 'EUROPE_FIRST_INTERNATIONAL_PRIORITY',
					'name' => 'Europe First Priority',
					'active' => 1,
				),
				array(
					'company' => 'FedEx',
					'code' => 'FEDEX_1_DAY_FREIGHT',
					'name' => '1 Day Freight',
					'active' => 1,
				),
				array(
					'company' => 'FedEx',
					'code' => 'FEDEX_2_DAY_FREIGHT',
					'name' => '2 Day Freight',
					'active' => 1,
				),
				array(
					'company' => 'FedEx',
					'code' => 'FEDEX_2_DAY',
					'name' => '2 Day',
					'active' => 1,
				),
				array(
					'company' => 'FedEx',
					'code' => 'FEDEX_2_DAY_AM',
					'name' => '2 Day AM',
					'active' => 1,
				),
				array(
					'company' => 'FedEx',
					'code' => 'FEDEX_3_DAY_FREIGHT',
					'name' => '3 Day Freight',
					'active' => 1,
				),
				array(
					'company' => 'FedEx',
					'code' => 'FEDEX_EXPRESS_SAVER',
					'name' => 'Express Saver',
					'active' => 1,
				),
				array(
					'company' => 'FedEx',
					'code' => 'FEDEX_GROUND',
					'name' => 'Ground',
					'active' => 1,
				),
				array(
					'company' => 'FedEx',
					'code' => 'FIRST_OVERNIGHT',
					'name' => 'First Overnight',
					'active' => 1,
				),
				array(
					'company' => 'FedEx',
					'code' => 'GROUND_HOME_DELIVERY',
					'name' => 'Home Delivery',
					'active' => 1,
				),
				array(
					'company' => 'FedEx',
					'code' => 'INTERNATIONAL_ECONOMY',
					'name' => 'International Economy',
					'active' => 1,
				),
				array(
					'company' => 'FedEx',
					'code' => 'INTERNATIONAL_ECONOMY_FREIGHT',
					'name' => 'International Economy Freight',
					'active' => 1,
				),
				array(
					'company' => 'FedEx',
					'code' => 'INTERNATIONAL_FIRST',
					'name' => 'International First',
					'active' => 1,
				),
				array(
					'company' => 'FedEx',
					'code' => 'INTERNATIONAL_GROUND',
					'name' => 'International Ground',
					'active' => 1,
				),
				array(
					'company' => 'FedEx',
					'code' => 'INTERNATIONAL_PRIORITY',
					'name' => 'International Priority',
					'active' => 1,
				),
				array(
					'company' => 'FedEx',
					'code' => 'INTERNATIONAL_PRIORITY_FREIGHT',
					'name' => 'International Priority Freight',
					'active' => 1,
				),
				array(
					'company' => 'FedEx',
					'code' => 'PRIORITY_OVERNIGHT',
					'name' => 'Priority Overnight',
					'active' => 1,
				),
				array(
					'company' => 'FedEx',
					'code' => 'SMART_POST',
					'name' => 'Smart Post',
					'active' => 1,
				),
				array(
					'company' => 'FedEx',
					'code' => 'STANDARD_OVERNIGHT',
					'name' => 'Standard Overnight',
					'active' => 1,
				),
				array(
					'company' => 'FedEx',
					'code' => 'FEDEX_FREIGHT',
					'name' => 'Freight',
					'active' => 1,
				),
				array(
					'company' => 'FedEx',
					'code' => 'FEDEX_NATIONAL_FREIGHT',
					'name' => 'National Freight',
					'active' => 1,
				),
				// Custom - just an example
				/*array(
					'name' => 'My Custom Shipping Method',
					'rate' => '1.50', // Rate charged per quantity
					'countries' => array(), // Array of countries and states within that country this method ships to, ex: array('US' => 'all','CA' => array('BC','AB'))
					'countries_exclude' => array(), // Array of countries and states within that country this method does NOT ship to see above for example
					'active' => 1,
				),*/
			)
		);
		/** An array of handling information. Example: array('price' => 1.50,'charge' => 'quantity') */
		public $handling;
		/** The currency all totals are in for this transaction */
		public $currency = "USD";
		/** An array of custom key/value pairs you want to store for use later. Example: array('mycustomkey' => 'My Custom Value') */
		public $custom;
		/** An array of configuration values passed to the class */
		public $c;
		
		/**
		 * Loads and returns an instance of the cart class, using module or framework level classes if they exist.
		 *
		 * Example:
		 * - $cart = cart::load($module,$c);
		 *
		 * @param string $module The module this cart is working with. Only applies to framework and module level classes.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return object An instance of either the cart class, the cart_framework class (if it exists), the module specific cart_framework class (if it exists).
		 */
		static function load($module = NULL,$c = NULL) {
			// Class name
			$class = __CLASS__;
			
			// Params
			if(!$module or is_array($module)) { // cart::load($c)
				$c = $module;
				$module = NULL;
			}
			
			// Framework
			if(FRAMEWORK) {
				$class_framework = $class."_framework";
				$params = func_get_args(); // Must be outside function call
				$class_load = load_class_module($class_framework,$params,$module,$c[type]); // No way to instanitiate class with array of params so we'll just return the class name and...
				return new $class_load($module,$c); // ...manually pass the params here
			}
			
			// Core class
			return new $class($c);
		}
		
		/**
		 * Constructs the class.
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function __construct($c = NULL) {
			self::cart($c);
		}
		function cart($c = NULL) {
			// Cached
			$this->cache_get();
			
			// Config - globals
			if(!$c) $c = array();
			if(g('config.cart')) $c = array_merge(g('config.cart'),$c);
			
			// Config - defaults
			if(!$this->x($c[debug])) $c[debug] = 0; // Debug
			
			// Config - variables
			if($c[tax]) $this->tax($c[tax]);
			if($c[shipping]) $this->shipping($c[shipping]);
			if($c[handling]) $this->handling($c[handling]);
			if($c[currency]) $this->currency($c[currency]);
			if($c[custom]) {
				foreach($c[custom] as $k => $v) {
					$this->custom($k,$v);
				}
			}
			unset($c[tax],$c[shipping],$c[handling],$c[currency],$c[custom]);
			
			// Config - store
			$this->c = $c;
			
			// Total - just to be sure, also to refresh cache and cache expiration time
			$this->total();
			
			// Cache - every access of the cart should refresh it's expire date // This gets handled in $this->total()
			//$this->cache_save();
		}
		
		/**
		 * Returns key for the given array. If no key exists, it creates one.
		 *
		 * @param array $array The array you want to get (or set) the key for.
		 * @return string The array's key.
		 */
		function key($array) {
			// Key
			if(!$array[key]) {
				list($milliseconds,$seconds) = explode(' ',microtime()); 
				$array[key] = (float) $milliseconds + (float) $seconds;
			}
			
			// Return
			return $array[key];
		}
		
		/**
		 * Returns the item array for the given item key. If no key exists, it creates one.
		 *
		 * @param string $key The unique key of the item we set when we added it to the cart.
		 * @return array The item array (if it exists).
		 */
		function item($key) {
			return $this->items[$key];
		}
		
		/**
		 * Adds an item to the cart.
		 *
		 * Example - simple:
		 * $item = array(
		 *		'id' => 123, // A unique id that we'll use when we call the update() and remove() methods.
		 *		'name' => 'Hamlet', // The name of the item
		 *		'description' => 'Story of a Danish prince tormented by the death of his father', // The description of the item
		 *		'image' => 'http://www.mywebsite.com/uploads/images/hamlet.jpg', // The thumbnail image of the item
		 *		'price' => 15.45, // The price for 1 of the item
		 *		'shipping' => array(
		 *			'weight' => array( // Shipping weeight for 1 quantity of the item
		 *				'weight' => 10,
		 *				'unit' => 'oz', // Defaults to ounces (oz), can be: gram, oz, lb, kg
		 *			),
		 *			//'weight' => 10, // Can also pass a single number for the weight and we'll set that to weight=>weight, and set weight=>unit to 'oz' 
		 *			'dimensions' => array( // Shipping dimensions for 1 quantity of the item
		 *				'width' => 6,
		 *				'length' => 10,
		 *				'height' => 1.5,
		 *				'unit' => 'in', // Defaults to inches (in), can be: cm, in, ft
		 *			)
		 *		)
		 * );
		 *
		 * Example - shipping and handling cost defined:
		 * $item = array(
		 *		...see 'simple' example...
		 *		'shipping' => array(
		 *			'price' => 5.00, // Static shipping price for this item
		 *			'charge' => 'quantity', // What do we charge shipping on: quantity (once on each quantity of each item ordered), item (once on each unique item, regardless of quantity)
		 *		),
		 *		'handling' => array(
		 *			'price' => 5.00, // Static handling price for this item
		 *			'charge' => 'quantity', // What do we charge handling on: quantity (once on each quantity of each item ordered), item (once on each unique item, regardless of quantity)
		 *		),
		 * );
		 *
		 * Example - advanced quantity information as well as custom variables:
		 * $item = array(
		 *		'id' => 'lu2h98h23823h23', // A unique id that we'll use when we call the update() and remove() methods.
		 *		'name' => '60W Light Bulb', // The name of the item
		 *		'price' => 2.00, // The price for 1 of the item
		 *		'quantity' => array(
		 *			'unit' => array(
		 *				'single' => 'Case', // The singular version of the unit. Example: 'Case'.
		 *				'pural' => 'Cases', // The plural version of the unit. Example: 'Cases'.
		 *				//'count' => 12, // How many quantities are in one of these quantity units? Example: 12 in a single case. // Never built
		 *				//'price' => 20.00, // Price for the entire unit. Example: $20.00 for a case. // Never built
		 *			),
		 *			'minimum' => 12, // Minimum quantity they must purchase to be able to place an order. Example: 12 (or 1 case).
		 *			'maximum' => 24, // Maximum quantity they can purchase in one order. Example: 24 (or 2 cases).
		 *		),
		 * );
		 *
		 * Example - defined tax rate and custom variables
		 * $item = array(
		 *		'id' => 'lu2h98h23823h23', // A unique id that we'll use when we call the update() and remove() methods.
		 *		'name' => '60W Light Bulb', // The name of the item
		 *		'price' => 2.00, // The price for 1 of the item
		 *		'tax' => array(
		 *			'rate' => .09, // A defined tax rate for this item (as a decimal, not percent). If none passed, we'll use the globally defined tax rate for the item's shipping address (see $this->tax()).
		 *		),
		 *		'custom' => array(
		 *			'mykey' => 'My Value' // A custom variable we want to attach to this item.
		 *		)
		 * );
		 *
		 * Example - defined package(s) for item
		 * $item = array(
		 *		...see 'simple' example...
		 *		'shipping' => array(
		 *			...since we already defined the dimensions/weight of the package(s) we dont' need to define the per quantity weight/dimensions...
		 *			'packages' => array(
		 *				array(
		 *					'dimensions' => array(
		 *						'width' => 12,
		 *						'length' => 8,
		 *						'height' => 10,
		 *						'unit' => 'in',
		 *					),
		 *					'weight' => array(
		 *						'weight' => 15,
		 *						'unit' => 'lb',
		 *					),
		 *				),
		 *				...we could simply add another array here for more packages...
		 *			)
		 *		)
		 * );
		 *
		 * Note: if there was an error adding this item to the cart, the array we return will contain an 'errors' array listing all error(s) that occured and the item will not be saved ot the cart.
		 *
		 * @param array $item The array of the item's information (see method comment for example).
		 * @param int $quantity The quantity of this item we're adding to the cart (not the whole quantity, but the quantity we're adding).
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array The item array.
		 */
		function item_add($item,$quantity,$c = NULL) {
			// Config
			if(!x($c[standardize])) $c[standardize] = 1; // Standardize item. May not want to do if we're upading just a value or two on an item.
			
			// Remove old errors
			$item[errors] = NULL;
			
			// Standardize
			if($c[standardize]) $item = $this->item_standardize($item);
			
			// New item
			$item_new = $item;
			
			// Exists, update
			if($this->items[$item_new[key]]) {
				// Quantity
				if($quantity) $item_new[quantity][count] = $this->items[$item_new[key]][quantity][count] + $quantity;
				
				// Update
				$item_new = $this->array_merge_associative($this->items[$item_new[key]],$item_new);
			}
			// Doesn't exist, add
			else {
				// Quantity
				$item_new[quantity][count] = $quantity;
			}
				
			// Total // Happens in $this->total() too, don't need to do here...unless we end up needing it in validate().
			//$item_new = $this->item_total($item_new);
		
			// Validate
			$item_new = $this->item_validate($item_new,array('debug' => $c[debug]));
			
			// Error
			if($item_new[errors]) {
				$item[errors] = $item_new[errors];
			}
			// No errors
			else {
				// Update item
				$item = $item_new;
				
				// Save
				$this->items[$item[key]] = $item;
			}
			
			// Total
			$this->total();
		
			// Return
			return $item;
		}
		
		/**
		 * Updates an item in the cart.
		 *
		 * @param string $key The key of the item you want to update.
		 * @param array $item An array of item info we want to update.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array The item array.
		 */
		function item_update($key,$item,$c = NULL) {
			$item[key] = $key;
			return self::item_add($item,NULL,$c);
		}
		
		/**
		 * Deletes an item from the cart.
		 *
		 * @param string $key The key of the item you want to remove.
		 */
		function item_delete($key) {
			// Remove
			unset($this->items[$key]);
			
			// Total
			$this->total();
		}
		
		/**
		 * Makes sure an item meets all requirments assigned to it specifically or globally.
		 *
		 * If an error(s) occured, it'll appear in the item's 'errors' array. Example: 'errors' => array("Please select a quantity of 10 or less.")
		 *
		 * @param array $item The array of the item you want to validate.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array The array of the item, including any errors that occured during validation.
		 */
		function item_validate($item,$c = NULL) {
			// Error
			if(!$item) return;
			
			// Config
			if(!$this->x($c[debug])) $c[debug] = 0; // Debug
			
			// Quantity - none
			if(!$item[quantity][count]) {
				$item[errors][] = "Please select a quantity.";
			}
			
			// Quantity - minimum
			if($item[quantity][minimum]) {
				if($item[quantity][count] < $item[quantity][minimum]) $item[errors][] = "Please select a quantity of ".$item[quantity][minimum]." or more.";
			}
			
			// Quantity - maximum
			if($item[quantity][maximum]) {
				if($item[quantity][count] > $item[quantity][maximum]) $item[errors][] = "Please select a quantity of ".$item[quantity][maximum]." or less.";
			}
			
			// Error found, remove from cart
			//if($item[errors]) unset($this->items[$item[key]]);
			
			// Return
			return $item;
		}
		
		/**
		 * Calculates totals for an item.
		 *
		 * @param array $item The item array of the item you want to calculate the totals of.
		 * @return array The item array with totals calculated.
		 */
		function item_total($item) {
			// Reset
			$item[total] = NULL;
			$item[totals] = NULL;
			
			// Subtotal
			$item[totals][subtotal] = $item[quantity][count] * $item[price];
			
			// Discounts - needs to be a negative number
			#build this
			
			// Tax
			$rate = NULL;
			if($item[shipping][address]) { // Only charge if we're actually shipping an item
				if($item[tax][rate]) $rate = $item[tax][rate];
				else if($this->tax) $rate = $this->tax[$item[shipping][address][state]];
			}
			if($rate) {
				$item[totals][tax] = round(($item[totals][subtotal] + $item[totals][discounts]) * $rate,2);
			}
			
			// Shipping
			if($item[shipping][price]) {
				if($item[shipping][charge] == 'item') $item[totals][shipping] = $item[shipping][price]; // Per item
				else { // Per quantity
					$item[totals][shipping] = $item[shipping][price] * $item[quantity][count];
					/*$quantity = $item[quantity][count];
					if($item[quantity][unit][count]) $quantity *= $item[quantity][count]; // Shipping price doesn't get set in a quantity unit, so if we use a quantity unit, quantity must be = quantity of the selected unit x quantity in each of the selected unit
					$item[totals][shipping] = $item[shipping][price] * $quantity;*/
				}
			}
			else if($this->c[shipping][price]) { // Global
				if($this->c[shipping][charge] == 'item') $item[totals][shipping] = $this->c[shipping][price]; // Per item
				else if($this->c[shipping][charge] == 'quantity') $this->totals[shipping] += $this->c[shipping][price] * $item[quantity][count]; // Per quantity
			}
			
			// Insurance
			#build this
			
			// Handling
			if($item[handling][price]) {
				if($item[handling][charge] == 'item') $item[totals][handling] = $item[handling][price]; // Per item
				else { // Per quantity
					$item[totals][handling] = $item[handling][price] * $item[quantity][count];
					/*$quantity = $item[quantity][count];
					if($item[quantity][unit][count]) $quantity *= $item[quantity][count]; // Handling price doesn't get set in a quantity unit, so if we use a quantity unit, quantity must be = quantity of the selected unit x quantity in each of the selected unit
					$item[totals][handling] = $item[handling][price] * $quantity;*/
				}
			}
			else if($this->c[handling][price]) { // Global
				if($this->c[handling][charge] == 'item') $item[totals][handling] = $this->c[handling][price]; // Per item
				else if($this->c[handling][charge] == 'quantity') $this->totals[handling] += $this->c[handling][price] * $item[quantity][count]; // Per quantity
			}
			
			// Total
			$item[total] = $item[totals][total] = array_sum($item[totals]);
			
			// Return
			return $item;
		}
		
		/**
		 * Standardizes an item's array of data.
		 *
		 * Gives item an id if none exists
		 *
		 * @param array $item The item array you want to standardize.
		 * @return array The standardized item array.
		 */
		function item_standardize($item) {
			// Key
			if(!$item[key]) $item[key] = $this->key($item);
			
			// Weight
			if(!$item[shipping][weight]) {
				$item[shipping][weight] = array(
					'weight' => $this->shipping['default'][weight][weight],
					'unit' => $this->shipping['default'][weight][unit],
				);
			}
			if($item[shipping][weight]) {
				// Passed single weight value, turn into an array
				if(!is_array($item[shipping][weight])) {
					$item[shipping][weight] = array(
						'weight' => $item[shipping][weight]
					);	
				}
				
				// Defaults
				if(!$item[shipping][weight][weight]) $item[shipping][weight][weight] = $this->shipping['default'][weight][weight];
				if(!$item[shipping][weight][unit]) $item[shipping][weight][unit] = $this->shipping['default'][weight][unit];
				
				// Standardize unit
				if($item[shipping][weight][unit] != 'oz') {
					$item[shipping][weight][weight] = $this->convert_weight($item[shipping][weight][weight],$item[shipping][weight][unit],'oz');
					$item[shipping][weight][unit] = 'oz';
				}
			}
			
			// Dimensions
			if(!$item[shipping][dimensions]) {
				$item[shipping][dimensions] = array(
					'width' => $this->shipping['default'][dimensions][width],
					'length' => $this->shipping['default'][dimensions][length],
					'height' => $this->shipping['default'][dimensions][height],
					'unit' => $this->shipping['default'][dimensions][unit],
				);
			}
			if($item[shipping][dimensions]) {
				// Defaults
				if(!$item[shipping][dimensions][width]) $item[shipping][dimensions][width] = $this->shipping['default'][dimensions][width];
				if(!$item[shipping][dimensions][length]) $item[shipping][dimensions][length] = $this->shipping['default'][dimensions][length];
				if(!$item[shipping][dimensions][height]) $item[shipping][dimensions][height] = $this->shipping['default'][dimensions][height];
				if(!$item[shipping][dimensions][unit]) $item[shipping][dimensions][unit] = $this->shipping['default'][dimensions][unit];
				
				// Standardize unit
				if($item[shipping][dimensions][unit] != 'in') {
					$item[shipping][dimensions][width] = $this->convert_dimensions($item[shipping][dimensions][width],$item[shipping][dimensions][unit],'in');
					$item[shipping][dimensions][length] = $this->convert_dimensions($item[shipping][dimensions][length],$item[shipping][dimensions][unit],'in');
					$item[shipping][dimensions][height] = $this->convert_dimensions($item[shipping][dimensions][height],$item[shipping][dimensions][unit],'in');
					$item[shipping][dimensions][unit] = 'in';
				}
				
				// Volume
				$item[shipping][dimensions][volume] = ($item[shipping][dimensions][width] * $item[shipping][dimensions][length] * $item[shipping][dimensions][height]);
			}
			
			// Return
			return $item;
		}
		
		/**
		 * Calculates totals for the entire cart.
		 *
		 * @return double The total for all items, discounts, tax, shipping, handling, and insurance in the cart.
		 */
		function total() {
			// Reset
			$this->total = NULL;
			$this->totals = NULL;
			
			// Items - double check totals
			foreach($this->items as $key => $item) {
				$this->items[$key] = $this->item_total($item);
			}
			
			// Subtotal
			foreach($this->items as $key => $item) {
				$this->totals[subtotal] += $item[totals][subtotal];
			}
			
			// Discounts - items
			foreach($this->items as $key => $item) {
				$this->totals[discounts] += $item[totals][discounts];
			}
			// Discounts - global
			foreach($this->discounts as $key => $discount) {
				// Flat
				if($discount[type] == "flat") {
					$discount[total] = $discount[rate];
				}
				// Perecent
				if($discount[type] == "percent") {
					if($discount[keys]) {
						$total = 0;
						foreach($this->items as $k => $item) {
							if(in_array($k,$discount[keys])) {
								$total += $item[totals][subtotal];	
							}
						}
					}
					else $total = array_sum($this->totals);
					$discount[total] = round(($discount[rate] / 100) * $total,2);
				}
				
				// Negative
				$discount[total] *= -1;
				
				// Save
				$this->discounts[$key] = $discount;
				// Tootal
				$this->totals[discounts] += $discount[total];
			}
			
			// Tax
			foreach($this->items as $key => $item) {
				$this->totals[tax] += $item[totals][tax];
			}
			
			// Shipping
			/*foreach($this->items as $key => $item) { // Item defined shipping charges // Gets included in selected rate total
				if($item[totals][shipping]) {
					$this->totals[shipping] += $item[totals][shipping];
				}
			}
			if($this->shipping[charge] == 'cart') $this->totals[shipping] += $this->shipping[price]; // Globally defined shipping charge // Gets included in selected rate total*/
			if($this->shipping[selected]) {
				$this->totals[shipping] = $this->shipping[selected][totals][total];
			}
			
			// Insurance
			#build this
			
			// Handling
			foreach($this->items as $key => $item) { // Item defined handling charges
				if($item[totals][handling]) {
					$this->totals[handling] += $item[totals][handling];
				}
			}
			if($this->handling[charge] == 'cart') { // Globally defined handling charge
				$this->totals[handling] += $this->handling[price]; 
			}
			
			// Total
			$this->total = $this->totals[total] = array_sum($this->totals);
			
			//print "items - \$cart->total():".return_array($this->items);
			
			// Cache
			$this->cache_save();
		}
		
		/**
		 * Adds the given discount array to the cart, checking any restrictions contained in the discount.
		 *
		 * This returns the discount array. If an error occured (a restriction wasn't met), the array will contain an error message with the key 'error'.
		 * Example:
		 *
		 * $result = $cart->discount($discount);
		 * if($result[error]) print "Error: ".$result[error]."<br />";
		 *
		 * @param array $discount The discount array.
		 * @return array The resulting discount array, including any 'error' that occured.
		 */
		function discount_add($discount) {
			// No discount
			if(!$discount) {
				$discount[error] = "You didn't pass a discount.";
			}
			
			// Dates
			if(!$discount[error] and ($discount[date][start] or $discount[date][end])) {
				if($discount[date][start]) $start = time_format($discount[date][start],'unix'); 
				if($discount[date][end]) $end = time_format($discount[date][end],'unix'); 
				
				// Too early
				if($start and $start > time()) {
					$discount[error] = "This discount isn't valid before ".date('m/d/Y',$start).".";
				}
				// Too late
				if($end and $end < time()) {
					$discount[error] = "This discount has expired.";
				}
			}
			
			// Save
			if(!$discount[error]) {
				// Key
				if(!$discount[key]) $discount[key] = $this->key($discount);
			
				// Save
				$this->discounts[$discount[key]] = $discount;
				
				// Total
				$this->total();
				
				// Cache // Gets run in $this->total()
				//$this->cache_save();
			}
			
			// Return
			return $discount;
		}
		
		/**
		 * Removes the given discount from the cart.
		 *
		 * @param string $key The 'key' of the discount you want to remove.
		 */
		function discount_delete($key) {
			// Error
			if(!$this->x($key)) return;
			
			// Delete
			unset($this->discounts[$key]);
			
			// Total
			$this->total();
		}
		
		/**
		 * Returns and (if $array passed) stores an array of sales tax rates charged per state.
		 *
		 * Tax should be passed in the array('state' => 'taxrate') format. For example:
		 * array(
		 *		'MN' => .06875,
		 *		'NY' => .07
		 * );
		 *
		 * Note: taxrate is a decimal (.07) not a percent (7%).
		 *
		 * Tax can also be set when we intalize the class. Example:
		 * $cart = new cart(array('tax' => array('MN' => .06875,'NY' => .07)));
		 *
		 * Tax should only be charged if you have a physical store location somewhere.
		 * Moreover, tax is only charged if the customer you're shipping to is in the same state as your store.
		 * Furthermore, you must be actually shipping an item to the state of your store. If you aren't shipping the item, tax shouldn't be charged.
		 *
		 * @param array $array The array of tax rates in array('state' => 'taxrate') format. Note: taxrate is a decimal (.07) not a percent (7%). Default = NULL
		 * @return array The stored array of sales tax rates charged per state.
		 */
		function tax($array = NULL) {
			// Set
			if($array and is_array($array)) {
				// Set
				$this->tax = $array;
				
				// Cache
				$this->cache_save();
			}
			
			// Return
			return $this->tax;
		}
		
		/**
		 * Returns and (if $array passed) stores information about how shipping is charged in this cart.
		 *
		 * Example:
		 * array(
		 *		'from' => array(
		 *			'zip' => 97214, // The zip code we're shipping from
		 *			'state' => 'OR', // The code of the state we're shipping from (US only, and only used by FedEx)
		 *			'country' => 'US' // The code of the country we're shipping from
		 * 		),
		 *		'default' => array(
		 *			'weight' => array(
		 *				'weight' => '16',
		 *				'unit' => 'oz',
		 *			),
		 *			'dimensions' => array(
		 *				'width' => '12',
		 *				'length' => '8',
		 *				'height' => '2',
		 *				'unit' => 'in',
		 *			),
		 *		),
		 *		'price' => 1.50, // The shipping cost you charge
		 *		'charge' => 'cart' // What do we charge shipping on (see below)
		 * )
		 *
		 * 'charge' values:
		 * - quantity - shipping charge is applied to each quantity of each item ordered
		 * - item - shipping charge is applied once to each unique item in the cart (they will only be charged once even if the item has a quantity of 10)
		 * - cart - shipping is only charged once over all, no matter how many items are ordered [default]
		 *
		 * @param array $array The array of shipping information. Default = NULL
		 * @return array The stored array of shipping pricing.
		 */
		function shipping($array) {
			// Set
			if($array and is_array($array)) {
				// Defaults
				if($array[price] and !$array[charge]) $array[charge] = "cart";
				
				// Merge
				foreach($array as $k => $v) {
					$this->shipping[$k] = $v;
				}
				
				// Cache
				$this->cache_save();
			}
			
			// Return
			return $this->shipping;
		}
		
		/**
		 * Sets the shipping address for all items or (if $item_key is passed) for a specific item.
		 *
		 * @param array $address The array of address information.
		 * @param string $item_key The key of the item this address is assigned to. If none passed, we apply this address to all items. Default = NULL
		 */
		function shipping_address($address,$item_key = NULL) {
			// Error
			if(!$address or !$this->items) return;
			
			// Address key
			if(!$address[key]) $address[key] = md5(serialize($address));
			
			// Single item
			if($item_key) {
				$this->items[$item_key][shipping][address] = $address;	
			}
			// All items
			else {
				foreach($this->items as $key => $item) {
					$this->items[$key][shipping][address] = $address;	
				}
			}
			
			// All addresses
			$this->shipping[addresses] = NULL;
			foreach($this->items as $key => $item) {
				if($item[shipping][address][key]) {
					if(!$this->shipping[addresses][$item[shipping][address][key]]) $this->shipping[addresses][$item[shipping][address][key]] = $item[shipping][address];
					$this->shipping[addresses][$item[shipping][address][key]][items][] = $key;
				}
			}
			
			// Total - in case we charge tax in state they're shipping to
			$this->total();
				
			// Cache // Gets handled in $this->total()
			//$this->cache_save();
		}
		
		/**
		 * Places items into packages based upon the address they're getting sent to.
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of packages for this cart's items. This is also stored in $this->shipping[packages].
		 */
		function shipping_package($c) {
			// Error
			if(!$this->items) return;
			
			// Config
			if(!$this->x($c[debug])) $c[debug] = 0; // Debug
			
			// Packages
			$packages = NULL;
			$packages_key = 0;
			
			// Items
			$items = $this->items;
			
			// Pre-defined shipping price
			foreach($items as $key => $item) {
				if($address_key = $item[shipping][address][key]) {
					// Shipping already defined, don't need to package
					if($item[totals][shipping]) unset($items[$key]);
				}
			}
			
			// Pre-packaged items
			if(count($items)) {
				foreach($items as $key => $item) {
					if($address_key = $item[shipping][address][key]) {
						// Total - custom (pre-defined)
						if($item[shipping][packages]) {
							foreach($item[shipping][packages] as $package) {
								// Package
								$package_key = $address_key."-".$packages_key;
								$packages[$package_key] = $package;
								
								// Addresses
								$packages[$package_key][address][to] = $this->shipping[addresses][$address_key];
								$packages[$package_key][address][from] = $this->shipping[from];
								
								// Item
								$packages[$package_key][items][] = $item[key];
							
								// Package key counter
								$packages_key += 1;
							}
							
							// Package defined, don't need to package
							unset($items[$key]);
						}
					}
				}
			}
			
			// Boxes - Loop through twice. First time check against overall dimensions, second time we don't (bit hacky, but better than just putting in one big box)
			for($x = 0;$x <= 1;$x++) {
				if(count($items) and $this->shipping[boxes]) {
					// Items
					$addresses = NULL;
					foreach($items as $key => $item) {
						if($address_key = $item[shipping][address][key]) {
							// Total - custom (pre-defined)
							if($item[totals][shipping]) {
								$addresses[$address_key][] = array(
									'key' => $key,
									'item' => $item,
								);
							}
							else {
								$addresses[$address_key][] = array(
									'dimensions' => array(
										$item[shipping][dimensions][width],
										$item[shipping][dimensions][length],
										$item[shipping][dimensions][height],
									),
									'weight' => $item[shipping][weight][weight],
									'quantity' => $item[quantity][count],
									'key' => $key,
									'item' => $item,
								);
							}
						}
					}
					
					// Package
					if(count($addresses)) {
						include_class('packer');
						foreach($addresses as $address_key => $address_items) {
							$packer = new packer(array('dimensions_check' => ($x == 0 ? 1 : 0),'debug' => $c[debug]));
							$packer->boxes($this->shipping[boxes]);
							$packer->items($address_items);
							$address_packages = $packer->pack();
							foreach($address_packages as $k => $v) {
								// Package
								$package_key = $address_key."-".$packages_key;
								$package = NULL;
								
								// Addresses
								$package[address][to] = $this->shipping[addresses][$address_key];
								$package[address][from] = $this->shipping[from];
								
								// Weight
								$package[weight][weight] = $v[weight];
								$package[weight][unit] = 'oz';
								// Dimensions
								$package[dimensions][unit] = 'in';
								$package[dimensions][width] = $v[box][dimensions][0];
								$package[dimensions][length] = $v[box][dimensions][1];
								$package[dimensions][height] = $v[box][dimensions][2];
								
								// Quantity
								$package[quantity][count] = $v[quantity];
								
								// Items
								foreach($v[items] as $item) {
									$package[items][] = $item[key];
									
									$items[$item[key]][quantity][count] -= $item[quantity];
									if($items[$item[key]][quantity][count] <= 0) unset($items[$item[key]]);
								}
								
								// Store
								$packages[$package_key] = $package;
								
								// Package key counter
								$packages_key += 1;
							}
						}
					}
				}
			}
			
			// No boxes, put in 1 box
			if(count($items)) {
				// Items
				foreach($items as $key => $item) {
					// Standardize // Not sure why this was ever called, just screws up $this->items as $item is method specific, meaning it's changed, but shouldn't be stored globally
					//$items[$key] = $this->items[$key] = $this->item_standardize($item);
					//$item = $items[$key];
					
					// Have address?
					if($address_key = $item[shipping][address][key]) {
						// Package
						$package_key = $address_key."-".$packages_key;
						$package = $packages[$package_key];
						$package_new = ($package ? 0 : 1);
						
						// Addresses
						$package[address][to] = $item[shipping][address];
						$package[address][from] = $this->shipping[from];
						
						// Total - custom (pre-defined)
						if($item[totals][shipping]) {
							$package[totals][custom] += $item[totals][shipping];
						}
						else {
							// Weight
							$package[weight][weight] += ($item[shipping][weight][weight] * $item[quantity][count]);
							$package[weight][unit] = $item[shipping][weight][unit];
							// Dimensions
							$package[dimensions][volume] += ($item[shipping][dimensions][volume] * $item[quantity][count]);
							$package[dimensions][unit] = $item[shipping][dimensions][unit];
							if($package_new and $item[quantity][count] == 1) { // First item in package, use item's width/length/height for package dimensions
								$package[dimensions][width] = $item[shipping][dimensions][width];
								$package[dimensions][length] = $item[shipping][dimensions][length];
								$package[dimensions][height] = $item[shipping][dimensions][height];
							}
							else { // Second+ item in package, must use combined volume for package dimensions
								$volume_side = round(pow(($package[dimensions][volume] * $item[quantity][count]),1/3),2);
								$package[dimensions][width] = $volume_side;
								$package[dimensions][length] = $volume_side;
								$package[dimensions][height] = $volume_side;
							}
						}
						
						// Quantity
						$package[quantity][count] += $item[quantity][count];
						
						// Items
						$package[items][] = $key;
						
						// Store
						$packages[$package_key] = $package;
							
						// Package key counter
						$packages_key += 1;
					}
				}
			}
			
			// Save
			$this->shipping[packages] = $packages;
			
			// Cache
			$this->cache_save();
			
			// Debug
			debug("packages:".return_array($packages),$c[debug]);
			
			// Return
			return $packages;
		}
		
		/**
		 * Calculates shipping rates for all 'packages' in this cart.
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of shipping rates in array('company|code' => array('code' => code,'company' => company,'rate' => rate)) foramt.
		 */
		function shipping_rates($c) {
			// Error
			if(!$this->items) return;
			
			// Config
			if(!$this->x($c[debug])) $c[debug] = 0; // Debug
			
			// Package
			$this->shipping_package(array('debug' => $c[debug]));
			if(!$this->shipping[packages]) {
				debug("No packages",$c[debug]);
				return;
			}
			
			// Rates
			$rates = NULL;
			$this->shipping[rates] = NULL;
			
			// Dynamic
			$dynamic = 0;
			if($this->shipping[methods]) {
				// Methods
				$shipping_methods = NULL;
				$shipping_methods_keys = NULL;
				foreach($this->shipping[methods] as $method_key => $method) {
					if(!$method[code]) $this->shipping[methods][$method_key][code] = $method[code] = ($method[company] ? $method[company] : "custom")."-".$method_key;
					if($method[active] == 1) {
						$shipping_methods[strtolower($method[company])][] = $method[code];
						$shipping_methods_keys[strtolower($method[company])][$method[code]] = $method_key;
					}
				}
				
				// Group packages by from/to address
				$addresses = NULL;
				foreach($this->shipping[packages] as $package_key => $package) {
					if($package[weight][weight] > 0 and $package[address][from] and $package[address][to]) {
						$key = md5(serialize($package[address][from]).serialize($package[address][to]));
						$addresses[$key][$package_key] = $package;
					}
				}
				if($addresses) {
					// Dynamic required
					$dynamic = 1;
				
					// Class
					include_class('shipping');
					
					// Loop through addresses
					$x = 0;
					foreach($addresses as $address => $packages) {
						$shipping = NULL;
						foreach($packages as $package_key => $package) {
							// Class
							if(!$shipping) {
								$shipping_config = array(
									'from_zip' => $package[address][from][zip],
									'from_state' => $package[address][from][state],
									'from_country' => $package[address][from][country],
									'to_zip' =>  $package[address][to][zip],
									'to_state' => $package[address][to][state],
									'to_country' => $package[address][to][country],
									'to_residential' => ($package[address][to][building] && $package[address][to][buildling] != "residential" ? 0 : 1),
									'debug' => $c[debug],
								);
								$shipping = new shipping($shipping_config);
							}
							
							// Package
							$shipping_package = array(
								'key' => $package_key,
								'weight' => $package[weight][weight],
								'weight_unit' => $package[weight][unit],
							);
							if($package[dimensions][width] and $package[dimensions][length] and $package[dimensions][height]) {
								$shipping_package[dimensions_width] = $package[dimensions][width];
								$shipping_package[dimensions_length] = $package[dimensions][length];
								$shipping_package[dimensions_height] = $package[dimensions][height];
								$shipping_package[dimensions_unit] = $package[dimensions][unit];
							}
							$shipping->package($shipping_package);
						}
						
						// Debug
						debug("shipping class (configured): ".return_array($shipping),$c[debug]);
						
						// Methods
						foreach($shipping_methods as $company => $methods) {
							$shipping_company_rates = NULL;
							// Company
							if($company) {
								// Company - class - config
								$shipping_company_config = g('config.cart.shipping.companies.'.$company); // Global
								$shipping_company_config = array_merge_associative($shipping_company_config,$this->shipping[companies][$company]); // Object specific
								$shipping_company_config[methods] = $methods;
								// Company - class
								$shipping_company_class = "shipping_".$company;
								if(class_exists($shipping_company_class)) {
									$shipping_company = new $shipping_company_class($shipping_company_config);
									$shipping->company($shipping_company);
									
									// Company - rates
									$shipping_company_rates = $shipping->calculate();
								}
							}
							// Custom
							else {
								foreach($methods as $method) {
									// Method info
									$method_key = $shipping_methods_keys[$company][$method];
									$method_array = $this->shipping[methods][$method_key];
									debug("custom method: ".return_array($method_array),$c[debug]);
									
									// Address
									$package_first = array_first($packages);
									$address_array = $package_first[address][to];
									
									// Use?
									$use = 1;
									debug("country: ".$address_array[country].", state: ".$address_array[state].", countries: ".return_array($method_array[countries]).", countries_exclude: ".return_array($method_array[countries_exclude]),$c[debug]);
									if(
										$method_array[countries]
										and (
											!$method_array[countries][$address_array[country]]
											or (
												$method_array[countries][$address_array[country]] != "all"
												and !in_array($address_array[state],$method_array[countries][$address_array[country]])
											)
										)
									) {
										$use = 0;
									}
									else if(
										$method_array[countries_exclude]
										and (
											$method_array[countries_exclude][$address_array[country]]
											and (
												$method_array[countries_exclude][$address_array[country]] == "all"
												or in_array($address_array[state],$method_array[countries_exclude][$address_array[country]])
											)
										)
									) {
										$use = 0;
									}
										
									// Save
									if($use) {
										foreach($packages as $package_key => $package) {
											$shipping_company_rates[rates][$method] += ($method_array[rate] * $package[quantity][count]);
											$shipping_company_rates[packages][$package_key][package] = $package;
											$shipping_company_rates[packages][$package_key][rates][$method] += ($method_array[rate] * $package[quantity][count]);
										}
									}
								}
							}
							debug("rates for company ".($company ? $company : "custom").": ".return_array($shipping_company_rates),$c[debug]);
							
							if($x == 0) { // First time through, define what methods have rates
								if($shipping_company_rates) {
									// Total
									foreach($shipping_company_rates[rates] as $method => $rate) {
										if($rate) {
											// Method array
											$method_key = $shipping_methods_keys[$company][$method];
											$method_array = $this->shipping[methods][$method_key];
											$method_array[key] = $method_key;
										
											// Total
											$rates[$method_key] = $method_array;
											$rates[$method_key][totals][dynamic] += $rate;
											$rates[$method_key][totals][total] += $rate;
										}
									}
									// Packages
									foreach($shipping_company_rates[packages] as $package) {
										foreach($package[rates] as $method => $rate) {
											// Method array
											$method_key = $shipping_methods_keys[$company][$method];
											$method_array = $this->shipping[methods][$method_key];
											$method_array[key] = $method_key;
											
											// Total - note, not currently saving this in db. Saving $item[shipping][price] (which is the custom, pre-defined price). Would need new functionality to get it from package to item array (probably in shipping_selected() method) and store it in a new column(?)
											$this->shipping[packages][$package[package][key]][methods][$method_key] = $method_array;
											$this->shipping[packages][$package[package][key]][methods][$method_key][totals][dynamic] += $rate;
											$this->shipping[packages][$package[package][key]][methods][$method_key][totals][total] += $rate;
										}
									}
								}
							}
							else if($rates) { // 2nd+ time through, make sure only using previous rates and make sure they all have a rate for this package
								foreach($rates as $method_key => $method_array) {
									if(strtolower($method_array[company]) == $company) {
										// Rate existed previously and in this packages rates
										if($rate = $shipping_company_rates[rates][$method_array[code]]) {
											// Total
											$rates[$method_key][totals][dynamic] += $rate;
											$rates[$method_key][totals][total] += $rate;
											
											// Packages - note, not currently saving this in db. Saving $item[shipping][price] (which is the custom, pre-defined price). Would need new functionality to get it from package to item array (probably in shipping_selected() method) and store it in a new column(?)
											foreach($shipping_company_rates[packages] as $package) {
												$rate = $package[rates][$method_array[code]];
												$this->shipping[packages][$package[package][key]][methods][$method_key][totals][dynamic] += $rate;
												$this->shipping[packages][$package[package][key]][methods][$method_key][totals][total] += $rate;
											}
										}
										// Reviously existing rate doesn't exist in this package, remove it
										else {
											unset($rates[$method_key]);	
										}
									}
								}
							}
						}
						
						// Counter - so we know when we're on a 2nd address (make sure we get results for all addresses, otherwise can't use that method)
						$x++;
					}
				}
				
				
				/* Old method, called calculate on each package (new method adds all packages to shipping class then calculates) */
				/*// Loop through packages
				$x = 0;
				foreach($this->shipping[packages] as $package_key => $package) {
					if($package[weight][weight] > 0 and $package[address][from] and $package[address][to]) {
						// Dynamic required
						$dynamic = 1;
						
						// Class config
						$shipping_config = array(
							'from_zip' => $package[address][from][zip],
							'from_state' => $package[address][from][state],
							'from_country' => $package[address][from][country],
							'to_zip' =>  $package[address][to][zip],
							'to_state' => $package[address][to][state],
							'to_country' => $package[address][to][country],
							'to_residential' => ($package[address][to][building] && $package[address][to][buildling] != "residential" ? 0 : 1),
							'weight' => $package[weight][weight],
							'weight_unit' => $package[weight][unit],
							'debug' => $c[debug],
						);
						if($package[dimensions][width] and $package[dimensions][length] and $package[dimensions][height]) {
							$shipping_config[dimensions_width] = $package[dimensions][width];
							$shipping_config[dimensions_length] = $package[dimensions][length];
							$shipping_config[dimensions_height] = $package[dimensions][height];
							$shipping_config[dimensions_unit] = $package[dimensions][unit];
						}
						debug("shipping config: ".return_array($shipping_config),$c[debug]);
					
						// Class
						include_class('shipping');
						$shipping = new shipping($shipping_config);
						
						// Methods
						foreach($shipping_methods as $company => $methods) {
							$shipping_company_rates = NULL;
							// Company
							if($company) {
								// Company - class - config
								$shipping_company_config = g('config.cart.shipping.companies.'.$company); // Global
								$shipping_company_config = array_merge_associative($shipping_company_config,$this->shipping[companies][$company]); // Object specific
								$shipping_company_config[methods] = $methods;
								// Company - class
								$shipping_company_class = "shipping_".$company;
								if(class_exists($shipping_company_class)) {
									$shipping_company = new $shipping_company_class($shipping_company_config);
									$shipping->company($shipping_company);
									
									// Company - rates
									$shipping_company_rates = $shipping->calculate();
								}
							}
							// Custom
							else {
								foreach($methods as $method) {
									// Method info
									$method_key = $shipping_methods_keys[$company][$method];
									$method_array = $this->shipping[methods][$method_key];
									debug("custom method: ".return_array($method_array),$c[debug]);
									
									// Use?
									$use = 1;
									debug("country: ".$package[address][to][country].", state: ".$package[address][to][state].", countries: ".return_array($method_array[countries]).", countries_exclude: ".return_array($method_array[countries_exclude]),$c[debug]);
									if(
										$method_array[countries]
										and (
											!$method_array[countries][$package[address][to][country]]
											or (
												$method_array[countries][$package[address][to][country]] != "all"
												and !in_array($package[address][to][state],$method_array[countries][$package[address][to][country]])
											)
										)
									) {
										$use = 0;
									}
									else if(
										$method_array[countries_exclude]
										and (
											$method_array[countries_exclude][$package[address][to][country]]
											and (
												$method_array[countries_exclude][$package[address][to][country]] == "all"
												or in_array($package[address][to][state],$method_array[countries_exclude][$package[address][to][country]])
											)
										)
									) {
										$use = 0;
									}
										
									// Save
									if($use) $shipping_company_rates[$method] = ($method_array[rate] * $package[quantity][count]);
								}
							}
							debug("rates for company ".($company ? $company : "custom").": ".return_array($shipping_company_rates),$c[debug]);
							
							if($x == 0) { // First time through, define what methods have rates
								if($shipping_company_rates) {
									foreach($shipping_company_rates as $method => $rate) {
										if($rate) {
											// Method array
											$method_key = $shipping_methods_keys[$company][$method];
											$method_array = $this->shipping[methods][$method_key];
											$method_array[key] = $method_key;
											
											// Package
											$package[methods][$method_key] = $method_array;
											$package[methods][$method_key][totals] = array(
												'dynamic' => $rate,
												'total' => $rate
											);
										
											// Global
											$rates[$method_key] = $method_array;
											$rates[$method_key][totals][dynamic] += $rate;
											$rates[$method_key][totals][total] += $rate;
										}
									}
								}
							}
							else if($rates) { // 2nd+ time through, make sure only using previous rates and make sure they all have a rate for this package
								foreach($rates as $method_key => $method_array) {
									if(strtolower($method_array[company]) == $company) {
										// Rate existed previously and in this packages rates
										if($rate = $shipping_company_rates[$method_array[code]]) {
											// Package
											$package[methods][$method_key] = $method_array;
											$package[methods][$method_key][totals] = array(
												'dynamic' => $rate,
												'total' => $rate
											);
											
											// Global
											$rates[$method_key][totals][dynamic] += $rate;
											$rates[$method_key][totals][total] += $rate;
										}
										// Reviously existing rate doesn't exist in this package, remove it
										else {
											unset($rates[$method_key]);	
										}
									}
								}
							}
						}
					}
					
					// Update package
					$this->shipping[packages][$package_key] = $package;
					
					// Package counter
					$x++;
				}*/
			}
			
			// Have dynamic rates (or no packages used dynamic rates)
			if($rates or !$dynamic) {
				// Custom (pre-defined) shipping
				foreach($this->shipping[packages] as $package_key => $package) {
					if($package[totals][custom]) {
						if(!$rates) {
							$this->shipping[methods][custom] = array(
								'name' => 'Standard Shipping',
							);
							$rates[custom] = array(
								'key' => 'custom',
								'name' => 'Standard Shipping'
							);
						}
						foreach($rates as $method_key => $method_array) {
							$rates[$method_key][totals][custom] += $package[totals][custom];
							$rates[$method_key][totals][total] += $package[totals][custom];
						}
					}
				}
				
				// Global shipping
				if($this->shipping[price] and $this->shipping[charge] == "cart") {
					if(!$rates) {
						$this->shipping[methods][custom] = array(
							'name' => 'Standard Shipping',
						);
						$rates[custom] = array(
							'key' => 'custom',
							'name' => 'Standard Shipping'
						);
					}
					foreach($rates as $method_key => $method_array) {
						$rates[$method_key][totals][custom] += $this->shipping[price];
						$rates[$method_key][totals][total] += $this->shipping[price];
					}
				}
				
				// Sort
				usort($rates,'shipping_rates_sort');
			}
			
			// Save
			$this->shipping[rates] = $rates;
			
			// Debug
			debug("rates:".return_array($rates),$c[debug]);
			
			// Cache
			$this->cache_save();
			
			// Return
			return $rates;
		}
		
		/**
		 * Returns array of shipping rates for use as form options (company|code|rate => name)
		 *
		 * @return array An options array of shipping rates to be used in a form select or radio input.
		 */
		function shipping_rates_options() {
			// Options
			$options = NULL;
			if($this->shipping[rates]) {
				foreach($this->shipping[rates] as $rate) {
					$options[$rate[key]."|".$rate[totals][total]] = string_price($rate[totals][total])." - ".($rate[company] ? $rate[company]." " : "").$rate[name]	;
				}
			}
			
			// Return
			return $options;
		}
		
		/**
		 * Stores the shipping method that has been selected by the customer.
		 *
		 * Note: the $key is the method's original key (the key it had in the array when you defined $this->shipping[methods]).
		 * This is not the array key it has in $this->shipping[rates] as those are ordered by rate (lowest to highest).
		 * It is, however, stored within a rates array in $this->shipping[rates], for example: $this->shipping[rates][0][key] = 123 would mean the key is "123".
		 *
		 * @param string $key The shipping method key.
		 * @param return array The selected shipping method array including company, name, code, and totals.
		 */
		function shipping_selected($key) {
			// Get selected
			if($this->shipping[rates]) {
				foreach($this->shipping[rates] as $k => $v) {
					if($v[key] == $key) {
						$this->shipping[selected] = $v;
						break;	
					}
				}
			}
			
			// Total
			$this->total();
			
			// Return
			return $this->shipping[selected];
		}
		
		/**
		 * Returns and (if $array passed) stores information about how handling is charged in this cart.
		 *
		 * Example:
		 * array(
		 *		'price' => 1.50, // The handling cost you charge
		 *		'charge' => 'cart' // What do we charge handling on (see below)
		 * )
		 *
		 * 'charge' values:
		 * - quantity - handling charge is applied to each quantity of each item ordered
		 * - item - handling charge is applied once to each unique item in the cart (they will only be charged once even if the item has a quantity of 10)
		 * - cart - handling is only charged once over all, no matter how many items are ordered [default]
		 *
		 * @param array $array The array of handling information. Default = NULL
		 * @return array The stored array of handling pricing.
		 */
		function handling($array) {
			// Set
			if($array and is_array($array)) {
				// Defaults
				if($array[price] and !$array[charge]) $array[charge] = "cart";
				
				// Merge
				foreach($array as $k => $v) {
					$this->handling[$k] = $v;
				}
				
				// Cache
				$this->cache_save();
			}
			
			// Return
			return $this->handling;
		}
		
		/**
		 * Returns and (if $currency passed) stores the code of the currency that all prices in this cart are in.
		 *
		 * @param string $currency The code of the currency used in the cart (ex: USD). Default = NULL
		 * @return string The code of the currency that all prices in this cart are in.
		 */
		function currency($currency = NULL) {
			// Set
			if($currency) {
				// Set
				$this->currency = $currency;
				
				// Cache
				$this->cache_save();
			}
			
			// Return
			return $this->currency;
		}
		
		/**
		 * Returns and (if $value passed) stores a custom value for the given $key.
		 *
		 * Example:
		 * $cart->custom('mycustomkey','My Custom Value'); // Sets the custom variable 'mycustomkey' to 'My Custom Value'.
		 * $custom_value = $cart->custom('mycustomkey'); // Returns stored custom value of 'My Custom Value'.
		 *
		 * Custom key/value pairs can also be set when we intalize the class. Example:
		 * $cart = new cart(array('custom' => array('mycustomkey' => 'My Custom Value','a2ndkey' => 123)));
		 *
		 * @param string $key The key of the custom variable.
		 * @param mixed $value The value you want to set this key to. Default = NULL
		 * @return mixed The stored custom value.
		 */
		function custom($key,$value = NULL) {
			// Set
			if($this->x($value)) {
				// Save
				$this->custom[$key] = $value;
				
				// Cache
				$this->cache_save();
			}
			
			// Return
			return $this->custom[$key];
		}
		
		/**
		 * Returns the number of items currently in the cart.				
		 * 
		 * @return int The number of items in the cart.
		 */
		function count() {
			// Count
			$count = 0;
			foreach($this->items as $k => $v) {
				$count += $v[quantity][count];	
			}
			
			// Return
			return $count;
		}
	
		/**
		 * Curls a given url and returns the contents.
		 *
		 * Configuration values (key, type, default - description):
		 * - follow, boolean, 1 - Do you want to follow to the location the URL might send you to?
		 * - header, boolean, 0 - Do you want to include headers in the returned results?
		 *
		 * @param string $url The URL you want to curl.
		 * @param string|array $post Either a string of data or an array of data we want to POST to the URL. Default = NULL
		 * @param array $c An array of configuration values. Deafult = NULL 
		 * @return string The content returned by the curled URL. 
		*/
		function curl($url,$post = NULL,$c = NULL) {
			// Config
			if(!$this->x($c[follow])) $c[follow] = 1;
			if(!$this->x($c[header])) $c[header] = 0;
			
			// Curl
			$ch = curl_init();
			curl_setopt($ch,CURLOPT_URL,$url);
			curl_setopt($ch,CURLOPT_FOLLOWLOCATION,$c[follow]);
			curl_setopt($ch,CURLOPT_HEADER,$c[header]);
			curl_setopt($ch,CURLOPT_TIMEOUT,30);
			curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
			if($post) {
				curl_setopt($ch,CURLOPT_POST,1);  
				curl_setopt($ch,CURLOPT_POSTFIELDS,$post);
			} 
			curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,0);  
			curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,0);
			curl_setopt($ch,CURLOPT_USERAGENT,$_SERVER['HTTP_USER_AGENT']);
			
			// Execute
			$contents = curl_exec($ch);
			
			// Error
			if(!$contents) {
				$this->debug("Curl error: ".curl_error($ch)." (".curl_errno($ch).")");
			}
			
			// Close
			curl_close($ch);
			
			// Return
			return $contents;
		}
		
		/**
		 * Determines and returns the IP address for the current user.
		 *
		 * @return string The IP address for the current user.
		 */
		function ip() {
			// Check ip from share internet
			if(!empty($_SERVER['HTTP_CLIENT_IP'])) $ip = $_SERVER['HTTP_CLIENT_IP'];
			// Check ip is pass from proxy
			elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
			// Default
			else $ip = $_SERVER['REMOTE_ADDR'];
			
			return $ip;
		}
		
		/**
		 * Determines if the passed variable has a value associated with it.
		 * 
		 * Returns true if value exists or false if it's empty:
		 * - 0 = true
		 * - "0" = true
		 * - "false" = true
		 * - false = false
		 * - "" = false
		 * - NULL = false
		 * - "NULL" = true
		 * - true = true
		 * - "true" = true
		 * 
		 * Also works for arrays.
		 * 
		 * @param mixed $value The value we want to check against
		 * @return boolean Whether or not the variable has a value associated with it
		 */
		function x($value) {
			if(is_array($value)) $return = true;
			else if(is_object($value)) $return = true;
			else if(strlen($value) > 0) $return = true;
			else $return = false;
			return $return;
		}
		
		/**
		 * Merges existing item with new array for that item.
		 *
		 * If a conflict occurs (same key), we'll use the new item's value.
		 *
		 * @param array $item_1 The old item we want to merge with the new item.
		 * @param array $item_2 The new item we want to merge with the old item.
		 * @return array The merged item array.
		 */
		function array_merge_associative($item_1,$item_2) {
			foreach($item_2 as $k => $v) {
				if(array_key_exists($k,$item_1) && is_array($v)) $item_1[$k] = $this->array_merge_associative($item_1[$k],$item_2[$k]);
				else $item_1[$k] = $v;
			}
			return $item_1;
		}
		
		/**
		 * Converts given amount from one currency to another using Google's calculator.					
		 * 
		 * @param int $amount The amount you want to convert.
		 * @param string $from The code of the amount's current currency (example: USD).
		 * @param string $to The code of the currency you want to convert the amount to (example: GBP).
		 * @param array $c An array of configuration values. Default = NULL
		 * @return int The converted amount.
		 */
		function convert_currency($amount,$from,$to,$c = NULL) {
			// Error
			if(!$amount or !$from or !$to) return;
			
			// Same currency
			if($from == $to) return $amount;
			
			// Config
			if(!$this->x($c['round'])) $c['round'] = 1; // Round the currency rate to 2 decimals
			if(!$this->x($c[cache])) $c[cache] = 1; // Cache and use cached results
			
			// Debug
			$this->debug("<b>".__CLASS__."->".__FUNCTION__."($amount,$from,$to);</b>");
			$this->debug("c:".return_array($c));
	
			// Cached
			/*if($c[cache]) {
				if($rate = cache_get('payments/currency/'.$from.'->'.$to)) {
					// Debug
					$this->debug('cached rate: '.$rate);
					
					// Convert
					$amount_converted = $amount / $rate; 
					
					// Round
					if($c['round'] == 1) $amount_converted = round($amount_converted,2); 
					
					// Return
					return $amount_converted;
				}
			}*/
			
			// Convert
			$url = "http://www.google.com/ig/calculator?hl=en&q=".$amount.$from."=?".$to;
			$response = $this->curl($url);
			$obj = json_decode($response);
			$ex = explode(' ',$obj->rhs);
			$amount_converted = $ex[0];
			$rate = $amount / $amount_converted;
			
			// Debug
			$this->debug("url: ".$url);
			$this->debug("results: <xmp>".$response."</xmp>");
			$this->debug("object:".return_array($obj));
			$this->debug("exploded:".return_array($ex));
			$this->debug("rate:".$rate);
			$this->debug("amount converted:".$amount_converted);
			
			// Round
			if($c['round'] == 1) $amount_converted = round($amount_converted,2);
			
			// Cache
			/*if($c[cache] == 1) {
				cache_save('payments/currency/'.$from.'->'.$to,$rate);
			}*/
			
			// Return
			return $amount_converted;
		}
	
		/**
		 * Converts the given weight from its old unit to a new unit.
		 *
		 * @param double $weight The current weight.
		 * @param string $old_unit The unit the current weight is in: gram, oz, lb, kg.
		 * @param string $new_unit The unit we want the new weight to be in: gram, oz, lb, kg.
		 * @param double $minimum The minimum value to return for the new weight. Defualt = .001
		 */
		function convert_weight($weight,$old_unit,$new_unit,$minimum = .001) {
			// Different unit?
			if($old_unit != $new_unit) {
				// Conversion values
				$units['oz'] = 1;
				$units['lb'] = 0.0625;
				$units['gram'] = 28.3495231;
				$units['kg'] = 0.0283495231;
				
				// Convert to ounces (if not already)
				if($old_unit != "oz") $weight = $weight / $units[$old_unit];
				
				// Convert to new unit
				$weight = $weight * $units[$new_unit];
			}
			
			// Minimum weight
			if($weight < $minimum) $weight = $minimum;
			
			// Return new weight
			$weight = round($weight,2);
			return $weight;
			
		}
		
		/**
		 * Converts the given dimension from its old unit to a new unit.
		 *
		 * @param double $dimension The current dimension.
		 * @param string $old_unit The unit the current dimension is in: cm, in, ft
		 * @param string $new_unit The unit we want the new dimension to be in: cm, in, ft
		 * @param double $minimum The minimum value to return for the new dimension. Defualt = .001
		 */
		function convert_dimensions($dimension,$old_unit,$new_unit,$minimum = .001) {
			// Different unit?
			if($old_unit != $new_unit) {
				// Conversion values
				$units['in'] = 1;
				$units['cm'] = 2.54;
				$units['ft'] = 0.083333;
				
				// Convert to inches (if not already)
				if($old_unit != "in") $dimension = $dimension / $units[$old_unit];
				
				// Convert to new unit
				$dimension = $dimension * $units[$new_unit];
			}
				
			// Minimum dimension
			if($dimension < $minimum) $dimension = $minimum;
			
			// Return new dimension
			return round($dimension,2);
		}
		
		/**
		 * Prints the given debugging message if debugging is turned on.
		 * 
		 * @param string $message The debugging message we want to display.
		 * @param boolean $debug_local When we call $this->debug() we can indicate whether debugging is turned on within that specific method (locally). Default = 0
		 */
		function debug($message,$debug_local = 0) {
			$debug = 0;
			if($debug_local) $debug = 1;
			else if($this->c[debug]) $debug = 1;
			
			if($debug) print $message."<br />";
		}
		
		/**
		 * Clears contents of the cart.
		 */
		function clear() {
			// Clear
			unset(
				$this->items,
				$this->total,
				$this->totals,
				$this->discounts,
				$this->shipping[addresses],
				$this->shipping[packages],
				$this->shipping[rates],
				$this->shipping[selected],
				$this->custom
			);
			
			// Save
			$this->cache_save();
		}
		
		
		/**
		 * Retrieves and restores the cached cart object (if one exists).
		 */
		function cache_get() {
			// Error
			if(!$this->cache_key) return;
			
			// Cache
			//$cache = $_SESSION[$this->cache_key]; // Session
			$cache = cache_get('cart/'.$this->cookie().'/'.$this->cache_key,array('life' => $this->c[cache_life],'force' => 1)); // Cache
			
			// Exists?
			if($cache) {
				// Yes, restore
				foreach($cache as $k => $v) {
					$this->$k = $v;
				}
			}	
		}
		
		/**
		 * Saves current cart to the cache.
		 */
		function cache_save() {
			// Error
			if(!$this->cache_key) return;
			
			// Save
			//$_SESSION[$this->cache_key] = $this; // Session
			cache_save('cart/'.$this->cookie().'/'.$this->cache_key,$this); // Cache
		}
		
		/**
		 * Gets (and generates, if it doesn't exist) this customer's cart identifier stored in the cookies.
		 *
		 * @return string The cart cookie for this customer.
		 */
		function cookie() {
			// Cookie
			$cookie = $this->cookie_get('cart');
			if(!$cookie) {
				$cookie = md5($this->ip()."|".$_SERVER['HTTP_USER_AGENT']."|".$_SESSION['session']['id']);
				$this->cookie_save('cart',$cookie);
			}
			
			// Return
			return $cookie;
		}
	}
}
	
if(!function_exists('shipping_rates_sort')) {	
	/**
	 * Sorts shipping rates based upon the rate, lowest to highest.
	 *
	 * @param array $a The first shipping rate array.
	 * @param array $b The second shipping rate array.
	 * @return int Whether or not $a > $b (-1 = less than, 0 = equal to, 1 = greater than)
	 */
	function shipping_rates_sort($a,$b) {
		if($a[totals][total] == $b[totals][total]) {
			return 0;
		}
		return ($a[totals][total] < $b[totals][total]) ? -1 : 1;
	}
}