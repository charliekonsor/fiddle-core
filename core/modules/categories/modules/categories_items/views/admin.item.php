<?php
// Parent
$parent_count = count($this->page->parents);
$parent = $parent_count - 3;

// No parent module, redirect to area home page
if(!$this->page->parents[$parent][module] or $this->page->parents[$parent][module] == "categories") redirect(DOMAIN.$this->area);
// Child module, but missing parent id, redirect to parent home page
if($this->module_class->v('parent') and !$this->page->parents[last][id]) {
	$parent_module = module::load($this->module_class->v('parent'));
	redirect($parent_module->url('home',$this->area));
}

// Permissions
if($this->permission('edit',1)) {
	// Heading
	print $this->heading();
	// Buttons
	print $this->heading_buttons();
	
	// Form
	$form = $this->item_class->form(array('type' => $this->type,'redirect' => urldecode($_GET['redirect']))); // Form
	$form->hidden('module_code',$this->page->parents[$parent][module]); // Module
	if($this->module_class->v('parent') and $this->module_class->v('db.parent_id') and !$this->item_class->parent_id) $form->hidden($this->module_class->v('db.parent_id'),$this->page->parents[last][id]); // Parent
	print $form->render(); // Render
}
?>