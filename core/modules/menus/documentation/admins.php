<?php
print "
<h1>Menus</h1>
Menus, of course, are the tool by which your users can navigate your website.  As such, they're quite important.<br /><br />

Any menu(s) your site uses should already be set up for you (you can see them on the main 'Menus' page in the admin: <a href='".DOMAIN."admin/menus'>".DOMAIN."admin/menus</a>). All you have to do is add, edit, or delete 'menu links'.<br /><br />

Before we get started, though, I just want to make a distiniction between 'Menus' and 'Menu Links'.<br /><br />

<b>Menus</b><br />
A menu isn't the links in a menu, but is instead the container that holds the links.  For example, you may have a 'Main Menu' and a 'Footer Menu'.  Each will have it's own set of 'Menu Links', but they, in themseleves, are just containers for those menu links.<br /><br />

<b>Menu  Links</b><br />
Once we have menu, we can then add 'Menu Links' to that menu.  A 'Menu Link' is a link that appears within a menu.  It is the management of menu links that you'll usually be dealing with, so let's find out how you do that.<br /><br />

<h2>Managing Menu Links</h2>
To get started, go to the menus page (<a href='".DOMAIN."admin/menus'>".DOMAIN."admin/menus</a>) and click on the 'Menu Links' icon: <span class='i i-menus_items i-inline-block'></span><br /><br />

<h3>Adding a Link</h3>
To add a new link, you first click on the 'Add New LInk' button you see in the top right of the page.<br /><br />

<em>'Add New Link' button</em><br />
<img src='".DOMAIN."core/modules/menus/documentation/images/add-menu-link-button.png' alt='' /><br /><br />

When you do, you should see a page that looks something like this:<br /><br />

<em>Menu link content type</em><br />
<img src='".DOMAIN."core/modules/menus/documentation/images/add-menu-link-form.png' alt='' /><br /><br />

Here you're going to choose what type of content you are linking to. The options you have here will vary depending on what modules have been installed on your website. You will almost assuredly have the options you see in the screenshot though.  We'll talk a little about each of these and then discuss what other options you might have on your specific website.<br /><br />

We will, however, use the 'Home Page' option to illustrate some of the finer points of menu link management, so be sure to check that out...oh wait, here it is already:<br /><br />

<b>Home Page</b><br />
If you click on the 'Home Page' link, you will (as you may have guessed) create a link to the home page of your website.  You'll also see that when you create or edit a home page link you can also manage the look of the home page at the same time:<br /><br />

<em>'Home Page' menu Link</em><br />
<img src='".DOMAIN."core/modules/menus/documentation/images/add-menu-link-home.png' width='650' alt='' /><br /><br />

You'll have this same feature (the ability to manage the content of a page) on any menu link that's linking to an actual page of content within your CMS.  For example, if you were linking to an 'Existing Page' you'd be able to edit the content of that page.<br /><br />

We've alreaady discussed how you manage the Home Page content in the 'Home' documentation so we won't rehash that.  But I do want you to take note of the 'Menu' section that appears in the sidebar.  This section lets you manage how this specific menu link appears within a menu.  As such, it only appears when you're managing content through the menus section of the admin.<br /><br />

If you look at the screenshot above you'll see we've expanded the 'Menu' section already.  In this section we have 3 ways to manage the look of the menu link:<br /><br />

<em>Name</em><br />
The 'Name' is the text we will display in the menu for this specific menu link.  This can be different than the 'Name' of the actual piece of content.  For example, our home page might be called 'Home' (the name of the content), but we might want the link in this menu to say 'Home Page'.  If so, we'd simply enter 'Home Page' in the 'Name' field in the Menu sidebar section.  As you can see by the slightly grayed out 'placeholder' text that appears in the screenshot above, if we leave this field blank, it'll default to the name of the content (in this case 'Home').<br /><br />

<em>Image</em><br />
What if we want the link to be an image instead of just text? Well, we can use the 'Image' field in the Menu sidebar for this.  Simply browse for the image you want to use and submit the form and we'll use that image for the menu link.<br /><br />

<em>Rollover Image</em><br />
So you have an image for the menu link, but now you want to get real fancy and have the image change once a user hovers over (or rolls over) that link. Well, you're in luck, because uploading a 'Rollover Image' will do just that.<br /><br />

And that's pretty much all you need to know about adding a menu link, generally speaking.  Now let's look at how you add a few other secific content types to a menu.<br /><br />

<b>Pages > New Page</b><br />
So you want to link to a page, but that page doesn't exist yet.  We've got you covered.  Just click on the 'New Page' option when selecting what content type you want to link a menu link to.<br /><br />

When you do, you'll see the 'Page Form' which you know all about from adding pages via the <a href='".DOMAIN."admin/pages'>Pages</a> module. However, as you may have noticed (or guessed based on what we just talked about) you'll also see the 'Menu' sidebar which lets you manage how this specific menu link looks. As we've already discussed, you can use that sidebar to manage the 'Name' of the menu link, use an 'Image' in the menu link, and even add a 'Rollover Image'. Everything else about the page form will be the same as adding a page via the Content > Pages section of the admin.<br /><br />

<b>Pages > Existing Page</b><br />
Maybe you want to link to a page you've already created. Well, that's a simple as clicking the 'Existing Page' option when adding a new menu link. When you do, however, you'll be presented with a page you probably haven't seen before:<br /><br />

<em>'Existing Page' menu link</em><br />
<img src='".DOMAIN."core/modules/menus/documentation/images/add-menu-link-page-existing.png' alt='' /><br /><br />

Here we list the pages you've already added. To select which page you want to link to, you simply click on the name of the page.  YOu can also use the text field to more easily locate a specific page. Simply start typing the page name and a list of resulting pages will appear just below the text field, for example:<br /><br />

<em>'Existing Page' menu link - quicksearch</em><br />
<img src='".DOMAIN."core/modules/menus/documentation/images/add-menu-link-page-existing-quicksearch.png' alt='' /><br /><br />

Then you just click on the option in the dropdown below the text field and you'll be shown the page form (along with the 'Menu' sidebar section).<br /><br />

<b>Users > Login</b><br />
Sometimes you're not linking to actual content, but instead to some functionality. This is the case when we select the 'Login' as the content type.<br /><br />

When we link to 'Login' we're linking to the CMS's built in login form.  Since we can't edit the login form, we won't see a regular content form when we select it as the 'content type' for a menu link.  Instead we'll see what we call a 'menu link form':<br /><br />

<em>Menu link form</em><br />
<img src='".DOMAIN."core/modules/menus/documentation/images/add-menu-link-generic.png' alt='' /><br /><br />

Here we have a single field, the 'Name' field, which lets us define the text of the menu link.<br /><br />

We also have the 'Menu' sidebar section, but as you can see from the screenshot, the 'Name' field isn't there anymore (which, of course, is because we already have the Name field in the main section of the form.

This works like any other form though. We'd simply fill in our information (in this case, the Name of the menu link and (optionally) an Image and Rollover image for the menu link) and click 'Submit'. And you've just added a link to the 'Login' section of the CMS! Well done, you!<br /><br />

<b>Users > Logout</b><br />
Just like the 'Login' link, we sometimes want to link to the 'Logout' functionality of the CMS so that logged in users can logout.  To do so, we just click on the Users > Logout option when adding a menu link and fill out the menu link form just like we did when creating a Login link.<br /><br />

<b>External Link</b><br />
So you want to link to something that's not on your site. Or maybe you want to link to something that's on your site, but you don't see an option for it when adding a new menu link.  Well have no fear, we can use the 'External Link' option for this.<br /><br />

<em>'External Link' form</em><br />
<img src='".DOMAIN."core/modules/menus/documentation/images/add-menu-link-external.png' width='650' alt='' /><br /><br />

Here we simply have to put in the 'Name' of the menu link (the text that will be displayed) and the 'URL' we want to link to.  This could be a URL from another site, a URL on your own site, it could even be a javascript link if you want to be real fancy (you just have to prefix it with 'javascript:', for example: javascript:alert('You clicked me!');).<br /><br />

<b>No Link</b><br />
Sometimes you don't want to link to anything, but you still want to add some text (or perhaps an image) to the menu. Well, you can use the 'No Link' option for that.<br /><br />

<em>'No Link' form</em><br />
<img src='".DOMAIN."core/modules/menus/documentation/images/add-menu-link-nolink.png' alt='' width='650' /><br /><br />

Here, we just input the 'Name' we want to appear and (optionally) the images we want to show for this menu link (which of course, isn't a link at all).<br /><br />

<b>Other Menu Links</b><br />
As we've already said, you may have different options when you're adding a new menu link, depending on what modules have been installed on your website.  Each of these should be pretty self explanatory.  If you're adding a link to specific content, you'll be able to edit that content and have a special 'Menu' section in the sidebar to manage how that specific link appears.  If you're adding a link to an uneditable part of the CMS, you'll simply get to input a 'Name' (the text of the link) and (optionally) upload an Image and Rollover Image.<br /><br />

If you have any trouble, though, <a href='mailto:lindsayb@angelvisiontech.com'>let us know</a>.<br /><br />

<h3>Editing a Link</h3>
So, you've added some menu links. But maybe you misspelled a name or you want to change the text of a link, or you want to make any other change. We got you covered.<br /><br />

To edit a link, you simply click the 'Edit' icon (<span class='i i-edit i-inline-block'></span>) next to a link and you'll be taken to the same page you saw when you added the content of that link.  For example:<br /><br />

<em>Editing a link</em><br />
<img src='".DOMAIN."core/modules/menus/documentation/images/edit-menu-link.png' alt='' width='650' /><br /><br />

After that, it's just a matter of updating the information you want to change and clicking 'Submit' to save the changes. Moving on.<br /><br />

<h3>Arranging Links</h3>
By default, links will appear in the order you added them to a menu.  But of course, you'll want to be able to change how they're arranged.  No worries, my friend.<br /><br />

To arrange the links within a menu all you have to do is go to the menu's links page, click on a link row and, without releasing your mouse click, drag the link up or down in the list.  As soon as you let go of your mouse, it'll be re-positioned and it's location automatically saved...like magic!<br /><br />

While dragging the link around, you may have also noticed you can drag it 'in', which is to say you can indent it.  What you're really doing is playing a menu link under another link, making it a 'child' of the link.  For example:<br /><br />

<em>Menu links as children</em><br />
<img src='".DOMAIN."core/modules/menus/documentation/images/arrange-menu-links-children.png' alt='' width='650' /><br /><br />

By doing this, you can create a 'sub-menu' under a specific link.  For example, if you had an 'About' link, you might have some links related to the 'About' information below it such as 'Company', 'History', and 'Contact'.<br /><br />

<h2>Menus</h2>
As we've already discussed, you'll really only be adding, editing, and otherwise managing Menu Links as opposed to the Menus they reside in.  That's because a menu must be setup by us when we implement the design so that we can define within the code exactly where the menu is positioned and how it looks.<br /><br />

That being said, if you like, you're welcome to edit the name of a menu or even add another menu (though you'll have to let us know when you've done so, so we can add a place for it within the design of the page).<br /><br />

And now you know everything you need to know about menus (and probably some stuff you didn't want to know). But, as always, if you have any questions, just <a href='mailto:lindsayb@angelvisiontech.com'>let us know</a>.<br /><br />";
?>