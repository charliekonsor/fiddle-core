<?php
if(!class_exists('form_framework_home',false)) {
	/**
	 * Extends the form_framework class, providing scripts specific to the 'home' module.
	 *
	 * @package kraken\modules\home
	 */
	class form_framework_home extends form_framework {
		/**
		 * construct
		 */
		function __construct($module,$id = NULL,$c = NULL) {
			$this->form_framework_home($module,$id,$c);
		}
		function form_framework_home($module,$id = NULL,$c = NULL) {
			// Parent
			parent::__construct($module,$id,$c);
		}
		
		/**
		 * render
		 */
		function render($c = NULL) {
			// View
			$view = view::load($this->module.'.item.'.$this->id);
			$file = $view->file();
			$html = file_get_contents($file);
			
			// Content
			preg_match_all('/\$this->item_class\->v\([\'|"](.*?)[\'|"]\)/',$html,$matches);
			foreach($matches[1] as $k => $v) {
				$label = ucwords(str_replace(array('home_','_'),array('',' '),$v));
				$this->input($label,'ckeditor',$v);
			}
			
			// Parent
			return parent::render($c);
		}
	}
}
?>