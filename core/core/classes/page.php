<?php
if(!class_exists('page',false)) {
	/**
	 * Determines the information and HTML of a page.
	 *
	 * Dependencies
	 * - functions
	 *   - g
	 *   - string_encode
	 *   - browser_mobile
	 * - classes
	 *   - file (when checking of CKEditor has styles/templates)
	 *
	 * @package kraken
	 */
	class page extends core_framework {
		// Variables
		public $url; // string
		public $sef, $variables = array(); // array
		
		/**
		 * Loads and returns an instance of either the core framework page class or (if it exists) the module specific framework page class.
		 *
		 * Example:
		 * - $page = page::load($url,$c);
		 *
		 * @param string $url The URL of the page we want to get information on. Default = current page's URL
		 * @param array $c An array of configuration values. Default = NULL
		 * @return object An instance of either the core framework page class or (if it exists) the module specific framework page class.
		 */
		static function load($url = NULL,$c = NULL) {
			// Class name
			$class = __CLASS__;
			
			// Framework
			if(FRAMEWORK) {
				$class_framework = $class."_framework";
				if(class_exists($class_framework)) return new $class_framework($url,$c);
			}
			
			// Core class
			return new $class($url,$c);
		}

		/**
		 * Constructs the class
		 *
		 * @param string $url The URL you want to get the page info for. Defaults to current url (URL constant).
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function __construct($url = NULL,$c = NULL) {
			self::page($url,$c);
		}
		function page($url = NULL,$c = NULL) {
			// Config
			if(!x($c[view_default])) $c[view_default] = 1; // Can use a 'default' view (as opposed to an explicilty defined view)
			
			// Parent
			parent::__construct($c);
			
			// URL
			if($url) {
				$this->url = $url;
			}
			else {
				// Get
				$this->url = $this->url();
				
				// Not existant images get redirect here, but we don't want them to be considered a 'page'
				if($this->url == D."favicon.ico" or in_array(strtolower(file::extension($this->url)),array('jpg','jpeg','gif','png')) and !strstr($this->url,'?')) {
					header("Location: ".DOMAIN."core/core/error.php?error=404");
					exit;
				}
				
				// Save?
				$save = 1;
				if($_REQUEST['ajax_action'] or $_REQUEST['process_action']) $save = 0;
				if(strstr($this->url,DOMAIN."core/core/error.php")) $save = 0;
				if($save) {
					if(!$_SESSION['urls']) $_SESSION['urls'][0] = $this->url;
					else if($this->url and $this->url != $_SESSION['urls'][0]) array_unshift($_SESSION['urls'],$this->url);
					
					if($_SESSION['urls'][10]) unset($_SESSION['urls'][10]);
				}
			}
			
			// SEF
			$this->sef = $this->sef($this->url);
			// Theme // No longer use theme in core
			//$this->theme = g('config.theme'); // In framework this will be m('settings.settings.theme'), but we'll overwrite this with that value in $page->page().
			// Template // No longer use template in core...might bring back (if so, move variable definitions form page_framework back to page), but not using for now
			//$this->template = SERVER."local/themes/".$this->theme."/templates/default.php"; // Again, will get overwritten in framework
			
			#probably build somethign here where we get a 'view' based upon files (no area, or module, just a view) which we can then use in view_array() and view_file(). Take $this->c[view_default] into account as well.
			$this->variables = $this->sef;
			
			// Database
			//$this->db = new db(); // Don't need now that we use a 'core' class
			
			// Global
			g('page',$this);
		}
		
		/**
		 * Helper function so you can easily get the value of a variable in the class. Example: $this->area.
		 *
		 * @param string $key The key of the variable you want to get the value of.
		 * @return mixed The value of the variable with the given key.
		 */
		function __get($key) {
			return $this->$key;	
		}
		
		/**
		 * Extracts all values from the page object and returns them in an array.
		 *
		 * @return array An array of the page object's values.
		 */
		function a() {
			foreach($this as $k => $v) $array[$k] = $v;
			return $array;
		}

		/**
		 * Gets (if not passed) and saves url to class (and to $_SESSION[urls] array if we're using current page's URL).
		 *
		 * @param string $url The URL you want to save. Defaults to the current page's URL.	
		 * @return string The URL.								
		 */
		function url($https = 1) {
			// Saved
			if($this->url) return $this->url;
			
			// URL
			$url = "http";
			if($_SERVER["HTTPS"] == "on" and $https == 1) $url .= "s";
			$url .= "://";
			//$url .= $_SERVER["SERVER_NAME"]; // Sometimes doesn't have www when it should
			$url .= $_SERVER["HTTP_HOST"];
			$url .= $_SERVER["REQUEST_URI"];
			
			// Home
			if($url == DOMAIN."index.php") $url = DOMAIN;
			if($url == DOMAIN."index.html") $url = DOMAIN;
			
			// Save
			$this->url = $url;
			
			// Return
			return $this->url;
		}
		
		/**
		 * Returns array of SEF variables for given URL
		 *
		 * @param string $url The URL you want to parse the SEF variables from.
		 * @return array The array of SEF variables.
		 */
		function sef($url) {
			// Get from sef query string
			//$sef = $_GET['sef'];
			// Get from URL
			$url = str_replace(array(DOMAIN,SDOMAIN),'',$url); // Remove domain
			list($sef,$query) = explode('?',$url,2); // Remove query string
			
			if(substr($sef,0,1) == "/") $sef = substr($sef,1); // Trim slash at beginning
			if(substr($sef,-1) == "/") $sef = substr($sef,0,strlen($sef) - 1); // Trim slash at end
			if($sef) {
				$array = explode('/',$sef);
				
				return $array;
			}	
		}
		
		/**
		 * Returns HTML of the given 'view' (if it exists).
		 *
		 * @param string $view The 'view' we want to get the HTML of. Defaults to current page view.
		 * @param array $variables An array of variables to pass when get the HTML of the view. Example: array('perpage' => 5); would be accessible via $perpage in the view file. Default = NULL
		 * @return string The HTML of the view (if it existed).
		 */
		/*function view($view = NULL,$variables = NULL) {
			// File
			$file = $this->view_file();
			
			// Have file
			if($file) $html = $this->html_get($file,$variables);
			// No file
			else $html = "
<div class='core-none'>I'm sorry, but we couldn't find the page you were looking for.</div>";
			
			// Backets // Deprecated
			//$html = $this->brackets($html);
			
			// HTTPS // This will already get handled via $page->html() as all calls to $page->view() are within $page->html() somehow
			//$html = $this->html_https($html);
			
			// Return
			return $html;
		}
		
		/**
		 * Parses given view string and returns the array of variables (area, module, view, id, etc.).
		 *
		 * @param string $view The 'view' we want to get the variables of. Defaults to current page's view.
		 * @return array An array of variables about the view string.
		 */
		/*function view_array($view) {
			// Error
			if(!$view) return;
			
			// Array
			$array = NULL;
			
			#build this
			
			// Return
			return $array;
		}
		
		/**
		 * Returns the file (if exists) for the given 'view'.
		 *
		 * @param string $view The 'view' you want to get the file path of.
		 * @return string The path to the filef or the given view.
		 */
		/*function view_file($view = NULL) {
			// File
			$file = NULL;
			
			#build this
			
			// Return
			return $file;
		}
		
		/**
		 * Returns the HTML of the page.
		 */
		function html() {
			// Speed
			/*$f_r = function_speed(__CLASS__."->".__FUNCTION__);
			
			// HTML
			$html = $this->view();
			
			// Organize
			$html = $this->html_organize($html);
			
			// Speed
			function_speed(__CLASS__."->".__FUNCTION__,$f_r);
			
			// Return
			return $html;*/
		}
	
		/**
		 * Replaces all bracket variables (ex: {page_text}) with the item's actual value and returns the resulting HTML.	
		 *
		 * Deprecated.						
		 * 
		 * @param string $html The HTML you want to process.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return string The processed HTML of the page.
		 */
		/*function brackets($html,$c = NULL) {
			// Speed
			$f_r = function_speed(__CLASS__."->".__FUNCTION__);
			
			// System variables
			$system = array(
				'DOMAIN' => DOMAIN,
				'SDOMAIN' => SDOMAIN,
				'D' => D,
				'SERVER' => SERVER,
				'IP' => IP,
				'URL' => URL,
			);
			
			// Matches
			preg_match_all('/\{([a-z0-9\._\-]+)\}/i',$html,$matches);
			if(count($matches[1]) > 0) {
				// Changes
				$changes = NULL;
				foreach($matches[1] as $match) {
					$change = NULL;
					//debug("bracket: ".$match);
					
					// Levels
					$levels = explode('.',$match);
					$levels_count = count($levels);
						
					// Header
					if($match == "header") $change = $this->header();
					// Footer
					else if($match == "footer") $change = $this->footer();
					// System variables
					else if($_change = $system[$match]) $change = $_change;
					// Page
					else if($levels[0] == "page") {
						// Default
						if(!$levels[2]) $change = $this->$levels[1]; // page.area
					}
					
					// Change
					$changes['{'.$match.'}'] = $change;
				}
				
				// Replace
				//debug("changes: ".return_array($changes));
				if($changes) {
					$html = str_replace(array_keys($changes),array_values($changes),$html);	
				}
			}
			
			// Speed
			function_speed(__CLASS__."->".__FUNCTION__,$f_r);
			
			// Return
			return $html;
		}
		
		/**
		 * Compiles and returns the standard HTML header for a page including meta data. 			
		 * 
		 * @param array $c An array of configuration values. Default = NULL
		 * @return string The HTML of the header.
		 */
		function header($c = NULL) {
			// Config
			if(!x($c[meta])) $c[meta] = 1; // Include meta information
			if(!$c[rss]) $c[rss] = NULL; // The URL of an RSS feed file to include
			if(!$c[rss_title]) $c[rss_title] = "RSS Feed"; // The 'title' attribute of the RSS element
			if(!$c[sitemap]) $c[sitemap] = NULL; // The URL of a sitemap XML file to include
			if(!$c[css]) $c[css] = $this->css(); // Either a single CSS file URL or an array of CSS file URL's to include
			if(!$c[css_public]) { // The 'public' CSS. Either a single CSS file URL or an array of CSS file URL's to include. Needed for CKEditor if we're in the admin.
				if($this->area == "admin") $c[css_public] = $this->css(array('area' => 'public')); 
			}
			if(!x($c[css_minify])) $c[css_minify] = 1; // Do you want us to 'minify' the CSS so it loads faster?
			if(!$c[javascript]) $c[javascript] = $this->javascript(); // Either a single javascript file URL or an array of javascript file URL's to include
			if(!x($c[javascript_minify])) $c[javascript_minify] = 1; // Do you want us to 'minify' the javascript so it loads faster?
			if(!$c[extra]) $c[extra] = NULL; // A string of 'extra' elements/text/etc. to include in the HTML of the header (appears right before the </head> tag.
			
			// Speed
			debug_speed('page - header');
			
			// Doctype
			$text .= "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>";
			
			// Meta
			if($c[meta]) {
				debug_speed('meta','page - header');
				$text .= "
<title>{meta_title}</title>
<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
<meta name='Content-Language' content='en-GB' />
<meta name='language' content='english' />
<meta name='title' content='{meta_title}' />
<meta name='description' content='{meta_description}' />
<meta name='keywords' content='{meta_keywords}' />";
				/*if(!preg_match('/[\'|"]author[\'|"]/i',m('settings','site.meta.custom'))) $text .= "
<meta name='author' content='Darkwater Studios, LLC.' />";
			if(!preg_match('/[\'|"]designer[\'|"]/i',m('settings','site.meta.custom'))) $text .= "
<meta name='designer' content='Darkwater Studios, LLC.' />";
			if(!preg_match('/[\'|"]robots[\'|"]/i',m('settings','site.meta.custom'))) $text .= "
<meta name='robots' content='index, follow' />";
			if(!preg_match('/[\'|"]googlebot[\'|"]/i',m('settings','site.meta.custom'))) $text .= "
<meta name='googlebot' content='index, follow' />";*/
				$text .= "
<meta property='og:title' content='{meta_title}' />
<meta property='og:description' content='{meta_description}' />
<meta property='og:image' content='{meta_image}' />
<link rel='image_src' href='{meta_image}' />";
			}
			
			// RSS
			debug_speed('rss','page - header');
			if($c[rss]) $text .= "
<link rel='alternate' type='application/rss+xml' title='".string_encode($c[rss_title])."' href='".$c[rss]."' />";
			
			// Sitemap
			debug_speed('sitemap','page - header');
			if($c[sitemap]) $text .= "
<link rel='sitemap' href='".$c[sitemap]."' type='application/xml' />";
		
			// Favicon
			debug_speed('favicon','page - header');
			if(file_exists(SERVER."favicon.ico")) $text .= "
<link rel='shortcut icon' href='".D."favicon.ico' type='image/x-icon' />";
			
			// CSS
			if($c[css]) {
				debug_speed('css','page - header');
				// Minified // Gets handled by $this->html_organize() now
				/*if((!x($_GET['minify']) or $_GET['minify'] == 1) and $c[css_minify] == 1) {
					# This is a bit odd since we don't use theme or area in the core class alone
					// Area CSS
					$css = minify_css($c[css]);
					$text .= "
<link rel='stylesheet' type='text/css' href='".$css."' />";
				}
				// Unminified
				else {*/
					foreach($c[css] as $file) {
						$text .= "
<link rel='stylesheet' type='text/css' href='".str_replace(SERVER,D,$file)."' />";
					}
				//}
				
				// Public CSS // Only passed if we're in the 'admin' area, otherwise html_organize() will define this
				if($c[css_public]) {
					$css = minify_css($c[css_public]);
					$text .= "
<script type='text/javascript'>var CSS = '".str_replace(SERVER,D,$css)."';</script>";
				}
			}
			
			// Javascript
			if($c[javascript]) {
				debug_speed('javascript','page - header');
				// Minified // Gets handled by $this->html_organize() now
				# This is a bit odd since we don't use theme or area in the core class alone
				/*if((!x($_GET['minify']) or $_GET['minify'] == 1) and $c[javascript_minify] == 1) $text .= "
<script type='text/javascript' language='javascript' src='".minify_javascript($c[javascript])."'></script>";
				// Unminified
				else {*/
					foreach($c[javascript] as $file) {
						$text .= "
<script type='text/javascript' language='javascript' src='".str_replace(SERVER,D,$file)."'></script>";
					}
				//}
				
				// Javascript - Sharing
				/*debug_speed('javascript - sharing','page - header');
				// AddThis
				if(m('settings','share.site') == "addthis") $text .= "
<script type='text/javascript' src='http".(SDOMAIN == D ? "s" : "")."://s7.addthis.com/js/250/addthis_widget.js".(m('settings','share.addthis.username') ? "#username=xa-4be83b6d36d95dfe" : "")."'></script>";
				// ShareThis
				if(m('settings','share.site') == "sharethis") {
				/*$text .= "
<script type='text/javascript' src='http://w.sharethis.com/button/sharethis.js#".(m('settings','share.sharethis.key') ? "publisher=".m('settings','share.sharethis.key')."&amp;" : "")."type=website&amp;charset=utf-8&amp;onmouseover=false'></script>";*/
					/*$_SESSION['share']['badges']['sharethis'] = 1;
				}*/
			}
			
			// Javascript - constants
			$text .= "
<script type='text/javascript'>
	var DOMAIN = '".D."';
	var SERVER = '".SERVER."';
	var FRAMEWORK = ".FRAMEWORK.";
	var MOBILE = ".(browser_mobile() ? 1 : 0).";";
			# This is a bit odd since we don't use theme in the core class alone
			if($this->theme) {
				// Theme
				$text .= "
	var THEME = '".$this->theme."';";
				
				// CKEditor styles
				$ckeditor_styles = cache_get('ckeditor/styles');
				if(!x($ckeditor_styles)) {
					$file = new file(SERVER."local/themes/".$this->theme."/css/ckeditor-styles.js");
					$ckeditor_styles = $file->exists_with_content();
					//$ckeditor_styles = file::exists_with_content(SERVER."local/themes/".$this->theme."/css/ckeditor-styles.js");
					cache_save('ckeditor/styles',$ckeditor_styles);
				}
				if($ckeditor_templates) $text .= "
	var CKEDITOR_STYLES = '".D."local/themes/".$this->theme."/css/ckeditor-styles.js';";
				// CKEditor Templates
				$ckeditor_templates = cache_get('ckeditor/templates');
				if(!x($ckeditor_templates)) {
					$file = new file(SERVER."local/themes/".$this->theme."/css/ckeditor-templates.js");
					$ckeditor_templates = $file->exists_with_content();
					//$ckeditor_templates = file::exists_with_content(SERVER."local/themes/".$this->theme."/css/ckeditor-templates.js");
					cache_save('ckeditor/templates',$ckeditor_templates);
				}
				if($ckeditor_templates) $text .= "
	var CKEDITOR_TEMPLATES = '".D."local/themes/".$this->theme."/css/ckeditor-templates.js';";
			}
			// Config
			$array = array('config.links.external.new');
			foreach($array as $v) $text .= "
	var ".str_replace('.','_',$v)." = '".g($v)."';";
			$text .= "
</script>";
			
			// Extra
			if($c[extra]) {
				debug_speed('extra','page - header');
				$text .= "
".$c[extra];	
			}
			
			$text .= "
</head>";
			
			// Notices - will only return HTML if we didn't already display them somewhere else
			debug_speed('notices','page - header');
			$text .= $this->notices();
			
			// End speed
			debug_speed('page - header');
			
			return $text;	
		}

		/**
		 * Returns (or sets, if pass a 2nd param) the meta value of the specified type			
		 * 
		 * @param string $type The type of meta information you want to set: title, description, keywords, image
		 * @param string $value The meta value you want to save for the given meta information. Default = NULL
		 * @return string The meta value of the given $type.
		 */
		function meta($type,$value = NULL) {
			// Get
			if(!$value) {
				$value = $this->meta[$type];
			}
			
			// Clean
			$value = $this->meta_clean($value);
			
			// Save
			$this->meta[$type] = $value;
			
			// Return
			return $value;
		}
	
		/**
		 * Returns (or sets if $value passed) meta title for the page.
		 * 
		 * @param string $value The value you want to save for the page meta title.
		 * @return string The page's meta title.
		 */
		function meta_title($value = NULL) {
			return $this->meta('title',$value);
		}
		
		/**
		 * Returns (or sets if $value passed) meta description for the page.
		 * 
		 * @param string $value The value you want to save for the page meta description.
		 * @return string The page's meta description.
		 */
		function meta_description($value = NULL) {
			return $this->meta('description',$value);
		}
		
		/**
		 * Returns (or sets if $value passed) meta keywords for the page.
		 * 
		 * @param string $value The value you want to save for the page meta keywords.
		 * @return string The page's meta keywords.
		 */
		function meta_keywords($value = NULL) {
			return $this->meta('keywords',$value);
		}
	
		/**
		 * Returns (or sets if $value passed) meta image for the page.
		 * 
		 * @param string $value The value you want to save for the page meta image.
		 * @return string The page's meta image.
		 */
		function meta_image($value = NULL) {
			return $this->meta('image',$value);
		}
	
		/**
		 * Clean up a string for use in the page's meta information.			
		 * 
		 * @param string $string The string you want to clean up.
		 * @return string The cleaned string.
		 */
		function meta_clean($string) {
			// Strip tags
			$string = strip_tags($string);
			// Remove double spaces
			$string = preg_replace('/\s\s+/',' ',$string);
			// Remove line breaks
			$string = preg_replace('/[\t\r\n]/','',$string);
			// Trim
			$string = trim($string);
			// Encode string
			$string = string_encode($string);
			
			// Length
			if(g('config.meta.maxlength')) $value = substr($value,0,g('config.meta.maxlength'));
			
			// Return
			return $string;
		}
	
		/**
		 * Returns HTML for displaying messages and errors which were stored in the $_SESSION on the previous page.
		 *
		 * @return string The HTML of the messages and errors (if there are any).							
		 */
		function notices() {
			if($_SESSION['notices']) {
				$text .= "
<div id='notices'>";
				foreach($_SESSION['notices'] as $k => $v) {
					$text .= "
	<div class='notice notice-".implode(' notice-',explode('_',$k))."'>
		<div class='notice-close'></div>
		".$v."
	</div>";
				}
				$text .= "
</div>";
			}
			unset($_SESSION['notices']);
			define('NOTICES_CALLED',true);
			
			return $text;
		}
	
		/**
		 * Saves a 'notice' to the $_SESSION so it can be displayed on the next page.
		 *
		 * !!! Moved to core/core/functions/core.php as we need to call this independent of the current page. !!!
		 *
		 * @param string $key The notice 'key'. It can be any string you want (most common is 'message' or 'error'). It will be added to the notice element class. Ex: key = 'error', the class 'notice-error' would be added to the element.
		 * @param string $notice The text of the notice.
		 * @param boolean $sticky Do you want this notice to be 'sticky'? If true, the notice will stay on the page rather than slide away after a few moments. 			
		 */
		/*function notice($key,$notice,$sticky = 0) {
			if($key and $notice) {
				$_SESSION['notices'][$key.($sticky ? "_sticky" : "")] = $notice;
			}
		}
	
		/**
		 * Saves a message to the $_SESSION 'notices' so it can be displayed on the next page.
		 *
		 * !!! Moved to core/core/functions/core.php as we need to call this independent of the current page. !!!
		 *
		 * @param string $error The text of the message notice.
		 * @param boolean $sticky Do you want this error notice to be 'sticky'? If true, the notice will stay on the page rather than slide away after a few moments. 			
		 */
		/*function message($error,$sticky = 0) {
			$this->notice('message',$error,$sticky);
		}
	
		/**
		 * Saves an error to the $_SESSION 'notices' so it can be displayed on the next page.
		 *
		 * !!! Moved to core/core/functions/core.php as we need to call this independent of the current page. !!!
		 *
		 * @param string $error The text of the error notice.
		 * @param boolean $sticky Do you want this error notice to be 'sticky'? If true, the notice will stay on the page rather than slide away after a few moments. 			
		 */
		/*function error($error,$sticky = 0) {
			$this->notice('error',$error,$sticky);
		}
	
		/**
		 * Compiles and returns the standard HTML footer for a page.		
		 * 
		 * @param array $c An array of configuration values. Default = NULL
		 * @return string The HTML of the footer.
		 */
		function footer($c = NULL) {
			$text .= "
	</html>";
			
			return $text;	
		}

		/**
		 * An array of core javascript files.									
		 * 
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of the full file paths to the needed javascript files.
		 */
		function javascript($c = NULL) {
			// Core
			$files[] = SERVER."core/core/js/jquery.js";
			$files[] = SERVER."core/core/js/js.js";
			if($this->area == "admin") $files[] = SERVER."core/core/admin/js/js.js";
			
			// Core - other
			if(g('config.include.javascript')) {
				foreach(g('config.include.javascript') as $k => $v) {
					if($v[core] and $v[files]) {
						foreach($v[files] as $file) {
							$files[] = SERVER.$file;
						}
					}
				}
			}
			
			// Local
			$files[] = SERVER."local/js/js.js";
			if($this->area == "admin") $files[] = SERVER."local/js/admin.js"; // Should maybe be /local/admin/js/js.js
			
			// Return
			return files_exists($files);
		}

		/**
		 * Returns an array of core CSS files.									
		 * 
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of the full file paths to the needed CSS files.
		 */
		function css($c = NULL) {
			// Config
			if(!$c[area]) $c[area] = $this->area; // The area of CSS you want: public, admin
			
			// Core
			$files[] = SERVER."core/core/css/css.css";
			if($c[area] == "admin") $files[] = SERVER."core/core/admin/css/css.css";
			
			// Core - other
			if(g('config.include.javascript')) {
				foreach(g('config.include.css') as $k => $v) {
					if($v[core] and $v[files]) {
						foreach($v[files] as $file) {
							$files[] = SERVER.$file;
						}
					}
				}
			}
				
			// Local
			$files[] = SERVER."local/css/css.css";
			if($c[area] == "admin") $files[] = SERVER."local/css/admin.css"; // Should maybe be /local/admin/css/css.css
			
			// Internet Explorer // Think I'd want them to manually add any IE specific styles to the header
			/*$browser = browser(array('array' => 1));
			list($v,$_temp) = explode('.',$browser[version],2);
			if($browser[browser] == "msie") {
				// Generel IE CSS
				$files[] = SERVER."local/css/ie.css";
				// Version specific IE CSS
				$files[] = SERVER."local/css/ie".$v.".css";
			}*/
			
			// Return
			return files_exists($files);
		}
		
		/**
		 * Checks to see if there is any of the 'dependent' javascript exists in the HTML.
		 *
		 * The <link> tag will get moved to the top when $this->html_organize() is called.
		 *
		 * @param string $html The HTML you want to check for dependent javascript.
		 * @return string $html The HTML with dependent javascript added.
		 */
		function html_dependent_javascript($html) {
			// Error
			if(!$html) return;
			
			// Javascript
			if(g('config.include.javascript')) {
				foreach(g('config.include.javascript') as $k => $v) {
					// Has a regex we can check against
					if($v[regex] and $v[files] and !$v[core]) {
						// Match
						if(preg_match($v[regex],$html)) {
							// Include
							$html .= include_javascript($k);
						}
					}
				}
			}
			
			// Return
			return $html;
		}
		
		/**
		 * Checks to see if there is any of the 'dependent' CSS exists in the HTML.
		 *
		 * The <link> tag will get moved to the top when $this->html_organize() is called.
		 *
		 * @param string $html The HTML you want to check for dependent CSS.
		 * @return string $html The HTML with dependent CSS added.
		 */
		function html_dependent_css($html) {
			// Error
			if(!$html) return;
			
			// CSS
			if(g('config.include.css')) {
				foreach(g('config.include.css') as $k => $v) {
					// Has a regex we can check against
					if($v[regex] and $v[files] and !$v[core]) {
						// Match
						if(preg_match($v[regex],$html)) {
							// Include
							$html .= include_css($k);
						}
					}
				}
			}
			
			// Return
			return $html;
		}

		/**
		 * Makes sure img/script/iframe/linl/etc tags use https:// if we're on a https page	
		 * 
		 * @param string $html The HTML you want to make sure is secure.
		 * @param boolean $force Do you want to force the use of https, even if we're not on an https page. Default = 0
		 * @return string The secure HTML.
		 */
		function html_https($html,$force = 0) {
			if(substr($this->url,0,5) == "https" or $force) {
				$html = preg_replace("/(src|pluginspage|codebase)=(['|\"])http:/i","$1=$2https:",$html); // Images, scripts, iframes, pluginspage, codebase
				$html = preg_replace("/<link([^>]*?)href=(['|\"])http:/i","<link$1href=$2https:",$html); // CSS
				$html = preg_replace("/url\((['|\"]?)http:/i","url($1https:",$html); // CSS Background
			}
			return $html;
		}
		
		/**
		 * Handles various organization of CSS and javascript for speed.
		 *
		 * - Loads dependent CSS and javascript
		 * - Converts src/url/etc. to https if URL is on https
		 * - Moves CSS and javascript to the header of the document
		 * - Places CSS before javascript
		 * - Minifies both (if $c[minify_css] == 1 and $c[minify_javascript] == 1)
		 * - Appends unminified file URLs with date of file (so it doesn't use an old browser cached version)
		 * - Removes duplicate files
		 *
		 * @param string $html The HTML you want to organize.
		 * @param array $c An array of configuration values.
		 * @return string The HTML organized.
		 */
		function html_organize($html,$c = NULL) {
			// Error
			if(!x($html)) return;
			
			// Speed
			$f_r = function_speed(__CLASS__."->".__FUNCTION__);
			
			// Config
			if(!x($c[minify_css])) {
				if(x($_GET['minify'])) $c[minify_css] = $_GET['minify'];
				else if(browser() == "msie") $c[minify_css] = 1; // Must minify in IE as they have a limit of 31 CSS files per page
				else $c[minify_css] = (debug_active() ? g('config.debug.minify.active') : g('config.minify.css.active'));
			}
			if(!x($c[minify_javascript])) {
				if(x($_GET['minify'])) $c[minify_javascript] = $_GET['minify'];
				else $c[minify_javascript] = (debug_active() ? g('config.debug.minify.active') : g('config.minify.javascript.active'));
			}
			
			// Domains (to minify)
			$domains_css = g('config.minify.css.domains'); // Won't actually minify these (because altering paths for images won't work), but grab them from HTML so we keep them 'below' the other js in the code so everything works fine
			if(!in_array(DOMAIN,$domains_css)) $domains_css[] = DOMAIN;
			if(!in_array(SDOMAIN,$domains_css)) $domains_css[] = SDOMAIN;
			$domains_javascript = g('config.minify.javascript.domains'); // Won't actually minify these (because altering paths for images won't work), but grab them from HTML so we keep them 'below' the other js in the code so everything works fine
			if(!in_array(DOMAIN,$domains_javascript)) $domains_javascript[] = DOMAIN;
			if(!in_array(SDOMAIN,$domains_javascript)) $domains_javascript[] = SDOMAIN;
			
			// Dependencies
			$html = $this->html_dependent_css($html);
			$html = $this->html_dependent_javascript($html);
			
			// HTTPS
			$html = $this->html_https($html);
			
			// Move conditional CSS to head  - placing before javascript if we can
			$start = "</head>";
			if(preg_match('/<script(.*?)<\/head>/si',$html)) $start = "<script"; // Have javascript in head we can place it above
			$regex = "/".preg_quote($start,'/')."(.*?)<\!--\[if(.*?)\](.*?)<link([^>]+)href=(['|\"])".preg_quote(DOMAIN,'/')."(.*?)(['|\"])(.*?)>(.*?)-->/si";
			$replace = "<!--[if$2]$3<conditionalink$4href=$5".DOMAIN."$6$7$8>$9-->".$start."$1";
			for($x = 0;$x < 1;) {
				if(preg_match($regex,$html,$match)) {
					$html = preg_replace($regex,$replace,$html);
				}
				else {
					$x = 1;
				}
			}
			
			// CSS
			$regex = "/<link([^>]+)href=(['|\"])(";
			foreach($domains_css as $x => $domain) $regex .= ($x ? "|" : "").preg_quote($domain,'/');
			$regex .= ")([^'\"]+)\.css(.*?)(['|\"])(.*?)>/si";
			//debug("regex: ".$regex);
			preg_match_all($regex,$html,$matches);
			if(count($matches[0])) {
				// Files
				$files = NULL;
				foreach($matches[4] as $x => $v) {
					$files[] = ($matches[3][$x] == DOMAIN || $matches[3][$x] == SDOMAIN ? SERVER : $matches[3][$x]).$v.".css".$matches[5][$x];
				}
				// Remove duplicates
				$files = array_unique($files);
				//debug("files: ".return_array($files));
				
				// Remove indivicual files from HTML
				/*$html = preg_replace('/ // This doesn't work on Windows computers for some reason
?'.substr($regex,1),'',$html);*/
				$html = preg_replace($regex,'',$html);
				
				// Minify
				if($c[minify_css]) {
					$files_keep = NULL;
					foreach($files as $k => $file) {
						if(substr($file,0,4) == "http") {
							$files_keep[] = $file;
							unset($files[$k]);
						}
					}
					
					$files = array(str_replace(D,SERVER,minify_css($files)));
					if($files_keep) $files = array_merge($files,$files_keep);
				}
				
				// Add file(s) to head
				$files_string = NULL;
				foreach($files as $file) {
					if(!$c[minify_css]) $file .= "?".filemtime($file);
					$files_string .= "<link rel='stylesheet' type='text/css' href='".str_replace(SERVER,D,$file)."' />\r\n";
				}
				if($file_minified and $this->area == "public") $files_string .= "<script type='text/javascript'>var CSS = '".$file_minified."';</script>\r\n";
				$count = 1; // For some reason, passing 1 as the 4th param gives a fatal error, pass param instead
				$html = str_replace("</head>",$files_string."</head>",$html,$count);
			}
			
			// Unescape conditional CSS
			$html = str_replace('<conditionalink','<link',$html);
			
			// Regular CSS
			/*$regex = "/".preg_quote($start,'/')."(.*?)<link([^>]+)href=(['|\"])".preg_quote(DOMAIN,'/')."(.*?)(['|\"])(.*?)>/si";
			$replace = "<link$2href=$3$4$5$6>".$start."$1";
			for($x = 0;$x < 1;) {
				if(preg_match($regex,$html,$match)) {
					$html = preg_replace($regex,$replace,$html);
				}
				else {
					$x = 1;
				}
			}*/
			
			// Javascript
			$regex = '/<script([^>]+)src=([\'|"])(';
			foreach($domains_javascript as $x => $domain) $regex .= ($x ? "|" : "").preg_quote($domain,'/');
			$regex .= ')([^\'"]+)\.js(.*?)([\'|"])(.*?)>((<\/script>)?)/si';
			//debug("regex: ".$regex);
			preg_match_all($regex,$html,$matches);
			if(count($matches[0])) {
				// Files
				$files = NULL;
				foreach($matches[4] as $x => $v) {
					$files[] = ($matches[3][$x] == DOMAIN || $matches[3][$x] == SDOMAIN ? SERVER : $matches[3][$x]).$v.".js".$matches[5][$x];
				}
				// Remove duplicates
				$files = array_unique($files);
				//debug("files: ".return_array($files));
				
				// Remove indivicual files from HTML
				/*$html = preg_replace('/ // This doesn't work on Windows computers for some reason
?'.substr($regex,1),'',$html);*/
				$html = preg_replace($regex,'',$html);
				
				// Minify
				if($c[minify_javascript]) {
					$files_keep = NULL;
					foreach($files as $k => $file) {
						if(strstr($file,'ckeditor') or substr($file,0,4) == "http") {
							$files_keep[] = $file;
							unset($files[$k]);
						}
					}
					
					$files = array(str_replace(D,SERVER,minify_javascript($files)));
					if($files_keep) $files = array_merge($files,$files_keep);
				}
				
				// Add file(s) to head
				$files_string = NULL;
				foreach($files as $file) {
					if(!$c[minify_javascript]) $file .= "?".filemtime($file);
					$files_string .= "<script type='text/javascript' language='javascript' src='".str_replace(SERVER,D,$file)."'></script>\r\n";
				}
				$count = 1; // For some reason, passing 1 as the 4th param gives a fatal error, pass param instead
				$html = str_replace("</head>",$files_string."</head>",$html,$count);
			}
			
			// Move javascript to head
			/*$regex = '/<\/head>(.*?)<script([^>]+)src=([\'|"])'.preg_quote(DOMAIN,'/').'(.*?)([\'|"])(.*?)>((<\/script>)?)/si';
			$replace = '<script$2src=$3'.DOMAIN.'$4$5$6>$7</head>$1';
			for($x = 0;$x < 1;) {
				if(preg_match($regex,$html,$matches)) {
					$html = preg_replace($regex,$replace,$html);
				}
				else {
					$x = 1;
				}
			}*/
			
			// Meta
			if($meta_title = $this->meta_title()) {
				$html = str_replace("{meta_title}",$meta_title,$html);
			}
			else {
				$html = str_replace("<title>{meta_title}</title>","",$html);
				$html = str_replace("<meta name='title' content='{meta_title}' />","",$html);
				$html = str_replace("<meta property='og:title' content='{meta_title}' />","",$html);
			}
			if($meta_description = $this->meta_description()) {
				$html = str_replace("{meta_description}",$meta_description,$html);
			}
			else {
				$html = str_replace("<meta name='description' content='{meta_description}' />","",$html);
				$html = str_replace("<meta property='og:description' content='{meta_description}' />","",$html);
			}
			if($meta_keywords = $this->meta_keywords()) {
				$html = str_replace("{meta_keywords}",$meta_keywords,$html);
			}
			else {
				$html = str_replace("<meta name='keywords' content='{meta_keywords}' />","",$html);
			}
			if($meta_image = $this->meta_image()) {
				$html = str_replace("{meta_image}",$meta_image,$html);
			}
			else {
				$html = str_replace("<meta property='og:image' content='{meta_image}' />","",$html);
				$html = str_replace("<link rel='image_src' href='{meta_image}' />","",$html);
			}
			
			// End speed
			function_speed(__CLASS__."->".__FUNCTION__,$f_r);
			
			// Return
			return $html;
		}

		/**
		 * Determines the heading for the given page and returns the HTML of it (or, if $c['return'] == "array", an array of url => title heading parts).
		 * 
		 * @param array $c An array of configuration values. Default = NULL
		 * @return string|array The HTML of the page heading or (if $c['return'] == "array") an array of the heading parts (url => title).
		 */
		/*function heading($c = NULL) {
			// Config
			if(!$c[separator]) $c[separator] = ": "; // The string you want to use when separating levels of the heading.
			if(!$c['return']) $c['return'] = "html"; // What to return: html [default], array (will be in url => title format).
			$array = NULL;
			
			// URL root
			$url = D;
			
			// Items
			foreach($this->sef as $v) {
				$url .= (substr($url,-1) == "/" ? "" : "/").$v;
				if(!is_int_value($v)) {
					$array[$url] = ucwords($v);
				}
			}
			
			// Array
			if($c['return'] == "array") $return = $array;
			// HTML
			else  {
				foreach($array as $k => $v) {
					$return .= ($return ? $c[separator] : "")."<a href='".$k."'>".$v."</a>";
				}
				$return = "<h1 class='page-heading'>".$return."</h1>";
			}
			
			return $return;
		}

		/**
		 * Determines the heading buttons for the given page and returns the HTML of it (or, if $c['return'] == "array", an array of url => title buttons).
		 * 
		 * @param array $c An array of configuration values. Default = NULL
		 * @return string|array The HTML of the page heading buttons or (if $c['return'] == "array") an array of the buttons (url => title).
		 */
		/*function heading_buttons($c = NULL) {
			# what can I build here?
		}

		/**
		 * Returns the HTML for displaying filters on the current page.
		 * 
		 * @param array $c An array of configuration values. Default = NULL
		 * @return string|object The HTML of the filters or (if $c['return'] == "object") the form class object of the filters.
		 */
		/*function manage_filters($c = NULL) {
			# what can I build here?
		}*/
	}
}
?>