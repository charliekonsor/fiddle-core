<?php
// Sections
$sections = admin_sections_framework();
	
// Section
if($this->page->variables[0]) {
	// Section
	$section = $this->page->variables[0];
	print "
					<h1 class='page-heading'><a href='".DOMAIN.$this->area."/".$this->page->variables[0]."'>".ucwords($section)."</a></h1>";

	// Modules
	if($sections[$section]) {
		foreach($sections[$section] as $k => $v) {
			if($v[active] and $v[sidebar]) print "
					<a href='".DOMAIN."admin/".$k."'>".m($k.'.name')."</a><br />";	
		}
	}
	
}
// Default
else {
	foreach($sections as $section => $modules) {
		$modules_permission = NULL;
		foreach($modules as $k => $v) {
			if($v[active] and ($v[sidebar] or count($modules) == 1) and permission($k,'manage')) {
				$modules_permission[] = $k;
			}
		}
		if($modules_permission) {
			if(count($modules_permission) > 1) redirect(D."admin/".$section);
			else redirect(D."admin/".$modules_permission[0]);
		}
	}
}
?>