<?php
if(!class_exists('payment_firstdata',false)) {
	/**
	 * An 'extension' of the payment class used for processing payments through First Data.
	 *
	 * First Data requires that a .pem certificate is sent along with all calls made to their API.
	 * You can created and download your .pem certificate here: https://secure.linkpt.net/lpc/servlet/LPCSupport
	 *
	 * Here's a short tutorial that also describes how to get the .pem certificate: https://ws.merchanttest.firstdataglobalgateway.com/fdggwsapi/services 
	 *
	 * The $pem variable used when constructing the class will be the full path to this .pem certificate on your server.
	 * Example: /home/user/domains/mydomain.com/public_html/local/12345678910.pem
	 *
	 * Notes:
	 * - You must turn on 'Non-Referenced Credits' in your First Data settings to be able to credit an account (as opposed to refunding all/part of a previous transaction).
	 * - This class uses (and consequently, requires) the SimpleXML PHP extension: http://php.net/manual/en/book.simplexml.php.
	 *
	 * Custom variables: 0
	 *
	 * Test account:
	 * - store_number: 1909289156
	 * - pem: 1909289156.pem (stored in core/core/classes/payment/1909289156.pem)
	 * - Other:
	 *   - Admin URL: https://www.staging.yourpay.com
	 *   - User ID: 289156
	 *   - Tax ID: 111111111
	 * 
	 * To do
	 * - the xml_object() function (and resulting use of the returned object) is untested.
	 * - Support for $payment->custom array. At first glance, I don't see any method for doing this save the notes->comments entity (which only allows letters, numbers, dashes, and underscores.
	 * - Return 'custom' variables in returned results (if possible)
	 * - check for required fields before sending?
	 *
	 * Dependencies
	 * - Functions
	 *   - x() - __construct()
	 *
	 * @package kraken\payments
	 */
	class payment_firstdata {
		/** Stores the path on your server to the .pem certificate First Data provided. */
		public $pem;
		/** Stores the store number of your First Data account. */
		public $user;
		/** Stores an array of configuration values passed to the class. */
		public $c;
		
		/**
		 * Constructs the class.
		 *
		 * Configuration values (key, type, default - description):
		 * - host, string, secure.linkpt.net - The API's secure host URL you want to send calls to.
		 * - port, int, 1129 - The port number you want to access when you send calls to the API.
		 * - test, boolean, 0 - Whether or not we want to process transactions via First Data testing 'sandbox'. Note: Your First Data account must be in 'Test Mode'.
		 * - result, string, LIVE - The only way to run a 'test' transaction on a live account is to force a specific response: GOOD, DECLINE, or DUPLICATE.  If we pass 'LIVE', it'll run the transaction normally.
		 *
		 * @param string $pem The full path on your server to the .pem certificate First Data provided.
		 * @param string $store_number The store number of your First Data account.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function __construct($pem,$store_number,$c = NULL) {
			self::payment_firstdata($pem,$store_number,$c);
		}
		function payment_firstdata($pem,$store_number,$c = NULL) {
			// PEM Certificate
			$this->pem = $pem;
			// Store Number
			$this->store_number = $store_number;
			
			// Config
			if(!$c[host]) $c[host] = "secure.linkpt.net";
			if(!$c[port]) $c[port] = "1129";
			if(!$c[result]) $c[result] = "LIVE";
			if(!x($c[test])) $c[test] = 0;
			$this->c = $c;
		}
		
		/**
		 * Authorizes the customer's account for given amount.
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction including 'result' (boolean, 1 = succes, 0 = error) and 'message'.
		 */
		function authorize($payment,$c = NULL) {
			// Common errors
			if($error_common = $this->errors()) {
				$error = $error_common;
			}
			// No amount passed
			else if(!$payment->amount) {
				$error = "No authorization amount was passed.";
			}
			// No credit card
			else if(!$payment->card[number]) {
				$error = "No credit card was passed for us to authorize.";	
			}
			
			// Error
			if($error) {
				$results = array(
					'result' => 0,
					'message' => $error
				);	
			}
			// Transaction
			else {
				// Send
				$results = $this->send($payment,"authorize",$c);
			}
			
			// Return
			return $results;
		}
		
		/**
		 * Captures a transaction that was previously authorized via the authorize() method.
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction including 'result' (boolean, 1 = succes, 0 = error) and 'message'.
		 */
		function capture($payment,$c = NULL) {
			// Common errors
			if($error_common = $this->errors()) {
				$error = $error_common;
			}
			// No transaction ID
			else if(!$payment->transaction) {
				$error = "No transaction ID was passed that we could cancel.";
			}
			// No amount passed
			else if(!$payment->amount) {
				$error = "No capture amount was passed.";
			}
			
			// Error
			if($error) {
				$results = array(
					'result' => 0,
					'message' => $error
				);	
			}
			// Transaction
			else {
				// Send
				$results = $this->send($payment,"capture",$c);
			}
			
			// Return
			return $results;
		}
		
		/**
		 * Charges the customer a given amount.
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction including 'result' (boolean, 1 = succes, 0 = error) and 'message' as well as the transaction id ('transaction').
		 */
		function charge($payment,$c = NULL) {
			// Common errors
			if($error_common = $this->errors()) {
				$error = $error_common;
			}
			// No amount passed
			else if(!$payment->amount) {
				$error = "No charge amount was passed.";
			}
			// No credit card
			else if(!$payment->card[number]) {
				$error = "No credit card was passed for us to charge.";	
			}
			
			// Error
			if($error) {
				$results = array(
					'result' => 0,
					'message' => $error
				);
			}
			// Transaction
			else {
				// Send
				$results = $this->send($payment,"charge",$c);
			}
			
			// Return
			return $results;
		}
		
		/**
		 * Refunds a customer/account a defined amount on a previous transaction.
		 *
		 * I'm not sure if First Data allow for 'partial' refunds (meaning only part of a transaction).
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction including 'result' (boolean, 1 = succes, 0 = error) and 'message'.
		 */
		function refund($payment,$c = NULL) {
			// Common errors
			if($error_common = $this->errors()) {
				$error = $error_common;
			}
			// No transaction ID
			else if(!$payment->transaction) {
				$error = "No transaction ID was passed for the transaction we want to refund.";
			}
			// No amount passed
			else if(!$payment->amount) {
				$error = "No refund amount was passed.";
			}
			// No credit card - yes, First Data does require at least the last 4 digits of a customer's credit card to refund all or part of a transaction
			else if(!$payment->card[number]) {
				$error = "No credit card was passed for us to refund this amount to.";	
			}
			
			// Error
			if($error) {
				$results = array(
					'result' => 0,
					'message' => $error
				);	
			}
			// Transaction
			else {
				// Send
				$results = $this->send($payment,"refund",$c);
			}
			
			// Return
			return $results;
		}
		
		/**
		 * Cancels/voids the given transaction.
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction including 'result' (boolean, 1 = succes, 0 = error) and 'message'.
		 */
		function cancel($payment,$c = NULL) {
			// Common errors
			if($error_common = $this->errors()) {
				$error = $error_common;
			}
			// No transaction ID
			else if(!$payment->transaction) {
				$error = "No transaction ID was passed that we could cancel.";
			}
			
			// Error
			if($error) {
				$results = array(
					'result' => 0,
					'message' => $error
				);	
			}
			// Transaction
			else {
				// Send
				$results = $this->send($payment,"cancel",$c);
			}
			
			// Return
			return $results;
		}
		
		/**
		 * Credits a customer the given amount.
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction including 'result' (boolean, 1 = succes, 0 = error) and 'message'.
		 */
		function credit($payment,$c = NULL) {
			// Common errors
			if($error_common = $this->errors()) {
				$error = $error_common;
			}
			// No amount passed
			else if(!$payment->amount) {
				$error = "No credit amount was passed.";
			}
			// No credit card
			else if(!$payment->card[number]) {
				$error = "No credit card was passed for us to credit.";	
			}
			
			// Error
			if($error) {
				$results = array(
					'result' => 0,
					'message' => $error
				);	
			}
			// Transaction
			else {
				// Remove transaction - must NOT be submitted for this to work (if we want to refund on a specific transaction we use the refund() method).
				$payment->transaction = NULL;
				// Send
				$results = $this->send($payment,"credit",$c);
			}
			
			// Return
			return $results;
		}
		
		/**
		 * Sends various 'transactions' to First Data API.
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param string $action The transaction action we're performing.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction including 'result' (boolean, 1 = succes, 0 = error) and 'message' as well as the transaction id ('transaction').
		 */
		function send($payment,$action,$c = NULL) {
			// URL
			if($this->c[test]) {
				//$url = "https://staging.linkpt.net:1129/LSGSXM";
				$url = "https://staging.linkpt.net:1129/LSGSXML";
			}
			else {
				//$url = "https://".$this->c[host].":".$this->c[port]."/LSGSXM";
				$url = "https://".$this->c[host].":".$this->c[port]."/LSGSXML";
			}
			
			// Test - needs this specific creditcard info
			if($this->c[test]) {
				//$payment->amount = 1.00; // Doesn't need this I don't think
				$payment->card[number] = '4005550000000019'; // Test credit card number (visa)
				$payment->card[expiration_month] = date('m'); // Current month
				$payment->card[expiration_year] = date('Y'); // Current year
				$payment->card[code] = '123'; // Doesn't use this value I don't think
			}
			
			// Transaction
			if($action == "authorize") $action_code = "preauth";
			if($action == "capture") $action_code = "postauth";
			if($action == "charge") $action_code = "sale";
			if($action == "refund") $action_code = "return";
			if($action == "cancel") $action_code = "void";
			if($action == "credit") $action_code = "credit";
			
			// Values - common
			$values = array(
				'merchantinfo' => array(
					'configfile' => $this->store_number
				),
				'orderoptions' => array(
					'ordertype' => $action_code,
					'result' => $this->c[result],
				),
				'transactiondetails' => array(
					'transactionorigin' => 'ECI', // Credit Card Retail Transactions: RETAIL, Mail order/telephone order: MOTO, E-commerce: ECI (or blank)
					'oid' => (in_array($action,array('capture','refund','cancel')) ? $payment->transaction : NULL), // Order ID number. The previous order id number (transaction) for capture, refund, and cancel calls.  For all others, this is the new order's id (transaction). Must be unique. If not set, gateway will assign one.
					'ponumber' => NULL, // ???
					'taxexempt' => NULL, // ???
					'terminaltype' => 'UNSPECIFIED', // Electronic cash register: POS, Point-of-sale credit card terminal: STANDALONE, Self-service station: UNATTENDED, E-commerce (or other): UNSPECIFIED
					'ip' => $payment->ip(),
				),
			);
			
			// Values - amount
			$values[payment][chargetotal] = $payment->amount;
			/*$values[payment][tax] = $payment->total[tax]; // Passing these values causes an 'Invalide XML' error
			$values[payment][vattax] = NULL;
			$values[payment][shipping] = $payment->total[shipping] + $payment->total[handling];
			$values[payment][subtotal] = $payment->total[subtotal];*/
			
			// Values - card
			$values[creditcard][cardnumber] = $payment->card[number];
			$values[creditcard][cardexpmonth] = str_pad($payment->card[expiration_month],2,"0",STR_PAD_LEFT);
			$values[creditcard][cardexpyear] = substr($payment->card[expiration_year],2,2);
			$values[creditcard][cvmindicator] = "provided";
			$values[creditcard][cvmvalue] = $payment->card[code];
			
			// Values - address
			$values[billing][name] = $payment->address[first_name]." ".$payment->address[last_name];
			$values[billing][company] = $payment->address[company];
			$values[billing][address1] = $payment->address[address];
			$values[billing][address2] = $payment->address[address_2];
			$values[billing][city] = $payment->address[city];
			$values[billing][state] = $payment->address[state];
			$values[billing][country] = $payment->address[country];
			$values[billing][phone] = $payment->address[phone];
			$values[billing][fax] = NULL;
			$values[billing][email] = $payment->address[email];
			$values[billing][addrnum] = NULL; # Required for AVS. If not provided, transactions will downgrade.
			$values[billing][zip] = $payment->address[zip]; # Required for AVS. If not provided, transactions will downgrade.
			
			// Values - shipping
			$values[shipping][name] = $payment->shipping[first_name].($payment->shipping[last_name] ? " ".$payment->shipping[last_name] : "");
			$values[shipping][address1] = $payment->shipping[address];
			$values[shipping][address2] = $payment->shipping[address_2];
			$values[shipping][city] = $payment->shipping[city];
			$values[shipping][state] = $payment->shipping[state];
			$values[shipping][zip] = $payment->shipping[zip];
			$values[shipping][country] = $payment->shipping[country];
			
			// Values - items
			/*$items = array (
				'id' 			=> '123456-A98765',
				'description' 	=> 'Logo T-Shirt',
				'quantity' 		=> '1',
				'price'			=> '12.99',
				'options' => array
						  (  
						  'name' => 'Color',
						  'value'=> 'Red'
						  ),
				'options2' => array
						(
						'name' => 'Size',
						'value' => 'XL'
						)
				);
			$values[items][0] = $items; # put array of items into hash
		
			# you could also submit this same item w/ options like this:
			$values[items][item1]["id"] = "123456-A98765";
			$values[items][item1]["description"] = "Logo T-Shirt";
			$values[items][item1]["quantity"] = "1";
			$values[items][item1]["price"] = "12.99";
		
				# pass options in like this:
				$values[items][item1]["option1"]["name"] = "Color";
				$values[items][item1]["option1"]["value"] = "Red";
				$values[items][item1]["option2"]["name"] = "Size";
				$values[items][item1]["option2"]["value"] = "XL";
		
				# or you could alternately pass the same options in like this:
				# $values[items][item1][0]["name"] = "Color";
				# $values[items][item1][0]["value"] = "Red";
				# $values[items][item1][1]["name"] = "Size";
				# $values[items][item1][1]["value"] = "XL";
				
			// Values - Notes
			$values[notes][comments] = "Repeat customer. Ship immediately.";
			$values[notes][referred] = "Saw ad on Web site.";*/
			
			// XML
			$xml = $payment->xml_create(array('order' => $values));
			
			// Debug
			$payment->debug("url: ".$url);
			$payment->debug("xml: <xmp>".$xml."</xmp>");
		
			// Send
			$response = $payment->curl($url,$xml,array('cert' => $this->pem));
			
			// Parse
			$response_object = $payment->xml_object($response);
			
			// Debug
			$payment->debug("response: <xmp>".$response."</xmp>");
			$payment->debug("response_object: ".return_array($response));
			
			// Results - response
			if($response_object) {
				$results = array(
					'result' => ($response_object->r_approved == "APPROVED" ? 1 : 0),
					'message' => ($response_object->r_error ? $response_object->r_error : $response_object->r_message),
					'transaction' => $response_object->r_ordernum,
				);
			}
			// Results - no response
			else {
				$results = array(
					'result' => 0,
					'message' => "No response was returned.",
				);
			}
			
			// Results
			$results[action] = $action;
			$results[test] = $this->c[test];
			$results[source] = "First Data";
			$results[response] = $response;
			
			// Return
			return $results;
		}
		
		/**
		 * Detects some common errors for this gateway and returns the error message.
		 *
		 * @return string The error message (if an error was detected).
		 */
		function errors() {
			// No .pem certificate
			if(!$this->pem) {
				$error = "The path to the .pem certificate First Data provided you is missing.";
			}
			// No login credentials
			else if(!$this->store_number or !$this->c[host] or !$this->c[port]) {
				$error = "The login credentials for First Data are missing.";
			}	
			// Certificate path incorrect
			else if(!file_exists($this->pem)) {
				$error = "The path to the .pem certificate is incorrect.";
			}
			
			// Return
			return $error;
		}
	}
}
?>