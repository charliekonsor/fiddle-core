<?php
/************************************************************************************/
/************************************** Config **************************************/
/************************************************************************************/
$config = array(
	'name' => 'Modules',
	'single' => 'Module',
	'plural' => 'Modules',
	'icon' => 'module',
	'db' => array(
		'table' => 'modules',
		'id' => 'module_id',
		'code' => 'module_code',
		'name' => 'module_name',
		'visible' => 'module_visible'
	),
	'permissions' => array(
		'add' => array(
			'label' => 'Install',
			'manage' => 1,
			'default' => array()
		),
		'delete' => array(
			'label' => 'Delete',
			'manage' => 1,
			'default' => array()
		),
		/*'copy' => array( // Not sure I want this
			'label' => 'Copy',
			'manage' => 1,
			'default' => array()
		),*/
		'settings' => array(
			'label' => 'Settings',
			'default' => array(
				'admins',
			)
		),
	),
	'admin' => array(
		'section' => 'system',
		'views' => array(
			'home' => 'All Modules',
			'item' => 'Edit Module'
		),
		/*'urls' => array(
			'item' => array(
				'format' => 'admin/{module}/{code}'
			)
		),*/
	),
	'public' => array(
		'active' => 0
	),
	'plugins' => array(),
	'uploads' => array(
		'types' => array(
			'file' => array(
				'storage' => 'local',
			),
		)
	),
);
?>