<?php
if(!class_exists('form_input_file_framework')) include_once SERVER."core/framework/classes/form/form_input_file.php";
if(!class_exists('form_input_file_plupload') and class_exists('form_input_file_framework',false)) {
	/**
	 * An extension of the form_input_file_framework class with functionality specific to plupload inputs.
	 *
	 * Note, this should probably be 2 classes, form_input_file_plupload (which extends form_input_file)
	 * and form_input_file_plupload (which extends form_input_file_framework), but the difference
	 * in the two html_field() methods would only be the 'browse' bit, so skip it.
	 */
	class form_input_file_plupload extends form_input_file_framework {
		/**
		 * Loads class. Needed so we can use debug_backtrace() to get this class name in PHP < 5.3 in $form_input->load().
		 */
		static function load($label = NULL,$name = NULL,$value = NULL,$c = NULL) {
			return parent::load($label,$name,$value,$c);
		}
		
		/**
		 * Construct
		 */
		function __construct($label = NULL,$name = NULL,$value = NULL,$c = NULL) {
			$this->form_input_file_plupload($label,$name,$value,$c);
		}
		function form_input_file_plupload($label = NULL,$name = NULL,$value = NULL,$c = NULL) {
			// Construct
			parent::__construct($label,$name,$value,$c);
			
			// Attributes - class
			$this->attributes['class'] .= ($this->attributes['class'] ? " " : "")."plupload-placeholder";
		}
		
		/**
		 * Renders the HTML for the input's 'field'.
		 *
		 * @param object $form The form object this input is a part of.
		 * @param array $c An array of config values. Default = NULL
		 * @return string The HTML of the input's field.
		 */
		function html_field($form,$c = NULL) {
			// Id
			if(!$this->attributes[id]) $this->attributes[id] = "plupload-".random();
			
			// JS/CSS - Include both of these more than once (2nd param == 0) as input might get tested in $module->settings() method function without the HTML being displayed
			$html .= include_css('plupload',0); 
			$html .= include_javascript('plupload',0);
					
			// Prepend
			if($this->prepend) $html .= $this->prepend;
			
			// Javascript
			$html .= "
<script type='text/javascript'>
$(document).ready(function() {
	$('#".$this->attributes[id]."').pluploadQueue({
		runtimes: 'gears,flash,silverlight,browserplus,html5',
		url: DOMAIN+'core/core/libraries/plupload/upload.php',
		max_file_size: '10mb',
		chunk_size: '1mb',
		unique_names: true,";
			// Accpet
			if($this->validate[accept]) $html.= "
		filters: [
			{title: 'Files', extensions: '".implode(',',$this->validate[accept])."'}
		],";
			$html .= "
		flash_swf_url: DOMAIN+'core/core/libraries/plupload/plupload.flash.swf',
		silverlight_xap_url: DOMAIN+'core/core/libraries/plupload/plupload.silverlight.xap'
	});
	
	// Client side form validation
	var \$form = $('#".$this->attributes[id]."').closest('form');
	\$form.bindFirst('submit',function(e) {
		// Debug
		debug('Plupload - submit clicked');
		
		// Plupload
		var plupload = $('.plupload',this).pluploadQueue();

		// Debug
		debug('Plupload - file count: '+plupload.files.length);
		
		// Validate
		\$form.valid();
		
		// Files in queue upload them first
		if(plupload.files.length > 0) {
			// Validation error
			if($('label.error',\$form).is(':visible')) {
				return false;
			}
		
			// All uploaded	
			if(plupload.files.length === (plupload.total.uploaded + plupload.total.failed)) {
				return true;
			}
		
			// Start 'submit' action
			form_submit(\$form,{submit_form:0});
			
			// When all files are uploaded submit form
			plupload.bind('StateChanged', function() {
				// Debug
				debug('Plupload - new file count: '+plupload.files.length+', uploaded: '+plupload.total.uploaded+', failed: '+plupload.total.failed);
				
				// All uploaded
				if(plupload.files.length === (plupload.total.uploaded + plupload.total.failed)) {
					// Debug
					debug('Plupload - all uploaded, submitting');
					
					// Submit
					//\$form.off('submit');
					form_submit(\$form);
					//$('form')[0].submit();
				}
			});
			   
			// Start upload
			plupload.start();
		}
		else {";
			if($this->validate[required]) $html .= "
			// Turn off submit
			form_submit_complete(\$form);
			
			// Show error
			$('#".$this->attributes[id]."').siblings('.error').show();
			
			// Hide error when file added
			plupload.bind('FilesAdded', function() {
				$('".$this->attributes[id]."').siblings('.error').hide();
			});";
			else $html .= "
			debug('Plupload - no files, none needed, submiting form');
			return true;";
			$html .= "
		}
		
		// Return
		e.stopImmediatePropagation();
		return false;
	});";
			if($this->validate[required]) $html .= "
	// Error - hidden for now
	$('#".$this->attributes[id]."').after(\"<label class='error form-error' for='".$this->name."'>This field is required.</label>\");";
			$html .= "
});
</script>";
			
			// Input
			$html .= "
<div class='plupload-container'>
	<div class='plupload' id='".$this->attributes[id]."'>";
				
			// Input - will be hidden via javascript, but serves as the actual file input we use when plupload not enabled
			/*$input = form_input_file::load($this->label,$this->name,$this->value,$this->c_duplicate());
			$input->validate[required] = 0; // Don't require
			$html .= $input->html_element($form,$c);*/
			
			$html .= "
	</div>
</div>";
			
			// Append
			if($this->append) $html .= $this->append;
		
			// Help
			if($this->help) $html .= help($this->help);
				
			// Error
			if($this->error) $html .= "
				<label class='error form-error' for='".$this->name."' style='display:inline;'>".$this->error."</label>";
			
			// Return
			return $html;
		}
		
		
			
		/**
		 * Processes the value for this input.
		 *
		 * @param object $form The form object which contains the submitted values ($form->post and $form->files).
		 * @param array $c An array of configuration values. Default = NULL
		 * @return mixed The resulting value.
		 */
		function process($form,$c = NULL) {
			debug("name (0): ".$this->name,$this->c[debug]);
			// Config
			if($c) $this->c = array_merge($this->c,$c);
			
			// Loop through all values
			if($this->attributes[id]) {
				// Is this a value we're looking for?
				$length = strlen($this->attributes[id]) + 1;
				foreach($form->post as $k => $v) {
					if(substr($k,0,$length) == $this->attributes[id]."_") {
						list($x,$key) = explode('_',substr($k,$length));
						if($x != "count") {
							if($key == "tmpname") {
								$key = "tmp_name";
								$v = file::temp()."plupload/".$v;
							}
							$form->files[$this->name][$key][$x] = $v;
							$values[$x][$key] = $v;
						}
					}
				}
				debug("values: ".return_array($values),$this->c[debug]);
				debug("files: ".return_array($form->files[$this->name]),$this->c[debug]);
				
				// Process
				if($values) {
					// Don't want to 'delete' anything 
					$this->old = NULL;
					
					// Store original name
					$name = $this->name;
				
					// Loop through values
					foreach($values as $x => $v) {
						$this->name = $name."[".$x."]";
						debug("name: ".$this->name,$this->c[debug]);
						$values[$x] = parent::process($form,$c);
					}
					
					// Retstore name
					$this->name = $name;
				}
			}
			
			// Return
			return $values;
		}
	}
}
?>