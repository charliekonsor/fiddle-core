<?php
// Permission
if($this->permission('manage',1)) {
	// Heading
	print $this->heading();
	// Buttons
	print $this->heading_buttons();
	// Filters
	print $this->module_class->manage_filters();
	
	// Packages
	if(u('type') == "supers") {
		if($packages = packages(array('complete' => 1))) {
			print "
<b>Packages</b><br />
<table class='core-table core-rows'>";
			foreach($packages as $package => $v) {
				$modules = count($v[modules]);
				$plugins = count($v[plugins]);
				print "
	<tr>
		<td width='18'>
			<a href='".DOMAIN."?process_action=modules_download&amp;process_module=".$this->module."&amp;package=".$package."' class='i i-arrow-down i-inline tooltip' title='Download'></a>
		</td>
		<td>
			".$v[name];
				if($modules or $plugins) {
					print "	
			<span class='core-tiny'>
				(".($modules ? $modules." ".string_pluralize('module',$modules).($plugins ? ", " : "") : "").($plugins ? $plugins." ".string_pluralize('plugin',$plugins) : "").")
			</span>";
				}
				print "<br />
			<span class='core-tiny'>".$v[description]."</span>
		</td>
	</tr>";
			}
			print "
</table><br />";
		}
	}
	
	// Modules
	if(u('type') == "supers" and ($packages or plugin())) print "
<b>Modules</b><br />";
	$array = $this->module_class->manage_items(array('return' => 'array','perpage' => 9999));
	if($array[items]) {
		print "
<table class='core-table core-rows'>";
		foreach($array[items] as $item) {
			// Module
			$module = $item->db('code');
			$module_class = module::load($module);
			if(!$module_class->v('parent')) {
				// Icons
				$icons = NULL;
				// Icons - download - we'll check user type in icon(), but only want to show (link or faded) if a super admin
				if(u('type') == "supers") {
					$icons[download] = $item->icon('download',array('required' => 1));
					$icons['download-rows'] = $item->icon('download-rows',array('required' => 1));
				}
				// Icons - settings
				$icons[settings] = $item->icon('settings',array('required' => 1));
				// Icons - documentation
				$icons[documentation] = $item->icon('documentation',array('required' => 1));
				// Icons - disable
				//$icons[disable] = $item->icon('disable,array('required' => 1));
				// Icons - delete - we'll check permission in icon(), but only want to show (link or faded) if we have permission
				if($item->permission('delete')) $icons[delete] = $item->icon('delete',array('required' => 1));
				
				// Icons - filter
				$icons = array_filter($icons);
				$icons_width = (count($icons) * 18);
					
				// Row
				print "
	<tr id='".$this->module."-".$item->id."'>
		<td width='".$icons_width."'>".implode($icons)."</td>
		<td>".$item->name."</td>
	</tr>";
			}
		}
		print "
</table><br />";
	}
	else print "
<div class='core-none'>No ".$this->module_class->v('plural')." Found</div><br />";

	// Plugins
	if(u('type') == "supers") {
		if($plugins = plugin()) {
			print "
<b>Plugins</b><br />
<table class='core-table core-rows'>";
			foreach($plugins as $plugin => $v) {
				// Plugin
				$plugin_class = plugin::load(NULL,NULL,$plugin);
				if(!$plugin_class->v('parent')) {
					// Icons
					$icons = NULL;
					// Icons - download - we'll check user type in icon(), but only want to show (link or faded) if a super admin
					if(u('type') == "supers") $icons[download] = $plugin_class->icon('download',array('required' => 1));
					// Icons - documentation
					$icons[documentation] = $plugin_class->icon('documentation',array('required' => 1));
					// Icons - filter
					$icons = array_filter($icons);
					$icons_width = (count($icons) * 18);
					
					// Row
					print "
	<tr>
		<td width='".$icons_width."'>".implode($icons)."</td>
		<td>".$v[name]."</td>
	</tr>";
				}
			}
			print "
</table><br />";
		}
	}
}
?>