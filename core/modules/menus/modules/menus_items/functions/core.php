<?php
/**
 * @package kraken\modules\menus\menus_items
 */
if(!function_exists('menu_nest')) {
	/**
	 * Creates the display for nested menus 
	 *
	 * @param int $id The id of the menu you want to get the nested items of.
	 * @param int $parnet The parent item's id (used when we get down to sub-levels of nesting. Default = 0
	 * @return string The HTML for displaying the nested menu items.
	 */
	function menu_nest($id,$parent = 0) {
		// Module
		$module = 'menus_items';
		$m = module::load($module);
		
		// Items
		$items = $m->rows(array('db.parent_id' => $id,'db.order_parent' => $parent,'order' => m($module.'.db.order')." ASC"));
		if($m->db->n($items)) {
			$text .= "
<ul class='nest-ul".(!$parent ? " nest {module:\"".$module."\"}" : "")."'>";
			while($item = $m->db->f($items)) {
				$item = $m->item($item);
				if(!$item->name) $linked_item = item::load($item->v('item_module'),$item->v('item_id'));
				$text .= "
	<li class='clear nest-item".($item->visible ? "" : " core-fade")."' id='".$module."-".$item->id."'>
		<div class='nest-handle core-row'>
			<div class='nest-icons'>
				".$item->icon('edit')."
				".$item->icon('delete')."
			</div>
			<a href='".$item->url('admin')."'>".($item->name ? $item->name : $linked_item->name)."</a>
		</div>
		".menu_nest($id,$item->id)."
	</li>";	
			}
			$text .= "
</ul>";
		}
		
		return $text;
	}
}
?>