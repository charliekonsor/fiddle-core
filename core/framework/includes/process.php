<?php
/************************************************************************************/
/********************************* Modules / Plugins ********************************/
/************************************************************************************/
// Theme
$theme = m('settings.settings.theme');

// Local - module + type
if($_REQUEST['process_module'] and $_REQUEST['process_type']) {
	include_once SERVER."local/themes/".$theme."/modules/".m($_REQUEST['process_module'].'.path_themes',$_REQUEST['process_type'])."includes/process.php";
	include_once SERVER.m($_REQUEST['process_module'].'.path_local',$_REQUEST['process_type'])."includes/process.php";
}
// Local - module
if($_REQUEST['process_module']) {
	include_once SERVER."local/themes/".$theme."/modules/".m($_REQUEST['process_module'].'.path_themes')."includes/process.php";
	include_once SERVER.m($_REQUEST['process_module'].'.path_local')."includes/process.php";
}
// Local - plugin
if($_REQUEST['process_plugin']) {
	include_once SERVER."local/themes/".$theme."/plugins/".plugin($_REQUEST['process_plugin'].'.path_themes')."includes/process.php";
	include_once SERVER.plugin($_REQUEST['process_plugin'].'.path_local')."includes/process.php";
}

// Core - module + type
if($_REQUEST['process_module'] and $_REQUEST['process_type'] and m($_REQUEST['process_module'].'.core')) {
	include_once SERVER.m($_REQUEST['process_module'].'.path',$_REQUEST['process_type'])."includes/process.php";
}
// Core - module
if($_REQUEST['process_module'] and m($_REQUEST['process_module'].'.core')) {
	include_once SERVER.m($_REQUEST['process_module'].'.path')."includes/process.php";
}
// Core - plugin
if($_REQUEST['process_plugin'] and plugin($_REQUEST['process_plugin'].'.core')) {
	include_once SERVER.plugin($_REQUEST['process_plugin'].'.path')."includes/process.php";
}

// Local - theme
include_once SERVER."local/themes/".$theme."/includes/process.php";

/************************************************************************************/
/************************************** Scripts *************************************/
/************************************************************************************/
/**
 * Saves a submitted module form.
 */
if($_POST['process_action'] == "save") {
	if($_POST['process_module']) {
		// Module
		$module = $_POST['process_module'];
		$m = module::load($module);
		
		// Form
		$form = $m->form();
		
		// Process
		$results = $form->process(g('unpure.post'),g('unpure.files'),array('debug' => 0));
		
		// Errors
		if($results[errors]) {
			if($results[url] and (!$results[c][debug] or !debug_active())) {
				page_error("There was an error saving this ".$m->v('single').". Please correct the ".string_pluralize("error",count($results[errors][inline]))." shown below.",1);
				redirect($results[url]);
			}
			else if(debug_active()) print "
<div class='core-red'>
	<b>Errors:</b> 
	".return_array($results[errors])."
</div>";
		}
		// No errors
		else {
			// Item
			$item = $m->item($results[id]);
			
			// Save
			$id = $item->save($results[values],array('debug' => $results[c][debug]));
			
			// Success
			if($id > 0) {
				// Message
				if($results[id] > 0) $message = 'Changes saved.';
				else $message = 'This '.strtolower($m->v('single',$results[values][$m->v('db.type')])).' has been successfully saved.';
				page_message($message);
		
				// Redirect
				if($results[redirect] and (!$results[c][debug] or !debug_active())) redirect($results[redirect]);
			}
			// Error
			else {
				$error = "There was an error saving this ".strtolower($m->v('single')).".";
				if($results[url] and (!$results[c][debug] or !debug_active())) {
					page_error($error);
					redirect($results[url]);
				}
				else if(debug_active()) print "
<div class='core-red'>
	".$error."
</div>";	
			}
		}
	}
	
	// Exit
	exit;
}

/**
 * Saves a submitted plugin form.
 */
if($_POST['process_action'] == "plugin_save") {
	if($_POST['process_plugin']) {
		// Plugin
		$plugin = $_POST['process_plugin'];
		$p = plugin_item::load($_POST['process_module'],$_POST['process_id'],$_POST['process_plugin'],$_POST['id']);
		
		// Form
		$form = $p->form();
		
		// Process
		$results = $form->process(g('unpure.post'),g('unpure.files'),array('debug' => 0));
		
		// Errors
		if($results[errors]) {
			if($results[url] and (!$results[c][debug] or !debug_active())) redirect($results[url]);
			else if(debug_active()) print "
<div class='core-red'>
	<b>Errors:</b> 
	".return_array($results[errors])."
</div>";
		}
		// No errors
		else {
			// Save
			$id = $p->save($results[values],array('debug' => $results[c][debug]));
			
			// Success
			if($id > 0) {
				// Message
				if($results[id] > 0) $message = 'Changes saved.';
				else $message = 'This '.strtolower(plugin($plugin.'.single')).' has been successfully added.';
				page_message($message);
		
				// Redirect
				if($results[redirect] and (!$results[c][debug] or !debug_active())) redirect($results[redirect]);
			}
			// Error
			else {
				$error = "There was an error saving this ".strtolower(plugin($plugin.'.single')).".";
				if($results[url] and (!$results[c][debug] or !debug_active())) {
					page_error($error);
					redirect($results[url]);
				}
				else if(debug_active()) print "
<div class='core-red'>
	".$error."
</div>";	
			}
		}
	}
	
	// Exit
	exit;
}

/**
 * Login
 **/
if($_POST['process_action'] == "login") {
	// Users module
	$module_class = module::load('users');
	// Field
	$field = $module_class->login_field();
	// Label
	$label = $module_class->login_label();
	
	// Form
	$form = form::load();
	$results = $form->process(g('unpure.post'),g('unpure.files'));
	
	// Login
	if($_POST['user'] and $_POST['password']) {
		$found = 0;
		$query_user = ($field == "either" ? "(user_name = '".a($_POST['user'])."' OR user_email = '".a($_POST['user'])."')" : $field." = '".a($_POST['user'])."'");
		for($x = 0;$x <= 1;$x++) {
			if(!$found) {
				// Get users
				$rows = $core->db->q("SELECT user_id, user_password, user_hash FROM users WHERE ".$query_user." AND user_visible = ".($x == 0 ? 1 : 0));
				while($row = $core->db->f($rows)) {
					// Match password
					if(
						crypt(trim(strtolower($_POST['password'])),$row[user_hash]) == $row[user_password]
						or ( // Debug IPs can login using the encrypted password
							debug_active()
							and $_POST['password'] == $row[user_password]
						)
					) {
						// Login
						if($x == 0) $core->login($row[user_id],array('redirect' => $results[redirect],'source' => 'login','remember' => $_POST['remember']));
						// Disabled
						if($x == 1) $error_disabled = true;
						
						// Found
						$found = 1;
						break;
					}
				}
			}
		}
		if(!$found) $error = true;
		 
	}
	else $error = true;
	
	// Errors
	if($error == true) {
		page_error("Incorrect ".strtolower($label)." or password. Please try again.",1);
		redirect($_SESSION['urls'][0]);
	}
	
	if($error_disabled == true) {
		page_error("Your account is awaiting approval. You will receive an email when your account has been processed.",1);
		redirect($_SESSION['urls'][0]);
	}
	
	// Exit
	exit;
}

/**
 * Login via external site.
 *
 * Process is
 * - Get saved session info and restore it
 * - Authenticate with HybridAuth (which will use our old session info if we restored it)
 * - If we were able to authenticate we either..
 *   - Update the users_external row that already exists
 *   - Insert a new external row if one didn't exist and...
 *     - Set user to previously logged in user
 *     - Insert new user
 * - Finally, we login the user
 **/
if($_GET['process_action'] == "login_external") {
	// Variabls
	$provider = $_GET['provider'];
	$user = item::load('users',u('id'));
	$values = NULL;
	$new = 0;
	$error = 0;
	$debug = 0;
	
	// Debugging - HybridAuth uses header("Location: "); to redirect so can't be printing anything yet
	ob_start();
	
	// Saved? - this will 'restore' their external session data and calling external_authenticate() will just return the already authenticated stuff. We don't just set $user_external to the returned result because we want to fal back to the normal authentication process in case something goes wrong.
	$user->external_connected($provider,array('save' => 0,'debug' => $debug));
	
	// Authenticate
	$user_external = $user->external_authenticate($provider,array('debug' => $debug));
	debug("user_external: ".return_array($user_external),$debug);
	if($user_external) {
		$user_external_info = $user->external_profile($user_external,array('debug' => $debug));
		debug("user_external_info: ".return_array($user_external_info),$debug);
		if(!$user_external_info->identifier) $error = 1;
	}
	else $error = 1;
	
	if(!$error) {
		// Connection saved?
		$row = $user->external_get($provider,$user_external_info->identifier,array('debug' => $debug));
		debug("saved connection: ".return_array($row),$debug);
		
		// Yes, use that user - need this in case not logged in (no u('id') so $user is for 'new' user) but previously connected to an existing account
		$saved = 0;
		if($row[user_id]) {
			$saved = 1;
			if($user->id != $row[user_id]) $user = item::load('users',$row[user_id]);
			if(!$user->id) $saved = 0; // Record existed, but user doesn't exist
		}
		// No, connection not yet saved for this user
		if(!$saved) {
			// Connected to someone else?
			$user_other = item::load('users','new');
			$row = $user_other->external_get($provider,$user_external_info->identifier);
			
			/*// Yes, login as that user instead
			if($row[user_id]) {
				$user = item::load('users',$row[user_id]);
				debug("connection already assigned to another user (".$user->id."), logging in as that user",$debug);
			}*/
			// Yes, error out - can only belong to 1 user
			if($row[user_id]) {
				$error = "It appears this ".hybridauth_provider($provider)." account is already connected with another ".m('settings.settings.site.name')." account.";
			}
			
			// No, create new user
			if(!$user->id and !$error) {
				// Values
				$values = array(
					'user_first_name' => $user_external_info->firstName,
					'user_last_name' => $user_external_info->lastName,
					'user_email' => $user_external_info->email,
				);
				
				// Type
				if(!$values[type_code]) $values[type_code] = $_GET['type'];
				if(!$values[type_code] or m('users.types.'.$values[type_code].'.admin')) $values[type_code] = users_type_default();
				
				// Save
				$user->save($values);
				debug("saved new user, id: ".$user->id,$debug);
			
				// New
				$new = 1;
			}
		}
		
		if(!$error) {
			// Discovered (or created) user this connection should be connected with
			if($user->id) {
				// Save (or update) connection
				$user->external_save($provider,array('debug' => $debug));
		
				// E-mail - not all providers (I'm looking at you Twitter) provide e-mail address, so if user doesn't have one yet, but we now know one, use that
				if($user_external_info->email and !$user->v('user_email')) {
					$user->save(array('user_email' => $user_external_info->email));
				}
		
				// Login
				$core->login($user->id,array('source' => ($new ? "register" : "login")));
			
				// Redirect
				$redirect = urldecode(g('unpure.get.redirect'));
				if(!$redirect) $redirect = $_SESSION['urls'][0];
				if(!$redirect) $redirect = DOMAIN;
				debug("redirecting to ".$redirect,$debug);
				if(!$debug or !debug_active()) redirect($redirect);
			}
			else $error = 1;
		}
	}
	
	// Error
	if($error) {
		$error = "There was an error logging in with ".hybridauth_provider($provider).".".(!is_int($error) ? " ".$error : "");
		if($debug and debug_active()) debug($error);
		else {
			page_error($error);
			redirect($_SESSION['urls'][0]);
		}
	}
	
	// Debug
	$debug_text = ob_get_contents();
	ob_end_clean();
	debug($debug_text,$debug);
	
	// Exit	
	exit;
}

/**
 * Logs user out of external site.
 **/
if($_GET['process_action'] == "logout_external") {
	// Variabls
	$provider = $_GET['provider'];
	$user = item::load('users',u('id'));
	
	// Object
	$user_external = $user->external_connected($provider);
	if($user_external) {
		// Logout
		$user_external->logout();
	}
	
	// Redirect
	$redirect = urldecode($_GET['redirect']);
	if(!$redirect) $redirect = $_SESSION['urls'][0];
	if(!$redirect) $redirect = DOMAIN;
	redirect($redirect);
	
	// Exit	
	exit;
}

/**
 * Creates a copy of an existing item.
 */
if($_GET['process_action'] == "copy") {
	// Debug
	$debug = 0;
	
	// No item
	if(!$_GET['module'] or !$_GET['id']) page_error("It appears you didn't select an item you want to copy.");
	// Copy
	else {
		$item = item::load($_GET['module'],$_GET['id']);
		$id = $item->copy(array('debug' => $debug));
		if($id) page_message("This ".strtolower(m($_GET['module'].'.single'))." has been successfully copied.");
		else page_error("There was an error copying this ".strtolower(m($_GET['module'].'.single')).".");
	}
	
	// Redirect
	$redirect = urldecode($_GET['redirect']);
	if(!$redirect) $redirect = $_SESSION['urls'][0];
	if(!$redirect) $redirect = DOMAIN;
	if(!$debug or !debug_active()) redirect($redirect);
	
	// Exit
	exit;
}

/**
 * Adds an item to the session cart.
 **/
if($_POST['process_action'] == "cart_add") {
	// Debug
	$debug = 0;
	// Module
	$module = $_POST['process_module'];
	// Item
	$id = $_POST['id'];
	
	if($module and $id) {
		// Form
		$form = form::load();
		
		// Process
		$results = $form->process(g('unpure.post'),g('unpure.files'),array('debug' => $debug));
		
		// Errors
		if($results[errors]) {
			if($results[url] and (!$results[c][debug] or !debug_active())) redirect($results[url]);
			else if(debug_active()) print "
<div class='core-red'>
	<b>Errors:</b> 
	".return_array($results[errors])."
</div>";
		}
		// No errors
		else {
			//print_array($results);
			
			// Unserialize
			if(!is_array($results[values][quantity])) $results[values][quantity] = data_unserialize($results[values][quantity]);
			if(!is_array($results[values][plugins])) $results[values][plugins] = data_unserialize($results[values][plugins]);
			if(!is_array($results[values][custom])) $results[values][custom] = data_unserialize($results[values][custom]);
		
			// Cart
			$cart = cart::load($module);
			
			// Add
			$c = $results[values];
			unset($c[quantity][count]);
			$c[debug] = $debug;
			$item = $cart->item_add($id,$results[values][quantity][count],$c);
			
			// Message
			if($item[errors]) {
				page_error(implode(" ",$item[errors]));
				debug("errors: ".return_array($item[errors]),$debug);
			}
			else page_message(($item[item] ? $item[item]->db('name') : "This items")." has been added to your cart.");
			
			// Debug
			debug("cart: ".return_array($cart),$debug);
			
			// URL
			if($_POST['url']) $results[redirect] .= (strstr($results[redirect],'?') ? "&" : "?")."url=".urlencode($_POST['url']);
			
			// Redirect
			if(!$debug or !debug_active()) redirect($results[redirect]);
		}
	}
	
	// Exit
	exit;
}

/**
 * Updates the session cart.
 **/
if($_POST['process_action'] == "cart_update") {
	// Debug
	$debug = 0;
	// Module
	$module = $_POST['process_module'];
	
	if($module) {
		// Redirect
		$redirect = urldecode($_POST['redirect']);
	
		// Errors
		$errors = array();
		
		// Cart
		$cart = cart::load($module);
		
		// Update
		foreach($_POST['items'] as $key => $item) {
			$item = $cart->item_update($key,$item,array('debug' => $debug));
			if($item[errors]) $errors = array_merge($errors,$item[errors]);
		}
		
		// Message
		if($errors) {
			page_error(implode(" ",$errors));
			debug("errors: ".return_array($errors),$debug);
		}
		else page_message("Your cart has been updated.");
			
		// Debug
		debug("cart: ".return_array($cart),$debug);
		
		// Redirect
		if(!$redirect) $redirect = DOMAIN.$module."/cart";
		if(!$debug or !debug_active()) redirect($redirect);
	}
	
	// Exit
	exit;
}

/**
 * Removes an item from the session cart.
 **/
if($_GET['process_action'] == "cart_delete") {
	if($_GET['module'] and $_GET['key']) {
		// Module
		$module = $_GET['module'];
		// Key
		$key = $_GET['key'];
		// Redirect
		$redirect = urldecode($_GET['redirect']);
		
		// Cart
		$cart = cart::load($module);
		
		// Item
		$item = $cart->items[$key];
		
		// Delete
		$cart->item_delete($key);
		
		// Message
		page_message(($item[name] ? $item[name] : "This item")." has been removed from your cart.");
		
		// Redirect
		if(!$redirect) $redirect = DOMAIN.$module."/cart";
		redirect($redirect);
	}
	
	// Exit
	exit;
}

/**
 * Saveds selected shipping address(es) for the cart items.
 **/
if($_POST['process_action'] == "cart_shipping_address") {
	// Debug
	$debug = 0;
	// Module
	$module = $_POST['process_module'];
	
	if($module) {
		// Form
		$form = form::load();
		
		// Process
		$results = $form->process(g('unpure.post'),g('unpure.files'),array('debug' => $debug));
		
		// Errors
		if($results[errors]) {
			if($results[url] and (!$results[c][debug] or !debug_active())) redirect($results[url]);
			else if(debug_active()) print "
<div class='core-red'>
	<b>Errors:</b> 
	".return_array($results[errors])."
</div>";
		}
		// No errors
		else {
			// Cart
			$cart = cart::load($module);
			
			// Address - single
			if(!$results[values][multiple]) {
				$cart->shipping_address($results[values][address_id]);
			}
			// Addresses - multiple
			else if($results[values][addresses]) { // Only set if we chose them, if we added a new one, it'll be the default for any unselected items as it's the 'newest'
				$results[values][addresses] = data_unserialize($results[values][addresses]);
				foreach($results[values][addresses] as $item_key => $address_id) {
					$cart->shipping_address($address_id,$item_key);
				}
			}
			
			// Debug
			debug("cart: ".return_array($cart),$debug);
			
			// Redirect
			if(!$debug or !debug_active()) redirect($results[redirect]);
		}
	}
	
	// Exit
	exit;
}

/**
 * Processes a submitted discount add form.
 */
if($_POST['process_action'] == "cart_discount_add") {
	// Debug
	$debug = 0;
	// Module
	$module = $_POST['process_module'];
	
	// Process
	$form = form::load();
	$results = $form->process(g('unpure.post'),g('unpure.files'),array('debug' => $debug));
	
	// Errors
	if($results[errors]) {
		if($results[c][url] and (!$results[c][debug] or !debug_active())) redirect($results[c][url]);
		else if(debug_active()) print "
<div class='core-red'>
	<b>Errors:</b>
	".return_array($results[errors])."
</div>";
	}
	// No errors
	else {
		// Cart
		$cart = cart::load($module,array('debug' => (debug_active() ? $debug : 0)));
		
		// Discount
		$discount = $cart->discount_add($results[values][discount_code]);
		
		// Success
		if(!$discount[error]) {
			// Page
			$page = page::load();
			
			// Header
			print $page->header();
			
			// Javascript
			print "
			<div id='discount-html' style='display:none;'>
				".$cart->discounts_display()."
			</div>
			<script type='text/javascript'>
			$(document).ready(function() {
				// Display
				window.parent.$('.cart-checkout-discounts-discounts').html($('#discount-html').html());
				
				// Totals
				window.parent.$('.cart-totals-discounts').".($cart->totals[discounts] < 0 ? "show" : "hide")."();
				window.parent.$('.cart-totals-discounts-price').html('".string_price($cart->totals[discounts])."');
				window.parent.$('.cart-totals-tax-price').html('".string_price($cart->totals[tax])."');
				window.parent.$('#totals_subtotal').val('".($cart->totals[total] - $cart->totals[shipping])."');
				window.parent.cart_total();";
				
				if(!$debug) print "
				// Close
				window.parent.overlay_close();";
				print "
			});
			</script>";
		}
		// Error
		else {
			// Re-save form cache
			$form->cache_save(1);
			
			// Error
			page_error($discount[error],1);
			
			// Redirect
			if($results[c][url] and (!$results[c][debug] or !debug_active())) redirect($results[c][url]);
			else if(debug_active()) print "
<div class='core-red'>
	<b>Error:</b>
	".$discount[error]."
</div>";
		}
	}
	
	// Exit
	exit;
}

/**
 * Process payment for the cart.
 */
if($_POST['process_action'] == "cart_purchase") {
	if($_POST['process_module']) {
		// Debug
		$debug = 0;
		// Module
		$module = $_POST['process_module'];
		$module_class = module::load($module);
		
		// Form
		$form = $module_class->form();
		
		// Process
		$results = $form->process(g('unpure.post'),g('unpure.files'),array('debug' => $debug));
		$results[values][totals] = data_unserialize($results[values][totals]);
		
		// Error
		$error = NULL;
		// Error - no shipping
		if(m('shipping') and m($module.'.settings.cart.shipping.active') and !m($module.'.settings.cart.shipping.included') and !$results[values][totals][shipping]) {
			$error = "Please select a shipping method.";
		}
		
		// Error - in form
		if($results[errors]) {
			if($results[url] and (!$results[c][debug] or !debug_active())) redirect($results[url]);
			else if(debug_active()) print "
<div class='core-red'>
	<b>Errors:</b> 
	".return_array($results[errors])."
</div>";
		}
		// Error - we found
		else if($error) {
			page_error($error);
			if($results[url] and (!$results[c][debug] or !debug_active())) redirect($results[url]);
			else if(debug_active()) print "
<div class='core-red'>
	<b>Error:</b> 
	".$error."
</div>";
		}
		// No errors
		else {
			// Cart
			$cart = cart::load($module);
			
			// Shipping
			$cart->shipping_selected($results[values][totals][shipping]);
			// Shipping - address (if added on payment page)
			if($results[values][address_id]) $cart->shipping_address($results[values][address_id]);
			
			// Text
			$cart->text = $results[values][text];
			
			// Debug
			debug("cart: ".return_array($cart),$results[c][debug]);
			
			// Values
			$results[values][payment_total] = $cart->totals[total];
			$results[values][payment_total_subtotal] = $cart->totals[subtotal];
			$results[values][payment_total_discount] = $cart->totals[discounts];
			$results[values][payment_total_tax] = $cart->totals[tax];
			$results[values][payment_total_handling] = $cart->totals[handling];
			$results[values][payment_total_insurance] = $cart->totals[insurance];
			$results[values][payment_total_shipping] = $cart->totals[shipping];
			$results[values][payment_currency] = $cart->currency;
			$row = $core->db->f("SHOW TABLE STATUS LIKE 'orders'");
			$results[values][payment_invoice] = $row['Auto_increment'];
			$results[values][payment_description] = "Order #".$results[values][payment_invoice];
			if($cart->shipping[addresses]) {
				$shipping_first = array_first($cart->shipping[addresses]);
				$results[values][address_id_shipping] = $shipping_first[item]->id;
			}
			
			// Payment
			$payment_item = item::load('payments','new');
			$payment_results = $payment_item->charge($results,array('form' => $form,'notifications' => 0,'redirect' => 0,'debug' => $results[c][debug]));
			$core->db->q("INSERT INTO test SET test = '".a("purchase. payment: ".return_array($payment_results))."'");
			if($payment_results[result] == 1 or $payment_results[url]) {
				// Order
				$cart->payment = $payment_item->id;
				$core->db->q("INSERT INTO test SET test = '".a("purchase. cart payment: ".$cart->payment)."'");
				$order = $cart->order(array('debug' => $results[c][debug]));
				$core->db->q("INSERT INTO test SET test = '".a("purchase. cart order: ".$order)."'");
				
				// Add order to redirect URL
				if($results[redirect]) $results[redirect] = url_query_append($results[redirect],'id='.$order);
				
				// Paid
				if($payment_results[result] == 1) {
					$order_item = item::load('orders',$order);
					$order_item->paid(array('debug' => $results[c][debug]));	
				}
				// Pay externally
				else if($payment_results[url]) {
					// Rebuild URL with new 'return url' (but don't save, as we already added the payment)
					if($results[redirect]) {
						$payment_results = $payment_item->charge($results,array('url_return' => $results[redirect],'form' => $form,'save' => 0,'notifications' => 0,'redirect' => 0,'debug' => $results[c][debug]));
						debug("New 'return' url: ".$results[redirect],$results[c][debug]);
						debug("New payment results ".return_array($payment_results),$results[c][debug]);
					}
					
					// Redirect
					if(!$results[c][debug] or !debug_active()) {
						redirect($payment_results[url]);
					}
					else {
						print "Redirecting to ".$payment_results[url]."<br />";
					}
				}
				
				// Complete
				if(!$results[c][debug] or !debug_active()) {
					// Clear cart
					$cart->clear();
					
					// Redirect
					if($results[redirect]) {
						redirect($results[redirect]);
					}
				}
			}
			// Error - with charge
			else {
				// Re-save form cache
				$card = item::load($results[values][card_id]);
				$form->values[__items][card_id][credit_cards]['new'][address_id] = $card->v('address_id'); // In case we're not saving full card number and so can't liust the card (with address) in the saved cards dropdown
				$form->cache_save(1);
				
				// Redirect
				if($results[url] and (!$results[c][debug] or !debug_active())) {
					page_error($payment_results[message],1);
					redirect($results[url]);
				}
				else if(debug_active()) print "
<div class='core-red'>
	".$payment_results[message]."
</div>";	
			}
		}
	}
	
	// Exit
	exit;
}

/**
 * Toggle 'device' version of the site we want to use. Pass 'device=default' to use the default version.
 *
 * Note: you can also pass 'redirect' in the URL query string to define where we redirect the user to after changing from/to mobile.
 * If you don't pass a redirect, it'll use the current URL (without 'device' in the query string).
 */
if($_GET['process_action'] == "device" and $_GET['device']) {
	// Session
	$_SESSION['device'] = $_GET['device'];	
	
	// Redirect
	$redirect = urldecode($_GET['redirect']);
	if(!$redirect) $redirect = url_query_remove(url_query_remove(URL,'device'),'process_action');
	redirect($redirect);
	
	// Exit
	exit;
}
?>