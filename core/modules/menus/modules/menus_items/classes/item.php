<?php
if(!class_exists('item_menus_items',false)) {
	/**
	 * Exends the core item class with scripts specific to the the menus_items module.
	 *
	 * @package kraken\modules\menus\menus_items
	 */
	class item_menus_items extends item {
		/**
		 * construct
		 */
		function __construct($module = NULL,$id = NULL,$c = NULL) {
			$this->item_menus_items($module,$id,$c);
		}
		function item_menus_items($module = NULL,$id = NULL,$c = NULL) {
			parent::item($module,$id,$c);
		}
		
		/**
		 * visible_check
		 */
		function visible_check() {
			// Core
			$visible = parent::visible_check();
			
			// Item/category/type
			if($this->v('item_id')) {
				// Item
				$item = item::load($this->v('item_module'),$this->v('item_id'));
				
				// Item isn't visible, make menu item invisible too
				if(!$item->db('visible')) $visible = 0;
			}
			
			// Return 
			return $visible;
		}
		
		/**
		 * Determines the URL for viewing an item in the given area.
		 *
		 * This only handes 'public' URLs and returns the URL of the item the menu item is linking to,
		 * not the a link to the actual menu item item.
		 *
		 * @param string $area The area of the site where this URL will link to: public [default], admin, account
		 * @param array $c An array of configuration values. Default = NULL
		 * @return string The URL of the item in the given area (if it exists).
		 */
		function url($area = NULL,$c = NULL) {
			// Error
			if(!$this->module) return;
			
			// Variables
			$url = NULL;
			if(!$area) $area = "public";
				
			// Public - here we're catching calls for the public menu item URL and instead returning the URL the menu item is linking to
			if($area == "public") {
				// URL - external
				if($this->v('item_type') == "external") {
					if(
						substr($this->v('item_url'),0,4) != "http"
						and substr($this->v('item_url'),0,7) != "mailto:"
						and substr($this->v('item_url'),0,4) != "tel:"
						and substr($this->v('item_url'),0,11) != "javascript:"
					) $url = "http://".$this->v('item_url');
					else $url = $this->v('item_url');
				}
				// URL - local
				else if($this->v('item_type') != "nolink") {
					// View - item/category/type
					if($this->v('item_id')) {
						$linked_item = item::load($this->v('item_module'),$this->v('item_id'));
						$url = $linked_item->url($this->v('module_area'),array('module' => $this->v('module_code'),'view' => $this->v('module_view')));
					}
					// View - module
					else if($this->v('module_code')) {
						$linked_module = module::load($this->v('module_code'));
						$url = $linked_module->url($this->v('module_view'),$this->v('module_area'));
					}
					// Area - home
					else if($this->v('module_area')) {
						$url = DOMAIN.($this->v('module_area') != "public" ? $this->v('module_area') : "");	
					}
				}
			}
			// Default
			else {
				$url = parent::url($area,$c);
			}
			
			// Return
			return $url;
		}
		
		/**
		 * Return the 'name' to use when displaying this menu link.
		 *
		 * @return string The 'name' to use when displaying this menu link.
		 */
		function name() {
			// Name
			$name = $this->db('name');
			if(!x($name) and $this->v('item_id')) {
				$linked_item = item::load($this->v('item_module'),$this->v('item_id'));
				if($linked_item) $name = $linked_item->db('name');
			}
			
			// Return
			return $name;
		}
	}
}
?>