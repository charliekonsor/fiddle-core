<?php
/**
 * @package kraken\modules\themes
 */
 
if(!function_exists('themes')) {
	/**
	 * Returns an array of available themes and their settings or (if $code passed) the given theme's settings.
	 *
	 * @param string $code The code of a specific theme you want to get (as opposed to all the themes). Default = NULL
	 * @return array An array of available themes and their related settings or (if $code passed) the settings for a single theme.
	 */
	function themes($code = NULL) {
		// Speed
		$f_r = function_speed(__FUNCTION__);
		
		// Cached
		$cache_name = 'themes/'.($code ? $code : '__all');
		if($themes = cache_get($cache_name)) {
			// Speed
			function_speed(__FUNCTION__,$f_r,1);
			// Return
			return $themes;
		}
		
		// File
		// Extensions
		$extensions = array('.php','.html','.htm');
		
		// Module
		$module = 'themes';
		$m = module::load($module);
		
		// Get themes
		$path = SERVER."local/themes/";
		$themes = file::directory_folders($path);
		if($themes) {
			foreach($themes as $theme) {
				if(!$code or $theme == $code) {
					// Config
					$config = NULL;
					include $path.$theme."/config.php";
					$array[$theme] = $config;
					
					// Item (if exists)
					$row = $m->row(array('db.code' => $theme));
					$item = $m->item($row);
						
					// Exists
					if($item->id) {
						// Settings
						$settings = data_unserialize($item->v('theme_settings'));
						if($settings) {
							if($array[$theme][settings]) $array[$theme][settings] = array_merge($array[$theme][settings],$settings);
							else $array[$theme][settings] = $settings;
						}
					}
					// Doesn't exist, insert
					else {
						$values = array(
							'db.code' => $theme,
							'db.name' => ($config[name] ? $config[name] : $theme)
						);
						$item->save($values);
					}
					
					// Variables
					$array[$theme][id] = $item->id;
					$array[$theme][code] = $item->code;
					if(!$array[$theme][name]) $array[$theme][name] = $item->name;
					
					// Templates
					$templates = file::directory_files($path.$theme."/templates/");
					if($templates) {
						foreach($templates as $template) {
							$extension = ".".file::extension($template);
							if(in_array($extension,$extensions)) {
								$name = str_replace($extensions,'',$template);
								$array[$theme][templates][$template] = $name;
							}
						}
						ksort($array[$theme][templates]);
					}
				}
			}
		}
		
		// Single theme
		if($code) $array = $array[$code];
		// All themes - sort alphabetically
		else if($array) ksort($array);
		
		// Cache
		cache_save($cache_name,$array);
		
		// Speed
		function_speed(__FUNCTION__,$f_r);
		
		// Return
		return $array;
	}
}

if(!function_exists('theme')) {
	/**
	 * Returns settings for the given theme.
	 *
	 * @param string $code The code of a specific theme you want to get the settings of.
	 * @return array An array of settings for the given theme.
	 */
	function theme($code) {
		if($code) return themes($code);
	}
}
?>