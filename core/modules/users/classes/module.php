<?php
if(!class_exists('module_users',false)) {
	/**
	 * Extends the module class, providing scripts specific to the 'users' module.
	 *
	 * @package kraken\modules\users
	 */
	class module_users extends module {
		/**
		 * construct
		 */
		function __construct($module,$c = NULL) {
			$this->module_users($module,$c);
		}
		function module_users($module,$c = NULL) {
			parent::__construct($module,$c);
		}
		
		/**
		 * Returns the login field(s) used by the users module (taking into account differences in settings for different user 'types').
		 *
		 * @return string The database column we use for login or (if it's either user_name or user_email) we return 'either'.
		 */
		function login_field() {
			// Field
			$field = NULL;
			
			// Type defined
			if($types = $this->v('types')) {
				foreach($types as $k => $v) {
					$field_type = $v[settings][login][field];
					if(!$field_type) $field_type = $this->v('settings.login.field');
					if($field_type and $field_type != $field) {
						// New field
						if(!$field) $field = $field_type;
						// Different field	
						else {
							$field = "either";
							break;
						}
					}
				}
			}
			// Module defined
			if(!$field) $field = $this->v('settings.login.field');
			// Default
			if(!$field) $field = "user_name";
			
			// Return
			return $field;
		}
		
		/**
		 * Returns the login form label for the 'user' field (taking into account the users module setting and differences in settings for different user 'types').
		 *
		 * @return string The login form 'user' field label: Username, E-mail, or Username/E-mail.
		 */
		function login_label() {
			// Field
			$field = $this->login_field();
			
			// Label
			$label = NULL;
			if($field == "either") $label = "Username/E-mail";
			else if($field == "user_email") $label = "E-mail";
			else $label = "Username";
			
			// Return
			return $label;
		}
	}
}
?>