<?php
/** Dawn - loads core functionality */
require_once dirname(__FILE__)."/../../../core/core/includes/dawn.php";
// Header
print $page->header()."
<body>";
	
// Run
$time = basename(__FILE__);
cron_run($time);

// Cache
cache_delete_expired();

// Footer
print "
</body>
".$page->footer();
/** Dusk - unloads core functionality. */
require_once dirname(__FILE__)."/../../../core/core/includes/dusk.php";
?>