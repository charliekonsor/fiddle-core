<?php
if(!class_exists('form_input_url',false)) {
	/**
	 * Creates a url input
	 */
	class form_input_url extends form_input_framework {
		/**
		 * Loads class. Needed so we can use debug_backtrace() to get this class name in PHP < 5.3 in $form_input->load().
		 */
		static function load($label = NULL,$name = NULL,$value = NULL,$c = NULL) {
			return parent::load($label,$name,$value,$c);
		}
		
		/**
		 * Construct
		 */
		function __construct($label = NULL,$name = NULL,$value = NULL,$c = NULL) {
			$this->form_input_url($label,$name,$value,$c);
		}
		function form_input_url($label = NULL,$name = NULL,$value = NULL,$c = NULL) {
			// Construct
			parent::__construct($label,$name,$value,$c);
			
			// Attributes - type - want to still use 'type' as the element's type as I don't like different browsers implementation of url inputs
			//$this->attributes[type] = 'text';
		}
	}
}
?>