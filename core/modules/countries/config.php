<?php
/************************************************************************************/
/************************************** Config **************************************/
/************************************************************************************/
$config = array(
	'name' => 'Countries',
	'single' => 'Country',
	'plural' => 'Countries',
	'icon' => 'countries',
	'db' => array(
		'table' => 'countries',
		'id' => 'country_id',
		'code' => 'country_code',
		'name' => 'country_name',
	),
	'permissions' => array(),
	'admin' => array(
		'active' => 0,
	),
	'account' => array(
		'active' => 0,
	),
	'public' => array(
		'active' => 0,
	),
	'settings' => array(),
	'install' => array(
		'db' => array(
			'rows' => 1,
		),
	),
);
?>