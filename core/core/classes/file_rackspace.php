<?php
if(!class_exists('file_rackspace',false)) {
	/**
	 * An extension of the file class with functionality specific to storing files via Rack Space Cloud Files.
	 *
	 * Note, if you update the version of OpenCloud, make sure you make the following fixes:
	 * - In $DataObject->PurgeCDN() method, change $url = $cdn . '/' . $this->name; to $url = $cdn . '/' . str_replace('%2F', '/', rawurlencode($this->name));
	 */
	class file_rackspace extends file {
		/** Your Rackspace uername. */
		public $rackspace_username = NULL;
		/** Your Rackspace API Key. */
		public $rackspace_api_key = NULL;
		/** The Rackspace 'container' you store your files in. */
		public $rackspace_container = NULL;
		/** The Rackspace container 'region' you defined when setting up the container. Usually DFW, ORD, or SYD. */
		public $rackspace_container_region = NULL;
		/** The Rackspace CDN URL for the container. You must be publishing container objects to CDN (see container's setting). You can get the URL by visiting the Cloud Files dashboard, adding a file, and viewing it (just remove file name from URL). Note: if this isn't set, we can still get the object's URL when $this->url() is called, but it'll mean an extra API call. */
		public $rackspace_container_url = NULL;
		/** Holds the OpenCloud Rackspace class 'connection' object. */
		public $rackspace_object_connection = NULL;
		/** Holds the ObjectStore cloudFiles object. */
		public $rackspace_object_objectstore = NULL;
		/** Holds the container object. */
		public $rackspace_object_container = NULL;
		
		/**
		 * Constructs the class.
		 *
		 * @param string|array $file A file (either the path to it or the $_FILES array of it) you will be interacting with with this instance of this class.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function __construct($file = NULL,$c = NULL) {
			self::file_rackspace($file,$c);
		}
		function file_rackspace($file = NULL,$c = NULL) {
			// Class
			require_once SERVER."core/core/libraries/php-opencloud/php-opencloud.php";
			
			// Credentials
			if($c[username]) file::value('rackspace_username',$c[username]);
			if(!file::value('rackspace_username')) file::value('rackspace_username',g('config.uploads.storage.rackspace.login.username'));
			if($c[api_key]) file::value('rackspace_api_key',$c[api_key]);
			if(!file::value('rackspace_api_key')) file::value('rackspace_api_key',g('config.uploads.storage.rackspace.login.api_key'));
			if($c[container]) file::value('rackspace_container',$c[container]);
			if(!file::value('rackspace_container')) file::value('rackspace_container',g('config.uploads.storage.rackspace.container'));
			if($c[container_region]) file::value('rackspace_container_region',$c[container_region]);
			if(!file::value('rackspace_container_region')) file::value('rackspace_container_region',g('config.uploads.storage.rackspace.container_region'));
			if($c[container_url]) file::value('rackspace_container_url',$c[container_url]);
			if(!file::value('rackspace_container_url')) file::value('rackspace_container_url',g('config.uploads.storage.rackspace.container_url'));
			if(file::value('rackspace_container_url') and substr(file::value('rackspace_container_url'),-1) != "/") file::value('rackspace_container_url',file::value('rackspace_container_url')."/");
			
			// Parent
			parent::__construct($file,$c);
		}
		
		/**
		 * Anaylzes current file for basic information.
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function analyze($c = NULL) {
			// Error
			if(!file::value('file')) return;
			
			// Exists // Skipping this to save time
			//if(file::call('exists')) {
				file::value('name',file::call('name'));
				file::value('path',str_replace(file::value('name'),'',file::value('file')));
				file::value('extension',file::call('extension'));
				file::value('extension_standardized',file::call('extension_standardized'));
				file::value('type',file::call('type'));
				file::value('file_changed',0);
				file::value('exists',1);
			/*}
			// Doesn't exist
			else {
				file::value('exists',0);	
			}*/
			
			// Debug
			debug("name: ".file::value('name'),file::value('c.debug'));
			debug("extension: ".file::value('extension'),file::value('c.debug'));
			debug("extension_standardized: ".file::value('extension_standardized'),file::value('c.debug'));
			debug("type: ".file::value('type'),file::value('c.debug'));
			debug("exists: ".file::value('exists'),file::value('c.debug'));
		}
		
		/**
		 * Localizes a file, making sure it has a full path and doesn't contain the domain.
		 *
		 * @param string $file The path to the file you want to localize. Defaults to the global $this->file.
		 * @return string The localized file path.
		 */
		/*static*/ function localize($file = NULL) {
			// Default
			if(!$file) $file = file::value('file');
			// Return
			return $file;
		}
		
		/**
		 * Gets the full URL of a file (opposite of 'localize').
		 *
		 * @param string $file The path to the file you want to get the URL of. Defaults to the global $this->file.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return string The URL of the file.
		 */
		/*static*/ function url($file = NULL,$c = NULL) {
			// Default
			if(!$file) $file = file::value('file');
			// Error
			if(!$file) return;
			
			// Config
			if(!x($c[https])) $c[https] = ($_SERVER['HTTPS'] == "on" ? 1 : 0);
			
			// URL
			if(file::value('rackspace_container_url')) {
				$url = file::value('rackspace_container_url').$file;
				if($c[https]) {
					$url = str_replace("http://","https://",$url);
					$url = preg_replace('/\.r[0-9]{2,3}\./','.ssl.',$url);
				}
			}
			else {
				// Container
				$container = file::call('rackspace_container');
				// Error
				if(!$container or !$file) return;
				
				// URL
				$url = ($c[https] ? $container->SSLURI() : $container->CDNURI())."/".$file;
			}
			
			// Return
			return $url;
		}
		
		/**
		 * Checks if a file (local or external) exists.					
		 * 
		 * @param string $file The file you want to check exists. Defaults to the global $this->file.
		 * @param boolean $external Not used in this child class
		 * @return boolean Whether or not the file exists.
		 */
		/*static*/ function exists($file = NULL,$external = 0) {
			// Default
			if(!$file) $file = file::value('file');
			
			// Container
			$container = file::call('rackspace_container');
			// Error
			if(!$container or !$file) return;
			
			// Exists?
			$objects = $container->ObjectList(array('prefix' => $file));
			if($objects) {
				while($object = $objects->Next()) {
					// Yes, return true
					if($object->Name() == $file) return true;
				}
			}
		}
		
		/**
		 * Returns the permission of the given file/directory (example: 0777) or, if the $permission param is passed, it sets the permission of the file/directory.		
		 * 
		 * @param string $file The file/directory we want to get (and set if 2nd param) the permission of. Defaults to the global $this->file.
		 * @param int|string $permission The permission you wan to apply to the file/directory. Default = NULL
		 * @return int The resulting permission for the file/directory.
		 */
		/*statice*/ function permission($file,$permission = NULL) {
			// Params
			if(is_int_value($file)) { // $file->permission($permission);
				$permission = $file;
				$file = NULL;
			}
			// Default
			if(!$file) $file = file::value('file');
			// Container
			$container = file::call('rackspace_container');
			// Error
			if(!$container or !$file) return;
			
			// Set permission
			if($permission) {
				#build this
			}
			
			// Get permission
			#build this
			
			// Return
			return $permission;
		}

		/**
		 * These functions aren't doable with remotely hosted files.											
		 */
		/*static*/ function orientation($file = NULL,$c = NULL) {
			return;
		}
		/*static*/ function dimensions($file = NULL,$c = NULL) {
			return;
		}
		/*static*/ function length($file = NULL,$c = NULL) {
			return;
		}
		/*static*/ function getid3($file = NULL) {
			return;
		}
		function save($destination = NULL,$c = NULL) {
			return;
		}
		/*static*/ function directory_create($directory,$recursive = 1) {	
		}
		
		/**
		 * Copies a file to another new destination (works with remote files as well).	
		 *
		 * !!! UNTESTED !!!
		 *
		 * Notes:
		 * - Both files must be stored on Rackspace.						
		 * 
		 * @param string $destination The path where you want to save the copied file, including the file's name, though we'll default to the original file's name if the path contains none.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return object The object for the copied file, if we successfully copied it (nothing is returned if there was an error).
		 */
		function copy($destination = NULL,$c = NULL) {
			// Container
			$container = file::call('rackspace_container');
			// Error
			if(!$container) return;
			
			// Return
			$return = 0;
			
			// Prepare
			$array = file::call('prepare',$destination);
			
			// Have what we need?
			if($array[source]->exists and $array[destination]->file) {
				// Debug
				debug("Copying ".$array[source]->file." to ".$array[destination]->file."",file::value('c.debug'));
				
				// Needs copying?
				if($array[source]->file != $array[destination]->file) {
					// Timeout
					ini_set('max_execution_time',1500); // 30 Minutes
					if(ini_get('safe_mode') == 0) set_time_limit(1500);
					ini_set('memory_limit','512M'); // 512 MB
					debug("time limit: ".ini_get('max_execution_time').", memory limit: ".ini_get('memory_limit'),file::value('c.debug'));
					$start = microtime_float();
					
					// Source
					$object_source = $array[source]->rackspace_object();
					
					// Destination
					$object_destination = $container->DataObject();
					$object_destination->name = $array[destination]->file;
					
					// Copy
					$object_source->Copy($object_destination);
					
					// Debug
					debug("file ".$array[source]->file." was copied to ".$array[destination]->file,file::value('c.debug'));
					debug("took ".round(microtime_float() - $start,3)." seconds",file::value('c.debug'));
				}
				
				// Exists?
				$array[destination]->analyze();
				if($array[destination]->exists) {
					// Return
					$return = 1;
					
					// Debug
					debug("Copied file and it does exist",file::value('c.debug'));
				}
			}
			
			// Return
			if($return) return $array[destination];
		}
		
		/**
		 * Deletes a file.
		 *
		 * @param string $file The path to the file you want to delete. Defaults to the global $this->file.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		/*static*/ function delete($file = NULL,$c = NULL) {
			// Params
			if(is_array($file)) { // $file->delete($c);
				$c = $file;
				$file = NULL;
			}
			// Default
			if(!$file) $file = file::value('file');
			// Error
			if(!$file) return; 
			
			// Localize
			$file = file::call('localize',$file);
			
			// Exists?
			if(file::call('exists',$file)) {
				// Object
				$object = file::call('rackspace_object',$file);
				
				// Delete
				//$object->PurgeCDN(); // Only 25 allowed per day
				$object->Delete();
				
				// Return
				return true;
			}
			
			// Return
			return false;
		}

		/**
		 * Pushes the given $source to the given $destination on the external storage service.	
		 *
		 * Notes:
		 * - If push is called via $form->process(), it'll pass an array of $c[values] which contain
		 *	 db defined values such as 'name', 'description', etc.			
		 * 
		 * @param string $source The full path to the local source file.
		 * @param string $destination The path on the external storage where you want to push this file. Default = NULL
		 * @param array $c An array of configuration values. Default = NULL
		 * @return object The object for the external file if we successfully pushed (nothing is returned if there was an error).
		 */
		function push($source,$destination = NULL,$c = NULL) {
			// Container
			$container = file::call('rackspace_container');
			// Error
			if(!$container) return;
			
			// Return
			$return = 0;
			
			// Source
			$array[source] = file::load($source);
			// Destination
			$array[destination] = file::call('prepare_destination',$source,$destination,$c);
			
			// Have what we need?
			if($array[source]->exists and $array[destination]->file) {
				// Debug
				debug("Pushing ".$array[source]->file." (local) to ".$array[destination]->storage,file::value('c.debug'));
				
				// Timeout
				ini_set('max_execution_time',1500); // 30 Minutes
				if(ini_get('safe_mode') == 0) set_time_limit(1500);
				ini_set('memory_limit','512M'); // 512 MB
				//debug("time limit: ".ini_get('max_execution_time').", memory limit: ".ini_get('memory_limit'),file::value('c.debug'));
				$start = microtime_float();
				
				// Content
				$content = NULL;
				// Content - remote file
				if(substr($array[source]->file,0,4) == "http") $content = $array[source]->get();
				// Content - local file - can just pass name
				else $content = $array[source]->file;
				
				// Push
				$object = $container->DataObject();
				$object_c = array('name' => $array[destination]->file,'content_type' => $array[destination]->contenttype());
				$object->Create($object_c,$content);
				
				// Debug
				debug("File ".$array[source]->file." was uploaded to ".$array[destination]->file,file::value('c.debug'));
				debug("Took ".round(microtime_float() - $start,3)." seconds",file::value('c.debug'));
					
				// Exists?
				$array[destination]->analyze();
				if($array[destination]->exists) {
					// Return
					$return = 1;
					
					// Debug
					debug("Pushed file and it does exist",file::value('c.debug'));
				}
			}
			
			// Return
			if($return) return $array[destination];
		}

		/**
		 * Pulls the current file from external storage servicc to the local $destination.				
		 * 
		 * @param string $destination The local path where you want to save the pulled file.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return object The object for the local file if we successfully pulled it (nothing is returned if there was an error).
		 */
		function pull($destination,$c = NULL) {
			// Error
			if(!$destination) return;
			
			#build this
		}
		
		/**
		 * Determines if the given directory exists.
		 *
		 * Since Rackspace doesn't use directories, but just names the file with slashes, there's no reason we need to actually check if it does exist.
		 * Plus, checking if the directory exists in prepare_destination() means extra calls to Rackspace.
		 *
		 * @param string $directory The path to the directory you want to check for.
		 * @return boolean Whether or not the directory exists.
		 */
		/*static*/ function directory_exists($directory) {
			return true;
		}
		
		/**
		 * Returns an array of items (files and directories) within the given directory.
		 *
		 * !!! UNTESTED !!!
		 *
		 * To do
		 * - Add ability to turn off 'recursive' (don't include if in a folder, is there a way to 'filter' when getting objects)
		 *
		 * @param string $directory The directory you want to look for items in. Default = NULL
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of files in the directory.
		 */
		/*static*/ function directory_items($directory = NULL,$c = NULL) {
			// Container
			$container = file::call('rackspace_container');
			// Error
			if(!$container) return;
			
			// Config
			if(!x($c[files])) $c[files] = 1; // Get files in the root directory.
			if(!x($c[directories])) $c[directories] = 1; // Get directories in the root directory.
			if(!x($c[recursive])) $c[recursive] = 0; // Get items from sub-directories as well as the items in the root directory.
			if(!x($c[multilevel])) $c[multilevel] = 1; // Return a 'multi-level' array when we're searching recursively. If true, child folders will be the key and their contents the value, ex: array('folder' => array('child' => array('file.txt'))). If no, it'll be a single level array such as array('folder','folder/child','folder/child/file.txt').
			if(!x($c[prefix])) $c[prefix] = NULL; // A prefix to append to each item path (used when we're looking through sub-directories in 'recursive' mode when multilevel is turned off).
			if(!$c[paths]) $c[paths] = 'relative'; // Do you want the file names returned to be 'relative' to the directory or 'full' (meaning, include the parent directory's full path)?
				
			// Variables
			$array = array();
			if($directory and substr($directory,-1) != "/") $directory .= "/"; // Make sure we have a trailing slash
			$directory_length = strlen($directory);
			
			// Items
			$objects = $container->ObjectList(array('prefix' => $directory));
			while($object = $objects->Next()) {
				// Not in desired directory, skip // Using 'prefix' instead
				//if($directory and substr($object->name,0,$directory_length) != $directory) continue;
				
				// Store
				$array[] = $object->name;
			}
			
			// Return
			return $array;
		}
		
		/**
		 * Creates (if not already created) and returns the container object.
		 *
		 * Note, caching object's globally for speed.
		 *
		 * @return object The container object.
		 */
		function rackspace_container() {
			// Already created
			if($container = file::value('rackspace_object_container')) return $container;
			
			// Credentials
			if(
				file::value('rackspace_username')
				and file::value('rackspace_api_key')
				and file::value('rackspace_container')
				and file::value('rackspace_container_region')
			) {
				// Connection
				if(!file::value('rackspace_object_connection')) {
					if(g('rackspace.object.connection')) $this->rackspace_object_connection = g('rackspace.object.connection');
					else {
						$this->rackspace_object_connection = new \OpenCloud\Rackspace(RACKSPACE_US,array('username' => file::value('rackspace_username'),'apiKey' => file::value('rackspace_api_key')));
						g('rackspace.object.connection',$this->rackspace_object_connection);
					}
				}
				if($this->rackspace_object_connection) {
					// ObjectStore
					if(!file::value('rackspace_object_objectstore')) {
						if(g('rackspace.object.objectstore')) $this->rackspace_object_objectstore = g('rackspace.object.objectstore');
						else {
							$this->rackspace_object_objectstore = $this->rackspace_object_connection->ObjectStore('cloudFiles',file::value('rackspace_container_region'));
							g('rackspace.object.objectstore',$this->rackspace_object_objectstore);
						}
					}
					if($this->rackspace_object_objectstore) {
						// Container
						if(g('rackspace.object.container')) $this->rackspace_object_container = g('rackspace.object.container');
						else {
							$this->rackspace_object_container = $this->rackspace_object_objectstore->Container(file::value('rackspace_container'));
							g('rackspace.object.container',$this->rackspace_object_container);
						}
						return $this->rackspace_object_container;
					}
				}
			}
		}
		
		/**
		 * Returns the DataObject object for the given file.
		 *
		 * @return object The file's DataObject object.
		 */
		function rackspace_object($file = NULL) {
			// Default
			if(!$file) $file = file::value('file');
			// Container
			$container = file::call('rackspace_container');
			// Error
			if(!$container or !$file) return;
		
			// Object
			$object = $container->DataObject($file);
			
			// Return
			return $object;
		}
	}
}
?>