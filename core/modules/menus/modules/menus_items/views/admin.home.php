<?php
// Child module, but missing parent id, redirect to parent home page
if($this->module_class->v('parent') and !$this->page->parents[last][id]) {
	$parent_module = module::load($this->module_class->v('parent'));
	redirect($parent_module->url('home',$this->area));
}

// Permission
if($this->permission('manage',1)) {
	// Heading
	print $this->heading();
	// Buttons
	print $this->heading_buttons();
	// Filters
	print $this->module_class->manage_filters();
	
	// Items
	$items = menu_nest($this->page->parents[last][id]);
	if($items) {
		print "
".include_javascript('jquery.ui.nested')."
".include_css('jquery.ui.nested')."
".$items;
	}
	else print "
<div class='core-none'>No ".$this->module_class->v('plural')." Found</div>";
}
?>