<?php
if(!class_exists('loader',false)) {
	/**
	 * Class for easily loading other classes.
	 *
	 * @package kraken
	 */
	class loader {
		// Variables
		var $cached;
		
		/**
		 * Loads and returns an instance of either the core or framework loader class.
		 *
		 * Example:
		 * - $loader = loader::load();
		 *
		 * @return object An instance of the loader class.
		 */
		static function load() {
			// Class names
			$class = __CLASS__;
		
			// Class name
			$class = __CLASS__;
			
			// Framework
			if(FRAMEWORK) {
				$class_framework = $class."_framework";
				if(class_exists($class_framework)) return new $class_framework();
			}
			
			// Core class
			return new $class();
		}
	}
}
?>