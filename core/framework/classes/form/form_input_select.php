<?php
if(!class_exists('form_input_select_framework',false)) {
	/**
	 * Creates a select input
	 */
	class form_input_select_framework extends form_input_select {
		/**
		 * Loads class. Needed so we can use debug_backtrace() to get this class name in PHP < 5.3 in $form_input->load().
		 */
		static function load($label = NULL,$name = NULL,$options = NULL,$value = NULL,$c = NULL) {
			$c[options] = $options;
			return parent::load($label,$name,$value,$c);
		}
		
		/**
		 * Construct
		 */
		function __construct($label = NULL,$name = NULL,$value = NULL,$c = NULL) {
			$this->form_input_select_framework($label,$name,$value,$c);
		}
		function form_input_select_framework($label = NULL,$name = NULL,$value = NULL,$c = NULL) {
			// Construct
			parent::__construct($label,$name,$value,$c);
		}
		
		/**
		 * Standardizes the given array of options.
		 *
		 * Want in array('value' => '123','label' => 'Item 123') format instead of array('123' => 'Item 123') format.
		 *
		 * In the framework, we can pass option 'classes' which load options from the database (ex: a module's items) so $array can be a string.
		 *
		 * @param array|string $options The array of options we want to standardize or an options 'class'.
		 * @return array The standardized array of options.
		 */
		function options($options) {
			// Options class?
			$class = ($options && !is_array($options) ? 1 : 0);
			
			// Parent
			$options_new = parent::options($options);
			
			// Blank option - because we can't manually add one when we're using these option 'classes'. Can overwite by passing 'options_blank' => 0
			if($class and count($options_new) and !$this->attributes[placeholder] and !x($this->c[options_blank])) {
				$this->c[options_blank] = 1;
			}
			
			// Return
			return $options_new;
		}
	}
}
?>