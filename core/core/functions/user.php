<?php
/**
 * @package kraken
 */
 
if(!function_exists('u')) {
	/**
	 * Returns the current user's value for the given key as stored in $_SESSION['u'].   
	 * 
	 * @param string $key The key you want to get the corresponding value of.
	 * @return mixed The user's corresponding value.
	 */
	function u($key) {
		// Item
		if(FRAMEWORK and $key == "item" and !$_SESSION['u']['item']) $_SESSION['u']['item'] = item::load('users',$_SESSION['u']['user_id']);
		// Return
		return array_eval('$_SESSION','u.'.$key);
	}
}

if(!function_exists('user_name_exists')) {
	/**
	 * Checks whether the given username already belongs to a user in the database (besides the $user id).
	 * 
	 * @param string $name The username we want to check for.
	 * @param int $user The id of the user who should already have this username assigned to them. So, when checking if an existing user's username already exists, we'd want to return false if only they have that username.
	 * @return boolean Whether or not the username address already belongs to a user (other than the $user user).
	 */
	function user_name_exists($name,$user = NULL) {
		$name = trim($name);
		if($name) {
			// Database
			$db = new db();
			
			// Query
			$row = $db->f("SELECT user_id FROM users WHERE user_name = '".a($name)."' AND user_id != '".$user."'");
			
			// Exists
			if($row[user_id]) return true;
			// Doesn't exist
			else return false;
		}
		else return true;
	}
}

if(!function_exists('user_email_exists')) {
	/**
	 * Checks whether the given e-mail already belongs to a user in the database (besides the $user id).
	 * 
	 * @param string $email The e-mail we want to check for.
	 * @param int $user The id of the user who should already have this e-mail assigned to them. So, when checking if an existing user's e-mail already exists, we'd want to return false if only they have that e-mail.
	 * @return boolean Whether or not the e-mail address already belongs to a user (other than the $user user).
	 */
	function user_email_exists($email,$user = NULL) {
		$email = trim($email);
		if($email) {
			// Database
			$db = new db();
			
			// Query
			$row = $db->f("SELECT user_id FROM users WHERE user_email = '".a($email)."' AND user_id != '".$user."'");
			
			// Exists
			if($row[user_id]) return true;
			// Doesn't exist
			else return false;
		}
		else return true;
	}
}
?>