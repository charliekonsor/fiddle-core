/**
 * Some default settings, in case they aren't defined.
 */
if(!CKEDITOR) var CKEDITOR;
if(!MOBILE) var MOBILE = 0;
if(!config_links_external_new) var config_links_external_new = 0;

/**
 * Functions to run when page loads											
 */
$(document).ready(function () {
	js_refresh();
});

/**
 * Refreshes jquery																
 */
function js_refresh() {
	/* Core functions */
	// Rows
	rows();
	// Overlay
	overlay();
	// Date
	date();
	// Form Validation
	validate();
	// Placholders
	placeholders();
	// Tooltips
	tooltips();
	// Notices
	notices();
	
	/* Optional functions */
	// SWFUpload
	if(typeof swfupload == "function") swfupload();
	// Color picker
	if(typeof colorpicker == "function") colorpicker();
	// Sortable
	if(typeof sortable == "function") sortable();	
	// Nestable
	if(typeof nest == "function") nest();	

	/* Refresh functions */
	// Admin
	if(typeof js_refresh_admin == "function") js_refresh_admin();
	// Framework
	if(typeof js_refresh_framework == "function") js_refresh_framework();
	// Framework admin
	if(typeof js_refresh_framework_admin == "function") js_refresh_framework_admin();
	// Local
	if(typeof js_refresh_local == "function") js_refresh_local();
	
	// External links
	if(config_links_external_new == 1) {
		$("a[href^=http]").each(function(){
			if(this.href.indexOf(document.domain) == -1) $(this).attr('target', '_blank');
		});
	}
}

/**
 * Prints given text in the footer 'debugger' div. Also adds it to the window console.							
 */
function debug(text) {
	$('#debug-javascript').append(text+'<br />');
	if(window.console) console.log(text);
}

/**
 * Adds a 'clear' button after a file input once a file is selected.
 *
 * Call on the file input by including the attribute onchange='file_clear_button(this)'.
 *
 * @param object element The file element we want to add the 'clear' button after.
 */
function file_clear_button(element) {
	$element = $(element);
	$element.siblings('a.i-clear').remove();
	$element.after('<a href="javascript:void(0);" onclick="file_clear(this);" class="i i-clear i-inline core-hover"></a>');
}

/**
 * Clears the selected file from the input. Note, file_clear_button must be called on the file input for this to work.
 *
 * @param object element The element of the 'clear button' we want to use to clear the adjacent file input.
 */
function file_clear(element) {
	var $element = $(element);
	var $parent = $element.parent();
	$element.remove();
	$parent.html($parent.html());
	
}

/**
 * Checks whethre or not the selected file already exists in the location we'll be uploading it to and, if it does,
 * it asks if they want to 'Overwrite' the old file, 'Rename' the new file, or 'Cancel' the upload all together.
 *
 * @param array c An array of configuration values. Default = NULL
 */
function file_overwrite_check(c) {
	// File
	if(!c.file && c.id) {
		c.file = $('#'+c.id).val();
	}
	
	if(c.path && c.file) {
		// Basename
		c.file = c.file.replace(/\\/g,'/').replace( /.*\//, '' );
		
		// URL
		var url = DOMAIN+c.path+c.file+"?"+new Date().getTime();
		
		// Debug
		debug('file: '+c.file+', url: '+url+', id: '+c.id);
	
		// Exists?
		$.ajax({
			url: url,
			type: 'HEAD',
			success: function(message,text,response) {
				// Debug
				debug('Content-Type: '+response.getResponseHeader('Content-Type'));
				
				// Is it actually an image? (and not just the 404 error page)
				if(c.ckeditor || response.getResponseHeader('Content-Type').indexOf('text/html') == -1) {
					//c.file = str_replace(new Array('"',"'"),new Array('&quot;','&#039;'),c.file); // Strinify doesn't seem to escape qoutes and I can't get it to work so we do random character swapping instead (see next line)
					c.file = str_replace(new Array('"',"'"),new Array('!@%^',"<>:{}"),c.file);
					debug('file (escaped): '+c.file);
	
					// HTML
					var html = "<div class='file-overwrite'>";
					html += "<div class='file-overwrite-text'>";
					html += "A file with the same name already exists.<br />";
					html += "Do you want to overwrite the existing file or rename it?<br /><br />";
					html += "</div>";
					html += "<div class='file-overwrite-buttons'>";
					html += "<a href='javascript:void(0);' onclick='"+(c.ckeditor ? "$(\"iframe.cke_dialog_ui_input_file\").attr(\"src\",\""+c.ckeditor+"&overwrite=1\");overlay_close();" : "file_overwrite_save("+JSON.stringify(c)+",1);")+"' class='file-overwrite-button file-overwrite-button-overwrite'>Overwrite</a>";
					html += "<a href='javascript:void(0);' onclick='"+(c.ckeditor ? "$(\"iframe.cke_dialog_ui_input_file\").attr(\"src\",\""+c.ckeditor+"&overwrite=0\");overlay_close();" : "file_overwrite_save("+JSON.stringify(c)+",0);")+"' class='file-overwrite-button file-overwrite-button-rename'>Rename</a>";
					if(!c.ckeditor) html += "<a href='javascript:void(0);' onclick='file_overwrite_save("+JSON.stringify(c)+",\"cancel\");' class='file-overwrite-button file-overwrite-button-cancel'>Cancel</a>";
					html += "</div>";
					debug("html: <xmp>"+html+"</xmp>");
					
					$.fancybox({
						type:html,
						content:html,
						autoSize:false,
						width:300,
						height:100,
						modal:true
					});
				}
			}
		});
	}
}

/**
 * Sends AJAX call to save how we chose to handle the file that already exists in the save location.
 *
 * @param array c An array of configuration values. Default = NULL
 * @param string overwrite What we chose to do with the file: overwrite, rename, cancel.
 */
function file_overwrite_save(c,overwrite) {
	// Save
	if(overwrite != "cancel") {
		// SWFUpload
		if(c.swfupload) {
			var swfu = $.swfupload.getInstance('#'+c.id);
			swfu.addPostParam('overwrite',overwrite);
		}
		// Default
		else {
			$.ajax({
				type: 'POST',
				url: DOMAIN,
				data: {
					ajax_action: 'file_overwrite_save',
					c: JSON.stringify(c),
					overwrite: overwrite
				},
				success: function(data) {
					debug('file_overwrite_save() result: <xmp>'+data+'</xmp>');	
				}
			});
		}
	}
	// Cancel
	else {
		// SWFUpload
		if(c.swfupload) {
			swfuploadCancel(c.id,c.swfupload_file.id,1);
		}
		// Default
		else if(c.id) {
			var $parent = $('#'+c.id).parent();
			$parent.html($parent.html());
			var $parent = $('#'+c.id).parent();
			$('.i-clear',$parent).remove();
		}
	}
	
	// Close overlay
	overlay_close();	
}

/**
 * Shows confirm box which redirects to url/calls function if user confirms text.
 */
function fw_confirm_redirect(text,url) {
	if(confirm(text)){
		location.replace(url);
	}
}

/**
 * Shows or hides given div based upon it's current state
 *
 * @param string selector The jQuery selector of the element we want to show/hide.	
 * @param array c An array of configuration values.
 */
function toggle(selector,c) {
	// Defaults
	var d = {
		animiation: "slide", // slide, fade, or null
		speed: 400, // Speed of animation (in milliseconds)
		class_open: "", // Class to add to element when it's open
		class_closed: "", // Class to add to element when it's closed
		heading: "", // ID of heading element (used to add/remove classes below)
		heading_class_open: "toggle-open", // Class to add to heading when element is open
		heading_class_closed: "toggle-closed" // Class to add to heading when element is closed
	}
	// Settings
	c = $.extend(d,c);
	// Element
	e = $(selector);
	
	// Hide
	if(e.is(':visible')) {
		if(c.animiation == "slide") e.slideUp(c.speed);
		else if(c.animiation == "fade") e.fadeOut(c.speed);
		else e.hide();
		if(x(c.class_open) || x(c.class_closed)) e.addClass(c.class_closed).removeClass(c.class_open);
		if(x(c.heading)) $("#"+c.heading).addClass(c.heading_class_closed).removeClass(c.heading_class_open);
	}
	// Show
	else {
		if(c.animiation == "slide") e.slideDown(c.speed);
		else if(c.animiation == "fade") e.fadeIn(c.speed);
		else e.show();
		if(x(c.class_open) || x(c.class_closed)) e.addClass(c.class_open).removeClass(c.class_closed);
		if(x(c.heading)) $("#"+c.heading).addClass(c.heading_class_open).removeClass(c.heading_class_closed);
	}
}

/**
 * Shows/hides sibling if given element has given value. Both original_selector (original id) and sibling_selector (sibling id) can be jQuery selectors (.class, #id) defaults to id. If require == 1 we'll add/remove required status of elments in sibling_selector	
 *
 * @param string original_selector The jQuery selector of the original element we want to get the value of.
 * @param string original_value The value of the original element needs to be to show the sibling.
 * @param string sibling_selector The jQuery selector of the item(s) we want to show/hide based upon the original item's value.
 * @param boolean required Do you want to require any sibling input which has the 'required-icon' class (meaning it's supposed to be required).
 */
function toggleSibling(original_selector,original_value,sibling_selector,require) {
	// Get Original Element
	var original_element = $(original_selector);
	var original_element_type = original_element.attr('type');
	
	// Get Value
	if(original_element_type == "checkbox" || original_element_type == "radio") var value = $(original_selector+':checked').val();
	else var value = original_element.val();
	
	// Debug
	//debug("value: "+value+", wanted value: "+original_value);
	
	// Show/Hide
	if(value == original_value || !x(value) && !x(original_value)) {
		$(sibling_selector).show();
		if(require == 1) $(sibling_selector+' .required-input').addClass('required');
	}
	else {
		$(sibling_selector).hide();
		if(require == 1) $(sibling_selector+' .required-input').removeClass('required');
	}
}

/**
 * Changes visible content for clicked tab in a set of tabs.	
 *
 * @param string tabClass A class which each tab in this set of tabs has.	
 * @param string tabSelector A jQuery selector for the specific tab we just clicked (or want to show the content of).
 * @param string contentClass A class which all content elements for each tab have.
 * @param string contentSelector A jQuery selector for the specific content element we want to show.		
 */
function tab(tabClass,tabSelector,contentClass,contentSelector) {
	$("."+tabClass).removeClass('tab-selected');
	if(tabSelector) $(tabSelector).addClass('tab-selected');
	if(contentClass) $("."+contentClass).hide();
	if(contentSelector) $(contentSelector).show();
}

/**
 * Gets values from a form (or any element), posts to AJAX, and handles response.
 *
 * @param string form The ID of the form you want to submit and save (without the jQuery # so, "my_form" instead of "#my_form").
 * @param array c An array of configuration values.
 */
var saveStatus = {};
function save(form,c) {
	// Config
	if(!c) c = {};
	
	// Status
	if(saveStatus[form] == 1) {
		// Already saving
		//debug('already processing form '+form+', wait half a second..<br />');
		setTimeout(function(){save(form,c)},500);
	}
	else {
		// Store 'saving' status
		//debug('saving form '+form+'<br />');
		saveStatus[form] = 1;
		
		$form = $('#'+form);
		
		// Defaults
		var d = {
			method: 'POST', // Form submission method: POST [default], GET
			url: DOMAIN,
			disable: (c.auto == 1 ? 0 : 1), // Disable submit
			loading: 0, // Show loading image over form while saving
			submit_id: null, // Can pass id of the 'submit' button element we want to disable (defaults to input[type=submit] elements inside form)
			on_submit: function(form,c) {debug('default on_submit call');}, // A function to run after the form has been submitted
			on_success: function(result,form,c) {debug('default on_success call');}, // A function to run after the save has been completed
			//on_success_overwrite: 0, // Does the 'on_success' function overwrite the normal save success processing or just run before default on_success script
			debug: 0 // Debug
		}
		// Settings
		c = $.extend(d,c);
		
		// Disable Submit
		if(c.disable) form_submit('#'+form,{submit_form:0,submit_id:c.submit_id});
		
		// CKEditor (Make sure content is intextarea)
		if(CKEDITOR) {
			for(instance in CKEDITOR.instances) CKEDITOR.instances[instance].updateElement();
		}
		
		// Data - form
		var data = 'form='+form;
		
		// Data = values
		$(':input',$form).each(function() {
			if(this.type == "radio" || this.type == "checkbox") {
				if(this.checked == true) data += '&' + this.name + '=' + encodeURIComponent(this.value);
			}
			else if(this.type == "select-multiple") {
				var name = this.name;
				$("option:selected",this).each(function(i,selected) {
					data += '&' + name + '=' + encodeURIComponent($(selected).val())
				});
			}
			else data += '&' + this.name + '=' + encodeURIComponent(this.value);
		});
		//debug('data: '+data);
		
		// Loader
		if(c.loading == 1) loader(form);
		
		// on_submit
		if(c.on_submit) {
			c.on_submit(form,c);
		}
		
		// Data - URL
		if(c.method == 'GET') {
			var url = DOMAIN+'?'+data;
			var data = '';
		}
		if(c.method == 'POST') {
			var url = DOMAIN;
			var data = data;
		}
		// AJAX
		$.ajax({
			type: c.method,
			url: url,
			data: data,
			c: c,
			success: function(result){
				// Done saving
				saveStatus[form] = 0;
				
				// Enalbe submit button
				form_submit_complete($form);
				
				// Config
				var c = this.c;
				
				// Debug
				if(c.debug) debug('save() result: '+result);
				
				// on_success
				if(c.on_success) {
					var success_return = c.on_success(result,form,c);
				}
					
				// Refresh jQuery
				js_refresh();
			},
			error: function(data, status, e){
				// Done saving
				saveStatus[form] = 0;
			}
		});
	}
}

/**
 * Submits form and disables submit button.
 * 
 * @param object f The form you're submitting.
 * @param object c An array of configuration values.									
 */
function form_submit(f,c) {
	// Defaults
	var d = {
		submit_form: 1, // Submit the form
		submit_id: null, // ID of submit element we're disabling, defaults to all input[type=submit] elements in form
		buttons: 0, // Disable buttons (as well as 'submit' items)
		image: DOMAIN+"core/core/images/ajax-loader-sm.gif" // The URL of a 'processing' image to display after the submit button 
	}
	// Settings
	c = $.extend(d,c);
	
	// Button(s)
	if(c.submit_id) var s = $('#'+c.submit_id);
	else var s = $(f).find("input[type='submit']:not(.submitting)"+(c.buttons == 1 ? ", input[type='button']:not(.submitting)" : ""));
	
	// Image
	if(c.image) {
		$(s).addClass('submitting').after("<img src='"+c.image+"' class='loader' />");
		$('img.loader').fadeTo(100,1,function() {
			// Submit form
			if(c.submit_form == 1) f.submit();
			// Disable button
			$(s).attr("disabled", "disabled").fadeTo("normal", 0.4);
		});
	}
	// No image
	else {
		// Submit form
		if(c.submit_form == 1) f.submit();
		// Disable button
		$(s).attr("disabled", "disabled").fadeTo("normal", 0.4);
	}
}

/**
 * Preloads image used in form submission.
 * 
 * @param array c An array of configuration values. Default = NULL 
 */
function form_submit_preload(c) {
	// Defaults
	var d = {
		image: DOMAIN+"core/core/images/ajax-loader-sm.gif" // The URL of a 'processing' image to display after the submit button 
	}
	// Settings
	c = $.extend(d,c);
	// Image
	$('body').append("<img src='"+c.image+"' style='display:none;' />");
}

/**
 * Removes 'submitting' status from a form submit button.	
 *
 * @param object f The form that's you want to remove the 'submitting' status from.							
 */
function form_submit_complete(f) {
	if(typeof f != "object") f = $(f);
	$(".submitting",f).removeClass('submitting').fadeTo(200,1).removeAttr('disabled').next("img.loader").remove();
}

/**
 * Determines if given string has a value
 *
 * @param mixed value The string we're checking against.
 * @return boolean Whether or not the given string has a value.
 */
function x(string) {
	if(string != "" && string != "undefined" && string != undefined && string || string === 0) return true;
	else return false;
}

/**
 * Gets or (if it doesn't yet exists) creates the id of the given jQuery element.
 */
function element_id(element,prefix) {
	if(!x(prefix)) prefix = "";
	// Get ID
	var id = element.attr('id');
	// No ID, create one
	if(!id) {
		id = prefix+Math.round(Math.random() * 99999);
		element.attr('id',id);
	}
	// Return
	return id;
}

/**
 * Shows loading icon with given text (optional) in given element selector.					
 */
function loader(selector,c) {
	// Defaults
	var d = {
		text: "Loading..",
		image: 1,
		image_url: DOMAIN+"core/core/images/ajax-loader.gif",
		fade: 0,
		fade_speed: 500,
		fade_opacity: .3,
		position: 'over' // over, center, left, right, replace
	}
	// Settings
	c = $.extend(d,c);
	
	// HTML
	var html = $(selector).html();
	
	// Over
	if(c.position == "over") {
		/*** New - Place selectors over content ***/
		var h = $(selector).height();
		if(h <= 5) c.position = "left";
		else {
			var w = $(selector).width();
			// Loader
			$(selector).prepend("<div class='loader'><div class='loader-image'>"+(c.image == 1 ? "<img src='"+c.image_url+"' alt='..' /> " : "")+c.text+"</div><div class='loader-div"+(c.fade == 1 ? " loader-fade" : "")+"' style='width:"+w+"px;height:"+h+"px;'></div></div>");
			// Fade In
			if(c.fade == 1) $(selector+' div.loader-fade').fadeTo(c.fade_speed,(1 - (1 * c.fade_opacity)));
		}
	}
	// Replace
	if(c.position == "replace") $(selector).html("<div class='loader'>"+(c.image == 1 ? "<img src='"+c.image_url+"' alt='..' class='loader-image' /> " : "")+c.text+"</div>");
	// Left / Right / Center
	if(c.position == "left" || c.position == "right" || c.position == "center") $(selector).html("<div style='text-align:"+c.position+";' class='loader'>"+(c.image == 1 ? "<img src='"+c.image_url+"' alt='..' class='loader-image' /> " : "")+c.text+"</div>");
}

/**
 * Checks or unchecks all checkboxes (with optional class name).
 * 
 * @param object checkbox The 'this' of the checkbox you want to check.	
 * @param string class_name A classname all checkboxes must have to be effect by the check/uncheck all functionality.			
 */
function checkAll(checkbox,class_name) {
	if(class_name) {
		if(checkbox.checked) $('input[type=checkbox].'+class_name).attr('checked', 'checked');
		else $('input[type=checkbox].'+class_name).removeAttr('checked');
	}
	else {
		if(checkbox.checked) $('input[type=checkbox]').attr('checked', 'checked');
		else $('input[type=checkbox]').removeAttr('checked');
	}
}

/**
 * Adds alternate shading to odd and even rows where class="core-row" or the parent class="core-rows".	
 */
function rows() {
	// Individual rows
	$('tbody tr.core-row:odd, div.core-row:odd, table.core-row:odd, td.core-row:odd, li.core-row:odd').addClass("odd").removeClass("even");
	$('tbody tr.core-row:even, div.core-row:even, table.core-row:even, td.core-row:even, li.core-row:even').addClass("even").removeClass("odd");
	
	// Child rows
	if($.browser.msie) {
		$('table.core-rows tbody tr:odd, div.core-rows div:odd, ul.core-rows li:odd').addClass("odd").removeClass("even");
		$('table.core-rows tbody tr:even, div.core-rows div:even, ul.core-rows li:even').addClass("even").removeClass("odd");
	}
}

/**
 * Applies tooltip functionality to all elements with class='tooltip'
 */
function tooltips() {
	if(!MOBILE) {
		// jQuery Tools
		/*if($().tooltip) {
			$('.tooltip').each(function() {
				// Default settings
				var settings_default = {
					tipClass: "tooltip-tip",
					delay: 100,
					effect: "fade",
					offset: [-3, -8],
					position: "top right",
					fadeInSpeed: 250,
					fadeOutSpeed: 100
				};
				
				// Settings
				var settings = $.extend(settings_default,$(this).metadata());
				
				// Apply
				$(this)
					.tooltip(settings)
					.dynamic();
			});
		}*/
		
		// Simple Tooltip - http://cssglobe.com/post/1695/easiest-tooltip-and-image-preview-using-jquery
		if($().tooltip) {
			// Elements
			$('.tooltip').each(function() {
				// Settings
				var settings = $(this).metadata();
				
				// Apply
				$(this).tooltip(settings);
			});
		}
	}
}

/**
 * Clears or restores default value for input field where there's a placeholder attribute.				
 */
function placeholders() {
	// Remove empty placeholder attributes
	$('input[placeholder], textarea[placeholder]').each(function() {
		var placeholder = $(this).attr('placeholder');
		if(!placeholder) $(this).removeAttr('placeholder');
	});
	
	// Classes
	$('input[placeholder], textarea[placeholder]')
		.addClass('placeholder') // Add placeholder class to elements with placeholder attribute, but no class
		.addClass('placeholder-element'); // We'll use this to determine which elements use placeholders (if there's a real value the element won't have class='placeholder')
	
	// Placeholder/value attribute checks
	$('input.placeholder-element, textarea.placeholder-element').each(function() {
		var placeholder = $(this).attr('placeholder');
		var value = $(this).val();
		
		// Value is same as placeholder, empty value
		/*if(placeholder == value) $(this).val(""); // If we added a 'placeholder' attribute and a value, we want to keep the value, even if it's the same as the value
		// Value is different than placeholder, remove placeholder class
		else*/ if(placeholder != value && value) $(this).removeClass('placeholder');
	});
	
	// HTML 5 Support?
	var html5 = support_placeholder();
	
	// No, use javascript
	if(!html5) {
		// No value, set to placeholder
		$('input.placeholder-element, textarea.placeholder-element').each(function() {
			if(!$(this).val()) $(this).val($(this).attr('placeholder'));
		});
		
		// Password Inputs (needs special code)
		$('input.placeholder-element[type=password]').each(function() {
			var id = $(this).attr('id');
			if(!id) {
				id = Math.round(Math.random()) * 9999;
				$(this).attr('id',id);
			}
			//var id = element_id($(this)); // IE has trouble with this
			
			// Clone original
			if(!$.browser.msie) {
				var input = $(this)
					.clone()
					.attr('type','text')
					.attr('name','')
					.attr('id',id+'_placeholder')
					.val($(this).attr('placeholder'))
					.removeClass('required')
					.addClass('placeholder')
					.addClass('placeholder_password');
			}
			else { // IE can't change 'type' so we have to re-create it all together.
				var input = "<input type='text' id='"+id+"_placeholder' value='"+$(this).attr('placeholder')+"' class='"+$(this).attr('class')+" placeholder placeholder_password' />";
			}
			
			// Add clone to DOM
			$(this).before(input);
			
			// IE - remove unwanted classes (couldn't do until added to the DOM)
			if($.browser.msie) $('#'+id+'_placeholder').removeClass('required');
				
			// Blur original (show clone if no value)
			$(this)
				.blur(function() { // Add onblur so we can hide this again if empty
					var id = $(this).attr('id');
					var val = $(this).val();
					if(!val) {
						$(this).hide();
						$('#'+id+'_placeholder').show();			
					}
				});
			// Focus on clone (switch to original)
			$('#'+id+'_placeholder').focus(function() {
				// Hide clone 
				$(this).hide();
				// Show original and focus
				var id = str_replace('_placeholder','',$(this).attr('id'));
				$('#'+id).show().focus();
			});
			
			// Hide original (original has no real value)
			if(!$(this).val() || $(this).val() == $(this).attr('placeholder')) {
				$(this)
					.val("") // Remove password field's value
					.hide() // Hide password field
					.removeClass('placeholder') // Remove placeholder class (don't want gray coloring)
			}
			// Hide placeholder (original has real value)
			else {
				$('#'+id+'_placeholder').hide();
			}
		});
			
		// Text Inputs (or password inputs on IE as it has trouble with above code)
		$('input.placeholder-element:not([type=password], .placeholder_password), textarea.placeholder-element').each(function() {
			// Events
			$(this)
				// Focus, clear placeholder value and styling (if no real value)
				.focus(function() {
					if($(this).attr('placeholder') == $(this).val()) $(this).val('').removeClass('placeholder');
				})
				// Blur, restore placeholder text and styling (if no real value)
				.blur(function() {
					if(!$(this).val()) $(this).val($(this).attr('placeholder')).addClass('placeholder');
				});
				
			// Form submit
			var form = $(this).closest('form');
			if(form) {
				// Empty all placeholder values (if class='placeholder', we didn't enter own value)
				$(form).submit(function() {
					$('input.placeholder, textarea.placeholder',form).val('');
				});
			}
		});
	}
	// Yes, let browser handle styling
	else {
		$('input.placeholder-element, textarea.placeholder-element').removeClass('placeholder');
	}
}

/**
 * Returns whether or not browser supports HTML 5 placeholder					
 */
function support_placeholder() {
	var i = document.createElement('input');
	return 'placeholder' in i;
}

/**
 * Removes notices from DOM (that aren't sticky).								
 */
function notices() {
	// Close button
	$('div.notice-close').click(function() {
		$(this).closest('div.notice').remove();
	});
	
	// Flash
	$('div.notice').css({opacity:0.2}).animate({opacity: 1.0}, 300, function() {
	// Slide down
	//$('div.notice').slideDown(300, function() {
		// Hide
		$('div.notice:not(.notice-sticky)').animate({opacity: 1.0}, 5000).slideUp(650,function() { $(this).remove(); });
	});
}

/**
 * Applies overlay window functionality to links with class='overlay'								
 */
function overlay() {
	// Fancyapps - Fancybox
	var items = $(".overlay:not(.overlay-added)");
	if($().fancybox) {
		items.each(function() {
			// Defaults
			var d = {
				group_selector: '.overlay', // Custom - needed to add this so galleries work properly since we're applying to individual elements instead of a selector.
				loop: true,
				afterShow: function() { js_refresh(); },
				nextEffect: 'fade',
				prevEffect: 'fade',
				helpers: {
					overlay: {
						opacity: .75
					},
					title: {
						type: 'inside'
					}
				}
			}
			
			// URL
			var url = this.href || this.alt;
			if(url) {
				// Image
				if(
					url.match(/\.(jpg|gif|png|bmp|jpeg)$/i) // Example: http://www.domain.com/image.jpg
					|| url.match(/\.(jpg|gif|png|bmp|jpeg)\?(.*)$/i) // Example: http://www.domain.com/image.jpg?t=123
				) {
					d.type = "image";
				}
				// SWF
				else if(url.match(/\.swf$|\.swf\?|^http:\/\/www\.youtube\.com\/v\/|^http:\/\/www\.youtube\.com\/embed\//)) {
					d.type = "swf";
				}
				// Default
				else {
					d.width = 670;
					d.height = 400;
					d.autoSize = false;
					d.helpers.title.position = 'top';
					
					// Type
					if(this.href.indexOf(document.domain) == -1) d.type = "iframe"; // External
					else d.type = "ajax"; // Local
				}
				
				// Title (in case we aslo apply tooltips which removes the title attribute). Must also check that it's not a gallery too (rel) otherwise they'd all have the same title.
				if(this.title && !this.rel) d.title = this.title;
				
				// Config
				var c = $.extend(true,d,$(this).metadata());
				
				// Apply
				$(this).addClass('overlay-added').fancybox(c);
			}
		});
	}
}

/**
 * Closes any open overlay window(s).					
 */
function overlay_close() {
	// Fancybox
	if($().fancybox) $.fancybox.close();
}

/**
 * Shows calendar date picker on all input fields with class="date"			
 */
function date() {
	// jQuery Tools - Date Input
	/*if($().dateinput) {
		$('input.date:not(.date-applied)').each(function() {
			// Default settings
			var settings_default = {
				selectors: true,
				format: 'mm/dd/yyyy',
				yearRange: [-100,100],
				onHide: function(e,date) {
					// Blur from input so we don't think we can edit it (especially on iPad where keyboard pops up)
					$('input.date').blur();
				}
			};
			
			// Settings
			var settings = $(this).metadata();
			var settings = $.extend(settings_default,$(this).metadata());
			
			// Value
			if(!settings.value) {
				var value = $(this).val();
				if(value) {
					var value_parts = value.split('/');
					settings.value = value_parts[2]+'-'+value_parts[0]+'-'+value_parts[1];
				}
			}
			
			// Apply
			$(this)
				.dateinput(settings)
				.addClass('date-applied')
				.attr('autocomplete','off')
				/*.focus(function() { // Blur from input so we don't think we can edit it (especially on iPad where keyboard pops up) // Removed as it conflicts with jQuery Tools natural 'clear' functionality when pressing delete.
					$(this).blur();
				})*//*;
		});
	}
	// jQuery UI - Datepicker
	else*/ if($().datepicker) {
		$('input.date:not(.date-applied)').each(function() {
			// Default settings
			var settings_default = {
				yearRange: "-100:+100",
				changeMonth: true,
				changeYear: true/*,
				onHide: function(e,date) {
					// Blur from input so we don't think we can edit it (especially on iPad where keyboard pops up)
					$('input.date').blur();
				}*/
			};
			
			// Settings
			var settings = $(this).metadata();
			var settings = $.extend(settings_default,$(this).metadata());
			
			// Apply
			$(this)
				.datepicker(settings)
				.addClass('date-applied')
				.attr('autocomplete','off');
		});
	}
}

/**
 * Adds form validation to all forms with class="require"						
 */
function validate() {
	// Required icon
	//$('input.required-icon:not(.required-icon-added, input[type=hidden], input[type=checkbox], input[type=radio]), select.required-icon:not(.required-icon-added), textarea.required-icon:not(.required-icon-added)').after('<span class="required-icon"><img src="'+DOMAIN+'core/core/images/required.gif" width="10" height="10" alt="Required" /></span>').addClass('norequired');
	
	// Validation
	if($().validate) {
		// Form Validation
		$('form.require').each(function() {
			form_submit_preload();
			$(this)
				.submit(function(event) {
					// CKEditor (Make sure content is intextarea)
					if(CKEDITOR) {
						for(instance in CKEDITOR.instances) CKEDITOR.instances[instance].updateElement();
					}
				})
				.validate({
					submitHandler: function(form) { form_submit(form); }
				});
		});
	}
}

/**
 * jQuery extension to get an element's outerHTML.
 *
 * http://stackoverflow.com/questions/2419749/get-selected-elements-outer-html					
 */
(function($) {
  $.fn.outerHTML = function() {
    return $(this).clone().wrap('<div></div>').parent().html();
  }
})(jQuery);

/**
 * jQuery extension to bine an event 'before' all other events, so it's fired first.
 *
 * http://stackoverflow.com/a/2641047/502311
 *
 * @param string name The name of the event "click", "mouseover", etc.
 * @param function fn The function you want to bind to the event
 */
(function($) {
	$.fn.bindFirst = function(name, fn) {
		// bind as you normally would
		// don't want to miss out on any jQuery magic
		this.bind(name, fn);
		// Thanks to a comment by @Martin, adding support for
		// namespaced events too.
		var handlers = this.data('events')[name.split('.')[0]];
		// take out the handler we just inserted from the end
		var handler = handlers.pop();
		// move it at the beginning
		handlers.splice(0, 0, handler);
	};
})(jQuery);

/**
 * A Javascript version of the PHP function number_format()
 */
function number_format(number,decimals,dec_point,thousands_sep) {
	var n = number, c = isNaN(decimals = Math.abs(decimals)) ? 2 : decimals;
	var d = dec_point == undefined ? "." : dec_point;
	var t = thousands_sep == undefined ? "," : thousands_sep, s = n < 0 ? "-" : "";
	var i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
	return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
}

/**
 * A Javascript version of the PHP function str_replace()
 */
function str_replace(search, replace, subject) {
	var f = search, r = replace, s = subject;
	var ra = is_array(r), sa = is_array(s), f = [].concat(f), r = [].concat(r), i = (s = [].concat(s)).length;
	while (j = 0, i--) {
		while (s[i] = s[i].split(f[j]).join(ra ? r[j] || "" : r[0]), ++j in f){};
	};
	return sa ? s : s[0];
}

/**
 * A Javascript version of the PHP function is_array()
 */
function is_array( mixed_var ) {
	return ( mixed_var instanceof Array );
}