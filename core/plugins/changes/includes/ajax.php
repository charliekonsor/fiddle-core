<?php
/**
 * Displays detailed information about the given change to an item										
 */
if($_GET['ajax_action'] == "change") {
	$row = $core->db->f("SELECT * FROM items_changes WHERE module_code = '".$_GET['module']."' AND item_id = '".$_GET['id']."' AND id = '".$_GET['change']."'");
	if($row[id]) {
		// Module
		$m = module::load($row[module_code]);
		// Changes
		$changes = data_unserialize($row[change]);
		foreach($changes as $column) {
			$field = $m->column_form($column);
			$label = (is_array($field[label]) ? $field[label][value] : $field[label]);
			if(!$label) {
				foreach(m($row[module_code].'.db') as $k => $v) {
					if($v == $column) $label = g('framework.db.'.$k.'.label');	
				}
			}
			$changes_string .= "<li>".($label ? $label." (".$column.")" : $column)."</li>";
		}
		
		print "
<table class='table'>
	<tr>	
		<td width='100'>Date</td>
		<td>".adodb_date('F j, Y g:i:s A T',$row[change_created])."</td>
	</tr>";
		if($row[user_id]) {
			$user = item::load('users',$row[user_id]);
			print "
	<tr>
		<td>User</td>
		<td><a href='".$user->url($_GET['area'])."'>".$user->name."</a></td>
	</tr>";
		}
		print "
	<tr>
		<td>IP Address</td>
		<td>".$row[user_ip]."</td>
	</tr>
	<tr>
		<td>URL</td>
		<td><a href='".$row[change_url]."'>".$row[change_url]."</a></td>
	</tr>
	<tr>
		<td valign='top'>Changes</td>
		<td valign='top'>
			<ul>
				".$changes_string."
			</ul>
		</td>
	</tr>
</table>";
	}
	else print "
<div class='core-none'>We have no record of this change.</div>";
	
	// Exit
	exit;
}
?>