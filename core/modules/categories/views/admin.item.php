<?php
// No parent module, redirect to area home page
if(!$this->page->parents[last][module]) redirect(DOMAIN.$this->area);

// Permission
if($this->permission('edit',1)) {
	// Heading
	print $this->heading();
	// Buttons
	print $this->heading_buttons();
	
	// Form
	$form = $this->item_class->form(array('type' => $this->type,'redirect' => urldecode($_GET['redirect']))); // Form
	$form->hidden('module_code',$this->page->parents[last][module]); // Module
	if(m($this->page->parents[last][module].'.types')) { // Type
		if($this->item_class->v('type_code')) $form->hidden('type_code',$this->item_class->v('type_code'));
		else if($this->page->parents[last][type]) $form->hidden('type_code',$this->page->parents[last][type]);
		else {
			$input = array(
				'label' => "Type <span class='core-tiny'>(optional)</span>",
				'name' => 'type_code',
				'type' => 'select',
				'options' => 'types',
				'options_module' => $this->page->parents[last][module],
				'placeholder' => ' '
			);
			$form->inputs = array_merge(array($input),$form->inputs);
		}
	}
	print $form->render(); // Render
}
?>