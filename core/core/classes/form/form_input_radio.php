<?php
if(!class_exists('form_input_radio',false)) {
	/**
	 * Creates a radio input
	 */
	class form_input_radio extends form_input_framework {
		/** Holds the array of options for this series of radio button(s). */
		public $options = NULL; // Set to NULL (not array()) so we don't try to merge an empty array with an options class (string).
		/** Holds the standardized array of options. */
		public $options_standardized = NULL;
		
		/**
		 * Loads class. Needed so we can use debug_backtrace() to get this class name in PHP < 5.3 in $form_input->load().
		 */
		static function load($label = NULL,$name = NULL,$options = NULL,$value = NULL,$c = NULL) {
			$c[options] = $options;
			return parent::load($label,$name,$value,$c);
		}
		
		/**
		 * Construct
		 */
		function __construct($label = NULL,$name = NULL,$value = NULL,$c = NULL) {
			$this->form_input_radio($label,$name,$value,$c);
		}
		function form_input_radio($label = NULL,$name = NULL,$value = NULL,$c = NULL) {
			// Construct
			parent::__construct($label,$name,$value,$c);
			
			// Options
			$this->options_standardized = $this->options($this->options);
			
			// Config
			if(!x($this->c[separator])) $this->c[separator] = "<br />";
			if(!x($this->c[error_label])) $this->c[error_label] = 1; // Include error label at end of checkboxes (set to 0 if you're adding the error label on your own
		}
		
		/**
		 * Renders the HTML for the input's 'label'.
		 *
		 * @param object $form The form object this input is a part of.
		 * @param array $c An array of config values. Default = NULL
		 * @return string The HTML of the input's label.
		 */
		function html_label($form,$c = NULL) {
			// HTML
			$html = $this->label[value];
			
			// Help
			if(/*count($this->options_standardized) > 1 and */$this->help) {
				$html .= help($this->help);
				$this->help = NULL;
			}
			
			// Return
			return $html;
		}
		
		/**
		 * Renders the HTML for the actual input.
		 *
		 * @param object $form The form object this input is a part of.
		 * @param array $c An array of config values. Default = NULL
		 * @return string The HTML of the input.
		 */
		function html_element($form,$c = NULL) {
			// Options count
			$c[count] = count($this->options_standardized);
			if($c[count]) {
				// Value - should mabye be in the 'Multiple' if statement above, but sometimes we serialize with just 1 option
				if($this->value) {
					$value = data_unserialize($this->value);
					if(is_array($value)) $this->value = $value;
				}
				
				// ID
				if($this->attributes[id]) $c[id] = $this->attributes[id];
				else $c[id] = "form-radio-".str_replace(array('[',']'),array('-',''),$this->name)."-";
				unset($this->attributes[id]);
				
				// Attributes
				$c[attributes] = $this->attributes($form);
			
				// HTML
				$html = $this->options_html($form,$this->options_standardized,$this->value,$c);
				// Required
				if($html and $this->c[error_label]) {
					if($this->validate[required]) $html .= "<label class='error form-error' for='".$this->name."'".($this->c[errors_inline] && $this->name && $this->errors[inline][$this->name] ? " style='display:inline;'>".$this->errors[inline][$this->name] : ">This field is required.")."</label>";	
				}
			}
			// No options, remove 'required'
			else if($this->name and $this->validate[required]) {
				$this->validate[required] = 0;
				$form->input_replace($this->name,$this);
			}
			
			// Return
			return $html;
		}
		
		/**
		 * Returns a string of attributes for an element.
		 *
		 * This really just acts as an interpreter between the new method (inputs as objects) and the old method (input as arrays).
		 *
		 * @param object $form The form object this input is a part of.
		 * @return string A string of HTML attributes for the element.
		 */
		function attributes($form) {
			// Error
			if(!$form) return;
			
			// Attributes - defined
			$input = $this->attributes;
			// Attributes - variables (if not explicity defined already)
			if(!$input[type]) $input[type] = $this->type;
			if(!$input[name]) $input[name] = $this->name;
			//if(!$input[value]) $input[value] = $this->value; // Don't have 'value' as an attribute
			// Attributes - validate (so it works with old method where input was an array)
			$input[validate] = $this->validate;
			
			// Parent
			$string = $form->attributes($input,$c);
			
			// Return
			return $string;
		}
		
		/**
		 * Returns a string of radio button inputs for the given array of option(s).
		 *
		 * @param object $form The form object this input is a part of.
		 * @param array $options The array of options to create inputs out of.
		 * @param string|array $value The current selected value. Default = NULL
		 * @param array $c An array of config values. Default = NULL
		 * @return string A string of HTML <input> elements.
		 */
		function options_html($form,$options,$value = NULL,$c = NULL) {
			// Config
			if(!$c[separator]) $c[separator] = $this->c[separator]; // What string do you want to use when separting the radio buttons
			if(!$c[indent]) $c[indent] = 0; // Number of levels to indent this option.
			if(!$c[id]) $c[id] = "form-radio-".mt_rand(); // ID to use for option (if no input id is defined and no option specific id is defined)
			
			// Indent
			for($x = 0;$x < $c[indent];$x++) $indent .= "&nbsp;&nbsp;&nbsp;&nbsp;";
			
			// Value
			if(!is_array($value)) $value = (string) $value;
			
			// Options
			$x = 0;
			foreach($options as $option) {
				// Value
				$option[value] = (string) $option[value];
				// Checked
				$checked = (x($value) && ($value === $option[value] || is_array($value) && in_array($option[value],$value)) ? 1 : 0);
				// Attributes
				$option_attributes = $c[attributes];
				// ID
				$option_id = NULL;
				if($option[id]) $option_id = $option[id];
				else if($c[count] == 1) $option_id = $c[id];
				if(!$option_id) $option_id = $c[id].$c[indent]."-".$x; // More than one option or no $this->attributes[id] was present, create on own
				$option_attributes .= ($option_attributes ? " " : "")."id='".$option_id."'";
				
				// Input
				$options_html .= $indent."<input value='".$option[value]."'".($checked ? " checked='checked'" : "").($option_attributes ? " ".$option_attributes : "")." /> <label for='".$option_id."'>".$option[label].$c[separator]."</label>";
				
				// Children
				if($option[options]) {
					$_c = $c;
					$_c[indent] += 1;
					$_c[id] = $option_id;
					$options_html .= $this->options_html($form,$option[options],$value,$_c);	
				}
				
				// Counter
				$x++;
			}
			
			// Return
			return $options_html;
		}
	}
}
?>