<?php
/**
 * @package kraken\xls
 */
 
if(!function_exists('xls2array')) {
	/**
	 * Converts given xls file to an array of data in that xls file.
	 * 
	 * @param string $file The path to the file you want to get the data from.
	 * @param array $c An array of configuration values. Default = NULL
	 * @return array The resulting array of data.
	 */
	function xls2array($file,$c = NULL) {
		// Error
		if(!$file) return;
		
		// Config
		if(!$c[method]) $c[method] = 'phpexcel'; // The method we want to use to parse the data: simple, phpexcelreader (requires you to upload phpExcelReader class to /core/core/libraries/), phpexcel (thorough, but resource heavy and requires you to upload phpexcel to /core/core/libraries/)
		if(!x($c[debug])) $c[debug] = 0; // Debug
		$c[method] = strtolower($c[method]); // Make sure it's lowercase
		
		// phpexcel - https://github.com/PHPOffice/PHPExcel
		if($c[method] == "phpexcel") {
			// File
			$class_file = SERVER."core/core/libraries/phpexcel/Classes/PHPExcel/IOFactory.php";
			if(file_exists($class_file)) {
				// Class
				set_include_path(SERVER.'core/core/libraries/phpexcel/Classes/');
				require_once $class_file;
				
				// Get XLS file type/version
				$type = PHPExcel_IOFactory::identify($file);
				
				// Reader
				$objReader = PHPExcel_IOFactory::createReader($type);
				// Only load data (not cell styles)
				if(method_exists($objReader,'setReadDataOnly')) $objReader->setReadDataOnly(true);
				// Load file
				$objPHPExcel = $objReader->load($file);
				
				// Array
				$array = $objPHPExcel->getActiveSheet()->toArray(NULL,false,false,false); // What to return if cell has no data, calculate formulas, format data, use actual row/column names for row/column keys (as opposed to juse zero based int keys)
				
				// Parse
				/*$objPHPExcel = PHPExcel_IOFactory::load($file);
				
				// Array
				$array = $objPHPExcel->getActiveSheet()->toArray(NULL,true,true,true);*/
			}
			else $c[method] = "php-excel-reader";
		}
		
		// php-excel-reader - http://code.google.com/p/php-excel-reader/
		if($c[method] == "php-excel-reader") {
			// File
			$class_file = SERVER."core/core/libraries/php-excel-reader/reader.php";
			if(file_exists($class_file)) {
				// Class
				require_once $class_file;
				
				// Parse
				$data = new Spreadsheet_Excel_Reader($file,false); // false makes it faster, means we don't store style info for cells
				debug("data: ".return_array($data),$c[debug]);
				debug("dump: ".$data->dump(true,true),$c[debug]);
				
				// Array
				$sheet = 0;
				for($row = 1;$row <= $data->rowcount($sheet);$row++) {
					for($col = 1;$col <= $data->colcount($sheet);$col++) {
						$array[$row][$col] = $data->val($row,$col,$sheet);
					}
				}
			}
			else $c[method] = "phpexcelreader";
		}
		
		// phpExcelReader - http://sourceforge.net/projects/phpexcelreader/ // Never works for me
		if($c[method] == "phpexcelreader") {
			// File
			$class_file = SERVER."core/core/libraries/phpExcelReader/Excel/reader.php";
			if(file_exists($class_file)) {
				// Class
				require_once $class_file;
				
				// Parse
				$data = new Spreadsheet_Excel_Reader($file,false);
				debug("data: ".return_array($data),$c[debug]);
				
				// Standardize
				for($i = 1; $i <= $data->sheets[0]['numRows']; $i++) {
					$row = NULL;
					for ($j = 1; $j <= $data->sheets[0]['numCols']; $j++) {
						$row[] = $data->sheets[0]['cells'][$i][$j];
					}
					$array[] = $row;
				}
			}
			else $c[method] = "simple";
		}
		
		// Simple
		if($c[method] == "simple") {
			#build this
		}
		
		// Return
		return $array;
	}
}

if(!function_exists('array2xls')) {
	/**
	 * Converts given array into an xls string which can then be saved as an xls file.
	 * 
	 * Note, this is a very simple xls version and doesn't include any of the fancy stuff.							
	 * 
	 * @param array $array The array of data you want to turn into an xls string.
	 * @param array $c An array of configuration values. Default = NULL
	 * @return string The resulting xls string.
	 */
	function array2xls($array,$c = NULL) {
		// Error
		if(!$array) return;
		
		// Config
		if(!x($c[delimiter])) $c[delimiter] = "\t";
		if(!x($c[row])) $c[row] = "\n";
		
		// String
		$string = NULL;
		
		// Build
		foreach($array as $x => $row) {
			$string_row = NULL;
			foreach($row as $k => $v) {
				$string_row .= ($string_row ? $c[delimiter] : "").xls_escape($v);
			}
			$string .= ($string ? $c[row] : "").$string_row;
		}
		
		// Return
		return $string;
	}
}
if(!function_exists('xls_escape')) {
	/**
	 * Escapes special system characters present in a string.
	 *
	 * @param string $string The string you want to escape.
	 * @param array $c An array of configuration values. Default = NULL
	 * @return string The string with all special characters escaped.							
	 */	
	function xls_escape($string,$c = NULL) {
		// Config
		if(!x($c[strip])) $c[strip] = 1; // Strip tags
		
		// Strip tags
		if($c[strip] == 1) $string = strip_tags($string);
		// Remove line breaks and tabs
		$string = str_replace(array("\t","\n","\r")," ",$string);
		
		// Return 
		return $string;
	}
}

if(!function_exists('xls_download')) {	
	/**
	 * Forces download of the given XLS string as a XLS file.
	 *
	 * @param string $string The XLS string you want to include in the download.
	 * @param string $name The name of the XLS file the user will download. Default = xls.xls
	 */
	function xls_download($string,$name = "xls.xls") {
		// Extension
		if(!file::extension($name)) $name .= ".xls";
		
		// Header
		header("Pragma: public"); // required
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Cache-Control: private",false); // required for certain browsers 
		header("Content-Type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename=\"".$name."\";" );
		header("Content-Transfer-Encoding: binary");
			
		// String
		print $string;
		
		// Exit
		exit;
	}
}
?>