<?php
/**
 * @package kraken\modules\states
 */
 
if(!function_exists('state_id')) {
	/**
	 * Returns the id of the state that corresponds to the given state code.
	 *
	 * @param string $code The state code you want to get the id of.
	 * @param string $country The (optional) country code for the state we're looking up. So as to avoid any code duplicates across countries. Default = NULL
	 * @return int The id of the state.
	 */
	function state_id($code,$country = NULL) {
		// Error
		if(!$code) return;
		
		// Standardize
		$code = strtolower($code);
		$country = strtoupper($country);
		
		// Database
		$db = db::load();
		
		// Country
		$row = $db->f("SELECT state_id FROM states WHERE LOWER(state_code) = '".a($code)."'".($country ? " AND country_code = '".a($country)."'" : ""));
		
		// Return
		return $row[state_id];
	}
}
 
if(!function_exists('state_code')) {
	/**
	 * Returns the code of the state that corresponds to the given state id.
	 *
	 * Note: since we don't have codes for most countries, it only returns the 'code' if the country is the United States. All others return the state name.
	 *
	 * @param int|array|object $id The id, database row, or item object of the state you want to get the code of
	 * @return string The corresponding state code.
	 */
	function state_code($id) {
		// Error
		if(!$id) return;
		
		// Item
		if(is_object($id)) {
			$item = $id;
			$id = $item->id;
		}
		else $item = item::load('states',$id);
		
		// Code
		if(in_array($item->v('country_code'),array('US'))) $code = $item->v('state_code');
		else $code = $item->v('state_name');
		
		// Return
		return $code;
	}
}
 
if(!function_exists('state_name')) {
	/**
	 * Returns the name of the state that corresponds to the given state id.
	 *
	 * @param int|array|object $id The id, database row, or item object of the state you want to get the name of
	 * @return string The corresponding state name.
	 */
	function state_name($id) {
		// Error
		if(!$id) return;
		
		// Item
		if(is_object($id)) {
			$item = $id;
			$id = $item->id;
		}
		else $item = item::load('states',$id);
		
		// Return
		return $item->v('state_name');
	}
}
?>