<?php
// Already logged in
if(u('id')) {
	// Redirect URL
	for($x = 1;$x <= 10;$x++) {
		if($_SESSION['urls'][$x] and !strstr($redirect,'login') and !strstr($redirect,'register')) {
			$redirect = $_SESSION['urls'][$x];
			$x = 11;
		}
	}
	if(!$redirect) $redirect = DOMAIN;
	
	// Redirect
	redirect(DOMAIN);
}
// Register
else {
	// Type
	$type = users_type_default();
	if($type) {
		$form = $this->module_class->form(array('type' => $type));
		print "
<div class='page page-module-".$this->module." page-view-".$this->view."'>
	<h1 class='page-heading'>Register</h1>
	".$form->render()."
</div>";
	}
	else {
		debug("There's no registered non-admin user type defined");	
	}
}
?>