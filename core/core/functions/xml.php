<?php
/**
 * @package kraken\xml
 */
 
if(!function_exists('array2xml')) {
	/**
	 * Converts given array into xml
	 *
	 * If you want to use a different tag for a parent element you can pass __xml_label in the children.
	 * For example, if you had multiple items in the array with numeric keys (ex: $items = array(array('k' => 'v'),array('k' => 'v'));) and you wanted their parent element to be 'item', you'd pass $items = array(array('k' => 'v','__xml_label' => 'item'));
	 * 
	 * @param array $array The array we want to convert into XML. Can be multi-dimensional.
	 * @param int $indent How much to indent the given line of data. Mostly used by the function when it's calling sub-items. Default = 0
	 * @return string The resulting XML string
	 */
	function array2xml($array,$indent = 0) {
		// Indent
		$indent_character = "	"; // Tab
		for($x = 0;$x < $indent;$x++) $indent_string .=  $indent_character;
		
		if($array) {
			foreach($array as $k => $v) {
				// Defined 'label'
				if(substr($k,0,2) == "__") continue;
				if(is_numeric($k) and $v[__xml_label]) $k = $v[__xml_label];
				
				// Parent
				$xml .= "
	".$indent_string."<".$k.">";
	
				// Child - array
				if(is_array($v)) {
					$xml .= array2xml($v,$indent + 1)."
	".$indent_string."</".$k.">";
				}
				// Child - value
				else {
					if(strip_tags($v) != $v) $xml .= "<![CDATA[".$v."]]>";
					else $xml .= xml_escape(s($v));
					$xml .= "</".$k.">";
				}
			}
		}
		
		// Return
		return $xml;
	}
}

if(!function_exists('xml2array')) {
	/**
	 * Parses given xml file (or xml content) and returns an array
	 *
	 * Based on http://www.bin-co.com/php/scripts/xml2array/									
	 * 
	 * @param string $file Either a local file we want to get the XML contents of or an XML string
	 * @param array $c An array of configuration values. Default = NULL
	 * @return array An arry of the parsed XML data.
	 */
	function xml2array($file,$c = NULL) {
		//$f_r = function_speed(__FUNCTION__);
		if(!x($c[get_attributes])) $c[get_attributes] = 1; // '1' or '0', do you want to include the attributes in the array
		if(!$c[priority]) $c[priority] = 'tag'; // 'tag' or 'attribute'
		if(!x($c[hard])) $c[hard] = 0; // Do you want to return a 'hard' array, numbered keys at every level (not yet supported)
		if(!x($c[debug])) $c[debug] = 0; // Debug
		if(strstr($file,'<') and strstr($file,'</')) $contents = $file; // Passed XML content
		else $contents = file_get_contents($file); // Passed XML file URL
		if(!$contents) return array();
		
		if(!function_exists('xml_parser_create')) {
			debug("xml_parser_create() function not found!",$c[debug]);
			//function_speed(__FUNCTION__,$f_r);
			return array();
		}
	
		// Remove <?xml> declaration
		$contents = preg_replace('/<\?xml(.*?)>/','',$contents);
	
		// Wrap // Parser only gets first value if no single parent div
		$contents = "<____xml____>".$contents."</____xml____>";
		//debug("contents: <xmp>".$contents."</xmp>",$c[debug]);
	
		// Get the XML parser of PHP - PHP must have this module for the parser to work
		$parser = xml_parser_create('');
		xml_parser_set_option($parser, XML_OPTION_TARGET_ENCODING, "UTF-8"); 
		xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
		xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
		xml_parse_into_struct($parser, trim($contents), $xml_values);
		xml_parser_free($parser);
	
		if(!$xml_values) {
			debug("Wasn't able to find any xml values.",$c[debug]);
			return; // Hmm...
		}
		//debug("raw values:",$xml_values,$c[debug]);
	
		// Initializations
		$xml_array = array();
		$parents = array();
		$opened_tags = array();
		$arr = array();
	
		$current = &$xml_array; // Refference
	
		// Go through the tags.
		$repeated_tag_index = array();// Multiple tags with same name will be turned into an array
		foreach($xml_values as $data) {
			unset($attributes,$value);// Remove existing values, or there will be trouble
	
			// This command will extract these variables into the foreach scope
			//  tag(string), type(string), level(int), attributes(array).
			extract($data);// We could use the array by itself, but this cooler.
	
			$result = array();
			$attributes_data = array();
			
			if(isset($value)) {
				if($c[priority] == 'tag') $result = $value;
				else $result['value'] = $value; // Put the value in a assoc array if we are in the 'Attribute' mode
			}
	
			// Set the attributes too.
			if(isset($attributes) and $c[get_attributes]) {
				foreach($attributes as $attr => $val) {
					if($c[priority] == 'tag') $attributes_data[$attr] = $val;
					else $result['attr'][$attr] = $val; // Set all the attributes in a array called 'attr'
				}
			}
	
			// See tag status and do the needed.
			if($type == "open") {// The starting of the tag '<tag>'
				$parent[$level-1] = &$current;
				if((!is_array($current) or !in_array($tag, array_keys($current))) and $c[hard] == 0) { // Insert New tag
					$current[$tag] = $result;
					if($attributes_data) $current[$tag. '_attr'] = $attributes_data;
					$repeated_tag_index[$tag.'_'.$level] = 1;
	
					$current = &$current[$tag];
	
				} else { // There was another element with the same tag name
	
					if(isset($current[$tag][0])) {// If there is a 0th element it is already an array
						$current[$tag][$repeated_tag_index[$tag.'_'.$level]] = $result;
						$repeated_tag_index[$tag.'_'.$level]++;
					} else {// This section will make the value an array if multiple tags with the same name appear together
						$current[$tag] = array($current[$tag],$result);// This will combine the existing item and the new item together to make an array
						$repeated_tag_index[$tag.'_'.$level] = 2;
						
						if(isset($current[$tag.'_attr'])) { // The attribute of the last(0th) tag must be moved as well
							$current[$tag]['0_attr'] = $current[$tag.'_attr'];
							unset($current[$tag.'_attr']);
						}
	
					}
					$last_item_index = $repeated_tag_index[$tag.'_'.$level]-1;
					$current = &$current[$tag][$last_item_index];
				}

	
			} elseif($type == "complete") { // Tags that ends in 1 line '<tag />'
				// See if the key is already taken.
				if(!x($current[$tag])) { // New Key
					$current[$tag] = $result;
					$repeated_tag_index[$tag.'_'.$level] = 1;
					if($c[priority] == 'tag' and $attributes_data) $current[$tag. '_attr'] = $attributes_data;
	
				} else { // If taken, put all things inside a list(array)
					if(isset($current[$tag][0]) and is_array($current[$tag])) {// If it is already an array...
	
						//  ...push the new element into that array.
						$current[$tag][$repeated_tag_index[$tag.'_'.$level]] = $result;
						
						if($c[priority] == 'tag' and $c[get_attributes] and $attributes_data) {
							$current[$tag][$repeated_tag_index[$tag.'_'.$level] . '_attr'] = $attributes_data;
						}
						$repeated_tag_index[$tag.'_'.$level]++;
	
					} else { // If it is not an array...
						$current[$tag] = array($current[$tag],$result); // ...Make it an array using using the existing value and the new value
						$repeated_tag_index[$tag.'_'.$level] = 1;
						if($c[priority] == 'tag' and $c[get_attributes]) {
							if(isset($current[$tag.'_attr'])) { // The attribute of the last(0th) tag must be moved as well
								
								$current[$tag]['0_attr'] = $current[$tag.'_attr'];
								unset($current[$tag.'_attr']);
							}
							
							if($attributes_data) {
								$current[$tag][$repeated_tag_index[$tag.'_'.$level] . '_attr'] = $attributes_data;
							}
						}
						$repeated_tag_index[$tag.'_'.$level]++; // 0 and 1 index is already taken
					}
				}
	
			} elseif($type == 'close') { // End of tag '</tag>'
				$current = &$parent[$level-1];
			}
		}
		
		// Unwrap
		$xml_array = $xml_array[____xml____];
		
		// Return
		return $xml_array;
	}
}

if(!function_exists('xml_escape')) {
	/**
	 * Escapes characters reservied by XML (&, <, >, ', ") in the given string.		
	 *
	 * Note, we won't escape the string if it's enclosed in a <![CDATA[ tag.			
	 * 
	 * @param string $string The string you want to escape.
	 * @return string The string with reserved characters escaped.
	 */
	function xml_escape($string) {
		if(substr($string,0,9) != "<![CDATA[") {
			$search = array("&","'",'"',"<",">","\t","\n","\r","  ");
			$replace = array("&amp;","&apos;","&quot;","&lt;","&gt;"," "," "," "," ");
			$unreplace = array("&amp;amp;","&apos;apos;","&quot;quot;","&lt;lt;","&gt;gt;");
			
			$string = str_replace($search,$replace,$string); // Covert to htmlentities
			$string = str_replace($unreplace,$replace,$string); // Fix characters that were already htmlentities
		}
		
		// Return
		return $string;				  
	}
}
?>