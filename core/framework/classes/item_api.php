<?php
if(!class_exists('item_api',false)) {
	/**
	 * An extenion of the item class with functionality specific to the api.
	 *
	 * @package kraken\api
	 */
	class item_api extends item {
		/**
		 * Loads and returns an instance of either the core item_api class or (if it exists) the item specific item_api class.
		 *
		 * Example:
		 * - $item_api = item_api::load($module,$id,$c);
		 *
		 * @param string $module The module of the item.
		 * @param int $id The id of the item.
		 * @return object An instance of either the core item_api class or (if it exists) the module specific item_api class.
		 */
		static function load($module,$id,$c = NULL) {
			$params = func_get_args(); // Must be outside function call
			$class_load = load_class_module(__CLASS__,$params,$module); // No way to instanitiate class with array of params so we'll just return the class name and...
			return new $class_load($module,$id,$c); // ...manually pass the params here
		}
		
		/**
		 * Constructs the class.
		 *
		 * @param string $module The module of the item.
		 * @param int $id The id of the item.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function __construct($module,$id,$c = NULL) {
			self::item_api($module,$id,$c);
		}
		function item_api($module,$id,$c = NULL) {
			// Parent
			parent::__construct($module,$id,$c);
		}
		
		/**
		 * Returns the API item array for this item.
		 *
		 * @param object $api The api object this was called from.
		 * @return array This item's item array as it's used in the API.
		 */
		function item($api) {
			// Format
			$format = m($this->module.'.api.actions.'.$api->call[action].'.item');
			if(!$format) $format = m($this->module.'.api.item');
			
			// Array
			$array = $this->format($this->row,$format,$api,"out");
			
			// XML label
			if($api->format == "xml") {
				$array[__xml_label] = "item";
			}
			
			// Return
			return $array;
		}
		
		/**
		 * Processes the given array of values through the given format array, retuning the formatted array of values.
		 *
		 * @param array $values The array of values you want to format.
		 * @param array $format The array which defines the format you want to return the values in.
		 * @param object $api The api object this was called from.
		 * @param string $direction When direction we're going: in (we received these values and are going to use them), out (we are displaying these values). Default = out
		 * @return array The formatted array of values.
		 */
		function format($values,$format,$api,$direction = "out") {
			// No format
			if(!$format) return $values;
			
			// Format
			foreach($format as $x => $v) {
				// In
				if($direction == "in") {
					// Value
					$value = $values[$v[label]];
					// Value - method
					if($v[method]) {
						if(method_exists($this,$v[method])) {
							$value = $this->$v[method]($value,$api);	
						}
					}
				
					// Key
					$key = $v[name];
				}
				// Out
				if($direction == "out") {
					// Value
					if(substr($v[name],0,3) == "db.") $value = $values[m($this->module.'.'.$v[name])];
					else $value = $values[$v[name]];
					// Value - method
					if($v[method]) {
						if(method_exists($this,$v[method])) {
							$value = $this->$v[method]($value,$api);	
						}
					}
				
					// Key
					if($v[label]) $key = $v[label];
					else $key = (substr($v[name],0,3) == "db." ? substr($v[name],3) : $v[name]);
				}
				
				// Pair
				$array[$key] = $value;
			}
			
			// Return
			return $array;
		}
		
		/**
		 * Returns the public URL for this item.
		 *
		 * Basically just a way for us to connect 'method' => 'url' in the API item format to the item classes url() method.
		 *
		 * @param string $value The stored URL value. This is always NULL as we don't have a 'name' field to match up to when getting a URL.
		 * @param object $api The API object.
		 * @return string The public url of this item.
		 */
		function url($value = NULL,$api) {
			// Parent
			$url = parent::url();
			
			// Return
			return $url;	
		}
	}
}
?>