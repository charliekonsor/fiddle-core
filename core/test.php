<?php
/************************************************************************************/
/*************************************** Dawn ***************************************/
/************************************************************************************/
/** Dawn - loads core functionality */
require_once "../core/core/includes/dawn.php";

// Header
if(debug_active() == false) exit;
print $page->header()."
<body>";
 
/************************************************************************************/
/************************************* Content **************************************/
/************************************************************************************/

$form = form::load();
$form->text('My Text','my_text','saved value',array('style' => 'border:1px solid red;'));
$form->rating('Rating','my_rating',2,array('half' => 1,'cancel' => 1,'stars' => 6));
print $form->render();

/*$array = array(
	array(
		'Date',
		'Time',
		'Employee',
		'Task',
	)
);
$total = 0;
$db_daedalus = db::load(array('username' => 'root','database' => 'angelvis_daedalus','password' => 'Cramp5Ramp%','server' => 'angelvision.tv'));
$rows = $db_daedalus->q("SELECT * FROM tasks_time WHERE project_id = 68 ORDER BY time_created");
while($row = $db_daedalus->f($rows)) {
	$row0 = $db_daedalus->f("SELECT * FROM users WHERE user_id = '".$row[employee_id]."'");
	$row1 = $db_daedalus->f("SELECT * FROM tasks WHERE task_id = '".$row[task_id]."'");
	$array[] = array(
		date('m/d/Y',$row[time_created]),
		minutes2hours($row[time]),
		$row0[user_first_name]." ".$row0[user_last_name],
		$row1[task_objective],
	);
	$total += $row[time];
}
$array[] = array(
	'Total',
	minutes2hours($total),
);

include_function('csv');
$csv = array2csv($array);
csv_download($csv,'nike-spreadsheet.csv');
exit;

/*$provider = "facebook";

$user = item::load('users',u('id'));
if($connection = $user->external_connected($provider,array('debug' => 1))) {
	//print "get facebook stuff";	
	//$profile = $connection->getUserProfile();
	
	// Array
	$array = NULL;
	
	// Facebook
	if($provider == "facebook") {
		$results = $connection->api()->api('/me/photos',array('limit' => 99999));
		foreach($results as $result) {
			
		}
	}
	
	if(count($array)) {
		print "
	<ul class='core-thumbs core-thumbs-m'>";
		foreach($array as $v) {
			
		}
		print "
	</ul>
	<div class='clear'></div>";
	}
	else print "
	<div class='core-none'>I'm afraid we couldn't find any of your ".hybridauth_provider($provider)." content.</div>";
	
	
	print "results:";
	print_array($results);
	
	print "profile:";
	print_array($profile);
	print "connection:";
	print_array($connection);
}
else {
	print "
<a href='".DOMAIN."?process_action=login_external&amp;provider=".$provider."&amp;redirect=".urlencode(URL)."'>Connect With ".hybridauth_provider($provider)."</a>";	
}

/*$core->db->q("ALTER TABLE  `users` ADD  `user_hash` VARCHAR( 64 ) NOT NULL AFTER  `user_template`");
$rows = $core->db->q("SELECT * FROM users");
while($row = $core->db->f($rows)) {
	$item = item::load('users',$row);
	$values = array(
		'user_password' => $item->v('user_password')
	);
	$item->save($values,array('debug' => 1));
}


/*$form = form::load('users',1);
print_array($form);

/*include_class("geo");
$geo = new geo();
$results = $geo->search(45.4395124,-122.7711943);
print_array($results);


/*$form = form::load('users',1);
$form->render();
print_array($form);

/*$item = item::load('users',1);
$item->db('name');
print_array($item);

/*cache_delete_expired();


/*$item = item::load('users',2);
$values = array(
	'user_password' => 'dwstudios'
);
$item->save($values,array('debug' => 1));

/*$string = "dwstudios";
$string_insensitive = "dwStuDios";
$hash = "7234872";
$crypt = crypt($string,$hash);
print "crypt: ".$crypt."<br />";

print "straigh match: ";
if(crypt($string,$hash) == $crypt) {
	print "matches";
}
print "<br />";

print "insensitive match: ";
if(crypt($string_insensitive,$hash) == $crypt) {
	print "matches";
}
print "<br />";

/*$item = item::load('users',1);
$item->db('name');
//print_array($item);
$run = 100;

$start = microtime_float();
for($x = 0;$x < $run;$x++) {
	cache_save('item/users/1/eng',$item);
}
print "txt took ".round(microtime_float() - $start,5)." seconds<br />";

$db = db::load();
$start = microtime_float();
for($x = 0;$x < $run;$x++) {
	$db->q("INSERT INTO test SET test = '".a(data_serialize($item))."'");
}
print "db took ".round(microtime_float() - $start,5)." seconds<br />";


/*include_function('email');
email('charlie@dwstudios.net','Test','test message');

/*
// Values - GET
$method = ($_GET['method'] ? $_GET['method'] : "card");
$gateway = ($_GET['gateway'] ? $_GET['gateway'] : "qbms");
$action = ($_GET['action'] ? $_GET['action'] : "charge-expired");
$payment = ($_GET['payment'] ? $_GET['payment'] : 123);
$test = (x($_GET['test']) ? $_GET['test'] : 1);

// Values - Action
list($action,$action_issue) = explode('-',$action);

// Values - Test
if($test) {
	$amount = (x($_GET['amount']) ? $_GET['amount'] : "1.23");
	$card = array(
		'number' => ($_GET['card_number'] ? $_GET['card_number'] : '4111111111111111'),
		'expiration_month' => ($_GET['card_expiration_month'] ? $_GET['card_expiration_month'] : 12),
		'expiration_year' => ($_GET['card_expiration_year'] ? $_GET['card_expiration_year'] : 2013),
		'code' => '123'
	);
	$address = array(
		'first_name' => 'Test',
		'last_name' => 'Customer',
		'address' => '3811 SW Hall Blvd',
		'address_2' => 'Suite 123',
		'city' => 'Beaverton',
		'state' => 'OR',
		'zip' => '97005',
		'country' => 'US',
	);
}

// Values - Live
else {
	$amount = (x($_GET['amount']) ? $_GET['amount'] : "0.01");
	$card = array(
		'number' => ($_GET['card_number'] ? $_GET['card_number'] : ''),
		'expiration_month' => ($_GET['card_expiration_month'] ? $_GET['card_expiration_month'] : 7),
		'expiration_year' => ($_GET['card_expiration_year'] ? $_GET['card_expiration_year'] : 2014),
		'code' => '654'
	);
	$address = array(
		'first_name' => 'Charles',
		'last_name' => 'Konsor',
		'address' => '1565 SE Maple Ave',
		'address_2' => 'Apt A',
		'city' => 'Portland',
		'state' => 'OR',
		'zip' => '97214',
		'country' => 'US',
	);
}

// Valeus - Transaction
$transaction = ($_GET['transaction'] ? $_GET['transaction'] : 'a:36:{s:4:"Type";s:6:"Charge";s:17:"CreditCardTransID";s:12:"YY1004539476";s:17:"AuthorizationCode";s:6:"356153";s:9:"AVSStreet";s:4:"Pass";s:6:"AVSZip";s:4:"Pass";s:21:"CardSecurityCodeMatch";s:4:"Pass";s:13:"ClientTransID";s:8:"q0d2a3cf";s:21:"MerchantAccountNumber";s:16:"5183555073002730";s:12:"ReconBatchID";s:38:"420131029 1Q13055183555073002730AUTO04";s:19:"PaymentGroupingCode";s:1:"5";s:13:"PaymentStatus";s:9:"Completed";s:20:"TxnAuthorizationTime";s:19:"2013-10-29T20:05:43";s:21:"TxnAuthorizationStamp";s:10:"1383077143";s:11:"NetworkName";b:0;s:13:"NetworkNumber";b:0;s:57:"CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardNumber";s:16:"xxxxxxxxxxxx1111";s:56:"CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationMonth";i:12;s:55:"CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationYear";i:2013;s:51:"CreditCardTxnInfo_CreditCardTxnInputInfo_NameOnCard";s:13:"Test Customer";s:58:"CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardAddress";s:27:"3811 SW Hall Blvd Suite 123";s:61:"CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardPostalCode";s:5:"97005";s:58:"CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardTxnType";s:6:"Charge";s:52:"CreditCardTxnInfo_CreditCardTxnResultInfo_ResultCode";s:1:"0";s:55:"CreditCardTxnInfo_CreditCardTxnResultInfo_ResultMessage";s:9:"Status OK";s:59:"CreditCardTxnInfo_CreditCardTxnResultInfo_CreditCardTransID";s:12:"YY1004539476";s:63:"CreditCardTxnInfo_CreditCardTxnResultInfo_MerchantAccountNumber";s:16:"5183555073002730";s:59:"CreditCardTxnInfo_CreditCardTxnResultInfo_AuthorizationCode";s:6:"356153";s:51:"CreditCardTxnInfo_CreditCardTxnResultInfo_AVSStreet";s:4:"Pass";s:48:"CreditCardTxnInfo_CreditCardTxnResultInfo_AVSZip";s:4:"Pass";s:63:"CreditCardTxnInfo_CreditCardTxnResultInfo_CardSecurityCodeMatch";s:4:"Pass";s:54:"CreditCardTxnInfo_CreditCardTxnResultInfo_ReconBatchID";s:38:"420131029 1Q13055183555073002730AUTO04";s:61:"CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentGroupingCode";s:1:"5";s:55:"CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentStatus";s:9:"Completed";s:62:"CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationTime";s:19:"2013-10-29T20:05:43";s:63:"CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationStamp";s:10:"1383077143";s:55:"CreditCardTxnInfo_CreditCardTxnResultInfo_ClientTransID";s:8:"q0d2a3cf";}');

// Values - Issues
if($action_issue == 'expired') $card[expiration_year] = 2001;

// Values
$values = array(
	'amount' => $amount,
	'card' => $card,
	'address' => $address,
	'transaction' => $transaction,
	'payment' => array(
		'payment_id' => $payment
	),
);

// Results
if($method == "card") $results = payments_card($gateway,$action,$values,array('test' => $test,'debug' => 1));
if($method == "paypal") $results = payments_paypal($action,$values,array('test' => $test,'debug' => 1));
if($method == "googlecheckout") $results = payments_googlecheckout($action,$values,array('test' => $test,'debug' => 1));

//$class = payments_card_class('qbms',array('test' => 1,'debug' => 1));

/* // Deprecate old ones, add new ones
$array = array(
	// Domestic
	'First-Class Mail Large Envelope',
	'First-Class Mail Letter',
	'First-Class Mail Parcel',
	'First-Class Mail Postcards',
	'Priority Mail',
	'Priority Mail Express Hold For Pickup',
	'Priority Mail Express',
	'Standard Post',
	'Media Mail',
	'Library Mail',
	'Priority Mail Express Flat Rate Envelope',
	'First-Class Mail Large Postcards',
	'Priority Mail Flat Rate Envelope',
	'Priority Mail Medium Flat Rate Box',
	'Priority Mail Large Flat Rate Box',
	'Priority Mail Express Sunday/Holiday Delivery',
	'Priority Mail Express Sunday/Holiday Delivery Flat Rate Envelope',
	'Priority Mail Express Flat Rate Envelope Hold For Pickup',
	'Priority Mail Small Flat Rate Box',
	'Priority Mail Padded Flat Rate Envelope',
	'Priority Mail Express Legal Flat Rate Envelope',
	'Priority Mail Express Legal Flat Rate Envelope Hold For Pickup',
	'Priority Mail Express Sunday/Holiday Delivery Legal Flat Rate Envelope',
	'Priority Mail Hold For Pickup',
	'Priority Mail Large Flat Rate Box Hold For Pickup',
	'Priority Mail Medium Flat Rate Box Hold For Pickup',
	'Priority Mail Small Flat Rate Box Hold For Pickup',
	'Priority Mail Flat Rate Envelope Hold For Pickup',
	'Priority Mail Gift Card Flat Rate Envelope',
	'Priority Mail Gift Card Flat Rate Envelope Hold For Pickup',
	'Priority Mail Window Flat Rate Envelope',
	'Priority Mail Window Flat Rate Envelope Hold For Pickup',
	'Priority Mail Small Flat Rate Envelope',
	'Priority Mail Small Flat Rate Envelope Hold For Pickup',
	'Priority Mail Legal Flat Rate Envelope',
	'Priority Mail Legal Flat Rate Envelope Hold For Pickup',
	'Priority Mail Padded Flat Rate Envelope Hold For Pickup',
	'Priority Mail Regional Rate Box A',
	'Priority Mail Regional Rate Box A Hold For Pickup',
	'Priority Mail Regional Rate Box B',
	'Priority Mail Regional Rate Box B Hold For Pickup',
	'First-Class Package Service Hold For Pickup',
	'Priority Mail Express Flat Rate Boxes',
	'Priority Mail Express Flat Rate Boxes Hold For Pickup',
	'Priority Mail Express Sunday/Holiday Delivery Flat Rate Boxes',
	'Priority Mail Regional Rate Box C',
	'Priority Mail Regional Rate Box C Hold For Pickup',
	'First-Class Package Service',
	'Priority Mail Express Padded Flat Rate Envelope',
	'Priority Mail Express Padded Flat Rate Envelope Hold For Pickup',
	'Priority Mail Express Sunday/Holiday Delivery Padded Flat Rate Envelope',
	// International
	'Priority Mail Express International',
	'Priority Mail International',
	'Global Express Guaranteed (GXG)',
	'Global Express Guaranteed Document',
	'Global Express Guaranteed Non-Document Rectangular',
	'Global Express Guaranteed Non-Document Non-Rectangular',
	'Priority Mail International Flat Rate Envelope',
	'Priority Mail International Medium Flat Rate Box',
	'Priority Mail Express International Flat Rate Envelope',
	'Priority Mail International Large Flat Rate Box',
	'USPS GXG Envelopes',
	'First-Class Mail International Letter',
	'First-Class Mail International Large Envelope',
	'First-Class Package International Service',
	'Priority Mail International Small Flat Rate Box',
	'Priority Mail Express International Legal Flat Rate Envelope',
	'Priority Mail International Gift Card Flat Rate Envelope',
	'Priority Mail International Window Flat Rate Envelope',
	'Priority Mail International Small Flat Rate Envelope',
	'First-Class Mail International Postcard',
	'Priority Mail International Legal Flat Rate Envelope',
	'Priority Mail International Padded Flat Rate Envelope',
	'Priority Mail International DVD Flat Rate priced box',
	'Priority Mail International Large Video Flat Rate priced box',
	'Priority Mail Express International Flat Rate Boxes',
	'Priority Mail Express International Padded Flat Rate Envelope',
);
$core->db->q("UPDATE shipping SET shipping_name = REPLACE(shipping_name,'Express Mail','Priority Mail Express') WHERE shipping_company = 'usps'");
$core->db->q("UPDATE shipping SET shipping_code = REPLACE(shipping_code,'Express Mail','Priority Mail Express') WHERE shipping_company = 'usps'");
$core->db->q("UPDATE shipping SET shipping_name = REPLACE(shipping_name,'Parcel Post','Standard Post') WHERE shipping_company = 'usps'");
$core->db->q("UPDATE shipping SET shipping_code = REPLACE(shipping_code,'Parcel Post','Standard Post') WHERE shipping_company = 'usps'");

$ids = NULL;
foreach($array as $k) {
	$row = $core->db->f("SELECT * FROM shipping WHERE shipping_company = 'usps' AND shipping_code = '".a($k)."'");
	if(!$row[shipping_id]) {
		$values = array(
			'type_code' => 'shipping_dynamic',
			'shipping_company' => 'usps',
			'shipping_code' => $k,
			'shipping_name' => $k,
			'shipping_active' => 0,
		);
		$item = item::load('shipping','new');	
		$row[shipping_id] = $item->save($values);
	}
	$ids[] = $row[shipping_id];
}
$rows = $core->db->q("SELECT * FROM shipping WHERE shipping_company = 'usps' AND shipping_id NOT IN (".implode(',',$ids).")");
while($row = $core->db->f($rows)) {
	if($row[shipping_active]) print "
<span class='core-red'>".$row[shipping_name]." was active and is no longer present</span><br />";
	$item = item::load('shipping',$row);
	$item->save(array('shipping_name' => $row[shipping_name]." (Deprecated)",'shipping_active' => 0));
	//$item->delete();
}

/* // Delete old ones, add new ones
$array = array(
	// Domestic
	'First-Class Mail Large Envelope',
	'First-Class Mail Letter',
	'First-Class Mail Parcel',
	'First-Class Mail Postcards',
	'Priority Mail',
	'Priority Mail Express Hold For Pickup',
	'Priority Mail Express',
	'Standard Post',
	'Media Mail',
	'Library Mail',
	'Priority Mail Express Flat Rate Envelope',
	'First-Class Mail Large Postcards',
	'Priority Mail Flat Rate Envelope',
	'Priority Mail Medium Flat Rate Box',
	'Priority Mail Large Flat Rate Box',
	'Priority Mail Express Sunday/Holiday Delivery',
	'Priority Mail Express Sunday/Holiday Delivery Flat Rate Envelope',
	'Priority Mail Express Flat Rate Envelope Hold For Pickup',
	'Priority Mail Small Flat Rate Box',
	'Priority Mail Padded Flat Rate Envelope',
	'Priority Mail Express Legal Flat Rate Envelope',
	'Priority Mail Express Legal Flat Rate Envelope Hold For Pickup',
	'Priority Mail Express Sunday/Holiday Delivery Legal Flat Rate Envelope',
	'Priority Mail Hold For Pickup',
	'Priority Mail Large Flat Rate Box Hold For Pickup',
	'Priority Mail Medium Flat Rate Box Hold For Pickup',
	'Priority Mail Small Flat Rate Box Hold For Pickup',
	'Priority Mail Flat Rate Envelope Hold For Pickup',
	'Priority Mail Gift Card Flat Rate Envelope',
	'Priority Mail Gift Card Flat Rate Envelope Hold For Pickup',
	'Priority Mail Window Flat Rate Envelope',
	'Priority Mail Window Flat Rate Envelope Hold For Pickup',
	'Priority Mail Small Flat Rate Envelope',
	'Priority Mail Small Flat Rate Envelope Hold For Pickup',
	'Priority Mail Legal Flat Rate Envelope',
	'Priority Mail Legal Flat Rate Envelope Hold For Pickup',
	'Priority Mail Padded Flat Rate Envelope Hold For Pickup',
	'Priority Mail Regional Rate Box A',
	'Priority Mail Regional Rate Box A Hold For Pickup',
	'Priority Mail Regional Rate Box B',
	'Priority Mail Regional Rate Box B Hold For Pickup',
	'First-Class Package Service Hold For Pickup',
	'Priority Mail Express Flat Rate Boxes',
	'Priority Mail Express Flat Rate Boxes Hold For Pickup',
	'Priority Mail Express Sunday/Holiday Delivery Flat Rate Boxes',
	'Priority Mail Regional Rate Box C',
	'Priority Mail Regional Rate Box C Hold For Pickup',
	'First-Class Package Service',
	'Priority Mail Express Padded Flat Rate Envelope',
	'Priority Mail Express Padded Flat Rate Envelope Hold For Pickup',
	'Priority Mail Express Sunday/Holiday Delivery Padded Flat Rate Envelope',
	// International
	'Priority Mail Express International',
	'Priority Mail International',
	'Global Express Guaranteed (GXG)',
	'Global Express Guaranteed Document',
	'Global Express Guaranteed Non-Document Rectangular',
	'Global Express Guaranteed Non-Document Non-Rectangular',
	'Priority Mail International Flat Rate Envelope',
	'Priority Mail International Medium Flat Rate Box',
	'Priority Mail Express International Flat Rate Envelope',
	'Priority Mail International Large Flat Rate Box',
	'USPS GXG Envelopes',
	'First-Class Mail International Letter',
	'First-Class Mail International Large Envelope',
	'First-Class Package International Service',
	'Priority Mail International Small Flat Rate Box',
	'Priority Mail Express International Legal Flat Rate Envelope',
	'Priority Mail International Gift Card Flat Rate Envelope',
	'Priority Mail International Window Flat Rate Envelope',
	'Priority Mail International Small Flat Rate Envelope',
	'First-Class Mail International Postcard',
	'Priority Mail International Legal Flat Rate Envelope',
	'Priority Mail International Padded Flat Rate Envelope',
	'Priority Mail International DVD Flat Rate priced box',
	'Priority Mail International Large Video Flat Rate priced box',
	'Priority Mail Express International Flat Rate Boxes',
	'Priority Mail Express International Padded Flat Rate Envelope',
);
$core->db->q("DELETE FROM shipping WHERE shipping_company = 'usps'");
$ids = NULL;
foreach($array as $k) {
	//$row = $core->db->f("SELECT * FROM shipping WHERE shipping_company = 'usps' AND shipping_code = '".a($k)."'");
	//if(!$row[shipping_id]) {
		$values = array(
			'type_code' => 'shipping_dynamic',
			'shipping_company' => 'usps',
			'shipping_code' => $k,
			'shipping_name' => $k,
			'shipping_active' => 0,
		);
		$item = item::load('shipping','new');	
		$item->save($values);
	//}
}

/*


/*$module = "videos";
$files = array(
	'video_file' => 'video',
);

$rows = $core->db->q("SELECT * FROM ".m($module.'.db.table'));
while($row = $core->db->f($rows)) {
	// Values
	$values = array(
		'video_name' => $row[video_name],
		'video_file' => $row[video_file],
		'__plugins' => array(
			'categories' => array(
				1 => 1
			)
		)
	);
	
	// URL
	$url = "MYCUSTOMURL";
	$values[__plugins][urls][url_name] = $url;
	
	// Categories
	#build this, grab from table to get ids
	
	
	// Files
	foreach($files as $column => $type) {
		$plugin = plugin::load($module,NULL,'files');
		
		$values[__plugins][files][$column] = $plugin->values($row[$column],$type);
		if($values[__plugins][files][$column]) {
			$values[__plugins][files][$column][file_column] = $column;	
		}
	}
	
	// Save
	$item = item::load($module,'new');
	$item->save($values);
}

/*$module = ($_GET['module'] ? $_GET['module'] : 'blogs');
print_array(m($module));

/*print "
<iframe width='1000' height='700' src='http://67.50.151.136/~myimpact/local/modules/movies/embed.php?id=162' frameborder='0' allowfullscreen></iframe>";

/*$item = item::load('categories_items',46);
$item->process(array('debug' => 1));	

/*$rows = $core->db->q("SELECT * FROM categories_items");
while($row = $core->db->f($rows)) {
	$item = item::load('categories_items',$row);
	$item->process();	
}

/*include_function('curl');
$contents = curl("http://www.myimpactmovie.com/",array('debug' => 1));
print_array($contents);


/*$array = array(
	array(
		'table' => 'pages',
		'prefix' => 'page',
		'after' => 'page_meta_keywords'
	),
);
foreach($array as $v) {
	$query = "ALTER TABLE `".$v[table]."` ADD `".$v[prefix]."_published` TINYINT( 1 ) NOT NULL DEFAULT '1' AFTER `".$v[after]."` ,
ADD `".$v[prefix]."_published_start` DATETIME NOT NULL AFTER `".$v[prefix]."_published` ,
ADD `".$v[prefix]."_published_end` DATETIME NOT NULL AFTER `".$v[prefix]."_published_start` ";
	print $query.";<br />";
	//if($v[table] != "blogs") $core->db->q($query);
	
	print "
'published' => '".$v[prefix]."_published',<br />
'published_start' => '".$v[prefix]."_published_start',<br />
'published_end' => '".$v[prefix]."_published_end',<br /><br />";
}

/*$array = array(
	array(
		'table' => 'articles',
		'prefix' => 'article',
		'after' => 'article_file'
	),
	array(
		'table' => 'blogs',
		'prefix' => 'blog',
		'after' => 'blog_text'
	),
	array(
		'table' => 'carousels',
		'prefix' => 'carousel',
		'after' => 'carousel_settings'
	),
	array(
		'table' => 'carousels_slides',
		'prefix' => 'slide',
		'after' => 'slide_video'
	),
	array(
		'table' => 'events',
		'prefix' => 'event',
		'after' => 'event_meta_keywords'
	),
	array(
		'table' => 'faqs',
		'prefix' => 'faq',
		'after' => 'faq_answer'
	),
	array(
		'table' => 'galleries',
		'prefix' => 'gallery',
		'after' => 'gallery_meta_keywords'
	),
	array(
		'table' => 'galleries_media',
		'prefix' => 'media',
		'after' => 'media_template'
	),
	array(
		'table' => 'news',
		'prefix' => 'news',
		'after' => 'news_meta_keywords'
	),
	array(
		'table' => 'newsletters',
		'prefix' => 'newsletter',
		'after' => 'newsletter_meta_keywords'
	),
	array(
		'table' => 'pages',
		'prefix' => 'page',
		'after' => 'page_meta_keywords'
	),
	array(
		'table' => 'products',
		'prefix' => 'product',
		'after' => 'product_meta_keywords'
	),
);
foreach($array as $v) {
	$query = "ALTER TABLE `".$v[table]."` ADD `".$v[prefix]."_published` TINYINT( 1 ) NOT NULL DEFAULT '1' AFTER `".$v[after]."` ,
ADD `".$v[prefix]."_published_start` DATETIME NOT NULL AFTER `".$v[prefix]."_published` ,
ADD `".$v[prefix]."_published_end` DATETIME NOT NULL AFTER `".$v[prefix]."_published_start` ";
	print $query.";<br />";
	if($v[table] != "blogs") $core->db->q($query);
	
	print "
'published' => '".$v[prefix]."_published',<br />
'published_start' => '".$v[prefix]."_published_start',<br />
'published_end' => '".$v[prefix]."_published_end',<br /><br />";
}

/*$cart = cart::load('products');
$rates = $cart->shipping_rates(array('debug' => 0));
print_array($rates);

//print_array($cart);

/*$file = file::load(NULL,array('storage' => 'amazons3'));
print_array($file->command('ListBuckets'));
//print_array($file->directory_folders());

/*require SERVER."core/core/libraries/aws/aws.phar";

use Aws\S3\S3Client;
use Aws\Common\Enum\Region;

// Instantiate the S3 client with your AWS credentials and desired AWS region
$client = S3Client::factory(array(
    'key'    => 'AKIAJIFSEVLVMN5IWFFQ',
    'secret' => '+IwpPpLq+pHNBUXaB+e/Z7zTRdAmvPXRFohIDk1Q',
));

// Command - method 1
//$result = $client->listBuckets();
// Command - method 2
$command = $client->getCommand('ListBuckets');
$result = $command->getResult();
// Command - method 3
//$result = $client->getCommand('ListBuckets')->getResult();

// Result - method 1
print_array($result->get('Buckets'));
// Result - method 2
print_array($result[Buckets]);
// Result - method 3 (gets all results, don't have to define the key)
$result_array = NULL;
$keys = $result->getKeys();
foreach($keys as $key) {
	$result_array[$key] = $result->get($key);
}
print_array($result_array);

/*$command = $client->getCommand('ListBuckets');
$result = $command->getResult();
print "result 1:";
print_array($result);
print "result 1.1:";
print_array($result->getKeys());
print "result 1.2:";
print_array($result[Buckets]);

$result = $client->listBuckets();
print "result 2:";
print_array($result);
print "result 2.1:";
print_array($result[Buckets]);
print "result 2.2:";
print_array($result->get('Buckets'));

/*$url = "http://www.stickyfan.com/?a=verify_paypal";
parse_str("handling_amount=0.00&payer_id=C2N55SSMQNGSW&ipn_track_id=41b90e02941d0&shipping=0.00&charset=windows-1252&payment_gross=0.01&verify_sign=ABiNjEoUEin0zr7eI323Rgu7noV2AFGP65r0vqKCjolyqpR.eY-gNtYU&item_name=Order 2042&txn_type=web_accept&receiver_id=WHD7WD9TTS6EG&payment_fee=0.01&mc_currency=USD&transaction_subject=2042&custom=2042&protection_eligibility=Ineligible&payer_status=verified&first_name=Charles&mc_gross=0.01&payment_date=12:14:22 Mar 26, 2013 PDT&payment_status=Completed&quantity=1&business=adrian@stickyfan.com&item_number=&last_name=Konsor&txn_id=2EB39522676115746&mc_fee=0.01&resend=true&payment_type=instant&notify_version=3.7&payer_email=paypal@writerscafe.org&receiver_email=adrian@stickyfan.com&tax=0.00&residence_country=US",$post);
print_array($post);

include_function('curl');
$result = curl($url,array('post' => $post));
print "result: <xmp>".$result."</xmp>";

/*if($_POST) {
	$form = form::load();
	$results = $form->process(g('unpure.post'));
	
	print "
<b>Unpurified</b>
".return_array(g('unpure.post'))."<br />
<b>Purified</b>
".return_array($_POST)."<br />
<b>Form</b>
".return_array($results[values])."<br />";	
}

$form = form::load();
$form->text('Text','text',$results[values][text]);
$form->textarea('Textarea','textarea',$results[values][textarea]);
$form->textarea('Textarea (characters)','textarea_characters',$results[values][textarea_characters],array('purify' => array('characters' => 1)));
$form->input('CKEditor','ckeditor','ckeditor',$results[values][ckeditor]);
print $form->render();


print_array(get_html_translation_table(HTML_ENTITIES));

/*$core->db->q("DELETE FROM modules WHERE module_code = 'shipping'");
$core->db->q("ALTER TABLE `modules` AUTO_INCREMENT =36");
$core->db->q("DROP TABLE `shipping`");

/*$form = form::load();
$form->file('File','file',NULL,array('file' => array('type' => 'image','browse' => array('active' => 1),'path' => 'local/uploads/content/images/')));
print $form->render();

/*require_once SERVER."core/core/classes/file_browse.php";
$directory = "local/uploads/content/";
$browse = file_browse::load($directory);
print $browse->html();

/*$file = file::load(SERVER."local/uploads/test_mp4_2.mp4");
$c = array(
	'id' => 'video',
	'events' => array(
		'play' => 'video_play',
		'pause' => 'video_pause',
		'ended' => 'video_end',
	),
);
print "
<script type='text/javascript'>
$(document).ready(function() {
	$('#".$c[id]."').css({position:'relative'}).append(\"<div style='position:absolute;width:100%;top:30px;text-align:center;' id='video-buttons'><button>Button 1</button>&nbsp;&nbsp;&nbsp;<button>Button 2</button>&nbsp;&nbsp;&nbsp;<button>Button 3</button></div>\");
	//$('#".$c[id]."').bind('ended',function() {alert('ended -1');video_end();});
});
function video_play() {
	//alert('play');
	$('#video-buttons').hide();
}
function video_pause() {
	//alert('pause');
	$('#video-buttons').show();
}
function video_end() {
	//alert('ended 0');
	document.getElementById('video_html5_api').webkitExitFullScreen();
	$('#video-buttons').show();
}

/*window.onload = function() {
	document.getElementById('video_html5_api').addEventListener('ended',function() {alert('ended -1');video_end();},false);
});
function video_end() {
	alert('ended 0');
	document.getElementById('video_html5_api').webkitExitFullScreen();
	alert('ended 1');
	$('#".$c[id]."').css({position:'relative'}).append(\"<div style='position:absolute;width:100%;top:30px;text-align:center;'><button>Button 1</button>&nbsp;&nbsp;&nbsp;<button>Button 2</button>&nbsp;&nbsp;&nbsp;<button>Button 3</button></div>\");
}*/
/*</script>
".$file->video_html($c)."";

/*print "
<video id='video'>
    <source src='".D."local/uploads/test_mp4_2.mp4' type='video/mp4' />
</video>
<script type='text/javascript'>
/*$('#video').bind('ended',function(){
	alert('Video Ended');
	//$('#video').webkitExitFullScreen();
	document.getElementById('video').webkitExitFullScreen();
},false);*/
/*document.getElementById('video').addEventListener('ended',function(){document.getElementById('video').webkitExitFullScreen();},false);
</script>";

/*$c = array(
	'credit_cards'  => array(
		'addresses' => array(
			'same' => array(
				'Same as Shipping' => array(
					'address_country' => '234',
					'address_first_name' => 'Test',
					'address_last_name' => 'Shipping',
					'address_address' => '123 Test St',
					'address_city' => 'Portland',
					'address_state' => '3536',
					'address_zip' => '97005',
					'address_phone' => '555-555-5555',
					'address_email' => 'charlie@dwstudios.net',
				),
			)
		)
	),
);
/*$c = array(
	'addresses' => array(
		'same' => array(
			'Same as Shipping' => array(
				'address_country' => '234',
				'address_first_name' => 'Test',
				'address_last_name' => 'Shipping',
				'address_address' => '123 Test St',
				'address_city' => 'Portland',
				'address_state' => '3536',
				'address_zip' => '97005',
				'address_phone' => '555-555-5555',
				'address_email' => 'charlie@dwstudios.net',
			),
		)
	)
);*/
/*$form = form::load('payments','new',$c);
$form->hidden('payment_total',1.23);
//$form->hidden('payment_total_subtotal',1.23);
//$form = form::load('credit_cards','new',$c);
//$form = form::load('addresses','new',$c);
print $form->render();

/*
//$row = $core->db->f("SELECT *,AES_DECRYPT(card_number,'".g('config.db.mysql.encryption')."') number,AES_DECRYPT(card_code,'".g('config.db.mysql.encryption')."') code FROM credit_cards WHERE card_id = 5");
//print_array($row);

$item = item::load('credit_cards',5);

$row = $item->row;
print_array($row);

$decrypted = $item->v_decrypted(array('card_number','card_code'));
print_array($decrypted);

print "number: ".$item->v_decrypted('card_number')."<br />";
print "code: ".$item->v_decrypted('card_code')."<br />";

/*

/*$rows = $core->db->q("SELECT * FROM countries");
while($row = $core->db->f($rows)) {
	$core->db->q("UPDATE states SET country_id = '".$row[country_id]."' WHERE country_code = '".$row[country_code]."'");	
}

/*$country_id = 274;
$country_code = "SS";
$array = array(
	'01' => 'Bahr el Gabel',
	'02' => 'Eastern Equatoria',
	'03' => 'Jonglei State',
	'04' => 'El Buhayrat',
	'05' => 'Northern Bahr el Ghazal',
	'06' => 'Unity',
	'07' => 'Upper Nile',
	'08' => 'Warab State',
	'09' => 'Western Bahr el Ghazal',
	'10' => 'Equatoria',
);

$country_id = 166;
$cuntry_code = "SD";
$array = array(
	'29' => 'Khartoum State',	
	'36' => 'Red Sea State',	
	'38' => 'Al Jazirah State',	
	'39' => 'Al Qadarif State',	
	'41' => 'White Nile State',	
	'42' => 'Blue Nile State',	
	'43' => 'Northern State',	
	'47' => 'Western Darfur State',	
	'49' => 'Southern Darfur',	
	'50' => 'Southern Kordofan',	
	'52' => 'Kassala State',	
	'53' => 'River Nile State',	
	'55' => 'Northern Darfur',	
	'56' => 'Northern Kordofan State',	
	'58' => 'Sinnar State',	
	'60' => 'Eastern Darfur',	
	'61' => 'Central Darfur State',	
);

foreach($array as $k => $v) {
	$core->db->q("INSERT INTO states SET state_name = '".a($v)."', country_id = '".$country_id."', country_code = '".$country_code."', state_code' => '".$k."'");	
}

/*

// phpDocumentor
//$command = "phpdoc -h";
/*$command = "phpdoc -d ".SERVER."core/ -t ".SERVER."core/core/documentation/ --ignore ".SERVER."core/core/libraries/,".SERVER."core/core/classes/payment/googlecheckout/,".SERVER."local/uploads/ --force";
print $command."<br />";
$result = shell_exec(escapeshellcmd($command));
print_array($result);

/*if($_POST) {
	$form = form::load();
	$results = $form->process(g('unpure.post'));
	print_array($results);	
}

$form = form::load();
$form->email('E-mail','test_email');
$form->email('Multiple E-mails','test_emails',NULL,array('multiple' => 1));
$form->submit();
print $form->render();

/*

/*$form = form::load('payments','new');
print $form->render();

/*
// Values - GET
$method = ($_GET['method'] ? $_GET['method'] : "card");
$gateway = ($_GET['gateway'] ? $_GET['gateway'] : "authorize");
$action = ($_GET['action'] ? $_GET['action'] : "charge-expired");
$payment = ($_GET['payment'] ? $_GET['payment'] : 123);
$test = (x($_GET['test']) ? $_GET['test'] : 1);

// Values - Action
list($action,$action_issue) = explode('-',$action);

// Values - Test
if($test) {
	$amount = (x($_GET['amount']) ? $_GET['amount'] : "1.23");
	$card = array(
		'number' => ($_GET['card_number'] ? $_GET['card_number'] : '4111111111111111'),
		'expiration_month' => ($_GET['card_expiration_month'] ? $_GET['card_expiration_month'] : 4),
		'expiration_year' => ($_GET['card_expiration_year'] ? $_GET['card_expiration_year'] : 2013),
		'code' => '123'
	);
	$address = array(
		'first_name' => 'Test',
		'last_name' => 'Customer',
		'address' => '3811 SW Hall Blvd',
		'address_2' => 'Suite 123',
		'city' => 'Beaverton',
		'state' => 'OR',
		'zip' => '97005',
		'country' => 'US',
	);
}

// Values - Live
else {
	$amount = (x($_GET['amount']) ? $_GET['amount'] : "0.01");
	$card = array(
		'number' => ($_GET['card_number'] ? $_GET['card_number'] : ''),
		'expiration_month' => ($_GET['card_expiration_month'] ? $_GET['card_expiration_month'] : 7),
		'expiration_year' => ($_GET['card_expiration_year'] ? $_GET['card_expiration_year'] : 2014),
		'code' => '654'
	);
	$address = array(
		'first_name' => 'Charles',
		'last_name' => 'Konsor',
		'address' => '1565 SE Maple Ave',
		'address_2' => 'Apt A',
		'city' => 'Portland',
		'state' => 'OR',
		'zip' => '97214',
		'country' => 'US',
	);
}

// Valeus - Transaction
$transaction = ($_GET['transaction'] ? $_GET['transaction'] : NULL);

// Values - Issues
if($action_issue == 'expired') $card[expiration_year] = 2001;

// Values
$values = array(
	'amount' => $amount,
	'card' => $card,
	'address' => $address,
	'transaction' => $transaction,
	'payment' => array(
		'payment_id' => $payment
	),
);

// Results
if($method == "card") $results = payments_card($gateway,$action,$values,array('test' => $test,'debug' => 1));
if($method == "paypal") $results = payments_paypal($action,$values,array('test' => $test,'debug' => 1));
if($method == "googlecheckout") $results = payments_googlecheckout($action,$values,array('test' => $test,'debug' => 1));

/*$module = 'articles';
$id = 1;

$form = form::load($modules,$id,array('inputs' => 0));
$form->file('File','article_file');
for($x = 1;$x <= 10;$x++) {
	$form->text('<br />Translation Language','article_translation_'.$x.'_language',NULL,array('row' => array('style' => 'display:none;','class' => 'translation_row')));
	$form->file('Translation File','article_translation_'.$x.'_file',NULL,array('row' => array('style' => 'display:none;','class' => 'translation_row')));
	$form->text('Translation File Name','article_translation_'.$x.'_name',NULL,array('row' => array('style' => 'display:none;','class' => 'translation_row')));
}
$form->button('Add a translation',array('onclick' => "$('.translation_row:hidden:lt(3)').show();"));
print $form->render();

print "
<select onchange=\"if(this.options[this.selectedIndex].value) location.assign(DOMAIN+'core/core/download.php?file='+this.options[this.selectedIndex].value);\">";
for($x = 1;$x <= 20;$x++) {
	if($this->item_class->v('article_translation_'.$x.'_file') and $this->item_class->v('article_translation_'.$x.'_language')) {
		$file = $this->item_class->file('article_translation_'.$x.'_file');
		print "
	<option value='".$file->url()."'>".$this->item_class->v('article_translation_'.$x.'_language')."</option>";
	}
}
print "
</select>";

/*print "<a href='".DOMAIN."local/uploads/files/armsreach_wb.swf' class='overlay autoSize:true}'>view swf file</a>";


// phpDocumentor
//$command = "phpdoc -h";
/*$command = "phpdoc -d ".SERVER."core/ -t ".SERVER."core/core/documentation/ --ignore ".SERVER."core/core/libraries/ --force";
print $command."<br />";
$result = shell_exec(escapeshellcmd($command));
print_array($result);




/*$file = SERVER."core/core/includes/dawn.php";
print "file: ".$file.", owner: ".fileowner($file).", group: ".filegroup($file)."<br />";
$ftp_owner = fileowner($file);
$ftp_group = filegroup($file);

//$file = SERVER."local/cache/framework/db.txt";
$file = SERVER."local/test.txt";
//file_put_contents($file,'test');
print "file: ".$file.", owner: ".fileowner($file).", group: ".filegroup($file)."<br />";
chown($file,$ftp_owner);
print "file: ".$file.", owner: ".fileowner($file).", group: ".filegroup($file)."<br />";


/*$core->db->q("DELETE FROM newsletters WHERE newsletter_id = 1");

$form = form::load('newsletters','new');
print $form->render();

/*$modules = array('users');
foreach($modules as $module) {
	$m = module::load($module);
	$rows = $m->rows();
	while($row = $m->db->f($rows)) {
		$item = $m->item($row);
		$item->process(array('debug' => 1));
	}
}


/*date_default_timezone_set('America/Los_Angeles');
print date_default_timezone_get().", ".date('T');


/*print_array(m('pages'));

/*$rows0 = $core->db->q("SHOW TABLES");
while($row0 = $core->db->f($rows0,array('association' => MYSQL_BOTH))) {
	$table = $row0[0];
	
	// Get Primary Key
	$row = $core->db->f("SHOW CREATE TABLE `".$table."`",array('association' => MYSQL_BOTH));
	preg_match('/DEFAULT CHARSET=([a-zA-Z0-9]{1,50})/',$row[1],$result);
	//print $result[1]."<br />";
	if($result[1] != "utf8") {
		print $result[1]."<br />";
		print "ALTER TABLE `".$table."`  DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci<br />";
		$core->db->q("ALTER TABLE `".$table."`  DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci");
		$rows1 = $core->db->q("SHOW FULL COLUMNS FROM `".$table."`");
		while($row1 = $core->db->f($rows1)) {
			if($row1[Collation] and $row1[Collation] != "utf8_general_ci") {
				print "ALTER TABLE `".$table."` CHANGE ".$row1['Field']." ".$row1['Field']." ".$row1['Type']." CHARACTER SET utf8 COLLATE utf8_general_ci ".($row1['Null'] == "NO" ? "NOT " : '')."NULL".($row1['Default'] ? " DEFAULT ".$row1['Default'] : '')."<br />";
				$core->db->q("ALTER TABLE `".$table."` CHANGE ".$row1['Field']." ".$row1['Field']." ".$row1['Type']." CHARACTER SET utf8 COLLATE utf8_general_ci ".($row1['Null'] == "NO" ? "NOT " : '')."NULL".($row1['Default'] ? " DEFAULT ".$row1['Default'] : ''));
			}
			//print_array($row1);
		}
		
		// ALTER TABLE `countries` CHANGE `country` `country` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL , CHANGE `country_code` `country_code` CHAR( 3 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL , CHANGE `country_continent` `country_continent` CHAR( 2 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL 
	}
}

// FFMPEG
/*$file = file::load(SERVER."local/uploads/test_mov_1.mov");
$file_new = $file->video_convert("mp4",array('debug' => 1));

print "video:";
$file = file::load($file_new);
print $file->video_html();

/*print_array(m('news.settings'));

/*

/*$file = file::load(SERVER."local/uploads/test_mp4_2.mp4");
$html = $file->video_html(array('fallback' => array(
	//SERVER."local/uploads/test_mp4_1.mp4",
	//SERVER."local/uploads/test_webm.webm",
	//SERVER."local/uploads/big_buck_bunny.ogv",
),'debug' => 1));
print $html;*/


/*

/*$modules = array('galleries_media');
foreach($modules as $module) {
	$m = module::load($module);
	$rows = $m->rows();
	while($row = $m->db->f($rows)) {
		$item = $m->item($row);
		$item->process(array('debug' => 1));
	}
}


// Item
/*$item = item::load('modules',$_GET['id']);
$item->delete(array('debug' => 1));

/*$db = new db();
$modules = array('articles' => 'article','news' => 'news','pages' => 'page','carousels' => 'carousel','carousels_slides' => 'slide','events' => 'event','faq' => 'faq','galleries' => 'gallery','galleries_media' => 'media');
foreach($modules as $module => $key) {
	$db->q("ALTER TABLE `".m($module.'.db.table')."` DROP `".$key."_published`");
	$db->q("ALTER TABLE `".m($module.'.db.table')."` DROP `".$key."_published_start`");
	$db->q("ALTER TABLE `".m($module.'.db.table')."` DROP `".$key."_published_end`");
	
	
	/*$m = module::load($module);
	$rows = $m->rows();
	while($row = $m->db->f($rows)) {
		$query = "INSERT INTO items_published SET module_code = '".$module."', item_id = '".$row[m($module.'.db.id')]."', published = '".$row[$key."_published"]."', published_start = '".$row[$key."_published_start"]."', published_end = '".$row[$key."_published_end"]."'";
		print $query."<br />";
		$m->db->q($query);
	}*/
	
/*}

/*foreach(m() as $module => $v) {
	$m = module::load($module);
	$rows = $m->rows();
	while($row = $m->db->f($rows)) {
		$item = $m->item($row);
		$item->process(array('debug' => 1));
	}
}

/*$module = 'galleries_media';
$m = module::load($module);
$rows = $m->rows();
while($row = $m->db->f($rows)) {
	$item = $m->item($row);
	$item->process(array('debug' => 1));
}

	//print_array(m('galleries_media.public'));

/*print "<br /><a href='http://www.reddit.com' class='overlay' title='Test Title'>Click for overlay</a><br /><br />";

print "<br /><a href='".D."local/uploads/images/o/l92abc8ny6gm2y3q.jpg' class='overlay' title='Test Title'>Click for overlay</a><br /><br />";


/*$file = "test.jpg";
print file::extension_standardized($file)."<br />";

/*if($_FILES) {
	print_array($_FILES);
	
	$file = new file($_FILES['test'],array('debug' => 0));
	print "name: ".$file->name."<br />";
	$file->process(SERVER."local/uploads/");
	
	print "<img src='".DOMAIN."local/uploads/".$file->name."?".time()."' /><br />";
}


/*print permission('contact','manage');

//print m('settings.settings.site.meta.title');

/*$url = DOMAIN."?test=1&__form=abc&four=five";
print url_query_append($url,"__form=123")."<br />";
$url = DOMAIN."?__form=abc&four=five";
print url_query_append($url,"__form=123")."<br />";
$url = DOMAIN."?test=1&__form=abc";
print url_query_append($url,"__form=123")."<br />";
$url = DOMAIN."?__form=abc";
print url_query_append($url,"__form=123")."<br />";

// Reprocess
/*$module = 'users';
$m = module::load($module);
$rows = $m->rows();
while($row = $db->f($rows)) {
	$item = item::load($module,$row);
	$item->process(array('debug' => 1));
}

print "<br /><a href='http://www.reddit.com' class='overlay' title='Test Title'>Click for overlay</a><br /><br />";

/*$db = new db();
$db->table_column('users','user_name');
$db->table_column('users','user_created');

/*print "<a href='#' class='tooltip' title='".string_encode("Here's a tooltip")."'>Hover for tooltip</a><br /><br />";
print "<p class='tooltip' title='".string_encode("Another tooltip<br />with a break")."'>Hover for a secondtooltip</p><br /><br />";

//search_index('pages',array('debug' => 1));

/*$item = item::load('pages',15);
$item->process_index(array('debug' => 1));

// Reprocess
/*$module = 'pages';
$db = new db();
$rows = $db->get($module);
while($row = $db->f($rows)) {
	$item = item::load($module,$row);
	$item->process(array('debug' => 1));
}

/*$tries = 100;

$start = microtime_float();
for($x = 1;$x <= $tries;$x++) {
	global $__GLOBAL;
	$__GLOBAL['test']['a']['b']['c'] = 123;	
}
print "method 1-set took ".round(microtime_float() - $start,5)." seconds<br />";

$start = microtime_float();
for($x = 1;$x <= $tries;$x++) {
	global $__GLOBAL;
	$value = $__GLOBAL['test']['a']['b']['c'];	
}
print "method 1-get took ".round(microtime_float() - $start,5)." seconds<br />";

$start = microtime_float();
for($x = 1;$x <= $tries;$x++) {
	g('test.a.b.c',123);	
}
print "method 2-set took ".round(microtime_float() - $start,5)." seconds<br />";

$start = microtime_float();
for($x = 1;$x <= $tries;$x++) {
	$value = g('test.a.b.c');	
}
print "method 2-get took ".round(microtime_float() - $start,5)." seconds<br />";

// Modules
/*$modules = array(
	'users' => array(
		'module_name' => 'Users',
		'module_settings' => array(
			array(
				'name' => 'name',
				'label' => 'Name',
				'required' => 1,
			),
			array(
				'name' => 'single',
				'label' => 'Singular',
				'required' => 1,
			),
			array(
				'name' => 'plural',
				'label' => 'Plural',
				'required' => 1,
			),
		),
		'module_core' => 1
	),

// Set table's and columns to UTF-8
/*$db = new db();
$rows = $db->q("SHOW TABLES");
while($row = $db->f($rows,array('association' => MYSQL_BOTH))) {
	$table = $row[0];
	
	// Get Primary Key
	$qry = $db->f("SHOW CREATE TABLE ".$table);
	preg_match('/DEFAULT CHARSET=([a-zA-Z0-9]{1,50})/',$qry[1],$result);
	//print $result[1]."<br />";
	if($result[1] != "utf8") {
		print $result[1]."<br />";
		print "ALTER TABLE ".$table."  DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci<br />";
		$db->q("ALTER TABLE ".$table."  DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci");
		$sql1 = $db->q("SHOW FULL COLUMNS FROM ".$table);
		while($qry1 = $db->f($sql1)) {
			if($qry1[Collation] and $qry1[Collation] != "utf8_general_ci") {
				print "ALTER TABLE ".$table." CHANGE ".$qry1['Field']." ".$qry1['Field']." ".$qry1['Type']." CHARACTER SET utf8 COLLATE utf8_general_ci ".($qry1['Null'] == "NO" ? "NOT " : '')."NULL".($qry1['Default'] ? " DEFAULT ".$qry1['Default'] : '')."<br />";
				$db->q("ALTER TABLE ".$table." CHANGE ".$qry1['Field']." ".$qry1['Field']." ".$qry1['Type']." CHARACTER SET utf8 COLLATE utf8_general_ci ".($qry1['Null'] == "NO" ? "NOT " : '')."NULL".($qry1['Default'] ? " DEFAULT ".$qry1['Default'] : ''));
			}
			//print_array($qry1);
		}
		
		// ALTER TABLE `countries` CHANGE `country` `country` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL , CHANGE `country_code` `country_code` CHAR( 3 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL , CHANGE `country_continent` `country_continent` CHAR( 2 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL 
	}
}

/************************************************************************************/
/*************************************** Dusk ***************************************/
/************************************************************************************/
// Footer
print "
</body>
".$page->footer();

/** Dusk - unloads core functionality. */
require_once "../core/core/includes/dusk.php";
?>