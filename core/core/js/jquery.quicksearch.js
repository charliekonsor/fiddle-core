/**
 * Filters items based on search.
 *
 * Note, I updated original code with all the 'options' you see (children, placeholder, etc.).
 * See below for original code.
 *
 * http://ejohn.org/blog/jquery-livesearch/	
 * https://github.com/vanntastic/jquery.liveupdate	
 */
$.fn.liveUpdate = function(parent,options){
	// Defaults
	var defaults = {
		children: 'li', // Element type of the child elements we want to filter (usually 'option' or 'li')
		placeholder: '', // Placeholder text (don't search if this is the q value)
		html: 0, // Include HTML when filtering text (as opposed to using just the plain text)
		quicksilver: 0, // Use quicksilver to score/filter (as opposed to basic indexOf filtering)
		nested: 1 // Allow for nested elements (ex: <ul><li><ul><li>), if 0 we hide nested elements when searching (only supported if children:'option')
	}
	// Passed options as first paramater
	if(typeof(parent) == 'object') {
		options = parent;
		parent = options.parent;
	}
	
	// Parent
	parent = $(parent);
	// Options
	options = $.extend(defaults,options);
	
	if(parent.length) {
		var rows = parent.children(options.children),
		cache = rows.map(function(){
			if(options.html) return this.innerHTML.toLowerCase();
			else return $(this).text().toLowerCase();
		});
		 
		this
			.keyup(filter).keyup()
			.parents('form').submit(function(){
				return false;
			});
	}
	return this;
	 
	function filter(){
		var q = $(this).val();
		var term = $.trim(q.toLowerCase()), scores = [];
		
		// No search term (or term == placeholder text), don't filter
		if(!term || q == options.placeholder) {
			rows.show();
			if(options.children == "option" && options.nested != 1) $('optgroup',parent).show();
		}
		// Have term, filter
		else {
			// Hide all rows
			if(options.children == "option" && options.nested != 1) $('optgroup',parent).hide();
			rows.hide();

			// Quicksilver Filter
			if(options.quicksilver) {
				cache.each(function(i){
					var score = this.score(term);
					if (score > 0) { scores.push([score, i]); }
				});
	
				$.each(scores.sort(function(a, b){return b[0] - a[0];}), function(){
					$(rows[ this[1] ]).show();
				});
			}
			// Basic Filter
			else {
				cache.each(function(i){
					if(this.indexOf(term) != -1) $(rows[i]).show();
				});
			}
		}
	}
};

/* Original Code 
jQuery.fn.liveUpdate = function(list){
  list = jQuery(list);

  if ( list.length ) {
    var rows = list.children('li'),
      cache = rows.map(function(){
        return this.innerHTML.toLowerCase();
      });
     
    this
      .keyup(filter).keyup()
      .parents('form').submit(function(){
        return false;
      });
  }
   
  return this;
   
  function filter(){
    var term = jQuery.trim( jQuery(this).val().toLowerCase() ), scores = [];
   
    if ( !term ) {
      rows.show();
    } else {
      rows.hide();

      cache.each(function(i){
        var score = this.score(term);
        if (score > 0) { scores.push([score, i]); }
      });

      jQuery.each(scores.sort(function(a, b){return b[0] - a[0];}), function(){
        jQuery(rows[ this[1] ]).show();
      });
    }
  }
};*/