<?php
if(!class_exists('file_browse',false)) {
	/**
	 * An extension of the file class which adds functionality for 'browsing' a directory.
	 *
	 * Dependencies:
	 * - x()
	 *
	 * @package kraken\files
	 */
	class file_browse extends file {
		/** Stores the directory we're browsing. */
		var $directory;
		
		/**
		 * Loads and returns an instance of the file_browse class.
		 *
		 * Example:
		 * - $browse = file_browse::load($file,$c);
		 *
		 * @param string $directory The path to the directory you will be browsing.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return object An instance of the class.
		 */
		static function load($directory = NULL,$c = NULL) {
			// Class name
			$class = __CLASS__;
			
			// Class
			return new $class($directory,$c);
		}
		
		/**
		 * Constructs the class.
		 *
		 * @param string $directory The path to the directory you will be browsing.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function __construct($directory = NULL,$c = NULL) {
			self::file_browse($directory,$c);
		}
		function file_browse($directory = NULL,$c = NULL) {
			// Error
			if(!$directory) return;
			
			// Parent
			parent::__construct();
			
			// Directory
			if(substr($directory,-1) != "/") $directory .= "/";
			$this->directory = $this->localize($directory);
		}
		
		/**
		 * Returns an array of items in this directory.
		 *
		 * @param string $directory The directory you want to get items from. Default = $this->directory
		 * @param array $c An array of configuration values.
		 * @return array An array of items in this directory.
		 */
		/*function items($directory = NULL,$c = NULL) {
			// Default - $browse->items($c);
			if(!$directory or is_array($directory)) {
				$c = $directory;
				$directory = $this->directory;
			}
			// Error
			if(!$directory) return;
			
			// Localize
			$directory = $this->localize($directory);
			
			// Config
			if(!x($c[files])) $c[files] = 1; // Get files in the root directory.
			if(!x($c[directories])) $c[directories] = 1; // Get directories in the root directory.
			if(!x($c[recursive])) $c[recursive] = 0; // Get items from sub-directories as well as the items in the root directory.
			if(!x($c[prefix])) $c[prefix] = NULL; // A prefix to append to each item path (used when we're looking through sub-directories in 'recursive' mode).
			if(!$c[paths]) $c[paths] = 'relative'; // Do you want the file names returned to be 'relative' to the directory or 'full' (meaning, include the parent directory's full path)?
				
			// Variables
			$array = array();
			if($directory and substr($directory,-1) != "/") $directory .= "/"; // Make sure we have a trailing slash
			if($c[paths] == "full") $c[prefix] = $directory.$c[prefix];
				
			// Open directory
			if($handle = opendir($directory)) {
				// Get entries
				while(false !== ($entry = readdir($handle))) {
					// Skip navigation entries (up one level, etc.)
					if(!in_array($entry,array(".",".."))) {
						// File
						if($c[files]) {
							// Entry is a file (and not a system file), add to array
							if(is_file($directory.$entry) and substr($entry,0,1) != ".") {
								$array[] = $c[prefix].$entry;
							}
						}
						// Directory
						if($c[directories] or $c[recursive]) {
							// Entry is a directory, add to array
							if(is_dir($directory.$entry)) {
								if($c[directories]) $array[] = $c[prefix].$entry;
								
								// Look for items inside (recursive)
								if($c[recursive] == 1) {
									$_c = $c;
									$_c[prefix] .= $entry."/";
									$sub_array = $this->directory_items($directory.$entry."/",$_c);
									if($sub_array) $array = array_merge($array,$sub_array);
								}
							}
						}
					}
				}
			}
			
			// Return
			return $array;
		}
		
		/**
		 * Returns an array of files in this directory.
		 *
		 * @param string $directory The directory you want to get files from. Default = $this->directory
		 * @param array $c An array of configuration values.
		 * @return array An array of files in this directory.
		 */
		/*function files($directory = NULL,$c = NULL) {
			// Default - $browse->files($c);
			if(!$directory or is_array($directory)) {
				$c = $directory;
				$directory = $this->directory;
			}
			// Error
			if(!$directory) return;
			
			// Localize
			$directory = $this->localize($directory);
			
			// Config
			$c[files] = 1;
			$c[directories] = 0;
			
			// Items
			$items = $this->items($directory,$c);
			
			// Sort
			asort($items);
			
			// Return
			return $items;
		}
		
		/**
		 * Returns an array of directories in this directory.
		 *
		 * @param string $directory The directory you want to get directories from. Default = $this->directory
		 * @param array $c An array of configuration values.
		 * @return array An array of directories in this directory.
		 */
		/*function directories($directory = NULL,$c = NULL) {
			// Default - $browse->directories($c);
			if(!$directory or is_array($directory)) {
				$c = $directory;
				$directory = $this->directory;
			}
			// Error
			if(!$directory) return;
			
			// Localize
			$directory = $this->localize($directory);
			
			// Config
			$c[files] = 0;
			$c[directories] = 1;
			
			// Items
			$items = $this->items($directory,$c);
			
			// Return
			return $items;
		}
		
		/**
		 * Returns HTML for displaying file browser.
		 *
		 * @param array $c An array of configuration values.
		 * @return string The HTML for file browsing.
		 */
		function html($c = NULL) {
			// Config
			if(!$c[url_directory]) $c[url_directory] = D."?ajax_action=file_browse_files";
			if(!$c[url_file]) $c[url_file] = D."?ajax_action=file_browse_file";
			if(!$c[url_delete]) $c[url_delete] = D."?ajax_action=file_browse_delete";
			if(!$c[preview_path]) $c[preview_path] = NULL; // The full path to where a preview version of this file will be (only works on top level I guess...unless child directories store their thumbs in this directory as well)
			if(!$c[row]) $c[row] = NULL; // jQuery selector of input row, used for positioning preview and removing required
			
	
			// HTML
			$html = "
<script type='text/javascript'>
function file_browse_directory(directory) {
	loader('.file-browse-files');
	$.ajax({
		type: 'GET',
		url: '".$c[url_directory].(strstr($c[url_directory],'?') ? "&" : "?")."directory='+encodeURIComponent(directory),
		success: function(result) {
			$('.file-browse-files').html(result);
		}
	});
}
function file_browse_file(file) {";
			if($_GET['CKEditorFuncNum']) {
				$html .= "
	window.opener.CKEDITOR.tools.callFunction(".$_GET['CKEditorFuncNum'].",str_replace(SERVER,DOMAIN,file));
	window.close();";
			}
			else {
				$html .= "
	loader('.file-browse');
	$.ajax({
		type: 'GET',
		url: '".$c[url_file].(strstr($c[url_file],'?') ? "&" : "?")."file='+encodeURIComponent(file),
		success: function(result) {
			$('".$c[row]." .form-browse-preview').html(result);
			$('".$c[row]." .required:input').removeClass('required');
			
			overlay_close();
			js_refresh();
		}
	});";
			}
			$html .= "
}
function file_browse_delete(event,file,selector,used) {
	if(confirm('Are you sure you want to delete this file?'+(used ? ' NOTE: This file is associated with an active '+used+'.' : ''))) {
		$(selector).fadeOut(500,function() {
			$(selector).remove();
		});
		
		$.ajax({
			type: 'GET',
			url: '".$c[url_delete].(strstr($c[url_delete],'?') ? "&" : "?")."file='+encodeURIComponent(file),
			success: function(result) {
				//alert(result);	
			}
		});
	}
	
	event.stopPropagation();
	event.cancelBubble = true;
}
</script>
<style>
.file-browse {
	width:100%;
}
.file-browse td {
	padding:10px;
	overflow:auto;
	vertical-align:top;
}

.file-browse-directories {
	width:120px;
	border-right:1px solid #ccc;
}

.file-browse-files ul {
	padding:0px;
	margin:0px;
}

.file-browse-files li {
	padding:10px;
	list-style:none;
	width:138px;
	height:140px;
	float:left;
	color:#333;
	position:relative;
	cursor:pointer;
	opacity:.80;
	filter:alpha(opacity=80); 
	-moz-opacity:0.8;
}
.file-browse-files li:hover {
	opacity:1;
	filter:alpha(opacity=100); 
	-moz-opacity:1;
	background:#f2f2f2;
}

.file-browse-files-file-delete {
	display:none;
	position:absolute;
	top:5px;
	right:5px;	
}
.file-browse-files li:hover .file-browse-files-file-delete {
	display:block;	
}

.file-browse-files-file-preview {
	width:100px;
	height:100px;
	margin:0px auto;
}
.file-browse-files-file-preview td {
	vertical-align:middle;
	text-align:center;
	padding:0px;
	height:auto;
}
.file-browse-files-file-name {
	display:block;
	margin:5px 0px;
	font-size:11px;
	line-height:12px;
	text-align:center;
	word-break:break-all;
}
</style>
<table class='file-browse'>
	<tr>";
			$directories = $this->directory_folders($this->directory);
			if($directories) $html .= "
		<td class='file-browse-directories'>
			<ul class='file-browse-directories-level file-browse-directories-level-top'>
				<li class='file-browse-directories-directory file-browse-directories-directory-children'>
					<a href='javascript:void(0);' onclick=\"file_browse_directory('".url_escape($this->directory,1)."');\">".basename($this->directory)."</a>
					".$this->html_directories($c)."
				</li>
			</ul>
		</td>";
			$html .= "
		<td class='file-browse-files'>
			".$this->html_files($c)."
		</td>
	</tr>
</table>";

			// Return
			return $html;
		}
		
		/**
		 * Returns HTML for displaying navigable directories.
		 *
		 * @param array $c An array of configuration values.
		 * @return string The HTML for displaying navigable directories.
		 */
		function html_directories($c = NULL) {
			// Config
			if(!x($c[child])) $c[child] = NULL; // Child folder(s) we want to show (used in recursive loop)
			
			// Directories
			$directories = $this->directory_folders($this->directory.$c[child]);
			if($directories) {
				$html .= "
			<ul class='file-browse-directories-level'>";
				foreach($directories as $directory) {
					$children = $this->html_directories(array('child' => $c[child].$directory."/"));
					$html .= "
				<li class='file-browse-directories-directory".($children ? " file-browse-directories-directory-children" : "")."'>
					<a href='javascript:void(0);' onclick=\"file_browse_directory('".url_escape($this->directory.$c[child].$directory."/",1)."');\">".$directory."</a>
					".$children."
				</li>";
				}
				$html .= "
			</ul>";
			}
			else if(!$c[child]) {
				$html .= "
			<div class='file-browse-none'>No Directories Found</div>";
			}

			// Return
			return $html;
		}
		
		/**
		 * Returns HTML for displaying files in a directory.
		 *
		 * @param string $directory The directory you want to get files from. Default = $this->directory
		 * @param array $c An array of configuration values.
		 * @return string The HTML for displaying navigable directories.
		 */
		function html_files($directory = NULL,$c = NULL) {
			// Default - $browse->html_files($c);
			if(!$directory or is_array($directory)) {
				$c = $directory;
				$directory = $this->directory;
			}
			// Error
			if(!$directory) return;
			
			// Localize
			$directory = $this->localize($directory);
			
			// Database // Should maybe make file_browse_framework class to hold this
			$db = db::load();
			
			// Directories
			$files = $this->directory_files($directory);
			if($files) {
				asort($files);
				$html .= "
			<ul>";
				foreach($files as $x => $file) {
					$html .= "
				<li class='file-browse-files-file' onclick=\"file_browse_file('".url_escape($directory.$file,1)."');\" id='file-browse-files-file-".$x."'>";
					// Delete
					if(u('admin')) {
						$row = $db->f("SELECT * FROM items_files WHERE file_path = '".a(str_replace(SERVER,'',$directory))."' AND file_name = '".a($file)."'"); // Should maybe make file_browse_framework class to hold this
						$html .= "
					<a href='javascript:void(0);' onclick=\"var event = arguments[0] || window.event;file_browse_delete(event,'".url_escape($directory.$file,1)."','#file-browse-files-file-".$x."'".($row[module_code] ? ",'".strtolower(m($row[module_code].'.single'))."'" : "").");\" class='i i-clear file-browse-files-file-delete'></a>";
					}
					
					// Preview
					$preview = NULL;
					$type = $this->type($file);
					if($type == "image") {
						if($c[preview_path]) $preview = $this->url($c[preview_path].$file);
						if(!$preview or !$this->exists($preview)) $preview = $this->url($directory.$file);
					}
					if(!$this->exists($preview)) {
						$extension = $this->extension($file);
						$preview = self::domain()."core/core/images/files/".$extension.".png";
					}
					if(!$this->exists($preview)) $preview = self::domain()."core/core/images/files/_blank.png";
					$preview_file = file::load($preview);
					$html .= "
					<table class='file-browse-files-file-preview'>
						<tr>
							<td>";
					if($preview_file->exists) {
						$html .= "
								".$preview_file->image_html(array('max_width' => 100,'max_height' => 100,'alt' => string_encode($file)));
					}
					$html .= "
							</td>
						</tr>
					</table>";
						
					// Name
					$html .= "
					<div class='file-browse-files-file-name'>
						".$file."
					</div>";
						
					$html .= "
				</li>";
				}
				$html .= "
			</ul>";
			}
			else {
				$html .= "
			<div class='file-browse-none'>No Files Found</div>";
			}

			// Return
			return $html;
		}
	}
}
?>