<?php
if(!class_exists('form_input_file',false)) {
	class form_input_file extends form_input_framework {
		/**
		 * Loads class. Needed so we can use debug_backtrace() to get this class name in PHP < 5.3 in $form_input->load().
		 */
		static function load($label = NULL,$name = NULL,$value = NULL,$c = NULL) {
			return parent::load($label,$name,$value,$c);
		}
		
		/**
		 * Construct
		 */
		function __construct($label = NULL,$name = NULL,$value = NULL,$c = NULL) {
			$this->form_input_file($label,$name,$value,$c);
		}
		function form_input_file($label = NULL,$name = NULL,$value = NULL,$c = NULL) {
			// Legacy - used to be $c[file]
			if($c[file]) {
				foreach($c[file] as $k => $v) $c[$k] = $v;
			}		
			
			// Construct
			parent::__construct($label,$name,$value,$c);
			
			// Config
			if(!x($this->c[preview])) $this->c[preview] = 1; // Whether or not we want to show preview of existing file on file inputs.
			
			// Attributes
			if(!$this->attributes[size]) $this->attributes[size] = 20; // Size attributes of file input
			if(!strstr($this->attributes[onchange],'file_clear_button')) $this->attributes[onchange] .= "file_clear_button(this);"; // Add's functionality for clearning file after selecting one.
			
			// Timeout settings (1 Hour)
			ini_set('max_execution_time',3600);
			set_time_limit(3600);
			// File size settings
			ini_set('upload_max_filesize','500M');
			ini_set('post_max_size','500M');
		}
		
		/**
		 * Renders the HTML for the input's 'row'.
		 *
		 * @param object $form The form object this input is a part of.
		 * @param array $c An array of config values. Default = NULL
		 * @return string The HTML of the input's row.
		 */
		function html_row($form,$c = NULL) {
			// Attributes - do here because we need to have run $form_input_file_framework->html_row() already (as that sets the default path)
			if(!x($this->c[overwrite])) $this->c[overwrite] = 0; // How do we want to handle files if another file with the same name already exists: check (ask user what they want to do) [default], 1 (overwrite), 0 (don't overwrite, rename instead)
			if($this->c[overwrite] === "check" and $this->c[path] and !strstr($this->attributes[onchange],'file_overwrite_check')) {
				if(!$this->attributes[id]) $this->attributes[id] = "input-file-".random();
				//$this->attributes[onchange] .= "file_overwrite_check({id:'".$this->attributes[id]."',path:'".$this->c[path]."'});"; // Checks if the file already exists and asks the user if they want to overwrite it or not // Using fancy js now, see below
				$html .= include_css('jquery.fancyapps.fancybox'); // Needed for modal if file exists
				$html .= include_javascript('jquery.fancyapps.fancybox');
				// Fancy javascript to make wure we only show this once. IE7 thinks blur (when we click outside the input) is another 'change' and will fire this again. Blur now too because IE7 also doesn't register the click when bluring so they'd have to click twice.
				$html .= "
<script type='text/javascript'>
$(document).ready(function() {
	$('#".$this->attributes[id]."').click(function(){
		 $(this).on( 
			  'change',
			  function(){
				$('#".$this->attributes[id]."').blur();
				file_overwrite_check({id:'".$this->attributes[id]."',path:'".$this->c[path]."'});
			  }
		 )
	});
});
</script>";
			}
			
			// Validate - required - remove required (but add classes indicating it should be required)
			if($this->value and $this->validate[required]) {
				$this->validate[required] = 0;
				$this->attributes['class'] .= ($this->attributes['class'] ? " " : "")."required-input required-icon";
			}
			
			// Config
			if(!$this->c[old]) $this->c[old] = $this->value; // Previous value
			
			// Parent
			$html .= parent::html_row($form,$c);
			
			// Return
			return $html;
		}
			
		
		/**
		 * Renders the HTML for the input's 'field'.
		 *
		 * @param object $form The form object this input is a part of.
		 * @param array $c An array of config values. Default = NULL
		 * @return string The HTML of the input's field.
		 */
		function html_field($form,$c = NULL) {
			// Input
			$html .= "
				".$this->html_element($form,$c);
					
			// Prepend
			if($this->prepend) $html = $this->prepend.$html;
			
			// Append
			if($this->append) $html .= $this->append;
		
			// Help
			if($this->help) $html .= help($this->help);
				
			// Error
			if($this->error) $html .= "
				<label class='error form-error' for='".$this->name."' style='display:inline;'>".$this->error."</label>";
				
			// Preview
			if($this->c[preview] and $this->value and $this->c[path]) {
				$html .= $this->preview($form,$c);
			}
			
			// Return
			return $html;
		}
			
		/**
		 * Processes the value for this input.
		 *
		 * @param object $form The form object which contains the submitted values ($form->post and $form->files).
		 * @param array $c An array of configuration values. Default = NULL
		 * @return mixed The resulting value.
		 */
		function process($form,$c = NULL) {
			// Config
			if($c) $this->c = array_merge($this->c,$c);
			
			// Delete
			$delete = 0;
			
			// Value
			$value = $this->value($form);
			
			// Not an uploaded file array, means it was already uploaded
			if(!x($value[name])) {
				$value = form::value($form->post,$this->name); // Value stored in POST (in case we upladed it via AJAX)
				if(!$value) $value = $this->value_temp; // Temp value (there was error when previously uploaded file, file was saved in input's 'temp' config though)
				if($value) {
					if(!strstr($value,'/')) $value = $this->c[path].$value; // Add path (if not already in the value, if getting file via 'file browser' the value contains the path too)
					$this->c[overwrite] = 1; // File uploaded via AJAX, overwrite that file when we save it now.
				}
			}
			
			// Value - debug
			debug("\$".__CLASS__."->".__FUNCTION__."() value (before, file). name: ".$this->name.", value: ".return_array($value),$this->c[debug]);
			
			// Original value
			$value_original = $value;
			
			// Upload (or already uploaded and have resulting value)
			debug("path: ".$this->c[path],$this->c[debug]);
			if($this->c[path] and ($value[name] or !is_array($value) and $value)) {
				// Overwrite?
				$overwrite = $this->c[overwrite];
				debug("overwrite (0): ".$overwrite,$c[debug]);
				if($overwrite == "check") {
					if(is_array($value) and x($_SESSION['__file_overwrite'][$this->attributes[id]][$value[name]])) {
						$overwrite = $_SESSION['__file_overwrite'][$this->attributes[id]][$value[name]];
					}
					else $overwrite = 0;
				}
				debug("overwrite (1): ".$overwrite,$c[debug]);
				
				// Advanced
				if(class_exists('file')) {
					// Process / upload
					$f = new file($value,array('overwrite' => $overwrite,'debug' => $this->c[debug]));
					
					// Error uploading
					if(!$f->exists) {
						$form->errors[header][$this->name] = "There was an error uploading ".($this->label[value] ? "the ".$this->label[value] : "a")." file.";
						$form->errors[inline][$this->name] = "There was an error uploading this file.";
						return;
					}
					
					// Resize
					if($this->c[width] and $this->c[height] and $f->type == "image") {
						$f->image_thumb($this->c[width],$this->c[height],array('crop' => $this->c[crop],'enlarge' => $this->c[enlarge]));
					}
					
					// Name
					$name = NULL; // Null means it'll use original name
					if($this->c[hash]) $name = string_random(16).".".$f->extension_standardized;
					
					// Path - we'll do this in the file class too, but need here in case we're passing $name
					if(substr($this->c[path],-1) != "/") $this->c[path] .= "/";
					
					// Save
					if($f_saved = $f->save($this->c[path].$name,array('smush' => $this->c[smush]))) {
						$f = $f_saved;
						// Value
						$value = $f->name;
						
						// Process
						$form->values_files[$this->name] = file_process($f->file,$this->c[type],$this->c);
						$value = $form->values_files[$this->name][name]; // In case name changed in file_process()
						debug("file values: ".return_array($form->values_files[$this->name]),$this->c[debug]);
						
						// Store
						/*$form->values_files[$this->name]['new'] = 1;
						$form->values_files[$this->name][name] = $f->name;
						$form->values_files[$this->name][type] = $f->type;
						$form->values_files[$this->name][extension] = $f->extension;
						$form->values_files[$this->name][path] = $this->c[path]; // Should maybe use the actual path (see below) in case we ever get a different file via the file browser) but old files only used the relative path (not full server path). Also, would have to update if we change servers and is just more to store.
						//$form->values_files[$this->name][path] = $f->path;
						$form->values_files[$this->name][width] = $f->width();
						$form->values_files[$this->name][height] = $f->height();
						$form->values_files[$this->name][length] = $f->length();
						$form->values_files[$this->name][size] = $f->size();
						
						// Quickstart
						if($f->type == "video" and $f->extension_standardized == "mp4") {
							$f->video_faststart(array('debug' => $this->c[debug]));
						}
						
						// Extensions
						if($this->c[extensions]) {
							// Unstore, we'll recreate in after conversion
							$form->values_files[$this->name][extensions] = NULL;
							
							// Debug
							debug("Converting file to other extensions:".return_array($this->c[extensions]),$this->c[debug]);
							
							// File
							$f = new file($this->c[path].$value,array('debug' => $this->c[debug]));
							
							// Extensions - video
							if($f->type == "video") {
								// Extensions
								foreach($this->c[extensions] as $extension => $v) {
									$converted = 0;
									
									// Already converted - might have selected a previously uploaded file via the file browser
									$name = str_replace(".".$f->extension,".".$extension,$f->name);
									$f_convert = new file($v[path].$name);
									if($f_convert->exists and !is_array($value_original)) {
										debug("already convereted, not re-converting",$this->c[debug]);
										$converted = 1;
									}
									// Convert
									else {
										$f_convert = $f;
										$f_convert->video_convert($extension,array('path' => $v[path],'debug' => $this->c[debug]));
										if($f_convert->exists) {
											$converted = 1;
										}
									}
									
									// Store
									if($converted) {
										$v[name] = $f_convert->name;
										$v[extension] = $f_convert->extension;
										$v[width] = $f_convert->width();
										$v[height] = $f_convert->height();
										$v[length] = $f_convert->length();
										$v[size] = $f_convert->size();
										$form->values_files[$this->name][extensions][$extension] = $v;
									}
								}
							}
						}
						
						// Thumbs
						if($this->c[thumbs]) {
							// Defaults
							foreach($this->c[thumbs] as $thumb => $v) {
								if(!$v[count]) $this->c[thumbs][$thumb][count] = 1;
								if(!$v[extension])  $this->c[thumbs][$thumb][extension] = ($f->type == "video" ? "jpg" : $f->extension);	
							}
							
							// Unstore, we'll recreate in after thumbing
							$form->values_files[$this->name][thumbs] = NULL;
							
							// Debug
							debug("Creating thumbnails from file:".return_array($this->c[thumbs]),$this->c[debug]);
							
							// File
							$f = new file($this->c[path].$value);
							// Thumbs - image
							if($f->type == "image") {
								foreach($this->c[thumbs] as $thumb => $v) {
									$thumbed = 0;
									
									// Already thumbed - might have selected a previously uploaded file via the file browser
									$f_thumb = new file($v[path].$f->name);
									if($f_thumb->exists and !is_array($value_original)) {
										debug("already thumbed, not re-thumbing",$this->c[debug]);
										$thumbed = 1;
									}
									// Thumb
									else {
										//$v[debug] = $this->c[debug];
										$f_thumb = new file($this->c[path].$value);
										$f_thumb->image_thumb($v[width],$v[height],$v);
										if($f_thumb_saved = $f_thumb->save($v[path])) {
											$f_thumb = $f_thumb_saved;
											if($f_thumb->exists) {
												$thumbed = 1;
											}
										}
									}
									
									// Store
									if($thumbed) {
										$v[name] = $f_thumb->name;
										$v[extension] = $f_thumb->extension;
										$v[size] = $f_thumb->size();
										//$v[width] = $f_thumb->width(); // Already set
										//$v[height] = $f_thumb->height(); // Already set
										$form->values_files[$this->name][thumbs][$thumb] = $v;
									}
								}
							}
							// Thumbs - video
							if($f->type == "video") {
								// Count/extension of screenshots (these should be the same across all thumb sizes, but check just to be sure)
								$count = 4;
								$extension = "jpg";
								foreach($this->c[thumbs] as $thumb => $v) {
									if($v[count] and $v[count] > $count) $count = $v[count];
									if($v[extension]) $extension = $v[extension]; // All using one for now
								}
								
								// Get video screenshot(s) (save to temp folder for now)
								$temp = $f->temp();
								$thumbs = $f->video_thumb($temp,array('number' => $count,'extension' => $extension,'debug' => $this->c[debug]));
								debug("video thumbs: ".return_array($thumbs),$this->c[debug]);
								
								// Resize screenshots
								if($thumbs) {
									foreach($this->c[thumbs] as $thumb => $v) {
										foreach($thumbs as $x => $thumb_name) {
											if($x <= $v[count]) {
												$thumbed = 0;
									
												// Already thumbed - might have selected a previously uploaded file via the file browser
												$f_thumb = new file($v[path].$thumb_name);
												if($f_thumb->exists and !is_array($value_original)) {
													debug("already thumbed, not re-thumbing",$this->c[debug]);
													$thumbed = 1;
												}
												// Thumb
												else {
													//$v[debug] = $this->c[debug];
													$f_thumb = new file($temp.$thumb_name);
													$f_thumb->image_thumb($v[width],$v[height],$v); // If no width/height it won't thumb and save will effectively 'copy' the original thumb.
													if($f_thumb_saved = $f_thumb->save($v[path])) {
														$f_thumb = $f_thumb_saved;
														if($f_thumb->exists) {
															$thumbed = 1;	
														}
													}
												}
												
												// Store
												if($thumbed) {
													$v[name] = $f_thumb->name;
													$v[extension] = $f_thumb->extension;
													$v[size] = $f_thumb->size();
													//$v[width] = $f_thumb->width(); // Already set
													//$v[height] = $f_thumb->height(); // Already set
													$form->values_files[$this->name][thumbs][$thumb][$x] = $v;
												}
											}
										}
									}
									// Delete temp video screenshot(s)
									foreach($thumbs as $x => $thumb_name) {
										debug("Deleting temp thumb: ".$temp.$thumb_name,$this->c[debug]);
										$f_thumb = new file($temp.$thumb_name);
										$f_thumb->delete();
									}
								}
							}
						}*/
					}
							
					// Values (framework only) - yes, this should be in the form_input_file_framework class, but that's a lot of duplicate code for a little change
					$values = NULL;
					if($form->module) {
						foreach($form->values as $k => $v) {
							if($key = array_search($k,m($form->module.'.db'))) {
								$values[$key] = $v;
							}
						}
					}
				}
				// Basic
				else {
					// Save
					$temp_file = (is_array($value) ? $value[tmp_name] : $value);
					move_uploaded_file($temp_file,$this->c[path]);
					$value = $value[name];
					debug("moved uploaded file from ".$value[tmp_name]." to ".$this->c[path],$this->c[debug]);
					
					// Store
					$form->values_files[$this->name]['new'] = 1;
					$form->values_files[$this->name][name] = $value;
					$form->values_files[$this->name][path] = $this->c[path];
					$form->values_files[$this->name][type] = ($this->c[type] ? $this->c[type] : "file");
				}
			
				// Delete
				if($this->c[old] and $this->c[old] != $value) $delete = 1;
			}
			// No file uploaded, did we delete the old one?
			else if($this->c[old]) {
				// Deleted?
				$deleted = form::value($form->post,'__file_deleted['.$this->name.']');
				// Yes
				if($deleted) {
					// Empty value
					$value = NULL;
					// Delete
					$delete = 1;
				}
				// No, use old value
				else $value = $this->c[old];	
			}
			// Nothing uploaded, no old value, no value
			else $value = NULL;
			
			// Delete old file / thumbs
			if($delete) {
				$this->delete($form,$c);
			}
			
			// Return
			return $value;
		}

		/**
		 * Determines and returns value for this input, taking multi-level array keys into account.
		 * 
		 * @param object $form The form object which contains the submitted values ($form->post and $form->files).
		 * @return mixed The retrieved value.
		 */
		function value($form) {
			return form::value_file($form->files,$this->name);
		}
		
		/**
		 * Deletes previous file after new one is uploaded.
		 *
		 * @param object $form The form object this input is a part of.
		 * @param array $c An array of config values. Default = NULL
		 */
		function delete($form,$c = NULL) {
			// Config
			if(!x($c[debug])) $c[debug] = 0; // Debug
			
			// Advanced
			if(class_exists('file')) {
				// File
				$f = file::load($this->c[path].$this->c[old],array('storage' => $this->c[storage]));
				if($f->file) {
					$f_name = $f->name(0);
					
					// Delete - extensions
					if($this->c[extensions]) {
						foreach($this->c[extensions] as $extension => $v) {
							$extension_file = $v[path].$f_name.".".$extension;
							$file = file::load($extension_file,array('storage' => $this->c[storage]));
							$file->delete();
							debug("deleted old extension file (".$extension."): ".$extension_file,$this->c[debug]);
						}
					}
					
					// Delete - thumbs
					if($this->c[thumbs]) {
						foreach($this->c[thumbs] as $thumb => $v) {
							for($x = 1;$x <= $v[count];$x++) {
								$thumb_file = $v[path].$f_name.($v[count] > 1 ? "_".$x : "").".".$v[extension];
								$file = file::load($thumb_file,array('storage' => $this->c[storage]));
								$file->delete();
								debug("deleted old thumb file (".$thumb.($v[count] > 1 ? "-".$x : "")."): ".$thumb_file,$this->c[debug]);
							}
						}
					}
					
					// Delete - file
					debug("deleting old file: ".$f->file,$this->c[debug]);
					$f->delete();
				}
			}
			// Basic
			else {
				if(is_file($this->c[path].$this->c[old])) {
					unlink($this->c[path].$this->c[old]);
				}
			}
		}
		
		/**
		 * Creates and returns the HTML for previewing a previously uploaded file.
		 *
		 * @param object $form The form object this input is a part of.
		 * @param array $c An array of config values. Default = NULL
		 * @return string The HTML of the file preview.
		 */
		function preview($form,$c = NULL) {
			// Value
			if($this->value and $this->c[path]) {
				// Config
				if(!x($c[delete])) $c[delete] = 1; // Show delete icon for removing the file
				
				// Random
				$r = "preview".random();
			
				// File
				$file = $this->c[path].$this->value;
			
				// File class
				if(class_exists('file')) {
					$f = new file($file);
					$type = $f->type;
				}
	
				// Container
				$text .= "
<div id='".$r."' class='form-preview".($type ? " form-preview-".$type : "")."'>";

				// Text
				$text .= "
	<div class='form-preview-keep'>Leave this field blank to keep the existing file.</div>
	<div class='clear'></div>";
	
				// Display
				$text .= "
	<div class='form-preview-media'>";
			
				// Display - advanced
				if($f) {
					// Image
					if($type == "image") $text .= "
		".$f->image_html(array('max_width' => 100,'max_height' => 100,'attributes' => " target='_blank'",'link' => $f->url(),'link_attributes' => " class='overlay'"));
					// Video
					else if($type == "video") {
						$video_c = array(
							'max_width' => 265,
							'auto' => 0,
						);
						// Extensions
						if($this->c[extensions]) {
							foreach($this->c[extensions] as $extension => $v) {
								$f_extension = file::load($v[path].str_replace(".".$f->extension(),".".$extension,$this->value));
								if($f_extension->exists) {
									// Same as original, don't want 2, don't add as fallback
									if($f_extension->extension == $f->extension) {
										$f = $f_extension; // Use converted video if one exists as it'll have all necessary tweaks/headers
									}
									// Different than original, add as fallback
									else {
										$video_c[fallback][] = $f_extension->file;	
									}
								}
							}
						}
						// HTML
						if($f) $text .= "
		".$f->video_html($video_c);
					}
					// Audio
					else if($type == "audio") $text .= "
		".$f->audio_html();
					// File
					else $text .= "
		<a href='".$f->url()."' target='_blank'>".basename($file)."</a>";
				}
				// Display - simple
				else {
					// URL
					$url = str_replace(SERVER,DOMAIN,$file);
					// File
					if($url) $text .= "
		<a href='".$url."' target='_blank'>".basename($file)."</a>";
					else $text .= basename($file);
				}
				
				$text .= "
	</div>";
	
				// Icons
				$text .= "
	<div class='form-preview-icons'>";
	
				// Delete
				if($c[delete]) {
					// Deleted input name
					$name = $this->name;
					$name = preg_replace('/\[/','][',$name,1); // Add first closing bracket bracket. Example: plugins[menu][item_image] becomes plugins][menu][item_image]
					$name = "__file_deleted[".$name;
					if(substr($name,-1) != "]") $name .= "]";
					
					// Required element selector
					if($this->attributes[id])  $selector = "#".$this->attributes[id].".required-input";
					else $selector = ".required-input:input[name=".$this->name."]"; // !!! This needs to be updated to work with a multilevel input names
					
					$text .= "
		<script type='text/javascript'>
		function deleteFile".$r."() {
			if(confirm('Are you sure you want to delete this file?')) {
				$('#".$r."').html(\"<input type='hidden' name='".$name."' value='1' />\");
				$('".$selector."').addClass('required'); // Re-require
			}
		}
		</script>
		<a href='javascript:void(0);' onclick='deleteFile".$r."();' class='i i-clear i-inline tooltip core-hover' title='Delete'></a>";
				}
				
				$text .= "
	</div>
	<div class='clear'></div>
</div>";
				
				// Return
				return $text;
			}
		}
	}
}
?>