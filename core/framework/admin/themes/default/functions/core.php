<?php
/**
 * @package kraken\themes\admin\default
 */
 
if(!function_exists('default_admin_tabs')) {
	/**
	 * Returns HTML for displaying the tabs at the top of the framework admin.
	 * 
	 * @param array $c An array of congiguration values. Default = NULL
	 * @return string The HTML of the admin tabs.
	 */
	function default_admin_tabs($c = NULL) {
		global $page;
		
		// Sections
		$sections = admin_sections_framework();
		
		// HTML
		$text .= "
<div class='admin-tabs'>
	<ul>";
		foreach($sections as $k => $v) {
			$text .= "
		<li".($v[$page->module] || $page->module == $k || $page->variables[0] == $k ? " class='selected'" : "").">
			<a href='".DOMAIN."admin/".$k."'>".ucwords($k)."</a>";
			
			# build sub modules / sub types here
			
			$text .= "
		</li>";
		}
		$text .= "
	</ul>
</div>";
		
		return $text;
	}
}

if(!function_exists('default_admin_sidebar')) {
	/**
	 * Returns HTML for displaying the sidebar of the framework admin.
	 * 
	 * @param array $c An array of congiguration values. Default = NULL
	 * @return string The HTML of the admin sidebar.
	 */
	function default_admin_sidebar($c = NULL) {
		global $page;
		
		// Sections
		$sections = admin_sections_framework();
		
		// HTML
		$text .= "
<div class='admin-sidebar'>
	<ul class='core-list-nobullets'>";
		foreach($sections as $k => $v) {
			$selected = ($v[$page->module] || $page->module == $k || $page->variables[0] == $k ? 1 : 0);
			$text .= "
		<li".($selected ? " class='selected'" : "").">
			<a href='".DOMAIN."admin/".$k."'>".ucwords($k)."</a>";
			if($selected and $k != "menu") {
				$text .= "
			<ul>";
				foreach($v as $module => $options) {
					// Types
					$types = NULL;
					if($options[types]) {
						foreach($options[types] as $type => $type_options) {
							if($type_options[sidebar] == 1) {
								$types .= "
				<li".($page->module == $module && $page->type == $type ? " class='selected'" : "")."><a href='".DOMAIN."admin/".$module."/".$type."'>".($options[sidebar] ? "&nbsp;&nbsp;&nbsp;&nbsp;" : "").m($module.'.name',$type)."</a></li>";
							}
						}
					}
					// Module
					if($options[sidebar]) $text .= "
				<li".($page->module == $module && !strstr($types,"class='selected'") ? " class='selected'" : "")."><a href='".DOMAIN."admin/".$module."'>".m($module.'.name')."</a></li>";
					if($types) $text .= $types;
				}
				$text .= "
			</ul>";
			}
			$text .= "
		</li>";
		}
		$text .= "
	</ul>
</div>";
		
		return $text;
	}
}
?>