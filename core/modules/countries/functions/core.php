<?php
/**
 * @package kraken\modules\countries
 */
 
if(!function_exists('country_id')) {
	/**
	 * Returns the id of the country that corresponds to the given country code.
	 *
	 * @param string $code The country code you want to get the id of.
	 * @return int The id of the country.
	 */
	function country_id($code) {
		// Error
		if(!$code) return;
		
		// Standardize
		$code = strtoupper($code);
		
		// Quick answer
		if($code == "US") return 234;
		if($code == "CA") return 38;
		
		// Database
		$db = db::load();
		
		// Country
		$row = $db->f("SELECT country_id FROM countries WHERE country_code = '".a($code)."'");
		
		// Return
		return $row[country_id];
	}
}
 
if(!function_exists('country_code')) {
	/**
	 * Returns the code of the country that corresponds to the given country id.
	 *
	 * @param int|array|object $id The id, database row, or item objectof the country you want to get the code of
	 * @return string The corresponding country code.
	 */
	function country_code($id) {
		// Error
		if(!$id) return;
		
		// Quick answer
		if($id == 234) return "US";
		if($id == 38) return "CA";
		
		// Item
		if(is_object($id)) {
			$item = $id;
			$id = $item->id;
		}
		else $item = item::load('countries',$id);
		
		// Return
		return $item->v('country_code');
	}
}
 
if(!function_exists('country_name')) {
	/**
	 * Returns the name of the country that corresponds to the given country id.
	 *
	 * @param int|array|object $id The id, database row, or item objectof the country you want to get the name of
	 * @return string The corresponding country name.
	 */
	function country_name($id) {
		// Error
		if(!$id) return;
		
		// Quick answer
		if($id == 234) return "United States";
		if($id == 38) return "Canada";
		
		// Item
		if(is_object($id)) {
			$item = $id;
			$id = $item->id;
		}
		else $item = item::load('countries',$id);
		
		// Return
		return $item->v('country_name');
	}
}
 
if(!function_exists('countries_form_toggle_javascript')) {
	/**
	 * Returns javascript for handling selection of a country and the loading of the states within that country.
	 *
	 * @param string $country The jQuery selectory for country <select> element.
	 * @param string $state_row The jQuery selectory for the form row (not the actual <select>) that contains the state input.
	 * @param string $zip_row The jQuery selectory for the form row (not the actual <input>) that contains the zip input.
	 * @param array $c An array of configuration values. Default = NULL
	 * @return string The javascript for handling country/state relationships in a form.
	 */
	function countries_form_toggle_javascript($country,$state_row,$zip_row,$c = NULL) {
		// Random
		$r = random();
		
		// Javascript
		$javascript = "
<script type='text/javascript'>
$(document).ready(function() { 
	$('".$country."').change(function() {
		countries_form_toggle_".$r."();
	});
	countries_form_toggle_".$r."();
});
function countries_form_toggle_".$r."() {
	var country = $('".$country."').val();
	var \$state_row = $('".$state_row."');
	var \$state_input = $('select',\$state_row);
	var \$state_placeholder = $('option:first',\$state_input);
	var state = \$state_input.val();
	var state_placeholder = '';
	if(\$state_placeholder.val() == '') state_placeholder = \$state_placeholder.text();
	var \$zip_row = $('".$zip_row."');
	
	// States
	$.ajax({
		type: 'POST',
		url: DOMAIN,
		data: {
			ajax_module: 'countries',
			ajax_action: 'countries_states',
			id: country,
			state: state,
			state_placeholder: state_placeholder
		},
		dataType: 'json',
		success: function(result) {
			//debug('state label: '+result.state.label+', zip label: '+result.zip.label+', states: <xmp>'+result.state.options+'</xmp>, debug: <xmp>'+result.debug+'</xmp>');
			
			// State
			if(!result.state.options) \$state_row.hide().children('.required-input').removeClass('required');
			else \$state_row.show().children('.required-input').addClass('required');
			\$state_input.html(result.state.options);
			
			// State label
			$('div.form-label',\$state_row).html(result.state.label);
			
			// Zip label
			$('div.form-label',\$zip_row).html(result.zip.label);
		}
	});
}
</script>";
		
		// Return
		return $javascript;
	}
}
?>