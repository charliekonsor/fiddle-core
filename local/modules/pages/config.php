<?php
/************************************************************************************/
/************************************** Config **************************************/
/************************************************************************************/
$config = array(
	'name' => 'Pages',
	'single' => 'Page',
	'plural' => 'Pages',
	'icon' => 'page',
	'db' => array(
		'table' => 'pages',
		'id' => 'page_id',
		'code' => 'page_code',
		'name' => 'page_name',
		'theme' => 'page_theme',
		'template' => 'page_template',
		'meta_title' => 'page_meta_title',
		'meta_description' => 'page_meta_description',
		'meta_keywords' => 'page_meta_keywords',
		'published' => 'page_published',
		'published_start' => 'page_published_start',
		'published_end' => 'page_published_end',
		'visible' => 'page_visible',
		'created' => 'page_created',
		'updated' => 'page_updated'
	),
	'admin' => array(
		'views' => array(
			'home' => 'All Pages',
			'item' => 'Edit Page'
		)
	),
	'public' => array(
		'views' => array(
			'item' => 'Page'
		),
		'urls' => array(
			'item' => array(
				'format' => '{name}'
			)
		),
		'meta' => array(
			'item' => array(
				'title' => array(
					'format' => '{page.item.meta.meta_title} | {modules.settings.settings.site.meta.title}'
				)
			)
		)
	),
	'plugins' => array(
		'published' => 1,
		'permissions' => 1,
		'sitemap' => 1,
	),
	'settings' => array(
		// Plugins - hidden settings and default settings (only if they overwrite the plugin's default settings)
		'sitemap' => array(
			'views' => array(
				'item'
			)
		),
		'search' => array(
			'public' => 1,
			'index' => 1,
			'columns' => array(
				'page_text' => 1
			)
		),
	),
	'forms' => array(
		'add' => array(
			'inputs' => array(
				array(
					'label' => 'Name',
					'type' => 'text',
					'name' => 'page_name',
					'validate' => array(
						'required' => 1,
					),
				),
				array(
					'label' => 'Text',
					'type' => 'ckeditor',
					'name' => 'page_text',
				)
			)
		)
	)
);
?>