<?php
if(!class_exists('form_input_text',false)) {
	/**
	 * Creates a text input
	 */
	class form_input_text extends form_input_framework {
		/**
		 * Loads class. Needed so we can use debug_backtrace() to get this class name in PHP < 5.3 in $form_input->load().
		 */
		static function load($label = NULL,$name = NULL,$value = NULL,$c = NULL) {
			return parent::load($label,$name,$value,$c);
		}
		
		/**
		 * Construct
		 */
		function __construct($label = NULL,$name = NULL,$value = NULL,$c = NULL) {
			$this->form_input_text($label,$name,$value,$c);
		}
		function form_input_text($label = NULL,$name = NULL,$value = NULL,$c = NULL) {
			// Construct
			parent::__construct($label,$name,$value,$c);
		}
	}
}
?>