<?php
/**
 * Gets dropdown options for a state input based upon the given country.
 */
if($_POST['ajax_action'] == "countries_states") {
	// Results
	$results = NULL;
	
	$results[debug] .= return_array($_POST);
	
	// Country
	$country = $_POST['id'];
	if($country == "null") $country = NULL;
	if(!is_array($country)) $country = array($country);
	$country = array_filter($country);
	//if(!x($country)) $country = array(234); // Default to United States
	$country_count = count($country);
	$results[debug] .= "country: ".return_array($country);
	if($country_count == 1) {
		$item = item::load('countries',array_first($country));
		$results[state][label] = $item->v('country_state_label');
		$results[zip][label] = $item->v('country_zip_label');
		$results[zip][regex] = $item->v('country_zip_regex');
	}
	else {
		$results[state][label] = "State/Region";
		$results[zip][label] = "Postal Code";
		$results[zip][regex] = NULL;
	}
	
	// States - placeholder
	if($_POST['state_placeholder']) $results[state][options] .= "
	<option value=''>".$_POST['state_placeholder']."</option>";
	
	// States - options
	if($country_count) {
		$query = "SELECT s.*,c.country_name FROM states s JOIN countries c ON s.country_id = c.country_id WHERE s.country_id IN (".implode(',',$country).") ORDER BY c.country_name ASC, s.state_name ASC";
		$results[debug] .= $query."<br />";
		$country_saved = NULL;
		$rows = $core->db->q($query);
		while($row = $core->db->f($rows)) {
			if($country_count > 1 and $country_saved != $row[country_name]) {
				if($country) $results[state][options] .= "
	</optgroup>";
				$results[state][options] .= "
	<optgroup label='".string_encode($row[country_name])."'>";
				$country_saved = $row[country_name];
			}
			$results[state][options] .= "
	<option value='".$row[state_id]."'".($_POST['state'] == $row[state_id] || in_array($row[state_id],$_POST['state']) ? " selected='selected'" : "").">".s($row[state_name])."</option>";
		}
		if($country_saved) $results[state][options] .= "
	</optgroup>";
	}
	else {
		$results[state][options] .= "
	<option value=''>Please select a country</option>";
	}
	
	// Resutls
	print json_encode($results);
	
	// Exit
	exit;
}
?>